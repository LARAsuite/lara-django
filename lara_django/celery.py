"""_____________________________________________________________________

:PROJECT: LARAsuite

*lara_django celery configuration*

:details: lara_django celery configuration.
         - to run the celery worker use the following command:
              celery -A lara_django worker -l info
              
:authors: mark doerr <mark.doerr@uni-greifswald.de>

.. note:: -
.. todo:: - 
________________________________________________________________________
"""

from __future__ import absolute_import, unicode_literals

import os

from celery import Celery
from django.conf import settings

from django.core.management import call_command

# set the default Django settings module for the 'celery' program.
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'lara_django.settings.development')

app = Celery('lara_django')

# Using a string here means the worker will not have to
# pickle the object when using Windows.

app.config_from_object('django.conf:settings', namespace='CELERY')

# Load task modules from all registered Django app configs.
app.autodiscover_tasks()

