"""
________________________________________________________________________

:PROJECT: lara-django

*lara-django development settings*

:details: Django settings for lara project.

:authors: mark doerr (mark@uni-greifswald.de)

:date: (creation)          20180623

.. note:: -
.. todo:: -
________________________________________________________________________

**Copyright**:
  This file is provided "AS IS" with NO WARRANTY OF ANY KIND,
  INCLUDING THE WARRANTIES OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.

  For further Information see LICENSE file that comes with this distribution.
________________________________________________________________________
"""

import os
import sys
import logging

from lara_django.settings.development import *

# this is a development setting, this should improve the performance of the unit tests
PASSWORD_HASHERS = ["django.contrib.auth.hashers.MD5PasswordHasher"]

if AWS_STORAGE_BUCKET_NAME:
    STORAGES = {
        "default": {
            "BACKEND": "storages.backends.s3boto3.S3Boto3Storage",
            "OPTIONS": {
                #"location": "media/",
                "bucket_name": env("AWS_STORAGE_MEDIA_BUCKET_NAME"),
                "access_key": env("AWS_S3_ACCESS_KEY_ID"),
                "secret_key": env("AWS_S3_SECRET_ACCESS_KEY"),
                "file_overwrite" : False,
                "endpoint_url" : env("AWS_S3_ENDPOINT_URL"),
                "custom_domain": env("AWS_S3_CUSTOM_DOMAIN"),
                "use_ssl" : False,  
            },
        },
        "staticfiles": {
        "BACKEND": "django.contrib.staticfiles.storage.StaticFilesStorage",
        },
        # "staticfiles": {
        #    "BACKEND": "storages.backends.s3boto3.S3Boto3Storage",
        #     "OPTIONS": {
        #         "use_ssl" : False,
        #         "verify" : False,
        #         "bucket_name": env("AWS_STORAGE_STATIC_BUCKET_NAME"),
        #         "access_key": env("AWS_S3_ACCESS_KEY_ID"),
        #         "secret_key": env("AWS_S3_SECRET_ACCESS_KEY"),
        #         "file_overwrite" : False,
        #         "endpoint_url" : env("AWS_S3_ENDPOINT_URL"),
        #         "custom_domain": env("AWS_S3_CUSTOM_DOMAIN"),
            
        #     },
        # },
    }
else:  # fallback to local file system
    print("INFO: using the local file system, for static and media files as AWS_STORAGE_BUCKET_NAME not set.")
    STORAGES = {
        "default": {
            "BACKEND": "django.core.files.storage.FileSystemStorage",
            "OPTIONS": {},
        },
        "staticfiles": {
            "BACKEND": "django.contrib.staticfiles.storage.StaticFilesStorage",
        },
    }

