"""
________________________________________________________________________

:PROJECT: lara-django

*lara-django development settings*

:details: Django settings for lara project.

:authors: mark doerr (mark@uni-greifswald.de)

:date: (creation)          20180623

.. note:: -
.. todo:: -
________________________________________________________________________

**Copyright**:
  This file is provided "AS IS" with NO WARRANTY OF ANY KIND,
  INCLUDING THE WARRANTIES OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.

  For further Information see LICENSE file that comes with this distribution.
________________________________________________________________________
"""

import os
import sys
import logging

from lara_django.settings.base import * # noqa: F403

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = env("DJANGO_DEBUG", default=True)
EMAIL_BACKEND = "django.core.mail.backends.console.EmailBackend"

MIDDLEWARE += [
    'debug_toolbar.middleware.DebugToolbarMiddleware',
]

# include lara core apps
LARA_APPS = env.list("LARA_APPS", default=['lara_django_base',
                                           'lara_django_people', 'lara_django_store',
                                           'lara_django_material', 'lara_django_material_store',
                                           'lara_django_organisms',  'lara_django_organisms_store',
                                           'lara_django_samples',
                                           'lara_django_sequences', 'lara_django_substances', 'lara_django_substances_store',
                                           'lara_django_data', 'lara_django_processes', 'lara_django_projects'
                                           ])

DJNAGO_DEV_APPS = [
    # this is only important in the development env.
    #'django.contrib.staticfiles',
    'django_extensions',
    'debug_toolbar',
]

# https://docs.djangoproject.com/en/dev/ref/settings/#installed-apps


INSTALLED_APPS = DJANGO_APPS + THIRD_PARTY_APPS + DJNAGO_DEV_APPS + LARA_APPS


# LOGIN_REDIRECT_URL = 'index'
# LOGOUT_REDIRECT_URL = 'index'

# STATICFILES_FINDERS = [
#     "django.contrib.staticfiles.finders.FileSystemFinder",
#     "django.contrib.staticfiles.finders.AppDirectoriesFinder",
# ]



# STATIC
# ------------------------------------------------------------------------------
# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/5.1/howto/static-files/
# STATIC ROOT is not required necessarily required in development - only, if you want to serve use collectstatic
#STATIC_ROOT = env("DJANGO_STATIC_ROOT", default=os.path.join(BASE_DIR, "static"))
# Absolute path to the media directory
# this is not required in devel
#MEDIA_ROOT = env("DJANGO_MEDIA_ROOT", default=os.path.join(BASE_DIR, "media"))
#print(f"MEDIA_ROOT -~~~~ {MEDIA_ROOT}" )

# might not be required in development 
#STATICFILES_DIRS = [
   # os.path.join(BASE_DIR, "static/"),
    #DJANGO_MEDIA_PATH  # media_path
#]

# this is used by debug_toolbar
INTERNAL_IPS = ("127.0.0.1",)

# advanced logging

# A sample logging configuration. The only tangible logging
# performed by this configuration is to send an email to
# the site admins on every HTTP 500 error.
# See http://docs.djangoproject.com/en/dev/topics/logging for
# more details on how to customize your logging configuration.
# LOGGING = {
#     "version": 1,
#     "disable_existing_loggers": False,
#     "handlers": {"mail_admins": {"level": "ERROR", "class": "django.utils.log.AdminEmailHandler"}},
#     "loggers": {
#         "django.request": {"handlers": ["mail_admins"], "level": "ERROR", "propagate": True}
#     },
# }


# django-minio-storage settings (deprecated)
#DEFAULT_FILE_STORAGE = "minio_storage.storage.MinioMediaStorage"
# STATICFILES_STORAGE = "minio_storage.storage.MinioStaticStorage"
# MINIO_STORAGE_ENDPOINT = 'localhost:9900'
# MINIO_STORAGE_ACCESS_KEY = "minio"  #= 'KBP6WXGPS387090EZMG8'
# MINIO_STORAGE_SECRET_KEY = "minio123" #= 'DRjFXylyfMqn2zilAr33xORhaYz5r9e8r37XPz3A'
# MINIO_STORAGE_USE_HTTPS = False
# MINIO_STORAGE_MEDIA_OBJECT_METADATA = {"Cache-Control": "max-age=1000"}
# MINIO_STORAGE_MEDIA_BUCKET_NAME = 'media'
# MINIO_STORAGE_MEDIA_BACKUP_BUCKET = 'RecycleBin'
# MINIO_STORAGE_MEDIA_BACKUP_FORMAT = '%c/'
# MINIO_STORAGE_AUTO_CREATE_MEDIA_BUCKET = True
# MINIO_STORAGE_STATIC_BUCKET_NAME = 'static'
# MINIO_STORAGE_AUTO_CREATE_STATIC_BUCKET = True


AWS_STORAGE_BUCKET_NAME = env('AWS_STORAGE_MEDIA_BUCKET_NAME', default=None)
AWS_S3_CUSTOM_DOMAIN = env('AWS_S3_CUSTOM_DOMAIN', default=None)
AWS_S3_USE_SSL = False #env('AWS_S3_USE_SSL', default=False)

# django-storages settings
# for parameters see: https://django-storages.readthedocs.io/en/latest/backends/amazon-S3.html

if AWS_STORAGE_BUCKET_NAME is not None:
    # STORAGES = {
    #     "default": {
    #         "BACKEND": "storages.backends.s3boto3.S3Boto3Storage",
    #         "OPTIONS": {
    #             #"location": "media/",
    #             "bucket_name": env("AWS_STORAGE_MEDIA_BUCKET_NAME"),
    #             "access_key": env("AWS_S3_ACCESS_KEY_ID"),
    #             "secret_key": env("AWS_S3_SECRET_ACCESS_KEY"),
    #             "file_overwrite" : False,
    #             "endpoint_url" : env("AWS_S3_ENDPOINT_URL"),
    #             "custom_domain": env("AWS_S3_CUSTOM_DOMAIN"),
    #             "use_ssl" : False,  
    #         },
    #     },
    #     "staticfiles": {
    #     "BACKEND": "django.contrib.staticfiles.storage.StaticFilesStorage",
    #     },
        # "staticfiles": {
        #    "BACKEND": "storages.backends.s3boto3.S3Boto3Storage",
        #     "OPTIONS": {
        #         "use_ssl" : False,
        #         "verify" : False,
        #         "bucket_name": env("AWS_STORAGE_STATIC_BUCKET_NAME"),
        #         "access_key": env("AWS_S3_ACCESS_KEY_ID"),
        #         "secret_key": env("AWS_S3_SECRET_ACCESS_KEY"),
        #         "file_overwrite" : False,
        #         "endpoint_url" : env("AWS_S3_ENDPOINT_URL"),
        #         "custom_domain": env("AWS_S3_CUSTOM_DOMAIN"),
            
        #     },
        # },
    #}
    pass
else:  # fallback to local file system
    print("INFO: using the local file system, for static and media files as AWS_STORAGE_BUCKET_NAME not set.")
    STORAGES = {
        "default": {
            "BACKEND": "django.core.files.storage.FileSystemStorage",
            "OPTIONS": {},
        },
        "staticfiles": {
            "BACKEND": "django.contrib.staticfiles.storage.StaticFilesStorage",
        },
    }




# TODO: move to logger 
print("________ MinIO settings ________")
print("AWS_STORAGE_BUCKET_NAME: ", AWS_STORAGE_BUCKET_NAME )
print("AWS_S3_CUSTOM_DOMAIN: ", AWS_S3_CUSTOM_DOMAIN )
print("DJANGO_MEDIA_ROOT: ", MEDIA_ROOT)
print("DJANGO_STATIC_ROOT: ", STATIC_ROOT)
#print("AWS_S3_ENDPOINT_URL: ", AWS_S3_ENDPOINT_URL)
# print("AWS_ACCESS_KEY_ID: ", AWS_ACCESS_KEY_ID)
# print("AWS_SECRET_ACCESS_KEY: ", AWS_SECRET_ACCESS_KEY)



# LOGGING
# ------------------------------------------------------------------------------
# https://docs.djangoproject.com/en/dev/ref/settings/#logging
# See https://docs.djangoproject.com/en/dev/topics/logging for
# more details on how to customize your logging configuration.
LOGGING = {
    "version": 1,
    "disable_existing_loggers": False,
    "formatters": {
        "ultra_verbose": {
            "format": "%(levelname)s| %(asctime)s - %(module)s %(process)d %(thread)d | %(message)s",
        },
        "verbose": {
            "format": "%(levelname)s| %(asctime)s - %(module)s | %(message)s",
        },
        "simple": {
            "format": "%(levelname)s| %(message)s",
        },
    },
    "handlers": {
        "console": {
            "level": "INFO",
            "class": "logging.StreamHandler",
            "formatter": "simple",
        },
        "file": {
            "level": "INFO",
            "formatter": "verbose",
            "class": "logging.FileHandler",
            "filename": env("DJANGO_LOG_FILE", default=os.path.join(BASE_DIR, "lara_django.log")),
        },
    },
    "root": {"level": "INFO", "handlers": ["console"]},
    "loggers": {
        "django": {
            "handlers": ["file"],
            "level": "INFO",
            "propagate": True,
        },
    },
}
