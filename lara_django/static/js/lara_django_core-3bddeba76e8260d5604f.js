/******/ (() => { // webpackBootstrap
/******/ 	"use strict";
/******/ 	var __webpack_modules__ = ({

/***/ "./node_modules/htmx.org/dist/htmx.esm.js":
/*!************************************************!*\
  !*** ./node_modules/htmx.org/dist/htmx.esm.js ***!
  \************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
var htmx = function () {
  'use strict';

  // Public API
  const htmx = {
    // Tsc madness here, assigning the functions directly results in an invalid TypeScript output, but reassigning is fine
    /* Event processing */
    /** @type {typeof onLoadHelper} */
    onLoad: null,
    /** @type {typeof processNode} */
    process: null,
    /** @type {typeof addEventListenerImpl} */
    on: null,
    /** @type {typeof removeEventListenerImpl} */
    off: null,
    /** @type {typeof triggerEvent} */
    trigger: null,
    /** @type {typeof ajaxHelper} */
    ajax: null,
    /* DOM querying helpers */
    /** @type {typeof find} */
    find: null,
    /** @type {typeof findAll} */
    findAll: null,
    /** @type {typeof closest} */
    closest: null,
    /**
     * Returns the input values that would resolve for a given element via the htmx value resolution mechanism
     *
     * @see https://htmx.org/api/#values
     *
     * @param {Element} elt the element to resolve values on
     * @param {HttpVerb} type the request type (e.g. **get** or **post**) non-GET's will include the enclosing form of the element. Defaults to **post**
     * @returns {Object}
     */
    values: function (elt, type) {
      const inputValues = getInputValues(elt, type || 'post');
      return inputValues.values;
    },
    /* DOM manipulation helpers */
    /** @type {typeof removeElement} */
    remove: null,
    /** @type {typeof addClassToElement} */
    addClass: null,
    /** @type {typeof removeClassFromElement} */
    removeClass: null,
    /** @type {typeof toggleClassOnElement} */
    toggleClass: null,
    /** @type {typeof takeClassForElement} */
    takeClass: null,
    /** @type {typeof swap} */
    swap: null,
    /* Extension entrypoints */
    /** @type {typeof defineExtension} */
    defineExtension: null,
    /** @type {typeof removeExtension} */
    removeExtension: null,
    /* Debugging */
    /** @type {typeof logAll} */
    logAll: null,
    /** @type {typeof logNone} */
    logNone: null,
    /* Debugging */
    /**
     * The logger htmx uses to log with
     *
     * @see https://htmx.org/api/#logger
     */
    logger: null,
    /**
     * A property holding the configuration htmx uses at runtime.
     *
     * Note that using a [meta tag](https://htmx.org/docs/#config) is the preferred mechanism for setting these properties.
     *
     * @see https://htmx.org/api/#config
     */
    config: {
      /**
       * Whether to use history.
       * @type boolean
       * @default true
       */
      historyEnabled: true,
      /**
       * The number of pages to keep in **localStorage** for history support.
       * @type number
       * @default 10
       */
      historyCacheSize: 10,
      /**
       * @type boolean
       * @default false
       */
      refreshOnHistoryMiss: false,
      /**
       * The default swap style to use if **[hx-swap](https://htmx.org/attributes/hx-swap)** is omitted.
       * @type HtmxSwapStyle
       * @default 'innerHTML'
       */
      defaultSwapStyle: 'innerHTML',
      /**
       * The default delay between receiving a response from the server and doing the swap.
       * @type number
       * @default 0
       */
      defaultSwapDelay: 0,
      /**
       * The default delay between completing the content swap and settling attributes.
       * @type number
       * @default 20
       */
      defaultSettleDelay: 20,
      /**
       * If true, htmx will inject a small amount of CSS into the page to make indicators invisible unless the **htmx-indicator** class is present.
       * @type boolean
       * @default true
       */
      includeIndicatorStyles: true,
      /**
       * The class to place on indicators when a request is in flight.
       * @type string
       * @default 'htmx-indicator'
       */
      indicatorClass: 'htmx-indicator',
      /**
       * The class to place on triggering elements when a request is in flight.
       * @type string
       * @default 'htmx-request'
       */
      requestClass: 'htmx-request',
      /**
       * The class to temporarily place on elements that htmx has added to the DOM.
       * @type string
       * @default 'htmx-added'
       */
      addedClass: 'htmx-added',
      /**
       * The class to place on target elements when htmx is in the settling phase.
       * @type string
       * @default 'htmx-settling'
       */
      settlingClass: 'htmx-settling',
      /**
       * The class to place on target elements when htmx is in the swapping phase.
       * @type string
       * @default 'htmx-swapping'
       */
      swappingClass: 'htmx-swapping',
      /**
       * Allows the use of eval-like functionality in htmx, to enable **hx-vars**, trigger conditions & script tag evaluation. Can be set to **false** for CSP compatibility.
       * @type boolean
       * @default true
       */
      allowEval: true,
      /**
       * If set to false, disables the interpretation of script tags.
       * @type boolean
       * @default true
       */
      allowScriptTags: true,
      /**
       * If set, the nonce will be added to inline scripts.
       * @type string
       * @default ''
       */
      inlineScriptNonce: '',
      /**
       * If set, the nonce will be added to inline styles.
       * @type string
       * @default ''
       */
      inlineStyleNonce: '',
      /**
       * The attributes to settle during the settling phase.
       * @type string[]
       * @default ['class', 'style', 'width', 'height']
       */
      attributesToSettle: ['class', 'style', 'width', 'height'],
      /**
       * Allow cross-site Access-Control requests using credentials such as cookies, authorization headers or TLS client certificates.
       * @type boolean
       * @default false
       */
      withCredentials: false,
      /**
       * @type number
       * @default 0
       */
      timeout: 0,
      /**
       * The default implementation of **getWebSocketReconnectDelay** for reconnecting after unexpected connection loss by the event code **Abnormal Closure**, **Service Restart** or **Try Again Later**.
       * @type {'full-jitter' | ((retryCount:number) => number)}
       * @default "full-jitter"
       */
      wsReconnectDelay: 'full-jitter',
      /**
       * The type of binary data being received over the WebSocket connection
       * @type BinaryType
       * @default 'blob'
       */
      wsBinaryType: 'blob',
      /**
       * @type string
       * @default '[hx-disable], [data-hx-disable]'
       */
      disableSelector: '[hx-disable], [data-hx-disable]',
      /**
       * @type {'auto' | 'instant' | 'smooth'}
       * @default 'instant'
       */
      scrollBehavior: 'instant',
      /**
       * If the focused element should be scrolled into view.
       * @type boolean
       * @default false
       */
      defaultFocusScroll: false,
      /**
       * If set to true htmx will include a cache-busting parameter in GET requests to avoid caching partial responses by the browser
       * @type boolean
       * @default false
       */
      getCacheBusterParam: false,
      /**
       * If set to true, htmx will use the View Transition API when swapping in new content.
       * @type boolean
       * @default false
       */
      globalViewTransitions: false,
      /**
       * htmx will format requests with these methods by encoding their parameters in the URL, not the request body
       * @type {(HttpVerb)[]}
       * @default ['get', 'delete']
       */
      methodsThatUseUrlParams: ['get', 'delete'],
      /**
       * If set to true, disables htmx-based requests to non-origin hosts.
       * @type boolean
       * @default false
       */
      selfRequestsOnly: true,
      /**
       * If set to true htmx will not update the title of the document when a title tag is found in new content
       * @type boolean
       * @default false
       */
      ignoreTitle: false,
      /**
       * Whether the target of a boosted element is scrolled into the viewport.
       * @type boolean
       * @default true
       */
      scrollIntoViewOnBoost: true,
      /**
       * The cache to store evaluated trigger specifications into.
       * You may define a simple object to use a never-clearing cache, or implement your own system using a [proxy object](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/Proxy)
       * @type {Object|null}
       * @default null
       */
      triggerSpecsCache: null,
      /** @type boolean */
      disableInheritance: false,
      /** @type HtmxResponseHandlingConfig[] */
      responseHandling: [{
        code: '204',
        swap: false
      }, {
        code: '[23]..',
        swap: true
      }, {
        code: '[45]..',
        swap: false,
        error: true
      }],
      /**
       * Whether to process OOB swaps on elements that are nested within the main response element.
       * @type boolean
       * @default true
       */
      allowNestedOobSwaps: true
    },
    /** @type {typeof parseInterval} */
    parseInterval: null,
    /** @type {typeof internalEval} */
    _: null,
    version: '2.0.4'
  };
  // Tsc madness part 2
  htmx.onLoad = onLoadHelper;
  htmx.process = processNode;
  htmx.on = addEventListenerImpl;
  htmx.off = removeEventListenerImpl;
  htmx.trigger = triggerEvent;
  htmx.ajax = ajaxHelper;
  htmx.find = find;
  htmx.findAll = findAll;
  htmx.closest = closest;
  htmx.remove = removeElement;
  htmx.addClass = addClassToElement;
  htmx.removeClass = removeClassFromElement;
  htmx.toggleClass = toggleClassOnElement;
  htmx.takeClass = takeClassForElement;
  htmx.swap = swap;
  htmx.defineExtension = defineExtension;
  htmx.removeExtension = removeExtension;
  htmx.logAll = logAll;
  htmx.logNone = logNone;
  htmx.parseInterval = parseInterval;
  htmx._ = internalEval;
  const internalAPI = {
    addTriggerHandler,
    bodyContains,
    canAccessLocalStorage,
    findThisElement,
    filterValues,
    swap,
    hasAttribute,
    getAttributeValue,
    getClosestAttributeValue,
    getClosestMatch,
    getExpressionVars,
    getHeaders,
    getInputValues,
    getInternalData,
    getSwapSpecification,
    getTriggerSpecs,
    getTarget,
    makeFragment,
    mergeObjects,
    makeSettleInfo,
    oobSwap,
    querySelectorExt,
    settleImmediately,
    shouldCancel,
    triggerEvent,
    triggerErrorEvent,
    withExtensions
  };
  const VERBS = ['get', 'post', 'put', 'delete', 'patch'];
  const VERB_SELECTOR = VERBS.map(function (verb) {
    return '[hx-' + verb + '], [data-hx-' + verb + ']';
  }).join(', ');

  //= ===================================================================
  // Utilities
  //= ===================================================================

  /**
   * Parses an interval string consistent with the way htmx does. Useful for plugins that have timing-related attributes.
   *
   * Caution: Accepts an int followed by either **s** or **ms**. All other values use **parseFloat**
   *
   * @see https://htmx.org/api/#parseInterval
   *
   * @param {string} str timing string
   * @returns {number|undefined}
   */
  function parseInterval(str) {
    if (str == undefined) {
      return undefined;
    }
    let interval = NaN;
    if (str.slice(-2) == 'ms') {
      interval = parseFloat(str.slice(0, -2));
    } else if (str.slice(-1) == 's') {
      interval = parseFloat(str.slice(0, -1)) * 1000;
    } else if (str.slice(-1) == 'm') {
      interval = parseFloat(str.slice(0, -1)) * 1000 * 60;
    } else {
      interval = parseFloat(str);
    }
    return isNaN(interval) ? undefined : interval;
  }

  /**
   * @param {Node} elt
   * @param {string} name
   * @returns {(string | null)}
   */
  function getRawAttribute(elt, name) {
    return elt instanceof Element && elt.getAttribute(name);
  }

  /**
   * @param {Element} elt
   * @param {string} qualifiedName
   * @returns {boolean}
   */
  // resolve with both hx and data-hx prefixes
  function hasAttribute(elt, qualifiedName) {
    return !!elt.hasAttribute && (elt.hasAttribute(qualifiedName) || elt.hasAttribute('data-' + qualifiedName));
  }

  /**
   *
   * @param {Node} elt
   * @param {string} qualifiedName
   * @returns {(string | null)}
   */
  function getAttributeValue(elt, qualifiedName) {
    return getRawAttribute(elt, qualifiedName) || getRawAttribute(elt, 'data-' + qualifiedName);
  }

  /**
   * @param {Node} elt
   * @returns {Node | null}
   */
  function parentElt(elt) {
    const parent = elt.parentElement;
    if (!parent && elt.parentNode instanceof ShadowRoot) return elt.parentNode;
    return parent;
  }

  /**
   * @returns {Document}
   */
  function getDocument() {
    return document;
  }

  /**
   * @param {Node} elt
   * @param {boolean} global
   * @returns {Node|Document}
   */
  function getRootNode(elt, global) {
    return elt.getRootNode ? elt.getRootNode({
      composed: global
    }) : getDocument();
  }

  /**
   * @param {Node} elt
   * @param {(e:Node) => boolean} condition
   * @returns {Node | null}
   */
  function getClosestMatch(elt, condition) {
    while (elt && !condition(elt)) {
      elt = parentElt(elt);
    }
    return elt || null;
  }

  /**
   * @param {Element} initialElement
   * @param {Element} ancestor
   * @param {string} attributeName
   * @returns {string|null}
   */
  function getAttributeValueWithDisinheritance(initialElement, ancestor, attributeName) {
    const attributeValue = getAttributeValue(ancestor, attributeName);
    const disinherit = getAttributeValue(ancestor, 'hx-disinherit');
    var inherit = getAttributeValue(ancestor, 'hx-inherit');
    if (initialElement !== ancestor) {
      if (htmx.config.disableInheritance) {
        if (inherit && (inherit === '*' || inherit.split(' ').indexOf(attributeName) >= 0)) {
          return attributeValue;
        } else {
          return null;
        }
      }
      if (disinherit && (disinherit === '*' || disinherit.split(' ').indexOf(attributeName) >= 0)) {
        return 'unset';
      }
    }
    return attributeValue;
  }

  /**
   * @param {Element} elt
   * @param {string} attributeName
   * @returns {string | null}
   */
  function getClosestAttributeValue(elt, attributeName) {
    let closestAttr = null;
    getClosestMatch(elt, function (e) {
      return !!(closestAttr = getAttributeValueWithDisinheritance(elt, asElement(e), attributeName));
    });
    if (closestAttr !== 'unset') {
      return closestAttr;
    }
  }

  /**
   * @param {Node} elt
   * @param {string} selector
   * @returns {boolean}
   */
  function matches(elt, selector) {
    // @ts-ignore: non-standard properties for browser compatibility
    // noinspection JSUnresolvedVariable
    const matchesFunction = elt instanceof Element && (elt.matches || elt.matchesSelector || elt.msMatchesSelector || elt.mozMatchesSelector || elt.webkitMatchesSelector || elt.oMatchesSelector);
    return !!matchesFunction && matchesFunction.call(elt, selector);
  }

  /**
   * @param {string} str
   * @returns {string}
   */
  function getStartTag(str) {
    const tagMatcher = /<([a-z][^\/\0>\x20\t\r\n\f]*)/i;
    const match = tagMatcher.exec(str);
    if (match) {
      return match[1].toLowerCase();
    } else {
      return '';
    }
  }

  /**
   * @param {string} resp
   * @returns {Document}
   */
  function parseHTML(resp) {
    const parser = new DOMParser();
    return parser.parseFromString(resp, 'text/html');
  }

  /**
   * @param {DocumentFragment} fragment
   * @param {Node} elt
   */
  function takeChildrenFor(fragment, elt) {
    while (elt.childNodes.length > 0) {
      fragment.append(elt.childNodes[0]);
    }
  }

  /**
   * @param {HTMLScriptElement} script
   * @returns {HTMLScriptElement}
   */
  function duplicateScript(script) {
    const newScript = getDocument().createElement('script');
    forEach(script.attributes, function (attr) {
      newScript.setAttribute(attr.name, attr.value);
    });
    newScript.textContent = script.textContent;
    newScript.async = false;
    if (htmx.config.inlineScriptNonce) {
      newScript.nonce = htmx.config.inlineScriptNonce;
    }
    return newScript;
  }

  /**
   * @param {HTMLScriptElement} script
   * @returns {boolean}
   */
  function isJavaScriptScriptNode(script) {
    return script.matches('script') && (script.type === 'text/javascript' || script.type === 'module' || script.type === '');
  }

  /**
   * we have to make new copies of script tags that we are going to insert because
   * SOME browsers (not saying who, but it involves an element and an animal) don't
   * execute scripts created in <template> tags when they are inserted into the DOM
   * and all the others do lmao
   * @param {DocumentFragment} fragment
   */
  function normalizeScriptTags(fragment) {
    Array.from(fragment.querySelectorAll('script')).forEach(/** @param {HTMLScriptElement} script */script => {
      if (isJavaScriptScriptNode(script)) {
        const newScript = duplicateScript(script);
        const parent = script.parentNode;
        try {
          parent.insertBefore(newScript, script);
        } catch (e) {
          logError(e);
        } finally {
          script.remove();
        }
      }
    });
  }

  /**
   * @typedef {DocumentFragment & {title?: string}} DocumentFragmentWithTitle
   * @description  a document fragment representing the response HTML, including
   * a `title` property for any title information found
   */

  /**
   * @param {string} response HTML
   * @returns {DocumentFragmentWithTitle}
   */
  function makeFragment(response) {
    // strip head tag to determine shape of response we are dealing with
    const responseWithNoHead = response.replace(/<head(\s[^>]*)?>[\s\S]*?<\/head>/i, '');
    const startTag = getStartTag(responseWithNoHead);
    /** @type DocumentFragmentWithTitle */
    let fragment;
    if (startTag === 'html') {
      // if it is a full document, parse it and return the body
      fragment = /** @type DocumentFragmentWithTitle */new DocumentFragment();
      const doc = parseHTML(response);
      takeChildrenFor(fragment, doc.body);
      fragment.title = doc.title;
    } else if (startTag === 'body') {
      // parse body w/o wrapping in template
      fragment = /** @type DocumentFragmentWithTitle */new DocumentFragment();
      const doc = parseHTML(responseWithNoHead);
      takeChildrenFor(fragment, doc.body);
      fragment.title = doc.title;
    } else {
      // otherwise we have non-body partial HTML content, so wrap it in a template to maximize parsing flexibility
      const doc = parseHTML('<body><template class="internal-htmx-wrapper">' + responseWithNoHead + '</template></body>');
      fragment = /** @type DocumentFragmentWithTitle */doc.querySelector('template').content;
      // extract title into fragment for later processing
      fragment.title = doc.title;

      // for legacy reasons we support a title tag at the root level of non-body responses, so we need to handle it
      var titleElement = fragment.querySelector('title');
      if (titleElement && titleElement.parentNode === fragment) {
        titleElement.remove();
        fragment.title = titleElement.innerText;
      }
    }
    if (fragment) {
      if (htmx.config.allowScriptTags) {
        normalizeScriptTags(fragment);
      } else {
        // remove all script tags if scripts are disabled
        fragment.querySelectorAll('script').forEach(script => script.remove());
      }
    }
    return fragment;
  }

  /**
   * @param {Function} func
   */
  function maybeCall(func) {
    if (func) {
      func();
    }
  }

  /**
   * @param {any} o
   * @param {string} type
   * @returns
   */
  function isType(o, type) {
    return Object.prototype.toString.call(o) === '[object ' + type + ']';
  }

  /**
   * @param {*} o
   * @returns {o is Function}
   */
  function isFunction(o) {
    return typeof o === 'function';
  }

  /**
   * @param {*} o
   * @returns {o is Object}
   */
  function isRawObject(o) {
    return isType(o, 'Object');
  }

  /**
   * @typedef {Object} OnHandler
   * @property {(keyof HTMLElementEventMap)|string} event
   * @property {EventListener} listener
   */

  /**
   * @typedef {Object} ListenerInfo
   * @property {string} trigger
   * @property {EventListener} listener
   * @property {EventTarget} on
   */

  /**
   * @typedef {Object} HtmxNodeInternalData
   * Element data
   * @property {number} [initHash]
   * @property {boolean} [boosted]
   * @property {OnHandler[]} [onHandlers]
   * @property {number} [timeout]
   * @property {ListenerInfo[]} [listenerInfos]
   * @property {boolean} [cancelled]
   * @property {boolean} [triggeredOnce]
   * @property {number} [delayed]
   * @property {number|null} [throttle]
   * @property {WeakMap<HtmxTriggerSpecification,WeakMap<EventTarget,string>>} [lastValue]
   * @property {boolean} [loaded]
   * @property {string} [path]
   * @property {string} [verb]
   * @property {boolean} [polling]
   * @property {HTMLButtonElement|HTMLInputElement|null} [lastButtonClicked]
   * @property {number} [requestCount]
   * @property {XMLHttpRequest} [xhr]
   * @property {(() => void)[]} [queuedRequests]
   * @property {boolean} [abortable]
   * @property {boolean} [firstInitCompleted]
   *
   * Event data
   * @property {HtmxTriggerSpecification} [triggerSpec]
   * @property {EventTarget[]} [handledFor]
   */

  /**
   * getInternalData retrieves "private" data stored by htmx within an element
   * @param {EventTarget|Event} elt
   * @returns {HtmxNodeInternalData}
   */
  function getInternalData(elt) {
    const dataProp = 'htmx-internal-data';
    let data = elt[dataProp];
    if (!data) {
      data = elt[dataProp] = {};
    }
    return data;
  }

  /**
   * toArray converts an ArrayLike object into a real array.
   * @template T
   * @param {ArrayLike<T>} arr
   * @returns {T[]}
   */
  function toArray(arr) {
    const returnArr = [];
    if (arr) {
      for (let i = 0; i < arr.length; i++) {
        returnArr.push(arr[i]);
      }
    }
    return returnArr;
  }

  /**
   * @template T
   * @param {T[]|NamedNodeMap|HTMLCollection|HTMLFormControlsCollection|ArrayLike<T>} arr
   * @param {(T) => void} func
   */
  function forEach(arr, func) {
    if (arr) {
      for (let i = 0; i < arr.length; i++) {
        func(arr[i]);
      }
    }
  }

  /**
   * @param {Element} el
   * @returns {boolean}
   */
  function isScrolledIntoView(el) {
    const rect = el.getBoundingClientRect();
    const elemTop = rect.top;
    const elemBottom = rect.bottom;
    return elemTop < window.innerHeight && elemBottom >= 0;
  }

  /**
   * Checks whether the element is in the document (includes shadow roots).
   * This function this is a slight misnomer; it will return true even for elements in the head.
   *
   * @param {Node} elt
   * @returns {boolean}
   */
  function bodyContains(elt) {
    return elt.getRootNode({
      composed: true
    }) === document;
  }

  /**
   * @param {string} trigger
   * @returns {string[]}
   */
  function splitOnWhitespace(trigger) {
    return trigger.trim().split(/\s+/);
  }

  /**
   * mergeObjects takes all the keys from
   * obj2 and duplicates them into obj1
   * @template T1
   * @template T2
   * @param {T1} obj1
   * @param {T2} obj2
   * @returns {T1 & T2}
   */
  function mergeObjects(obj1, obj2) {
    for (const key in obj2) {
      if (obj2.hasOwnProperty(key)) {
        // @ts-ignore tsc doesn't seem to properly handle types merging
        obj1[key] = obj2[key];
      }
    }
    // @ts-ignore tsc doesn't seem to properly handle types merging
    return obj1;
  }

  /**
   * @param {string} jString
   * @returns {any|null}
   */
  function parseJSON(jString) {
    try {
      return JSON.parse(jString);
    } catch (error) {
      logError(error);
      return null;
    }
  }

  /**
   * @returns {boolean}
   */
  function canAccessLocalStorage() {
    const test = 'htmx:localStorageTest';
    try {
      localStorage.setItem(test, test);
      localStorage.removeItem(test);
      return true;
    } catch (e) {
      return false;
    }
  }

  /**
   * @param {string} path
   * @returns {string}
   */
  function normalizePath(path) {
    try {
      const url = new URL(path);
      if (url) {
        path = url.pathname + url.search;
      }
      // remove trailing slash, unless index page
      if (!/^\/$/.test(path)) {
        path = path.replace(/\/+$/, '');
      }
      return path;
    } catch (e) {
      // be kind to IE11, which doesn't support URL()
      return path;
    }
  }

  //= =========================================================================================
  // public API
  //= =========================================================================================

  /**
   * @param {string} str
   * @returns {any}
   */
  function internalEval(str) {
    return maybeEval(getDocument().body, function () {
      return eval(str);
    });
  }

  /**
   * Adds a callback for the **htmx:load** event. This can be used to process new content, for example initializing the content with a javascript library
   *
   * @see https://htmx.org/api/#onLoad
   *
   * @param {(elt: Node) => void} callback the callback to call on newly loaded content
   * @returns {EventListener}
   */
  function onLoadHelper(callback) {
    const value = htmx.on('htmx:load', /** @param {CustomEvent} evt */function (evt) {
      callback(evt.detail.elt);
    });
    return value;
  }

  /**
   * Log all htmx events, useful for debugging.
   *
   * @see https://htmx.org/api/#logAll
   */
  function logAll() {
    htmx.logger = function (elt, event, data) {
      if (console) {
        console.log(event, elt, data);
      }
    };
  }
  function logNone() {
    htmx.logger = null;
  }

  /**
   * Finds an element matching the selector
   *
   * @see https://htmx.org/api/#find
   *
   * @param {ParentNode|string} eltOrSelector  the root element to find the matching element in, inclusive | the selector to match
   * @param {string} [selector] the selector to match
   * @returns {Element|null}
   */
  function find(eltOrSelector, selector) {
    if (typeof eltOrSelector !== 'string') {
      return eltOrSelector.querySelector(selector);
    } else {
      return find(getDocument(), eltOrSelector);
    }
  }

  /**
   * Finds all elements matching the selector
   *
   * @see https://htmx.org/api/#findAll
   *
   * @param {ParentNode|string} eltOrSelector the root element to find the matching elements in, inclusive | the selector to match
   * @param {string} [selector] the selector to match
   * @returns {NodeListOf<Element>}
   */
  function findAll(eltOrSelector, selector) {
    if (typeof eltOrSelector !== 'string') {
      return eltOrSelector.querySelectorAll(selector);
    } else {
      return findAll(getDocument(), eltOrSelector);
    }
  }

  /**
   * @returns Window
   */
  function getWindow() {
    return window;
  }

  /**
   * Removes an element from the DOM
   *
   * @see https://htmx.org/api/#remove
   *
   * @param {Node} elt
   * @param {number} [delay]
   */
  function removeElement(elt, delay) {
    elt = resolveTarget(elt);
    if (delay) {
      getWindow().setTimeout(function () {
        removeElement(elt);
        elt = null;
      }, delay);
    } else {
      parentElt(elt).removeChild(elt);
    }
  }

  /**
   * @param {any} elt
   * @return {Element|null}
   */
  function asElement(elt) {
    return elt instanceof Element ? elt : null;
  }

  /**
   * @param {any} elt
   * @return {HTMLElement|null}
   */
  function asHtmlElement(elt) {
    return elt instanceof HTMLElement ? elt : null;
  }

  /**
   * @param {any} value
   * @return {string|null}
   */
  function asString(value) {
    return typeof value === 'string' ? value : null;
  }

  /**
   * @param {EventTarget} elt
   * @return {ParentNode|null}
   */
  function asParentNode(elt) {
    return elt instanceof Element || elt instanceof Document || elt instanceof DocumentFragment ? elt : null;
  }

  /**
   * This method adds a class to the given element.
   *
   * @see https://htmx.org/api/#addClass
   *
   * @param {Element|string} elt the element to add the class to
   * @param {string} clazz the class to add
   * @param {number} [delay] the delay (in milliseconds) before class is added
   */
  function addClassToElement(elt, clazz, delay) {
    elt = asElement(resolveTarget(elt));
    if (!elt) {
      return;
    }
    if (delay) {
      getWindow().setTimeout(function () {
        addClassToElement(elt, clazz);
        elt = null;
      }, delay);
    } else {
      elt.classList && elt.classList.add(clazz);
    }
  }

  /**
   * Removes a class from the given element
   *
   * @see https://htmx.org/api/#removeClass
   *
   * @param {Node|string} node element to remove the class from
   * @param {string} clazz the class to remove
   * @param {number} [delay] the delay (in milliseconds before class is removed)
   */
  function removeClassFromElement(node, clazz, delay) {
    let elt = asElement(resolveTarget(node));
    if (!elt) {
      return;
    }
    if (delay) {
      getWindow().setTimeout(function () {
        removeClassFromElement(elt, clazz);
        elt = null;
      }, delay);
    } else {
      if (elt.classList) {
        elt.classList.remove(clazz);
        // if there are no classes left, remove the class attribute
        if (elt.classList.length === 0) {
          elt.removeAttribute('class');
        }
      }
    }
  }

  /**
   * Toggles the given class on an element
   *
   * @see https://htmx.org/api/#toggleClass
   *
   * @param {Element|string} elt the element to toggle the class on
   * @param {string} clazz the class to toggle
   */
  function toggleClassOnElement(elt, clazz) {
    elt = resolveTarget(elt);
    elt.classList.toggle(clazz);
  }

  /**
   * Takes the given class from its siblings, so that among its siblings, only the given element will have the class.
   *
   * @see https://htmx.org/api/#takeClass
   *
   * @param {Node|string} elt the element that will take the class
   * @param {string} clazz the class to take
   */
  function takeClassForElement(elt, clazz) {
    elt = resolveTarget(elt);
    forEach(elt.parentElement.children, function (child) {
      removeClassFromElement(child, clazz);
    });
    addClassToElement(asElement(elt), clazz);
  }

  /**
   * Finds the closest matching element in the given elements parentage, inclusive of the element
   *
   * @see https://htmx.org/api/#closest
   *
   * @param {Element|string} elt the element to find the selector from
   * @param {string} selector the selector to find
   * @returns {Element|null}
   */
  function closest(elt, selector) {
    elt = asElement(resolveTarget(elt));
    if (elt && elt.closest) {
      return elt.closest(selector);
    } else {
      // TODO remove when IE goes away
      do {
        if (elt == null || matches(elt, selector)) {
          return elt;
        }
      } while (elt = elt && asElement(parentElt(elt)));
      return null;
    }
  }

  /**
   * @param {string} str
   * @param {string} prefix
   * @returns {boolean}
   */
  function startsWith(str, prefix) {
    return str.substring(0, prefix.length) === prefix;
  }

  /**
   * @param {string} str
   * @param {string} suffix
   * @returns {boolean}
   */
  function endsWith(str, suffix) {
    return str.substring(str.length - suffix.length) === suffix;
  }

  /**
   * @param {string} selector
   * @returns {string}
   */
  function normalizeSelector(selector) {
    const trimmedSelector = selector.trim();
    if (startsWith(trimmedSelector, '<') && endsWith(trimmedSelector, '/>')) {
      return trimmedSelector.substring(1, trimmedSelector.length - 2);
    } else {
      return trimmedSelector;
    }
  }

  /**
   * @param {Node|Element|Document|string} elt
   * @param {string} selector
   * @param {boolean=} global
   * @returns {(Node|Window)[]}
   */
  function querySelectorAllExt(elt, selector, global) {
    if (selector.indexOf('global ') === 0) {
      return querySelectorAllExt(elt, selector.slice(7), true);
    }
    elt = resolveTarget(elt);
    const parts = [];
    {
      let chevronsCount = 0;
      let offset = 0;
      for (let i = 0; i < selector.length; i++) {
        const char = selector[i];
        if (char === ',' && chevronsCount === 0) {
          parts.push(selector.substring(offset, i));
          offset = i + 1;
          continue;
        }
        if (char === '<') {
          chevronsCount++;
        } else if (char === '/' && i < selector.length - 1 && selector[i + 1] === '>') {
          chevronsCount--;
        }
      }
      if (offset < selector.length) {
        parts.push(selector.substring(offset));
      }
    }
    const result = [];
    const unprocessedParts = [];
    while (parts.length > 0) {
      const selector = normalizeSelector(parts.shift());
      let item;
      if (selector.indexOf('closest ') === 0) {
        item = closest(asElement(elt), normalizeSelector(selector.substr(8)));
      } else if (selector.indexOf('find ') === 0) {
        item = find(asParentNode(elt), normalizeSelector(selector.substr(5)));
      } else if (selector === 'next' || selector === 'nextElementSibling') {
        item = asElement(elt).nextElementSibling;
      } else if (selector.indexOf('next ') === 0) {
        item = scanForwardQuery(elt, normalizeSelector(selector.substr(5)), !!global);
      } else if (selector === 'previous' || selector === 'previousElementSibling') {
        item = asElement(elt).previousElementSibling;
      } else if (selector.indexOf('previous ') === 0) {
        item = scanBackwardsQuery(elt, normalizeSelector(selector.substr(9)), !!global);
      } else if (selector === 'document') {
        item = document;
      } else if (selector === 'window') {
        item = window;
      } else if (selector === 'body') {
        item = document.body;
      } else if (selector === 'root') {
        item = getRootNode(elt, !!global);
      } else if (selector === 'host') {
        item = (/** @type ShadowRoot */elt.getRootNode()).host;
      } else {
        unprocessedParts.push(selector);
      }
      if (item) {
        result.push(item);
      }
    }
    if (unprocessedParts.length > 0) {
      const standardSelector = unprocessedParts.join(',');
      const rootNode = asParentNode(getRootNode(elt, !!global));
      result.push(...toArray(rootNode.querySelectorAll(standardSelector)));
    }
    return result;
  }

  /**
   * @param {Node} start
   * @param {string} match
   * @param {boolean} global
   * @returns {Element}
   */
  var scanForwardQuery = function (start, match, global) {
    const results = asParentNode(getRootNode(start, global)).querySelectorAll(match);
    for (let i = 0; i < results.length; i++) {
      const elt = results[i];
      if (elt.compareDocumentPosition(start) === Node.DOCUMENT_POSITION_PRECEDING) {
        return elt;
      }
    }
  };

  /**
   * @param {Node} start
   * @param {string} match
   * @param {boolean} global
   * @returns {Element}
   */
  var scanBackwardsQuery = function (start, match, global) {
    const results = asParentNode(getRootNode(start, global)).querySelectorAll(match);
    for (let i = results.length - 1; i >= 0; i--) {
      const elt = results[i];
      if (elt.compareDocumentPosition(start) === Node.DOCUMENT_POSITION_FOLLOWING) {
        return elt;
      }
    }
  };

  /**
   * @param {Node|string} eltOrSelector
   * @param {string=} selector
   * @returns {Node|Window}
   */
  function querySelectorExt(eltOrSelector, selector) {
    if (typeof eltOrSelector !== 'string') {
      return querySelectorAllExt(eltOrSelector, selector)[0];
    } else {
      return querySelectorAllExt(getDocument().body, eltOrSelector)[0];
    }
  }

  /**
   * @template {EventTarget} T
   * @param {T|string} eltOrSelector
   * @param {T} [context]
   * @returns {Element|T|null}
   */
  function resolveTarget(eltOrSelector, context) {
    if (typeof eltOrSelector === 'string') {
      return find(asParentNode(context) || document, eltOrSelector);
    } else {
      return eltOrSelector;
    }
  }

  /**
   * @typedef {keyof HTMLElementEventMap|string} AnyEventName
   */

  /**
   * @typedef {Object} EventArgs
   * @property {EventTarget} target
   * @property {AnyEventName} event
   * @property {EventListener} listener
   * @property {Object|boolean} options
   */

  /**
   * @param {EventTarget|AnyEventName} arg1
   * @param {AnyEventName|EventListener} arg2
   * @param {EventListener|Object|boolean} [arg3]
   * @param {Object|boolean} [arg4]
   * @returns {EventArgs}
   */
  function processEventArgs(arg1, arg2, arg3, arg4) {
    if (isFunction(arg2)) {
      return {
        target: getDocument().body,
        event: asString(arg1),
        listener: arg2,
        options: arg3
      };
    } else {
      return {
        target: resolveTarget(arg1),
        event: asString(arg2),
        listener: arg3,
        options: arg4
      };
    }
  }

  /**
   * Adds an event listener to an element
   *
   * @see https://htmx.org/api/#on
   *
   * @param {EventTarget|string} arg1 the element to add the listener to | the event name to add the listener for
   * @param {string|EventListener} arg2 the event name to add the listener for | the listener to add
   * @param {EventListener|Object|boolean} [arg3] the listener to add | options to add
   * @param {Object|boolean} [arg4] options to add
   * @returns {EventListener}
   */
  function addEventListenerImpl(arg1, arg2, arg3, arg4) {
    ready(function () {
      const eventArgs = processEventArgs(arg1, arg2, arg3, arg4);
      eventArgs.target.addEventListener(eventArgs.event, eventArgs.listener, eventArgs.options);
    });
    const b = isFunction(arg2);
    return b ? arg2 : arg3;
  }

  /**
   * Removes an event listener from an element
   *
   * @see https://htmx.org/api/#off
   *
   * @param {EventTarget|string} arg1 the element to remove the listener from | the event name to remove the listener from
   * @param {string|EventListener} arg2 the event name to remove the listener from | the listener to remove
   * @param {EventListener} [arg3] the listener to remove
   * @returns {EventListener}
   */
  function removeEventListenerImpl(arg1, arg2, arg3) {
    ready(function () {
      const eventArgs = processEventArgs(arg1, arg2, arg3);
      eventArgs.target.removeEventListener(eventArgs.event, eventArgs.listener);
    });
    return isFunction(arg2) ? arg2 : arg3;
  }

  //= ===================================================================
  // Node processing
  //= ===================================================================

  const DUMMY_ELT = getDocument().createElement('output'); // dummy element for bad selectors
  /**
   * @param {Element} elt
   * @param {string} attrName
   * @returns {(Node|Window)[]}
   */
  function findAttributeTargets(elt, attrName) {
    const attrTarget = getClosestAttributeValue(elt, attrName);
    if (attrTarget) {
      if (attrTarget === 'this') {
        return [findThisElement(elt, attrName)];
      } else {
        const result = querySelectorAllExt(elt, attrTarget);
        if (result.length === 0) {
          logError('The selector "' + attrTarget + '" on ' + attrName + ' returned no matches!');
          return [DUMMY_ELT];
        } else {
          return result;
        }
      }
    }
  }

  /**
   * @param {Element} elt
   * @param {string} attribute
   * @returns {Element|null}
   */
  function findThisElement(elt, attribute) {
    return asElement(getClosestMatch(elt, function (elt) {
      return getAttributeValue(asElement(elt), attribute) != null;
    }));
  }

  /**
   * @param {Element} elt
   * @returns {Node|Window|null}
   */
  function getTarget(elt) {
    const targetStr = getClosestAttributeValue(elt, 'hx-target');
    if (targetStr) {
      if (targetStr === 'this') {
        return findThisElement(elt, 'hx-target');
      } else {
        return querySelectorExt(elt, targetStr);
      }
    } else {
      const data = getInternalData(elt);
      if (data.boosted) {
        return getDocument().body;
      } else {
        return elt;
      }
    }
  }

  /**
   * @param {string} name
   * @returns {boolean}
   */
  function shouldSettleAttribute(name) {
    const attributesToSettle = htmx.config.attributesToSettle;
    for (let i = 0; i < attributesToSettle.length; i++) {
      if (name === attributesToSettle[i]) {
        return true;
      }
    }
    return false;
  }

  /**
   * @param {Element} mergeTo
   * @param {Element} mergeFrom
   */
  function cloneAttributes(mergeTo, mergeFrom) {
    forEach(mergeTo.attributes, function (attr) {
      if (!mergeFrom.hasAttribute(attr.name) && shouldSettleAttribute(attr.name)) {
        mergeTo.removeAttribute(attr.name);
      }
    });
    forEach(mergeFrom.attributes, function (attr) {
      if (shouldSettleAttribute(attr.name)) {
        mergeTo.setAttribute(attr.name, attr.value);
      }
    });
  }

  /**
   * @param {HtmxSwapStyle} swapStyle
   * @param {Element} target
   * @returns {boolean}
   */
  function isInlineSwap(swapStyle, target) {
    const extensions = getExtensions(target);
    for (let i = 0; i < extensions.length; i++) {
      const extension = extensions[i];
      try {
        if (extension.isInlineSwap(swapStyle)) {
          return true;
        }
      } catch (e) {
        logError(e);
      }
    }
    return swapStyle === 'outerHTML';
  }

  /**
   * @param {string} oobValue
   * @param {Element} oobElement
   * @param {HtmxSettleInfo} settleInfo
   * @param {Node|Document} [rootNode]
   * @returns
   */
  function oobSwap(oobValue, oobElement, settleInfo, rootNode) {
    rootNode = rootNode || getDocument();
    let selector = '#' + getRawAttribute(oobElement, 'id');
    /** @type HtmxSwapStyle */
    let swapStyle = 'outerHTML';
    if (oobValue === 'true') {
      // do nothing
    } else if (oobValue.indexOf(':') > 0) {
      swapStyle = oobValue.substring(0, oobValue.indexOf(':'));
      selector = oobValue.substring(oobValue.indexOf(':') + 1);
    } else {
      swapStyle = oobValue;
    }
    oobElement.removeAttribute('hx-swap-oob');
    oobElement.removeAttribute('data-hx-swap-oob');
    const targets = querySelectorAllExt(rootNode, selector, false);
    if (targets) {
      forEach(targets, function (target) {
        let fragment;
        const oobElementClone = oobElement.cloneNode(true);
        fragment = getDocument().createDocumentFragment();
        fragment.appendChild(oobElementClone);
        if (!isInlineSwap(swapStyle, target)) {
          fragment = asParentNode(oobElementClone); // if this is not an inline swap, we use the content of the node, not the node itself
        }
        const beforeSwapDetails = {
          shouldSwap: true,
          target,
          fragment
        };
        if (!triggerEvent(target, 'htmx:oobBeforeSwap', beforeSwapDetails)) return;
        target = beforeSwapDetails.target; // allow re-targeting
        if (beforeSwapDetails.shouldSwap) {
          handlePreservedElements(fragment);
          swapWithStyle(swapStyle, target, target, fragment, settleInfo);
          restorePreservedElements();
        }
        forEach(settleInfo.elts, function (elt) {
          triggerEvent(elt, 'htmx:oobAfterSwap', beforeSwapDetails);
        });
      });
      oobElement.parentNode.removeChild(oobElement);
    } else {
      oobElement.parentNode.removeChild(oobElement);
      triggerErrorEvent(getDocument().body, 'htmx:oobErrorNoTarget', {
        content: oobElement
      });
    }
    return oobValue;
  }
  function restorePreservedElements() {
    const pantry = find('#--htmx-preserve-pantry--');
    if (pantry) {
      for (const preservedElt of [...pantry.children]) {
        const existingElement = find('#' + preservedElt.id);
        // @ts-ignore - use proposed moveBefore feature
        existingElement.parentNode.moveBefore(preservedElt, existingElement);
        existingElement.remove();
      }
      pantry.remove();
    }
  }

  /**
   * @param {DocumentFragment|ParentNode} fragment
   */
  function handlePreservedElements(fragment) {
    forEach(findAll(fragment, '[hx-preserve], [data-hx-preserve]'), function (preservedElt) {
      const id = getAttributeValue(preservedElt, 'id');
      const existingElement = getDocument().getElementById(id);
      if (existingElement != null) {
        if (preservedElt.moveBefore) {
          // if the moveBefore API exists, use it
          // get or create a storage spot for stuff
          let pantry = find('#--htmx-preserve-pantry--');
          if (pantry == null) {
            getDocument().body.insertAdjacentHTML('afterend', "<div id='--htmx-preserve-pantry--'></div>");
            pantry = find('#--htmx-preserve-pantry--');
          }
          // @ts-ignore - use proposed moveBefore feature
          pantry.moveBefore(existingElement, null);
        } else {
          preservedElt.parentNode.replaceChild(existingElement, preservedElt);
        }
      }
    });
  }

  /**
   * @param {Node} parentNode
   * @param {ParentNode} fragment
   * @param {HtmxSettleInfo} settleInfo
   */
  function handleAttributes(parentNode, fragment, settleInfo) {
    forEach(fragment.querySelectorAll('[id]'), function (newNode) {
      const id = getRawAttribute(newNode, 'id');
      if (id && id.length > 0) {
        const normalizedId = id.replace("'", "\\'");
        const normalizedTag = newNode.tagName.replace(':', '\\:');
        const parentElt = asParentNode(parentNode);
        const oldNode = parentElt && parentElt.querySelector(normalizedTag + "[id='" + normalizedId + "']");
        if (oldNode && oldNode !== parentElt) {
          const newAttributes = newNode.cloneNode();
          cloneAttributes(newNode, oldNode);
          settleInfo.tasks.push(function () {
            cloneAttributes(newNode, newAttributes);
          });
        }
      }
    });
  }

  /**
   * @param {Node} child
   * @returns {HtmxSettleTask}
   */
  function makeAjaxLoadTask(child) {
    return function () {
      removeClassFromElement(child, htmx.config.addedClass);
      processNode(asElement(child));
      processFocus(asParentNode(child));
      triggerEvent(child, 'htmx:load');
    };
  }

  /**
   * @param {ParentNode} child
   */
  function processFocus(child) {
    const autofocus = '[autofocus]';
    const autoFocusedElt = asHtmlElement(matches(child, autofocus) ? child : child.querySelector(autofocus));
    if (autoFocusedElt != null) {
      autoFocusedElt.focus();
    }
  }

  /**
   * @param {Node} parentNode
   * @param {Node} insertBefore
   * @param {ParentNode} fragment
   * @param {HtmxSettleInfo} settleInfo
   */
  function insertNodesBefore(parentNode, insertBefore, fragment, settleInfo) {
    handleAttributes(parentNode, fragment, settleInfo);
    while (fragment.childNodes.length > 0) {
      const child = fragment.firstChild;
      addClassToElement(asElement(child), htmx.config.addedClass);
      parentNode.insertBefore(child, insertBefore);
      if (child.nodeType !== Node.TEXT_NODE && child.nodeType !== Node.COMMENT_NODE) {
        settleInfo.tasks.push(makeAjaxLoadTask(child));
      }
    }
  }

  /**
   * based on https://gist.github.com/hyamamoto/fd435505d29ebfa3d9716fd2be8d42f0,
   * derived from Java's string hashcode implementation
   * @param {string} string
   * @param {number} hash
   * @returns {number}
   */
  function stringHash(string, hash) {
    let char = 0;
    while (char < string.length) {
      hash = (hash << 5) - hash + string.charCodeAt(char++) | 0; // bitwise or ensures we have a 32-bit int
    }
    return hash;
  }

  /**
   * @param {Element} elt
   * @returns {number}
   */
  function attributeHash(elt) {
    let hash = 0;
    // IE fix
    if (elt.attributes) {
      for (let i = 0; i < elt.attributes.length; i++) {
        const attribute = elt.attributes[i];
        if (attribute.value) {
          // only include attributes w/ actual values (empty is same as non-existent)
          hash = stringHash(attribute.name, hash);
          hash = stringHash(attribute.value, hash);
        }
      }
    }
    return hash;
  }

  /**
   * @param {EventTarget} elt
   */
  function deInitOnHandlers(elt) {
    const internalData = getInternalData(elt);
    if (internalData.onHandlers) {
      for (let i = 0; i < internalData.onHandlers.length; i++) {
        const handlerInfo = internalData.onHandlers[i];
        removeEventListenerImpl(elt, handlerInfo.event, handlerInfo.listener);
      }
      delete internalData.onHandlers;
    }
  }

  /**
   * @param {Node} element
   */
  function deInitNode(element) {
    const internalData = getInternalData(element);
    if (internalData.timeout) {
      clearTimeout(internalData.timeout);
    }
    if (internalData.listenerInfos) {
      forEach(internalData.listenerInfos, function (info) {
        if (info.on) {
          removeEventListenerImpl(info.on, info.trigger, info.listener);
        }
      });
    }
    deInitOnHandlers(element);
    forEach(Object.keys(internalData), function (key) {
      if (key !== 'firstInitCompleted') delete internalData[key];
    });
  }

  /**
   * @param {Node} element
   */
  function cleanUpElement(element) {
    triggerEvent(element, 'htmx:beforeCleanupElement');
    deInitNode(element);
    // @ts-ignore IE11 code
    // noinspection JSUnresolvedReference
    if (element.children) {
      // IE
      // @ts-ignore
      forEach(element.children, function (child) {
        cleanUpElement(child);
      });
    }
  }

  /**
   * @param {Node} target
   * @param {ParentNode} fragment
   * @param {HtmxSettleInfo} settleInfo
   */
  function swapOuterHTML(target, fragment, settleInfo) {
    if (target instanceof Element && target.tagName === 'BODY') {
      // special case the body to innerHTML because DocumentFragments can't contain a body elt unfortunately
      return swapInnerHTML(target, fragment, settleInfo);
    }
    /** @type {Node} */
    let newElt;
    const eltBeforeNewContent = target.previousSibling;
    const parentNode = parentElt(target);
    if (!parentNode) {
      // when parent node disappears, we can't do anything
      return;
    }
    insertNodesBefore(parentNode, target, fragment, settleInfo);
    if (eltBeforeNewContent == null) {
      newElt = parentNode.firstChild;
    } else {
      newElt = eltBeforeNewContent.nextSibling;
    }
    settleInfo.elts = settleInfo.elts.filter(function (e) {
      return e !== target;
    });
    // scan through all newly added content and add all elements to the settle info so we trigger
    // events properly on them
    while (newElt && newElt !== target) {
      if (newElt instanceof Element) {
        settleInfo.elts.push(newElt);
      }
      newElt = newElt.nextSibling;
    }
    cleanUpElement(target);
    if (target instanceof Element) {
      target.remove();
    } else {
      target.parentNode.removeChild(target);
    }
  }

  /**
   * @param {Node} target
   * @param {ParentNode} fragment
   * @param {HtmxSettleInfo} settleInfo
   */
  function swapAfterBegin(target, fragment, settleInfo) {
    return insertNodesBefore(target, target.firstChild, fragment, settleInfo);
  }

  /**
   * @param {Node} target
   * @param {ParentNode} fragment
   * @param {HtmxSettleInfo} settleInfo
   */
  function swapBeforeBegin(target, fragment, settleInfo) {
    return insertNodesBefore(parentElt(target), target, fragment, settleInfo);
  }

  /**
   * @param {Node} target
   * @param {ParentNode} fragment
   * @param {HtmxSettleInfo} settleInfo
   */
  function swapBeforeEnd(target, fragment, settleInfo) {
    return insertNodesBefore(target, null, fragment, settleInfo);
  }

  /**
   * @param {Node} target
   * @param {ParentNode} fragment
   * @param {HtmxSettleInfo} settleInfo
   */
  function swapAfterEnd(target, fragment, settleInfo) {
    return insertNodesBefore(parentElt(target), target.nextSibling, fragment, settleInfo);
  }

  /**
   * @param {Node} target
   */
  function swapDelete(target) {
    cleanUpElement(target);
    const parent = parentElt(target);
    if (parent) {
      return parent.removeChild(target);
    }
  }

  /**
   * @param {Node} target
   * @param {ParentNode} fragment
   * @param {HtmxSettleInfo} settleInfo
   */
  function swapInnerHTML(target, fragment, settleInfo) {
    const firstChild = target.firstChild;
    insertNodesBefore(target, firstChild, fragment, settleInfo);
    if (firstChild) {
      while (firstChild.nextSibling) {
        cleanUpElement(firstChild.nextSibling);
        target.removeChild(firstChild.nextSibling);
      }
      cleanUpElement(firstChild);
      target.removeChild(firstChild);
    }
  }

  /**
   * @param {HtmxSwapStyle} swapStyle
   * @param {Element} elt
   * @param {Node} target
   * @param {ParentNode} fragment
   * @param {HtmxSettleInfo} settleInfo
   */
  function swapWithStyle(swapStyle, elt, target, fragment, settleInfo) {
    switch (swapStyle) {
      case 'none':
        return;
      case 'outerHTML':
        swapOuterHTML(target, fragment, settleInfo);
        return;
      case 'afterbegin':
        swapAfterBegin(target, fragment, settleInfo);
        return;
      case 'beforebegin':
        swapBeforeBegin(target, fragment, settleInfo);
        return;
      case 'beforeend':
        swapBeforeEnd(target, fragment, settleInfo);
        return;
      case 'afterend':
        swapAfterEnd(target, fragment, settleInfo);
        return;
      case 'delete':
        swapDelete(target);
        return;
      default:
        var extensions = getExtensions(elt);
        for (let i = 0; i < extensions.length; i++) {
          const ext = extensions[i];
          try {
            const newElements = ext.handleSwap(swapStyle, target, fragment, settleInfo);
            if (newElements) {
              if (Array.isArray(newElements)) {
                // if handleSwap returns an array (like) of elements, we handle them
                for (let j = 0; j < newElements.length; j++) {
                  const child = newElements[j];
                  if (child.nodeType !== Node.TEXT_NODE && child.nodeType !== Node.COMMENT_NODE) {
                    settleInfo.tasks.push(makeAjaxLoadTask(child));
                  }
                }
              }
              return;
            }
          } catch (e) {
            logError(e);
          }
        }
        if (swapStyle === 'innerHTML') {
          swapInnerHTML(target, fragment, settleInfo);
        } else {
          swapWithStyle(htmx.config.defaultSwapStyle, elt, target, fragment, settleInfo);
        }
    }
  }

  /**
   * @param {DocumentFragment} fragment
   * @param {HtmxSettleInfo} settleInfo
   * @param {Node|Document} [rootNode]
   */
  function findAndSwapOobElements(fragment, settleInfo, rootNode) {
    var oobElts = findAll(fragment, '[hx-swap-oob], [data-hx-swap-oob]');
    forEach(oobElts, function (oobElement) {
      if (htmx.config.allowNestedOobSwaps || oobElement.parentElement === null) {
        const oobValue = getAttributeValue(oobElement, 'hx-swap-oob');
        if (oobValue != null) {
          oobSwap(oobValue, oobElement, settleInfo, rootNode);
        }
      } else {
        oobElement.removeAttribute('hx-swap-oob');
        oobElement.removeAttribute('data-hx-swap-oob');
      }
    });
    return oobElts.length > 0;
  }

  /**
   * Implements complete swapping pipeline, including: focus and selection preservation,
   * title updates, scroll, OOB swapping, normal swapping and settling
   * @param {string|Element} target
   * @param {string} content
   * @param {HtmxSwapSpecification} swapSpec
   * @param {SwapOptions} [swapOptions]
   */
  function swap(target, content, swapSpec, swapOptions) {
    if (!swapOptions) {
      swapOptions = {};
    }
    target = resolveTarget(target);
    const rootNode = swapOptions.contextElement ? getRootNode(swapOptions.contextElement, false) : getDocument();

    // preserve focus and selection
    const activeElt = document.activeElement;
    let selectionInfo = {};
    try {
      selectionInfo = {
        elt: activeElt,
        // @ts-ignore
        start: activeElt ? activeElt.selectionStart : null,
        // @ts-ignore
        end: activeElt ? activeElt.selectionEnd : null
      };
    } catch (e) {
      // safari issue - see https://github.com/microsoft/playwright/issues/5894
    }
    const settleInfo = makeSettleInfo(target);

    // For text content swaps, don't parse the response as HTML, just insert it
    if (swapSpec.swapStyle === 'textContent') {
      target.textContent = content;
      // Otherwise, make the fragment and process it
    } else {
      let fragment = makeFragment(content);
      settleInfo.title = fragment.title;

      // select-oob swaps
      if (swapOptions.selectOOB) {
        const oobSelectValues = swapOptions.selectOOB.split(',');
        for (let i = 0; i < oobSelectValues.length; i++) {
          const oobSelectValue = oobSelectValues[i].split(':', 2);
          let id = oobSelectValue[0].trim();
          if (id.indexOf('#') === 0) {
            id = id.substring(1);
          }
          const oobValue = oobSelectValue[1] || 'true';
          const oobElement = fragment.querySelector('#' + id);
          if (oobElement) {
            oobSwap(oobValue, oobElement, settleInfo, rootNode);
          }
        }
      }
      // oob swaps
      findAndSwapOobElements(fragment, settleInfo, rootNode);
      forEach(findAll(fragment, 'template'), /** @param {HTMLTemplateElement} template */function (template) {
        if (template.content && findAndSwapOobElements(template.content, settleInfo, rootNode)) {
          // Avoid polluting the DOM with empty templates that were only used to encapsulate oob swap
          template.remove();
        }
      });

      // normal swap
      if (swapOptions.select) {
        const newFragment = getDocument().createDocumentFragment();
        forEach(fragment.querySelectorAll(swapOptions.select), function (node) {
          newFragment.appendChild(node);
        });
        fragment = newFragment;
      }
      handlePreservedElements(fragment);
      swapWithStyle(swapSpec.swapStyle, swapOptions.contextElement, target, fragment, settleInfo);
      restorePreservedElements();
    }

    // apply saved focus and selection information to swapped content
    if (selectionInfo.elt && !bodyContains(selectionInfo.elt) && getRawAttribute(selectionInfo.elt, 'id')) {
      const newActiveElt = document.getElementById(getRawAttribute(selectionInfo.elt, 'id'));
      const focusOptions = {
        preventScroll: swapSpec.focusScroll !== undefined ? !swapSpec.focusScroll : !htmx.config.defaultFocusScroll
      };
      if (newActiveElt) {
        // @ts-ignore
        if (selectionInfo.start && newActiveElt.setSelectionRange) {
          try {
            // @ts-ignore
            newActiveElt.setSelectionRange(selectionInfo.start, selectionInfo.end);
          } catch (e) {
            // the setSelectionRange method is present on fields that don't support it, so just let this fail
          }
        }
        newActiveElt.focus(focusOptions);
      }
    }
    target.classList.remove(htmx.config.swappingClass);
    forEach(settleInfo.elts, function (elt) {
      if (elt.classList) {
        elt.classList.add(htmx.config.settlingClass);
      }
      triggerEvent(elt, 'htmx:afterSwap', swapOptions.eventInfo);
    });
    if (swapOptions.afterSwapCallback) {
      swapOptions.afterSwapCallback();
    }

    // merge in new title after swap but before settle
    if (!swapSpec.ignoreTitle) {
      handleTitle(settleInfo.title);
    }

    // settle
    const doSettle = function () {
      forEach(settleInfo.tasks, function (task) {
        task.call();
      });
      forEach(settleInfo.elts, function (elt) {
        if (elt.classList) {
          elt.classList.remove(htmx.config.settlingClass);
        }
        triggerEvent(elt, 'htmx:afterSettle', swapOptions.eventInfo);
      });
      if (swapOptions.anchor) {
        const anchorTarget = asElement(resolveTarget('#' + swapOptions.anchor));
        if (anchorTarget) {
          anchorTarget.scrollIntoView({
            block: 'start',
            behavior: 'auto'
          });
        }
      }
      updateScrollState(settleInfo.elts, swapSpec);
      if (swapOptions.afterSettleCallback) {
        swapOptions.afterSettleCallback();
      }
    };
    if (swapSpec.settleDelay > 0) {
      getWindow().setTimeout(doSettle, swapSpec.settleDelay);
    } else {
      doSettle();
    }
  }

  /**
   * @param {XMLHttpRequest} xhr
   * @param {string} header
   * @param {EventTarget} elt
   */
  function handleTriggerHeader(xhr, header, elt) {
    const triggerBody = xhr.getResponseHeader(header);
    if (triggerBody.indexOf('{') === 0) {
      const triggers = parseJSON(triggerBody);
      for (const eventName in triggers) {
        if (triggers.hasOwnProperty(eventName)) {
          let detail = triggers[eventName];
          if (isRawObject(detail)) {
            // @ts-ignore
            elt = detail.target !== undefined ? detail.target : elt;
          } else {
            detail = {
              value: detail
            };
          }
          triggerEvent(elt, eventName, detail);
        }
      }
    } else {
      const eventNames = triggerBody.split(',');
      for (let i = 0; i < eventNames.length; i++) {
        triggerEvent(elt, eventNames[i].trim(), []);
      }
    }
  }
  const WHITESPACE = /\s/;
  const WHITESPACE_OR_COMMA = /[\s,]/;
  const SYMBOL_START = /[_$a-zA-Z]/;
  const SYMBOL_CONT = /[_$a-zA-Z0-9]/;
  const STRINGISH_START = ['"', "'", '/'];
  const NOT_WHITESPACE = /[^\s]/;
  const COMBINED_SELECTOR_START = /[{(]/;
  const COMBINED_SELECTOR_END = /[})]/;

  /**
   * @param {string} str
   * @returns {string[]}
   */
  function tokenizeString(str) {
    /** @type string[] */
    const tokens = [];
    let position = 0;
    while (position < str.length) {
      if (SYMBOL_START.exec(str.charAt(position))) {
        var startPosition = position;
        while (SYMBOL_CONT.exec(str.charAt(position + 1))) {
          position++;
        }
        tokens.push(str.substring(startPosition, position + 1));
      } else if (STRINGISH_START.indexOf(str.charAt(position)) !== -1) {
        const startChar = str.charAt(position);
        var startPosition = position;
        position++;
        while (position < str.length && str.charAt(position) !== startChar) {
          if (str.charAt(position) === '\\') {
            position++;
          }
          position++;
        }
        tokens.push(str.substring(startPosition, position + 1));
      } else {
        const symbol = str.charAt(position);
        tokens.push(symbol);
      }
      position++;
    }
    return tokens;
  }

  /**
   * @param {string} token
   * @param {string|null} last
   * @param {string} paramName
   * @returns {boolean}
   */
  function isPossibleRelativeReference(token, last, paramName) {
    return SYMBOL_START.exec(token.charAt(0)) && token !== 'true' && token !== 'false' && token !== 'this' && token !== paramName && last !== '.';
  }

  /**
   * @param {EventTarget|string} elt
   * @param {string[]} tokens
   * @param {string} paramName
   * @returns {ConditionalFunction|null}
   */
  function maybeGenerateConditional(elt, tokens, paramName) {
    if (tokens[0] === '[') {
      tokens.shift();
      let bracketCount = 1;
      let conditionalSource = ' return (function(' + paramName + '){ return (';
      let last = null;
      while (tokens.length > 0) {
        const token = tokens[0];
        // @ts-ignore For some reason tsc doesn't understand the shift call, and thinks we're comparing the same value here, i.e. '[' vs ']'
        if (token === ']') {
          bracketCount--;
          if (bracketCount === 0) {
            if (last === null) {
              conditionalSource = conditionalSource + 'true';
            }
            tokens.shift();
            conditionalSource += ')})';
            try {
              const conditionFunction = maybeEval(elt, function () {
                return Function(conditionalSource)();
              }, function () {
                return true;
              });
              conditionFunction.source = conditionalSource;
              return conditionFunction;
            } catch (e) {
              triggerErrorEvent(getDocument().body, 'htmx:syntax:error', {
                error: e,
                source: conditionalSource
              });
              return null;
            }
          }
        } else if (token === '[') {
          bracketCount++;
        }
        if (isPossibleRelativeReference(token, last, paramName)) {
          conditionalSource += '((' + paramName + '.' + token + ') ? (' + paramName + '.' + token + ') : (window.' + token + '))';
        } else {
          conditionalSource = conditionalSource + token;
        }
        last = tokens.shift();
      }
    }
  }

  /**
   * @param {string[]} tokens
   * @param {RegExp} match
   * @returns {string}
   */
  function consumeUntil(tokens, match) {
    let result = '';
    while (tokens.length > 0 && !match.test(tokens[0])) {
      result += tokens.shift();
    }
    return result;
  }

  /**
   * @param {string[]} tokens
   * @returns {string}
   */
  function consumeCSSSelector(tokens) {
    let result;
    if (tokens.length > 0 && COMBINED_SELECTOR_START.test(tokens[0])) {
      tokens.shift();
      result = consumeUntil(tokens, COMBINED_SELECTOR_END).trim();
      tokens.shift();
    } else {
      result = consumeUntil(tokens, WHITESPACE_OR_COMMA);
    }
    return result;
  }
  const INPUT_SELECTOR = 'input, textarea, select';

  /**
   * @param {Element} elt
   * @param {string} explicitTrigger
   * @param {Object} cache for trigger specs
   * @returns {HtmxTriggerSpecification[]}
   */
  function parseAndCacheTrigger(elt, explicitTrigger, cache) {
    /** @type HtmxTriggerSpecification[] */
    const triggerSpecs = [];
    const tokens = tokenizeString(explicitTrigger);
    do {
      consumeUntil(tokens, NOT_WHITESPACE);
      const initialLength = tokens.length;
      const trigger = consumeUntil(tokens, /[,\[\s]/);
      if (trigger !== '') {
        if (trigger === 'every') {
          /** @type HtmxTriggerSpecification */
          const every = {
            trigger: 'every'
          };
          consumeUntil(tokens, NOT_WHITESPACE);
          every.pollInterval = parseInterval(consumeUntil(tokens, /[,\[\s]/));
          consumeUntil(tokens, NOT_WHITESPACE);
          var eventFilter = maybeGenerateConditional(elt, tokens, 'event');
          if (eventFilter) {
            every.eventFilter = eventFilter;
          }
          triggerSpecs.push(every);
        } else {
          /** @type HtmxTriggerSpecification */
          const triggerSpec = {
            trigger
          };
          var eventFilter = maybeGenerateConditional(elt, tokens, 'event');
          if (eventFilter) {
            triggerSpec.eventFilter = eventFilter;
          }
          consumeUntil(tokens, NOT_WHITESPACE);
          while (tokens.length > 0 && tokens[0] !== ',') {
            const token = tokens.shift();
            if (token === 'changed') {
              triggerSpec.changed = true;
            } else if (token === 'once') {
              triggerSpec.once = true;
            } else if (token === 'consume') {
              triggerSpec.consume = true;
            } else if (token === 'delay' && tokens[0] === ':') {
              tokens.shift();
              triggerSpec.delay = parseInterval(consumeUntil(tokens, WHITESPACE_OR_COMMA));
            } else if (token === 'from' && tokens[0] === ':') {
              tokens.shift();
              if (COMBINED_SELECTOR_START.test(tokens[0])) {
                var from_arg = consumeCSSSelector(tokens);
              } else {
                var from_arg = consumeUntil(tokens, WHITESPACE_OR_COMMA);
                if (from_arg === 'closest' || from_arg === 'find' || from_arg === 'next' || from_arg === 'previous') {
                  tokens.shift();
                  const selector = consumeCSSSelector(tokens);
                  // `next` and `previous` allow a selector-less syntax
                  if (selector.length > 0) {
                    from_arg += ' ' + selector;
                  }
                }
              }
              triggerSpec.from = from_arg;
            } else if (token === 'target' && tokens[0] === ':') {
              tokens.shift();
              triggerSpec.target = consumeCSSSelector(tokens);
            } else if (token === 'throttle' && tokens[0] === ':') {
              tokens.shift();
              triggerSpec.throttle = parseInterval(consumeUntil(tokens, WHITESPACE_OR_COMMA));
            } else if (token === 'queue' && tokens[0] === ':') {
              tokens.shift();
              triggerSpec.queue = consumeUntil(tokens, WHITESPACE_OR_COMMA);
            } else if (token === 'root' && tokens[0] === ':') {
              tokens.shift();
              triggerSpec[token] = consumeCSSSelector(tokens);
            } else if (token === 'threshold' && tokens[0] === ':') {
              tokens.shift();
              triggerSpec[token] = consumeUntil(tokens, WHITESPACE_OR_COMMA);
            } else {
              triggerErrorEvent(elt, 'htmx:syntax:error', {
                token: tokens.shift()
              });
            }
            consumeUntil(tokens, NOT_WHITESPACE);
          }
          triggerSpecs.push(triggerSpec);
        }
      }
      if (tokens.length === initialLength) {
        triggerErrorEvent(elt, 'htmx:syntax:error', {
          token: tokens.shift()
        });
      }
      consumeUntil(tokens, NOT_WHITESPACE);
    } while (tokens[0] === ',' && tokens.shift());
    if (cache) {
      cache[explicitTrigger] = triggerSpecs;
    }
    return triggerSpecs;
  }

  /**
   * @param {Element} elt
   * @returns {HtmxTriggerSpecification[]}
   */
  function getTriggerSpecs(elt) {
    const explicitTrigger = getAttributeValue(elt, 'hx-trigger');
    let triggerSpecs = [];
    if (explicitTrigger) {
      const cache = htmx.config.triggerSpecsCache;
      triggerSpecs = cache && cache[explicitTrigger] || parseAndCacheTrigger(elt, explicitTrigger, cache);
    }
    if (triggerSpecs.length > 0) {
      return triggerSpecs;
    } else if (matches(elt, 'form')) {
      return [{
        trigger: 'submit'
      }];
    } else if (matches(elt, 'input[type="button"], input[type="submit"]')) {
      return [{
        trigger: 'click'
      }];
    } else if (matches(elt, INPUT_SELECTOR)) {
      return [{
        trigger: 'change'
      }];
    } else {
      return [{
        trigger: 'click'
      }];
    }
  }

  /**
   * @param {Element} elt
   */
  function cancelPolling(elt) {
    getInternalData(elt).cancelled = true;
  }

  /**
   * @param {Element} elt
   * @param {TriggerHandler} handler
   * @param {HtmxTriggerSpecification} spec
   */
  function processPolling(elt, handler, spec) {
    const nodeData = getInternalData(elt);
    nodeData.timeout = getWindow().setTimeout(function () {
      if (bodyContains(elt) && nodeData.cancelled !== true) {
        if (!maybeFilterEvent(spec, elt, makeEvent('hx:poll:trigger', {
          triggerSpec: spec,
          target: elt
        }))) {
          handler(elt);
        }
        processPolling(elt, handler, spec);
      }
    }, spec.pollInterval);
  }

  /**
   * @param {HTMLAnchorElement} elt
   * @returns {boolean}
   */
  function isLocalLink(elt) {
    return location.hostname === elt.hostname && getRawAttribute(elt, 'href') && getRawAttribute(elt, 'href').indexOf('#') !== 0;
  }

  /**
   * @param {Element} elt
   */
  function eltIsDisabled(elt) {
    return closest(elt, htmx.config.disableSelector);
  }

  /**
   * @param {Element} elt
   * @param {HtmxNodeInternalData} nodeData
   * @param {HtmxTriggerSpecification[]} triggerSpecs
   */
  function boostElement(elt, nodeData, triggerSpecs) {
    if (elt instanceof HTMLAnchorElement && isLocalLink(elt) && (elt.target === '' || elt.target === '_self') || elt.tagName === 'FORM' && String(getRawAttribute(elt, 'method')).toLowerCase() !== 'dialog') {
      nodeData.boosted = true;
      let verb, path;
      if (elt.tagName === 'A') {
        verb = /** @type HttpVerb */'get';
        path = getRawAttribute(elt, 'href');
      } else {
        const rawAttribute = getRawAttribute(elt, 'method');
        verb = /** @type HttpVerb */rawAttribute ? rawAttribute.toLowerCase() : 'get';
        path = getRawAttribute(elt, 'action');
        if (path == null || path === '') {
          // if there is no action attribute on the form set path to current href before the
          // following logic to properly clear parameters on a GET (not on a POST!)
          path = getDocument().location.href;
        }
        if (verb === 'get' && path.includes('?')) {
          path = path.replace(/\?[^#]+/, '');
        }
      }
      triggerSpecs.forEach(function (triggerSpec) {
        addEventListener(elt, function (node, evt) {
          const elt = asElement(node);
          if (eltIsDisabled(elt)) {
            cleanUpElement(elt);
            return;
          }
          issueAjaxRequest(verb, path, elt, evt);
        }, nodeData, triggerSpec, true);
      });
    }
  }

  /**
   * @param {Event} evt
   * @param {Node} node
   * @returns {boolean}
   */
  function shouldCancel(evt, node) {
    const elt = asElement(node);
    if (!elt) {
      return false;
    }
    if (evt.type === 'submit' || evt.type === 'click') {
      if (elt.tagName === 'FORM') {
        return true;
      }
      if (matches(elt, 'input[type="submit"], button') && (matches(elt, '[form]') || closest(elt, 'form') !== null)) {
        return true;
      }
      if (elt instanceof HTMLAnchorElement && elt.href && (elt.getAttribute('href') === '#' || elt.getAttribute('href').indexOf('#') !== 0)) {
        return true;
      }
    }
    return false;
  }

  /**
   * @param {Node} elt
   * @param {Event|MouseEvent|KeyboardEvent|TouchEvent} evt
   * @returns {boolean}
   */
  function ignoreBoostedAnchorCtrlClick(elt, evt) {
    return getInternalData(elt).boosted && elt instanceof HTMLAnchorElement && evt.type === 'click' && (
    // @ts-ignore this will resolve to undefined for events that don't define those properties, which is fine
    evt.ctrlKey || evt.metaKey);
  }

  /**
   * @param {HtmxTriggerSpecification} triggerSpec
   * @param {Node} elt
   * @param {Event} evt
   * @returns {boolean}
   */
  function maybeFilterEvent(triggerSpec, elt, evt) {
    const eventFilter = triggerSpec.eventFilter;
    if (eventFilter) {
      try {
        return eventFilter.call(elt, evt) !== true;
      } catch (e) {
        const source = eventFilter.source;
        triggerErrorEvent(getDocument().body, 'htmx:eventFilter:error', {
          error: e,
          source
        });
        return true;
      }
    }
    return false;
  }

  /**
   * @param {Node} elt
   * @param {TriggerHandler} handler
   * @param {HtmxNodeInternalData} nodeData
   * @param {HtmxTriggerSpecification} triggerSpec
   * @param {boolean} [explicitCancel]
   */
  function addEventListener(elt, handler, nodeData, triggerSpec, explicitCancel) {
    const elementData = getInternalData(elt);
    /** @type {(Node|Window)[]} */
    let eltsToListenOn;
    if (triggerSpec.from) {
      eltsToListenOn = querySelectorAllExt(elt, triggerSpec.from);
    } else {
      eltsToListenOn = [elt];
    }
    // store the initial values of the elements, so we can tell if they change
    if (triggerSpec.changed) {
      if (!('lastValue' in elementData)) {
        elementData.lastValue = new WeakMap();
      }
      eltsToListenOn.forEach(function (eltToListenOn) {
        if (!elementData.lastValue.has(triggerSpec)) {
          elementData.lastValue.set(triggerSpec, new WeakMap());
        }
        // @ts-ignore value will be undefined for non-input elements, which is fine
        elementData.lastValue.get(triggerSpec).set(eltToListenOn, eltToListenOn.value);
      });
    }
    forEach(eltsToListenOn, function (eltToListenOn) {
      /** @type EventListener */
      const eventListener = function (evt) {
        if (!bodyContains(elt)) {
          eltToListenOn.removeEventListener(triggerSpec.trigger, eventListener);
          return;
        }
        if (ignoreBoostedAnchorCtrlClick(elt, evt)) {
          return;
        }
        if (explicitCancel || shouldCancel(evt, elt)) {
          evt.preventDefault();
        }
        if (maybeFilterEvent(triggerSpec, elt, evt)) {
          return;
        }
        const eventData = getInternalData(evt);
        eventData.triggerSpec = triggerSpec;
        if (eventData.handledFor == null) {
          eventData.handledFor = [];
        }
        if (eventData.handledFor.indexOf(elt) < 0) {
          eventData.handledFor.push(elt);
          if (triggerSpec.consume) {
            evt.stopPropagation();
          }
          if (triggerSpec.target && evt.target) {
            if (!matches(asElement(evt.target), triggerSpec.target)) {
              return;
            }
          }
          if (triggerSpec.once) {
            if (elementData.triggeredOnce) {
              return;
            } else {
              elementData.triggeredOnce = true;
            }
          }
          if (triggerSpec.changed) {
            const node = event.target;
            // @ts-ignore value will be undefined for non-input elements, which is fine
            const value = node.value;
            const lastValue = elementData.lastValue.get(triggerSpec);
            if (lastValue.has(node) && lastValue.get(node) === value) {
              return;
            }
            lastValue.set(node, value);
          }
          if (elementData.delayed) {
            clearTimeout(elementData.delayed);
          }
          if (elementData.throttle) {
            return;
          }
          if (triggerSpec.throttle > 0) {
            if (!elementData.throttle) {
              triggerEvent(elt, 'htmx:trigger');
              handler(elt, evt);
              elementData.throttle = getWindow().setTimeout(function () {
                elementData.throttle = null;
              }, triggerSpec.throttle);
            }
          } else if (triggerSpec.delay > 0) {
            elementData.delayed = getWindow().setTimeout(function () {
              triggerEvent(elt, 'htmx:trigger');
              handler(elt, evt);
            }, triggerSpec.delay);
          } else {
            triggerEvent(elt, 'htmx:trigger');
            handler(elt, evt);
          }
        }
      };
      if (nodeData.listenerInfos == null) {
        nodeData.listenerInfos = [];
      }
      nodeData.listenerInfos.push({
        trigger: triggerSpec.trigger,
        listener: eventListener,
        on: eltToListenOn
      });
      eltToListenOn.addEventListener(triggerSpec.trigger, eventListener);
    });
  }
  let windowIsScrolling = false; // used by initScrollHandler
  let scrollHandler = null;
  function initScrollHandler() {
    if (!scrollHandler) {
      scrollHandler = function () {
        windowIsScrolling = true;
      };
      window.addEventListener('scroll', scrollHandler);
      window.addEventListener('resize', scrollHandler);
      setInterval(function () {
        if (windowIsScrolling) {
          windowIsScrolling = false;
          forEach(getDocument().querySelectorAll("[hx-trigger*='revealed'],[data-hx-trigger*='revealed']"), function (elt) {
            maybeReveal(elt);
          });
        }
      }, 200);
    }
  }

  /**
   * @param {Element} elt
   */
  function maybeReveal(elt) {
    if (!hasAttribute(elt, 'data-hx-revealed') && isScrolledIntoView(elt)) {
      elt.setAttribute('data-hx-revealed', 'true');
      const nodeData = getInternalData(elt);
      if (nodeData.initHash) {
        triggerEvent(elt, 'revealed');
      } else {
        // if the node isn't initialized, wait for it before triggering the request
        elt.addEventListener('htmx:afterProcessNode', function () {
          triggerEvent(elt, 'revealed');
        }, {
          once: true
        });
      }
    }
  }

  //= ===================================================================

  /**
   * @param {Element} elt
   * @param {TriggerHandler} handler
   * @param {HtmxNodeInternalData} nodeData
   * @param {number} delay
   */
  function loadImmediately(elt, handler, nodeData, delay) {
    const load = function () {
      if (!nodeData.loaded) {
        nodeData.loaded = true;
        triggerEvent(elt, 'htmx:trigger');
        handler(elt);
      }
    };
    if (delay > 0) {
      getWindow().setTimeout(load, delay);
    } else {
      load();
    }
  }

  /**
   * @param {Element} elt
   * @param {HtmxNodeInternalData} nodeData
   * @param {HtmxTriggerSpecification[]} triggerSpecs
   * @returns {boolean}
   */
  function processVerbs(elt, nodeData, triggerSpecs) {
    let explicitAction = false;
    forEach(VERBS, function (verb) {
      if (hasAttribute(elt, 'hx-' + verb)) {
        const path = getAttributeValue(elt, 'hx-' + verb);
        explicitAction = true;
        nodeData.path = path;
        nodeData.verb = verb;
        triggerSpecs.forEach(function (triggerSpec) {
          addTriggerHandler(elt, triggerSpec, nodeData, function (node, evt) {
            const elt = asElement(node);
            if (closest(elt, htmx.config.disableSelector)) {
              cleanUpElement(elt);
              return;
            }
            issueAjaxRequest(verb, path, elt, evt);
          });
        });
      }
    });
    return explicitAction;
  }

  /**
   * @callback TriggerHandler
   * @param {Node} elt
   * @param {Event} [evt]
   */

  /**
   * @param {Node} elt
   * @param {HtmxTriggerSpecification} triggerSpec
   * @param {HtmxNodeInternalData} nodeData
   * @param {TriggerHandler} handler
   */
  function addTriggerHandler(elt, triggerSpec, nodeData, handler) {
    if (triggerSpec.trigger === 'revealed') {
      initScrollHandler();
      addEventListener(elt, handler, nodeData, triggerSpec);
      maybeReveal(asElement(elt));
    } else if (triggerSpec.trigger === 'intersect') {
      const observerOptions = {};
      if (triggerSpec.root) {
        observerOptions.root = querySelectorExt(elt, triggerSpec.root);
      }
      if (triggerSpec.threshold) {
        observerOptions.threshold = parseFloat(triggerSpec.threshold);
      }
      const observer = new IntersectionObserver(function (entries) {
        for (let i = 0; i < entries.length; i++) {
          const entry = entries[i];
          if (entry.isIntersecting) {
            triggerEvent(elt, 'intersect');
            break;
          }
        }
      }, observerOptions);
      observer.observe(asElement(elt));
      addEventListener(asElement(elt), handler, nodeData, triggerSpec);
    } else if (!nodeData.firstInitCompleted && triggerSpec.trigger === 'load') {
      if (!maybeFilterEvent(triggerSpec, elt, makeEvent('load', {
        elt
      }))) {
        loadImmediately(asElement(elt), handler, nodeData, triggerSpec.delay);
      }
    } else if (triggerSpec.pollInterval > 0) {
      nodeData.polling = true;
      processPolling(asElement(elt), handler, triggerSpec);
    } else {
      addEventListener(elt, handler, nodeData, triggerSpec);
    }
  }

  /**
   * @param {Node} node
   * @returns {boolean}
   */
  function shouldProcessHxOn(node) {
    const elt = asElement(node);
    if (!elt) {
      return false;
    }
    const attributes = elt.attributes;
    for (let j = 0; j < attributes.length; j++) {
      const attrName = attributes[j].name;
      if (startsWith(attrName, 'hx-on:') || startsWith(attrName, 'data-hx-on:') || startsWith(attrName, 'hx-on-') || startsWith(attrName, 'data-hx-on-')) {
        return true;
      }
    }
    return false;
  }

  /**
   * @param {Node} elt
   * @returns {Element[]}
   */
  const HX_ON_QUERY = new XPathEvaluator().createExpression('.//*[@*[ starts-with(name(), "hx-on:") or starts-with(name(), "data-hx-on:") or' + ' starts-with(name(), "hx-on-") or starts-with(name(), "data-hx-on-") ]]');
  function processHXOnRoot(elt, elements) {
    if (shouldProcessHxOn(elt)) {
      elements.push(asElement(elt));
    }
    const iter = HX_ON_QUERY.evaluate(elt);
    let node = null;
    while (node = iter.iterateNext()) elements.push(asElement(node));
  }
  function findHxOnWildcardElements(elt) {
    /** @type {Element[]} */
    const elements = [];
    if (elt instanceof DocumentFragment) {
      for (const child of elt.childNodes) {
        processHXOnRoot(child, elements);
      }
    } else {
      processHXOnRoot(elt, elements);
    }
    return elements;
  }

  /**
   * @param {Element} elt
   * @returns {NodeListOf<Element>|[]}
   */
  function findElementsToProcess(elt) {
    if (elt.querySelectorAll) {
      const boostedSelector = ', [hx-boost] a, [data-hx-boost] a, a[hx-boost], a[data-hx-boost]';
      const extensionSelectors = [];
      for (const e in extensions) {
        const extension = extensions[e];
        if (extension.getSelectors) {
          var selectors = extension.getSelectors();
          if (selectors) {
            extensionSelectors.push(selectors);
          }
        }
      }
      const results = elt.querySelectorAll(VERB_SELECTOR + boostedSelector + ", form, [type='submit']," + ' [hx-ext], [data-hx-ext], [hx-trigger], [data-hx-trigger]' + extensionSelectors.flat().map(s => ', ' + s).join(''));
      return results;
    } else {
      return [];
    }
  }

  /**
   * Handle submit buttons/inputs that have the form attribute set
   * see https://developer.mozilla.org/docs/Web/HTML/Element/button
   * @param {Event} evt
   */
  function maybeSetLastButtonClicked(evt) {
    const elt = /** @type {HTMLButtonElement|HTMLInputElement} */closest(asElement(evt.target), "button, input[type='submit']");
    const internalData = getRelatedFormData(evt);
    if (internalData) {
      internalData.lastButtonClicked = elt;
    }
  }

  /**
   * @param {Event} evt
   */
  function maybeUnsetLastButtonClicked(evt) {
    const internalData = getRelatedFormData(evt);
    if (internalData) {
      internalData.lastButtonClicked = null;
    }
  }

  /**
   * @param {Event} evt
   * @returns {HtmxNodeInternalData|undefined}
   */
  function getRelatedFormData(evt) {
    const elt = closest(asElement(evt.target), "button, input[type='submit']");
    if (!elt) {
      return;
    }
    const form = resolveTarget('#' + getRawAttribute(elt, 'form'), elt.getRootNode()) || closest(elt, 'form');
    if (!form) {
      return;
    }
    return getInternalData(form);
  }

  /**
   * @param {EventTarget} elt
   */
  function initButtonTracking(elt) {
    // need to handle both click and focus in:
    //   focusin - in case someone tabs in to a button and hits the space bar
    //   click - on OSX buttons do not focus on click see https://bugs.webkit.org/show_bug.cgi?id=13724
    elt.addEventListener('click', maybeSetLastButtonClicked);
    elt.addEventListener('focusin', maybeSetLastButtonClicked);
    elt.addEventListener('focusout', maybeUnsetLastButtonClicked);
  }

  /**
   * @param {Element} elt
   * @param {string} eventName
   * @param {string} code
   */
  function addHxOnEventHandler(elt, eventName, code) {
    const nodeData = getInternalData(elt);
    if (!Array.isArray(nodeData.onHandlers)) {
      nodeData.onHandlers = [];
    }
    let func;
    /** @type EventListener */
    const listener = function (e) {
      maybeEval(elt, function () {
        if (eltIsDisabled(elt)) {
          return;
        }
        if (!func) {
          func = new Function('event', code);
        }
        func.call(elt, e);
      });
    };
    elt.addEventListener(eventName, listener);
    nodeData.onHandlers.push({
      event: eventName,
      listener
    });
  }

  /**
   * @param {Element} elt
   */
  function processHxOnWildcard(elt) {
    // wipe any previous on handlers so that this function takes precedence
    deInitOnHandlers(elt);
    for (let i = 0; i < elt.attributes.length; i++) {
      const name = elt.attributes[i].name;
      const value = elt.attributes[i].value;
      if (startsWith(name, 'hx-on') || startsWith(name, 'data-hx-on')) {
        const afterOnPosition = name.indexOf('-on') + 3;
        const nextChar = name.slice(afterOnPosition, afterOnPosition + 1);
        if (nextChar === '-' || nextChar === ':') {
          let eventName = name.slice(afterOnPosition + 1);
          // if the eventName starts with a colon or dash, prepend "htmx" for shorthand support
          if (startsWith(eventName, ':')) {
            eventName = 'htmx' + eventName;
          } else if (startsWith(eventName, '-')) {
            eventName = 'htmx:' + eventName.slice(1);
          } else if (startsWith(eventName, 'htmx-')) {
            eventName = 'htmx:' + eventName.slice(5);
          }
          addHxOnEventHandler(elt, eventName, value);
        }
      }
    }
  }

  /**
   * @param {Element|HTMLInputElement} elt
   */
  function initNode(elt) {
    if (closest(elt, htmx.config.disableSelector)) {
      cleanUpElement(elt);
      return;
    }
    const nodeData = getInternalData(elt);
    const attrHash = attributeHash(elt);
    if (nodeData.initHash !== attrHash) {
      // clean up any previously processed info
      deInitNode(elt);
      nodeData.initHash = attrHash;
      triggerEvent(elt, 'htmx:beforeProcessNode');
      const triggerSpecs = getTriggerSpecs(elt);
      const hasExplicitHttpAction = processVerbs(elt, nodeData, triggerSpecs);
      if (!hasExplicitHttpAction) {
        if (getClosestAttributeValue(elt, 'hx-boost') === 'true') {
          boostElement(elt, nodeData, triggerSpecs);
        } else if (hasAttribute(elt, 'hx-trigger')) {
          triggerSpecs.forEach(function (triggerSpec) {
            // For "naked" triggers, don't do anything at all
            addTriggerHandler(elt, triggerSpec, nodeData, function () {});
          });
        }
      }

      // Handle submit buttons/inputs that have the form attribute set
      // see https://developer.mozilla.org/docs/Web/HTML/Element/button
      if (elt.tagName === 'FORM' || getRawAttribute(elt, 'type') === 'submit' && hasAttribute(elt, 'form')) {
        initButtonTracking(elt);
      }
      nodeData.firstInitCompleted = true;
      triggerEvent(elt, 'htmx:afterProcessNode');
    }
  }

  /**
   * Processes new content, enabling htmx behavior. This can be useful if you have content that is added to the DOM outside of the normal htmx request cycle but still want htmx attributes to work.
   *
   * @see https://htmx.org/api/#process
   *
   * @param {Element|string} elt element to process
   */
  function processNode(elt) {
    elt = resolveTarget(elt);
    if (closest(elt, htmx.config.disableSelector)) {
      cleanUpElement(elt);
      return;
    }
    initNode(elt);
    forEach(findElementsToProcess(elt), function (child) {
      initNode(child);
    });
    forEach(findHxOnWildcardElements(elt), processHxOnWildcard);
  }

  //= ===================================================================
  // Event/Log Support
  //= ===================================================================

  /**
   * @param {string} str
   * @returns {string}
   */
  function kebabEventName(str) {
    return str.replace(/([a-z0-9])([A-Z])/g, '$1-$2').toLowerCase();
  }

  /**
   * @param {string} eventName
   * @param {any} detail
   * @returns {CustomEvent}
   */
  function makeEvent(eventName, detail) {
    let evt;
    if (window.CustomEvent && typeof window.CustomEvent === 'function') {
      // TODO: `composed: true` here is a hack to make global event handlers work with events in shadow DOM
      // This breaks expected encapsulation but needs to be here until decided otherwise by core devs
      evt = new CustomEvent(eventName, {
        bubbles: true,
        cancelable: true,
        composed: true,
        detail
      });
    } else {
      evt = getDocument().createEvent('CustomEvent');
      evt.initCustomEvent(eventName, true, true, detail);
    }
    return evt;
  }

  /**
   * @param {EventTarget|string} elt
   * @param {string} eventName
   * @param {any=} detail
   */
  function triggerErrorEvent(elt, eventName, detail) {
    triggerEvent(elt, eventName, mergeObjects({
      error: eventName
    }, detail));
  }

  /**
   * @param {string} eventName
   * @returns {boolean}
   */
  function ignoreEventForLogging(eventName) {
    return eventName === 'htmx:afterProcessNode';
  }

  /**
   * `withExtensions` locates all active extensions for a provided element, then
   * executes the provided function using each of the active extensions.  It should
   * be called internally at every extendable execution point in htmx.
   *
   * @param {Element} elt
   * @param {(extension:HtmxExtension) => void} toDo
   * @returns void
   */
  function withExtensions(elt, toDo) {
    forEach(getExtensions(elt), function (extension) {
      try {
        toDo(extension);
      } catch (e) {
        logError(e);
      }
    });
  }
  function logError(msg) {
    if (console.error) {
      console.error(msg);
    } else if (console.log) {
      console.log('ERROR: ', msg);
    }
  }

  /**
   * Triggers a given event on an element
   *
   * @see https://htmx.org/api/#trigger
   *
   * @param {EventTarget|string} elt the element to trigger the event on
   * @param {string} eventName the name of the event to trigger
   * @param {any=} detail details for the event
   * @returns {boolean}
   */
  function triggerEvent(elt, eventName, detail) {
    elt = resolveTarget(elt);
    if (detail == null) {
      detail = {};
    }
    detail.elt = elt;
    const event = makeEvent(eventName, detail);
    if (htmx.logger && !ignoreEventForLogging(eventName)) {
      htmx.logger(elt, eventName, detail);
    }
    if (detail.error) {
      logError(detail.error);
      triggerEvent(elt, 'htmx:error', {
        errorInfo: detail
      });
    }
    let eventResult = elt.dispatchEvent(event);
    const kebabName = kebabEventName(eventName);
    if (eventResult && kebabName !== eventName) {
      const kebabedEvent = makeEvent(kebabName, event.detail);
      eventResult = eventResult && elt.dispatchEvent(kebabedEvent);
    }
    withExtensions(asElement(elt), function (extension) {
      eventResult = eventResult && extension.onEvent(eventName, event) !== false && !event.defaultPrevented;
    });
    return eventResult;
  }

  //= ===================================================================
  // History Support
  //= ===================================================================
  let currentPathForHistory = location.pathname + location.search;

  /**
   * @returns {Element}
   */
  function getHistoryElement() {
    const historyElt = getDocument().querySelector('[hx-history-elt],[data-hx-history-elt]');
    return historyElt || getDocument().body;
  }

  /**
   * @param {string} url
   * @param {Element} rootElt
   */
  function saveToHistoryCache(url, rootElt) {
    if (!canAccessLocalStorage()) {
      return;
    }

    // get state to save
    const innerHTML = cleanInnerHtmlForHistory(rootElt);
    const title = getDocument().title;
    const scroll = window.scrollY;
    if (htmx.config.historyCacheSize <= 0) {
      // make sure that an eventually already existing cache is purged
      localStorage.removeItem('htmx-history-cache');
      return;
    }
    url = normalizePath(url);
    const historyCache = parseJSON(localStorage.getItem('htmx-history-cache')) || [];
    for (let i = 0; i < historyCache.length; i++) {
      if (historyCache[i].url === url) {
        historyCache.splice(i, 1);
        break;
      }
    }

    /** @type HtmxHistoryItem */
    const newHistoryItem = {
      url,
      content: innerHTML,
      title,
      scroll
    };
    triggerEvent(getDocument().body, 'htmx:historyItemCreated', {
      item: newHistoryItem,
      cache: historyCache
    });
    historyCache.push(newHistoryItem);
    while (historyCache.length > htmx.config.historyCacheSize) {
      historyCache.shift();
    }

    // keep trying to save the cache until it succeeds or is empty
    while (historyCache.length > 0) {
      try {
        localStorage.setItem('htmx-history-cache', JSON.stringify(historyCache));
        break;
      } catch (e) {
        triggerErrorEvent(getDocument().body, 'htmx:historyCacheError', {
          cause: e,
          cache: historyCache
        });
        historyCache.shift(); // shrink the cache and retry
      }
    }
  }

  /**
   * @typedef {Object} HtmxHistoryItem
   * @property {string} url
   * @property {string} content
   * @property {string} title
   * @property {number} scroll
   */

  /**
   * @param {string} url
   * @returns {HtmxHistoryItem|null}
   */
  function getCachedHistory(url) {
    if (!canAccessLocalStorage()) {
      return null;
    }
    url = normalizePath(url);
    const historyCache = parseJSON(localStorage.getItem('htmx-history-cache')) || [];
    for (let i = 0; i < historyCache.length; i++) {
      if (historyCache[i].url === url) {
        return historyCache[i];
      }
    }
    return null;
  }

  /**
   * @param {Element} elt
   * @returns {string}
   */
  function cleanInnerHtmlForHistory(elt) {
    const className = htmx.config.requestClass;
    const clone = /** @type Element */elt.cloneNode(true);
    forEach(findAll(clone, '.' + className), function (child) {
      removeClassFromElement(child, className);
    });
    // remove the disabled attribute for any element disabled due to an htmx request
    forEach(findAll(clone, '[data-disabled-by-htmx]'), function (child) {
      child.removeAttribute('disabled');
    });
    return clone.innerHTML;
  }
  function saveCurrentPageToHistory() {
    const elt = getHistoryElement();
    const path = currentPathForHistory || location.pathname + location.search;

    // Allow history snapshot feature to be disabled where hx-history="false"
    // is present *anywhere* in the current document we're about to save,
    // so we can prevent privileged data entering the cache.
    // The page will still be reachable as a history entry, but htmx will fetch it
    // live from the server onpopstate rather than look in the localStorage cache
    let disableHistoryCache;
    try {
      disableHistoryCache = getDocument().querySelector('[hx-history="false" i],[data-hx-history="false" i]');
    } catch (e) {
      // IE11: insensitive modifier not supported so fallback to case sensitive selector
      disableHistoryCache = getDocument().querySelector('[hx-history="false"],[data-hx-history="false"]');
    }
    if (!disableHistoryCache) {
      triggerEvent(getDocument().body, 'htmx:beforeHistorySave', {
        path,
        historyElt: elt
      });
      saveToHistoryCache(path, elt);
    }
    if (htmx.config.historyEnabled) history.replaceState({
      htmx: true
    }, getDocument().title, window.location.href);
  }

  /**
   * @param {string} path
   */
  function pushUrlIntoHistory(path) {
    // remove the cache buster parameter, if any
    if (htmx.config.getCacheBusterParam) {
      path = path.replace(/org\.htmx\.cache-buster=[^&]*&?/, '');
      if (endsWith(path, '&') || endsWith(path, '?')) {
        path = path.slice(0, -1);
      }
    }
    if (htmx.config.historyEnabled) {
      history.pushState({
        htmx: true
      }, '', path);
    }
    currentPathForHistory = path;
  }

  /**
   * @param {string} path
   */
  function replaceUrlInHistory(path) {
    if (htmx.config.historyEnabled) history.replaceState({
      htmx: true
    }, '', path);
    currentPathForHistory = path;
  }

  /**
   * @param {HtmxSettleTask[]} tasks
   */
  function settleImmediately(tasks) {
    forEach(tasks, function (task) {
      task.call(undefined);
    });
  }

  /**
   * @param {string} path
   */
  function loadHistoryFromServer(path) {
    const request = new XMLHttpRequest();
    const details = {
      path,
      xhr: request
    };
    triggerEvent(getDocument().body, 'htmx:historyCacheMiss', details);
    request.open('GET', path, true);
    request.setRequestHeader('HX-Request', 'true');
    request.setRequestHeader('HX-History-Restore-Request', 'true');
    request.setRequestHeader('HX-Current-URL', getDocument().location.href);
    request.onload = function () {
      if (this.status >= 200 && this.status < 400) {
        triggerEvent(getDocument().body, 'htmx:historyCacheMissLoad', details);
        const fragment = makeFragment(this.response);
        /** @type ParentNode */
        const content = fragment.querySelector('[hx-history-elt],[data-hx-history-elt]') || fragment;
        const historyElement = getHistoryElement();
        const settleInfo = makeSettleInfo(historyElement);
        handleTitle(fragment.title);
        handlePreservedElements(fragment);
        swapInnerHTML(historyElement, content, settleInfo);
        restorePreservedElements();
        settleImmediately(settleInfo.tasks);
        currentPathForHistory = path;
        triggerEvent(getDocument().body, 'htmx:historyRestore', {
          path,
          cacheMiss: true,
          serverResponse: this.response
        });
      } else {
        triggerErrorEvent(getDocument().body, 'htmx:historyCacheMissLoadError', details);
      }
    };
    request.send();
  }

  /**
   * @param {string} [path]
   */
  function restoreHistory(path) {
    saveCurrentPageToHistory();
    path = path || location.pathname + location.search;
    const cached = getCachedHistory(path);
    if (cached) {
      const fragment = makeFragment(cached.content);
      const historyElement = getHistoryElement();
      const settleInfo = makeSettleInfo(historyElement);
      handleTitle(cached.title);
      handlePreservedElements(fragment);
      swapInnerHTML(historyElement, fragment, settleInfo);
      restorePreservedElements();
      settleImmediately(settleInfo.tasks);
      getWindow().setTimeout(function () {
        window.scrollTo(0, cached.scroll);
      }, 0); // next 'tick', so browser has time to render layout
      currentPathForHistory = path;
      triggerEvent(getDocument().body, 'htmx:historyRestore', {
        path,
        item: cached
      });
    } else {
      if (htmx.config.refreshOnHistoryMiss) {
        // @ts-ignore: optional parameter in reload() function throws error
        // noinspection JSUnresolvedReference
        window.location.reload(true);
      } else {
        loadHistoryFromServer(path);
      }
    }
  }

  /**
   * @param {Element} elt
   * @returns {Element[]}
   */
  function addRequestIndicatorClasses(elt) {
    let indicators = /** @type Element[] */findAttributeTargets(elt, 'hx-indicator');
    if (indicators == null) {
      indicators = [elt];
    }
    forEach(indicators, function (ic) {
      const internalData = getInternalData(ic);
      internalData.requestCount = (internalData.requestCount || 0) + 1;
      ic.classList.add.call(ic.classList, htmx.config.requestClass);
    });
    return indicators;
  }

  /**
   * @param {Element} elt
   * @returns {Element[]}
   */
  function disableElements(elt) {
    let disabledElts = /** @type Element[] */findAttributeTargets(elt, 'hx-disabled-elt');
    if (disabledElts == null) {
      disabledElts = [];
    }
    forEach(disabledElts, function (disabledElement) {
      const internalData = getInternalData(disabledElement);
      internalData.requestCount = (internalData.requestCount || 0) + 1;
      disabledElement.setAttribute('disabled', '');
      disabledElement.setAttribute('data-disabled-by-htmx', '');
    });
    return disabledElts;
  }

  /**
   * @param {Element[]} indicators
   * @param {Element[]} disabled
   */
  function removeRequestIndicators(indicators, disabled) {
    forEach(indicators.concat(disabled), function (ele) {
      const internalData = getInternalData(ele);
      internalData.requestCount = (internalData.requestCount || 1) - 1;
    });
    forEach(indicators, function (ic) {
      const internalData = getInternalData(ic);
      if (internalData.requestCount === 0) {
        ic.classList.remove.call(ic.classList, htmx.config.requestClass);
      }
    });
    forEach(disabled, function (disabledElement) {
      const internalData = getInternalData(disabledElement);
      if (internalData.requestCount === 0) {
        disabledElement.removeAttribute('disabled');
        disabledElement.removeAttribute('data-disabled-by-htmx');
      }
    });
  }

  //= ===================================================================
  // Input Value Processing
  //= ===================================================================

  /**
   * @param {Element[]} processed
   * @param {Element} elt
   * @returns {boolean}
   */
  function haveSeenNode(processed, elt) {
    for (let i = 0; i < processed.length; i++) {
      const node = processed[i];
      if (node.isSameNode(elt)) {
        return true;
      }
    }
    return false;
  }

  /**
   * @param {Element} element
   * @return {boolean}
   */
  function shouldInclude(element) {
    // Cast to trick tsc, undefined values will work fine here
    const elt = /** @type {HTMLInputElement} */element;
    if (elt.name === '' || elt.name == null || elt.disabled || closest(elt, 'fieldset[disabled]')) {
      return false;
    }
    // ignore "submitter" types (see jQuery src/serialize.js)
    if (elt.type === 'button' || elt.type === 'submit' || elt.tagName === 'image' || elt.tagName === 'reset' || elt.tagName === 'file') {
      return false;
    }
    if (elt.type === 'checkbox' || elt.type === 'radio') {
      return elt.checked;
    }
    return true;
  }

  /** @param {string} name
   * @param {string|Array|FormDataEntryValue} value
   * @param {FormData} formData */
  function addValueToFormData(name, value, formData) {
    if (name != null && value != null) {
      if (Array.isArray(value)) {
        value.forEach(function (v) {
          formData.append(name, v);
        });
      } else {
        formData.append(name, value);
      }
    }
  }

  /** @param {string} name
   * @param {string|Array} value
   * @param {FormData} formData */
  function removeValueFromFormData(name, value, formData) {
    if (name != null && value != null) {
      let values = formData.getAll(name);
      if (Array.isArray(value)) {
        values = values.filter(v => value.indexOf(v) < 0);
      } else {
        values = values.filter(v => v !== value);
      }
      formData.delete(name);
      forEach(values, v => formData.append(name, v));
    }
  }

  /**
   * @param {Element[]} processed
   * @param {FormData} formData
   * @param {HtmxElementValidationError[]} errors
   * @param {Element|HTMLInputElement|HTMLSelectElement|HTMLFormElement} elt
   * @param {boolean} validate
   */
  function processInputValue(processed, formData, errors, elt, validate) {
    if (elt == null || haveSeenNode(processed, elt)) {
      return;
    } else {
      processed.push(elt);
    }
    if (shouldInclude(elt)) {
      const name = getRawAttribute(elt, 'name');
      // @ts-ignore value will be undefined for non-input elements, which is fine
      let value = elt.value;
      if (elt instanceof HTMLSelectElement && elt.multiple) {
        value = toArray(elt.querySelectorAll('option:checked')).map(function (e) {
          return (/** @type HTMLOptionElement */e).value;
        });
      }
      // include file inputs
      if (elt instanceof HTMLInputElement && elt.files) {
        value = toArray(elt.files);
      }
      addValueToFormData(name, value, formData);
      if (validate) {
        validateElement(elt, errors);
      }
    }
    if (elt instanceof HTMLFormElement) {
      forEach(elt.elements, function (input) {
        if (processed.indexOf(input) >= 0) {
          // The input has already been processed and added to the values, but the FormData that will be
          //  constructed right after on the form, will include it once again. So remove that input's value
          //  now to avoid duplicates
          removeValueFromFormData(input.name, input.value, formData);
        } else {
          processed.push(input);
        }
        if (validate) {
          validateElement(input, errors);
        }
      });
      new FormData(elt).forEach(function (value, name) {
        if (value instanceof File && value.name === '') {
          return; // ignore no-name files
        }
        addValueToFormData(name, value, formData);
      });
    }
  }

  /**
   *
   * @param {Element} elt
   * @param {HtmxElementValidationError[]} errors
   */
  function validateElement(elt, errors) {
    const element = /** @type {HTMLElement & ElementInternals} */elt;
    if (element.willValidate) {
      triggerEvent(element, 'htmx:validation:validate');
      if (!element.checkValidity()) {
        errors.push({
          elt: element,
          message: element.validationMessage,
          validity: element.validity
        });
        triggerEvent(element, 'htmx:validation:failed', {
          message: element.validationMessage,
          validity: element.validity
        });
      }
    }
  }

  /**
   * Override values in the one FormData with those from another.
   * @param {FormData} receiver the formdata that will be mutated
   * @param {FormData} donor the formdata that will provide the overriding values
   * @returns {FormData} the {@linkcode receiver}
   */
  function overrideFormData(receiver, donor) {
    for (const key of donor.keys()) {
      receiver.delete(key);
    }
    donor.forEach(function (value, key) {
      receiver.append(key, value);
    });
    return receiver;
  }

  /**
  * @param {Element|HTMLFormElement} elt
  * @param {HttpVerb} verb
  * @returns {{errors: HtmxElementValidationError[], formData: FormData, values: Object}}
  */
  function getInputValues(elt, verb) {
    /** @type Element[] */
    const processed = [];
    const formData = new FormData();
    const priorityFormData = new FormData();
    /** @type HtmxElementValidationError[] */
    const errors = [];
    const internalData = getInternalData(elt);
    if (internalData.lastButtonClicked && !bodyContains(internalData.lastButtonClicked)) {
      internalData.lastButtonClicked = null;
    }

    // only validate when form is directly submitted and novalidate or formnovalidate are not set
    // or if the element has an explicit hx-validate="true" on it
    let validate = elt instanceof HTMLFormElement && elt.noValidate !== true || getAttributeValue(elt, 'hx-validate') === 'true';
    if (internalData.lastButtonClicked) {
      validate = validate && internalData.lastButtonClicked.formNoValidate !== true;
    }

    // for a non-GET include the closest form
    if (verb !== 'get') {
      processInputValue(processed, priorityFormData, errors, closest(elt, 'form'), validate);
    }

    // include the element itself
    processInputValue(processed, formData, errors, elt, validate);

    // if a button or submit was clicked last, include its value
    if (internalData.lastButtonClicked || elt.tagName === 'BUTTON' || elt.tagName === 'INPUT' && getRawAttribute(elt, 'type') === 'submit') {
      const button = internalData.lastButtonClicked || (/** @type HTMLInputElement|HTMLButtonElement */elt);
      const name = getRawAttribute(button, 'name');
      addValueToFormData(name, button.value, priorityFormData);
    }

    // include any explicit includes
    const includes = findAttributeTargets(elt, 'hx-include');
    forEach(includes, function (node) {
      processInputValue(processed, formData, errors, asElement(node), validate);
      // if a non-form is included, include any input values within it
      if (!matches(node, 'form')) {
        forEach(asParentNode(node).querySelectorAll(INPUT_SELECTOR), function (descendant) {
          processInputValue(processed, formData, errors, descendant, validate);
        });
      }
    });

    // values from a <form> take precedence, overriding the regular values
    overrideFormData(formData, priorityFormData);
    return {
      errors,
      formData,
      values: formDataProxy(formData)
    };
  }

  /**
   * @param {string} returnStr
   * @param {string} name
   * @param {any} realValue
   * @returns {string}
   */
  function appendParam(returnStr, name, realValue) {
    if (returnStr !== '') {
      returnStr += '&';
    }
    if (String(realValue) === '[object Object]') {
      realValue = JSON.stringify(realValue);
    }
    const s = encodeURIComponent(realValue);
    returnStr += encodeURIComponent(name) + '=' + s;
    return returnStr;
  }

  /**
   * @param {FormData|Object} values
   * @returns string
   */
  function urlEncode(values) {
    values = formDataFromObject(values);
    let returnStr = '';
    values.forEach(function (value, key) {
      returnStr = appendParam(returnStr, key, value);
    });
    return returnStr;
  }

  //= ===================================================================
  // Ajax
  //= ===================================================================

  /**
  * @param {Element} elt
  * @param {Element} target
  * @param {string} prompt
  * @returns {HtmxHeaderSpecification}
  */
  function getHeaders(elt, target, prompt) {
    /** @type HtmxHeaderSpecification */
    const headers = {
      'HX-Request': 'true',
      'HX-Trigger': getRawAttribute(elt, 'id'),
      'HX-Trigger-Name': getRawAttribute(elt, 'name'),
      'HX-Target': getAttributeValue(target, 'id'),
      'HX-Current-URL': getDocument().location.href
    };
    getValuesForElement(elt, 'hx-headers', false, headers);
    if (prompt !== undefined) {
      headers['HX-Prompt'] = prompt;
    }
    if (getInternalData(elt).boosted) {
      headers['HX-Boosted'] = 'true';
    }
    return headers;
  }

  /**
  * filterValues takes an object containing form input values
  * and returns a new object that only contains keys that are
  * specified by the closest "hx-params" attribute
  * @param {FormData} inputValues
  * @param {Element} elt
  * @returns {FormData}
  */
  function filterValues(inputValues, elt) {
    const paramsValue = getClosestAttributeValue(elt, 'hx-params');
    if (paramsValue) {
      if (paramsValue === 'none') {
        return new FormData();
      } else if (paramsValue === '*') {
        return inputValues;
      } else if (paramsValue.indexOf('not ') === 0) {
        forEach(paramsValue.slice(4).split(','), function (name) {
          name = name.trim();
          inputValues.delete(name);
        });
        return inputValues;
      } else {
        const newValues = new FormData();
        forEach(paramsValue.split(','), function (name) {
          name = name.trim();
          if (inputValues.has(name)) {
            inputValues.getAll(name).forEach(function (value) {
              newValues.append(name, value);
            });
          }
        });
        return newValues;
      }
    } else {
      return inputValues;
    }
  }

  /**
   * @param {Element} elt
   * @return {boolean}
   */
  function isAnchorLink(elt) {
    return !!getRawAttribute(elt, 'href') && getRawAttribute(elt, 'href').indexOf('#') >= 0;
  }

  /**
  * @param {Element} elt
  * @param {HtmxSwapStyle} [swapInfoOverride]
  * @returns {HtmxSwapSpecification}
  */
  function getSwapSpecification(elt, swapInfoOverride) {
    const swapInfo = swapInfoOverride || getClosestAttributeValue(elt, 'hx-swap');
    /** @type HtmxSwapSpecification */
    const swapSpec = {
      swapStyle: getInternalData(elt).boosted ? 'innerHTML' : htmx.config.defaultSwapStyle,
      swapDelay: htmx.config.defaultSwapDelay,
      settleDelay: htmx.config.defaultSettleDelay
    };
    if (htmx.config.scrollIntoViewOnBoost && getInternalData(elt).boosted && !isAnchorLink(elt)) {
      swapSpec.show = 'top';
    }
    if (swapInfo) {
      const split = splitOnWhitespace(swapInfo);
      if (split.length > 0) {
        for (let i = 0; i < split.length; i++) {
          const value = split[i];
          if (value.indexOf('swap:') === 0) {
            swapSpec.swapDelay = parseInterval(value.slice(5));
          } else if (value.indexOf('settle:') === 0) {
            swapSpec.settleDelay = parseInterval(value.slice(7));
          } else if (value.indexOf('transition:') === 0) {
            swapSpec.transition = value.slice(11) === 'true';
          } else if (value.indexOf('ignoreTitle:') === 0) {
            swapSpec.ignoreTitle = value.slice(12) === 'true';
          } else if (value.indexOf('scroll:') === 0) {
            const scrollSpec = value.slice(7);
            var splitSpec = scrollSpec.split(':');
            const scrollVal = splitSpec.pop();
            var selectorVal = splitSpec.length > 0 ? splitSpec.join(':') : null;
            // @ts-ignore
            swapSpec.scroll = scrollVal;
            swapSpec.scrollTarget = selectorVal;
          } else if (value.indexOf('show:') === 0) {
            const showSpec = value.slice(5);
            var splitSpec = showSpec.split(':');
            const showVal = splitSpec.pop();
            var selectorVal = splitSpec.length > 0 ? splitSpec.join(':') : null;
            swapSpec.show = showVal;
            swapSpec.showTarget = selectorVal;
          } else if (value.indexOf('focus-scroll:') === 0) {
            const focusScrollVal = value.slice('focus-scroll:'.length);
            swapSpec.focusScroll = focusScrollVal == 'true';
          } else if (i == 0) {
            swapSpec.swapStyle = value;
          } else {
            logError('Unknown modifier in hx-swap: ' + value);
          }
        }
      }
    }
    return swapSpec;
  }

  /**
   * @param {Element} elt
   * @return {boolean}
   */
  function usesFormData(elt) {
    return getClosestAttributeValue(elt, 'hx-encoding') === 'multipart/form-data' || matches(elt, 'form') && getRawAttribute(elt, 'enctype') === 'multipart/form-data';
  }

  /**
   * @param {XMLHttpRequest} xhr
   * @param {Element} elt
   * @param {FormData} filteredParameters
   * @returns {*|string|null}
   */
  function encodeParamsForBody(xhr, elt, filteredParameters) {
    let encodedParameters = null;
    withExtensions(elt, function (extension) {
      if (encodedParameters == null) {
        encodedParameters = extension.encodeParameters(xhr, filteredParameters, elt);
      }
    });
    if (encodedParameters != null) {
      return encodedParameters;
    } else {
      if (usesFormData(elt)) {
        // Force conversion to an actual FormData object in case filteredParameters is a formDataProxy
        // See https://github.com/bigskysoftware/htmx/issues/2317
        return overrideFormData(new FormData(), formDataFromObject(filteredParameters));
      } else {
        return urlEncode(filteredParameters);
      }
    }
  }

  /**
  *
  * @param {Element} target
  * @returns {HtmxSettleInfo}
  */
  function makeSettleInfo(target) {
    return {
      tasks: [],
      elts: [target]
    };
  }

  /**
   * @param {Element[]} content
   * @param {HtmxSwapSpecification} swapSpec
   */
  function updateScrollState(content, swapSpec) {
    const first = content[0];
    const last = content[content.length - 1];
    if (swapSpec.scroll) {
      var target = null;
      if (swapSpec.scrollTarget) {
        target = asElement(querySelectorExt(first, swapSpec.scrollTarget));
      }
      if (swapSpec.scroll === 'top' && (first || target)) {
        target = target || first;
        target.scrollTop = 0;
      }
      if (swapSpec.scroll === 'bottom' && (last || target)) {
        target = target || last;
        target.scrollTop = target.scrollHeight;
      }
    }
    if (swapSpec.show) {
      var target = null;
      if (swapSpec.showTarget) {
        let targetStr = swapSpec.showTarget;
        if (swapSpec.showTarget === 'window') {
          targetStr = 'body';
        }
        target = asElement(querySelectorExt(first, targetStr));
      }
      if (swapSpec.show === 'top' && (first || target)) {
        target = target || first;
        // @ts-ignore For some reason tsc doesn't recognize "instant" as a valid option for now
        target.scrollIntoView({
          block: 'start',
          behavior: htmx.config.scrollBehavior
        });
      }
      if (swapSpec.show === 'bottom' && (last || target)) {
        target = target || last;
        // @ts-ignore For some reason tsc doesn't recognize "instant" as a valid option for now
        target.scrollIntoView({
          block: 'end',
          behavior: htmx.config.scrollBehavior
        });
      }
    }
  }

  /**
  * @param {Element} elt
  * @param {string} attr
  * @param {boolean=} evalAsDefault
  * @param {Object=} values
  * @returns {Object}
  */
  function getValuesForElement(elt, attr, evalAsDefault, values) {
    if (values == null) {
      values = {};
    }
    if (elt == null) {
      return values;
    }
    const attributeValue = getAttributeValue(elt, attr);
    if (attributeValue) {
      let str = attributeValue.trim();
      let evaluateValue = evalAsDefault;
      if (str === 'unset') {
        return null;
      }
      if (str.indexOf('javascript:') === 0) {
        str = str.slice(11);
        evaluateValue = true;
      } else if (str.indexOf('js:') === 0) {
        str = str.slice(3);
        evaluateValue = true;
      }
      if (str.indexOf('{') !== 0) {
        str = '{' + str + '}';
      }
      let varsValues;
      if (evaluateValue) {
        varsValues = maybeEval(elt, function () {
          return Function('return (' + str + ')')();
        }, {});
      } else {
        varsValues = parseJSON(str);
      }
      for (const key in varsValues) {
        if (varsValues.hasOwnProperty(key)) {
          if (values[key] == null) {
            values[key] = varsValues[key];
          }
        }
      }
    }
    return getValuesForElement(asElement(parentElt(elt)), attr, evalAsDefault, values);
  }

  /**
   * @param {EventTarget|string} elt
   * @param {() => any} toEval
   * @param {any=} defaultVal
   * @returns {any}
   */
  function maybeEval(elt, toEval, defaultVal) {
    if (htmx.config.allowEval) {
      return toEval();
    } else {
      triggerErrorEvent(elt, 'htmx:evalDisallowedError');
      return defaultVal;
    }
  }

  /**
  * @param {Element} elt
  * @param {*?} expressionVars
  * @returns
  */
  function getHXVarsForElement(elt, expressionVars) {
    return getValuesForElement(elt, 'hx-vars', true, expressionVars);
  }

  /**
  * @param {Element} elt
  * @param {*?} expressionVars
  * @returns
  */
  function getHXValsForElement(elt, expressionVars) {
    return getValuesForElement(elt, 'hx-vals', false, expressionVars);
  }

  /**
  * @param {Element} elt
  * @returns {FormData}
  */
  function getExpressionVars(elt) {
    return mergeObjects(getHXVarsForElement(elt), getHXValsForElement(elt));
  }

  /**
   * @param {XMLHttpRequest} xhr
   * @param {string} header
   * @param {string|null} headerValue
   */
  function safelySetHeaderValue(xhr, header, headerValue) {
    if (headerValue !== null) {
      try {
        xhr.setRequestHeader(header, headerValue);
      } catch (e) {
        // On an exception, try to set the header URI encoded instead
        xhr.setRequestHeader(header, encodeURIComponent(headerValue));
        xhr.setRequestHeader(header + '-URI-AutoEncoded', 'true');
      }
    }
  }

  /**
   * @param {XMLHttpRequest} xhr
   * @return {string}
   */
  function getPathFromResponse(xhr) {
    // NB: IE11 does not support this stuff
    if (xhr.responseURL && typeof URL !== 'undefined') {
      try {
        const url = new URL(xhr.responseURL);
        return url.pathname + url.search;
      } catch (e) {
        triggerErrorEvent(getDocument().body, 'htmx:badResponseUrl', {
          url: xhr.responseURL
        });
      }
    }
  }

  /**
   * @param {XMLHttpRequest} xhr
   * @param {RegExp} regexp
   * @return {boolean}
   */
  function hasHeader(xhr, regexp) {
    return regexp.test(xhr.getAllResponseHeaders());
  }

  /**
   * Issues an htmx-style AJAX request
   *
   * @see https://htmx.org/api/#ajax
   *
   * @param {HttpVerb} verb
   * @param {string} path the URL path to make the AJAX
   * @param {Element|string|HtmxAjaxHelperContext} context the element to target (defaults to the **body**) | a selector for the target | a context object that contains any of the following
   * @return {Promise<void>} Promise that resolves immediately if no request is sent, or when the request is complete
   */
  function ajaxHelper(verb, path, context) {
    verb = /** @type HttpVerb */verb.toLowerCase();
    if (context) {
      if (context instanceof Element || typeof context === 'string') {
        return issueAjaxRequest(verb, path, null, null, {
          targetOverride: resolveTarget(context) || DUMMY_ELT,
          returnPromise: true
        });
      } else {
        let resolvedTarget = resolveTarget(context.target);
        // If target is supplied but can't resolve OR source is supplied but both target and source can't be resolved
        // then use DUMMY_ELT to abort the request with htmx:targetError to avoid it replacing body by mistake
        if (context.target && !resolvedTarget || context.source && !resolvedTarget && !resolveTarget(context.source)) {
          resolvedTarget = DUMMY_ELT;
        }
        return issueAjaxRequest(verb, path, resolveTarget(context.source), context.event, {
          handler: context.handler,
          headers: context.headers,
          values: context.values,
          targetOverride: resolvedTarget,
          swapOverride: context.swap,
          select: context.select,
          returnPromise: true
        });
      }
    } else {
      return issueAjaxRequest(verb, path, null, null, {
        returnPromise: true
      });
    }
  }

  /**
   * @param {Element} elt
   * @return {Element[]}
   */
  function hierarchyForElt(elt) {
    const arr = [];
    while (elt) {
      arr.push(elt);
      elt = elt.parentElement;
    }
    return arr;
  }

  /**
   * @param {Element} elt
   * @param {string} path
   * @param {HtmxRequestConfig} requestConfig
   * @return {boolean}
   */
  function verifyPath(elt, path, requestConfig) {
    let sameHost;
    let url;
    if (typeof URL === 'function') {
      url = new URL(path, document.location.href);
      const origin = document.location.origin;
      sameHost = origin === url.origin;
    } else {
      // IE11 doesn't support URL
      url = path;
      sameHost = startsWith(path, document.location.origin);
    }
    if (htmx.config.selfRequestsOnly) {
      if (!sameHost) {
        return false;
      }
    }
    return triggerEvent(elt, 'htmx:validateUrl', mergeObjects({
      url,
      sameHost
    }, requestConfig));
  }

  /**
   * @param {Object|FormData} obj
   * @return {FormData}
   */
  function formDataFromObject(obj) {
    if (obj instanceof FormData) return obj;
    const formData = new FormData();
    for (const key in obj) {
      if (obj.hasOwnProperty(key)) {
        if (obj[key] && typeof obj[key].forEach === 'function') {
          obj[key].forEach(function (v) {
            formData.append(key, v);
          });
        } else if (typeof obj[key] === 'object' && !(obj[key] instanceof Blob)) {
          formData.append(key, JSON.stringify(obj[key]));
        } else {
          formData.append(key, obj[key]);
        }
      }
    }
    return formData;
  }

  /**
   * @param {FormData} formData
   * @param {string} name
   * @param {Array} array
   * @returns {Array}
   */
  function formDataArrayProxy(formData, name, array) {
    // mutating the array should mutate the underlying form data
    return new Proxy(array, {
      get: function (target, key) {
        if (typeof key === 'number') return target[key];
        if (key === 'length') return target.length;
        if (key === 'push') {
          return function (value) {
            target.push(value);
            formData.append(name, value);
          };
        }
        if (typeof target[key] === 'function') {
          return function () {
            target[key].apply(target, arguments);
            formData.delete(name);
            target.forEach(function (v) {
              formData.append(name, v);
            });
          };
        }
        if (target[key] && target[key].length === 1) {
          return target[key][0];
        } else {
          return target[key];
        }
      },
      set: function (target, index, value) {
        target[index] = value;
        formData.delete(name);
        target.forEach(function (v) {
          formData.append(name, v);
        });
        return true;
      }
    });
  }

  /**
   * @param {FormData} formData
   * @returns {Object}
   */
  function formDataProxy(formData) {
    return new Proxy(formData, {
      get: function (target, name) {
        if (typeof name === 'symbol') {
          // Forward symbol calls to the FormData itself directly
          const result = Reflect.get(target, name);
          // Wrap in function with apply to correctly bind the FormData context, as a direct call would result in an illegal invocation error
          if (typeof result === 'function') {
            return function () {
              return result.apply(formData, arguments);
            };
          } else {
            return result;
          }
        }
        if (name === 'toJSON') {
          // Support JSON.stringify call on proxy
          return () => Object.fromEntries(formData);
        }
        if (name in target) {
          // Wrap in function with apply to correctly bind the FormData context, as a direct call would result in an illegal invocation error
          if (typeof target[name] === 'function') {
            return function () {
              return formData[name].apply(formData, arguments);
            };
          } else {
            return target[name];
          }
        }
        const array = formData.getAll(name);
        // Those 2 undefined & single value returns are for retro-compatibility as we weren't using FormData before
        if (array.length === 0) {
          return undefined;
        } else if (array.length === 1) {
          return array[0];
        } else {
          return formDataArrayProxy(target, name, array);
        }
      },
      set: function (target, name, value) {
        if (typeof name !== 'string') {
          return false;
        }
        target.delete(name);
        if (value && typeof value.forEach === 'function') {
          value.forEach(function (v) {
            target.append(name, v);
          });
        } else if (typeof value === 'object' && !(value instanceof Blob)) {
          target.append(name, JSON.stringify(value));
        } else {
          target.append(name, value);
        }
        return true;
      },
      deleteProperty: function (target, name) {
        if (typeof name === 'string') {
          target.delete(name);
        }
        return true;
      },
      // Support Object.assign call from proxy
      ownKeys: function (target) {
        return Reflect.ownKeys(Object.fromEntries(target));
      },
      getOwnPropertyDescriptor: function (target, prop) {
        return Reflect.getOwnPropertyDescriptor(Object.fromEntries(target), prop);
      }
    });
  }

  /**
   * @param {HttpVerb} verb
   * @param {string} path
   * @param {Element} elt
   * @param {Event} event
   * @param {HtmxAjaxEtc} [etc]
   * @param {boolean} [confirmed]
   * @return {Promise<void>}
   */
  function issueAjaxRequest(verb, path, elt, event, etc, confirmed) {
    let resolve = null;
    let reject = null;
    etc = etc != null ? etc : {};
    if (etc.returnPromise && typeof Promise !== 'undefined') {
      var promise = new Promise(function (_resolve, _reject) {
        resolve = _resolve;
        reject = _reject;
      });
    }
    if (elt == null) {
      elt = getDocument().body;
    }
    const responseHandler = etc.handler || handleAjaxResponse;
    const select = etc.select || null;
    if (!bodyContains(elt)) {
      // do not issue requests for elements removed from the DOM
      maybeCall(resolve);
      return promise;
    }
    const target = etc.targetOverride || asElement(getTarget(elt));
    if (target == null || target == DUMMY_ELT) {
      triggerErrorEvent(elt, 'htmx:targetError', {
        target: getAttributeValue(elt, 'hx-target')
      });
      maybeCall(reject);
      return promise;
    }
    let eltData = getInternalData(elt);
    const submitter = eltData.lastButtonClicked;
    if (submitter) {
      const buttonPath = getRawAttribute(submitter, 'formaction');
      if (buttonPath != null) {
        path = buttonPath;
      }
      const buttonVerb = getRawAttribute(submitter, 'formmethod');
      if (buttonVerb != null) {
        // ignore buttons with formmethod="dialog"
        if (buttonVerb.toLowerCase() !== 'dialog') {
          verb = /** @type HttpVerb */buttonVerb;
        }
      }
    }
    const confirmQuestion = getClosestAttributeValue(elt, 'hx-confirm');
    // allow event-based confirmation w/ a callback
    if (confirmed === undefined) {
      const issueRequest = function (skipConfirmation) {
        return issueAjaxRequest(verb, path, elt, event, etc, !!skipConfirmation);
      };
      const confirmDetails = {
        target,
        elt,
        path,
        verb,
        triggeringEvent: event,
        etc,
        issueRequest,
        question: confirmQuestion
      };
      if (triggerEvent(elt, 'htmx:confirm', confirmDetails) === false) {
        maybeCall(resolve);
        return promise;
      }
    }
    let syncElt = elt;
    let syncStrategy = getClosestAttributeValue(elt, 'hx-sync');
    let queueStrategy = null;
    let abortable = false;
    if (syncStrategy) {
      const syncStrings = syncStrategy.split(':');
      const selector = syncStrings[0].trim();
      if (selector === 'this') {
        syncElt = findThisElement(elt, 'hx-sync');
      } else {
        syncElt = asElement(querySelectorExt(elt, selector));
      }
      // default to the drop strategy
      syncStrategy = (syncStrings[1] || 'drop').trim();
      eltData = getInternalData(syncElt);
      if (syncStrategy === 'drop' && eltData.xhr && eltData.abortable !== true) {
        maybeCall(resolve);
        return promise;
      } else if (syncStrategy === 'abort') {
        if (eltData.xhr) {
          maybeCall(resolve);
          return promise;
        } else {
          abortable = true;
        }
      } else if (syncStrategy === 'replace') {
        triggerEvent(syncElt, 'htmx:abort'); // abort the current request and continue
      } else if (syncStrategy.indexOf('queue') === 0) {
        const queueStrArray = syncStrategy.split(' ');
        queueStrategy = (queueStrArray[1] || 'last').trim();
      }
    }
    if (eltData.xhr) {
      if (eltData.abortable) {
        triggerEvent(syncElt, 'htmx:abort'); // abort the current request and continue
      } else {
        if (queueStrategy == null) {
          if (event) {
            const eventData = getInternalData(event);
            if (eventData && eventData.triggerSpec && eventData.triggerSpec.queue) {
              queueStrategy = eventData.triggerSpec.queue;
            }
          }
          if (queueStrategy == null) {
            queueStrategy = 'last';
          }
        }
        if (eltData.queuedRequests == null) {
          eltData.queuedRequests = [];
        }
        if (queueStrategy === 'first' && eltData.queuedRequests.length === 0) {
          eltData.queuedRequests.push(function () {
            issueAjaxRequest(verb, path, elt, event, etc);
          });
        } else if (queueStrategy === 'all') {
          eltData.queuedRequests.push(function () {
            issueAjaxRequest(verb, path, elt, event, etc);
          });
        } else if (queueStrategy === 'last') {
          eltData.queuedRequests = []; // dump existing queue
          eltData.queuedRequests.push(function () {
            issueAjaxRequest(verb, path, elt, event, etc);
          });
        }
        maybeCall(resolve);
        return promise;
      }
    }
    const xhr = new XMLHttpRequest();
    eltData.xhr = xhr;
    eltData.abortable = abortable;
    const endRequestLock = function () {
      eltData.xhr = null;
      eltData.abortable = false;
      if (eltData.queuedRequests != null && eltData.queuedRequests.length > 0) {
        const queuedRequest = eltData.queuedRequests.shift();
        queuedRequest();
      }
    };
    const promptQuestion = getClosestAttributeValue(elt, 'hx-prompt');
    if (promptQuestion) {
      var promptResponse = prompt(promptQuestion);
      // prompt returns null if cancelled and empty string if accepted with no entry
      if (promptResponse === null || !triggerEvent(elt, 'htmx:prompt', {
        prompt: promptResponse,
        target
      })) {
        maybeCall(resolve);
        endRequestLock();
        return promise;
      }
    }
    if (confirmQuestion && !confirmed) {
      if (!confirm(confirmQuestion)) {
        maybeCall(resolve);
        endRequestLock();
        return promise;
      }
    }
    let headers = getHeaders(elt, target, promptResponse);
    if (verb !== 'get' && !usesFormData(elt)) {
      headers['Content-Type'] = 'application/x-www-form-urlencoded';
    }
    if (etc.headers) {
      headers = mergeObjects(headers, etc.headers);
    }
    const results = getInputValues(elt, verb);
    let errors = results.errors;
    const rawFormData = results.formData;
    if (etc.values) {
      overrideFormData(rawFormData, formDataFromObject(etc.values));
    }
    const expressionVars = formDataFromObject(getExpressionVars(elt));
    const allFormData = overrideFormData(rawFormData, expressionVars);
    let filteredFormData = filterValues(allFormData, elt);
    if (htmx.config.getCacheBusterParam && verb === 'get') {
      filteredFormData.set('org.htmx.cache-buster', getRawAttribute(target, 'id') || 'true');
    }

    // behavior of anchors w/ empty href is to use the current URL
    if (path == null || path === '') {
      path = getDocument().location.href;
    }

    /**
     * @type {Object}
     * @property {boolean} [credentials]
     * @property {number} [timeout]
     * @property {boolean} [noHeaders]
     */
    const requestAttrValues = getValuesForElement(elt, 'hx-request');
    const eltIsBoosted = getInternalData(elt).boosted;
    let useUrlParams = htmx.config.methodsThatUseUrlParams.indexOf(verb) >= 0;

    /** @type HtmxRequestConfig */
    const requestConfig = {
      boosted: eltIsBoosted,
      useUrlParams,
      formData: filteredFormData,
      parameters: formDataProxy(filteredFormData),
      unfilteredFormData: allFormData,
      unfilteredParameters: formDataProxy(allFormData),
      headers,
      target,
      verb,
      errors,
      withCredentials: etc.credentials || requestAttrValues.credentials || htmx.config.withCredentials,
      timeout: etc.timeout || requestAttrValues.timeout || htmx.config.timeout,
      path,
      triggeringEvent: event
    };
    if (!triggerEvent(elt, 'htmx:configRequest', requestConfig)) {
      maybeCall(resolve);
      endRequestLock();
      return promise;
    }

    // copy out in case the object was overwritten
    path = requestConfig.path;
    verb = requestConfig.verb;
    headers = requestConfig.headers;
    filteredFormData = formDataFromObject(requestConfig.parameters);
    errors = requestConfig.errors;
    useUrlParams = requestConfig.useUrlParams;
    if (errors && errors.length > 0) {
      triggerEvent(elt, 'htmx:validation:halted', requestConfig);
      maybeCall(resolve);
      endRequestLock();
      return promise;
    }
    const splitPath = path.split('#');
    const pathNoAnchor = splitPath[0];
    const anchor = splitPath[1];
    let finalPath = path;
    if (useUrlParams) {
      finalPath = pathNoAnchor;
      const hasValues = !filteredFormData.keys().next().done;
      if (hasValues) {
        if (finalPath.indexOf('?') < 0) {
          finalPath += '?';
        } else {
          finalPath += '&';
        }
        finalPath += urlEncode(filteredFormData);
        if (anchor) {
          finalPath += '#' + anchor;
        }
      }
    }
    if (!verifyPath(elt, finalPath, requestConfig)) {
      triggerErrorEvent(elt, 'htmx:invalidPath', requestConfig);
      maybeCall(reject);
      return promise;
    }
    xhr.open(verb.toUpperCase(), finalPath, true);
    xhr.overrideMimeType('text/html');
    xhr.withCredentials = requestConfig.withCredentials;
    xhr.timeout = requestConfig.timeout;

    // request headers
    if (requestAttrValues.noHeaders) {
      // ignore all headers
    } else {
      for (const header in headers) {
        if (headers.hasOwnProperty(header)) {
          const headerValue = headers[header];
          safelySetHeaderValue(xhr, header, headerValue);
        }
      }
    }

    /** @type {HtmxResponseInfo} */
    const responseInfo = {
      xhr,
      target,
      requestConfig,
      etc,
      boosted: eltIsBoosted,
      select,
      pathInfo: {
        requestPath: path,
        finalRequestPath: finalPath,
        responsePath: null,
        anchor
      }
    };
    xhr.onload = function () {
      try {
        const hierarchy = hierarchyForElt(elt);
        responseInfo.pathInfo.responsePath = getPathFromResponse(xhr);
        responseHandler(elt, responseInfo);
        if (responseInfo.keepIndicators !== true) {
          removeRequestIndicators(indicators, disableElts);
        }
        triggerEvent(elt, 'htmx:afterRequest', responseInfo);
        triggerEvent(elt, 'htmx:afterOnLoad', responseInfo);
        // if the body no longer contains the element, trigger the event on the closest parent
        // remaining in the DOM
        if (!bodyContains(elt)) {
          let secondaryTriggerElt = null;
          while (hierarchy.length > 0 && secondaryTriggerElt == null) {
            const parentEltInHierarchy = hierarchy.shift();
            if (bodyContains(parentEltInHierarchy)) {
              secondaryTriggerElt = parentEltInHierarchy;
            }
          }
          if (secondaryTriggerElt) {
            triggerEvent(secondaryTriggerElt, 'htmx:afterRequest', responseInfo);
            triggerEvent(secondaryTriggerElt, 'htmx:afterOnLoad', responseInfo);
          }
        }
        maybeCall(resolve);
        endRequestLock();
      } catch (e) {
        triggerErrorEvent(elt, 'htmx:onLoadError', mergeObjects({
          error: e
        }, responseInfo));
        throw e;
      }
    };
    xhr.onerror = function () {
      removeRequestIndicators(indicators, disableElts);
      triggerErrorEvent(elt, 'htmx:afterRequest', responseInfo);
      triggerErrorEvent(elt, 'htmx:sendError', responseInfo);
      maybeCall(reject);
      endRequestLock();
    };
    xhr.onabort = function () {
      removeRequestIndicators(indicators, disableElts);
      triggerErrorEvent(elt, 'htmx:afterRequest', responseInfo);
      triggerErrorEvent(elt, 'htmx:sendAbort', responseInfo);
      maybeCall(reject);
      endRequestLock();
    };
    xhr.ontimeout = function () {
      removeRequestIndicators(indicators, disableElts);
      triggerErrorEvent(elt, 'htmx:afterRequest', responseInfo);
      triggerErrorEvent(elt, 'htmx:timeout', responseInfo);
      maybeCall(reject);
      endRequestLock();
    };
    if (!triggerEvent(elt, 'htmx:beforeRequest', responseInfo)) {
      maybeCall(resolve);
      endRequestLock();
      return promise;
    }
    var indicators = addRequestIndicatorClasses(elt);
    var disableElts = disableElements(elt);
    forEach(['loadstart', 'loadend', 'progress', 'abort'], function (eventName) {
      forEach([xhr, xhr.upload], function (target) {
        target.addEventListener(eventName, function (event) {
          triggerEvent(elt, 'htmx:xhr:' + eventName, {
            lengthComputable: event.lengthComputable,
            loaded: event.loaded,
            total: event.total
          });
        });
      });
    });
    triggerEvent(elt, 'htmx:beforeSend', responseInfo);
    const params = useUrlParams ? null : encodeParamsForBody(xhr, elt, filteredFormData);
    xhr.send(params);
    return promise;
  }

  /**
   * @typedef {Object} HtmxHistoryUpdate
   * @property {string|null} [type]
   * @property {string|null} [path]
   */

  /**
   * @param {Element} elt
   * @param {HtmxResponseInfo} responseInfo
   * @return {HtmxHistoryUpdate}
   */
  function determineHistoryUpdates(elt, responseInfo) {
    const xhr = responseInfo.xhr;

    //= ==========================================
    // First consult response headers
    //= ==========================================
    let pathFromHeaders = null;
    let typeFromHeaders = null;
    if (hasHeader(xhr, /HX-Push:/i)) {
      pathFromHeaders = xhr.getResponseHeader('HX-Push');
      typeFromHeaders = 'push';
    } else if (hasHeader(xhr, /HX-Push-Url:/i)) {
      pathFromHeaders = xhr.getResponseHeader('HX-Push-Url');
      typeFromHeaders = 'push';
    } else if (hasHeader(xhr, /HX-Replace-Url:/i)) {
      pathFromHeaders = xhr.getResponseHeader('HX-Replace-Url');
      typeFromHeaders = 'replace';
    }

    // if there was a response header, that has priority
    if (pathFromHeaders) {
      if (pathFromHeaders === 'false') {
        return {};
      } else {
        return {
          type: typeFromHeaders,
          path: pathFromHeaders
        };
      }
    }

    //= ==========================================
    // Next resolve via DOM values
    //= ==========================================
    const requestPath = responseInfo.pathInfo.finalRequestPath;
    const responsePath = responseInfo.pathInfo.responsePath;
    const pushUrl = getClosestAttributeValue(elt, 'hx-push-url');
    const replaceUrl = getClosestAttributeValue(elt, 'hx-replace-url');
    const elementIsBoosted = getInternalData(elt).boosted;
    let saveType = null;
    let path = null;
    if (pushUrl) {
      saveType = 'push';
      path = pushUrl;
    } else if (replaceUrl) {
      saveType = 'replace';
      path = replaceUrl;
    } else if (elementIsBoosted) {
      saveType = 'push';
      path = responsePath || requestPath; // if there is no response path, go with the original request path
    }
    if (path) {
      // false indicates no push, return empty object
      if (path === 'false') {
        return {};
      }

      // true indicates we want to follow wherever the server ended up sending us
      if (path === 'true') {
        path = responsePath || requestPath; // if there is no response path, go with the original request path
      }

      // restore any anchor associated with the request
      if (responseInfo.pathInfo.anchor && path.indexOf('#') === -1) {
        path = path + '#' + responseInfo.pathInfo.anchor;
      }
      return {
        type: saveType,
        path
      };
    } else {
      return {};
    }
  }

  /**
   * @param {HtmxResponseHandlingConfig} responseHandlingConfig
   * @param {number} status
   * @return {boolean}
   */
  function codeMatches(responseHandlingConfig, status) {
    var regExp = new RegExp(responseHandlingConfig.code);
    return regExp.test(status.toString(10));
  }

  /**
   * @param {XMLHttpRequest} xhr
   * @return {HtmxResponseHandlingConfig}
   */
  function resolveResponseHandling(xhr) {
    for (var i = 0; i < htmx.config.responseHandling.length; i++) {
      /** @type HtmxResponseHandlingConfig */
      var responseHandlingElement = htmx.config.responseHandling[i];
      if (codeMatches(responseHandlingElement, xhr.status)) {
        return responseHandlingElement;
      }
    }
    // no matches, return no swap
    return {
      swap: false
    };
  }

  /**
   * @param {string} title
   */
  function handleTitle(title) {
    if (title) {
      const titleElt = find('title');
      if (titleElt) {
        titleElt.innerHTML = title;
      } else {
        window.document.title = title;
      }
    }
  }

  /**
   * @param {Element} elt
   * @param {HtmxResponseInfo} responseInfo
   */
  function handleAjaxResponse(elt, responseInfo) {
    const xhr = responseInfo.xhr;
    let target = responseInfo.target;
    const etc = responseInfo.etc;
    const responseInfoSelect = responseInfo.select;
    if (!triggerEvent(elt, 'htmx:beforeOnLoad', responseInfo)) return;
    if (hasHeader(xhr, /HX-Trigger:/i)) {
      handleTriggerHeader(xhr, 'HX-Trigger', elt);
    }
    if (hasHeader(xhr, /HX-Location:/i)) {
      saveCurrentPageToHistory();
      let redirectPath = xhr.getResponseHeader('HX-Location');
      /** @type {HtmxAjaxHelperContext&{path:string}} */
      var redirectSwapSpec;
      if (redirectPath.indexOf('{') === 0) {
        redirectSwapSpec = parseJSON(redirectPath);
        // what's the best way to throw an error if the user didn't include this
        redirectPath = redirectSwapSpec.path;
        delete redirectSwapSpec.path;
      }
      ajaxHelper('get', redirectPath, redirectSwapSpec).then(function () {
        pushUrlIntoHistory(redirectPath);
      });
      return;
    }
    const shouldRefresh = hasHeader(xhr, /HX-Refresh:/i) && xhr.getResponseHeader('HX-Refresh') === 'true';
    if (hasHeader(xhr, /HX-Redirect:/i)) {
      responseInfo.keepIndicators = true;
      location.href = xhr.getResponseHeader('HX-Redirect');
      shouldRefresh && location.reload();
      return;
    }
    if (shouldRefresh) {
      responseInfo.keepIndicators = true;
      location.reload();
      return;
    }
    if (hasHeader(xhr, /HX-Retarget:/i)) {
      if (xhr.getResponseHeader('HX-Retarget') === 'this') {
        responseInfo.target = elt;
      } else {
        responseInfo.target = asElement(querySelectorExt(elt, xhr.getResponseHeader('HX-Retarget')));
      }
    }
    const historyUpdate = determineHistoryUpdates(elt, responseInfo);
    const responseHandling = resolveResponseHandling(xhr);
    const shouldSwap = responseHandling.swap;
    let isError = !!responseHandling.error;
    let ignoreTitle = htmx.config.ignoreTitle || responseHandling.ignoreTitle;
    let selectOverride = responseHandling.select;
    if (responseHandling.target) {
      responseInfo.target = asElement(querySelectorExt(elt, responseHandling.target));
    }
    var swapOverride = etc.swapOverride;
    if (swapOverride == null && responseHandling.swapOverride) {
      swapOverride = responseHandling.swapOverride;
    }

    // response headers override response handling config
    if (hasHeader(xhr, /HX-Retarget:/i)) {
      if (xhr.getResponseHeader('HX-Retarget') === 'this') {
        responseInfo.target = elt;
      } else {
        responseInfo.target = asElement(querySelectorExt(elt, xhr.getResponseHeader('HX-Retarget')));
      }
    }
    if (hasHeader(xhr, /HX-Reswap:/i)) {
      swapOverride = xhr.getResponseHeader('HX-Reswap');
    }
    var serverResponse = xhr.response;
    /** @type HtmxBeforeSwapDetails */
    var beforeSwapDetails = mergeObjects({
      shouldSwap,
      serverResponse,
      isError,
      ignoreTitle,
      selectOverride,
      swapOverride
    }, responseInfo);
    if (responseHandling.event && !triggerEvent(target, responseHandling.event, beforeSwapDetails)) return;
    if (!triggerEvent(target, 'htmx:beforeSwap', beforeSwapDetails)) return;
    target = beforeSwapDetails.target; // allow re-targeting
    serverResponse = beforeSwapDetails.serverResponse; // allow updating content
    isError = beforeSwapDetails.isError; // allow updating error
    ignoreTitle = beforeSwapDetails.ignoreTitle; // allow updating ignoring title
    selectOverride = beforeSwapDetails.selectOverride; // allow updating select override
    swapOverride = beforeSwapDetails.swapOverride; // allow updating swap override

    responseInfo.target = target; // Make updated target available to response events
    responseInfo.failed = isError; // Make failed property available to response events
    responseInfo.successful = !isError; // Make successful property available to response events

    if (beforeSwapDetails.shouldSwap) {
      if (xhr.status === 286) {
        cancelPolling(elt);
      }
      withExtensions(elt, function (extension) {
        serverResponse = extension.transformResponse(serverResponse, xhr, elt);
      });

      // Save current page if there will be a history update
      if (historyUpdate.type) {
        saveCurrentPageToHistory();
      }
      var swapSpec = getSwapSpecification(elt, swapOverride);
      if (!swapSpec.hasOwnProperty('ignoreTitle')) {
        swapSpec.ignoreTitle = ignoreTitle;
      }
      target.classList.add(htmx.config.swappingClass);

      // optional transition API promise callbacks
      let settleResolve = null;
      let settleReject = null;
      if (responseInfoSelect) {
        selectOverride = responseInfoSelect;
      }
      if (hasHeader(xhr, /HX-Reselect:/i)) {
        selectOverride = xhr.getResponseHeader('HX-Reselect');
      }
      const selectOOB = getClosestAttributeValue(elt, 'hx-select-oob');
      const select = getClosestAttributeValue(elt, 'hx-select');
      let doSwap = function () {
        try {
          // if we need to save history, do so, before swapping so that relative resources have the correct base URL
          if (historyUpdate.type) {
            triggerEvent(getDocument().body, 'htmx:beforeHistoryUpdate', mergeObjects({
              history: historyUpdate
            }, responseInfo));
            if (historyUpdate.type === 'push') {
              pushUrlIntoHistory(historyUpdate.path);
              triggerEvent(getDocument().body, 'htmx:pushedIntoHistory', {
                path: historyUpdate.path
              });
            } else {
              replaceUrlInHistory(historyUpdate.path);
              triggerEvent(getDocument().body, 'htmx:replacedInHistory', {
                path: historyUpdate.path
              });
            }
          }
          swap(target, serverResponse, swapSpec, {
            select: selectOverride || select,
            selectOOB,
            eventInfo: responseInfo,
            anchor: responseInfo.pathInfo.anchor,
            contextElement: elt,
            afterSwapCallback: function () {
              if (hasHeader(xhr, /HX-Trigger-After-Swap:/i)) {
                let finalElt = elt;
                if (!bodyContains(elt)) {
                  finalElt = getDocument().body;
                }
                handleTriggerHeader(xhr, 'HX-Trigger-After-Swap', finalElt);
              }
            },
            afterSettleCallback: function () {
              if (hasHeader(xhr, /HX-Trigger-After-Settle:/i)) {
                let finalElt = elt;
                if (!bodyContains(elt)) {
                  finalElt = getDocument().body;
                }
                handleTriggerHeader(xhr, 'HX-Trigger-After-Settle', finalElt);
              }
              maybeCall(settleResolve);
            }
          });
        } catch (e) {
          triggerErrorEvent(elt, 'htmx:swapError', responseInfo);
          maybeCall(settleReject);
          throw e;
        }
      };
      let shouldTransition = htmx.config.globalViewTransitions;
      if (swapSpec.hasOwnProperty('transition')) {
        shouldTransition = swapSpec.transition;
      }
      if (shouldTransition && triggerEvent(elt, 'htmx:beforeTransition', responseInfo) && typeof Promise !== 'undefined' &&
      // @ts-ignore experimental feature atm
      document.startViewTransition) {
        const settlePromise = new Promise(function (_resolve, _reject) {
          settleResolve = _resolve;
          settleReject = _reject;
        });
        // wrap the original doSwap() in a call to startViewTransition()
        const innerDoSwap = doSwap;
        doSwap = function () {
          // @ts-ignore experimental feature atm
          document.startViewTransition(function () {
            innerDoSwap();
            return settlePromise;
          });
        };
      }
      if (swapSpec.swapDelay > 0) {
        getWindow().setTimeout(doSwap, swapSpec.swapDelay);
      } else {
        doSwap();
      }
    }
    if (isError) {
      triggerErrorEvent(elt, 'htmx:responseError', mergeObjects({
        error: 'Response Status Error Code ' + xhr.status + ' from ' + responseInfo.pathInfo.requestPath
      }, responseInfo));
    }
  }

  //= ===================================================================
  // Extensions API
  //= ===================================================================

  /** @type {Object<string, HtmxExtension>} */
  const extensions = {};

  /**
   * extensionBase defines the default functions for all extensions.
   * @returns {HtmxExtension}
   */
  function extensionBase() {
    return {
      init: function (api) {
        return null;
      },
      getSelectors: function () {
        return null;
      },
      onEvent: function (name, evt) {
        return true;
      },
      transformResponse: function (text, xhr, elt) {
        return text;
      },
      isInlineSwap: function (swapStyle) {
        return false;
      },
      handleSwap: function (swapStyle, target, fragment, settleInfo) {
        return false;
      },
      encodeParameters: function (xhr, parameters, elt) {
        return null;
      }
    };
  }

  /**
   * defineExtension initializes the extension and adds it to the htmx registry
   *
   * @see https://htmx.org/api/#defineExtension
   *
   * @param {string} name the extension name
   * @param {Partial<HtmxExtension>} extension the extension definition
   */
  function defineExtension(name, extension) {
    if (extension.init) {
      extension.init(internalAPI);
    }
    extensions[name] = mergeObjects(extensionBase(), extension);
  }

  /**
   * removeExtension removes an extension from the htmx registry
   *
   * @see https://htmx.org/api/#removeExtension
   *
   * @param {string} name
   */
  function removeExtension(name) {
    delete extensions[name];
  }

  /**
   * getExtensions searches up the DOM tree to return all extensions that can be applied to a given element
   *
   * @param {Element} elt
   * @param {HtmxExtension[]=} extensionsToReturn
   * @param {string[]=} extensionsToIgnore
   * @returns {HtmxExtension[]}
   */
  function getExtensions(elt, extensionsToReturn, extensionsToIgnore) {
    if (extensionsToReturn == undefined) {
      extensionsToReturn = [];
    }
    if (elt == undefined) {
      return extensionsToReturn;
    }
    if (extensionsToIgnore == undefined) {
      extensionsToIgnore = [];
    }
    const extensionsForElement = getAttributeValue(elt, 'hx-ext');
    if (extensionsForElement) {
      forEach(extensionsForElement.split(','), function (extensionName) {
        extensionName = extensionName.replace(/ /g, '');
        if (extensionName.slice(0, 7) == 'ignore:') {
          extensionsToIgnore.push(extensionName.slice(7));
          return;
        }
        if (extensionsToIgnore.indexOf(extensionName) < 0) {
          const extension = extensions[extensionName];
          if (extension && extensionsToReturn.indexOf(extension) < 0) {
            extensionsToReturn.push(extension);
          }
        }
      });
    }
    return getExtensions(asElement(parentElt(elt)), extensionsToReturn, extensionsToIgnore);
  }

  //= ===================================================================
  // Initialization
  //= ===================================================================
  var isReady = false;
  getDocument().addEventListener('DOMContentLoaded', function () {
    isReady = true;
  });

  /**
   * Execute a function now if DOMContentLoaded has fired, otherwise listen for it.
   *
   * This function uses isReady because there is no reliable way to ask the browser whether
   * the DOMContentLoaded event has already been fired; there's a gap between DOMContentLoaded
   * firing and readystate=complete.
   */
  function ready(fn) {
    // Checking readyState here is a failsafe in case the htmx script tag entered the DOM by
    // some means other than the initial page load.
    if (isReady || getDocument().readyState === 'complete') {
      fn();
    } else {
      getDocument().addEventListener('DOMContentLoaded', fn);
    }
  }
  function insertIndicatorStyles() {
    if (htmx.config.includeIndicatorStyles !== false) {
      const nonceAttribute = htmx.config.inlineStyleNonce ? ` nonce="${htmx.config.inlineStyleNonce}"` : '';
      getDocument().head.insertAdjacentHTML('beforeend', '<style' + nonceAttribute + '>\
      .' + htmx.config.indicatorClass + '{opacity:0}\
      .' + htmx.config.requestClass + ' .' + htmx.config.indicatorClass + '{opacity:1; transition: opacity 200ms ease-in;}\
      .' + htmx.config.requestClass + '.' + htmx.config.indicatorClass + '{opacity:1; transition: opacity 200ms ease-in;}\
      </style>');
    }
  }
  function getMetaConfig() {
    /** @type HTMLMetaElement */
    const element = getDocument().querySelector('meta[name="htmx-config"]');
    if (element) {
      return parseJSON(element.content);
    } else {
      return null;
    }
  }
  function mergeMetaConfig() {
    const metaConfig = getMetaConfig();
    if (metaConfig) {
      htmx.config = mergeObjects(htmx.config, metaConfig);
    }
  }

  // initialize the document
  ready(function () {
    mergeMetaConfig();
    insertIndicatorStyles();
    let body = getDocument().body;
    processNode(body);
    const restoredElts = getDocument().querySelectorAll("[hx-trigger='restored'],[data-hx-trigger='restored']");
    body.addEventListener('htmx:abort', function (evt) {
      const target = evt.target;
      const internalData = getInternalData(target);
      if (internalData && internalData.xhr) {
        internalData.xhr.abort();
      }
    });
    /** @type {(ev: PopStateEvent) => any} */
    const originalPopstate = window.onpopstate ? window.onpopstate.bind(window) : null;
    /** @type {(ev: PopStateEvent) => any} */
    window.onpopstate = function (event) {
      if (event.state && event.state.htmx) {
        restoreHistory();
        forEach(restoredElts, function (elt) {
          triggerEvent(elt, 'htmx:restored', {
            document: getDocument(),
            triggerEvent
          });
        });
      } else {
        if (originalPopstate) {
          originalPopstate(event);
        }
      }
    };
    getWindow().setTimeout(function () {
      triggerEvent(body, 'htmx:load', {}); // give ready handlers a chance to load up before firing this event
      body = null; // kill reference for gc
    }, 0);
  });
  return htmx;
}();

/** @typedef {'get'|'head'|'post'|'put'|'delete'|'connect'|'options'|'trace'|'patch'} HttpVerb */

/**
 * @typedef {Object} SwapOptions
 * @property {string} [select]
 * @property {string} [selectOOB]
 * @property {*} [eventInfo]
 * @property {string} [anchor]
 * @property {Element} [contextElement]
 * @property {swapCallback} [afterSwapCallback]
 * @property {swapCallback} [afterSettleCallback]
 */

/**
 * @callback swapCallback
 */

/**
 * @typedef {'innerHTML' | 'outerHTML' | 'beforebegin' | 'afterbegin' | 'beforeend' | 'afterend' | 'delete' | 'none' | string} HtmxSwapStyle
 */

/**
 * @typedef HtmxSwapSpecification
 * @property {HtmxSwapStyle} swapStyle
 * @property {number} swapDelay
 * @property {number} settleDelay
 * @property {boolean} [transition]
 * @property {boolean} [ignoreTitle]
 * @property {string} [head]
 * @property {'top' | 'bottom'} [scroll]
 * @property {string} [scrollTarget]
 * @property {string} [show]
 * @property {string} [showTarget]
 * @property {boolean} [focusScroll]
 */

/**
 * @typedef {((this:Node, evt:Event) => boolean) & {source: string}} ConditionalFunction
 */

/**
 * @typedef {Object} HtmxTriggerSpecification
 * @property {string} trigger
 * @property {number} [pollInterval]
 * @property {ConditionalFunction} [eventFilter]
 * @property {boolean} [changed]
 * @property {boolean} [once]
 * @property {boolean} [consume]
 * @property {number} [delay]
 * @property {string} [from]
 * @property {string} [target]
 * @property {number} [throttle]
 * @property {string} [queue]
 * @property {string} [root]
 * @property {string} [threshold]
 */

/**
 * @typedef {{elt: Element, message: string, validity: ValidityState}} HtmxElementValidationError
 */

/**
 * @typedef {Record<string, string>} HtmxHeaderSpecification
 * @property {'true'} HX-Request
 * @property {string|null} HX-Trigger
 * @property {string|null} HX-Trigger-Name
 * @property {string|null} HX-Target
 * @property {string} HX-Current-URL
 * @property {string} [HX-Prompt]
 * @property {'true'} [HX-Boosted]
 * @property {string} [Content-Type]
 * @property {'true'} [HX-History-Restore-Request]
 */

/** @typedef HtmxAjaxHelperContext
 * @property {Element|string} [source]
 * @property {Event} [event]
 * @property {HtmxAjaxHandler} [handler]
 * @property {Element|string} [target]
 * @property {HtmxSwapStyle} [swap]
 * @property {Object|FormData} [values]
 * @property {Record<string,string>} [headers]
 * @property {string} [select]
 */

/**
 * @typedef {Object} HtmxRequestConfig
 * @property {boolean} boosted
 * @property {boolean} useUrlParams
 * @property {FormData} formData
 * @property {Object} parameters formData proxy
 * @property {FormData} unfilteredFormData
 * @property {Object} unfilteredParameters unfilteredFormData proxy
 * @property {HtmxHeaderSpecification} headers
 * @property {Element} target
 * @property {HttpVerb} verb
 * @property {HtmxElementValidationError[]} errors
 * @property {boolean} withCredentials
 * @property {number} timeout
 * @property {string} path
 * @property {Event} triggeringEvent
 */

/**
 * @typedef {Object} HtmxResponseInfo
 * @property {XMLHttpRequest} xhr
 * @property {Element} target
 * @property {HtmxRequestConfig} requestConfig
 * @property {HtmxAjaxEtc} etc
 * @property {boolean} boosted
 * @property {string} select
 * @property {{requestPath: string, finalRequestPath: string, responsePath: string|null, anchor: string}} pathInfo
 * @property {boolean} [failed]
 * @property {boolean} [successful]
 * @property {boolean} [keepIndicators]
 */

/**
 * @typedef {Object} HtmxAjaxEtc
 * @property {boolean} [returnPromise]
 * @property {HtmxAjaxHandler} [handler]
 * @property {string} [select]
 * @property {Element} [targetOverride]
 * @property {HtmxSwapStyle} [swapOverride]
 * @property {Record<string,string>} [headers]
 * @property {Object|FormData} [values]
 * @property {boolean} [credentials]
 * @property {number} [timeout]
 */

/**
 * @typedef {Object} HtmxResponseHandlingConfig
 * @property {string} [code]
 * @property {boolean} swap
 * @property {boolean} [error]
 * @property {boolean} [ignoreTitle]
 * @property {string} [select]
 * @property {string} [target]
 * @property {string} [swapOverride]
 * @property {string} [event]
 */

/**
 * @typedef {HtmxResponseInfo & {shouldSwap: boolean, serverResponse: any, isError: boolean, ignoreTitle: boolean, selectOverride:string, swapOverride:string}} HtmxBeforeSwapDetails
 */

/**
 * @callback HtmxAjaxHandler
 * @param {Element} elt
 * @param {HtmxResponseInfo} responseInfo
 */

/**
 * @typedef {(() => void)} HtmxSettleTask
 */

/**
 * @typedef {Object} HtmxSettleInfo
 * @property {HtmxSettleTask[]} tasks
 * @property {Element[]} elts
 * @property {string} [title]
 */

/**
 * @see https://github.com/bigskysoftware/htmx-extensions/blob/main/README.md
 * @typedef {Object} HtmxExtension
 * @property {(api: any) => void} init
 * @property {(name: string, event: Event|CustomEvent) => boolean} onEvent
 * @property {(text: string, xhr: XMLHttpRequest, elt: Element) => string} transformResponse
 * @property {(swapStyle: HtmxSwapStyle) => boolean} isInlineSwap
 * @property {(swapStyle: HtmxSwapStyle, target: Node, fragment: Node, settleInfo: HtmxSettleInfo) => boolean|Node[]} handleSwap
 * @property {(xhr: XMLHttpRequest, parameters: FormData, elt: Node) => *|string|null} encodeParameters
 * @property {() => string[]|null} getSelectors
 */
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (htmx);

/***/ }),

/***/ "./lara_django/assets/styles/input.css":
/*!*********************************************!*\
  !*** ./lara_django/assets/styles/input.css ***!
  \*********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
// extracted by mini-css-extract-plugin


/***/ })

/******/ 	});
/************************************************************************/
/******/ 	// The module cache
/******/ 	var __webpack_module_cache__ = {};
/******/ 	
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/ 		// Check if module is in cache
/******/ 		var cachedModule = __webpack_module_cache__[moduleId];
/******/ 		if (cachedModule !== undefined) {
/******/ 			return cachedModule.exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = __webpack_module_cache__[moduleId] = {
/******/ 			// no module.id needed
/******/ 			// no module.loaded needed
/******/ 			exports: {}
/******/ 		};
/******/ 	
/******/ 		// Execute the module function
/******/ 		__webpack_modules__[moduleId](module, module.exports, __webpack_require__);
/******/ 	
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/ 	
/************************************************************************/
/******/ 	/* webpack/runtime/define property getters */
/******/ 	(() => {
/******/ 		// define getter functions for harmony exports
/******/ 		__webpack_require__.d = (exports, definition) => {
/******/ 			for(var key in definition) {
/******/ 				if(__webpack_require__.o(definition, key) && !__webpack_require__.o(exports, key)) {
/******/ 					Object.defineProperty(exports, key, { enumerable: true, get: definition[key] });
/******/ 				}
/******/ 			}
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/hasOwnProperty shorthand */
/******/ 	(() => {
/******/ 		__webpack_require__.o = (obj, prop) => (Object.prototype.hasOwnProperty.call(obj, prop))
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/make namespace object */
/******/ 	(() => {
/******/ 		// define __esModule on exports
/******/ 		__webpack_require__.r = (exports) => {
/******/ 			if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 				Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 			}
/******/ 			Object.defineProperty(exports, '__esModule', { value: true });
/******/ 		};
/******/ 	})();
/******/ 	
/************************************************************************/
var __webpack_exports__ = {};
// This entry needs to be wrapped in an IIFE because it needs to be isolated against other modules in the chunk.
(() => {
/*!*********************************************!*\
  !*** ./lara_django/assets/scripts/index.js ***!
  \*********************************************/
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _styles_input_css__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../styles/input.css */ "./lara_django/assets/styles/input.css");

window.htmx = __webpack_require__(/*! htmx.org */ "./node_modules/htmx.org/dist/htmx.esm.js");
window.addEventListener('load', () => {
  console.print('Hello, LARA django!');
});
})();

/******/ })()
;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoianMvbGFyYV9kamFuZ29fY29yZS0zYmRkZWJhNzZlODI2MGQ1NjA0Zi5qcyIsIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7OztBQUFBLElBQUlBLElBQUksR0FBSSxZQUFXO0VBQ3JCLFlBQVk7O0VBRVo7RUFDQSxNQUFNQSxJQUFJLEdBQUc7SUFDWDtJQUNBO0lBQ0E7SUFDQUMsTUFBTSxFQUFFLElBQUk7SUFDWjtJQUNBQyxPQUFPLEVBQUUsSUFBSTtJQUNiO0lBQ0FDLEVBQUUsRUFBRSxJQUFJO0lBQ1I7SUFDQUMsR0FBRyxFQUFFLElBQUk7SUFDVDtJQUNBQyxPQUFPLEVBQUUsSUFBSTtJQUNiO0lBQ0FDLElBQUksRUFBRSxJQUFJO0lBQ1Y7SUFDQTtJQUNBQyxJQUFJLEVBQUUsSUFBSTtJQUNWO0lBQ0FDLE9BQU8sRUFBRSxJQUFJO0lBQ2I7SUFDQUMsT0FBTyxFQUFFLElBQUk7SUFDYjtBQUNKO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7SUFDSUMsTUFBTSxFQUFFLFNBQUFBLENBQVNDLEdBQUcsRUFBRUMsSUFBSSxFQUFFO01BQzFCLE1BQU1DLFdBQVcsR0FBR0MsY0FBYyxDQUFDSCxHQUFHLEVBQUVDLElBQUksSUFBSSxNQUFNLENBQUM7TUFDdkQsT0FBT0MsV0FBVyxDQUFDSCxNQUFNO0lBQzNCLENBQUM7SUFDRDtJQUNBO0lBQ0FLLE1BQU0sRUFBRSxJQUFJO0lBQ1o7SUFDQUMsUUFBUSxFQUFFLElBQUk7SUFDZDtJQUNBQyxXQUFXLEVBQUUsSUFBSTtJQUNqQjtJQUNBQyxXQUFXLEVBQUUsSUFBSTtJQUNqQjtJQUNBQyxTQUFTLEVBQUUsSUFBSTtJQUNmO0lBQ0FDLElBQUksRUFBRSxJQUFJO0lBQ1Y7SUFDQTtJQUNBQyxlQUFlLEVBQUUsSUFBSTtJQUNyQjtJQUNBQyxlQUFlLEVBQUUsSUFBSTtJQUNyQjtJQUNBO0lBQ0FDLE1BQU0sRUFBRSxJQUFJO0lBQ1o7SUFDQUMsT0FBTyxFQUFFLElBQUk7SUFDYjtJQUNBO0FBQ0o7QUFDQTtBQUNBO0FBQ0E7SUFDSUMsTUFBTSxFQUFFLElBQUk7SUFDWjtBQUNKO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtJQUNJQyxNQUFNLEVBQUU7TUFDTjtBQUNOO0FBQ0E7QUFDQTtBQUNBO01BQ01DLGNBQWMsRUFBRSxJQUFJO01BQ3BCO0FBQ047QUFDQTtBQUNBO0FBQ0E7TUFDTUMsZ0JBQWdCLEVBQUUsRUFBRTtNQUNwQjtBQUNOO0FBQ0E7QUFDQTtNQUNNQyxvQkFBb0IsRUFBRSxLQUFLO01BQzNCO0FBQ047QUFDQTtBQUNBO0FBQ0E7TUFDTUMsZ0JBQWdCLEVBQUUsV0FBVztNQUM3QjtBQUNOO0FBQ0E7QUFDQTtBQUNBO01BQ01DLGdCQUFnQixFQUFFLENBQUM7TUFDbkI7QUFDTjtBQUNBO0FBQ0E7QUFDQTtNQUNNQyxrQkFBa0IsRUFBRSxFQUFFO01BQ3RCO0FBQ047QUFDQTtBQUNBO0FBQ0E7TUFDTUMsc0JBQXNCLEVBQUUsSUFBSTtNQUM1QjtBQUNOO0FBQ0E7QUFDQTtBQUNBO01BQ01DLGNBQWMsRUFBRSxnQkFBZ0I7TUFDaEM7QUFDTjtBQUNBO0FBQ0E7QUFDQTtNQUNNQyxZQUFZLEVBQUUsY0FBYztNQUM1QjtBQUNOO0FBQ0E7QUFDQTtBQUNBO01BQ01DLFVBQVUsRUFBRSxZQUFZO01BQ3hCO0FBQ047QUFDQTtBQUNBO0FBQ0E7TUFDTUMsYUFBYSxFQUFFLGVBQWU7TUFDOUI7QUFDTjtBQUNBO0FBQ0E7QUFDQTtNQUNNQyxhQUFhLEVBQUUsZUFBZTtNQUM5QjtBQUNOO0FBQ0E7QUFDQTtBQUNBO01BQ01DLFNBQVMsRUFBRSxJQUFJO01BQ2Y7QUFDTjtBQUNBO0FBQ0E7QUFDQTtNQUNNQyxlQUFlLEVBQUUsSUFBSTtNQUNyQjtBQUNOO0FBQ0E7QUFDQTtBQUNBO01BQ01DLGlCQUFpQixFQUFFLEVBQUU7TUFDckI7QUFDTjtBQUNBO0FBQ0E7QUFDQTtNQUNNQyxnQkFBZ0IsRUFBRSxFQUFFO01BQ3BCO0FBQ047QUFDQTtBQUNBO0FBQ0E7TUFDTUMsa0JBQWtCLEVBQUUsQ0FBQyxPQUFPLEVBQUUsT0FBTyxFQUFFLE9BQU8sRUFBRSxRQUFRLENBQUM7TUFDekQ7QUFDTjtBQUNBO0FBQ0E7QUFDQTtNQUNNQyxlQUFlLEVBQUUsS0FBSztNQUN0QjtBQUNOO0FBQ0E7QUFDQTtNQUNNQyxPQUFPLEVBQUUsQ0FBQztNQUNWO0FBQ047QUFDQTtBQUNBO0FBQ0E7TUFDTUMsZ0JBQWdCLEVBQUUsYUFBYTtNQUMvQjtBQUNOO0FBQ0E7QUFDQTtBQUNBO01BQ01DLFlBQVksRUFBRSxNQUFNO01BQ3BCO0FBQ047QUFDQTtBQUNBO01BQ01DLGVBQWUsRUFBRSxpQ0FBaUM7TUFDbEQ7QUFDTjtBQUNBO0FBQ0E7TUFDTUMsY0FBYyxFQUFFLFNBQVM7TUFDekI7QUFDTjtBQUNBO0FBQ0E7QUFDQTtNQUNNQyxrQkFBa0IsRUFBRSxLQUFLO01BQ3pCO0FBQ047QUFDQTtBQUNBO0FBQ0E7TUFDTUMsbUJBQW1CLEVBQUUsS0FBSztNQUMxQjtBQUNOO0FBQ0E7QUFDQTtBQUNBO01BQ01DLHFCQUFxQixFQUFFLEtBQUs7TUFDNUI7QUFDTjtBQUNBO0FBQ0E7QUFDQTtNQUNNQyx1QkFBdUIsRUFBRSxDQUFDLEtBQUssRUFBRSxRQUFRLENBQUM7TUFDMUM7QUFDTjtBQUNBO0FBQ0E7QUFDQTtNQUNNQyxnQkFBZ0IsRUFBRSxJQUFJO01BQ3RCO0FBQ047QUFDQTtBQUNBO0FBQ0E7TUFDTUMsV0FBVyxFQUFFLEtBQUs7TUFDbEI7QUFDTjtBQUNBO0FBQ0E7QUFDQTtNQUNNQyxxQkFBcUIsRUFBRSxJQUFJO01BQzNCO0FBQ047QUFDQTtBQUNBO0FBQ0E7QUFDQTtNQUNNQyxpQkFBaUIsRUFBRSxJQUFJO01BQ3ZCO01BQ0FDLGtCQUFrQixFQUFFLEtBQUs7TUFDekI7TUFDQUMsZ0JBQWdCLEVBQUUsQ0FDaEI7UUFBRUMsSUFBSSxFQUFFLEtBQUs7UUFBRXhDLElBQUksRUFBRTtNQUFNLENBQUMsRUFDNUI7UUFBRXdDLElBQUksRUFBRSxRQUFRO1FBQUV4QyxJQUFJLEVBQUU7TUFBSyxDQUFDLEVBQzlCO1FBQUV3QyxJQUFJLEVBQUUsUUFBUTtRQUFFeEMsSUFBSSxFQUFFLEtBQUs7UUFBRXlDLEtBQUssRUFBRTtNQUFLLENBQUMsQ0FDN0M7TUFDRDtBQUNOO0FBQ0E7QUFDQTtBQUNBO01BQ01DLG1CQUFtQixFQUFFO0lBQ3ZCLENBQUM7SUFDRDtJQUNBQyxhQUFhLEVBQUUsSUFBSTtJQUNuQjtJQUNBQyxDQUFDLEVBQUUsSUFBSTtJQUNQQyxPQUFPLEVBQUU7RUFDWCxDQUFDO0VBQ0Q7RUFDQWpFLElBQUksQ0FBQ0MsTUFBTSxHQUFHaUUsWUFBWTtFQUMxQmxFLElBQUksQ0FBQ0UsT0FBTyxHQUFHaUUsV0FBVztFQUMxQm5FLElBQUksQ0FBQ0csRUFBRSxHQUFHaUUsb0JBQW9CO0VBQzlCcEUsSUFBSSxDQUFDSSxHQUFHLEdBQUdpRSx1QkFBdUI7RUFDbENyRSxJQUFJLENBQUNLLE9BQU8sR0FBR2lFLFlBQVk7RUFDM0J0RSxJQUFJLENBQUNNLElBQUksR0FBR2lFLFVBQVU7RUFDdEJ2RSxJQUFJLENBQUNPLElBQUksR0FBR0EsSUFBSTtFQUNoQlAsSUFBSSxDQUFDUSxPQUFPLEdBQUdBLE9BQU87RUFDdEJSLElBQUksQ0FBQ1MsT0FBTyxHQUFHQSxPQUFPO0VBQ3RCVCxJQUFJLENBQUNlLE1BQU0sR0FBR3lELGFBQWE7RUFDM0J4RSxJQUFJLENBQUNnQixRQUFRLEdBQUd5RCxpQkFBaUI7RUFDakN6RSxJQUFJLENBQUNpQixXQUFXLEdBQUd5RCxzQkFBc0I7RUFDekMxRSxJQUFJLENBQUNrQixXQUFXLEdBQUd5RCxvQkFBb0I7RUFDdkMzRSxJQUFJLENBQUNtQixTQUFTLEdBQUd5RCxtQkFBbUI7RUFDcEM1RSxJQUFJLENBQUNvQixJQUFJLEdBQUdBLElBQUk7RUFDaEJwQixJQUFJLENBQUNxQixlQUFlLEdBQUdBLGVBQWU7RUFDdENyQixJQUFJLENBQUNzQixlQUFlLEdBQUdBLGVBQWU7RUFDdEN0QixJQUFJLENBQUN1QixNQUFNLEdBQUdBLE1BQU07RUFDcEJ2QixJQUFJLENBQUN3QixPQUFPLEdBQUdBLE9BQU87RUFDdEJ4QixJQUFJLENBQUMrRCxhQUFhLEdBQUdBLGFBQWE7RUFDbEMvRCxJQUFJLENBQUNnRSxDQUFDLEdBQUdhLFlBQVk7RUFFckIsTUFBTUMsV0FBVyxHQUFHO0lBQ2xCQyxpQkFBaUI7SUFDakJDLFlBQVk7SUFDWkMscUJBQXFCO0lBQ3JCQyxlQUFlO0lBQ2ZDLFlBQVk7SUFDWi9ELElBQUk7SUFDSmdFLFlBQVk7SUFDWkMsaUJBQWlCO0lBQ2pCQyx3QkFBd0I7SUFDeEJDLGVBQWU7SUFDZkMsaUJBQWlCO0lBQ2pCQyxVQUFVO0lBQ1YzRSxjQUFjO0lBQ2Q0RSxlQUFlO0lBQ2ZDLG9CQUFvQjtJQUNwQkMsZUFBZTtJQUNmQyxTQUFTO0lBQ1RDLFlBQVk7SUFDWkMsWUFBWTtJQUNaQyxjQUFjO0lBQ2RDLE9BQU87SUFDUEMsZ0JBQWdCO0lBQ2hCQyxpQkFBaUI7SUFDakJDLFlBQVk7SUFDWjlCLFlBQVk7SUFDWitCLGlCQUFpQjtJQUNqQkM7RUFDRixDQUFDO0VBRUQsTUFBTUMsS0FBSyxHQUFHLENBQUMsS0FBSyxFQUFFLE1BQU0sRUFBRSxLQUFLLEVBQUUsUUFBUSxFQUFFLE9BQU8sQ0FBQztFQUN2RCxNQUFNQyxhQUFhLEdBQUdELEtBQUssQ0FBQ0UsR0FBRyxDQUFDLFVBQVNDLElBQUksRUFBRTtJQUM3QyxPQUFPLE1BQU0sR0FBR0EsSUFBSSxHQUFHLGNBQWMsR0FBR0EsSUFBSSxHQUFHLEdBQUc7RUFDcEQsQ0FBQyxDQUFDLENBQUNDLElBQUksQ0FBQyxJQUFJLENBQUM7O0VBRWI7RUFDQTtFQUNBOztFQUVBO0FBQ0Y7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0VBQ0UsU0FBUzVDLGFBQWFBLENBQUM2QyxHQUFHLEVBQUU7SUFDMUIsSUFBSUEsR0FBRyxJQUFJQyxTQUFTLEVBQUU7TUFDcEIsT0FBT0EsU0FBUztJQUNsQjtJQUVBLElBQUlDLFFBQVEsR0FBR0MsR0FBRztJQUNsQixJQUFJSCxHQUFHLENBQUNJLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLElBQUksRUFBRTtNQUN6QkYsUUFBUSxHQUFHRyxVQUFVLENBQUNMLEdBQUcsQ0FBQ0ksS0FBSyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDO0lBQ3pDLENBQUMsTUFBTSxJQUFJSixHQUFHLENBQUNJLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLEdBQUcsRUFBRTtNQUMvQkYsUUFBUSxHQUFHRyxVQUFVLENBQUNMLEdBQUcsQ0FBQ0ksS0FBSyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDLEdBQUcsSUFBSTtJQUNoRCxDQUFDLE1BQU0sSUFBSUosR0FBRyxDQUFDSSxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxHQUFHLEVBQUU7TUFDL0JGLFFBQVEsR0FBR0csVUFBVSxDQUFDTCxHQUFHLENBQUNJLEtBQUssQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxHQUFHLElBQUksR0FBRyxFQUFFO0lBQ3JELENBQUMsTUFBTTtNQUNMRixRQUFRLEdBQUdHLFVBQVUsQ0FBQ0wsR0FBRyxDQUFDO0lBQzVCO0lBQ0EsT0FBT00sS0FBSyxDQUFDSixRQUFRLENBQUMsR0FBR0QsU0FBUyxHQUFHQyxRQUFRO0VBQy9DOztFQUVBO0FBQ0Y7QUFDQTtBQUNBO0FBQ0E7RUFDRSxTQUFTSyxlQUFlQSxDQUFDeEcsR0FBRyxFQUFFeUcsSUFBSSxFQUFFO0lBQ2xDLE9BQU96RyxHQUFHLFlBQVkwRyxPQUFPLElBQUkxRyxHQUFHLENBQUMyRyxZQUFZLENBQUNGLElBQUksQ0FBQztFQUN6RDs7RUFFQTtBQUNGO0FBQ0E7QUFDQTtBQUNBO0VBQ0U7RUFDQSxTQUFTaEMsWUFBWUEsQ0FBQ3pFLEdBQUcsRUFBRTRHLGFBQWEsRUFBRTtJQUN4QyxPQUFPLENBQUMsQ0FBQzVHLEdBQUcsQ0FBQ3lFLFlBQVksS0FBS3pFLEdBQUcsQ0FBQ3lFLFlBQVksQ0FBQ21DLGFBQWEsQ0FBQyxJQUMzRDVHLEdBQUcsQ0FBQ3lFLFlBQVksQ0FBQyxPQUFPLEdBQUdtQyxhQUFhLENBQUMsQ0FBQztFQUM5Qzs7RUFFQTtBQUNGO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7RUFDRSxTQUFTbEMsaUJBQWlCQSxDQUFDMUUsR0FBRyxFQUFFNEcsYUFBYSxFQUFFO0lBQzdDLE9BQU9KLGVBQWUsQ0FBQ3hHLEdBQUcsRUFBRTRHLGFBQWEsQ0FBQyxJQUFJSixlQUFlLENBQUN4RyxHQUFHLEVBQUUsT0FBTyxHQUFHNEcsYUFBYSxDQUFDO0VBQzdGOztFQUVBO0FBQ0Y7QUFDQTtBQUNBO0VBQ0UsU0FBU0MsU0FBU0EsQ0FBQzdHLEdBQUcsRUFBRTtJQUN0QixNQUFNOEcsTUFBTSxHQUFHOUcsR0FBRyxDQUFDK0csYUFBYTtJQUNoQyxJQUFJLENBQUNELE1BQU0sSUFBSTlHLEdBQUcsQ0FBQ2dILFVBQVUsWUFBWUMsVUFBVSxFQUFFLE9BQU9qSCxHQUFHLENBQUNnSCxVQUFVO0lBQzFFLE9BQU9GLE1BQU07RUFDZjs7RUFFQTtBQUNGO0FBQ0E7RUFDRSxTQUFTSSxXQUFXQSxDQUFBLEVBQUc7SUFDckIsT0FBT0MsUUFBUTtFQUNqQjs7RUFFQTtBQUNGO0FBQ0E7QUFDQTtBQUNBO0VBQ0UsU0FBU0MsV0FBV0EsQ0FBQ3BILEdBQUcsRUFBRXFILE1BQU0sRUFBRTtJQUNoQyxPQUFPckgsR0FBRyxDQUFDb0gsV0FBVyxHQUFHcEgsR0FBRyxDQUFDb0gsV0FBVyxDQUFDO01BQUVFLFFBQVEsRUFBRUQ7SUFBTyxDQUFDLENBQUMsR0FBR0gsV0FBVyxDQUFDLENBQUM7RUFDaEY7O0VBRUE7QUFDRjtBQUNBO0FBQ0E7QUFDQTtFQUNFLFNBQVN0QyxlQUFlQSxDQUFDNUUsR0FBRyxFQUFFdUgsU0FBUyxFQUFFO0lBQ3ZDLE9BQU92SCxHQUFHLElBQUksQ0FBQ3VILFNBQVMsQ0FBQ3ZILEdBQUcsQ0FBQyxFQUFFO01BQzdCQSxHQUFHLEdBQUc2RyxTQUFTLENBQUM3RyxHQUFHLENBQUM7SUFDdEI7SUFFQSxPQUFPQSxHQUFHLElBQUksSUFBSTtFQUNwQjs7RUFFQTtBQUNGO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7RUFDRSxTQUFTd0gsbUNBQW1DQSxDQUFDQyxjQUFjLEVBQUVDLFFBQVEsRUFBRUMsYUFBYSxFQUFFO0lBQ3BGLE1BQU1DLGNBQWMsR0FBR2xELGlCQUFpQixDQUFDZ0QsUUFBUSxFQUFFQyxhQUFhLENBQUM7SUFDakUsTUFBTUUsVUFBVSxHQUFHbkQsaUJBQWlCLENBQUNnRCxRQUFRLEVBQUUsZUFBZSxDQUFDO0lBQy9ELElBQUlJLE9BQU8sR0FBR3BELGlCQUFpQixDQUFDZ0QsUUFBUSxFQUFFLFlBQVksQ0FBQztJQUN2RCxJQUFJRCxjQUFjLEtBQUtDLFFBQVEsRUFBRTtNQUMvQixJQUFJckksSUFBSSxDQUFDMEIsTUFBTSxDQUFDZ0Msa0JBQWtCLEVBQUU7UUFDbEMsSUFBSStFLE9BQU8sS0FBS0EsT0FBTyxLQUFLLEdBQUcsSUFBSUEsT0FBTyxDQUFDQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUNDLE9BQU8sQ0FBQ0wsYUFBYSxDQUFDLElBQUksQ0FBQyxDQUFDLEVBQUU7VUFDbEYsT0FBT0MsY0FBYztRQUN2QixDQUFDLE1BQU07VUFDTCxPQUFPLElBQUk7UUFDYjtNQUNGO01BQ0EsSUFBSUMsVUFBVSxLQUFLQSxVQUFVLEtBQUssR0FBRyxJQUFJQSxVQUFVLENBQUNFLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQ0MsT0FBTyxDQUFDTCxhQUFhLENBQUMsSUFBSSxDQUFDLENBQUMsRUFBRTtRQUMzRixPQUFPLE9BQU87TUFDaEI7SUFDRjtJQUNBLE9BQU9DLGNBQWM7RUFDdkI7O0VBRUE7QUFDRjtBQUNBO0FBQ0E7QUFDQTtFQUNFLFNBQVNqRCx3QkFBd0JBLENBQUMzRSxHQUFHLEVBQUUySCxhQUFhLEVBQUU7SUFDcEQsSUFBSU0sV0FBVyxHQUFHLElBQUk7SUFDdEJyRCxlQUFlLENBQUM1RSxHQUFHLEVBQUUsVUFBU2tJLENBQUMsRUFBRTtNQUMvQixPQUFPLENBQUMsRUFBRUQsV0FBVyxHQUFHVCxtQ0FBbUMsQ0FBQ3hILEdBQUcsRUFBRW1JLFNBQVMsQ0FBQ0QsQ0FBQyxDQUFDLEVBQUVQLGFBQWEsQ0FBQyxDQUFDO0lBQ2hHLENBQUMsQ0FBQztJQUNGLElBQUlNLFdBQVcsS0FBSyxPQUFPLEVBQUU7TUFDM0IsT0FBT0EsV0FBVztJQUNwQjtFQUNGOztFQUVBO0FBQ0Y7QUFDQTtBQUNBO0FBQ0E7RUFDRSxTQUFTRyxPQUFPQSxDQUFDcEksR0FBRyxFQUFFcUksUUFBUSxFQUFFO0lBQzlCO0lBQ0E7SUFDQSxNQUFNQyxlQUFlLEdBQUd0SSxHQUFHLFlBQVkwRyxPQUFPLEtBQUsxRyxHQUFHLENBQUNvSSxPQUFPLElBQUlwSSxHQUFHLENBQUN1SSxlQUFlLElBQUl2SSxHQUFHLENBQUN3SSxpQkFBaUIsSUFBSXhJLEdBQUcsQ0FBQ3lJLGtCQUFrQixJQUFJekksR0FBRyxDQUFDMEkscUJBQXFCLElBQUkxSSxHQUFHLENBQUMySSxnQkFBZ0IsQ0FBQztJQUM5TCxPQUFPLENBQUMsQ0FBQ0wsZUFBZSxJQUFJQSxlQUFlLENBQUNNLElBQUksQ0FBQzVJLEdBQUcsRUFBRXFJLFFBQVEsQ0FBQztFQUNqRTs7RUFFQTtBQUNGO0FBQ0E7QUFDQTtFQUNFLFNBQVNRLFdBQVdBLENBQUM1QyxHQUFHLEVBQUU7SUFDeEIsTUFBTTZDLFVBQVUsR0FBRyxnQ0FBZ0M7SUFDbkQsTUFBTUMsS0FBSyxHQUFHRCxVQUFVLENBQUNFLElBQUksQ0FBQy9DLEdBQUcsQ0FBQztJQUNsQyxJQUFJOEMsS0FBSyxFQUFFO01BQ1QsT0FBT0EsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDRSxXQUFXLENBQUMsQ0FBQztJQUMvQixDQUFDLE1BQU07TUFDTCxPQUFPLEVBQUU7SUFDWDtFQUNGOztFQUVBO0FBQ0Y7QUFDQTtBQUNBO0VBQ0UsU0FBU0MsU0FBU0EsQ0FBQ0MsSUFBSSxFQUFFO0lBQ3ZCLE1BQU1DLE1BQU0sR0FBRyxJQUFJQyxTQUFTLENBQUMsQ0FBQztJQUM5QixPQUFPRCxNQUFNLENBQUNFLGVBQWUsQ0FBQ0gsSUFBSSxFQUFFLFdBQVcsQ0FBQztFQUNsRDs7RUFFQTtBQUNGO0FBQ0E7QUFDQTtFQUNFLFNBQVNJLGVBQWVBLENBQUNDLFFBQVEsRUFBRXhKLEdBQUcsRUFBRTtJQUN0QyxPQUFPQSxHQUFHLENBQUN5SixVQUFVLENBQUNDLE1BQU0sR0FBRyxDQUFDLEVBQUU7TUFDaENGLFFBQVEsQ0FBQ0csTUFBTSxDQUFDM0osR0FBRyxDQUFDeUosVUFBVSxDQUFDLENBQUMsQ0FBQyxDQUFDO0lBQ3BDO0VBQ0Y7O0VBRUE7QUFDRjtBQUNBO0FBQ0E7RUFDRSxTQUFTRyxlQUFlQSxDQUFDQyxNQUFNLEVBQUU7SUFDL0IsTUFBTUMsU0FBUyxHQUFHNUMsV0FBVyxDQUFDLENBQUMsQ0FBQzZDLGFBQWEsQ0FBQyxRQUFRLENBQUM7SUFDdkRDLE9BQU8sQ0FBQ0gsTUFBTSxDQUFDSSxVQUFVLEVBQUUsVUFBU0MsSUFBSSxFQUFFO01BQ3hDSixTQUFTLENBQUNLLFlBQVksQ0FBQ0QsSUFBSSxDQUFDekQsSUFBSSxFQUFFeUQsSUFBSSxDQUFDRSxLQUFLLENBQUM7SUFDL0MsQ0FBQyxDQUFDO0lBQ0ZOLFNBQVMsQ0FBQ08sV0FBVyxHQUFHUixNQUFNLENBQUNRLFdBQVc7SUFDMUNQLFNBQVMsQ0FBQ1EsS0FBSyxHQUFHLEtBQUs7SUFDdkIsSUFBSWpMLElBQUksQ0FBQzBCLE1BQU0sQ0FBQ2UsaUJBQWlCLEVBQUU7TUFDakNnSSxTQUFTLENBQUNTLEtBQUssR0FBR2xMLElBQUksQ0FBQzBCLE1BQU0sQ0FBQ2UsaUJBQWlCO0lBQ2pEO0lBQ0EsT0FBT2dJLFNBQVM7RUFDbEI7O0VBRUE7QUFDRjtBQUNBO0FBQ0E7RUFDRSxTQUFTVSxzQkFBc0JBLENBQUNYLE1BQU0sRUFBRTtJQUN0QyxPQUFPQSxNQUFNLENBQUN6QixPQUFPLENBQUMsUUFBUSxDQUFDLEtBQUt5QixNQUFNLENBQUM1SixJQUFJLEtBQUssaUJBQWlCLElBQUk0SixNQUFNLENBQUM1SixJQUFJLEtBQUssUUFBUSxJQUFJNEosTUFBTSxDQUFDNUosSUFBSSxLQUFLLEVBQUUsQ0FBQztFQUMxSDs7RUFFQTtBQUNGO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtFQUNFLFNBQVN3SyxtQkFBbUJBLENBQUNqQixRQUFRLEVBQUU7SUFDckNrQixLQUFLLENBQUNDLElBQUksQ0FBQ25CLFFBQVEsQ0FBQ29CLGdCQUFnQixDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUNaLE9BQU8sQ0FBQyx3Q0FBMENILE1BQU0sSUFBSztNQUMzRyxJQUFJVyxzQkFBc0IsQ0FBQ1gsTUFBTSxDQUFDLEVBQUU7UUFDbEMsTUFBTUMsU0FBUyxHQUFHRixlQUFlLENBQUNDLE1BQU0sQ0FBQztRQUN6QyxNQUFNL0MsTUFBTSxHQUFHK0MsTUFBTSxDQUFDN0MsVUFBVTtRQUNoQyxJQUFJO1VBQ0ZGLE1BQU0sQ0FBQytELFlBQVksQ0FBQ2YsU0FBUyxFQUFFRCxNQUFNLENBQUM7UUFDeEMsQ0FBQyxDQUFDLE9BQU8zQixDQUFDLEVBQUU7VUFDVjRDLFFBQVEsQ0FBQzVDLENBQUMsQ0FBQztRQUNiLENBQUMsU0FBUztVQUNSMkIsTUFBTSxDQUFDekosTUFBTSxDQUFDLENBQUM7UUFDakI7TUFDRjtJQUNGLENBQUMsQ0FBQztFQUNKOztFQUVBO0FBQ0Y7QUFDQTtBQUNBO0FBQ0E7O0VBRUU7QUFDRjtBQUNBO0FBQ0E7RUFDRSxTQUFTK0UsWUFBWUEsQ0FBQzRGLFFBQVEsRUFBRTtJQUM5QjtJQUNBLE1BQU1DLGtCQUFrQixHQUFHRCxRQUFRLENBQUNFLE9BQU8sQ0FBQyxtQ0FBbUMsRUFBRSxFQUFFLENBQUM7SUFDcEYsTUFBTUMsUUFBUSxHQUFHckMsV0FBVyxDQUFDbUMsa0JBQWtCLENBQUM7SUFDaEQ7SUFDQSxJQUFJeEIsUUFBUTtJQUNaLElBQUkwQixRQUFRLEtBQUssTUFBTSxFQUFFO01BQ3ZCO01BQ0ExQixRQUFRLEdBQUcsc0NBQXdDLElBQUkyQixnQkFBZ0IsQ0FBQyxDQUFFO01BQzFFLE1BQU1DLEdBQUcsR0FBR2xDLFNBQVMsQ0FBQzZCLFFBQVEsQ0FBQztNQUMvQnhCLGVBQWUsQ0FBQ0MsUUFBUSxFQUFFNEIsR0FBRyxDQUFDQyxJQUFJLENBQUM7TUFDbkM3QixRQUFRLENBQUM4QixLQUFLLEdBQUdGLEdBQUcsQ0FBQ0UsS0FBSztJQUM1QixDQUFDLE1BQU0sSUFBSUosUUFBUSxLQUFLLE1BQU0sRUFBRTtNQUM5QjtNQUNBMUIsUUFBUSxHQUFHLHNDQUF3QyxJQUFJMkIsZ0JBQWdCLENBQUMsQ0FBRTtNQUMxRSxNQUFNQyxHQUFHLEdBQUdsQyxTQUFTLENBQUM4QixrQkFBa0IsQ0FBQztNQUN6Q3pCLGVBQWUsQ0FBQ0MsUUFBUSxFQUFFNEIsR0FBRyxDQUFDQyxJQUFJLENBQUM7TUFDbkM3QixRQUFRLENBQUM4QixLQUFLLEdBQUdGLEdBQUcsQ0FBQ0UsS0FBSztJQUM1QixDQUFDLE1BQU07TUFDTDtNQUNBLE1BQU1GLEdBQUcsR0FBR2xDLFNBQVMsQ0FBQyxnREFBZ0QsR0FBRzhCLGtCQUFrQixHQUFHLG9CQUFvQixDQUFDO01BQ25IeEIsUUFBUSxHQUFHLHNDQUF3QzRCLEdBQUcsQ0FBQ0csYUFBYSxDQUFDLFVBQVUsQ0FBQyxDQUFDQyxPQUFRO01BQ3pGO01BQ0FoQyxRQUFRLENBQUM4QixLQUFLLEdBQUdGLEdBQUcsQ0FBQ0UsS0FBSzs7TUFFMUI7TUFDQSxJQUFJRyxZQUFZLEdBQUdqQyxRQUFRLENBQUMrQixhQUFhLENBQUMsT0FBTyxDQUFDO01BQ2xELElBQUlFLFlBQVksSUFBSUEsWUFBWSxDQUFDekUsVUFBVSxLQUFLd0MsUUFBUSxFQUFFO1FBQ3hEaUMsWUFBWSxDQUFDckwsTUFBTSxDQUFDLENBQUM7UUFDckJvSixRQUFRLENBQUM4QixLQUFLLEdBQUdHLFlBQVksQ0FBQ0MsU0FBUztNQUN6QztJQUNGO0lBQ0EsSUFBSWxDLFFBQVEsRUFBRTtNQUNaLElBQUluSyxJQUFJLENBQUMwQixNQUFNLENBQUNjLGVBQWUsRUFBRTtRQUMvQjRJLG1CQUFtQixDQUFDakIsUUFBUSxDQUFDO01BQy9CLENBQUMsTUFBTTtRQUNMO1FBQ0FBLFFBQVEsQ0FBQ29CLGdCQUFnQixDQUFDLFFBQVEsQ0FBQyxDQUFDWixPQUFPLENBQUVILE1BQU0sSUFBS0EsTUFBTSxDQUFDekosTUFBTSxDQUFDLENBQUMsQ0FBQztNQUMxRTtJQUNGO0lBQ0EsT0FBT29KLFFBQVE7RUFDakI7O0VBRUE7QUFDRjtBQUNBO0VBQ0UsU0FBU21DLFNBQVNBLENBQUNDLElBQUksRUFBRTtJQUN2QixJQUFJQSxJQUFJLEVBQUU7TUFDUkEsSUFBSSxDQUFDLENBQUM7SUFDUjtFQUNGOztFQUVBO0FBQ0Y7QUFDQTtBQUNBO0FBQ0E7RUFDRSxTQUFTQyxNQUFNQSxDQUFDQyxDQUFDLEVBQUU3TCxJQUFJLEVBQUU7SUFDdkIsT0FBTzhMLE1BQU0sQ0FBQ0MsU0FBUyxDQUFDQyxRQUFRLENBQUNyRCxJQUFJLENBQUNrRCxDQUFDLENBQUMsS0FBSyxVQUFVLEdBQUc3TCxJQUFJLEdBQUcsR0FBRztFQUN0RTs7RUFFQTtBQUNGO0FBQ0E7QUFDQTtFQUNFLFNBQVNpTSxVQUFVQSxDQUFDSixDQUFDLEVBQUU7SUFDckIsT0FBTyxPQUFPQSxDQUFDLEtBQUssVUFBVTtFQUNoQzs7RUFFQTtBQUNGO0FBQ0E7QUFDQTtFQUNFLFNBQVNLLFdBQVdBLENBQUNMLENBQUMsRUFBRTtJQUN0QixPQUFPRCxNQUFNLENBQUNDLENBQUMsRUFBRSxRQUFRLENBQUM7RUFDNUI7O0VBRUE7QUFDRjtBQUNBO0FBQ0E7QUFDQTs7RUFFRTtBQUNGO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0VBRUU7QUFDRjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0VBRUU7QUFDRjtBQUNBO0FBQ0E7QUFDQTtFQUNFLFNBQVMvRyxlQUFlQSxDQUFDL0UsR0FBRyxFQUFFO0lBQzVCLE1BQU1vTSxRQUFRLEdBQUcsb0JBQW9CO0lBQ3JDLElBQUlDLElBQUksR0FBR3JNLEdBQUcsQ0FBQ29NLFFBQVEsQ0FBQztJQUN4QixJQUFJLENBQUNDLElBQUksRUFBRTtNQUNUQSxJQUFJLEdBQUdyTSxHQUFHLENBQUNvTSxRQUFRLENBQUMsR0FBRyxDQUFDLENBQUM7SUFDM0I7SUFDQSxPQUFPQyxJQUFJO0VBQ2I7O0VBRUE7QUFDRjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0VBQ0UsU0FBU0MsT0FBT0EsQ0FBQ0MsR0FBRyxFQUFFO0lBQ3BCLE1BQU1DLFNBQVMsR0FBRyxFQUFFO0lBQ3BCLElBQUlELEdBQUcsRUFBRTtNQUNQLEtBQUssSUFBSUUsQ0FBQyxHQUFHLENBQUMsRUFBRUEsQ0FBQyxHQUFHRixHQUFHLENBQUM3QyxNQUFNLEVBQUUrQyxDQUFDLEVBQUUsRUFBRTtRQUNuQ0QsU0FBUyxDQUFDRSxJQUFJLENBQUNILEdBQUcsQ0FBQ0UsQ0FBQyxDQUFDLENBQUM7TUFDeEI7SUFDRjtJQUNBLE9BQU9ELFNBQVM7RUFDbEI7O0VBRUE7QUFDRjtBQUNBO0FBQ0E7QUFDQTtFQUNFLFNBQVN4QyxPQUFPQSxDQUFDdUMsR0FBRyxFQUFFWCxJQUFJLEVBQUU7SUFDMUIsSUFBSVcsR0FBRyxFQUFFO01BQ1AsS0FBSyxJQUFJRSxDQUFDLEdBQUcsQ0FBQyxFQUFFQSxDQUFDLEdBQUdGLEdBQUcsQ0FBQzdDLE1BQU0sRUFBRStDLENBQUMsRUFBRSxFQUFFO1FBQ25DYixJQUFJLENBQUNXLEdBQUcsQ0FBQ0UsQ0FBQyxDQUFDLENBQUM7TUFDZDtJQUNGO0VBQ0Y7O0VBRUE7QUFDRjtBQUNBO0FBQ0E7RUFDRSxTQUFTRSxrQkFBa0JBLENBQUNDLEVBQUUsRUFBRTtJQUM5QixNQUFNQyxJQUFJLEdBQUdELEVBQUUsQ0FBQ0UscUJBQXFCLENBQUMsQ0FBQztJQUN2QyxNQUFNQyxPQUFPLEdBQUdGLElBQUksQ0FBQ0csR0FBRztJQUN4QixNQUFNQyxVQUFVLEdBQUdKLElBQUksQ0FBQ0ssTUFBTTtJQUM5QixPQUFPSCxPQUFPLEdBQUdJLE1BQU0sQ0FBQ0MsV0FBVyxJQUFJSCxVQUFVLElBQUksQ0FBQztFQUN4RDs7RUFFQTtBQUNGO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtFQUNFLFNBQVM1SSxZQUFZQSxDQUFDckUsR0FBRyxFQUFFO0lBQ3pCLE9BQU9BLEdBQUcsQ0FBQ29ILFdBQVcsQ0FBQztNQUFFRSxRQUFRLEVBQUU7SUFBSyxDQUFDLENBQUMsS0FBS0gsUUFBUTtFQUN6RDs7RUFFQTtBQUNGO0FBQ0E7QUFDQTtFQUNFLFNBQVNrRyxpQkFBaUJBLENBQUMzTixPQUFPLEVBQUU7SUFDbEMsT0FBT0EsT0FBTyxDQUFDNE4sSUFBSSxDQUFDLENBQUMsQ0FBQ3ZGLEtBQUssQ0FBQyxLQUFLLENBQUM7RUFDcEM7O0VBRUE7QUFDRjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0VBQ0UsU0FBUzNDLFlBQVlBLENBQUNtSSxJQUFJLEVBQUVDLElBQUksRUFBRTtJQUNoQyxLQUFLLE1BQU1DLEdBQUcsSUFBSUQsSUFBSSxFQUFFO01BQ3RCLElBQUlBLElBQUksQ0FBQ0UsY0FBYyxDQUFDRCxHQUFHLENBQUMsRUFBRTtRQUM1QjtRQUNBRixJQUFJLENBQUNFLEdBQUcsQ0FBQyxHQUFHRCxJQUFJLENBQUNDLEdBQUcsQ0FBQztNQUN2QjtJQUNGO0lBQ0E7SUFDQSxPQUFPRixJQUFJO0VBQ2I7O0VBRUE7QUFDRjtBQUNBO0FBQ0E7RUFDRSxTQUFTSSxTQUFTQSxDQUFDQyxPQUFPLEVBQUU7SUFDMUIsSUFBSTtNQUNGLE9BQU9DLElBQUksQ0FBQ0MsS0FBSyxDQUFDRixPQUFPLENBQUM7SUFDNUIsQ0FBQyxDQUFDLE9BQU8xSyxLQUFLLEVBQUU7TUFDZDRILFFBQVEsQ0FBQzVILEtBQUssQ0FBQztNQUNmLE9BQU8sSUFBSTtJQUNiO0VBQ0Y7O0VBRUE7QUFDRjtBQUNBO0VBQ0UsU0FBU29CLHFCQUFxQkEsQ0FBQSxFQUFHO0lBQy9CLE1BQU15SixJQUFJLEdBQUcsdUJBQXVCO0lBQ3BDLElBQUk7TUFDRkMsWUFBWSxDQUFDQyxPQUFPLENBQUNGLElBQUksRUFBRUEsSUFBSSxDQUFDO01BQ2hDQyxZQUFZLENBQUNFLFVBQVUsQ0FBQ0gsSUFBSSxDQUFDO01BQzdCLE9BQU8sSUFBSTtJQUNiLENBQUMsQ0FBQyxPQUFPN0YsQ0FBQyxFQUFFO01BQ1YsT0FBTyxLQUFLO0lBQ2Q7RUFDRjs7RUFFQTtBQUNGO0FBQ0E7QUFDQTtFQUNFLFNBQVNpRyxhQUFhQSxDQUFDQyxJQUFJLEVBQUU7SUFDM0IsSUFBSTtNQUNGLE1BQU1DLEdBQUcsR0FBRyxJQUFJQyxHQUFHLENBQUNGLElBQUksQ0FBQztNQUN6QixJQUFJQyxHQUFHLEVBQUU7UUFDUEQsSUFBSSxHQUFHQyxHQUFHLENBQUNFLFFBQVEsR0FBR0YsR0FBRyxDQUFDRyxNQUFNO01BQ2xDO01BQ0E7TUFDQSxJQUFJLENBQUUsTUFBTSxDQUFDVCxJQUFJLENBQUNLLElBQUksQ0FBRSxFQUFFO1FBQ3hCQSxJQUFJLEdBQUdBLElBQUksQ0FBQ25ELE9BQU8sQ0FBQyxNQUFNLEVBQUUsRUFBRSxDQUFDO01BQ2pDO01BQ0EsT0FBT21ELElBQUk7SUFDYixDQUFDLENBQUMsT0FBT2xHLENBQUMsRUFBRTtNQUNWO01BQ0EsT0FBT2tHLElBQUk7SUFDYjtFQUNGOztFQUVBO0VBQ0E7RUFDQTs7RUFFQTtBQUNGO0FBQ0E7QUFDQTtFQUNFLFNBQVNsSyxZQUFZQSxDQUFDK0IsR0FBRyxFQUFFO0lBQ3pCLE9BQU93SSxTQUFTLENBQUN2SCxXQUFXLENBQUMsQ0FBQyxDQUFDbUUsSUFBSSxFQUFFLFlBQVc7TUFDOUMsT0FBT3FELElBQUksQ0FBQ3pJLEdBQUcsQ0FBQztJQUNsQixDQUFDLENBQUM7RUFDSjs7RUFFQTtBQUNGO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0VBQ0UsU0FBUzFDLFlBQVlBLENBQUNvTCxRQUFRLEVBQUU7SUFDOUIsTUFBTXZFLEtBQUssR0FBRy9LLElBQUksQ0FBQ0csRUFBRSxDQUFDLFdBQVcsRUFBRSwrQkFBZ0MsVUFBU29QLEdBQUcsRUFBRTtNQUMvRUQsUUFBUSxDQUFDQyxHQUFHLENBQUNDLE1BQU0sQ0FBQzdPLEdBQUcsQ0FBQztJQUMxQixDQUFDLENBQUM7SUFDRixPQUFPb0ssS0FBSztFQUNkOztFQUVBO0FBQ0Y7QUFDQTtBQUNBO0FBQ0E7RUFDRSxTQUFTeEosTUFBTUEsQ0FBQSxFQUFHO0lBQ2hCdkIsSUFBSSxDQUFDeUIsTUFBTSxHQUFHLFVBQVNkLEdBQUcsRUFBRThPLEtBQUssRUFBRXpDLElBQUksRUFBRTtNQUN2QyxJQUFJMEMsT0FBTyxFQUFFO1FBQ1hBLE9BQU8sQ0FBQ0MsR0FBRyxDQUFDRixLQUFLLEVBQUU5TyxHQUFHLEVBQUVxTSxJQUFJLENBQUM7TUFDL0I7SUFDRixDQUFDO0VBQ0g7RUFFQSxTQUFTeEwsT0FBT0EsQ0FBQSxFQUFHO0lBQ2pCeEIsSUFBSSxDQUFDeUIsTUFBTSxHQUFHLElBQUk7RUFDcEI7O0VBRUE7QUFDRjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0VBQ0UsU0FBU2xCLElBQUlBLENBQUNxUCxhQUFhLEVBQUU1RyxRQUFRLEVBQUU7SUFDckMsSUFBSSxPQUFPNEcsYUFBYSxLQUFLLFFBQVEsRUFBRTtNQUNyQyxPQUFPQSxhQUFhLENBQUMxRCxhQUFhLENBQUNsRCxRQUFRLENBQUM7SUFDOUMsQ0FBQyxNQUFNO01BQ0wsT0FBT3pJLElBQUksQ0FBQ3NILFdBQVcsQ0FBQyxDQUFDLEVBQUUrSCxhQUFhLENBQUM7SUFDM0M7RUFDRjs7RUFFQTtBQUNGO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7RUFDRSxTQUFTcFAsT0FBT0EsQ0FBQ29QLGFBQWEsRUFBRTVHLFFBQVEsRUFBRTtJQUN4QyxJQUFJLE9BQU80RyxhQUFhLEtBQUssUUFBUSxFQUFFO01BQ3JDLE9BQU9BLGFBQWEsQ0FBQ3JFLGdCQUFnQixDQUFDdkMsUUFBUSxDQUFDO0lBQ2pELENBQUMsTUFBTTtNQUNMLE9BQU94SSxPQUFPLENBQUNxSCxXQUFXLENBQUMsQ0FBQyxFQUFFK0gsYUFBYSxDQUFDO0lBQzlDO0VBQ0Y7O0VBRUE7QUFDRjtBQUNBO0VBQ0UsU0FBU0MsU0FBU0EsQ0FBQSxFQUFHO0lBQ25CLE9BQU8vQixNQUFNO0VBQ2Y7O0VBRUE7QUFDRjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtFQUNFLFNBQVN0SixhQUFhQSxDQUFDN0QsR0FBRyxFQUFFbVAsS0FBSyxFQUFFO0lBQ2pDblAsR0FBRyxHQUFHb1AsYUFBYSxDQUFDcFAsR0FBRyxDQUFDO0lBQ3hCLElBQUltUCxLQUFLLEVBQUU7TUFDVEQsU0FBUyxDQUFDLENBQUMsQ0FBQ0csVUFBVSxDQUFDLFlBQVc7UUFDaEN4TCxhQUFhLENBQUM3RCxHQUFHLENBQUM7UUFDbEJBLEdBQUcsR0FBRyxJQUFJO01BQ1osQ0FBQyxFQUFFbVAsS0FBSyxDQUFDO0lBQ1gsQ0FBQyxNQUFNO01BQ0x0SSxTQUFTLENBQUM3RyxHQUFHLENBQUMsQ0FBQ3NQLFdBQVcsQ0FBQ3RQLEdBQUcsQ0FBQztJQUNqQztFQUNGOztFQUVBO0FBQ0Y7QUFDQTtBQUNBO0VBQ0UsU0FBU21JLFNBQVNBLENBQUNuSSxHQUFHLEVBQUU7SUFDdEIsT0FBT0EsR0FBRyxZQUFZMEcsT0FBTyxHQUFHMUcsR0FBRyxHQUFHLElBQUk7RUFDNUM7O0VBRUE7QUFDRjtBQUNBO0FBQ0E7RUFDRSxTQUFTdVAsYUFBYUEsQ0FBQ3ZQLEdBQUcsRUFBRTtJQUMxQixPQUFPQSxHQUFHLFlBQVl3UCxXQUFXLEdBQUd4UCxHQUFHLEdBQUcsSUFBSTtFQUNoRDs7RUFFQTtBQUNGO0FBQ0E7QUFDQTtFQUNFLFNBQVN5UCxRQUFRQSxDQUFDckYsS0FBSyxFQUFFO0lBQ3ZCLE9BQU8sT0FBT0EsS0FBSyxLQUFLLFFBQVEsR0FBR0EsS0FBSyxHQUFHLElBQUk7RUFDakQ7O0VBRUE7QUFDRjtBQUNBO0FBQ0E7RUFDRSxTQUFTc0YsWUFBWUEsQ0FBQzFQLEdBQUcsRUFBRTtJQUN6QixPQUFPQSxHQUFHLFlBQVkwRyxPQUFPLElBQUkxRyxHQUFHLFlBQVkyUCxRQUFRLElBQUkzUCxHQUFHLFlBQVltTCxnQkFBZ0IsR0FBR25MLEdBQUcsR0FBRyxJQUFJO0VBQzFHOztFQUVBO0FBQ0Y7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtFQUNFLFNBQVM4RCxpQkFBaUJBLENBQUM5RCxHQUFHLEVBQUU0UCxLQUFLLEVBQUVULEtBQUssRUFBRTtJQUM1Q25QLEdBQUcsR0FBR21JLFNBQVMsQ0FBQ2lILGFBQWEsQ0FBQ3BQLEdBQUcsQ0FBQyxDQUFDO0lBQ25DLElBQUksQ0FBQ0EsR0FBRyxFQUFFO01BQ1I7SUFDRjtJQUNBLElBQUltUCxLQUFLLEVBQUU7TUFDVEQsU0FBUyxDQUFDLENBQUMsQ0FBQ0csVUFBVSxDQUFDLFlBQVc7UUFDaEN2TCxpQkFBaUIsQ0FBQzlELEdBQUcsRUFBRTRQLEtBQUssQ0FBQztRQUM3QjVQLEdBQUcsR0FBRyxJQUFJO01BQ1osQ0FBQyxFQUFFbVAsS0FBSyxDQUFDO0lBQ1gsQ0FBQyxNQUFNO01BQ0xuUCxHQUFHLENBQUM2UCxTQUFTLElBQUk3UCxHQUFHLENBQUM2UCxTQUFTLENBQUNDLEdBQUcsQ0FBQ0YsS0FBSyxDQUFDO0lBQzNDO0VBQ0Y7O0VBRUE7QUFDRjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0VBQ0UsU0FBUzdMLHNCQUFzQkEsQ0FBQ2dNLElBQUksRUFBRUgsS0FBSyxFQUFFVCxLQUFLLEVBQUU7SUFDbEQsSUFBSW5QLEdBQUcsR0FBR21JLFNBQVMsQ0FBQ2lILGFBQWEsQ0FBQ1csSUFBSSxDQUFDLENBQUM7SUFDeEMsSUFBSSxDQUFDL1AsR0FBRyxFQUFFO01BQ1I7SUFDRjtJQUNBLElBQUltUCxLQUFLLEVBQUU7TUFDVEQsU0FBUyxDQUFDLENBQUMsQ0FBQ0csVUFBVSxDQUFDLFlBQVc7UUFDaEN0TCxzQkFBc0IsQ0FBQy9ELEdBQUcsRUFBRTRQLEtBQUssQ0FBQztRQUNsQzVQLEdBQUcsR0FBRyxJQUFJO01BQ1osQ0FBQyxFQUFFbVAsS0FBSyxDQUFDO0lBQ1gsQ0FBQyxNQUFNO01BQ0wsSUFBSW5QLEdBQUcsQ0FBQzZQLFNBQVMsRUFBRTtRQUNqQjdQLEdBQUcsQ0FBQzZQLFNBQVMsQ0FBQ3pQLE1BQU0sQ0FBQ3dQLEtBQUssQ0FBQztRQUMzQjtRQUNBLElBQUk1UCxHQUFHLENBQUM2UCxTQUFTLENBQUNuRyxNQUFNLEtBQUssQ0FBQyxFQUFFO1VBQzlCMUosR0FBRyxDQUFDZ1EsZUFBZSxDQUFDLE9BQU8sQ0FBQztRQUM5QjtNQUNGO0lBQ0Y7RUFDRjs7RUFFQTtBQUNGO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0VBQ0UsU0FBU2hNLG9CQUFvQkEsQ0FBQ2hFLEdBQUcsRUFBRTRQLEtBQUssRUFBRTtJQUN4QzVQLEdBQUcsR0FBR29QLGFBQWEsQ0FBQ3BQLEdBQUcsQ0FBQztJQUN4QkEsR0FBRyxDQUFDNlAsU0FBUyxDQUFDSSxNQUFNLENBQUNMLEtBQUssQ0FBQztFQUM3Qjs7RUFFQTtBQUNGO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0VBQ0UsU0FBUzNMLG1CQUFtQkEsQ0FBQ2pFLEdBQUcsRUFBRTRQLEtBQUssRUFBRTtJQUN2QzVQLEdBQUcsR0FBR29QLGFBQWEsQ0FBQ3BQLEdBQUcsQ0FBQztJQUN4QmdLLE9BQU8sQ0FBQ2hLLEdBQUcsQ0FBQytHLGFBQWEsQ0FBQ21KLFFBQVEsRUFBRSxVQUFTQyxLQUFLLEVBQUU7TUFDbERwTSxzQkFBc0IsQ0FBQ29NLEtBQUssRUFBRVAsS0FBSyxDQUFDO0lBQ3RDLENBQUMsQ0FBQztJQUNGOUwsaUJBQWlCLENBQUNxRSxTQUFTLENBQUNuSSxHQUFHLENBQUMsRUFBRTRQLEtBQUssQ0FBQztFQUMxQzs7RUFFQTtBQUNGO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7RUFDRSxTQUFTOVAsT0FBT0EsQ0FBQ0UsR0FBRyxFQUFFcUksUUFBUSxFQUFFO0lBQzlCckksR0FBRyxHQUFHbUksU0FBUyxDQUFDaUgsYUFBYSxDQUFDcFAsR0FBRyxDQUFDLENBQUM7SUFDbkMsSUFBSUEsR0FBRyxJQUFJQSxHQUFHLENBQUNGLE9BQU8sRUFBRTtNQUN0QixPQUFPRSxHQUFHLENBQUNGLE9BQU8sQ0FBQ3VJLFFBQVEsQ0FBQztJQUM5QixDQUFDLE1BQU07TUFDTDtNQUNBLEdBQUc7UUFDRCxJQUFJckksR0FBRyxJQUFJLElBQUksSUFBSW9JLE9BQU8sQ0FBQ3BJLEdBQUcsRUFBRXFJLFFBQVEsQ0FBQyxFQUFFO1VBQ3pDLE9BQU9ySSxHQUFHO1FBQ1o7TUFDRixDQUFDLFFBQ01BLEdBQUcsR0FBR0EsR0FBRyxJQUFJbUksU0FBUyxDQUFDdEIsU0FBUyxDQUFDN0csR0FBRyxDQUFDLENBQUM7TUFDN0MsT0FBTyxJQUFJO0lBQ2I7RUFDRjs7RUFFQTtBQUNGO0FBQ0E7QUFDQTtBQUNBO0VBQ0UsU0FBU29RLFVBQVVBLENBQUNuSyxHQUFHLEVBQUVvSyxNQUFNLEVBQUU7SUFDL0IsT0FBT3BLLEdBQUcsQ0FBQ3FLLFNBQVMsQ0FBQyxDQUFDLEVBQUVELE1BQU0sQ0FBQzNHLE1BQU0sQ0FBQyxLQUFLMkcsTUFBTTtFQUNuRDs7RUFFQTtBQUNGO0FBQ0E7QUFDQTtBQUNBO0VBQ0UsU0FBU0UsUUFBUUEsQ0FBQ3RLLEdBQUcsRUFBRXVLLE1BQU0sRUFBRTtJQUM3QixPQUFPdkssR0FBRyxDQUFDcUssU0FBUyxDQUFDckssR0FBRyxDQUFDeUQsTUFBTSxHQUFHOEcsTUFBTSxDQUFDOUcsTUFBTSxDQUFDLEtBQUs4RyxNQUFNO0VBQzdEOztFQUVBO0FBQ0Y7QUFDQTtBQUNBO0VBQ0UsU0FBU0MsaUJBQWlCQSxDQUFDcEksUUFBUSxFQUFFO0lBQ25DLE1BQU1xSSxlQUFlLEdBQUdySSxRQUFRLENBQUNpRixJQUFJLENBQUMsQ0FBQztJQUN2QyxJQUFJOEMsVUFBVSxDQUFDTSxlQUFlLEVBQUUsR0FBRyxDQUFDLElBQUlILFFBQVEsQ0FBQ0csZUFBZSxFQUFFLElBQUksQ0FBQyxFQUFFO01BQ3ZFLE9BQU9BLGVBQWUsQ0FBQ0osU0FBUyxDQUFDLENBQUMsRUFBRUksZUFBZSxDQUFDaEgsTUFBTSxHQUFHLENBQUMsQ0FBQztJQUNqRSxDQUFDLE1BQU07TUFDTCxPQUFPZ0gsZUFBZTtJQUN4QjtFQUNGOztFQUVBO0FBQ0Y7QUFDQTtBQUNBO0FBQ0E7QUFDQTtFQUNFLFNBQVNDLG1CQUFtQkEsQ0FBQzNRLEdBQUcsRUFBRXFJLFFBQVEsRUFBRWhCLE1BQU0sRUFBRTtJQUNsRCxJQUFJZ0IsUUFBUSxDQUFDTCxPQUFPLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxFQUFFO01BQ3JDLE9BQU8ySSxtQkFBbUIsQ0FBQzNRLEdBQUcsRUFBRXFJLFFBQVEsQ0FBQ2hDLEtBQUssQ0FBQyxDQUFDLENBQUMsRUFBRSxJQUFJLENBQUM7SUFDMUQ7SUFFQXJHLEdBQUcsR0FBR29QLGFBQWEsQ0FBQ3BQLEdBQUcsQ0FBQztJQUV4QixNQUFNNFEsS0FBSyxHQUFHLEVBQUU7SUFDaEI7TUFDRSxJQUFJQyxhQUFhLEdBQUcsQ0FBQztNQUNyQixJQUFJQyxNQUFNLEdBQUcsQ0FBQztNQUNkLEtBQUssSUFBSXJFLENBQUMsR0FBRyxDQUFDLEVBQUVBLENBQUMsR0FBR3BFLFFBQVEsQ0FBQ3FCLE1BQU0sRUFBRStDLENBQUMsRUFBRSxFQUFFO1FBQ3hDLE1BQU1zRSxJQUFJLEdBQUcxSSxRQUFRLENBQUNvRSxDQUFDLENBQUM7UUFDeEIsSUFBSXNFLElBQUksS0FBSyxHQUFHLElBQUlGLGFBQWEsS0FBSyxDQUFDLEVBQUU7VUFDdkNELEtBQUssQ0FBQ2xFLElBQUksQ0FBQ3JFLFFBQVEsQ0FBQ2lJLFNBQVMsQ0FBQ1EsTUFBTSxFQUFFckUsQ0FBQyxDQUFDLENBQUM7VUFDekNxRSxNQUFNLEdBQUdyRSxDQUFDLEdBQUcsQ0FBQztVQUNkO1FBQ0Y7UUFDQSxJQUFJc0UsSUFBSSxLQUFLLEdBQUcsRUFBRTtVQUNoQkYsYUFBYSxFQUFFO1FBQ2pCLENBQUMsTUFBTSxJQUFJRSxJQUFJLEtBQUssR0FBRyxJQUFJdEUsQ0FBQyxHQUFHcEUsUUFBUSxDQUFDcUIsTUFBTSxHQUFHLENBQUMsSUFBSXJCLFFBQVEsQ0FBQ29FLENBQUMsR0FBRyxDQUFDLENBQUMsS0FBSyxHQUFHLEVBQUU7VUFDN0VvRSxhQUFhLEVBQUU7UUFDakI7TUFDRjtNQUNBLElBQUlDLE1BQU0sR0FBR3pJLFFBQVEsQ0FBQ3FCLE1BQU0sRUFBRTtRQUM1QmtILEtBQUssQ0FBQ2xFLElBQUksQ0FBQ3JFLFFBQVEsQ0FBQ2lJLFNBQVMsQ0FBQ1EsTUFBTSxDQUFDLENBQUM7TUFDeEM7SUFDRjtJQUVBLE1BQU1FLE1BQU0sR0FBRyxFQUFFO0lBQ2pCLE1BQU1DLGdCQUFnQixHQUFHLEVBQUU7SUFDM0IsT0FBT0wsS0FBSyxDQUFDbEgsTUFBTSxHQUFHLENBQUMsRUFBRTtNQUN2QixNQUFNckIsUUFBUSxHQUFHb0ksaUJBQWlCLENBQUNHLEtBQUssQ0FBQ00sS0FBSyxDQUFDLENBQUMsQ0FBQztNQUNqRCxJQUFJQyxJQUFJO01BQ1IsSUFBSTlJLFFBQVEsQ0FBQ0wsT0FBTyxDQUFDLFVBQVUsQ0FBQyxLQUFLLENBQUMsRUFBRTtRQUN0Q21KLElBQUksR0FBR3JSLE9BQU8sQ0FBQ3FJLFNBQVMsQ0FBQ25JLEdBQUcsQ0FBQyxFQUFFeVEsaUJBQWlCLENBQUNwSSxRQUFRLENBQUMrSSxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztNQUN2RSxDQUFDLE1BQU0sSUFBSS9JLFFBQVEsQ0FBQ0wsT0FBTyxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsRUFBRTtRQUMxQ21KLElBQUksR0FBR3ZSLElBQUksQ0FBQzhQLFlBQVksQ0FBQzFQLEdBQUcsQ0FBQyxFQUFFeVEsaUJBQWlCLENBQUNwSSxRQUFRLENBQUMrSSxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztNQUN2RSxDQUFDLE1BQU0sSUFBSS9JLFFBQVEsS0FBSyxNQUFNLElBQUlBLFFBQVEsS0FBSyxvQkFBb0IsRUFBRTtRQUNuRThJLElBQUksR0FBR2hKLFNBQVMsQ0FBQ25JLEdBQUcsQ0FBQyxDQUFDcVIsa0JBQWtCO01BQzFDLENBQUMsTUFBTSxJQUFJaEosUUFBUSxDQUFDTCxPQUFPLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxFQUFFO1FBQzFDbUosSUFBSSxHQUFHRyxnQkFBZ0IsQ0FBQ3RSLEdBQUcsRUFBRXlRLGlCQUFpQixDQUFDcEksUUFBUSxDQUFDK0ksTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDL0osTUFBTSxDQUFDO01BQy9FLENBQUMsTUFBTSxJQUFJZ0IsUUFBUSxLQUFLLFVBQVUsSUFBSUEsUUFBUSxLQUFLLHdCQUF3QixFQUFFO1FBQzNFOEksSUFBSSxHQUFHaEosU0FBUyxDQUFDbkksR0FBRyxDQUFDLENBQUN1UixzQkFBc0I7TUFDOUMsQ0FBQyxNQUFNLElBQUlsSixRQUFRLENBQUNMLE9BQU8sQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDLEVBQUU7UUFDOUNtSixJQUFJLEdBQUdLLGtCQUFrQixDQUFDeFIsR0FBRyxFQUFFeVEsaUJBQWlCLENBQUNwSSxRQUFRLENBQUMrSSxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMvSixNQUFNLENBQUM7TUFDakYsQ0FBQyxNQUFNLElBQUlnQixRQUFRLEtBQUssVUFBVSxFQUFFO1FBQ2xDOEksSUFBSSxHQUFHaEssUUFBUTtNQUNqQixDQUFDLE1BQU0sSUFBSWtCLFFBQVEsS0FBSyxRQUFRLEVBQUU7UUFDaEM4SSxJQUFJLEdBQUdoRSxNQUFNO01BQ2YsQ0FBQyxNQUFNLElBQUk5RSxRQUFRLEtBQUssTUFBTSxFQUFFO1FBQzlCOEksSUFBSSxHQUFHaEssUUFBUSxDQUFDa0UsSUFBSTtNQUN0QixDQUFDLE1BQU0sSUFBSWhELFFBQVEsS0FBSyxNQUFNLEVBQUU7UUFDOUI4SSxJQUFJLEdBQUcvSixXQUFXLENBQUNwSCxHQUFHLEVBQUUsQ0FBQyxDQUFDcUgsTUFBTSxDQUFDO01BQ25DLENBQUMsTUFBTSxJQUFJZ0IsUUFBUSxLQUFLLE1BQU0sRUFBRTtRQUM5QjhJLElBQUksR0FBRyxDQUFDLHVCQUF3Qm5SLEdBQUcsQ0FBQ29ILFdBQVcsQ0FBQyxDQUFDLEVBQUdxSyxJQUFJO01BQzFELENBQUMsTUFBTTtRQUNMUixnQkFBZ0IsQ0FBQ3ZFLElBQUksQ0FBQ3JFLFFBQVEsQ0FBQztNQUNqQztNQUVBLElBQUk4SSxJQUFJLEVBQUU7UUFDUkgsTUFBTSxDQUFDdEUsSUFBSSxDQUFDeUUsSUFBSSxDQUFDO01BQ25CO0lBQ0Y7SUFFQSxJQUFJRixnQkFBZ0IsQ0FBQ3ZILE1BQU0sR0FBRyxDQUFDLEVBQUU7TUFDL0IsTUFBTWdJLGdCQUFnQixHQUFHVCxnQkFBZ0IsQ0FBQ2pMLElBQUksQ0FBQyxHQUFHLENBQUM7TUFDbkQsTUFBTTJMLFFBQVEsR0FBR2pDLFlBQVksQ0FBQ3RJLFdBQVcsQ0FBQ3BILEdBQUcsRUFBRSxDQUFDLENBQUNxSCxNQUFNLENBQUMsQ0FBQztNQUN6RDJKLE1BQU0sQ0FBQ3RFLElBQUksQ0FBQyxHQUFHSixPQUFPLENBQUNxRixRQUFRLENBQUMvRyxnQkFBZ0IsQ0FBQzhHLGdCQUFnQixDQUFDLENBQUMsQ0FBQztJQUN0RTtJQUVBLE9BQU9WLE1BQU07RUFDZjs7RUFFQTtBQUNGO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7RUFDRSxJQUFJTSxnQkFBZ0IsR0FBRyxTQUFBQSxDQUFTTSxLQUFLLEVBQUU3SSxLQUFLLEVBQUUxQixNQUFNLEVBQUU7SUFDcEQsTUFBTXdLLE9BQU8sR0FBR25DLFlBQVksQ0FBQ3RJLFdBQVcsQ0FBQ3dLLEtBQUssRUFBRXZLLE1BQU0sQ0FBQyxDQUFDLENBQUN1RCxnQkFBZ0IsQ0FBQzdCLEtBQUssQ0FBQztJQUNoRixLQUFLLElBQUkwRCxDQUFDLEdBQUcsQ0FBQyxFQUFFQSxDQUFDLEdBQUdvRixPQUFPLENBQUNuSSxNQUFNLEVBQUUrQyxDQUFDLEVBQUUsRUFBRTtNQUN2QyxNQUFNek0sR0FBRyxHQUFHNlIsT0FBTyxDQUFDcEYsQ0FBQyxDQUFDO01BQ3RCLElBQUl6TSxHQUFHLENBQUM4Uix1QkFBdUIsQ0FBQ0YsS0FBSyxDQUFDLEtBQUtHLElBQUksQ0FBQ0MsMkJBQTJCLEVBQUU7UUFDM0UsT0FBT2hTLEdBQUc7TUFDWjtJQUNGO0VBQ0YsQ0FBQzs7RUFFRDtBQUNGO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7RUFDRSxJQUFJd1Isa0JBQWtCLEdBQUcsU0FBQUEsQ0FBU0ksS0FBSyxFQUFFN0ksS0FBSyxFQUFFMUIsTUFBTSxFQUFFO0lBQ3RELE1BQU13SyxPQUFPLEdBQUduQyxZQUFZLENBQUN0SSxXQUFXLENBQUN3SyxLQUFLLEVBQUV2SyxNQUFNLENBQUMsQ0FBQyxDQUFDdUQsZ0JBQWdCLENBQUM3QixLQUFLLENBQUM7SUFDaEYsS0FBSyxJQUFJMEQsQ0FBQyxHQUFHb0YsT0FBTyxDQUFDbkksTUFBTSxHQUFHLENBQUMsRUFBRStDLENBQUMsSUFBSSxDQUFDLEVBQUVBLENBQUMsRUFBRSxFQUFFO01BQzVDLE1BQU16TSxHQUFHLEdBQUc2UixPQUFPLENBQUNwRixDQUFDLENBQUM7TUFDdEIsSUFBSXpNLEdBQUcsQ0FBQzhSLHVCQUF1QixDQUFDRixLQUFLLENBQUMsS0FBS0csSUFBSSxDQUFDRSwyQkFBMkIsRUFBRTtRQUMzRSxPQUFPalMsR0FBRztNQUNaO0lBQ0Y7RUFDRixDQUFDOztFQUVEO0FBQ0Y7QUFDQTtBQUNBO0FBQ0E7RUFDRSxTQUFTdUYsZ0JBQWdCQSxDQUFDMEosYUFBYSxFQUFFNUcsUUFBUSxFQUFFO0lBQ2pELElBQUksT0FBTzRHLGFBQWEsS0FBSyxRQUFRLEVBQUU7TUFDckMsT0FBTzBCLG1CQUFtQixDQUFDMUIsYUFBYSxFQUFFNUcsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDO0lBQ3hELENBQUMsTUFBTTtNQUNMLE9BQU9zSSxtQkFBbUIsQ0FBQ3pKLFdBQVcsQ0FBQyxDQUFDLENBQUNtRSxJQUFJLEVBQUU0RCxhQUFhLENBQUMsQ0FBQyxDQUFDLENBQUM7SUFDbEU7RUFDRjs7RUFFQTtBQUNGO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7RUFDRSxTQUFTRyxhQUFhQSxDQUFDSCxhQUFhLEVBQUVpRCxPQUFPLEVBQUU7SUFDN0MsSUFBSSxPQUFPakQsYUFBYSxLQUFLLFFBQVEsRUFBRTtNQUNyQyxPQUFPclAsSUFBSSxDQUFDOFAsWUFBWSxDQUFDd0MsT0FBTyxDQUFDLElBQUkvSyxRQUFRLEVBQUU4SCxhQUFhLENBQUM7SUFDL0QsQ0FBQyxNQUFNO01BQ0wsT0FBT0EsYUFBYTtJQUN0QjtFQUNGOztFQUVBO0FBQ0Y7QUFDQTs7RUFFRTtBQUNGO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7RUFFRTtBQUNGO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtFQUNFLFNBQVNrRCxnQkFBZ0JBLENBQUNDLElBQUksRUFBRUMsSUFBSSxFQUFFQyxJQUFJLEVBQUVDLElBQUksRUFBRTtJQUNoRCxJQUFJckcsVUFBVSxDQUFDbUcsSUFBSSxDQUFDLEVBQUU7TUFDcEIsT0FBTztRQUNMRyxNQUFNLEVBQUV0TCxXQUFXLENBQUMsQ0FBQyxDQUFDbUUsSUFBSTtRQUMxQnlELEtBQUssRUFBRVcsUUFBUSxDQUFDMkMsSUFBSSxDQUFDO1FBQ3JCSyxRQUFRLEVBQUVKLElBQUk7UUFDZEssT0FBTyxFQUFFSjtNQUNYLENBQUM7SUFDSCxDQUFDLE1BQU07TUFDTCxPQUFPO1FBQ0xFLE1BQU0sRUFBRXBELGFBQWEsQ0FBQ2dELElBQUksQ0FBQztRQUMzQnRELEtBQUssRUFBRVcsUUFBUSxDQUFDNEMsSUFBSSxDQUFDO1FBQ3JCSSxRQUFRLEVBQUVILElBQUk7UUFDZEksT0FBTyxFQUFFSDtNQUNYLENBQUM7SUFDSDtFQUNGOztFQUVBO0FBQ0Y7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7RUFDRSxTQUFTOU8sb0JBQW9CQSxDQUFDMk8sSUFBSSxFQUFFQyxJQUFJLEVBQUVDLElBQUksRUFBRUMsSUFBSSxFQUFFO0lBQ3BESSxLQUFLLENBQUMsWUFBVztNQUNmLE1BQU1DLFNBQVMsR0FBR1QsZ0JBQWdCLENBQUNDLElBQUksRUFBRUMsSUFBSSxFQUFFQyxJQUFJLEVBQUVDLElBQUksQ0FBQztNQUMxREssU0FBUyxDQUFDSixNQUFNLENBQUNLLGdCQUFnQixDQUFDRCxTQUFTLENBQUM5RCxLQUFLLEVBQUU4RCxTQUFTLENBQUNILFFBQVEsRUFBRUcsU0FBUyxDQUFDRixPQUFPLENBQUM7SUFDM0YsQ0FBQyxDQUFDO0lBQ0YsTUFBTUksQ0FBQyxHQUFHNUcsVUFBVSxDQUFDbUcsSUFBSSxDQUFDO0lBQzFCLE9BQU9TLENBQUMsR0FBR1QsSUFBSSxHQUFHQyxJQUFJO0VBQ3hCOztFQUVBO0FBQ0Y7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0VBQ0UsU0FBUzVPLHVCQUF1QkEsQ0FBQzBPLElBQUksRUFBRUMsSUFBSSxFQUFFQyxJQUFJLEVBQUU7SUFDakRLLEtBQUssQ0FBQyxZQUFXO01BQ2YsTUFBTUMsU0FBUyxHQUFHVCxnQkFBZ0IsQ0FBQ0MsSUFBSSxFQUFFQyxJQUFJLEVBQUVDLElBQUksQ0FBQztNQUNwRE0sU0FBUyxDQUFDSixNQUFNLENBQUNPLG1CQUFtQixDQUFDSCxTQUFTLENBQUM5RCxLQUFLLEVBQUU4RCxTQUFTLENBQUNILFFBQVEsQ0FBQztJQUMzRSxDQUFDLENBQUM7SUFDRixPQUFPdkcsVUFBVSxDQUFDbUcsSUFBSSxDQUFDLEdBQUdBLElBQUksR0FBR0MsSUFBSTtFQUN2Qzs7RUFFQTtFQUNBO0VBQ0E7O0VBRUEsTUFBTVUsU0FBUyxHQUFHOUwsV0FBVyxDQUFDLENBQUMsQ0FBQzZDLGFBQWEsQ0FBQyxRQUFRLENBQUMsRUFBQztFQUN4RDtBQUNGO0FBQ0E7QUFDQTtBQUNBO0VBQ0UsU0FBU2tKLG9CQUFvQkEsQ0FBQ2pULEdBQUcsRUFBRWtULFFBQVEsRUFBRTtJQUMzQyxNQUFNQyxVQUFVLEdBQUd4Tyx3QkFBd0IsQ0FBQzNFLEdBQUcsRUFBRWtULFFBQVEsQ0FBQztJQUMxRCxJQUFJQyxVQUFVLEVBQUU7TUFDZCxJQUFJQSxVQUFVLEtBQUssTUFBTSxFQUFFO1FBQ3pCLE9BQU8sQ0FBQzVPLGVBQWUsQ0FBQ3ZFLEdBQUcsRUFBRWtULFFBQVEsQ0FBQyxDQUFDO01BQ3pDLENBQUMsTUFBTTtRQUNMLE1BQU1sQyxNQUFNLEdBQUdMLG1CQUFtQixDQUFDM1EsR0FBRyxFQUFFbVQsVUFBVSxDQUFDO1FBQ25ELElBQUluQyxNQUFNLENBQUN0SCxNQUFNLEtBQUssQ0FBQyxFQUFFO1VBQ3ZCb0IsUUFBUSxDQUFDLGdCQUFnQixHQUFHcUksVUFBVSxHQUFHLE9BQU8sR0FBR0QsUUFBUSxHQUFHLHVCQUF1QixDQUFDO1VBQ3RGLE9BQU8sQ0FBQ0YsU0FBUyxDQUFDO1FBQ3BCLENBQUMsTUFBTTtVQUNMLE9BQU9oQyxNQUFNO1FBQ2Y7TUFDRjtJQUNGO0VBQ0Y7O0VBRUE7QUFDRjtBQUNBO0FBQ0E7QUFDQTtFQUNFLFNBQVN6TSxlQUFlQSxDQUFDdkUsR0FBRyxFQUFFb1QsU0FBUyxFQUFFO0lBQ3ZDLE9BQU9qTCxTQUFTLENBQUN2RCxlQUFlLENBQUM1RSxHQUFHLEVBQUUsVUFBU0EsR0FBRyxFQUFFO01BQ2xELE9BQU8wRSxpQkFBaUIsQ0FBQ3lELFNBQVMsQ0FBQ25JLEdBQUcsQ0FBQyxFQUFFb1QsU0FBUyxDQUFDLElBQUksSUFBSTtJQUM3RCxDQUFDLENBQUMsQ0FBQztFQUNMOztFQUVBO0FBQ0Y7QUFDQTtBQUNBO0VBQ0UsU0FBU2xPLFNBQVNBLENBQUNsRixHQUFHLEVBQUU7SUFDdEIsTUFBTXFULFNBQVMsR0FBRzFPLHdCQUF3QixDQUFDM0UsR0FBRyxFQUFFLFdBQVcsQ0FBQztJQUM1RCxJQUFJcVQsU0FBUyxFQUFFO01BQ2IsSUFBSUEsU0FBUyxLQUFLLE1BQU0sRUFBRTtRQUN4QixPQUFPOU8sZUFBZSxDQUFDdkUsR0FBRyxFQUFFLFdBQVcsQ0FBQztNQUMxQyxDQUFDLE1BQU07UUFDTCxPQUFPdUYsZ0JBQWdCLENBQUN2RixHQUFHLEVBQUVxVCxTQUFTLENBQUM7TUFDekM7SUFDRixDQUFDLE1BQU07TUFDTCxNQUFNaEgsSUFBSSxHQUFHdEgsZUFBZSxDQUFDL0UsR0FBRyxDQUFDO01BQ2pDLElBQUlxTSxJQUFJLENBQUNpSCxPQUFPLEVBQUU7UUFDaEIsT0FBT3BNLFdBQVcsQ0FBQyxDQUFDLENBQUNtRSxJQUFJO01BQzNCLENBQUMsTUFBTTtRQUNMLE9BQU9yTCxHQUFHO01BQ1o7SUFDRjtFQUNGOztFQUVBO0FBQ0Y7QUFDQTtBQUNBO0VBQ0UsU0FBU3VULHFCQUFxQkEsQ0FBQzlNLElBQUksRUFBRTtJQUNuQyxNQUFNekUsa0JBQWtCLEdBQUczQyxJQUFJLENBQUMwQixNQUFNLENBQUNpQixrQkFBa0I7SUFDekQsS0FBSyxJQUFJeUssQ0FBQyxHQUFHLENBQUMsRUFBRUEsQ0FBQyxHQUFHekssa0JBQWtCLENBQUMwSCxNQUFNLEVBQUUrQyxDQUFDLEVBQUUsRUFBRTtNQUNsRCxJQUFJaEcsSUFBSSxLQUFLekUsa0JBQWtCLENBQUN5SyxDQUFDLENBQUMsRUFBRTtRQUNsQyxPQUFPLElBQUk7TUFDYjtJQUNGO0lBQ0EsT0FBTyxLQUFLO0VBQ2Q7O0VBRUE7QUFDRjtBQUNBO0FBQ0E7RUFDRSxTQUFTK0csZUFBZUEsQ0FBQ0MsT0FBTyxFQUFFQyxTQUFTLEVBQUU7SUFDM0MxSixPQUFPLENBQUN5SixPQUFPLENBQUN4SixVQUFVLEVBQUUsVUFBU0MsSUFBSSxFQUFFO01BQ3pDLElBQUksQ0FBQ3dKLFNBQVMsQ0FBQ2pQLFlBQVksQ0FBQ3lGLElBQUksQ0FBQ3pELElBQUksQ0FBQyxJQUFJOE0scUJBQXFCLENBQUNySixJQUFJLENBQUN6RCxJQUFJLENBQUMsRUFBRTtRQUMxRWdOLE9BQU8sQ0FBQ3pELGVBQWUsQ0FBQzlGLElBQUksQ0FBQ3pELElBQUksQ0FBQztNQUNwQztJQUNGLENBQUMsQ0FBQztJQUNGdUQsT0FBTyxDQUFDMEosU0FBUyxDQUFDekosVUFBVSxFQUFFLFVBQVNDLElBQUksRUFBRTtNQUMzQyxJQUFJcUoscUJBQXFCLENBQUNySixJQUFJLENBQUN6RCxJQUFJLENBQUMsRUFBRTtRQUNwQ2dOLE9BQU8sQ0FBQ3RKLFlBQVksQ0FBQ0QsSUFBSSxDQUFDekQsSUFBSSxFQUFFeUQsSUFBSSxDQUFDRSxLQUFLLENBQUM7TUFDN0M7SUFDRixDQUFDLENBQUM7RUFDSjs7RUFFQTtBQUNGO0FBQ0E7QUFDQTtBQUNBO0VBQ0UsU0FBU3VKLFlBQVlBLENBQUNDLFNBQVMsRUFBRXBCLE1BQU0sRUFBRTtJQUN2QyxNQUFNcUIsVUFBVSxHQUFHQyxhQUFhLENBQUN0QixNQUFNLENBQUM7SUFDeEMsS0FBSyxJQUFJL0YsQ0FBQyxHQUFHLENBQUMsRUFBRUEsQ0FBQyxHQUFHb0gsVUFBVSxDQUFDbkssTUFBTSxFQUFFK0MsQ0FBQyxFQUFFLEVBQUU7TUFDMUMsTUFBTXNILFNBQVMsR0FBR0YsVUFBVSxDQUFDcEgsQ0FBQyxDQUFDO01BQy9CLElBQUk7UUFDRixJQUFJc0gsU0FBUyxDQUFDSixZQUFZLENBQUNDLFNBQVMsQ0FBQyxFQUFFO1VBQ3JDLE9BQU8sSUFBSTtRQUNiO01BQ0YsQ0FBQyxDQUFDLE9BQU8xTCxDQUFDLEVBQUU7UUFDVjRDLFFBQVEsQ0FBQzVDLENBQUMsQ0FBQztNQUNiO0lBQ0Y7SUFDQSxPQUFPMEwsU0FBUyxLQUFLLFdBQVc7RUFDbEM7O0VBRUE7QUFDRjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7RUFDRSxTQUFTdE8sT0FBT0EsQ0FBQzBPLFFBQVEsRUFBRUMsVUFBVSxFQUFFQyxVQUFVLEVBQUV2QyxRQUFRLEVBQUU7SUFDM0RBLFFBQVEsR0FBR0EsUUFBUSxJQUFJekssV0FBVyxDQUFDLENBQUM7SUFDcEMsSUFBSW1CLFFBQVEsR0FBRyxHQUFHLEdBQUc3QixlQUFlLENBQUN5TixVQUFVLEVBQUUsSUFBSSxDQUFDO0lBQ3REO0lBQ0EsSUFBSUwsU0FBUyxHQUFHLFdBQVc7SUFDM0IsSUFBSUksUUFBUSxLQUFLLE1BQU0sRUFBRTtNQUN2QjtJQUFBLENBQ0QsTUFBTSxJQUFJQSxRQUFRLENBQUNoTSxPQUFPLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxFQUFFO01BQ3BDNEwsU0FBUyxHQUFHSSxRQUFRLENBQUMxRCxTQUFTLENBQUMsQ0FBQyxFQUFFMEQsUUFBUSxDQUFDaE0sT0FBTyxDQUFDLEdBQUcsQ0FBQyxDQUFDO01BQ3hESyxRQUFRLEdBQUcyTCxRQUFRLENBQUMxRCxTQUFTLENBQUMwRCxRQUFRLENBQUNoTSxPQUFPLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxDQUFDO0lBQzFELENBQUMsTUFBTTtNQUNMNEwsU0FBUyxHQUFHSSxRQUFRO0lBQ3RCO0lBQ0FDLFVBQVUsQ0FBQ2pFLGVBQWUsQ0FBQyxhQUFhLENBQUM7SUFDekNpRSxVQUFVLENBQUNqRSxlQUFlLENBQUMsa0JBQWtCLENBQUM7SUFFOUMsTUFBTW1FLE9BQU8sR0FBR3hELG1CQUFtQixDQUFDZ0IsUUFBUSxFQUFFdEosUUFBUSxFQUFFLEtBQUssQ0FBQztJQUM5RCxJQUFJOEwsT0FBTyxFQUFFO01BQ1huSyxPQUFPLENBQ0xtSyxPQUFPLEVBQ1AsVUFBUzNCLE1BQU0sRUFBRTtRQUNmLElBQUloSixRQUFRO1FBQ1osTUFBTTRLLGVBQWUsR0FBR0gsVUFBVSxDQUFDSSxTQUFTLENBQUMsSUFBSSxDQUFDO1FBQ2xEN0ssUUFBUSxHQUFHdEMsV0FBVyxDQUFDLENBQUMsQ0FBQ29OLHNCQUFzQixDQUFDLENBQUM7UUFDakQ5SyxRQUFRLENBQUMrSyxXQUFXLENBQUNILGVBQWUsQ0FBQztRQUNyQyxJQUFJLENBQUNULFlBQVksQ0FBQ0MsU0FBUyxFQUFFcEIsTUFBTSxDQUFDLEVBQUU7VUFDcENoSixRQUFRLEdBQUdrRyxZQUFZLENBQUMwRSxlQUFlLENBQUMsRUFBQztRQUMzQztRQUVBLE1BQU1JLGlCQUFpQixHQUFHO1VBQUVDLFVBQVUsRUFBRSxJQUFJO1VBQUVqQyxNQUFNO1VBQUVoSjtRQUFTLENBQUM7UUFDaEUsSUFBSSxDQUFDN0YsWUFBWSxDQUFDNk8sTUFBTSxFQUFFLG9CQUFvQixFQUFFZ0MsaUJBQWlCLENBQUMsRUFBRTtRQUVwRWhDLE1BQU0sR0FBR2dDLGlCQUFpQixDQUFDaEMsTUFBTSxFQUFDO1FBQ2xDLElBQUlnQyxpQkFBaUIsQ0FBQ0MsVUFBVSxFQUFFO1VBQ2hDQyx1QkFBdUIsQ0FBQ2xMLFFBQVEsQ0FBQztVQUNqQ21MLGFBQWEsQ0FBQ2YsU0FBUyxFQUFFcEIsTUFBTSxFQUFFQSxNQUFNLEVBQUVoSixRQUFRLEVBQUUwSyxVQUFVLENBQUM7VUFDOURVLHdCQUF3QixDQUFDLENBQUM7UUFDNUI7UUFDQTVLLE9BQU8sQ0FBQ2tLLFVBQVUsQ0FBQ1csSUFBSSxFQUFFLFVBQVM3VSxHQUFHLEVBQUU7VUFDckMyRCxZQUFZLENBQUMzRCxHQUFHLEVBQUUsbUJBQW1CLEVBQUV3VSxpQkFBaUIsQ0FBQztRQUMzRCxDQUFDLENBQUM7TUFDSixDQUNGLENBQUM7TUFDRFAsVUFBVSxDQUFDak4sVUFBVSxDQUFDc0ksV0FBVyxDQUFDMkUsVUFBVSxDQUFDO0lBQy9DLENBQUMsTUFBTTtNQUNMQSxVQUFVLENBQUNqTixVQUFVLENBQUNzSSxXQUFXLENBQUMyRSxVQUFVLENBQUM7TUFDN0N2TyxpQkFBaUIsQ0FBQ3dCLFdBQVcsQ0FBQyxDQUFDLENBQUNtRSxJQUFJLEVBQUUsdUJBQXVCLEVBQUU7UUFBRUcsT0FBTyxFQUFFeUk7TUFBVyxDQUFDLENBQUM7SUFDekY7SUFDQSxPQUFPRCxRQUFRO0VBQ2pCO0VBRUEsU0FBU1ksd0JBQXdCQSxDQUFBLEVBQUc7SUFDbEMsTUFBTUUsTUFBTSxHQUFHbFYsSUFBSSxDQUFDLDJCQUEyQixDQUFDO0lBQ2hELElBQUlrVixNQUFNLEVBQUU7TUFDVixLQUFLLE1BQU1DLFlBQVksSUFBSSxDQUFDLEdBQUdELE1BQU0sQ0FBQzVFLFFBQVEsQ0FBQyxFQUFFO1FBQy9DLE1BQU04RSxlQUFlLEdBQUdwVixJQUFJLENBQUMsR0FBRyxHQUFHbVYsWUFBWSxDQUFDRSxFQUFFLENBQUM7UUFDbkQ7UUFDQUQsZUFBZSxDQUFDaE8sVUFBVSxDQUFDa08sVUFBVSxDQUFDSCxZQUFZLEVBQUVDLGVBQWUsQ0FBQztRQUNwRUEsZUFBZSxDQUFDNVUsTUFBTSxDQUFDLENBQUM7TUFDMUI7TUFDQTBVLE1BQU0sQ0FBQzFVLE1BQU0sQ0FBQyxDQUFDO0lBQ2pCO0VBQ0Y7O0VBRUE7QUFDRjtBQUNBO0VBQ0UsU0FBU3NVLHVCQUF1QkEsQ0FBQ2xMLFFBQVEsRUFBRTtJQUN6Q1EsT0FBTyxDQUFDbkssT0FBTyxDQUFDMkosUUFBUSxFQUFFLG1DQUFtQyxDQUFDLEVBQUUsVUFBU3VMLFlBQVksRUFBRTtNQUNyRixNQUFNRSxFQUFFLEdBQUd2USxpQkFBaUIsQ0FBQ3FRLFlBQVksRUFBRSxJQUFJLENBQUM7TUFDaEQsTUFBTUMsZUFBZSxHQUFHOU4sV0FBVyxDQUFDLENBQUMsQ0FBQ2lPLGNBQWMsQ0FBQ0YsRUFBRSxDQUFDO01BQ3hELElBQUlELGVBQWUsSUFBSSxJQUFJLEVBQUU7UUFDM0IsSUFBSUQsWUFBWSxDQUFDRyxVQUFVLEVBQUU7VUFBRTtVQUM3QjtVQUNBLElBQUlKLE1BQU0sR0FBR2xWLElBQUksQ0FBQywyQkFBMkIsQ0FBQztVQUM5QyxJQUFJa1YsTUFBTSxJQUFJLElBQUksRUFBRTtZQUNsQjVOLFdBQVcsQ0FBQyxDQUFDLENBQUNtRSxJQUFJLENBQUMrSixrQkFBa0IsQ0FBQyxVQUFVLEVBQUUsMkNBQTJDLENBQUM7WUFDOUZOLE1BQU0sR0FBR2xWLElBQUksQ0FBQywyQkFBMkIsQ0FBQztVQUM1QztVQUNBO1VBQ0FrVixNQUFNLENBQUNJLFVBQVUsQ0FBQ0YsZUFBZSxFQUFFLElBQUksQ0FBQztRQUMxQyxDQUFDLE1BQU07VUFDTEQsWUFBWSxDQUFDL04sVUFBVSxDQUFDcU8sWUFBWSxDQUFDTCxlQUFlLEVBQUVELFlBQVksQ0FBQztRQUNyRTtNQUNGO0lBQ0YsQ0FBQyxDQUFDO0VBQ0o7O0VBRUE7QUFDRjtBQUNBO0FBQ0E7QUFDQTtFQUNFLFNBQVNPLGdCQUFnQkEsQ0FBQ3RPLFVBQVUsRUFBRXdDLFFBQVEsRUFBRTBLLFVBQVUsRUFBRTtJQUMxRGxLLE9BQU8sQ0FBQ1IsUUFBUSxDQUFDb0IsZ0JBQWdCLENBQUMsTUFBTSxDQUFDLEVBQUUsVUFBUzJLLE9BQU8sRUFBRTtNQUMzRCxNQUFNTixFQUFFLEdBQUd6TyxlQUFlLENBQUMrTyxPQUFPLEVBQUUsSUFBSSxDQUFDO01BQ3pDLElBQUlOLEVBQUUsSUFBSUEsRUFBRSxDQUFDdkwsTUFBTSxHQUFHLENBQUMsRUFBRTtRQUN2QixNQUFNOEwsWUFBWSxHQUFHUCxFQUFFLENBQUNoSyxPQUFPLENBQUMsR0FBRyxFQUFFLEtBQUssQ0FBQztRQUMzQyxNQUFNd0ssYUFBYSxHQUFHRixPQUFPLENBQUNHLE9BQU8sQ0FBQ3pLLE9BQU8sQ0FBQyxHQUFHLEVBQUUsS0FBSyxDQUFDO1FBQ3pELE1BQU1wRSxTQUFTLEdBQUc2SSxZQUFZLENBQUMxSSxVQUFVLENBQUM7UUFDMUMsTUFBTTJPLE9BQU8sR0FBRzlPLFNBQVMsSUFBSUEsU0FBUyxDQUFDMEUsYUFBYSxDQUFDa0ssYUFBYSxHQUFHLE9BQU8sR0FBR0QsWUFBWSxHQUFHLElBQUksQ0FBQztRQUNuRyxJQUFJRyxPQUFPLElBQUlBLE9BQU8sS0FBSzlPLFNBQVMsRUFBRTtVQUNwQyxNQUFNK08sYUFBYSxHQUFHTCxPQUFPLENBQUNsQixTQUFTLENBQUMsQ0FBQztVQUN6Q2IsZUFBZSxDQUFDK0IsT0FBTyxFQUFFSSxPQUFPLENBQUM7VUFDakN6QixVQUFVLENBQUMyQixLQUFLLENBQUNuSixJQUFJLENBQUMsWUFBVztZQUMvQjhHLGVBQWUsQ0FBQytCLE9BQU8sRUFBRUssYUFBYSxDQUFDO1VBQ3pDLENBQUMsQ0FBQztRQUNKO01BQ0Y7SUFDRixDQUFDLENBQUM7RUFDSjs7RUFFQTtBQUNGO0FBQ0E7QUFDQTtFQUNFLFNBQVNFLGdCQUFnQkEsQ0FBQzNGLEtBQUssRUFBRTtJQUMvQixPQUFPLFlBQVc7TUFDaEJwTSxzQkFBc0IsQ0FBQ29NLEtBQUssRUFBRTlRLElBQUksQ0FBQzBCLE1BQU0sQ0FBQ1UsVUFBVSxDQUFDO01BQ3JEK0IsV0FBVyxDQUFDMkUsU0FBUyxDQUFDZ0ksS0FBSyxDQUFDLENBQUM7TUFDN0I0RixZQUFZLENBQUNyRyxZQUFZLENBQUNTLEtBQUssQ0FBQyxDQUFDO01BQ2pDeE0sWUFBWSxDQUFDd00sS0FBSyxFQUFFLFdBQVcsQ0FBQztJQUNsQyxDQUFDO0VBQ0g7O0VBRUE7QUFDRjtBQUNBO0VBQ0UsU0FBUzRGLFlBQVlBLENBQUM1RixLQUFLLEVBQUU7SUFDM0IsTUFBTTZGLFNBQVMsR0FBRyxhQUFhO0lBQy9CLE1BQU1DLGNBQWMsR0FBRzFHLGFBQWEsQ0FBQ25ILE9BQU8sQ0FBQytILEtBQUssRUFBRTZGLFNBQVMsQ0FBQyxHQUFHN0YsS0FBSyxHQUFHQSxLQUFLLENBQUM1RSxhQUFhLENBQUN5SyxTQUFTLENBQUMsQ0FBQztJQUN4RyxJQUFJQyxjQUFjLElBQUksSUFBSSxFQUFFO01BQzFCQSxjQUFjLENBQUNDLEtBQUssQ0FBQyxDQUFDO0lBQ3hCO0VBQ0Y7O0VBRUE7QUFDRjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0VBQ0UsU0FBU0MsaUJBQWlCQSxDQUFDblAsVUFBVSxFQUFFNkQsWUFBWSxFQUFFckIsUUFBUSxFQUFFMEssVUFBVSxFQUFFO0lBQ3pFb0IsZ0JBQWdCLENBQUN0TyxVQUFVLEVBQUV3QyxRQUFRLEVBQUUwSyxVQUFVLENBQUM7SUFDbEQsT0FBTzFLLFFBQVEsQ0FBQ0MsVUFBVSxDQUFDQyxNQUFNLEdBQUcsQ0FBQyxFQUFFO01BQ3JDLE1BQU15RyxLQUFLLEdBQUczRyxRQUFRLENBQUM0TSxVQUFVO01BQ2pDdFMsaUJBQWlCLENBQUNxRSxTQUFTLENBQUNnSSxLQUFLLENBQUMsRUFBRTlRLElBQUksQ0FBQzBCLE1BQU0sQ0FBQ1UsVUFBVSxDQUFDO01BQzNEdUYsVUFBVSxDQUFDNkQsWUFBWSxDQUFDc0YsS0FBSyxFQUFFdEYsWUFBWSxDQUFDO01BQzVDLElBQUlzRixLQUFLLENBQUNrRyxRQUFRLEtBQUt0RSxJQUFJLENBQUN1RSxTQUFTLElBQUluRyxLQUFLLENBQUNrRyxRQUFRLEtBQUt0RSxJQUFJLENBQUN3RSxZQUFZLEVBQUU7UUFDN0VyQyxVQUFVLENBQUMyQixLQUFLLENBQUNuSixJQUFJLENBQUNvSixnQkFBZ0IsQ0FBQzNGLEtBQUssQ0FBQyxDQUFDO01BQ2hEO0lBQ0Y7RUFDRjs7RUFFQTtBQUNGO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtFQUNFLFNBQVNxRyxVQUFVQSxDQUFDQyxNQUFNLEVBQUVDLElBQUksRUFBRTtJQUNoQyxJQUFJM0YsSUFBSSxHQUFHLENBQUM7SUFDWixPQUFPQSxJQUFJLEdBQUcwRixNQUFNLENBQUMvTSxNQUFNLEVBQUU7TUFDM0JnTixJQUFJLEdBQUcsQ0FBQ0EsSUFBSSxJQUFJLENBQUMsSUFBSUEsSUFBSSxHQUFHRCxNQUFNLENBQUNFLFVBQVUsQ0FBQzVGLElBQUksRUFBRSxDQUFDLEdBQUcsQ0FBQyxFQUFDO0lBQzVEO0lBQ0EsT0FBTzJGLElBQUk7RUFDYjs7RUFFQTtBQUNGO0FBQ0E7QUFDQTtFQUNFLFNBQVNFLGFBQWFBLENBQUM1VyxHQUFHLEVBQUU7SUFDMUIsSUFBSTBXLElBQUksR0FBRyxDQUFDO0lBQ1o7SUFDQSxJQUFJMVcsR0FBRyxDQUFDaUssVUFBVSxFQUFFO01BQ2xCLEtBQUssSUFBSXdDLENBQUMsR0FBRyxDQUFDLEVBQUVBLENBQUMsR0FBR3pNLEdBQUcsQ0FBQ2lLLFVBQVUsQ0FBQ1AsTUFBTSxFQUFFK0MsQ0FBQyxFQUFFLEVBQUU7UUFDOUMsTUFBTTJHLFNBQVMsR0FBR3BULEdBQUcsQ0FBQ2lLLFVBQVUsQ0FBQ3dDLENBQUMsQ0FBQztRQUNuQyxJQUFJMkcsU0FBUyxDQUFDaEosS0FBSyxFQUFFO1VBQUU7VUFDckJzTSxJQUFJLEdBQUdGLFVBQVUsQ0FBQ3BELFNBQVMsQ0FBQzNNLElBQUksRUFBRWlRLElBQUksQ0FBQztVQUN2Q0EsSUFBSSxHQUFHRixVQUFVLENBQUNwRCxTQUFTLENBQUNoSixLQUFLLEVBQUVzTSxJQUFJLENBQUM7UUFDMUM7TUFDRjtJQUNGO0lBQ0EsT0FBT0EsSUFBSTtFQUNiOztFQUVBO0FBQ0Y7QUFDQTtFQUNFLFNBQVNHLGdCQUFnQkEsQ0FBQzdXLEdBQUcsRUFBRTtJQUM3QixNQUFNOFcsWUFBWSxHQUFHL1IsZUFBZSxDQUFDL0UsR0FBRyxDQUFDO0lBQ3pDLElBQUk4VyxZQUFZLENBQUNDLFVBQVUsRUFBRTtNQUMzQixLQUFLLElBQUl0SyxDQUFDLEdBQUcsQ0FBQyxFQUFFQSxDQUFDLEdBQUdxSyxZQUFZLENBQUNDLFVBQVUsQ0FBQ3JOLE1BQU0sRUFBRStDLENBQUMsRUFBRSxFQUFFO1FBQ3ZELE1BQU11SyxXQUFXLEdBQUdGLFlBQVksQ0FBQ0MsVUFBVSxDQUFDdEssQ0FBQyxDQUFDO1FBQzlDL0ksdUJBQXVCLENBQUMxRCxHQUFHLEVBQUVnWCxXQUFXLENBQUNsSSxLQUFLLEVBQUVrSSxXQUFXLENBQUN2RSxRQUFRLENBQUM7TUFDdkU7TUFDQSxPQUFPcUUsWUFBWSxDQUFDQyxVQUFVO0lBQ2hDO0VBQ0Y7O0VBRUE7QUFDRjtBQUNBO0VBQ0UsU0FBU0UsVUFBVUEsQ0FBQ0MsT0FBTyxFQUFFO0lBQzNCLE1BQU1KLFlBQVksR0FBRy9SLGVBQWUsQ0FBQ21TLE9BQU8sQ0FBQztJQUM3QyxJQUFJSixZQUFZLENBQUM1VSxPQUFPLEVBQUU7TUFDeEJpVixZQUFZLENBQUNMLFlBQVksQ0FBQzVVLE9BQU8sQ0FBQztJQUNwQztJQUNBLElBQUk0VSxZQUFZLENBQUNNLGFBQWEsRUFBRTtNQUM5QnBOLE9BQU8sQ0FBQzhNLFlBQVksQ0FBQ00sYUFBYSxFQUFFLFVBQVNDLElBQUksRUFBRTtRQUNqRCxJQUFJQSxJQUFJLENBQUM3WCxFQUFFLEVBQUU7VUFDWGtFLHVCQUF1QixDQUFDMlQsSUFBSSxDQUFDN1gsRUFBRSxFQUFFNlgsSUFBSSxDQUFDM1gsT0FBTyxFQUFFMlgsSUFBSSxDQUFDNUUsUUFBUSxDQUFDO1FBQy9EO01BQ0YsQ0FBQyxDQUFDO0lBQ0o7SUFDQW9FLGdCQUFnQixDQUFDSyxPQUFPLENBQUM7SUFDekJsTixPQUFPLENBQUMrQixNQUFNLENBQUN1TCxJQUFJLENBQUNSLFlBQVksQ0FBQyxFQUFFLFVBQVNySixHQUFHLEVBQUU7TUFBRSxJQUFJQSxHQUFHLEtBQUssb0JBQW9CLEVBQUUsT0FBT3FKLFlBQVksQ0FBQ3JKLEdBQUcsQ0FBQztJQUFDLENBQUMsQ0FBQztFQUNsSDs7RUFFQTtBQUNGO0FBQ0E7RUFDRSxTQUFTOEosY0FBY0EsQ0FBQ0wsT0FBTyxFQUFFO0lBQy9CdlQsWUFBWSxDQUFDdVQsT0FBTyxFQUFFLDJCQUEyQixDQUFDO0lBQ2xERCxVQUFVLENBQUNDLE9BQU8sQ0FBQztJQUNuQjtJQUNBO0lBQ0EsSUFBSUEsT0FBTyxDQUFDaEgsUUFBUSxFQUFFO01BQUU7TUFDdEI7TUFDQWxHLE9BQU8sQ0FBQ2tOLE9BQU8sQ0FBQ2hILFFBQVEsRUFBRSxVQUFTQyxLQUFLLEVBQUU7UUFBRW9ILGNBQWMsQ0FBQ3BILEtBQUssQ0FBQztNQUFDLENBQUMsQ0FBQztJQUN0RTtFQUNGOztFQUVBO0FBQ0Y7QUFDQTtBQUNBO0FBQ0E7RUFDRSxTQUFTcUgsYUFBYUEsQ0FBQ2hGLE1BQU0sRUFBRWhKLFFBQVEsRUFBRTBLLFVBQVUsRUFBRTtJQUNuRCxJQUFJMUIsTUFBTSxZQUFZOUwsT0FBTyxJQUFJOEwsTUFBTSxDQUFDa0QsT0FBTyxLQUFLLE1BQU0sRUFBRTtNQUFFO01BQzVELE9BQU8rQixhQUFhLENBQUNqRixNQUFNLEVBQUVoSixRQUFRLEVBQUUwSyxVQUFVLENBQUM7SUFDcEQ7SUFDQTtJQUNBLElBQUl3RCxNQUFNO0lBQ1YsTUFBTUMsbUJBQW1CLEdBQUduRixNQUFNLENBQUNvRixlQUFlO0lBQ2xELE1BQU01USxVQUFVLEdBQUdILFNBQVMsQ0FBQzJMLE1BQU0sQ0FBQztJQUNwQyxJQUFJLENBQUN4TCxVQUFVLEVBQUU7TUFBRTtNQUNqQjtJQUNGO0lBQ0FtUCxpQkFBaUIsQ0FBQ25QLFVBQVUsRUFBRXdMLE1BQU0sRUFBRWhKLFFBQVEsRUFBRTBLLFVBQVUsQ0FBQztJQUMzRCxJQUFJeUQsbUJBQW1CLElBQUksSUFBSSxFQUFFO01BQy9CRCxNQUFNLEdBQUcxUSxVQUFVLENBQUNvUCxVQUFVO0lBQ2hDLENBQUMsTUFBTTtNQUNMc0IsTUFBTSxHQUFHQyxtQkFBbUIsQ0FBQ0UsV0FBVztJQUMxQztJQUNBM0QsVUFBVSxDQUFDVyxJQUFJLEdBQUdYLFVBQVUsQ0FBQ1csSUFBSSxDQUFDaUQsTUFBTSxDQUFDLFVBQVM1UCxDQUFDLEVBQUU7TUFBRSxPQUFPQSxDQUFDLEtBQUtzSyxNQUFNO0lBQUMsQ0FBQyxDQUFDO0lBQzdFO0lBQ0E7SUFDQSxPQUFPa0YsTUFBTSxJQUFJQSxNQUFNLEtBQUtsRixNQUFNLEVBQUU7TUFDbEMsSUFBSWtGLE1BQU0sWUFBWWhSLE9BQU8sRUFBRTtRQUM3QndOLFVBQVUsQ0FBQ1csSUFBSSxDQUFDbkksSUFBSSxDQUFDZ0wsTUFBTSxDQUFDO01BQzlCO01BQ0FBLE1BQU0sR0FBR0EsTUFBTSxDQUFDRyxXQUFXO0lBQzdCO0lBQ0FOLGNBQWMsQ0FBQy9FLE1BQU0sQ0FBQztJQUN0QixJQUFJQSxNQUFNLFlBQVk5TCxPQUFPLEVBQUU7TUFDN0I4TCxNQUFNLENBQUNwUyxNQUFNLENBQUMsQ0FBQztJQUNqQixDQUFDLE1BQU07TUFDTG9TLE1BQU0sQ0FBQ3hMLFVBQVUsQ0FBQ3NJLFdBQVcsQ0FBQ2tELE1BQU0sQ0FBQztJQUN2QztFQUNGOztFQUVBO0FBQ0Y7QUFDQTtBQUNBO0FBQ0E7RUFDRSxTQUFTdUYsY0FBY0EsQ0FBQ3ZGLE1BQU0sRUFBRWhKLFFBQVEsRUFBRTBLLFVBQVUsRUFBRTtJQUNwRCxPQUFPaUMsaUJBQWlCLENBQUMzRCxNQUFNLEVBQUVBLE1BQU0sQ0FBQzRELFVBQVUsRUFBRTVNLFFBQVEsRUFBRTBLLFVBQVUsQ0FBQztFQUMzRTs7RUFFQTtBQUNGO0FBQ0E7QUFDQTtBQUNBO0VBQ0UsU0FBUzhELGVBQWVBLENBQUN4RixNQUFNLEVBQUVoSixRQUFRLEVBQUUwSyxVQUFVLEVBQUU7SUFDckQsT0FBT2lDLGlCQUFpQixDQUFDdFAsU0FBUyxDQUFDMkwsTUFBTSxDQUFDLEVBQUVBLE1BQU0sRUFBRWhKLFFBQVEsRUFBRTBLLFVBQVUsQ0FBQztFQUMzRTs7RUFFQTtBQUNGO0FBQ0E7QUFDQTtBQUNBO0VBQ0UsU0FBUytELGFBQWFBLENBQUN6RixNQUFNLEVBQUVoSixRQUFRLEVBQUUwSyxVQUFVLEVBQUU7SUFDbkQsT0FBT2lDLGlCQUFpQixDQUFDM0QsTUFBTSxFQUFFLElBQUksRUFBRWhKLFFBQVEsRUFBRTBLLFVBQVUsQ0FBQztFQUM5RDs7RUFFQTtBQUNGO0FBQ0E7QUFDQTtBQUNBO0VBQ0UsU0FBU2dFLFlBQVlBLENBQUMxRixNQUFNLEVBQUVoSixRQUFRLEVBQUUwSyxVQUFVLEVBQUU7SUFDbEQsT0FBT2lDLGlCQUFpQixDQUFDdFAsU0FBUyxDQUFDMkwsTUFBTSxDQUFDLEVBQUVBLE1BQU0sQ0FBQ3FGLFdBQVcsRUFBRXJPLFFBQVEsRUFBRTBLLFVBQVUsQ0FBQztFQUN2Rjs7RUFFQTtBQUNGO0FBQ0E7RUFDRSxTQUFTaUUsVUFBVUEsQ0FBQzNGLE1BQU0sRUFBRTtJQUMxQitFLGNBQWMsQ0FBQy9FLE1BQU0sQ0FBQztJQUN0QixNQUFNMUwsTUFBTSxHQUFHRCxTQUFTLENBQUMyTCxNQUFNLENBQUM7SUFDaEMsSUFBSTFMLE1BQU0sRUFBRTtNQUNWLE9BQU9BLE1BQU0sQ0FBQ3dJLFdBQVcsQ0FBQ2tELE1BQU0sQ0FBQztJQUNuQztFQUNGOztFQUVBO0FBQ0Y7QUFDQTtBQUNBO0FBQ0E7RUFDRSxTQUFTaUYsYUFBYUEsQ0FBQ2pGLE1BQU0sRUFBRWhKLFFBQVEsRUFBRTBLLFVBQVUsRUFBRTtJQUNuRCxNQUFNa0MsVUFBVSxHQUFHNUQsTUFBTSxDQUFDNEQsVUFBVTtJQUNwQ0QsaUJBQWlCLENBQUMzRCxNQUFNLEVBQUU0RCxVQUFVLEVBQUU1TSxRQUFRLEVBQUUwSyxVQUFVLENBQUM7SUFDM0QsSUFBSWtDLFVBQVUsRUFBRTtNQUNkLE9BQU9BLFVBQVUsQ0FBQ3lCLFdBQVcsRUFBRTtRQUM3Qk4sY0FBYyxDQUFDbkIsVUFBVSxDQUFDeUIsV0FBVyxDQUFDO1FBQ3RDckYsTUFBTSxDQUFDbEQsV0FBVyxDQUFDOEcsVUFBVSxDQUFDeUIsV0FBVyxDQUFDO01BQzVDO01BQ0FOLGNBQWMsQ0FBQ25CLFVBQVUsQ0FBQztNQUMxQjVELE1BQU0sQ0FBQ2xELFdBQVcsQ0FBQzhHLFVBQVUsQ0FBQztJQUNoQztFQUNGOztFQUVBO0FBQ0Y7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0VBQ0UsU0FBU3pCLGFBQWFBLENBQUNmLFNBQVMsRUFBRTVULEdBQUcsRUFBRXdTLE1BQU0sRUFBRWhKLFFBQVEsRUFBRTBLLFVBQVUsRUFBRTtJQUNuRSxRQUFRTixTQUFTO01BQ2YsS0FBSyxNQUFNO1FBQ1Q7TUFDRixLQUFLLFdBQVc7UUFDZDRELGFBQWEsQ0FBQ2hGLE1BQU0sRUFBRWhKLFFBQVEsRUFBRTBLLFVBQVUsQ0FBQztRQUMzQztNQUNGLEtBQUssWUFBWTtRQUNmNkQsY0FBYyxDQUFDdkYsTUFBTSxFQUFFaEosUUFBUSxFQUFFMEssVUFBVSxDQUFDO1FBQzVDO01BQ0YsS0FBSyxhQUFhO1FBQ2hCOEQsZUFBZSxDQUFDeEYsTUFBTSxFQUFFaEosUUFBUSxFQUFFMEssVUFBVSxDQUFDO1FBQzdDO01BQ0YsS0FBSyxXQUFXO1FBQ2QrRCxhQUFhLENBQUN6RixNQUFNLEVBQUVoSixRQUFRLEVBQUUwSyxVQUFVLENBQUM7UUFDM0M7TUFDRixLQUFLLFVBQVU7UUFDYmdFLFlBQVksQ0FBQzFGLE1BQU0sRUFBRWhKLFFBQVEsRUFBRTBLLFVBQVUsQ0FBQztRQUMxQztNQUNGLEtBQUssUUFBUTtRQUNYaUUsVUFBVSxDQUFDM0YsTUFBTSxDQUFDO1FBQ2xCO01BQ0Y7UUFDRSxJQUFJcUIsVUFBVSxHQUFHQyxhQUFhLENBQUM5VCxHQUFHLENBQUM7UUFDbkMsS0FBSyxJQUFJeU0sQ0FBQyxHQUFHLENBQUMsRUFBRUEsQ0FBQyxHQUFHb0gsVUFBVSxDQUFDbkssTUFBTSxFQUFFK0MsQ0FBQyxFQUFFLEVBQUU7VUFDMUMsTUFBTTJMLEdBQUcsR0FBR3ZFLFVBQVUsQ0FBQ3BILENBQUMsQ0FBQztVQUN6QixJQUFJO1lBQ0YsTUFBTTRMLFdBQVcsR0FBR0QsR0FBRyxDQUFDRSxVQUFVLENBQUMxRSxTQUFTLEVBQUVwQixNQUFNLEVBQUVoSixRQUFRLEVBQUUwSyxVQUFVLENBQUM7WUFDM0UsSUFBSW1FLFdBQVcsRUFBRTtjQUNmLElBQUkzTixLQUFLLENBQUM2TixPQUFPLENBQUNGLFdBQVcsQ0FBQyxFQUFFO2dCQUM5QjtnQkFDQSxLQUFLLElBQUlHLENBQUMsR0FBRyxDQUFDLEVBQUVBLENBQUMsR0FBR0gsV0FBVyxDQUFDM08sTUFBTSxFQUFFOE8sQ0FBQyxFQUFFLEVBQUU7a0JBQzNDLE1BQU1ySSxLQUFLLEdBQUdrSSxXQUFXLENBQUNHLENBQUMsQ0FBQztrQkFDNUIsSUFBSXJJLEtBQUssQ0FBQ2tHLFFBQVEsS0FBS3RFLElBQUksQ0FBQ3VFLFNBQVMsSUFBSW5HLEtBQUssQ0FBQ2tHLFFBQVEsS0FBS3RFLElBQUksQ0FBQ3dFLFlBQVksRUFBRTtvQkFDN0VyQyxVQUFVLENBQUMyQixLQUFLLENBQUNuSixJQUFJLENBQUNvSixnQkFBZ0IsQ0FBQzNGLEtBQUssQ0FBQyxDQUFDO2tCQUNoRDtnQkFDRjtjQUNGO2NBQ0E7WUFDRjtVQUNGLENBQUMsQ0FBQyxPQUFPakksQ0FBQyxFQUFFO1lBQ1Y0QyxRQUFRLENBQUM1QyxDQUFDLENBQUM7VUFDYjtRQUNGO1FBQ0EsSUFBSTBMLFNBQVMsS0FBSyxXQUFXLEVBQUU7VUFDN0I2RCxhQUFhLENBQUNqRixNQUFNLEVBQUVoSixRQUFRLEVBQUUwSyxVQUFVLENBQUM7UUFDN0MsQ0FBQyxNQUFNO1VBQ0xTLGFBQWEsQ0FBQ3RWLElBQUksQ0FBQzBCLE1BQU0sQ0FBQ0ksZ0JBQWdCLEVBQUVuQixHQUFHLEVBQUV3UyxNQUFNLEVBQUVoSixRQUFRLEVBQUUwSyxVQUFVLENBQUM7UUFDaEY7SUFDSjtFQUNGOztFQUVBO0FBQ0Y7QUFDQTtBQUNBO0FBQ0E7RUFDRSxTQUFTdUUsc0JBQXNCQSxDQUFDalAsUUFBUSxFQUFFMEssVUFBVSxFQUFFdkMsUUFBUSxFQUFFO0lBQzlELElBQUkrRyxPQUFPLEdBQUc3WSxPQUFPLENBQUMySixRQUFRLEVBQUUsbUNBQW1DLENBQUM7SUFDcEVRLE9BQU8sQ0FBQzBPLE9BQU8sRUFBRSxVQUFTekUsVUFBVSxFQUFFO01BQ3BDLElBQUk1VSxJQUFJLENBQUMwQixNQUFNLENBQUNvQyxtQkFBbUIsSUFBSThRLFVBQVUsQ0FBQ2xOLGFBQWEsS0FBSyxJQUFJLEVBQUU7UUFDeEUsTUFBTWlOLFFBQVEsR0FBR3RQLGlCQUFpQixDQUFDdVAsVUFBVSxFQUFFLGFBQWEsQ0FBQztRQUM3RCxJQUFJRCxRQUFRLElBQUksSUFBSSxFQUFFO1VBQ3BCMU8sT0FBTyxDQUFDME8sUUFBUSxFQUFFQyxVQUFVLEVBQUVDLFVBQVUsRUFBRXZDLFFBQVEsQ0FBQztRQUNyRDtNQUNGLENBQUMsTUFBTTtRQUNMc0MsVUFBVSxDQUFDakUsZUFBZSxDQUFDLGFBQWEsQ0FBQztRQUN6Q2lFLFVBQVUsQ0FBQ2pFLGVBQWUsQ0FBQyxrQkFBa0IsQ0FBQztNQUNoRDtJQUNGLENBQUMsQ0FBQztJQUNGLE9BQU8wSSxPQUFPLENBQUNoUCxNQUFNLEdBQUcsQ0FBQztFQUMzQjs7RUFFQTtBQUNGO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0VBQ0UsU0FBU2pKLElBQUlBLENBQUMrUixNQUFNLEVBQUVoSCxPQUFPLEVBQUVtTixRQUFRLEVBQUVDLFdBQVcsRUFBRTtJQUNwRCxJQUFJLENBQUNBLFdBQVcsRUFBRTtNQUNoQkEsV0FBVyxHQUFHLENBQUMsQ0FBQztJQUNsQjtJQUVBcEcsTUFBTSxHQUFHcEQsYUFBYSxDQUFDb0QsTUFBTSxDQUFDO0lBQzlCLE1BQU1iLFFBQVEsR0FBR2lILFdBQVcsQ0FBQ0MsY0FBYyxHQUFHelIsV0FBVyxDQUFDd1IsV0FBVyxDQUFDQyxjQUFjLEVBQUUsS0FBSyxDQUFDLEdBQUczUixXQUFXLENBQUMsQ0FBQzs7SUFFNUc7SUFDQSxNQUFNNFIsU0FBUyxHQUFHM1IsUUFBUSxDQUFDNFIsYUFBYTtJQUN4QyxJQUFJQyxhQUFhLEdBQUcsQ0FBQyxDQUFDO0lBQ3RCLElBQUk7TUFDRkEsYUFBYSxHQUFHO1FBQ2RoWixHQUFHLEVBQUU4WSxTQUFTO1FBQ2Q7UUFDQWxILEtBQUssRUFBRWtILFNBQVMsR0FBR0EsU0FBUyxDQUFDRyxjQUFjLEdBQUcsSUFBSTtRQUNsRDtRQUNBQyxHQUFHLEVBQUVKLFNBQVMsR0FBR0EsU0FBUyxDQUFDSyxZQUFZLEdBQUc7TUFDNUMsQ0FBQztJQUNILENBQUMsQ0FBQyxPQUFPalIsQ0FBQyxFQUFFO01BQ1Y7SUFBQTtJQUVGLE1BQU1nTSxVQUFVLEdBQUc3TyxjQUFjLENBQUNtTixNQUFNLENBQUM7O0lBRXpDO0lBQ0EsSUFBSW1HLFFBQVEsQ0FBQy9FLFNBQVMsS0FBSyxhQUFhLEVBQUU7TUFDeENwQixNQUFNLENBQUNuSSxXQUFXLEdBQUdtQixPQUFPO01BQzlCO0lBQ0EsQ0FBQyxNQUFNO01BQ0wsSUFBSWhDLFFBQVEsR0FBR3JFLFlBQVksQ0FBQ3FHLE9BQU8sQ0FBQztNQUVwQzBJLFVBQVUsQ0FBQzVJLEtBQUssR0FBRzlCLFFBQVEsQ0FBQzhCLEtBQUs7O01BRWpDO01BQ0EsSUFBSXNOLFdBQVcsQ0FBQ1EsU0FBUyxFQUFFO1FBQ3pCLE1BQU1DLGVBQWUsR0FBR1QsV0FBVyxDQUFDUSxTQUFTLENBQUNyUixLQUFLLENBQUMsR0FBRyxDQUFDO1FBQ3hELEtBQUssSUFBSTBFLENBQUMsR0FBRyxDQUFDLEVBQUVBLENBQUMsR0FBRzRNLGVBQWUsQ0FBQzNQLE1BQU0sRUFBRStDLENBQUMsRUFBRSxFQUFFO1VBQy9DLE1BQU02TSxjQUFjLEdBQUdELGVBQWUsQ0FBQzVNLENBQUMsQ0FBQyxDQUFDMUUsS0FBSyxDQUFDLEdBQUcsRUFBRSxDQUFDLENBQUM7VUFDdkQsSUFBSWtOLEVBQUUsR0FBR3FFLGNBQWMsQ0FBQyxDQUFDLENBQUMsQ0FBQ2hNLElBQUksQ0FBQyxDQUFDO1VBQ2pDLElBQUkySCxFQUFFLENBQUNqTixPQUFPLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxFQUFFO1lBQ3pCaU4sRUFBRSxHQUFHQSxFQUFFLENBQUMzRSxTQUFTLENBQUMsQ0FBQyxDQUFDO1VBQ3RCO1VBQ0EsTUFBTTBELFFBQVEsR0FBR3NGLGNBQWMsQ0FBQyxDQUFDLENBQUMsSUFBSSxNQUFNO1VBQzVDLE1BQU1yRixVQUFVLEdBQUd6SyxRQUFRLENBQUMrQixhQUFhLENBQUMsR0FBRyxHQUFHMEosRUFBRSxDQUFDO1VBQ25ELElBQUloQixVQUFVLEVBQUU7WUFDZDNPLE9BQU8sQ0FBQzBPLFFBQVEsRUFBRUMsVUFBVSxFQUFFQyxVQUFVLEVBQUV2QyxRQUFRLENBQUM7VUFDckQ7UUFDRjtNQUNGO01BQ0E7TUFDQThHLHNCQUFzQixDQUFDalAsUUFBUSxFQUFFMEssVUFBVSxFQUFFdkMsUUFBUSxDQUFDO01BQ3REM0gsT0FBTyxDQUFDbkssT0FBTyxDQUFDMkosUUFBUSxFQUFFLFVBQVUsQ0FBQyxFQUFFLDRDQUE0QyxVQUFTK1AsUUFBUSxFQUFFO1FBQ3BHLElBQUlBLFFBQVEsQ0FBQy9OLE9BQU8sSUFBSWlOLHNCQUFzQixDQUFDYyxRQUFRLENBQUMvTixPQUFPLEVBQUUwSSxVQUFVLEVBQUV2QyxRQUFRLENBQUMsRUFBRTtVQUN0RjtVQUNBNEgsUUFBUSxDQUFDblosTUFBTSxDQUFDLENBQUM7UUFDbkI7TUFDRixDQUFDLENBQUM7O01BRUY7TUFDQSxJQUFJd1ksV0FBVyxDQUFDWSxNQUFNLEVBQUU7UUFDdEIsTUFBTUMsV0FBVyxHQUFHdlMsV0FBVyxDQUFDLENBQUMsQ0FBQ29OLHNCQUFzQixDQUFDLENBQUM7UUFDMUR0SyxPQUFPLENBQUNSLFFBQVEsQ0FBQ29CLGdCQUFnQixDQUFDZ08sV0FBVyxDQUFDWSxNQUFNLENBQUMsRUFBRSxVQUFTekosSUFBSSxFQUFFO1VBQ3BFMEosV0FBVyxDQUFDbEYsV0FBVyxDQUFDeEUsSUFBSSxDQUFDO1FBQy9CLENBQUMsQ0FBQztRQUNGdkcsUUFBUSxHQUFHaVEsV0FBVztNQUN4QjtNQUNBL0UsdUJBQXVCLENBQUNsTCxRQUFRLENBQUM7TUFDakNtTCxhQUFhLENBQUNnRSxRQUFRLENBQUMvRSxTQUFTLEVBQUVnRixXQUFXLENBQUNDLGNBQWMsRUFBRXJHLE1BQU0sRUFBRWhKLFFBQVEsRUFBRTBLLFVBQVUsQ0FBQztNQUMzRlUsd0JBQXdCLENBQUMsQ0FBQztJQUM1Qjs7SUFFQTtJQUNBLElBQUlvRSxhQUFhLENBQUNoWixHQUFHLElBQ25CLENBQUNxRSxZQUFZLENBQUMyVSxhQUFhLENBQUNoWixHQUFHLENBQUMsSUFDaEN3RyxlQUFlLENBQUN3UyxhQUFhLENBQUNoWixHQUFHLEVBQUUsSUFBSSxDQUFDLEVBQUU7TUFDMUMsTUFBTTBaLFlBQVksR0FBR3ZTLFFBQVEsQ0FBQ2dPLGNBQWMsQ0FBQzNPLGVBQWUsQ0FBQ3dTLGFBQWEsQ0FBQ2haLEdBQUcsRUFBRSxJQUFJLENBQUMsQ0FBQztNQUN0RixNQUFNMlosWUFBWSxHQUFHO1FBQUVDLGFBQWEsRUFBRWpCLFFBQVEsQ0FBQ2tCLFdBQVcsS0FBSzNULFNBQVMsR0FBRyxDQUFDeVMsUUFBUSxDQUFDa0IsV0FBVyxHQUFHLENBQUN4YSxJQUFJLENBQUMwQixNQUFNLENBQUN3QjtNQUFtQixDQUFDO01BQ3BJLElBQUltWCxZQUFZLEVBQUU7UUFDaEI7UUFDQSxJQUFJVixhQUFhLENBQUNwSCxLQUFLLElBQUk4SCxZQUFZLENBQUNJLGlCQUFpQixFQUFFO1VBQ3pELElBQUk7WUFDRjtZQUNBSixZQUFZLENBQUNJLGlCQUFpQixDQUFDZCxhQUFhLENBQUNwSCxLQUFLLEVBQUVvSCxhQUFhLENBQUNFLEdBQUcsQ0FBQztVQUN4RSxDQUFDLENBQUMsT0FBT2hSLENBQUMsRUFBRTtZQUNWO1VBQUE7UUFFSjtRQUNBd1IsWUFBWSxDQUFDeEQsS0FBSyxDQUFDeUQsWUFBWSxDQUFDO01BQ2xDO0lBQ0Y7SUFFQW5ILE1BQU0sQ0FBQzNDLFNBQVMsQ0FBQ3pQLE1BQU0sQ0FBQ2YsSUFBSSxDQUFDMEIsTUFBTSxDQUFDWSxhQUFhLENBQUM7SUFDbERxSSxPQUFPLENBQUNrSyxVQUFVLENBQUNXLElBQUksRUFBRSxVQUFTN1UsR0FBRyxFQUFFO01BQ3JDLElBQUlBLEdBQUcsQ0FBQzZQLFNBQVMsRUFBRTtRQUNqQjdQLEdBQUcsQ0FBQzZQLFNBQVMsQ0FBQ0MsR0FBRyxDQUFDelEsSUFBSSxDQUFDMEIsTUFBTSxDQUFDVyxhQUFhLENBQUM7TUFDOUM7TUFDQWlDLFlBQVksQ0FBQzNELEdBQUcsRUFBRSxnQkFBZ0IsRUFBRTRZLFdBQVcsQ0FBQ21CLFNBQVMsQ0FBQztJQUM1RCxDQUFDLENBQUM7SUFDRixJQUFJbkIsV0FBVyxDQUFDb0IsaUJBQWlCLEVBQUU7TUFDakNwQixXQUFXLENBQUNvQixpQkFBaUIsQ0FBQyxDQUFDO0lBQ2pDOztJQUVBO0lBQ0EsSUFBSSxDQUFDckIsUUFBUSxDQUFDL1YsV0FBVyxFQUFFO01BQ3pCcVgsV0FBVyxDQUFDL0YsVUFBVSxDQUFDNUksS0FBSyxDQUFDO0lBQy9COztJQUVBO0lBQ0EsTUFBTTRPLFFBQVEsR0FBRyxTQUFBQSxDQUFBLEVBQVc7TUFDMUJsUSxPQUFPLENBQUNrSyxVQUFVLENBQUMyQixLQUFLLEVBQUUsVUFBU3NFLElBQUksRUFBRTtRQUN2Q0EsSUFBSSxDQUFDdlIsSUFBSSxDQUFDLENBQUM7TUFDYixDQUFDLENBQUM7TUFDRm9CLE9BQU8sQ0FBQ2tLLFVBQVUsQ0FBQ1csSUFBSSxFQUFFLFVBQVM3VSxHQUFHLEVBQUU7UUFDckMsSUFBSUEsR0FBRyxDQUFDNlAsU0FBUyxFQUFFO1VBQ2pCN1AsR0FBRyxDQUFDNlAsU0FBUyxDQUFDelAsTUFBTSxDQUFDZixJQUFJLENBQUMwQixNQUFNLENBQUNXLGFBQWEsQ0FBQztRQUNqRDtRQUNBaUMsWUFBWSxDQUFDM0QsR0FBRyxFQUFFLGtCQUFrQixFQUFFNFksV0FBVyxDQUFDbUIsU0FBUyxDQUFDO01BQzlELENBQUMsQ0FBQztNQUVGLElBQUluQixXQUFXLENBQUN3QixNQUFNLEVBQUU7UUFDdEIsTUFBTUMsWUFBWSxHQUFHbFMsU0FBUyxDQUFDaUgsYUFBYSxDQUFDLEdBQUcsR0FBR3dKLFdBQVcsQ0FBQ3dCLE1BQU0sQ0FBQyxDQUFDO1FBQ3ZFLElBQUlDLFlBQVksRUFBRTtVQUNoQkEsWUFBWSxDQUFDQyxjQUFjLENBQUM7WUFBRUMsS0FBSyxFQUFFLE9BQU87WUFBRUMsUUFBUSxFQUFFO1VBQU8sQ0FBQyxDQUFDO1FBQ25FO01BQ0Y7TUFFQUMsaUJBQWlCLENBQUN2RyxVQUFVLENBQUNXLElBQUksRUFBRThELFFBQVEsQ0FBQztNQUM1QyxJQUFJQyxXQUFXLENBQUM4QixtQkFBbUIsRUFBRTtRQUNuQzlCLFdBQVcsQ0FBQzhCLG1CQUFtQixDQUFDLENBQUM7TUFDbkM7SUFDRixDQUFDO0lBRUQsSUFBSS9CLFFBQVEsQ0FBQ2dDLFdBQVcsR0FBRyxDQUFDLEVBQUU7TUFDNUJ6TCxTQUFTLENBQUMsQ0FBQyxDQUFDRyxVQUFVLENBQUM2SyxRQUFRLEVBQUV2QixRQUFRLENBQUNnQyxXQUFXLENBQUM7SUFDeEQsQ0FBQyxNQUFNO01BQ0xULFFBQVEsQ0FBQyxDQUFDO0lBQ1o7RUFDRjs7RUFFQTtBQUNGO0FBQ0E7QUFDQTtBQUNBO0VBQ0UsU0FBU1UsbUJBQW1CQSxDQUFDQyxHQUFHLEVBQUVDLE1BQU0sRUFBRTlhLEdBQUcsRUFBRTtJQUM3QyxNQUFNK2EsV0FBVyxHQUFHRixHQUFHLENBQUNHLGlCQUFpQixDQUFDRixNQUFNLENBQUM7SUFDakQsSUFBSUMsV0FBVyxDQUFDL1MsT0FBTyxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsRUFBRTtNQUNsQyxNQUFNaVQsUUFBUSxHQUFHdE4sU0FBUyxDQUFDb04sV0FBVyxDQUFDO01BQ3ZDLEtBQUssTUFBTUcsU0FBUyxJQUFJRCxRQUFRLEVBQUU7UUFDaEMsSUFBSUEsUUFBUSxDQUFDdk4sY0FBYyxDQUFDd04sU0FBUyxDQUFDLEVBQUU7VUFDdEMsSUFBSXJNLE1BQU0sR0FBR29NLFFBQVEsQ0FBQ0MsU0FBUyxDQUFDO1VBQ2hDLElBQUkvTyxXQUFXLENBQUMwQyxNQUFNLENBQUMsRUFBRTtZQUN2QjtZQUNBN08sR0FBRyxHQUFHNk8sTUFBTSxDQUFDMkQsTUFBTSxLQUFLdE0sU0FBUyxHQUFHMkksTUFBTSxDQUFDMkQsTUFBTSxHQUFHeFMsR0FBRztVQUN6RCxDQUFDLE1BQU07WUFDTDZPLE1BQU0sR0FBRztjQUFFekUsS0FBSyxFQUFFeUU7WUFBTyxDQUFDO1VBQzVCO1VBQ0FsTCxZQUFZLENBQUMzRCxHQUFHLEVBQUVrYixTQUFTLEVBQUVyTSxNQUFNLENBQUM7UUFDdEM7TUFDRjtJQUNGLENBQUMsTUFBTTtNQUNMLE1BQU1zTSxVQUFVLEdBQUdKLFdBQVcsQ0FBQ2hULEtBQUssQ0FBQyxHQUFHLENBQUM7TUFDekMsS0FBSyxJQUFJMEUsQ0FBQyxHQUFHLENBQUMsRUFBRUEsQ0FBQyxHQUFHME8sVUFBVSxDQUFDelIsTUFBTSxFQUFFK0MsQ0FBQyxFQUFFLEVBQUU7UUFDMUM5SSxZQUFZLENBQUMzRCxHQUFHLEVBQUVtYixVQUFVLENBQUMxTyxDQUFDLENBQUMsQ0FBQ2EsSUFBSSxDQUFDLENBQUMsRUFBRSxFQUFFLENBQUM7TUFDN0M7SUFDRjtFQUNGO0VBRUEsTUFBTThOLFVBQVUsR0FBRyxJQUFJO0VBQ3ZCLE1BQU1DLG1CQUFtQixHQUFHLE9BQU87RUFDbkMsTUFBTUMsWUFBWSxHQUFHLFlBQVk7RUFDakMsTUFBTUMsV0FBVyxHQUFHLGVBQWU7RUFDbkMsTUFBTUMsZUFBZSxHQUFHLENBQUMsR0FBRyxFQUFFLEdBQUcsRUFBRSxHQUFHLENBQUM7RUFDdkMsTUFBTUMsY0FBYyxHQUFHLE9BQU87RUFDOUIsTUFBTUMsdUJBQXVCLEdBQUcsTUFBTTtFQUN0QyxNQUFNQyxxQkFBcUIsR0FBRyxNQUFNOztFQUVwQztBQUNGO0FBQ0E7QUFDQTtFQUNFLFNBQVNDLGNBQWNBLENBQUMzVixHQUFHLEVBQUU7SUFDM0I7SUFDQSxNQUFNNFYsTUFBTSxHQUFHLEVBQUU7SUFDakIsSUFBSUMsUUFBUSxHQUFHLENBQUM7SUFDaEIsT0FBT0EsUUFBUSxHQUFHN1YsR0FBRyxDQUFDeUQsTUFBTSxFQUFFO01BQzVCLElBQUk0UixZQUFZLENBQUN0UyxJQUFJLENBQUMvQyxHQUFHLENBQUM4VixNQUFNLENBQUNELFFBQVEsQ0FBQyxDQUFDLEVBQUU7UUFDM0MsSUFBSUUsYUFBYSxHQUFHRixRQUFRO1FBQzVCLE9BQU9QLFdBQVcsQ0FBQ3ZTLElBQUksQ0FBQy9DLEdBQUcsQ0FBQzhWLE1BQU0sQ0FBQ0QsUUFBUSxHQUFHLENBQUMsQ0FBQyxDQUFDLEVBQUU7VUFDakRBLFFBQVEsRUFBRTtRQUNaO1FBQ0FELE1BQU0sQ0FBQ25QLElBQUksQ0FBQ3pHLEdBQUcsQ0FBQ3FLLFNBQVMsQ0FBQzBMLGFBQWEsRUFBRUYsUUFBUSxHQUFHLENBQUMsQ0FBQyxDQUFDO01BQ3pELENBQUMsTUFBTSxJQUFJTixlQUFlLENBQUN4VCxPQUFPLENBQUMvQixHQUFHLENBQUM4VixNQUFNLENBQUNELFFBQVEsQ0FBQyxDQUFDLEtBQUssQ0FBQyxDQUFDLEVBQUU7UUFDL0QsTUFBTUcsU0FBUyxHQUFHaFcsR0FBRyxDQUFDOFYsTUFBTSxDQUFDRCxRQUFRLENBQUM7UUFDdEMsSUFBSUUsYUFBYSxHQUFHRixRQUFRO1FBQzVCQSxRQUFRLEVBQUU7UUFDVixPQUFPQSxRQUFRLEdBQUc3VixHQUFHLENBQUN5RCxNQUFNLElBQUl6RCxHQUFHLENBQUM4VixNQUFNLENBQUNELFFBQVEsQ0FBQyxLQUFLRyxTQUFTLEVBQUU7VUFDbEUsSUFBSWhXLEdBQUcsQ0FBQzhWLE1BQU0sQ0FBQ0QsUUFBUSxDQUFDLEtBQUssSUFBSSxFQUFFO1lBQ2pDQSxRQUFRLEVBQUU7VUFDWjtVQUNBQSxRQUFRLEVBQUU7UUFDWjtRQUNBRCxNQUFNLENBQUNuUCxJQUFJLENBQUN6RyxHQUFHLENBQUNxSyxTQUFTLENBQUMwTCxhQUFhLEVBQUVGLFFBQVEsR0FBRyxDQUFDLENBQUMsQ0FBQztNQUN6RCxDQUFDLE1BQU07UUFDTCxNQUFNSSxNQUFNLEdBQUdqVyxHQUFHLENBQUM4VixNQUFNLENBQUNELFFBQVEsQ0FBQztRQUNuQ0QsTUFBTSxDQUFDblAsSUFBSSxDQUFDd1AsTUFBTSxDQUFDO01BQ3JCO01BQ0FKLFFBQVEsRUFBRTtJQUNaO0lBQ0EsT0FBT0QsTUFBTTtFQUNmOztFQUVBO0FBQ0Y7QUFDQTtBQUNBO0FBQ0E7QUFDQTtFQUNFLFNBQVNNLDJCQUEyQkEsQ0FBQ0MsS0FBSyxFQUFFQyxJQUFJLEVBQUVDLFNBQVMsRUFBRTtJQUMzRCxPQUFPaEIsWUFBWSxDQUFDdFMsSUFBSSxDQUFDb1QsS0FBSyxDQUFDTCxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFDdkNLLEtBQUssS0FBSyxNQUFNLElBQ2hCQSxLQUFLLEtBQUssT0FBTyxJQUNqQkEsS0FBSyxLQUFLLE1BQU0sSUFDaEJBLEtBQUssS0FBS0UsU0FBUyxJQUNuQkQsSUFBSSxLQUFLLEdBQUc7RUFDaEI7O0VBRUE7QUFDRjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0VBQ0UsU0FBU0Usd0JBQXdCQSxDQUFDdmMsR0FBRyxFQUFFNmIsTUFBTSxFQUFFUyxTQUFTLEVBQUU7SUFDeEQsSUFBSVQsTUFBTSxDQUFDLENBQUMsQ0FBQyxLQUFLLEdBQUcsRUFBRTtNQUNyQkEsTUFBTSxDQUFDM0ssS0FBSyxDQUFDLENBQUM7TUFDZCxJQUFJc0wsWUFBWSxHQUFHLENBQUM7TUFDcEIsSUFBSUMsaUJBQWlCLEdBQUcsb0JBQW9CLEdBQUdILFNBQVMsR0FBRyxhQUFhO01BQ3hFLElBQUlELElBQUksR0FBRyxJQUFJO01BQ2YsT0FBT1IsTUFBTSxDQUFDblMsTUFBTSxHQUFHLENBQUMsRUFBRTtRQUN4QixNQUFNMFMsS0FBSyxHQUFHUCxNQUFNLENBQUMsQ0FBQyxDQUFDO1FBQ3ZCO1FBQ0EsSUFBSU8sS0FBSyxLQUFLLEdBQUcsRUFBRTtVQUNqQkksWUFBWSxFQUFFO1VBQ2QsSUFBSUEsWUFBWSxLQUFLLENBQUMsRUFBRTtZQUN0QixJQUFJSCxJQUFJLEtBQUssSUFBSSxFQUFFO2NBQ2pCSSxpQkFBaUIsR0FBR0EsaUJBQWlCLEdBQUcsTUFBTTtZQUNoRDtZQUNBWixNQUFNLENBQUMzSyxLQUFLLENBQUMsQ0FBQztZQUNkdUwsaUJBQWlCLElBQUksS0FBSztZQUMxQixJQUFJO2NBQ0YsTUFBTUMsaUJBQWlCLEdBQUdqTyxTQUFTLENBQUN6TyxHQUFHLEVBQUUsWUFBVztnQkFDbEQsT0FBTzJjLFFBQVEsQ0FBQ0YsaUJBQWlCLENBQUMsQ0FBQyxDQUFDO2NBQ3RDLENBQUMsRUFDRCxZQUFXO2dCQUFFLE9BQU8sSUFBSTtjQUFDLENBQUMsQ0FBQztjQUMzQkMsaUJBQWlCLENBQUNFLE1BQU0sR0FBR0gsaUJBQWlCO2NBQzVDLE9BQU9DLGlCQUFpQjtZQUMxQixDQUFDLENBQUMsT0FBT3hVLENBQUMsRUFBRTtjQUNWeEMsaUJBQWlCLENBQUN3QixXQUFXLENBQUMsQ0FBQyxDQUFDbUUsSUFBSSxFQUFFLG1CQUFtQixFQUFFO2dCQUFFbkksS0FBSyxFQUFFZ0YsQ0FBQztnQkFBRTBVLE1BQU0sRUFBRUg7Y0FBa0IsQ0FBQyxDQUFDO2NBQ25HLE9BQU8sSUFBSTtZQUNiO1VBQ0Y7UUFDRixDQUFDLE1BQU0sSUFBSUwsS0FBSyxLQUFLLEdBQUcsRUFBRTtVQUN4QkksWUFBWSxFQUFFO1FBQ2hCO1FBQ0EsSUFBSUwsMkJBQTJCLENBQUNDLEtBQUssRUFBRUMsSUFBSSxFQUFFQyxTQUFTLENBQUMsRUFBRTtVQUN2REcsaUJBQWlCLElBQUksSUFBSSxHQUFHSCxTQUFTLEdBQUcsR0FBRyxHQUFHRixLQUFLLEdBQUcsT0FBTyxHQUFHRSxTQUFTLEdBQUcsR0FBRyxHQUFHRixLQUFLLEdBQUcsY0FBYyxHQUFHQSxLQUFLLEdBQUcsSUFBSTtRQUN6SCxDQUFDLE1BQU07VUFDTEssaUJBQWlCLEdBQUdBLGlCQUFpQixHQUFHTCxLQUFLO1FBQy9DO1FBQ0FDLElBQUksR0FBR1IsTUFBTSxDQUFDM0ssS0FBSyxDQUFDLENBQUM7TUFDdkI7SUFDRjtFQUNGOztFQUVBO0FBQ0Y7QUFDQTtBQUNBO0FBQ0E7RUFDRSxTQUFTMkwsWUFBWUEsQ0FBQ2hCLE1BQU0sRUFBRTlTLEtBQUssRUFBRTtJQUNuQyxJQUFJaUksTUFBTSxHQUFHLEVBQUU7SUFDZixPQUFPNkssTUFBTSxDQUFDblMsTUFBTSxHQUFHLENBQUMsSUFBSSxDQUFDWCxLQUFLLENBQUNnRixJQUFJLENBQUM4TixNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRTtNQUNsRDdLLE1BQU0sSUFBSTZLLE1BQU0sQ0FBQzNLLEtBQUssQ0FBQyxDQUFDO0lBQzFCO0lBQ0EsT0FBT0YsTUFBTTtFQUNmOztFQUVBO0FBQ0Y7QUFDQTtBQUNBO0VBQ0UsU0FBUzhMLGtCQUFrQkEsQ0FBQ2pCLE1BQU0sRUFBRTtJQUNsQyxJQUFJN0ssTUFBTTtJQUNWLElBQUk2SyxNQUFNLENBQUNuUyxNQUFNLEdBQUcsQ0FBQyxJQUFJZ1MsdUJBQXVCLENBQUMzTixJQUFJLENBQUM4TixNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRTtNQUNoRUEsTUFBTSxDQUFDM0ssS0FBSyxDQUFDLENBQUM7TUFDZEYsTUFBTSxHQUFHNkwsWUFBWSxDQUFDaEIsTUFBTSxFQUFFRixxQkFBcUIsQ0FBQyxDQUFDck8sSUFBSSxDQUFDLENBQUM7TUFDM0R1TyxNQUFNLENBQUMzSyxLQUFLLENBQUMsQ0FBQztJQUNoQixDQUFDLE1BQU07TUFDTEYsTUFBTSxHQUFHNkwsWUFBWSxDQUFDaEIsTUFBTSxFQUFFUixtQkFBbUIsQ0FBQztJQUNwRDtJQUNBLE9BQU9ySyxNQUFNO0VBQ2Y7RUFFQSxNQUFNK0wsY0FBYyxHQUFHLHlCQUF5Qjs7RUFFaEQ7QUFDRjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0VBQ0UsU0FBU0Msb0JBQW9CQSxDQUFDaGQsR0FBRyxFQUFFaWQsZUFBZSxFQUFFQyxLQUFLLEVBQUU7SUFDekQ7SUFDQSxNQUFNQyxZQUFZLEdBQUcsRUFBRTtJQUN2QixNQUFNdEIsTUFBTSxHQUFHRCxjQUFjLENBQUNxQixlQUFlLENBQUM7SUFDOUMsR0FBRztNQUNESixZQUFZLENBQUNoQixNQUFNLEVBQUVKLGNBQWMsQ0FBQztNQUNwQyxNQUFNMkIsYUFBYSxHQUFHdkIsTUFBTSxDQUFDblMsTUFBTTtNQUNuQyxNQUFNaEssT0FBTyxHQUFHbWQsWUFBWSxDQUFDaEIsTUFBTSxFQUFFLFNBQVMsQ0FBQztNQUMvQyxJQUFJbmMsT0FBTyxLQUFLLEVBQUUsRUFBRTtRQUNsQixJQUFJQSxPQUFPLEtBQUssT0FBTyxFQUFFO1VBQ3ZCO1VBQ0EsTUFBTTJkLEtBQUssR0FBRztZQUFFM2QsT0FBTyxFQUFFO1VBQVEsQ0FBQztVQUNsQ21kLFlBQVksQ0FBQ2hCLE1BQU0sRUFBRUosY0FBYyxDQUFDO1VBQ3BDNEIsS0FBSyxDQUFDQyxZQUFZLEdBQUdsYSxhQUFhLENBQUN5WixZQUFZLENBQUNoQixNQUFNLEVBQUUsU0FBUyxDQUFDLENBQUM7VUFDbkVnQixZQUFZLENBQUNoQixNQUFNLEVBQUVKLGNBQWMsQ0FBQztVQUNwQyxJQUFJOEIsV0FBVyxHQUFHaEIsd0JBQXdCLENBQUN2YyxHQUFHLEVBQUU2YixNQUFNLEVBQUUsT0FBTyxDQUFDO1VBQ2hFLElBQUkwQixXQUFXLEVBQUU7WUFDZkYsS0FBSyxDQUFDRSxXQUFXLEdBQUdBLFdBQVc7VUFDakM7VUFDQUosWUFBWSxDQUFDelEsSUFBSSxDQUFDMlEsS0FBSyxDQUFDO1FBQzFCLENBQUMsTUFBTTtVQUNMO1VBQ0EsTUFBTUcsV0FBVyxHQUFHO1lBQUU5ZDtVQUFRLENBQUM7VUFDL0IsSUFBSTZkLFdBQVcsR0FBR2hCLHdCQUF3QixDQUFDdmMsR0FBRyxFQUFFNmIsTUFBTSxFQUFFLE9BQU8sQ0FBQztVQUNoRSxJQUFJMEIsV0FBVyxFQUFFO1lBQ2ZDLFdBQVcsQ0FBQ0QsV0FBVyxHQUFHQSxXQUFXO1VBQ3ZDO1VBQ0FWLFlBQVksQ0FBQ2hCLE1BQU0sRUFBRUosY0FBYyxDQUFDO1VBQ3BDLE9BQU9JLE1BQU0sQ0FBQ25TLE1BQU0sR0FBRyxDQUFDLElBQUltUyxNQUFNLENBQUMsQ0FBQyxDQUFDLEtBQUssR0FBRyxFQUFFO1lBQzdDLE1BQU1PLEtBQUssR0FBR1AsTUFBTSxDQUFDM0ssS0FBSyxDQUFDLENBQUM7WUFDNUIsSUFBSWtMLEtBQUssS0FBSyxTQUFTLEVBQUU7Y0FDdkJvQixXQUFXLENBQUNDLE9BQU8sR0FBRyxJQUFJO1lBQzVCLENBQUMsTUFBTSxJQUFJckIsS0FBSyxLQUFLLE1BQU0sRUFBRTtjQUMzQm9CLFdBQVcsQ0FBQ0UsSUFBSSxHQUFHLElBQUk7WUFDekIsQ0FBQyxNQUFNLElBQUl0QixLQUFLLEtBQUssU0FBUyxFQUFFO2NBQzlCb0IsV0FBVyxDQUFDRyxPQUFPLEdBQUcsSUFBSTtZQUM1QixDQUFDLE1BQU0sSUFBSXZCLEtBQUssS0FBSyxPQUFPLElBQUlQLE1BQU0sQ0FBQyxDQUFDLENBQUMsS0FBSyxHQUFHLEVBQUU7Y0FDakRBLE1BQU0sQ0FBQzNLLEtBQUssQ0FBQyxDQUFDO2NBQ2RzTSxXQUFXLENBQUNyTyxLQUFLLEdBQUcvTCxhQUFhLENBQUN5WixZQUFZLENBQUNoQixNQUFNLEVBQUVSLG1CQUFtQixDQUFDLENBQUM7WUFDOUUsQ0FBQyxNQUFNLElBQUllLEtBQUssS0FBSyxNQUFNLElBQUlQLE1BQU0sQ0FBQyxDQUFDLENBQUMsS0FBSyxHQUFHLEVBQUU7Y0FDaERBLE1BQU0sQ0FBQzNLLEtBQUssQ0FBQyxDQUFDO2NBQ2QsSUFBSXdLLHVCQUF1QixDQUFDM04sSUFBSSxDQUFDOE4sTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUU7Z0JBQzNDLElBQUkrQixRQUFRLEdBQUdkLGtCQUFrQixDQUFDakIsTUFBTSxDQUFDO2NBQzNDLENBQUMsTUFBTTtnQkFDTCxJQUFJK0IsUUFBUSxHQUFHZixZQUFZLENBQUNoQixNQUFNLEVBQUVSLG1CQUFtQixDQUFDO2dCQUN4RCxJQUFJdUMsUUFBUSxLQUFLLFNBQVMsSUFBSUEsUUFBUSxLQUFLLE1BQU0sSUFBSUEsUUFBUSxLQUFLLE1BQU0sSUFBSUEsUUFBUSxLQUFLLFVBQVUsRUFBRTtrQkFDbkcvQixNQUFNLENBQUMzSyxLQUFLLENBQUMsQ0FBQztrQkFDZCxNQUFNN0ksUUFBUSxHQUFHeVUsa0JBQWtCLENBQUNqQixNQUFNLENBQUM7a0JBQzNDO2tCQUNBLElBQUl4VCxRQUFRLENBQUNxQixNQUFNLEdBQUcsQ0FBQyxFQUFFO29CQUN2QmtVLFFBQVEsSUFBSSxHQUFHLEdBQUd2VixRQUFRO2tCQUM1QjtnQkFDRjtjQUNGO2NBQ0FtVixXQUFXLENBQUM3UyxJQUFJLEdBQUdpVCxRQUFRO1lBQzdCLENBQUMsTUFBTSxJQUFJeEIsS0FBSyxLQUFLLFFBQVEsSUFBSVAsTUFBTSxDQUFDLENBQUMsQ0FBQyxLQUFLLEdBQUcsRUFBRTtjQUNsREEsTUFBTSxDQUFDM0ssS0FBSyxDQUFDLENBQUM7Y0FDZHNNLFdBQVcsQ0FBQ2hMLE1BQU0sR0FBR3NLLGtCQUFrQixDQUFDakIsTUFBTSxDQUFDO1lBQ2pELENBQUMsTUFBTSxJQUFJTyxLQUFLLEtBQUssVUFBVSxJQUFJUCxNQUFNLENBQUMsQ0FBQyxDQUFDLEtBQUssR0FBRyxFQUFFO2NBQ3BEQSxNQUFNLENBQUMzSyxLQUFLLENBQUMsQ0FBQztjQUNkc00sV0FBVyxDQUFDSyxRQUFRLEdBQUd6YSxhQUFhLENBQUN5WixZQUFZLENBQUNoQixNQUFNLEVBQUVSLG1CQUFtQixDQUFDLENBQUM7WUFDakYsQ0FBQyxNQUFNLElBQUllLEtBQUssS0FBSyxPQUFPLElBQUlQLE1BQU0sQ0FBQyxDQUFDLENBQUMsS0FBSyxHQUFHLEVBQUU7Y0FDakRBLE1BQU0sQ0FBQzNLLEtBQUssQ0FBQyxDQUFDO2NBQ2RzTSxXQUFXLENBQUNNLEtBQUssR0FBR2pCLFlBQVksQ0FBQ2hCLE1BQU0sRUFBRVIsbUJBQW1CLENBQUM7WUFDL0QsQ0FBQyxNQUFNLElBQUllLEtBQUssS0FBSyxNQUFNLElBQUlQLE1BQU0sQ0FBQyxDQUFDLENBQUMsS0FBSyxHQUFHLEVBQUU7Y0FDaERBLE1BQU0sQ0FBQzNLLEtBQUssQ0FBQyxDQUFDO2NBQ2RzTSxXQUFXLENBQUNwQixLQUFLLENBQUMsR0FBR1Usa0JBQWtCLENBQUNqQixNQUFNLENBQUM7WUFDakQsQ0FBQyxNQUFNLElBQUlPLEtBQUssS0FBSyxXQUFXLElBQUlQLE1BQU0sQ0FBQyxDQUFDLENBQUMsS0FBSyxHQUFHLEVBQUU7Y0FDckRBLE1BQU0sQ0FBQzNLLEtBQUssQ0FBQyxDQUFDO2NBQ2RzTSxXQUFXLENBQUNwQixLQUFLLENBQUMsR0FBR1MsWUFBWSxDQUFDaEIsTUFBTSxFQUFFUixtQkFBbUIsQ0FBQztZQUNoRSxDQUFDLE1BQU07Y0FDTDNWLGlCQUFpQixDQUFDMUYsR0FBRyxFQUFFLG1CQUFtQixFQUFFO2dCQUFFb2MsS0FBSyxFQUFFUCxNQUFNLENBQUMzSyxLQUFLLENBQUM7Y0FBRSxDQUFDLENBQUM7WUFDeEU7WUFDQTJMLFlBQVksQ0FBQ2hCLE1BQU0sRUFBRUosY0FBYyxDQUFDO1VBQ3RDO1VBQ0EwQixZQUFZLENBQUN6USxJQUFJLENBQUM4USxXQUFXLENBQUM7UUFDaEM7TUFDRjtNQUNBLElBQUkzQixNQUFNLENBQUNuUyxNQUFNLEtBQUswVCxhQUFhLEVBQUU7UUFDbkMxWCxpQkFBaUIsQ0FBQzFGLEdBQUcsRUFBRSxtQkFBbUIsRUFBRTtVQUFFb2MsS0FBSyxFQUFFUCxNQUFNLENBQUMzSyxLQUFLLENBQUM7UUFBRSxDQUFDLENBQUM7TUFDeEU7TUFDQTJMLFlBQVksQ0FBQ2hCLE1BQU0sRUFBRUosY0FBYyxDQUFDO0lBQ3RDLENBQUMsUUFBUUksTUFBTSxDQUFDLENBQUMsQ0FBQyxLQUFLLEdBQUcsSUFBSUEsTUFBTSxDQUFDM0ssS0FBSyxDQUFDLENBQUM7SUFDNUMsSUFBSWdNLEtBQUssRUFBRTtNQUNUQSxLQUFLLENBQUNELGVBQWUsQ0FBQyxHQUFHRSxZQUFZO0lBQ3ZDO0lBQ0EsT0FBT0EsWUFBWTtFQUNyQjs7RUFFQTtBQUNGO0FBQ0E7QUFDQTtFQUNFLFNBQVNsWSxlQUFlQSxDQUFDakYsR0FBRyxFQUFFO0lBQzVCLE1BQU1pZCxlQUFlLEdBQUd2WSxpQkFBaUIsQ0FBQzFFLEdBQUcsRUFBRSxZQUFZLENBQUM7SUFDNUQsSUFBSW1kLFlBQVksR0FBRyxFQUFFO0lBQ3JCLElBQUlGLGVBQWUsRUFBRTtNQUNuQixNQUFNQyxLQUFLLEdBQUc3ZCxJQUFJLENBQUMwQixNQUFNLENBQUMrQixpQkFBaUI7TUFDM0NxYSxZQUFZLEdBQUlELEtBQUssSUFBSUEsS0FBSyxDQUFDRCxlQUFlLENBQUMsSUFBS0Qsb0JBQW9CLENBQUNoZCxHQUFHLEVBQUVpZCxlQUFlLEVBQUVDLEtBQUssQ0FBQztJQUN2RztJQUVBLElBQUlDLFlBQVksQ0FBQ3pULE1BQU0sR0FBRyxDQUFDLEVBQUU7TUFDM0IsT0FBT3lULFlBQVk7SUFDckIsQ0FBQyxNQUFNLElBQUkvVSxPQUFPLENBQUNwSSxHQUFHLEVBQUUsTUFBTSxDQUFDLEVBQUU7TUFDL0IsT0FBTyxDQUFDO1FBQUVOLE9BQU8sRUFBRTtNQUFTLENBQUMsQ0FBQztJQUNoQyxDQUFDLE1BQU0sSUFBSTBJLE9BQU8sQ0FBQ3BJLEdBQUcsRUFBRSw0Q0FBNEMsQ0FBQyxFQUFFO01BQ3JFLE9BQU8sQ0FBQztRQUFFTixPQUFPLEVBQUU7TUFBUSxDQUFDLENBQUM7SUFDL0IsQ0FBQyxNQUFNLElBQUkwSSxPQUFPLENBQUNwSSxHQUFHLEVBQUUrYyxjQUFjLENBQUMsRUFBRTtNQUN2QyxPQUFPLENBQUM7UUFBRXJkLE9BQU8sRUFBRTtNQUFTLENBQUMsQ0FBQztJQUNoQyxDQUFDLE1BQU07TUFDTCxPQUFPLENBQUM7UUFBRUEsT0FBTyxFQUFFO01BQVEsQ0FBQyxDQUFDO0lBQy9CO0VBQ0Y7O0VBRUE7QUFDRjtBQUNBO0VBQ0UsU0FBU3FlLGFBQWFBLENBQUMvZCxHQUFHLEVBQUU7SUFDMUIrRSxlQUFlLENBQUMvRSxHQUFHLENBQUMsQ0FBQ2dlLFNBQVMsR0FBRyxJQUFJO0VBQ3ZDOztFQUVBO0FBQ0Y7QUFDQTtBQUNBO0FBQ0E7RUFDRSxTQUFTQyxjQUFjQSxDQUFDamUsR0FBRyxFQUFFa2UsT0FBTyxFQUFFQyxJQUFJLEVBQUU7SUFDMUMsTUFBTUMsUUFBUSxHQUFHclosZUFBZSxDQUFDL0UsR0FBRyxDQUFDO0lBQ3JDb2UsUUFBUSxDQUFDbGMsT0FBTyxHQUFHZ04sU0FBUyxDQUFDLENBQUMsQ0FBQ0csVUFBVSxDQUFDLFlBQVc7TUFDbkQsSUFBSWhMLFlBQVksQ0FBQ3JFLEdBQUcsQ0FBQyxJQUFJb2UsUUFBUSxDQUFDSixTQUFTLEtBQUssSUFBSSxFQUFFO1FBQ3BELElBQUksQ0FBQ0ssZ0JBQWdCLENBQUNGLElBQUksRUFBRW5lLEdBQUcsRUFBRXNlLFNBQVMsQ0FBQyxpQkFBaUIsRUFBRTtVQUM1RGQsV0FBVyxFQUFFVyxJQUFJO1VBQ2pCM0wsTUFBTSxFQUFFeFM7UUFDVixDQUFDLENBQUMsQ0FBQyxFQUFFO1VBQ0hrZSxPQUFPLENBQUNsZSxHQUFHLENBQUM7UUFDZDtRQUNBaWUsY0FBYyxDQUFDamUsR0FBRyxFQUFFa2UsT0FBTyxFQUFFQyxJQUFJLENBQUM7TUFDcEM7SUFDRixDQUFDLEVBQUVBLElBQUksQ0FBQ2IsWUFBWSxDQUFDO0VBQ3ZCOztFQUVBO0FBQ0Y7QUFDQTtBQUNBO0VBQ0UsU0FBU2lCLFdBQVdBLENBQUN2ZSxHQUFHLEVBQUU7SUFDeEIsT0FBT3dlLFFBQVEsQ0FBQ0MsUUFBUSxLQUFLemUsR0FBRyxDQUFDeWUsUUFBUSxJQUN2Q2pZLGVBQWUsQ0FBQ3hHLEdBQUcsRUFBRSxNQUFNLENBQUMsSUFDNUJ3RyxlQUFlLENBQUN4RyxHQUFHLEVBQUUsTUFBTSxDQUFDLENBQUNnSSxPQUFPLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQztFQUNuRDs7RUFFQTtBQUNGO0FBQ0E7RUFDRSxTQUFTMFcsYUFBYUEsQ0FBQzFlLEdBQUcsRUFBRTtJQUMxQixPQUFPRixPQUFPLENBQUNFLEdBQUcsRUFBRVgsSUFBSSxDQUFDMEIsTUFBTSxDQUFDc0IsZUFBZSxDQUFDO0VBQ2xEOztFQUVBO0FBQ0Y7QUFDQTtBQUNBO0FBQ0E7RUFDRSxTQUFTc2MsWUFBWUEsQ0FBQzNlLEdBQUcsRUFBRW9lLFFBQVEsRUFBRWpCLFlBQVksRUFBRTtJQUNqRCxJQUFLbmQsR0FBRyxZQUFZNGUsaUJBQWlCLElBQUlMLFdBQVcsQ0FBQ3ZlLEdBQUcsQ0FBQyxLQUFLQSxHQUFHLENBQUN3UyxNQUFNLEtBQUssRUFBRSxJQUFJeFMsR0FBRyxDQUFDd1MsTUFBTSxLQUFLLE9BQU8sQ0FBQyxJQUFNeFMsR0FBRyxDQUFDMFYsT0FBTyxLQUFLLE1BQU0sSUFBSW1KLE1BQU0sQ0FBQ3JZLGVBQWUsQ0FBQ3hHLEdBQUcsRUFBRSxRQUFRLENBQUMsQ0FBQyxDQUFDaUosV0FBVyxDQUFDLENBQUMsS0FBSyxRQUFTLEVBQUU7TUFDNU1tVixRQUFRLENBQUM5SyxPQUFPLEdBQUcsSUFBSTtNQUN2QixJQUFJdk4sSUFBSSxFQUFFcUksSUFBSTtNQUNkLElBQUlwTyxHQUFHLENBQUMwVixPQUFPLEtBQUssR0FBRyxFQUFFO1FBQ3ZCM1AsSUFBSSxHQUFJLHFCQUFzQixLQUFPO1FBQ3JDcUksSUFBSSxHQUFHNUgsZUFBZSxDQUFDeEcsR0FBRyxFQUFFLE1BQU0sQ0FBQztNQUNyQyxDQUFDLE1BQU07UUFDTCxNQUFNOGUsWUFBWSxHQUFHdFksZUFBZSxDQUFDeEcsR0FBRyxFQUFFLFFBQVEsQ0FBQztRQUNuRCtGLElBQUksR0FBSSxxQkFBc0IrWSxZQUFZLEdBQUdBLFlBQVksQ0FBQzdWLFdBQVcsQ0FBQyxDQUFDLEdBQUcsS0FBTztRQUNqRm1GLElBQUksR0FBRzVILGVBQWUsQ0FBQ3hHLEdBQUcsRUFBRSxRQUFRLENBQUM7UUFDckMsSUFBSW9PLElBQUksSUFBSSxJQUFJLElBQUlBLElBQUksS0FBSyxFQUFFLEVBQUU7VUFDL0I7VUFDQTtVQUNBQSxJQUFJLEdBQUdsSCxXQUFXLENBQUMsQ0FBQyxDQUFDc1gsUUFBUSxDQUFDTyxJQUFJO1FBQ3BDO1FBQ0EsSUFBSWhaLElBQUksS0FBSyxLQUFLLElBQUlxSSxJQUFJLENBQUM0USxRQUFRLENBQUMsR0FBRyxDQUFDLEVBQUU7VUFDeEM1USxJQUFJLEdBQUdBLElBQUksQ0FBQ25ELE9BQU8sQ0FBQyxTQUFTLEVBQUUsRUFBRSxDQUFDO1FBQ3BDO01BQ0Y7TUFDQWtTLFlBQVksQ0FBQ25ULE9BQU8sQ0FBQyxVQUFTd1QsV0FBVyxFQUFFO1FBQ3pDM0ssZ0JBQWdCLENBQUM3UyxHQUFHLEVBQUUsVUFBUytQLElBQUksRUFBRW5CLEdBQUcsRUFBRTtVQUN4QyxNQUFNNU8sR0FBRyxHQUFHbUksU0FBUyxDQUFDNEgsSUFBSSxDQUFDO1VBQzNCLElBQUkyTyxhQUFhLENBQUMxZSxHQUFHLENBQUMsRUFBRTtZQUN0QnVYLGNBQWMsQ0FBQ3ZYLEdBQUcsQ0FBQztZQUNuQjtVQUNGO1VBQ0FpZixnQkFBZ0IsQ0FBQ2xaLElBQUksRUFBRXFJLElBQUksRUFBRXBPLEdBQUcsRUFBRTRPLEdBQUcsQ0FBQztRQUN4QyxDQUFDLEVBQUV3UCxRQUFRLEVBQUVaLFdBQVcsRUFBRSxJQUFJLENBQUM7TUFDakMsQ0FBQyxDQUFDO0lBQ0o7RUFDRjs7RUFFQTtBQUNGO0FBQ0E7QUFDQTtBQUNBO0VBQ0UsU0FBUy9YLFlBQVlBLENBQUNtSixHQUFHLEVBQUVtQixJQUFJLEVBQUU7SUFDL0IsTUFBTS9QLEdBQUcsR0FBR21JLFNBQVMsQ0FBQzRILElBQUksQ0FBQztJQUMzQixJQUFJLENBQUMvUCxHQUFHLEVBQUU7TUFDUixPQUFPLEtBQUs7SUFDZDtJQUNBLElBQUk0TyxHQUFHLENBQUMzTyxJQUFJLEtBQUssUUFBUSxJQUFJMk8sR0FBRyxDQUFDM08sSUFBSSxLQUFLLE9BQU8sRUFBRTtNQUNqRCxJQUFJRCxHQUFHLENBQUMwVixPQUFPLEtBQUssTUFBTSxFQUFFO1FBQzFCLE9BQU8sSUFBSTtNQUNiO01BQ0EsSUFBSXROLE9BQU8sQ0FBQ3BJLEdBQUcsRUFBRSw4QkFBOEIsQ0FBQyxLQUM3Q29JLE9BQU8sQ0FBQ3BJLEdBQUcsRUFBRSxRQUFRLENBQUMsSUFBSUYsT0FBTyxDQUFDRSxHQUFHLEVBQUUsTUFBTSxDQUFDLEtBQUssSUFBSSxDQUFDLEVBQUU7UUFDM0QsT0FBTyxJQUFJO01BQ2I7TUFDQSxJQUFJQSxHQUFHLFlBQVk0ZSxpQkFBaUIsSUFBSTVlLEdBQUcsQ0FBQytlLElBQUksS0FDN0MvZSxHQUFHLENBQUMyRyxZQUFZLENBQUMsTUFBTSxDQUFDLEtBQUssR0FBRyxJQUFJM0csR0FBRyxDQUFDMkcsWUFBWSxDQUFDLE1BQU0sQ0FBQyxDQUFDcUIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsQ0FBQyxFQUFFO1FBQ25GLE9BQU8sSUFBSTtNQUNiO0lBQ0Y7SUFDQSxPQUFPLEtBQUs7RUFDZDs7RUFFQTtBQUNGO0FBQ0E7QUFDQTtBQUNBO0VBQ0UsU0FBU2tYLDRCQUE0QkEsQ0FBQ2xmLEdBQUcsRUFBRTRPLEdBQUcsRUFBRTtJQUM5QyxPQUFPN0osZUFBZSxDQUFDL0UsR0FBRyxDQUFDLENBQUNzVCxPQUFPLElBQUl0VCxHQUFHLFlBQVk0ZSxpQkFBaUIsSUFBSWhRLEdBQUcsQ0FBQzNPLElBQUksS0FBSyxPQUFPO0lBQzdGO0lBQ0MyTyxHQUFHLENBQUN1USxPQUFPLElBQUl2USxHQUFHLENBQUN3USxPQUFPLENBQUM7RUFDaEM7O0VBRUE7QUFDRjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0VBQ0UsU0FBU2YsZ0JBQWdCQSxDQUFDYixXQUFXLEVBQUV4ZCxHQUFHLEVBQUU0TyxHQUFHLEVBQUU7SUFDL0MsTUFBTTJPLFdBQVcsR0FBR0MsV0FBVyxDQUFDRCxXQUFXO0lBQzNDLElBQUlBLFdBQVcsRUFBRTtNQUNmLElBQUk7UUFDRixPQUFPQSxXQUFXLENBQUMzVSxJQUFJLENBQUM1SSxHQUFHLEVBQUU0TyxHQUFHLENBQUMsS0FBSyxJQUFJO01BQzVDLENBQUMsQ0FBQyxPQUFPMUcsQ0FBQyxFQUFFO1FBQ1YsTUFBTTBVLE1BQU0sR0FBR1csV0FBVyxDQUFDWCxNQUFNO1FBQ2pDbFgsaUJBQWlCLENBQUN3QixXQUFXLENBQUMsQ0FBQyxDQUFDbUUsSUFBSSxFQUFFLHdCQUF3QixFQUFFO1VBQUVuSSxLQUFLLEVBQUVnRixDQUFDO1VBQUUwVTtRQUFPLENBQUMsQ0FBQztRQUNyRixPQUFPLElBQUk7TUFDYjtJQUNGO0lBQ0EsT0FBTyxLQUFLO0VBQ2Q7O0VBRUE7QUFDRjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7RUFDRSxTQUFTL0osZ0JBQWdCQSxDQUFDN1MsR0FBRyxFQUFFa2UsT0FBTyxFQUFFRSxRQUFRLEVBQUVaLFdBQVcsRUFBRTZCLGNBQWMsRUFBRTtJQUM3RSxNQUFNQyxXQUFXLEdBQUd2YSxlQUFlLENBQUMvRSxHQUFHLENBQUM7SUFDeEM7SUFDQSxJQUFJdWYsY0FBYztJQUNsQixJQUFJL0IsV0FBVyxDQUFDN1MsSUFBSSxFQUFFO01BQ3BCNFUsY0FBYyxHQUFHNU8sbUJBQW1CLENBQUMzUSxHQUFHLEVBQUV3ZCxXQUFXLENBQUM3UyxJQUFJLENBQUM7SUFDN0QsQ0FBQyxNQUFNO01BQ0w0VSxjQUFjLEdBQUcsQ0FBQ3ZmLEdBQUcsQ0FBQztJQUN4QjtJQUNBO0lBQ0EsSUFBSXdkLFdBQVcsQ0FBQ0MsT0FBTyxFQUFFO01BQ3ZCLElBQUksRUFBRSxXQUFXLElBQUk2QixXQUFXLENBQUMsRUFBRTtRQUNqQ0EsV0FBVyxDQUFDRSxTQUFTLEdBQUcsSUFBSUMsT0FBTyxDQUFDLENBQUM7TUFDdkM7TUFDQUYsY0FBYyxDQUFDdlYsT0FBTyxDQUFDLFVBQVMwVixhQUFhLEVBQUU7UUFDN0MsSUFBSSxDQUFDSixXQUFXLENBQUNFLFNBQVMsQ0FBQ0csR0FBRyxDQUFDbkMsV0FBVyxDQUFDLEVBQUU7VUFDM0M4QixXQUFXLENBQUNFLFNBQVMsQ0FBQ0ksR0FBRyxDQUFDcEMsV0FBVyxFQUFFLElBQUlpQyxPQUFPLENBQUMsQ0FBQyxDQUFDO1FBQ3ZEO1FBQ0E7UUFDQUgsV0FBVyxDQUFDRSxTQUFTLENBQUNLLEdBQUcsQ0FBQ3JDLFdBQVcsQ0FBQyxDQUFDb0MsR0FBRyxDQUFDRixhQUFhLEVBQUVBLGFBQWEsQ0FBQ3RWLEtBQUssQ0FBQztNQUNoRixDQUFDLENBQUM7SUFDSjtJQUNBSixPQUFPLENBQUN1VixjQUFjLEVBQUUsVUFBU0csYUFBYSxFQUFFO01BQzlDO01BQ0EsTUFBTUksYUFBYSxHQUFHLFNBQUFBLENBQVNsUixHQUFHLEVBQUU7UUFDbEMsSUFBSSxDQUFDdkssWUFBWSxDQUFDckUsR0FBRyxDQUFDLEVBQUU7VUFDdEIwZixhQUFhLENBQUMzTSxtQkFBbUIsQ0FBQ3lLLFdBQVcsQ0FBQzlkLE9BQU8sRUFBRW9nQixhQUFhLENBQUM7VUFDckU7UUFDRjtRQUNBLElBQUlaLDRCQUE0QixDQUFDbGYsR0FBRyxFQUFFNE8sR0FBRyxDQUFDLEVBQUU7VUFDMUM7UUFDRjtRQUNBLElBQUl5USxjQUFjLElBQUk1WixZQUFZLENBQUNtSixHQUFHLEVBQUU1TyxHQUFHLENBQUMsRUFBRTtVQUM1QzRPLEdBQUcsQ0FBQ21SLGNBQWMsQ0FBQyxDQUFDO1FBQ3RCO1FBQ0EsSUFBSTFCLGdCQUFnQixDQUFDYixXQUFXLEVBQUV4ZCxHQUFHLEVBQUU0TyxHQUFHLENBQUMsRUFBRTtVQUMzQztRQUNGO1FBQ0EsTUFBTW9SLFNBQVMsR0FBR2piLGVBQWUsQ0FBQzZKLEdBQUcsQ0FBQztRQUN0Q29SLFNBQVMsQ0FBQ3hDLFdBQVcsR0FBR0EsV0FBVztRQUNuQyxJQUFJd0MsU0FBUyxDQUFDQyxVQUFVLElBQUksSUFBSSxFQUFFO1VBQ2hDRCxTQUFTLENBQUNDLFVBQVUsR0FBRyxFQUFFO1FBQzNCO1FBQ0EsSUFBSUQsU0FBUyxDQUFDQyxVQUFVLENBQUNqWSxPQUFPLENBQUNoSSxHQUFHLENBQUMsR0FBRyxDQUFDLEVBQUU7VUFDekNnZ0IsU0FBUyxDQUFDQyxVQUFVLENBQUN2VCxJQUFJLENBQUMxTSxHQUFHLENBQUM7VUFDOUIsSUFBSXdkLFdBQVcsQ0FBQ0csT0FBTyxFQUFFO1lBQ3ZCL08sR0FBRyxDQUFDc1IsZUFBZSxDQUFDLENBQUM7VUFDdkI7VUFDQSxJQUFJMUMsV0FBVyxDQUFDaEwsTUFBTSxJQUFJNUQsR0FBRyxDQUFDNEQsTUFBTSxFQUFFO1lBQ3BDLElBQUksQ0FBQ3BLLE9BQU8sQ0FBQ0QsU0FBUyxDQUFDeUcsR0FBRyxDQUFDNEQsTUFBTSxDQUFDLEVBQUVnTCxXQUFXLENBQUNoTCxNQUFNLENBQUMsRUFBRTtjQUN2RDtZQUNGO1VBQ0Y7VUFDQSxJQUFJZ0wsV0FBVyxDQUFDRSxJQUFJLEVBQUU7WUFDcEIsSUFBSTRCLFdBQVcsQ0FBQ2EsYUFBYSxFQUFFO2NBQzdCO1lBQ0YsQ0FBQyxNQUFNO2NBQ0xiLFdBQVcsQ0FBQ2EsYUFBYSxHQUFHLElBQUk7WUFDbEM7VUFDRjtVQUNBLElBQUkzQyxXQUFXLENBQUNDLE9BQU8sRUFBRTtZQUN2QixNQUFNMU4sSUFBSSxHQUFHakIsS0FBSyxDQUFDMEQsTUFBTTtZQUN6QjtZQUNBLE1BQU1wSSxLQUFLLEdBQUcyRixJQUFJLENBQUMzRixLQUFLO1lBQ3hCLE1BQU1vVixTQUFTLEdBQUdGLFdBQVcsQ0FBQ0UsU0FBUyxDQUFDSyxHQUFHLENBQUNyQyxXQUFXLENBQUM7WUFDeEQsSUFBSWdDLFNBQVMsQ0FBQ0csR0FBRyxDQUFDNVAsSUFBSSxDQUFDLElBQUl5UCxTQUFTLENBQUNLLEdBQUcsQ0FBQzlQLElBQUksQ0FBQyxLQUFLM0YsS0FBSyxFQUFFO2NBQ3hEO1lBQ0Y7WUFDQW9WLFNBQVMsQ0FBQ0ksR0FBRyxDQUFDN1AsSUFBSSxFQUFFM0YsS0FBSyxDQUFDO1VBQzVCO1VBQ0EsSUFBSWtWLFdBQVcsQ0FBQ2MsT0FBTyxFQUFFO1lBQ3ZCakosWUFBWSxDQUFDbUksV0FBVyxDQUFDYyxPQUFPLENBQUM7VUFDbkM7VUFDQSxJQUFJZCxXQUFXLENBQUN6QixRQUFRLEVBQUU7WUFDeEI7VUFDRjtVQUVBLElBQUlMLFdBQVcsQ0FBQ0ssUUFBUSxHQUFHLENBQUMsRUFBRTtZQUM1QixJQUFJLENBQUN5QixXQUFXLENBQUN6QixRQUFRLEVBQUU7Y0FDekJsYSxZQUFZLENBQUMzRCxHQUFHLEVBQUUsY0FBYyxDQUFDO2NBQ2pDa2UsT0FBTyxDQUFDbGUsR0FBRyxFQUFFNE8sR0FBRyxDQUFDO2NBQ2pCMFEsV0FBVyxDQUFDekIsUUFBUSxHQUFHM08sU0FBUyxDQUFDLENBQUMsQ0FBQ0csVUFBVSxDQUFDLFlBQVc7Z0JBQ3ZEaVEsV0FBVyxDQUFDekIsUUFBUSxHQUFHLElBQUk7Y0FDN0IsQ0FBQyxFQUFFTCxXQUFXLENBQUNLLFFBQVEsQ0FBQztZQUMxQjtVQUNGLENBQUMsTUFBTSxJQUFJTCxXQUFXLENBQUNyTyxLQUFLLEdBQUcsQ0FBQyxFQUFFO1lBQ2hDbVEsV0FBVyxDQUFDYyxPQUFPLEdBQUdsUixTQUFTLENBQUMsQ0FBQyxDQUFDRyxVQUFVLENBQUMsWUFBVztjQUN0RDFMLFlBQVksQ0FBQzNELEdBQUcsRUFBRSxjQUFjLENBQUM7Y0FDakNrZSxPQUFPLENBQUNsZSxHQUFHLEVBQUU0TyxHQUFHLENBQUM7WUFDbkIsQ0FBQyxFQUFFNE8sV0FBVyxDQUFDck8sS0FBSyxDQUFDO1VBQ3ZCLENBQUMsTUFBTTtZQUNMeEwsWUFBWSxDQUFDM0QsR0FBRyxFQUFFLGNBQWMsQ0FBQztZQUNqQ2tlLE9BQU8sQ0FBQ2xlLEdBQUcsRUFBRTRPLEdBQUcsQ0FBQztVQUNuQjtRQUNGO01BQ0YsQ0FBQztNQUNELElBQUl3UCxRQUFRLENBQUNoSCxhQUFhLElBQUksSUFBSSxFQUFFO1FBQ2xDZ0gsUUFBUSxDQUFDaEgsYUFBYSxHQUFHLEVBQUU7TUFDN0I7TUFDQWdILFFBQVEsQ0FBQ2hILGFBQWEsQ0FBQzFLLElBQUksQ0FBQztRQUMxQmhOLE9BQU8sRUFBRThkLFdBQVcsQ0FBQzlkLE9BQU87UUFDNUIrUyxRQUFRLEVBQUVxTixhQUFhO1FBQ3ZCdGdCLEVBQUUsRUFBRWtnQjtNQUNOLENBQUMsQ0FBQztNQUNGQSxhQUFhLENBQUM3TSxnQkFBZ0IsQ0FBQzJLLFdBQVcsQ0FBQzlkLE9BQU8sRUFBRW9nQixhQUFhLENBQUM7SUFDcEUsQ0FBQyxDQUFDO0VBQ0o7RUFFQSxJQUFJTyxpQkFBaUIsR0FBRyxLQUFLLEVBQUM7RUFDOUIsSUFBSUMsYUFBYSxHQUFHLElBQUk7RUFDeEIsU0FBU0MsaUJBQWlCQSxDQUFBLEVBQUc7SUFDM0IsSUFBSSxDQUFDRCxhQUFhLEVBQUU7TUFDbEJBLGFBQWEsR0FBRyxTQUFBQSxDQUFBLEVBQVc7UUFDekJELGlCQUFpQixHQUFHLElBQUk7TUFDMUIsQ0FBQztNQUNEbFQsTUFBTSxDQUFDMEYsZ0JBQWdCLENBQUMsUUFBUSxFQUFFeU4sYUFBYSxDQUFDO01BQ2hEblQsTUFBTSxDQUFDMEYsZ0JBQWdCLENBQUMsUUFBUSxFQUFFeU4sYUFBYSxDQUFDO01BQ2hERSxXQUFXLENBQUMsWUFBVztRQUNyQixJQUFJSCxpQkFBaUIsRUFBRTtVQUNyQkEsaUJBQWlCLEdBQUcsS0FBSztVQUN6QnJXLE9BQU8sQ0FBQzlDLFdBQVcsQ0FBQyxDQUFDLENBQUMwRCxnQkFBZ0IsQ0FBQyx3REFBd0QsQ0FBQyxFQUFFLFVBQVM1SyxHQUFHLEVBQUU7WUFDOUd5Z0IsV0FBVyxDQUFDemdCLEdBQUcsQ0FBQztVQUNsQixDQUFDLENBQUM7UUFDSjtNQUNGLENBQUMsRUFBRSxHQUFHLENBQUM7SUFDVDtFQUNGOztFQUVBO0FBQ0Y7QUFDQTtFQUNFLFNBQVN5Z0IsV0FBV0EsQ0FBQ3pnQixHQUFHLEVBQUU7SUFDeEIsSUFBSSxDQUFDeUUsWUFBWSxDQUFDekUsR0FBRyxFQUFFLGtCQUFrQixDQUFDLElBQUkyTSxrQkFBa0IsQ0FBQzNNLEdBQUcsQ0FBQyxFQUFFO01BQ3JFQSxHQUFHLENBQUNtSyxZQUFZLENBQUMsa0JBQWtCLEVBQUUsTUFBTSxDQUFDO01BQzVDLE1BQU1pVSxRQUFRLEdBQUdyWixlQUFlLENBQUMvRSxHQUFHLENBQUM7TUFDckMsSUFBSW9lLFFBQVEsQ0FBQ3NDLFFBQVEsRUFBRTtRQUNyQi9jLFlBQVksQ0FBQzNELEdBQUcsRUFBRSxVQUFVLENBQUM7TUFDL0IsQ0FBQyxNQUFNO1FBQ0w7UUFDQUEsR0FBRyxDQUFDNlMsZ0JBQWdCLENBQUMsdUJBQXVCLEVBQUUsWUFBVztVQUFFbFAsWUFBWSxDQUFDM0QsR0FBRyxFQUFFLFVBQVUsQ0FBQztRQUFDLENBQUMsRUFBRTtVQUFFMGQsSUFBSSxFQUFFO1FBQUssQ0FBQyxDQUFDO01BQzdHO0lBQ0Y7RUFDRjs7RUFFQTs7RUFFQTtBQUNGO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7RUFDRSxTQUFTaUQsZUFBZUEsQ0FBQzNnQixHQUFHLEVBQUVrZSxPQUFPLEVBQUVFLFFBQVEsRUFBRWpQLEtBQUssRUFBRTtJQUN0RCxNQUFNeVIsSUFBSSxHQUFHLFNBQUFBLENBQUEsRUFBVztNQUN0QixJQUFJLENBQUN4QyxRQUFRLENBQUN5QyxNQUFNLEVBQUU7UUFDcEJ6QyxRQUFRLENBQUN5QyxNQUFNLEdBQUcsSUFBSTtRQUN0QmxkLFlBQVksQ0FBQzNELEdBQUcsRUFBRSxjQUFjLENBQUM7UUFDakNrZSxPQUFPLENBQUNsZSxHQUFHLENBQUM7TUFDZDtJQUNGLENBQUM7SUFDRCxJQUFJbVAsS0FBSyxHQUFHLENBQUMsRUFBRTtNQUNiRCxTQUFTLENBQUMsQ0FBQyxDQUFDRyxVQUFVLENBQUN1UixJQUFJLEVBQUV6UixLQUFLLENBQUM7SUFDckMsQ0FBQyxNQUFNO01BQ0x5UixJQUFJLENBQUMsQ0FBQztJQUNSO0VBQ0Y7O0VBRUE7QUFDRjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0VBQ0UsU0FBU0UsWUFBWUEsQ0FBQzlnQixHQUFHLEVBQUVvZSxRQUFRLEVBQUVqQixZQUFZLEVBQUU7SUFDakQsSUFBSTRELGNBQWMsR0FBRyxLQUFLO0lBQzFCL1csT0FBTyxDQUFDcEUsS0FBSyxFQUFFLFVBQVNHLElBQUksRUFBRTtNQUM1QixJQUFJdEIsWUFBWSxDQUFDekUsR0FBRyxFQUFFLEtBQUssR0FBRytGLElBQUksQ0FBQyxFQUFFO1FBQ25DLE1BQU1xSSxJQUFJLEdBQUcxSixpQkFBaUIsQ0FBQzFFLEdBQUcsRUFBRSxLQUFLLEdBQUcrRixJQUFJLENBQUM7UUFDakRnYixjQUFjLEdBQUcsSUFBSTtRQUNyQjNDLFFBQVEsQ0FBQ2hRLElBQUksR0FBR0EsSUFBSTtRQUNwQmdRLFFBQVEsQ0FBQ3JZLElBQUksR0FBR0EsSUFBSTtRQUNwQm9YLFlBQVksQ0FBQ25ULE9BQU8sQ0FBQyxVQUFTd1QsV0FBVyxFQUFFO1VBQ3pDcFosaUJBQWlCLENBQUNwRSxHQUFHLEVBQUV3ZCxXQUFXLEVBQUVZLFFBQVEsRUFBRSxVQUFTck8sSUFBSSxFQUFFbkIsR0FBRyxFQUFFO1lBQ2hFLE1BQU01TyxHQUFHLEdBQUdtSSxTQUFTLENBQUM0SCxJQUFJLENBQUM7WUFDM0IsSUFBSWpRLE9BQU8sQ0FBQ0UsR0FBRyxFQUFFWCxJQUFJLENBQUMwQixNQUFNLENBQUNzQixlQUFlLENBQUMsRUFBRTtjQUM3Q2tWLGNBQWMsQ0FBQ3ZYLEdBQUcsQ0FBQztjQUNuQjtZQUNGO1lBQ0FpZixnQkFBZ0IsQ0FBQ2xaLElBQUksRUFBRXFJLElBQUksRUFBRXBPLEdBQUcsRUFBRTRPLEdBQUcsQ0FBQztVQUN4QyxDQUFDLENBQUM7UUFDSixDQUFDLENBQUM7TUFDSjtJQUNGLENBQUMsQ0FBQztJQUNGLE9BQU9tUyxjQUFjO0VBQ3ZCOztFQUVBO0FBQ0Y7QUFDQTtBQUNBO0FBQ0E7O0VBRUU7QUFDRjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0VBQ0UsU0FBUzNjLGlCQUFpQkEsQ0FBQ3BFLEdBQUcsRUFBRXdkLFdBQVcsRUFBRVksUUFBUSxFQUFFRixPQUFPLEVBQUU7SUFDOUQsSUFBSVYsV0FBVyxDQUFDOWQsT0FBTyxLQUFLLFVBQVUsRUFBRTtNQUN0QzZnQixpQkFBaUIsQ0FBQyxDQUFDO01BQ25CMU4sZ0JBQWdCLENBQUM3UyxHQUFHLEVBQUVrZSxPQUFPLEVBQUVFLFFBQVEsRUFBRVosV0FBVyxDQUFDO01BQ3JEaUQsV0FBVyxDQUFDdFksU0FBUyxDQUFDbkksR0FBRyxDQUFDLENBQUM7SUFDN0IsQ0FBQyxNQUFNLElBQUl3ZCxXQUFXLENBQUM5ZCxPQUFPLEtBQUssV0FBVyxFQUFFO01BQzlDLE1BQU1zaEIsZUFBZSxHQUFHLENBQUMsQ0FBQztNQUMxQixJQUFJeEQsV0FBVyxDQUFDeUQsSUFBSSxFQUFFO1FBQ3BCRCxlQUFlLENBQUNDLElBQUksR0FBRzFiLGdCQUFnQixDQUFDdkYsR0FBRyxFQUFFd2QsV0FBVyxDQUFDeUQsSUFBSSxDQUFDO01BQ2hFO01BQ0EsSUFBSXpELFdBQVcsQ0FBQzBELFNBQVMsRUFBRTtRQUN6QkYsZUFBZSxDQUFDRSxTQUFTLEdBQUc1YSxVQUFVLENBQUNrWCxXQUFXLENBQUMwRCxTQUFTLENBQUM7TUFDL0Q7TUFDQSxNQUFNQyxRQUFRLEdBQUcsSUFBSUMsb0JBQW9CLENBQUMsVUFBU0MsT0FBTyxFQUFFO1FBQzFELEtBQUssSUFBSTVVLENBQUMsR0FBRyxDQUFDLEVBQUVBLENBQUMsR0FBRzRVLE9BQU8sQ0FBQzNYLE1BQU0sRUFBRStDLENBQUMsRUFBRSxFQUFFO1VBQ3ZDLE1BQU02VSxLQUFLLEdBQUdELE9BQU8sQ0FBQzVVLENBQUMsQ0FBQztVQUN4QixJQUFJNlUsS0FBSyxDQUFDQyxjQUFjLEVBQUU7WUFDeEI1ZCxZQUFZLENBQUMzRCxHQUFHLEVBQUUsV0FBVyxDQUFDO1lBQzlCO1VBQ0Y7UUFDRjtNQUNGLENBQUMsRUFBRWdoQixlQUFlLENBQUM7TUFDbkJHLFFBQVEsQ0FBQ0ssT0FBTyxDQUFDclosU0FBUyxDQUFDbkksR0FBRyxDQUFDLENBQUM7TUFDaEM2UyxnQkFBZ0IsQ0FBQzFLLFNBQVMsQ0FBQ25JLEdBQUcsQ0FBQyxFQUFFa2UsT0FBTyxFQUFFRSxRQUFRLEVBQUVaLFdBQVcsQ0FBQztJQUNsRSxDQUFDLE1BQU0sSUFBSSxDQUFDWSxRQUFRLENBQUNxRCxrQkFBa0IsSUFBSWpFLFdBQVcsQ0FBQzlkLE9BQU8sS0FBSyxNQUFNLEVBQUU7TUFDekUsSUFBSSxDQUFDMmUsZ0JBQWdCLENBQUNiLFdBQVcsRUFBRXhkLEdBQUcsRUFBRXNlLFNBQVMsQ0FBQyxNQUFNLEVBQUU7UUFBRXRlO01BQUksQ0FBQyxDQUFDLENBQUMsRUFBRTtRQUNuRTJnQixlQUFlLENBQUN4WSxTQUFTLENBQUNuSSxHQUFHLENBQUMsRUFBRWtlLE9BQU8sRUFBRUUsUUFBUSxFQUFFWixXQUFXLENBQUNyTyxLQUFLLENBQUM7TUFDdkU7SUFDRixDQUFDLE1BQU0sSUFBSXFPLFdBQVcsQ0FBQ0YsWUFBWSxHQUFHLENBQUMsRUFBRTtNQUN2Q2MsUUFBUSxDQUFDc0QsT0FBTyxHQUFHLElBQUk7TUFDdkJ6RCxjQUFjLENBQUM5VixTQUFTLENBQUNuSSxHQUFHLENBQUMsRUFBRWtlLE9BQU8sRUFBRVYsV0FBVyxDQUFDO0lBQ3RELENBQUMsTUFBTTtNQUNMM0ssZ0JBQWdCLENBQUM3UyxHQUFHLEVBQUVrZSxPQUFPLEVBQUVFLFFBQVEsRUFBRVosV0FBVyxDQUFDO0lBQ3ZEO0VBQ0Y7O0VBRUE7QUFDRjtBQUNBO0FBQ0E7RUFDRSxTQUFTbUUsaUJBQWlCQSxDQUFDNVIsSUFBSSxFQUFFO0lBQy9CLE1BQU0vUCxHQUFHLEdBQUdtSSxTQUFTLENBQUM0SCxJQUFJLENBQUM7SUFDM0IsSUFBSSxDQUFDL1AsR0FBRyxFQUFFO01BQ1IsT0FBTyxLQUFLO0lBQ2Q7SUFDQSxNQUFNaUssVUFBVSxHQUFHakssR0FBRyxDQUFDaUssVUFBVTtJQUNqQyxLQUFLLElBQUl1TyxDQUFDLEdBQUcsQ0FBQyxFQUFFQSxDQUFDLEdBQUd2TyxVQUFVLENBQUNQLE1BQU0sRUFBRThPLENBQUMsRUFBRSxFQUFFO01BQzFDLE1BQU10RixRQUFRLEdBQUdqSixVQUFVLENBQUN1TyxDQUFDLENBQUMsQ0FBQy9SLElBQUk7TUFDbkMsSUFBSTJKLFVBQVUsQ0FBQzhDLFFBQVEsRUFBRSxRQUFRLENBQUMsSUFBSTlDLFVBQVUsQ0FBQzhDLFFBQVEsRUFBRSxhQUFhLENBQUMsSUFDdkU5QyxVQUFVLENBQUM4QyxRQUFRLEVBQUUsUUFBUSxDQUFDLElBQUk5QyxVQUFVLENBQUM4QyxRQUFRLEVBQUUsYUFBYSxDQUFDLEVBQUU7UUFDdkUsT0FBTyxJQUFJO01BQ2I7SUFDRjtJQUNBLE9BQU8sS0FBSztFQUNkOztFQUVBO0FBQ0Y7QUFDQTtBQUNBO0VBQ0UsTUFBTTBPLFdBQVcsR0FBRyxJQUFJQyxjQUFjLENBQUMsQ0FBQyxDQUNyQ0MsZ0JBQWdCLENBQUMsaUZBQWlGLEdBQ2pHLHlFQUF5RSxDQUFDO0VBRTlFLFNBQVNDLGVBQWVBLENBQUMvaEIsR0FBRyxFQUFFZ2lCLFFBQVEsRUFBRTtJQUN0QyxJQUFJTCxpQkFBaUIsQ0FBQzNoQixHQUFHLENBQUMsRUFBRTtNQUMxQmdpQixRQUFRLENBQUN0VixJQUFJLENBQUN2RSxTQUFTLENBQUNuSSxHQUFHLENBQUMsQ0FBQztJQUMvQjtJQUNBLE1BQU1paUIsSUFBSSxHQUFHTCxXQUFXLENBQUNNLFFBQVEsQ0FBQ2xpQixHQUFHLENBQUM7SUFDdEMsSUFBSStQLElBQUksR0FBRyxJQUFJO0lBQ2YsT0FBT0EsSUFBSSxHQUFHa1MsSUFBSSxDQUFDRSxXQUFXLENBQUMsQ0FBQyxFQUFFSCxRQUFRLENBQUN0VixJQUFJLENBQUN2RSxTQUFTLENBQUM0SCxJQUFJLENBQUMsQ0FBQztFQUNsRTtFQUVBLFNBQVNxUyx3QkFBd0JBLENBQUNwaUIsR0FBRyxFQUFFO0lBQ3JDO0lBQ0EsTUFBTWdpQixRQUFRLEdBQUcsRUFBRTtJQUNuQixJQUFJaGlCLEdBQUcsWUFBWW1MLGdCQUFnQixFQUFFO01BQ25DLEtBQUssTUFBTWdGLEtBQUssSUFBSW5RLEdBQUcsQ0FBQ3lKLFVBQVUsRUFBRTtRQUNsQ3NZLGVBQWUsQ0FBQzVSLEtBQUssRUFBRTZSLFFBQVEsQ0FBQztNQUNsQztJQUNGLENBQUMsTUFBTTtNQUNMRCxlQUFlLENBQUMvaEIsR0FBRyxFQUFFZ2lCLFFBQVEsQ0FBQztJQUNoQztJQUNBLE9BQU9BLFFBQVE7RUFDakI7O0VBRUE7QUFDRjtBQUNBO0FBQ0E7RUFDRSxTQUFTSyxxQkFBcUJBLENBQUNyaUIsR0FBRyxFQUFFO0lBQ2xDLElBQUlBLEdBQUcsQ0FBQzRLLGdCQUFnQixFQUFFO01BQ3hCLE1BQU0wWCxlQUFlLEdBQUcsa0VBQWtFO01BRTFGLE1BQU1DLGtCQUFrQixHQUFHLEVBQUU7TUFDN0IsS0FBSyxNQUFNcmEsQ0FBQyxJQUFJMkwsVUFBVSxFQUFFO1FBQzFCLE1BQU1FLFNBQVMsR0FBR0YsVUFBVSxDQUFDM0wsQ0FBQyxDQUFDO1FBQy9CLElBQUk2TCxTQUFTLENBQUN5TyxZQUFZLEVBQUU7VUFDMUIsSUFBSUMsU0FBUyxHQUFHMU8sU0FBUyxDQUFDeU8sWUFBWSxDQUFDLENBQUM7VUFDeEMsSUFBSUMsU0FBUyxFQUFFO1lBQ2JGLGtCQUFrQixDQUFDN1YsSUFBSSxDQUFDK1YsU0FBUyxDQUFDO1VBQ3BDO1FBQ0Y7TUFDRjtNQUVBLE1BQU01USxPQUFPLEdBQUc3UixHQUFHLENBQUM0SyxnQkFBZ0IsQ0FBQy9FLGFBQWEsR0FBR3ljLGVBQWUsR0FBRywwQkFBMEIsR0FDL0YsMkRBQTJELEdBQUdDLGtCQUFrQixDQUFDRyxJQUFJLENBQUMsQ0FBQyxDQUFDNWMsR0FBRyxDQUFDNmMsQ0FBQyxJQUFJLElBQUksR0FBR0EsQ0FBQyxDQUFDLENBQUMzYyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUM7TUFFdEgsT0FBTzZMLE9BQU87SUFDaEIsQ0FBQyxNQUFNO01BQ0wsT0FBTyxFQUFFO0lBQ1g7RUFDRjs7RUFFQTtBQUNGO0FBQ0E7QUFDQTtBQUNBO0VBQ0UsU0FBUytRLHlCQUF5QkEsQ0FBQ2hVLEdBQUcsRUFBRTtJQUN0QyxNQUFNNU8sR0FBRyxHQUFHLGlEQUFtREYsT0FBTyxDQUFDcUksU0FBUyxDQUFDeUcsR0FBRyxDQUFDNEQsTUFBTSxDQUFDLEVBQUUsOEJBQThCLENBQUU7SUFDOUgsTUFBTXNFLFlBQVksR0FBRytMLGtCQUFrQixDQUFDalUsR0FBRyxDQUFDO0lBQzVDLElBQUlrSSxZQUFZLEVBQUU7TUFDaEJBLFlBQVksQ0FBQ2dNLGlCQUFpQixHQUFHOWlCLEdBQUc7SUFDdEM7RUFDRjs7RUFFQTtBQUNGO0FBQ0E7RUFDRSxTQUFTK2lCLDJCQUEyQkEsQ0FBQ25VLEdBQUcsRUFBRTtJQUN4QyxNQUFNa0ksWUFBWSxHQUFHK0wsa0JBQWtCLENBQUNqVSxHQUFHLENBQUM7SUFDNUMsSUFBSWtJLFlBQVksRUFBRTtNQUNoQkEsWUFBWSxDQUFDZ00saUJBQWlCLEdBQUcsSUFBSTtJQUN2QztFQUNGOztFQUVBO0FBQ0Y7QUFDQTtBQUNBO0VBQ0UsU0FBU0Qsa0JBQWtCQSxDQUFDalUsR0FBRyxFQUFFO0lBQy9CLE1BQU01TyxHQUFHLEdBQUdGLE9BQU8sQ0FBQ3FJLFNBQVMsQ0FBQ3lHLEdBQUcsQ0FBQzRELE1BQU0sQ0FBQyxFQUFFLDhCQUE4QixDQUFDO0lBQzFFLElBQUksQ0FBQ3hTLEdBQUcsRUFBRTtNQUNSO0lBQ0Y7SUFDQSxNQUFNZ2pCLElBQUksR0FBRzVULGFBQWEsQ0FBQyxHQUFHLEdBQUc1SSxlQUFlLENBQUN4RyxHQUFHLEVBQUUsTUFBTSxDQUFDLEVBQUVBLEdBQUcsQ0FBQ29ILFdBQVcsQ0FBQyxDQUFDLENBQUMsSUFBSXRILE9BQU8sQ0FBQ0UsR0FBRyxFQUFFLE1BQU0sQ0FBQztJQUN6RyxJQUFJLENBQUNnakIsSUFBSSxFQUFFO01BQ1Q7SUFDRjtJQUNBLE9BQU9qZSxlQUFlLENBQUNpZSxJQUFJLENBQUM7RUFDOUI7O0VBRUE7QUFDRjtBQUNBO0VBQ0UsU0FBU0Msa0JBQWtCQSxDQUFDampCLEdBQUcsRUFBRTtJQUMvQjtJQUNBO0lBQ0E7SUFDQUEsR0FBRyxDQUFDNlMsZ0JBQWdCLENBQUMsT0FBTyxFQUFFK1AseUJBQXlCLENBQUM7SUFDeEQ1aUIsR0FBRyxDQUFDNlMsZ0JBQWdCLENBQUMsU0FBUyxFQUFFK1AseUJBQXlCLENBQUM7SUFDMUQ1aUIsR0FBRyxDQUFDNlMsZ0JBQWdCLENBQUMsVUFBVSxFQUFFa1EsMkJBQTJCLENBQUM7RUFDL0Q7O0VBRUE7QUFDRjtBQUNBO0FBQ0E7QUFDQTtFQUNFLFNBQVNHLG1CQUFtQkEsQ0FBQ2xqQixHQUFHLEVBQUVrYixTQUFTLEVBQUVqWSxJQUFJLEVBQUU7SUFDakQsTUFBTW1iLFFBQVEsR0FBR3JaLGVBQWUsQ0FBQy9FLEdBQUcsQ0FBQztJQUNyQyxJQUFJLENBQUMwSyxLQUFLLENBQUM2TixPQUFPLENBQUM2RixRQUFRLENBQUNySCxVQUFVLENBQUMsRUFBRTtNQUN2Q3FILFFBQVEsQ0FBQ3JILFVBQVUsR0FBRyxFQUFFO0lBQzFCO0lBQ0EsSUFBSW5MLElBQUk7SUFDUjtJQUNBLE1BQU02RyxRQUFRLEdBQUcsU0FBQUEsQ0FBU3ZLLENBQUMsRUFBRTtNQUMzQnVHLFNBQVMsQ0FBQ3pPLEdBQUcsRUFBRSxZQUFXO1FBQ3hCLElBQUkwZSxhQUFhLENBQUMxZSxHQUFHLENBQUMsRUFBRTtVQUN0QjtRQUNGO1FBQ0EsSUFBSSxDQUFDNEwsSUFBSSxFQUFFO1VBQ1RBLElBQUksR0FBRyxJQUFJK1EsUUFBUSxDQUFDLE9BQU8sRUFBRTFaLElBQUksQ0FBQztRQUNwQztRQUNBMkksSUFBSSxDQUFDaEQsSUFBSSxDQUFDNUksR0FBRyxFQUFFa0ksQ0FBQyxDQUFDO01BQ25CLENBQUMsQ0FBQztJQUNKLENBQUM7SUFDRGxJLEdBQUcsQ0FBQzZTLGdCQUFnQixDQUFDcUksU0FBUyxFQUFFekksUUFBUSxDQUFDO0lBQ3pDMkwsUUFBUSxDQUFDckgsVUFBVSxDQUFDckssSUFBSSxDQUFDO01BQUVvQyxLQUFLLEVBQUVvTSxTQUFTO01BQUV6STtJQUFTLENBQUMsQ0FBQztFQUMxRDs7RUFFQTtBQUNGO0FBQ0E7RUFDRSxTQUFTMFEsbUJBQW1CQSxDQUFDbmpCLEdBQUcsRUFBRTtJQUNoQztJQUNBNlcsZ0JBQWdCLENBQUM3VyxHQUFHLENBQUM7SUFFckIsS0FBSyxJQUFJeU0sQ0FBQyxHQUFHLENBQUMsRUFBRUEsQ0FBQyxHQUFHek0sR0FBRyxDQUFDaUssVUFBVSxDQUFDUCxNQUFNLEVBQUUrQyxDQUFDLEVBQUUsRUFBRTtNQUM5QyxNQUFNaEcsSUFBSSxHQUFHekcsR0FBRyxDQUFDaUssVUFBVSxDQUFDd0MsQ0FBQyxDQUFDLENBQUNoRyxJQUFJO01BQ25DLE1BQU0yRCxLQUFLLEdBQUdwSyxHQUFHLENBQUNpSyxVQUFVLENBQUN3QyxDQUFDLENBQUMsQ0FBQ3JDLEtBQUs7TUFDckMsSUFBSWdHLFVBQVUsQ0FBQzNKLElBQUksRUFBRSxPQUFPLENBQUMsSUFBSTJKLFVBQVUsQ0FBQzNKLElBQUksRUFBRSxZQUFZLENBQUMsRUFBRTtRQUMvRCxNQUFNMmMsZUFBZSxHQUFHM2MsSUFBSSxDQUFDdUIsT0FBTyxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUM7UUFDL0MsTUFBTXFiLFFBQVEsR0FBRzVjLElBQUksQ0FBQ0osS0FBSyxDQUFDK2MsZUFBZSxFQUFFQSxlQUFlLEdBQUcsQ0FBQyxDQUFDO1FBQ2pFLElBQUlDLFFBQVEsS0FBSyxHQUFHLElBQUlBLFFBQVEsS0FBSyxHQUFHLEVBQUU7VUFDeEMsSUFBSW5JLFNBQVMsR0FBR3pVLElBQUksQ0FBQ0osS0FBSyxDQUFDK2MsZUFBZSxHQUFHLENBQUMsQ0FBQztVQUMvQztVQUNBLElBQUloVCxVQUFVLENBQUM4SyxTQUFTLEVBQUUsR0FBRyxDQUFDLEVBQUU7WUFDOUJBLFNBQVMsR0FBRyxNQUFNLEdBQUdBLFNBQVM7VUFDaEMsQ0FBQyxNQUFNLElBQUk5SyxVQUFVLENBQUM4SyxTQUFTLEVBQUUsR0FBRyxDQUFDLEVBQUU7WUFDckNBLFNBQVMsR0FBRyxPQUFPLEdBQUdBLFNBQVMsQ0FBQzdVLEtBQUssQ0FBQyxDQUFDLENBQUM7VUFDMUMsQ0FBQyxNQUFNLElBQUkrSixVQUFVLENBQUM4SyxTQUFTLEVBQUUsT0FBTyxDQUFDLEVBQUU7WUFDekNBLFNBQVMsR0FBRyxPQUFPLEdBQUdBLFNBQVMsQ0FBQzdVLEtBQUssQ0FBQyxDQUFDLENBQUM7VUFDMUM7VUFFQTZjLG1CQUFtQixDQUFDbGpCLEdBQUcsRUFBRWtiLFNBQVMsRUFBRTlRLEtBQUssQ0FBQztRQUM1QztNQUNGO0lBQ0Y7RUFDRjs7RUFFQTtBQUNGO0FBQ0E7RUFDRSxTQUFTa1osUUFBUUEsQ0FBQ3RqQixHQUFHLEVBQUU7SUFDckIsSUFBSUYsT0FBTyxDQUFDRSxHQUFHLEVBQUVYLElBQUksQ0FBQzBCLE1BQU0sQ0FBQ3NCLGVBQWUsQ0FBQyxFQUFFO01BQzdDa1YsY0FBYyxDQUFDdlgsR0FBRyxDQUFDO01BQ25CO0lBQ0Y7SUFDQSxNQUFNb2UsUUFBUSxHQUFHclosZUFBZSxDQUFDL0UsR0FBRyxDQUFDO0lBQ3JDLE1BQU11akIsUUFBUSxHQUFHM00sYUFBYSxDQUFDNVcsR0FBRyxDQUFDO0lBQ25DLElBQUlvZSxRQUFRLENBQUNzQyxRQUFRLEtBQUs2QyxRQUFRLEVBQUU7TUFDbEM7TUFDQXRNLFVBQVUsQ0FBQ2pYLEdBQUcsQ0FBQztNQUVmb2UsUUFBUSxDQUFDc0MsUUFBUSxHQUFHNkMsUUFBUTtNQUU1QjVmLFlBQVksQ0FBQzNELEdBQUcsRUFBRSx3QkFBd0IsQ0FBQztNQUUzQyxNQUFNbWQsWUFBWSxHQUFHbFksZUFBZSxDQUFDakYsR0FBRyxDQUFDO01BQ3pDLE1BQU13akIscUJBQXFCLEdBQUcxQyxZQUFZLENBQUM5Z0IsR0FBRyxFQUFFb2UsUUFBUSxFQUFFakIsWUFBWSxDQUFDO01BRXZFLElBQUksQ0FBQ3FHLHFCQUFxQixFQUFFO1FBQzFCLElBQUk3ZSx3QkFBd0IsQ0FBQzNFLEdBQUcsRUFBRSxVQUFVLENBQUMsS0FBSyxNQUFNLEVBQUU7VUFDeEQyZSxZQUFZLENBQUMzZSxHQUFHLEVBQUVvZSxRQUFRLEVBQUVqQixZQUFZLENBQUM7UUFDM0MsQ0FBQyxNQUFNLElBQUkxWSxZQUFZLENBQUN6RSxHQUFHLEVBQUUsWUFBWSxDQUFDLEVBQUU7VUFDMUNtZCxZQUFZLENBQUNuVCxPQUFPLENBQUMsVUFBU3dULFdBQVcsRUFBRTtZQUN6QztZQUNBcFosaUJBQWlCLENBQUNwRSxHQUFHLEVBQUV3ZCxXQUFXLEVBQUVZLFFBQVEsRUFBRSxZQUFXLENBQ3pELENBQUMsQ0FBQztVQUNKLENBQUMsQ0FBQztRQUNKO01BQ0Y7O01BRUE7TUFDQTtNQUNBLElBQUlwZSxHQUFHLENBQUMwVixPQUFPLEtBQUssTUFBTSxJQUFLbFAsZUFBZSxDQUFDeEcsR0FBRyxFQUFFLE1BQU0sQ0FBQyxLQUFLLFFBQVEsSUFBSXlFLFlBQVksQ0FBQ3pFLEdBQUcsRUFBRSxNQUFNLENBQUUsRUFBRTtRQUN0R2lqQixrQkFBa0IsQ0FBQ2pqQixHQUFHLENBQUM7TUFDekI7TUFFQW9lLFFBQVEsQ0FBQ3FELGtCQUFrQixHQUFHLElBQUk7TUFDbEM5ZCxZQUFZLENBQUMzRCxHQUFHLEVBQUUsdUJBQXVCLENBQUM7SUFDNUM7RUFDRjs7RUFFQTtBQUNGO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtFQUNFLFNBQVN3RCxXQUFXQSxDQUFDeEQsR0FBRyxFQUFFO0lBQ3hCQSxHQUFHLEdBQUdvUCxhQUFhLENBQUNwUCxHQUFHLENBQUM7SUFDeEIsSUFBSUYsT0FBTyxDQUFDRSxHQUFHLEVBQUVYLElBQUksQ0FBQzBCLE1BQU0sQ0FBQ3NCLGVBQWUsQ0FBQyxFQUFFO01BQzdDa1YsY0FBYyxDQUFDdlgsR0FBRyxDQUFDO01BQ25CO0lBQ0Y7SUFDQXNqQixRQUFRLENBQUN0akIsR0FBRyxDQUFDO0lBQ2JnSyxPQUFPLENBQUNxWSxxQkFBcUIsQ0FBQ3JpQixHQUFHLENBQUMsRUFBRSxVQUFTbVEsS0FBSyxFQUFFO01BQUVtVCxRQUFRLENBQUNuVCxLQUFLLENBQUM7SUFBQyxDQUFDLENBQUM7SUFDeEVuRyxPQUFPLENBQUNvWSx3QkFBd0IsQ0FBQ3BpQixHQUFHLENBQUMsRUFBRW1qQixtQkFBbUIsQ0FBQztFQUM3RDs7RUFFQTtFQUNBO0VBQ0E7O0VBRUE7QUFDRjtBQUNBO0FBQ0E7RUFDRSxTQUFTTSxjQUFjQSxDQUFDeGQsR0FBRyxFQUFFO0lBQzNCLE9BQU9BLEdBQUcsQ0FBQ2dGLE9BQU8sQ0FBQyxvQkFBb0IsRUFBRSxPQUFPLENBQUMsQ0FBQ2hDLFdBQVcsQ0FBQyxDQUFDO0VBQ2pFOztFQUVBO0FBQ0Y7QUFDQTtBQUNBO0FBQ0E7RUFDRSxTQUFTcVYsU0FBU0EsQ0FBQ3BELFNBQVMsRUFBRXJNLE1BQU0sRUFBRTtJQUNwQyxJQUFJRCxHQUFHO0lBQ1AsSUFBSXpCLE1BQU0sQ0FBQ3VXLFdBQVcsSUFBSSxPQUFPdlcsTUFBTSxDQUFDdVcsV0FBVyxLQUFLLFVBQVUsRUFBRTtNQUNsRTtNQUNBO01BQ0E5VSxHQUFHLEdBQUcsSUFBSThVLFdBQVcsQ0FBQ3hJLFNBQVMsRUFBRTtRQUFFeUksT0FBTyxFQUFFLElBQUk7UUFBRUMsVUFBVSxFQUFFLElBQUk7UUFBRXRjLFFBQVEsRUFBRSxJQUFJO1FBQUV1SDtNQUFPLENBQUMsQ0FBQztJQUMvRixDQUFDLE1BQU07TUFDTEQsR0FBRyxHQUFHMUgsV0FBVyxDQUFDLENBQUMsQ0FBQzJjLFdBQVcsQ0FBQyxhQUFhLENBQUM7TUFDOUNqVixHQUFHLENBQUNrVixlQUFlLENBQUM1SSxTQUFTLEVBQUUsSUFBSSxFQUFFLElBQUksRUFBRXJNLE1BQU0sQ0FBQztJQUNwRDtJQUNBLE9BQU9ELEdBQUc7RUFDWjs7RUFFQTtBQUNGO0FBQ0E7QUFDQTtBQUNBO0VBQ0UsU0FBU2xKLGlCQUFpQkEsQ0FBQzFGLEdBQUcsRUFBRWtiLFNBQVMsRUFBRXJNLE1BQU0sRUFBRTtJQUNqRGxMLFlBQVksQ0FBQzNELEdBQUcsRUFBRWtiLFNBQVMsRUFBRTlWLFlBQVksQ0FBQztNQUFFbEMsS0FBSyxFQUFFZ1k7SUFBVSxDQUFDLEVBQUVyTSxNQUFNLENBQUMsQ0FBQztFQUMxRTs7RUFFQTtBQUNGO0FBQ0E7QUFDQTtFQUNFLFNBQVNrVixxQkFBcUJBLENBQUM3SSxTQUFTLEVBQUU7SUFDeEMsT0FBT0EsU0FBUyxLQUFLLHVCQUF1QjtFQUM5Qzs7RUFFQTtBQUNGO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7RUFDRSxTQUFTdlYsY0FBY0EsQ0FBQzNGLEdBQUcsRUFBRWdrQixJQUFJLEVBQUU7SUFDakNoYSxPQUFPLENBQUM4SixhQUFhLENBQUM5VCxHQUFHLENBQUMsRUFBRSxVQUFTK1QsU0FBUyxFQUFFO01BQzlDLElBQUk7UUFDRmlRLElBQUksQ0FBQ2pRLFNBQVMsQ0FBQztNQUNqQixDQUFDLENBQUMsT0FBTzdMLENBQUMsRUFBRTtRQUNWNEMsUUFBUSxDQUFDNUMsQ0FBQyxDQUFDO01BQ2I7SUFDRixDQUFDLENBQUM7RUFDSjtFQUVBLFNBQVM0QyxRQUFRQSxDQUFDbVosR0FBRyxFQUFFO0lBQ3JCLElBQUlsVixPQUFPLENBQUM3TCxLQUFLLEVBQUU7TUFDakI2TCxPQUFPLENBQUM3TCxLQUFLLENBQUMrZ0IsR0FBRyxDQUFDO0lBQ3BCLENBQUMsTUFBTSxJQUFJbFYsT0FBTyxDQUFDQyxHQUFHLEVBQUU7TUFDdEJELE9BQU8sQ0FBQ0MsR0FBRyxDQUFDLFNBQVMsRUFBRWlWLEdBQUcsQ0FBQztJQUM3QjtFQUNGOztFQUVBO0FBQ0Y7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0VBQ0UsU0FBU3RnQixZQUFZQSxDQUFDM0QsR0FBRyxFQUFFa2IsU0FBUyxFQUFFck0sTUFBTSxFQUFFO0lBQzVDN08sR0FBRyxHQUFHb1AsYUFBYSxDQUFDcFAsR0FBRyxDQUFDO0lBQ3hCLElBQUk2TyxNQUFNLElBQUksSUFBSSxFQUFFO01BQ2xCQSxNQUFNLEdBQUcsQ0FBQyxDQUFDO0lBQ2I7SUFDQUEsTUFBTSxDQUFDN08sR0FBRyxHQUFHQSxHQUFHO0lBQ2hCLE1BQU04TyxLQUFLLEdBQUd3UCxTQUFTLENBQUNwRCxTQUFTLEVBQUVyTSxNQUFNLENBQUM7SUFDMUMsSUFBSXhQLElBQUksQ0FBQ3lCLE1BQU0sSUFBSSxDQUFDaWpCLHFCQUFxQixDQUFDN0ksU0FBUyxDQUFDLEVBQUU7TUFDcEQ3YixJQUFJLENBQUN5QixNQUFNLENBQUNkLEdBQUcsRUFBRWtiLFNBQVMsRUFBRXJNLE1BQU0sQ0FBQztJQUNyQztJQUNBLElBQUlBLE1BQU0sQ0FBQzNMLEtBQUssRUFBRTtNQUNoQjRILFFBQVEsQ0FBQytELE1BQU0sQ0FBQzNMLEtBQUssQ0FBQztNQUN0QlMsWUFBWSxDQUFDM0QsR0FBRyxFQUFFLFlBQVksRUFBRTtRQUFFa2tCLFNBQVMsRUFBRXJWO01BQU8sQ0FBQyxDQUFDO0lBQ3hEO0lBQ0EsSUFBSXNWLFdBQVcsR0FBR25rQixHQUFHLENBQUNva0IsYUFBYSxDQUFDdFYsS0FBSyxDQUFDO0lBQzFDLE1BQU11VixTQUFTLEdBQUdaLGNBQWMsQ0FBQ3ZJLFNBQVMsQ0FBQztJQUMzQyxJQUFJaUosV0FBVyxJQUFJRSxTQUFTLEtBQUtuSixTQUFTLEVBQUU7TUFDMUMsTUFBTW9KLFlBQVksR0FBR2hHLFNBQVMsQ0FBQytGLFNBQVMsRUFBRXZWLEtBQUssQ0FBQ0QsTUFBTSxDQUFDO01BQ3ZEc1YsV0FBVyxHQUFHQSxXQUFXLElBQUlua0IsR0FBRyxDQUFDb2tCLGFBQWEsQ0FBQ0UsWUFBWSxDQUFDO0lBQzlEO0lBQ0EzZSxjQUFjLENBQUN3QyxTQUFTLENBQUNuSSxHQUFHLENBQUMsRUFBRSxVQUFTK1QsU0FBUyxFQUFFO01BQ2pEb1EsV0FBVyxHQUFHQSxXQUFXLElBQUtwUSxTQUFTLENBQUN3USxPQUFPLENBQUNySixTQUFTLEVBQUVwTSxLQUFLLENBQUMsS0FBSyxLQUFLLElBQUksQ0FBQ0EsS0FBSyxDQUFDMFYsZ0JBQWlCO0lBQ3pHLENBQUMsQ0FBQztJQUNGLE9BQU9MLFdBQVc7RUFDcEI7O0VBRUE7RUFDQTtFQUNBO0VBQ0EsSUFBSU0scUJBQXFCLEdBQUdqRyxRQUFRLENBQUNqUSxRQUFRLEdBQUdpUSxRQUFRLENBQUNoUSxNQUFNOztFQUUvRDtBQUNGO0FBQ0E7RUFDRSxTQUFTa1csaUJBQWlCQSxDQUFBLEVBQUc7SUFDM0IsTUFBTUMsVUFBVSxHQUFHemQsV0FBVyxDQUFDLENBQUMsQ0FBQ3FFLGFBQWEsQ0FBQyx3Q0FBd0MsQ0FBQztJQUN4RixPQUFPb1osVUFBVSxJQUFJemQsV0FBVyxDQUFDLENBQUMsQ0FBQ21FLElBQUk7RUFDekM7O0VBRUE7QUFDRjtBQUNBO0FBQ0E7RUFDRSxTQUFTdVosa0JBQWtCQSxDQUFDdlcsR0FBRyxFQUFFd1csT0FBTyxFQUFFO0lBQ3hDLElBQUksQ0FBQ3ZnQixxQkFBcUIsQ0FBQyxDQUFDLEVBQUU7TUFDNUI7SUFDRjs7SUFFQTtJQUNBLE1BQU13Z0IsU0FBUyxHQUFHQyx3QkFBd0IsQ0FBQ0YsT0FBTyxDQUFDO0lBQ25ELE1BQU12WixLQUFLLEdBQUdwRSxXQUFXLENBQUMsQ0FBQyxDQUFDb0UsS0FBSztJQUNqQyxNQUFNMFosTUFBTSxHQUFHN1gsTUFBTSxDQUFDOFgsT0FBTztJQUU3QixJQUFJNWxCLElBQUksQ0FBQzBCLE1BQU0sQ0FBQ0UsZ0JBQWdCLElBQUksQ0FBQyxFQUFFO01BQ3JDO01BQ0ErTSxZQUFZLENBQUNFLFVBQVUsQ0FBQyxvQkFBb0IsQ0FBQztNQUM3QztJQUNGO0lBRUFHLEdBQUcsR0FBR0YsYUFBYSxDQUFDRSxHQUFHLENBQUM7SUFFeEIsTUFBTTZXLFlBQVksR0FBR3ZYLFNBQVMsQ0FBQ0ssWUFBWSxDQUFDbVgsT0FBTyxDQUFDLG9CQUFvQixDQUFDLENBQUMsSUFBSSxFQUFFO0lBQ2hGLEtBQUssSUFBSTFZLENBQUMsR0FBRyxDQUFDLEVBQUVBLENBQUMsR0FBR3lZLFlBQVksQ0FBQ3hiLE1BQU0sRUFBRStDLENBQUMsRUFBRSxFQUFFO01BQzVDLElBQUl5WSxZQUFZLENBQUN6WSxDQUFDLENBQUMsQ0FBQzRCLEdBQUcsS0FBS0EsR0FBRyxFQUFFO1FBQy9CNlcsWUFBWSxDQUFDRSxNQUFNLENBQUMzWSxDQUFDLEVBQUUsQ0FBQyxDQUFDO1FBQ3pCO01BQ0Y7SUFDRjs7SUFFQTtJQUNBLE1BQU00WSxjQUFjLEdBQUc7TUFBRWhYLEdBQUc7TUFBRTdDLE9BQU8sRUFBRXNaLFNBQVM7TUFBRXhaLEtBQUs7TUFBRTBaO0lBQU8sQ0FBQztJQUVqRXJoQixZQUFZLENBQUN1RCxXQUFXLENBQUMsQ0FBQyxDQUFDbUUsSUFBSSxFQUFFLHlCQUF5QixFQUFFO01BQUU4RixJQUFJLEVBQUVrVSxjQUFjO01BQUVuSSxLQUFLLEVBQUVnSTtJQUFhLENBQUMsQ0FBQztJQUUxR0EsWUFBWSxDQUFDeFksSUFBSSxDQUFDMlksY0FBYyxDQUFDO0lBQ2pDLE9BQU9ILFlBQVksQ0FBQ3hiLE1BQU0sR0FBR3JLLElBQUksQ0FBQzBCLE1BQU0sQ0FBQ0UsZ0JBQWdCLEVBQUU7TUFDekRpa0IsWUFBWSxDQUFDaFUsS0FBSyxDQUFDLENBQUM7SUFDdEI7O0lBRUE7SUFDQSxPQUFPZ1UsWUFBWSxDQUFDeGIsTUFBTSxHQUFHLENBQUMsRUFBRTtNQUM5QixJQUFJO1FBQ0ZzRSxZQUFZLENBQUNDLE9BQU8sQ0FBQyxvQkFBb0IsRUFBRUosSUFBSSxDQUFDeVgsU0FBUyxDQUFDSixZQUFZLENBQUMsQ0FBQztRQUN4RTtNQUNGLENBQUMsQ0FBQyxPQUFPaGQsQ0FBQyxFQUFFO1FBQ1Z4QyxpQkFBaUIsQ0FBQ3dCLFdBQVcsQ0FBQyxDQUFDLENBQUNtRSxJQUFJLEVBQUUsd0JBQXdCLEVBQUU7VUFBRWthLEtBQUssRUFBRXJkLENBQUM7VUFBRWdWLEtBQUssRUFBRWdJO1FBQWEsQ0FBQyxDQUFDO1FBQ2xHQSxZQUFZLENBQUNoVSxLQUFLLENBQUMsQ0FBQyxFQUFDO01BQ3ZCO0lBQ0Y7RUFDRjs7RUFFQTtBQUNGO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7RUFFRTtBQUNGO0FBQ0E7QUFDQTtFQUNFLFNBQVNzVSxnQkFBZ0JBLENBQUNuWCxHQUFHLEVBQUU7SUFDN0IsSUFBSSxDQUFDL0oscUJBQXFCLENBQUMsQ0FBQyxFQUFFO01BQzVCLE9BQU8sSUFBSTtJQUNiO0lBRUErSixHQUFHLEdBQUdGLGFBQWEsQ0FBQ0UsR0FBRyxDQUFDO0lBRXhCLE1BQU02VyxZQUFZLEdBQUd2WCxTQUFTLENBQUNLLFlBQVksQ0FBQ21YLE9BQU8sQ0FBQyxvQkFBb0IsQ0FBQyxDQUFDLElBQUksRUFBRTtJQUNoRixLQUFLLElBQUkxWSxDQUFDLEdBQUcsQ0FBQyxFQUFFQSxDQUFDLEdBQUd5WSxZQUFZLENBQUN4YixNQUFNLEVBQUUrQyxDQUFDLEVBQUUsRUFBRTtNQUM1QyxJQUFJeVksWUFBWSxDQUFDelksQ0FBQyxDQUFDLENBQUM0QixHQUFHLEtBQUtBLEdBQUcsRUFBRTtRQUMvQixPQUFPNlcsWUFBWSxDQUFDelksQ0FBQyxDQUFDO01BQ3hCO0lBQ0Y7SUFDQSxPQUFPLElBQUk7RUFDYjs7RUFFQTtBQUNGO0FBQ0E7QUFDQTtFQUNFLFNBQVNzWSx3QkFBd0JBLENBQUMva0IsR0FBRyxFQUFFO0lBQ3JDLE1BQU15bEIsU0FBUyxHQUFHcG1CLElBQUksQ0FBQzBCLE1BQU0sQ0FBQ1MsWUFBWTtJQUMxQyxNQUFNa2tCLEtBQUssR0FBRyxvQkFBc0IxbEIsR0FBRyxDQUFDcVUsU0FBUyxDQUFDLElBQUksQ0FBRTtJQUN4RHJLLE9BQU8sQ0FBQ25LLE9BQU8sQ0FBQzZsQixLQUFLLEVBQUUsR0FBRyxHQUFHRCxTQUFTLENBQUMsRUFBRSxVQUFTdFYsS0FBSyxFQUFFO01BQ3ZEcE0sc0JBQXNCLENBQUNvTSxLQUFLLEVBQUVzVixTQUFTLENBQUM7SUFDMUMsQ0FBQyxDQUFDO0lBQ0Y7SUFDQXpiLE9BQU8sQ0FBQ25LLE9BQU8sQ0FBQzZsQixLQUFLLEVBQUUseUJBQXlCLENBQUMsRUFBRSxVQUFTdlYsS0FBSyxFQUFFO01BQ2pFQSxLQUFLLENBQUNILGVBQWUsQ0FBQyxVQUFVLENBQUM7SUFDbkMsQ0FBQyxDQUFDO0lBQ0YsT0FBTzBWLEtBQUssQ0FBQ1osU0FBUztFQUN4QjtFQUVBLFNBQVNhLHdCQUF3QkEsQ0FBQSxFQUFHO0lBQ2xDLE1BQU0zbEIsR0FBRyxHQUFHMGtCLGlCQUFpQixDQUFDLENBQUM7SUFDL0IsTUFBTXRXLElBQUksR0FBR3FXLHFCQUFxQixJQUFJakcsUUFBUSxDQUFDalEsUUFBUSxHQUFHaVEsUUFBUSxDQUFDaFEsTUFBTTs7SUFFekU7SUFDQTtJQUNBO0lBQ0E7SUFDQTtJQUNBLElBQUlvWCxtQkFBbUI7SUFDdkIsSUFBSTtNQUNGQSxtQkFBbUIsR0FBRzFlLFdBQVcsQ0FBQyxDQUFDLENBQUNxRSxhQUFhLENBQUMsb0RBQW9ELENBQUM7SUFDekcsQ0FBQyxDQUFDLE9BQU9yRCxDQUFDLEVBQUU7TUFDWjtNQUNFMGQsbUJBQW1CLEdBQUcxZSxXQUFXLENBQUMsQ0FBQyxDQUFDcUUsYUFBYSxDQUFDLGdEQUFnRCxDQUFDO0lBQ3JHO0lBQ0EsSUFBSSxDQUFDcWEsbUJBQW1CLEVBQUU7TUFDeEJqaUIsWUFBWSxDQUFDdUQsV0FBVyxDQUFDLENBQUMsQ0FBQ21FLElBQUksRUFBRSx3QkFBd0IsRUFBRTtRQUFFK0MsSUFBSTtRQUFFdVcsVUFBVSxFQUFFM2tCO01BQUksQ0FBQyxDQUFDO01BQ3JGNGtCLGtCQUFrQixDQUFDeFcsSUFBSSxFQUFFcE8sR0FBRyxDQUFDO0lBQy9CO0lBRUEsSUFBSVgsSUFBSSxDQUFDMEIsTUFBTSxDQUFDQyxjQUFjLEVBQUU2a0IsT0FBTyxDQUFDQyxZQUFZLENBQUM7TUFBRXptQixJQUFJLEVBQUU7SUFBSyxDQUFDLEVBQUU2SCxXQUFXLENBQUMsQ0FBQyxDQUFDb0UsS0FBSyxFQUFFNkIsTUFBTSxDQUFDcVIsUUFBUSxDQUFDTyxJQUFJLENBQUM7RUFDakg7O0VBRUE7QUFDRjtBQUNBO0VBQ0UsU0FBU2dILGtCQUFrQkEsQ0FBQzNYLElBQUksRUFBRTtJQUNsQztJQUNFLElBQUkvTyxJQUFJLENBQUMwQixNQUFNLENBQUN5QixtQkFBbUIsRUFBRTtNQUNuQzRMLElBQUksR0FBR0EsSUFBSSxDQUFDbkQsT0FBTyxDQUFDLGlDQUFpQyxFQUFFLEVBQUUsQ0FBQztNQUMxRCxJQUFJc0YsUUFBUSxDQUFDbkMsSUFBSSxFQUFFLEdBQUcsQ0FBQyxJQUFJbUMsUUFBUSxDQUFDbkMsSUFBSSxFQUFFLEdBQUcsQ0FBQyxFQUFFO1FBQzlDQSxJQUFJLEdBQUdBLElBQUksQ0FBQy9ILEtBQUssQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUM7TUFDMUI7SUFDRjtJQUNBLElBQUloSCxJQUFJLENBQUMwQixNQUFNLENBQUNDLGNBQWMsRUFBRTtNQUM5QjZrQixPQUFPLENBQUNHLFNBQVMsQ0FBQztRQUFFM21CLElBQUksRUFBRTtNQUFLLENBQUMsRUFBRSxFQUFFLEVBQUUrTyxJQUFJLENBQUM7SUFDN0M7SUFDQXFXLHFCQUFxQixHQUFHclcsSUFBSTtFQUM5Qjs7RUFFQTtBQUNGO0FBQ0E7RUFDRSxTQUFTNlgsbUJBQW1CQSxDQUFDN1gsSUFBSSxFQUFFO0lBQ2pDLElBQUkvTyxJQUFJLENBQUMwQixNQUFNLENBQUNDLGNBQWMsRUFBRTZrQixPQUFPLENBQUNDLFlBQVksQ0FBQztNQUFFem1CLElBQUksRUFBRTtJQUFLLENBQUMsRUFBRSxFQUFFLEVBQUUrTyxJQUFJLENBQUM7SUFDOUVxVyxxQkFBcUIsR0FBR3JXLElBQUk7RUFDOUI7O0VBRUE7QUFDRjtBQUNBO0VBQ0UsU0FBUzVJLGlCQUFpQkEsQ0FBQ3FRLEtBQUssRUFBRTtJQUNoQzdMLE9BQU8sQ0FBQzZMLEtBQUssRUFBRSxVQUFTc0UsSUFBSSxFQUFFO01BQzVCQSxJQUFJLENBQUN2UixJQUFJLENBQUMxQyxTQUFTLENBQUM7SUFDdEIsQ0FBQyxDQUFDO0VBQ0o7O0VBRUE7QUFDRjtBQUNBO0VBQ0UsU0FBU2dnQixxQkFBcUJBLENBQUM5WCxJQUFJLEVBQUU7SUFDbkMsTUFBTStYLE9BQU8sR0FBRyxJQUFJQyxjQUFjLENBQUMsQ0FBQztJQUNwQyxNQUFNQyxPQUFPLEdBQUc7TUFBRWpZLElBQUk7TUFBRXlNLEdBQUcsRUFBRXNMO0lBQVEsQ0FBQztJQUN0Q3hpQixZQUFZLENBQUN1RCxXQUFXLENBQUMsQ0FBQyxDQUFDbUUsSUFBSSxFQUFFLHVCQUF1QixFQUFFZ2IsT0FBTyxDQUFDO0lBQ2xFRixPQUFPLENBQUNHLElBQUksQ0FBQyxLQUFLLEVBQUVsWSxJQUFJLEVBQUUsSUFBSSxDQUFDO0lBQy9CK1gsT0FBTyxDQUFDSSxnQkFBZ0IsQ0FBQyxZQUFZLEVBQUUsTUFBTSxDQUFDO0lBQzlDSixPQUFPLENBQUNJLGdCQUFnQixDQUFDLDRCQUE0QixFQUFFLE1BQU0sQ0FBQztJQUM5REosT0FBTyxDQUFDSSxnQkFBZ0IsQ0FBQyxnQkFBZ0IsRUFBRXJmLFdBQVcsQ0FBQyxDQUFDLENBQUNzWCxRQUFRLENBQUNPLElBQUksQ0FBQztJQUN2RW9ILE9BQU8sQ0FBQ0ssTUFBTSxHQUFHLFlBQVc7TUFDMUIsSUFBSSxJQUFJLENBQUNDLE1BQU0sSUFBSSxHQUFHLElBQUksSUFBSSxDQUFDQSxNQUFNLEdBQUcsR0FBRyxFQUFFO1FBQzNDOWlCLFlBQVksQ0FBQ3VELFdBQVcsQ0FBQyxDQUFDLENBQUNtRSxJQUFJLEVBQUUsMkJBQTJCLEVBQUVnYixPQUFPLENBQUM7UUFDdEUsTUFBTTdjLFFBQVEsR0FBR3JFLFlBQVksQ0FBQyxJQUFJLENBQUM0RixRQUFRLENBQUM7UUFDNUM7UUFDQSxNQUFNUyxPQUFPLEdBQUdoQyxRQUFRLENBQUMrQixhQUFhLENBQUMsd0NBQXdDLENBQUMsSUFBSS9CLFFBQVE7UUFDNUYsTUFBTWtkLGNBQWMsR0FBR2hDLGlCQUFpQixDQUFDLENBQUM7UUFDMUMsTUFBTXhRLFVBQVUsR0FBRzdPLGNBQWMsQ0FBQ3FoQixjQUFjLENBQUM7UUFDakR6TSxXQUFXLENBQUN6USxRQUFRLENBQUM4QixLQUFLLENBQUM7UUFFM0JvSix1QkFBdUIsQ0FBQ2xMLFFBQVEsQ0FBQztRQUNqQ2lPLGFBQWEsQ0FBQ2lQLGNBQWMsRUFBRWxiLE9BQU8sRUFBRTBJLFVBQVUsQ0FBQztRQUNsRFUsd0JBQXdCLENBQUMsQ0FBQztRQUMxQnBQLGlCQUFpQixDQUFDME8sVUFBVSxDQUFDMkIsS0FBSyxDQUFDO1FBQ25DNE8scUJBQXFCLEdBQUdyVyxJQUFJO1FBQzVCekssWUFBWSxDQUFDdUQsV0FBVyxDQUFDLENBQUMsQ0FBQ21FLElBQUksRUFBRSxxQkFBcUIsRUFBRTtVQUFFK0MsSUFBSTtVQUFFdVksU0FBUyxFQUFFLElBQUk7VUFBRUMsY0FBYyxFQUFFLElBQUksQ0FBQzdiO1FBQVMsQ0FBQyxDQUFDO01BQ25ILENBQUMsTUFBTTtRQUNMckYsaUJBQWlCLENBQUN3QixXQUFXLENBQUMsQ0FBQyxDQUFDbUUsSUFBSSxFQUFFLGdDQUFnQyxFQUFFZ2IsT0FBTyxDQUFDO01BQ2xGO0lBQ0YsQ0FBQztJQUNERixPQUFPLENBQUNVLElBQUksQ0FBQyxDQUFDO0VBQ2hCOztFQUVBO0FBQ0Y7QUFDQTtFQUNFLFNBQVNDLGNBQWNBLENBQUMxWSxJQUFJLEVBQUU7SUFDNUJ1WCx3QkFBd0IsQ0FBQyxDQUFDO0lBQzFCdlgsSUFBSSxHQUFHQSxJQUFJLElBQUlvUSxRQUFRLENBQUNqUSxRQUFRLEdBQUdpUSxRQUFRLENBQUNoUSxNQUFNO0lBQ2xELE1BQU11WSxNQUFNLEdBQUd2QixnQkFBZ0IsQ0FBQ3BYLElBQUksQ0FBQztJQUNyQyxJQUFJMlksTUFBTSxFQUFFO01BQ1YsTUFBTXZkLFFBQVEsR0FBR3JFLFlBQVksQ0FBQzRoQixNQUFNLENBQUN2YixPQUFPLENBQUM7TUFDN0MsTUFBTWtiLGNBQWMsR0FBR2hDLGlCQUFpQixDQUFDLENBQUM7TUFDMUMsTUFBTXhRLFVBQVUsR0FBRzdPLGNBQWMsQ0FBQ3FoQixjQUFjLENBQUM7TUFDakR6TSxXQUFXLENBQUM4TSxNQUFNLENBQUN6YixLQUFLLENBQUM7TUFDekJvSix1QkFBdUIsQ0FBQ2xMLFFBQVEsQ0FBQztNQUNqQ2lPLGFBQWEsQ0FBQ2lQLGNBQWMsRUFBRWxkLFFBQVEsRUFBRTBLLFVBQVUsQ0FBQztNQUNuRFUsd0JBQXdCLENBQUMsQ0FBQztNQUMxQnBQLGlCQUFpQixDQUFDME8sVUFBVSxDQUFDMkIsS0FBSyxDQUFDO01BQ25DM0csU0FBUyxDQUFDLENBQUMsQ0FBQ0csVUFBVSxDQUFDLFlBQVc7UUFDaENsQyxNQUFNLENBQUM2WixRQUFRLENBQUMsQ0FBQyxFQUFFRCxNQUFNLENBQUMvQixNQUFNLENBQUM7TUFDbkMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxFQUFDO01BQ05QLHFCQUFxQixHQUFHclcsSUFBSTtNQUM1QnpLLFlBQVksQ0FBQ3VELFdBQVcsQ0FBQyxDQUFDLENBQUNtRSxJQUFJLEVBQUUscUJBQXFCLEVBQUU7UUFBRStDLElBQUk7UUFBRStDLElBQUksRUFBRTRWO01BQU8sQ0FBQyxDQUFDO0lBQ2pGLENBQUMsTUFBTTtNQUNMLElBQUkxbkIsSUFBSSxDQUFDMEIsTUFBTSxDQUFDRyxvQkFBb0IsRUFBRTtRQUNwQztRQUNBO1FBQ0FpTSxNQUFNLENBQUNxUixRQUFRLENBQUN5SSxNQUFNLENBQUMsSUFBSSxDQUFDO01BQzlCLENBQUMsTUFBTTtRQUNMZixxQkFBcUIsQ0FBQzlYLElBQUksQ0FBQztNQUM3QjtJQUNGO0VBQ0Y7O0VBRUE7QUFDRjtBQUNBO0FBQ0E7RUFDRSxTQUFTOFksMEJBQTBCQSxDQUFDbG5CLEdBQUcsRUFBRTtJQUN2QyxJQUFJbW5CLFVBQVUsR0FBRyxzQkFBd0JsVSxvQkFBb0IsQ0FBQ2pULEdBQUcsRUFBRSxjQUFjLENBQUU7SUFDbkYsSUFBSW1uQixVQUFVLElBQUksSUFBSSxFQUFFO01BQ3RCQSxVQUFVLEdBQUcsQ0FBQ25uQixHQUFHLENBQUM7SUFDcEI7SUFDQWdLLE9BQU8sQ0FBQ21kLFVBQVUsRUFBRSxVQUFTQyxFQUFFLEVBQUU7TUFDL0IsTUFBTXRRLFlBQVksR0FBRy9SLGVBQWUsQ0FBQ3FpQixFQUFFLENBQUM7TUFDeEN0USxZQUFZLENBQUN1USxZQUFZLEdBQUcsQ0FBQ3ZRLFlBQVksQ0FBQ3VRLFlBQVksSUFBSSxDQUFDLElBQUksQ0FBQztNQUNoRUQsRUFBRSxDQUFDdlgsU0FBUyxDQUFDQyxHQUFHLENBQUNsSCxJQUFJLENBQUN3ZSxFQUFFLENBQUN2WCxTQUFTLEVBQUV4USxJQUFJLENBQUMwQixNQUFNLENBQUNTLFlBQVksQ0FBQztJQUMvRCxDQUFDLENBQUM7SUFDRixPQUFPMmxCLFVBQVU7RUFDbkI7O0VBRUE7QUFDRjtBQUNBO0FBQ0E7RUFDRSxTQUFTRyxlQUFlQSxDQUFDdG5CLEdBQUcsRUFBRTtJQUM1QixJQUFJdW5CLFlBQVksR0FBRyxzQkFBd0J0VSxvQkFBb0IsQ0FBQ2pULEdBQUcsRUFBRSxpQkFBaUIsQ0FBRTtJQUN4RixJQUFJdW5CLFlBQVksSUFBSSxJQUFJLEVBQUU7TUFDeEJBLFlBQVksR0FBRyxFQUFFO0lBQ25CO0lBQ0F2ZCxPQUFPLENBQUN1ZCxZQUFZLEVBQUUsVUFBU0MsZUFBZSxFQUFFO01BQzlDLE1BQU0xUSxZQUFZLEdBQUcvUixlQUFlLENBQUN5aUIsZUFBZSxDQUFDO01BQ3JEMVEsWUFBWSxDQUFDdVEsWUFBWSxHQUFHLENBQUN2USxZQUFZLENBQUN1USxZQUFZLElBQUksQ0FBQyxJQUFJLENBQUM7TUFDaEVHLGVBQWUsQ0FBQ3JkLFlBQVksQ0FBQyxVQUFVLEVBQUUsRUFBRSxDQUFDO01BQzVDcWQsZUFBZSxDQUFDcmQsWUFBWSxDQUFDLHVCQUF1QixFQUFFLEVBQUUsQ0FBQztJQUMzRCxDQUFDLENBQUM7SUFDRixPQUFPb2QsWUFBWTtFQUNyQjs7RUFFQTtBQUNGO0FBQ0E7QUFDQTtFQUNFLFNBQVNFLHVCQUF1QkEsQ0FBQ04sVUFBVSxFQUFFTyxRQUFRLEVBQUU7SUFDckQxZCxPQUFPLENBQUNtZCxVQUFVLENBQUNRLE1BQU0sQ0FBQ0QsUUFBUSxDQUFDLEVBQUUsVUFBU0UsR0FBRyxFQUFFO01BQ2pELE1BQU05USxZQUFZLEdBQUcvUixlQUFlLENBQUM2aUIsR0FBRyxDQUFDO01BQ3pDOVEsWUFBWSxDQUFDdVEsWUFBWSxHQUFHLENBQUN2USxZQUFZLENBQUN1USxZQUFZLElBQUksQ0FBQyxJQUFJLENBQUM7SUFDbEUsQ0FBQyxDQUFDO0lBQ0ZyZCxPQUFPLENBQUNtZCxVQUFVLEVBQUUsVUFBU0MsRUFBRSxFQUFFO01BQy9CLE1BQU10USxZQUFZLEdBQUcvUixlQUFlLENBQUNxaUIsRUFBRSxDQUFDO01BQ3hDLElBQUl0USxZQUFZLENBQUN1USxZQUFZLEtBQUssQ0FBQyxFQUFFO1FBQ25DRCxFQUFFLENBQUN2WCxTQUFTLENBQUN6UCxNQUFNLENBQUN3SSxJQUFJLENBQUN3ZSxFQUFFLENBQUN2WCxTQUFTLEVBQUV4USxJQUFJLENBQUMwQixNQUFNLENBQUNTLFlBQVksQ0FBQztNQUNsRTtJQUNGLENBQUMsQ0FBQztJQUNGd0ksT0FBTyxDQUFDMGQsUUFBUSxFQUFFLFVBQVNGLGVBQWUsRUFBRTtNQUMxQyxNQUFNMVEsWUFBWSxHQUFHL1IsZUFBZSxDQUFDeWlCLGVBQWUsQ0FBQztNQUNyRCxJQUFJMVEsWUFBWSxDQUFDdVEsWUFBWSxLQUFLLENBQUMsRUFBRTtRQUNuQ0csZUFBZSxDQUFDeFgsZUFBZSxDQUFDLFVBQVUsQ0FBQztRQUMzQ3dYLGVBQWUsQ0FBQ3hYLGVBQWUsQ0FBQyx1QkFBdUIsQ0FBQztNQUMxRDtJQUNGLENBQUMsQ0FBQztFQUNKOztFQUVBO0VBQ0E7RUFDQTs7RUFFQTtBQUNGO0FBQ0E7QUFDQTtBQUNBO0VBQ0UsU0FBUzZYLFlBQVlBLENBQUNDLFNBQVMsRUFBRTluQixHQUFHLEVBQUU7SUFDcEMsS0FBSyxJQUFJeU0sQ0FBQyxHQUFHLENBQUMsRUFBRUEsQ0FBQyxHQUFHcWIsU0FBUyxDQUFDcGUsTUFBTSxFQUFFK0MsQ0FBQyxFQUFFLEVBQUU7TUFDekMsTUFBTXNELElBQUksR0FBRytYLFNBQVMsQ0FBQ3JiLENBQUMsQ0FBQztNQUN6QixJQUFJc0QsSUFBSSxDQUFDZ1ksVUFBVSxDQUFDL25CLEdBQUcsQ0FBQyxFQUFFO1FBQ3hCLE9BQU8sSUFBSTtNQUNiO0lBQ0Y7SUFDQSxPQUFPLEtBQUs7RUFDZDs7RUFFQTtBQUNGO0FBQ0E7QUFDQTtFQUNFLFNBQVNnb0IsYUFBYUEsQ0FBQzlRLE9BQU8sRUFBRTtJQUM5QjtJQUNBLE1BQU1sWCxHQUFHLEdBQUcsK0JBQWlDa1gsT0FBUTtJQUNyRCxJQUFJbFgsR0FBRyxDQUFDeUcsSUFBSSxLQUFLLEVBQUUsSUFBSXpHLEdBQUcsQ0FBQ3lHLElBQUksSUFBSSxJQUFJLElBQUl6RyxHQUFHLENBQUMwbkIsUUFBUSxJQUFJNW5CLE9BQU8sQ0FBQ0UsR0FBRyxFQUFFLG9CQUFvQixDQUFDLEVBQUU7TUFDN0YsT0FBTyxLQUFLO0lBQ2Q7SUFDQTtJQUNBLElBQUlBLEdBQUcsQ0FBQ0MsSUFBSSxLQUFLLFFBQVEsSUFBSUQsR0FBRyxDQUFDQyxJQUFJLEtBQUssUUFBUSxJQUFJRCxHQUFHLENBQUMwVixPQUFPLEtBQUssT0FBTyxJQUFJMVYsR0FBRyxDQUFDMFYsT0FBTyxLQUFLLE9BQU8sSUFBSTFWLEdBQUcsQ0FBQzBWLE9BQU8sS0FBSyxNQUFNLEVBQUU7TUFDbEksT0FBTyxLQUFLO0lBQ2Q7SUFDQSxJQUFJMVYsR0FBRyxDQUFDQyxJQUFJLEtBQUssVUFBVSxJQUFJRCxHQUFHLENBQUNDLElBQUksS0FBSyxPQUFPLEVBQUU7TUFDbkQsT0FBT0QsR0FBRyxDQUFDaW9CLE9BQU87SUFDcEI7SUFDQSxPQUFPLElBQUk7RUFDYjs7RUFFQTtBQUNGO0FBQ0E7RUFDRSxTQUFTQyxrQkFBa0JBLENBQUN6aEIsSUFBSSxFQUFFMkQsS0FBSyxFQUFFK2QsUUFBUSxFQUFFO0lBQ2pELElBQUkxaEIsSUFBSSxJQUFJLElBQUksSUFBSTJELEtBQUssSUFBSSxJQUFJLEVBQUU7TUFDakMsSUFBSU0sS0FBSyxDQUFDNk4sT0FBTyxDQUFDbk8sS0FBSyxDQUFDLEVBQUU7UUFDeEJBLEtBQUssQ0FBQ0osT0FBTyxDQUFDLFVBQVNvZSxDQUFDLEVBQUU7VUFBRUQsUUFBUSxDQUFDeGUsTUFBTSxDQUFDbEQsSUFBSSxFQUFFMmhCLENBQUMsQ0FBQztRQUFDLENBQUMsQ0FBQztNQUN6RCxDQUFDLE1BQU07UUFDTEQsUUFBUSxDQUFDeGUsTUFBTSxDQUFDbEQsSUFBSSxFQUFFMkQsS0FBSyxDQUFDO01BQzlCO0lBQ0Y7RUFDRjs7RUFFQTtBQUNGO0FBQ0E7RUFDRSxTQUFTaWUsdUJBQXVCQSxDQUFDNWhCLElBQUksRUFBRTJELEtBQUssRUFBRStkLFFBQVEsRUFBRTtJQUN0RCxJQUFJMWhCLElBQUksSUFBSSxJQUFJLElBQUkyRCxLQUFLLElBQUksSUFBSSxFQUFFO01BQ2pDLElBQUlySyxNQUFNLEdBQUdvb0IsUUFBUSxDQUFDRyxNQUFNLENBQUM3aEIsSUFBSSxDQUFDO01BQ2xDLElBQUlpRSxLQUFLLENBQUM2TixPQUFPLENBQUNuTyxLQUFLLENBQUMsRUFBRTtRQUN4QnJLLE1BQU0sR0FBR0EsTUFBTSxDQUFDK1gsTUFBTSxDQUFDc1EsQ0FBQyxJQUFJaGUsS0FBSyxDQUFDcEMsT0FBTyxDQUFDb2dCLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQztNQUNuRCxDQUFDLE1BQU07UUFDTHJvQixNQUFNLEdBQUdBLE1BQU0sQ0FBQytYLE1BQU0sQ0FBQ3NRLENBQUMsSUFBSUEsQ0FBQyxLQUFLaGUsS0FBSyxDQUFDO01BQzFDO01BQ0ErZCxRQUFRLENBQUNJLE1BQU0sQ0FBQzloQixJQUFJLENBQUM7TUFDckJ1RCxPQUFPLENBQUNqSyxNQUFNLEVBQUVxb0IsQ0FBQyxJQUFJRCxRQUFRLENBQUN4ZSxNQUFNLENBQUNsRCxJQUFJLEVBQUUyaEIsQ0FBQyxDQUFDLENBQUM7SUFDaEQ7RUFDRjs7RUFFQTtBQUNGO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtFQUNFLFNBQVNJLGlCQUFpQkEsQ0FBQ1YsU0FBUyxFQUFFSyxRQUFRLEVBQUVNLE1BQU0sRUFBRXpvQixHQUFHLEVBQUUwb0IsUUFBUSxFQUFFO0lBQ3JFLElBQUkxb0IsR0FBRyxJQUFJLElBQUksSUFBSTZuQixZQUFZLENBQUNDLFNBQVMsRUFBRTluQixHQUFHLENBQUMsRUFBRTtNQUMvQztJQUNGLENBQUMsTUFBTTtNQUNMOG5CLFNBQVMsQ0FBQ3BiLElBQUksQ0FBQzFNLEdBQUcsQ0FBQztJQUNyQjtJQUNBLElBQUlnb0IsYUFBYSxDQUFDaG9CLEdBQUcsQ0FBQyxFQUFFO01BQ3RCLE1BQU15RyxJQUFJLEdBQUdELGVBQWUsQ0FBQ3hHLEdBQUcsRUFBRSxNQUFNLENBQUM7TUFDekM7TUFDQSxJQUFJb0ssS0FBSyxHQUFHcEssR0FBRyxDQUFDb0ssS0FBSztNQUNyQixJQUFJcEssR0FBRyxZQUFZMm9CLGlCQUFpQixJQUFJM29CLEdBQUcsQ0FBQzRvQixRQUFRLEVBQUU7UUFDcER4ZSxLQUFLLEdBQUdrQyxPQUFPLENBQUN0TSxHQUFHLENBQUM0SyxnQkFBZ0IsQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDLENBQUM5RSxHQUFHLENBQUMsVUFBU29DLENBQUMsRUFBRTtVQUFFLE9BQU8sQ0FBQyw4QkFBK0JBLENBQUMsRUFBR2tDLEtBQUs7UUFBQyxDQUFDLENBQUM7TUFDL0g7TUFDQTtNQUNBLElBQUlwSyxHQUFHLFlBQVk2b0IsZ0JBQWdCLElBQUk3b0IsR0FBRyxDQUFDOG9CLEtBQUssRUFBRTtRQUNoRDFlLEtBQUssR0FBR2tDLE9BQU8sQ0FBQ3RNLEdBQUcsQ0FBQzhvQixLQUFLLENBQUM7TUFDNUI7TUFDQVosa0JBQWtCLENBQUN6aEIsSUFBSSxFQUFFMkQsS0FBSyxFQUFFK2QsUUFBUSxDQUFDO01BQ3pDLElBQUlPLFFBQVEsRUFBRTtRQUNaSyxlQUFlLENBQUMvb0IsR0FBRyxFQUFFeW9CLE1BQU0sQ0FBQztNQUM5QjtJQUNGO0lBQ0EsSUFBSXpvQixHQUFHLFlBQVlncEIsZUFBZSxFQUFFO01BQ2xDaGYsT0FBTyxDQUFDaEssR0FBRyxDQUFDZ2lCLFFBQVEsRUFBRSxVQUFTaUgsS0FBSyxFQUFFO1FBQ3BDLElBQUluQixTQUFTLENBQUM5ZixPQUFPLENBQUNpaEIsS0FBSyxDQUFDLElBQUksQ0FBQyxFQUFFO1VBQ2pDO1VBQ0E7VUFDQTtVQUNBWix1QkFBdUIsQ0FBQ1ksS0FBSyxDQUFDeGlCLElBQUksRUFBRXdpQixLQUFLLENBQUM3ZSxLQUFLLEVBQUUrZCxRQUFRLENBQUM7UUFDNUQsQ0FBQyxNQUFNO1VBQ0xMLFNBQVMsQ0FBQ3BiLElBQUksQ0FBQ3VjLEtBQUssQ0FBQztRQUN2QjtRQUNBLElBQUlQLFFBQVEsRUFBRTtVQUNaSyxlQUFlLENBQUNFLEtBQUssRUFBRVIsTUFBTSxDQUFDO1FBQ2hDO01BQ0YsQ0FBQyxDQUFDO01BQ0YsSUFBSVMsUUFBUSxDQUFDbHBCLEdBQUcsQ0FBQyxDQUFDZ0ssT0FBTyxDQUFDLFVBQVNJLEtBQUssRUFBRTNELElBQUksRUFBRTtRQUM5QyxJQUFJMkQsS0FBSyxZQUFZK2UsSUFBSSxJQUFJL2UsS0FBSyxDQUFDM0QsSUFBSSxLQUFLLEVBQUUsRUFBRTtVQUM5QyxPQUFNLENBQUM7UUFDVDtRQUNBeWhCLGtCQUFrQixDQUFDemhCLElBQUksRUFBRTJELEtBQUssRUFBRStkLFFBQVEsQ0FBQztNQUMzQyxDQUFDLENBQUM7SUFDSjtFQUNGOztFQUVBO0FBQ0Y7QUFDQTtBQUNBO0FBQ0E7RUFDRSxTQUFTWSxlQUFlQSxDQUFDL29CLEdBQUcsRUFBRXlvQixNQUFNLEVBQUU7SUFDcEMsTUFBTXZSLE9BQU8sR0FBRyw2Q0FBK0NsWCxHQUFJO0lBQ25FLElBQUlrWCxPQUFPLENBQUNrUyxZQUFZLEVBQUU7TUFDeEJ6bEIsWUFBWSxDQUFDdVQsT0FBTyxFQUFFLDBCQUEwQixDQUFDO01BQ2pELElBQUksQ0FBQ0EsT0FBTyxDQUFDbVMsYUFBYSxDQUFDLENBQUMsRUFBRTtRQUM1QlosTUFBTSxDQUFDL2IsSUFBSSxDQUFDO1VBQUUxTSxHQUFHLEVBQUVrWCxPQUFPO1VBQUVvUyxPQUFPLEVBQUVwUyxPQUFPLENBQUNxUyxpQkFBaUI7VUFBRUMsUUFBUSxFQUFFdFMsT0FBTyxDQUFDc1M7UUFBUyxDQUFDLENBQUM7UUFDN0Y3bEIsWUFBWSxDQUFDdVQsT0FBTyxFQUFFLHdCQUF3QixFQUFFO1VBQUVvUyxPQUFPLEVBQUVwUyxPQUFPLENBQUNxUyxpQkFBaUI7VUFBRUMsUUFBUSxFQUFFdFMsT0FBTyxDQUFDc1M7UUFBUyxDQUFDLENBQUM7TUFDckg7SUFDRjtFQUNGOztFQUVBO0FBQ0Y7QUFDQTtBQUNBO0FBQ0E7QUFDQTtFQUNFLFNBQVNDLGdCQUFnQkEsQ0FBQ0MsUUFBUSxFQUFFQyxLQUFLLEVBQUU7SUFDekMsS0FBSyxNQUFNbGMsR0FBRyxJQUFJa2MsS0FBSyxDQUFDclMsSUFBSSxDQUFDLENBQUMsRUFBRTtNQUM5Qm9TLFFBQVEsQ0FBQ25CLE1BQU0sQ0FBQzlhLEdBQUcsQ0FBQztJQUN0QjtJQUNBa2MsS0FBSyxDQUFDM2YsT0FBTyxDQUFDLFVBQVNJLEtBQUssRUFBRXFELEdBQUcsRUFBRTtNQUNqQ2ljLFFBQVEsQ0FBQy9mLE1BQU0sQ0FBQzhELEdBQUcsRUFBRXJELEtBQUssQ0FBQztJQUM3QixDQUFDLENBQUM7SUFDRixPQUFPc2YsUUFBUTtFQUNqQjs7RUFFQTtBQUNGO0FBQ0E7QUFDQTtBQUNBO0VBQ0UsU0FBU3ZwQixjQUFjQSxDQUFDSCxHQUFHLEVBQUUrRixJQUFJLEVBQUU7SUFDakM7SUFDQSxNQUFNK2hCLFNBQVMsR0FBRyxFQUFFO0lBQ3BCLE1BQU1LLFFBQVEsR0FBRyxJQUFJZSxRQUFRLENBQUMsQ0FBQztJQUMvQixNQUFNVSxnQkFBZ0IsR0FBRyxJQUFJVixRQUFRLENBQUMsQ0FBQztJQUN2QztJQUNBLE1BQU1ULE1BQU0sR0FBRyxFQUFFO0lBQ2pCLE1BQU0zUixZQUFZLEdBQUcvUixlQUFlLENBQUMvRSxHQUFHLENBQUM7SUFDekMsSUFBSThXLFlBQVksQ0FBQ2dNLGlCQUFpQixJQUFJLENBQUN6ZSxZQUFZLENBQUN5UyxZQUFZLENBQUNnTSxpQkFBaUIsQ0FBQyxFQUFFO01BQ25GaE0sWUFBWSxDQUFDZ00saUJBQWlCLEdBQUcsSUFBSTtJQUN2Qzs7SUFFQTtJQUNBO0lBQ0EsSUFBSTRGLFFBQVEsR0FBSTFvQixHQUFHLFlBQVlncEIsZUFBZSxJQUFJaHBCLEdBQUcsQ0FBQzZwQixVQUFVLEtBQUssSUFBSSxJQUFLbmxCLGlCQUFpQixDQUFDMUUsR0FBRyxFQUFFLGFBQWEsQ0FBQyxLQUFLLE1BQU07SUFDOUgsSUFBSThXLFlBQVksQ0FBQ2dNLGlCQUFpQixFQUFFO01BQ2xDNEYsUUFBUSxHQUFHQSxRQUFRLElBQUk1UixZQUFZLENBQUNnTSxpQkFBaUIsQ0FBQ2dILGNBQWMsS0FBSyxJQUFJO0lBQy9FOztJQUVBO0lBQ0EsSUFBSS9qQixJQUFJLEtBQUssS0FBSyxFQUFFO01BQ2xCeWlCLGlCQUFpQixDQUFDVixTQUFTLEVBQUU4QixnQkFBZ0IsRUFBRW5CLE1BQU0sRUFBRTNvQixPQUFPLENBQUNFLEdBQUcsRUFBRSxNQUFNLENBQUMsRUFBRTBvQixRQUFRLENBQUM7SUFDeEY7O0lBRUE7SUFDQUYsaUJBQWlCLENBQUNWLFNBQVMsRUFBRUssUUFBUSxFQUFFTSxNQUFNLEVBQUV6b0IsR0FBRyxFQUFFMG9CLFFBQVEsQ0FBQzs7SUFFN0Q7SUFDQSxJQUFJNVIsWUFBWSxDQUFDZ00saUJBQWlCLElBQUk5aUIsR0FBRyxDQUFDMFYsT0FBTyxLQUFLLFFBQVEsSUFDN0QxVixHQUFHLENBQUMwVixPQUFPLEtBQUssT0FBTyxJQUFJbFAsZUFBZSxDQUFDeEcsR0FBRyxFQUFFLE1BQU0sQ0FBQyxLQUFLLFFBQVMsRUFBRTtNQUN0RSxNQUFNK3BCLE1BQU0sR0FBR2pULFlBQVksQ0FBQ2dNLGlCQUFpQixLQUFLLCtDQUFnRDlpQixHQUFHLENBQUU7TUFDdkcsTUFBTXlHLElBQUksR0FBR0QsZUFBZSxDQUFDdWpCLE1BQU0sRUFBRSxNQUFNLENBQUM7TUFDNUM3QixrQkFBa0IsQ0FBQ3poQixJQUFJLEVBQUVzakIsTUFBTSxDQUFDM2YsS0FBSyxFQUFFd2YsZ0JBQWdCLENBQUM7SUFDMUQ7O0lBRUE7SUFDQSxNQUFNNUssUUFBUSxHQUFHL0wsb0JBQW9CLENBQUNqVCxHQUFHLEVBQUUsWUFBWSxDQUFDO0lBQ3hEZ0ssT0FBTyxDQUFDZ1YsUUFBUSxFQUFFLFVBQVNqUCxJQUFJLEVBQUU7TUFDL0J5WSxpQkFBaUIsQ0FBQ1YsU0FBUyxFQUFFSyxRQUFRLEVBQUVNLE1BQU0sRUFBRXRnQixTQUFTLENBQUM0SCxJQUFJLENBQUMsRUFBRTJZLFFBQVEsQ0FBQztNQUN6RTtNQUNBLElBQUksQ0FBQ3RnQixPQUFPLENBQUMySCxJQUFJLEVBQUUsTUFBTSxDQUFDLEVBQUU7UUFDMUIvRixPQUFPLENBQUMwRixZQUFZLENBQUNLLElBQUksQ0FBQyxDQUFDbkYsZ0JBQWdCLENBQUNtUyxjQUFjLENBQUMsRUFBRSxVQUFTaU4sVUFBVSxFQUFFO1VBQ2hGeEIsaUJBQWlCLENBQUNWLFNBQVMsRUFBRUssUUFBUSxFQUFFTSxNQUFNLEVBQUV1QixVQUFVLEVBQUV0QixRQUFRLENBQUM7UUFDdEUsQ0FBQyxDQUFDO01BQ0o7SUFDRixDQUFDLENBQUM7O0lBRUY7SUFDQWUsZ0JBQWdCLENBQUN0QixRQUFRLEVBQUV5QixnQkFBZ0IsQ0FBQztJQUU1QyxPQUFPO01BQUVuQixNQUFNO01BQUVOLFFBQVE7TUFBRXBvQixNQUFNLEVBQUVrcUIsYUFBYSxDQUFDOUIsUUFBUTtJQUFFLENBQUM7RUFDOUQ7O0VBRUE7QUFDRjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0VBQ0UsU0FBUytCLFdBQVdBLENBQUNDLFNBQVMsRUFBRTFqQixJQUFJLEVBQUUyakIsU0FBUyxFQUFFO0lBQy9DLElBQUlELFNBQVMsS0FBSyxFQUFFLEVBQUU7TUFDcEJBLFNBQVMsSUFBSSxHQUFHO0lBQ2xCO0lBQ0EsSUFBSXRMLE1BQU0sQ0FBQ3VMLFNBQVMsQ0FBQyxLQUFLLGlCQUFpQixFQUFFO01BQzNDQSxTQUFTLEdBQUd2YyxJQUFJLENBQUN5WCxTQUFTLENBQUM4RSxTQUFTLENBQUM7SUFDdkM7SUFDQSxNQUFNekgsQ0FBQyxHQUFHMEgsa0JBQWtCLENBQUNELFNBQVMsQ0FBQztJQUN2Q0QsU0FBUyxJQUFJRSxrQkFBa0IsQ0FBQzVqQixJQUFJLENBQUMsR0FBRyxHQUFHLEdBQUdrYyxDQUFDO0lBQy9DLE9BQU93SCxTQUFTO0VBQ2xCOztFQUVBO0FBQ0Y7QUFDQTtBQUNBO0VBQ0UsU0FBU0csU0FBU0EsQ0FBQ3ZxQixNQUFNLEVBQUU7SUFDekJBLE1BQU0sR0FBR3dxQixrQkFBa0IsQ0FBQ3hxQixNQUFNLENBQUM7SUFDbkMsSUFBSW9xQixTQUFTLEdBQUcsRUFBRTtJQUNsQnBxQixNQUFNLENBQUNpSyxPQUFPLENBQUMsVUFBU0ksS0FBSyxFQUFFcUQsR0FBRyxFQUFFO01BQ2xDMGMsU0FBUyxHQUFHRCxXQUFXLENBQUNDLFNBQVMsRUFBRTFjLEdBQUcsRUFBRXJELEtBQUssQ0FBQztJQUNoRCxDQUFDLENBQUM7SUFDRixPQUFPK2YsU0FBUztFQUNsQjs7RUFFQTtFQUNBO0VBQ0E7O0VBRUE7QUFDRjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0VBQ0UsU0FBU3JsQixVQUFVQSxDQUFDOUUsR0FBRyxFQUFFd1MsTUFBTSxFQUFFZ1ksTUFBTSxFQUFFO0lBQ3ZDO0lBQ0EsTUFBTUMsT0FBTyxHQUFHO01BQ2QsWUFBWSxFQUFFLE1BQU07TUFDcEIsWUFBWSxFQUFFamtCLGVBQWUsQ0FBQ3hHLEdBQUcsRUFBRSxJQUFJLENBQUM7TUFDeEMsaUJBQWlCLEVBQUV3RyxlQUFlLENBQUN4RyxHQUFHLEVBQUUsTUFBTSxDQUFDO01BQy9DLFdBQVcsRUFBRTBFLGlCQUFpQixDQUFDOE4sTUFBTSxFQUFFLElBQUksQ0FBQztNQUM1QyxnQkFBZ0IsRUFBRXRMLFdBQVcsQ0FBQyxDQUFDLENBQUNzWCxRQUFRLENBQUNPO0lBQzNDLENBQUM7SUFDRDJMLG1CQUFtQixDQUFDMXFCLEdBQUcsRUFBRSxZQUFZLEVBQUUsS0FBSyxFQUFFeXFCLE9BQU8sQ0FBQztJQUN0RCxJQUFJRCxNQUFNLEtBQUt0a0IsU0FBUyxFQUFFO01BQ3hCdWtCLE9BQU8sQ0FBQyxXQUFXLENBQUMsR0FBR0QsTUFBTTtJQUMvQjtJQUNBLElBQUl6bEIsZUFBZSxDQUFDL0UsR0FBRyxDQUFDLENBQUNzVCxPQUFPLEVBQUU7TUFDaENtWCxPQUFPLENBQUMsWUFBWSxDQUFDLEdBQUcsTUFBTTtJQUNoQztJQUNBLE9BQU9BLE9BQU87RUFDaEI7O0VBRUE7QUFDRjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtFQUNFLFNBQVNqbUIsWUFBWUEsQ0FBQ3RFLFdBQVcsRUFBRUYsR0FBRyxFQUFFO0lBQ3RDLE1BQU0ycUIsV0FBVyxHQUFHaG1CLHdCQUF3QixDQUFDM0UsR0FBRyxFQUFFLFdBQVcsQ0FBQztJQUM5RCxJQUFJMnFCLFdBQVcsRUFBRTtNQUNmLElBQUlBLFdBQVcsS0FBSyxNQUFNLEVBQUU7UUFDMUIsT0FBTyxJQUFJekIsUUFBUSxDQUFDLENBQUM7TUFDdkIsQ0FBQyxNQUFNLElBQUl5QixXQUFXLEtBQUssR0FBRyxFQUFFO1FBQzlCLE9BQU96cUIsV0FBVztNQUNwQixDQUFDLE1BQU0sSUFBSXlxQixXQUFXLENBQUMzaUIsT0FBTyxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsRUFBRTtRQUM1Q2dDLE9BQU8sQ0FBQzJnQixXQUFXLENBQUN0a0IsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDMEIsS0FBSyxDQUFDLEdBQUcsQ0FBQyxFQUFFLFVBQVN0QixJQUFJLEVBQUU7VUFDdERBLElBQUksR0FBR0EsSUFBSSxDQUFDNkcsSUFBSSxDQUFDLENBQUM7VUFDbEJwTixXQUFXLENBQUNxb0IsTUFBTSxDQUFDOWhCLElBQUksQ0FBQztRQUMxQixDQUFDLENBQUM7UUFDRixPQUFPdkcsV0FBVztNQUNwQixDQUFDLE1BQU07UUFDTCxNQUFNMHFCLFNBQVMsR0FBRyxJQUFJMUIsUUFBUSxDQUFDLENBQUM7UUFDaENsZixPQUFPLENBQUMyZ0IsV0FBVyxDQUFDNWlCLEtBQUssQ0FBQyxHQUFHLENBQUMsRUFBRSxVQUFTdEIsSUFBSSxFQUFFO1VBQzdDQSxJQUFJLEdBQUdBLElBQUksQ0FBQzZHLElBQUksQ0FBQyxDQUFDO1VBQ2xCLElBQUlwTixXQUFXLENBQUN5ZixHQUFHLENBQUNsWixJQUFJLENBQUMsRUFBRTtZQUN6QnZHLFdBQVcsQ0FBQ29vQixNQUFNLENBQUM3aEIsSUFBSSxDQUFDLENBQUN1RCxPQUFPLENBQUMsVUFBU0ksS0FBSyxFQUFFO2NBQUV3Z0IsU0FBUyxDQUFDamhCLE1BQU0sQ0FBQ2xELElBQUksRUFBRTJELEtBQUssQ0FBQztZQUFDLENBQUMsQ0FBQztVQUNyRjtRQUNGLENBQUMsQ0FBQztRQUNGLE9BQU93Z0IsU0FBUztNQUNsQjtJQUNGLENBQUMsTUFBTTtNQUNMLE9BQU8xcUIsV0FBVztJQUNwQjtFQUNGOztFQUVBO0FBQ0Y7QUFDQTtBQUNBO0VBQ0UsU0FBUzJxQixZQUFZQSxDQUFDN3FCLEdBQUcsRUFBRTtJQUN6QixPQUFPLENBQUMsQ0FBQ3dHLGVBQWUsQ0FBQ3hHLEdBQUcsRUFBRSxNQUFNLENBQUMsSUFBSXdHLGVBQWUsQ0FBQ3hHLEdBQUcsRUFBRSxNQUFNLENBQUMsQ0FBQ2dJLE9BQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDO0VBQ3pGOztFQUVBO0FBQ0Y7QUFDQTtBQUNBO0FBQ0E7RUFDRSxTQUFTaEQsb0JBQW9CQSxDQUFDaEYsR0FBRyxFQUFFOHFCLGdCQUFnQixFQUFFO0lBQ25ELE1BQU1DLFFBQVEsR0FBR0QsZ0JBQWdCLElBQUlubUIsd0JBQXdCLENBQUMzRSxHQUFHLEVBQUUsU0FBUyxDQUFDO0lBQzdFO0lBQ0EsTUFBTTJZLFFBQVEsR0FBRztNQUNmL0UsU0FBUyxFQUFFN08sZUFBZSxDQUFDL0UsR0FBRyxDQUFDLENBQUNzVCxPQUFPLEdBQUcsV0FBVyxHQUFHalUsSUFBSSxDQUFDMEIsTUFBTSxDQUFDSSxnQkFBZ0I7TUFDcEY2cEIsU0FBUyxFQUFFM3JCLElBQUksQ0FBQzBCLE1BQU0sQ0FBQ0ssZ0JBQWdCO01BQ3ZDdVosV0FBVyxFQUFFdGIsSUFBSSxDQUFDMEIsTUFBTSxDQUFDTTtJQUMzQixDQUFDO0lBQ0QsSUFBSWhDLElBQUksQ0FBQzBCLE1BQU0sQ0FBQzhCLHFCQUFxQixJQUFJa0MsZUFBZSxDQUFDL0UsR0FBRyxDQUFDLENBQUNzVCxPQUFPLElBQUksQ0FBQ3VYLFlBQVksQ0FBQzdxQixHQUFHLENBQUMsRUFBRTtNQUMzRjJZLFFBQVEsQ0FBQ3NTLElBQUksR0FBRyxLQUFLO0lBQ3ZCO0lBQ0EsSUFBSUYsUUFBUSxFQUFFO01BQ1osTUFBTWhqQixLQUFLLEdBQUdzRixpQkFBaUIsQ0FBQzBkLFFBQVEsQ0FBQztNQUN6QyxJQUFJaGpCLEtBQUssQ0FBQzJCLE1BQU0sR0FBRyxDQUFDLEVBQUU7UUFDcEIsS0FBSyxJQUFJK0MsQ0FBQyxHQUFHLENBQUMsRUFBRUEsQ0FBQyxHQUFHMUUsS0FBSyxDQUFDMkIsTUFBTSxFQUFFK0MsQ0FBQyxFQUFFLEVBQUU7VUFDckMsTUFBTXJDLEtBQUssR0FBR3JDLEtBQUssQ0FBQzBFLENBQUMsQ0FBQztVQUN0QixJQUFJckMsS0FBSyxDQUFDcEMsT0FBTyxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsRUFBRTtZQUNoQzJRLFFBQVEsQ0FBQ3FTLFNBQVMsR0FBRzVuQixhQUFhLENBQUNnSCxLQUFLLENBQUMvRCxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUM7VUFDcEQsQ0FBQyxNQUFNLElBQUkrRCxLQUFLLENBQUNwQyxPQUFPLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxFQUFFO1lBQ3pDMlEsUUFBUSxDQUFDZ0MsV0FBVyxHQUFHdlgsYUFBYSxDQUFDZ0gsS0FBSyxDQUFDL0QsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDO1VBQ3RELENBQUMsTUFBTSxJQUFJK0QsS0FBSyxDQUFDcEMsT0FBTyxDQUFDLGFBQWEsQ0FBQyxLQUFLLENBQUMsRUFBRTtZQUM3QzJRLFFBQVEsQ0FBQ3VTLFVBQVUsR0FBRzlnQixLQUFLLENBQUMvRCxLQUFLLENBQUMsRUFBRSxDQUFDLEtBQUssTUFBTTtVQUNsRCxDQUFDLE1BQU0sSUFBSStELEtBQUssQ0FBQ3BDLE9BQU8sQ0FBQyxjQUFjLENBQUMsS0FBSyxDQUFDLEVBQUU7WUFDOUMyUSxRQUFRLENBQUMvVixXQUFXLEdBQUd3SCxLQUFLLENBQUMvRCxLQUFLLENBQUMsRUFBRSxDQUFDLEtBQUssTUFBTTtVQUNuRCxDQUFDLE1BQU0sSUFBSStELEtBQUssQ0FBQ3BDLE9BQU8sQ0FBQyxTQUFTLENBQUMsS0FBSyxDQUFDLEVBQUU7WUFDekMsTUFBTW1qQixVQUFVLEdBQUcvZ0IsS0FBSyxDQUFDL0QsS0FBSyxDQUFDLENBQUMsQ0FBQztZQUNqQyxJQUFJK2tCLFNBQVMsR0FBR0QsVUFBVSxDQUFDcGpCLEtBQUssQ0FBQyxHQUFHLENBQUM7WUFDckMsTUFBTXNqQixTQUFTLEdBQUdELFNBQVMsQ0FBQ0UsR0FBRyxDQUFDLENBQUM7WUFDakMsSUFBSUMsV0FBVyxHQUFHSCxTQUFTLENBQUMxaEIsTUFBTSxHQUFHLENBQUMsR0FBRzBoQixTQUFTLENBQUNwbEIsSUFBSSxDQUFDLEdBQUcsQ0FBQyxHQUFHLElBQUk7WUFDbkU7WUFDQTJTLFFBQVEsQ0FBQ3FNLE1BQU0sR0FBR3FHLFNBQVM7WUFDM0IxUyxRQUFRLENBQUM2UyxZQUFZLEdBQUdELFdBQVc7VUFDckMsQ0FBQyxNQUFNLElBQUluaEIsS0FBSyxDQUFDcEMsT0FBTyxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsRUFBRTtZQUN2QyxNQUFNeWpCLFFBQVEsR0FBR3JoQixLQUFLLENBQUMvRCxLQUFLLENBQUMsQ0FBQyxDQUFDO1lBQy9CLElBQUkra0IsU0FBUyxHQUFHSyxRQUFRLENBQUMxakIsS0FBSyxDQUFDLEdBQUcsQ0FBQztZQUNuQyxNQUFNMmpCLE9BQU8sR0FBR04sU0FBUyxDQUFDRSxHQUFHLENBQUMsQ0FBQztZQUMvQixJQUFJQyxXQUFXLEdBQUdILFNBQVMsQ0FBQzFoQixNQUFNLEdBQUcsQ0FBQyxHQUFHMGhCLFNBQVMsQ0FBQ3BsQixJQUFJLENBQUMsR0FBRyxDQUFDLEdBQUcsSUFBSTtZQUNuRTJTLFFBQVEsQ0FBQ3NTLElBQUksR0FBR1MsT0FBTztZQUN2Qi9TLFFBQVEsQ0FBQ2dULFVBQVUsR0FBR0osV0FBVztVQUNuQyxDQUFDLE1BQU0sSUFBSW5oQixLQUFLLENBQUNwQyxPQUFPLENBQUMsZUFBZSxDQUFDLEtBQUssQ0FBQyxFQUFFO1lBQy9DLE1BQU00akIsY0FBYyxHQUFHeGhCLEtBQUssQ0FBQy9ELEtBQUssQ0FBQyxlQUFlLENBQUNxRCxNQUFNLENBQUM7WUFDMURpUCxRQUFRLENBQUNrQixXQUFXLEdBQUcrUixjQUFjLElBQUksTUFBTTtVQUNqRCxDQUFDLE1BQU0sSUFBSW5mLENBQUMsSUFBSSxDQUFDLEVBQUU7WUFDakJrTSxRQUFRLENBQUMvRSxTQUFTLEdBQUd4SixLQUFLO1VBQzVCLENBQUMsTUFBTTtZQUNMVSxRQUFRLENBQUMsK0JBQStCLEdBQUdWLEtBQUssQ0FBQztVQUNuRDtRQUNGO01BQ0Y7SUFDRjtJQUNBLE9BQU91TyxRQUFRO0VBQ2pCOztFQUVBO0FBQ0Y7QUFDQTtBQUNBO0VBQ0UsU0FBU2tULFlBQVlBLENBQUM3ckIsR0FBRyxFQUFFO0lBQ3pCLE9BQU8yRSx3QkFBd0IsQ0FBQzNFLEdBQUcsRUFBRSxhQUFhLENBQUMsS0FBSyxxQkFBcUIsSUFDNUVvSSxPQUFPLENBQUNwSSxHQUFHLEVBQUUsTUFBTSxDQUFDLElBQUl3RyxlQUFlLENBQUN4RyxHQUFHLEVBQUUsU0FBUyxDQUFDLEtBQUsscUJBQXNCO0VBQ3JGOztFQUVBO0FBQ0Y7QUFDQTtBQUNBO0FBQ0E7QUFDQTtFQUNFLFNBQVM4ckIsbUJBQW1CQSxDQUFDalIsR0FBRyxFQUFFN2EsR0FBRyxFQUFFK3JCLGtCQUFrQixFQUFFO0lBQ3pELElBQUlDLGlCQUFpQixHQUFHLElBQUk7SUFDNUJybUIsY0FBYyxDQUFDM0YsR0FBRyxFQUFFLFVBQVMrVCxTQUFTLEVBQUU7TUFDdEMsSUFBSWlZLGlCQUFpQixJQUFJLElBQUksRUFBRTtRQUM3QkEsaUJBQWlCLEdBQUdqWSxTQUFTLENBQUNrWSxnQkFBZ0IsQ0FBQ3BSLEdBQUcsRUFBRWtSLGtCQUFrQixFQUFFL3JCLEdBQUcsQ0FBQztNQUM5RTtJQUNGLENBQUMsQ0FBQztJQUNGLElBQUlnc0IsaUJBQWlCLElBQUksSUFBSSxFQUFFO01BQzdCLE9BQU9BLGlCQUFpQjtJQUMxQixDQUFDLE1BQU07TUFDTCxJQUFJSCxZQUFZLENBQUM3ckIsR0FBRyxDQUFDLEVBQUU7UUFDckI7UUFDQTtRQUNBLE9BQU95cEIsZ0JBQWdCLENBQUMsSUFBSVAsUUFBUSxDQUFDLENBQUMsRUFBRXFCLGtCQUFrQixDQUFDd0Isa0JBQWtCLENBQUMsQ0FBQztNQUNqRixDQUFDLE1BQU07UUFDTCxPQUFPekIsU0FBUyxDQUFDeUIsa0JBQWtCLENBQUM7TUFDdEM7SUFDRjtFQUNGOztFQUVBO0FBQ0Y7QUFDQTtBQUNBO0FBQ0E7RUFDRSxTQUFTMW1CLGNBQWNBLENBQUNtTixNQUFNLEVBQUU7SUFDOUIsT0FBTztNQUFFcUQsS0FBSyxFQUFFLEVBQUU7TUFBRWhCLElBQUksRUFBRSxDQUFDckMsTUFBTTtJQUFFLENBQUM7RUFDdEM7O0VBRUE7QUFDRjtBQUNBO0FBQ0E7RUFDRSxTQUFTaUksaUJBQWlCQSxDQUFDalAsT0FBTyxFQUFFbU4sUUFBUSxFQUFFO0lBQzVDLE1BQU11VCxLQUFLLEdBQUcxZ0IsT0FBTyxDQUFDLENBQUMsQ0FBQztJQUN4QixNQUFNNlEsSUFBSSxHQUFHN1EsT0FBTyxDQUFDQSxPQUFPLENBQUM5QixNQUFNLEdBQUcsQ0FBQyxDQUFDO0lBQ3hDLElBQUlpUCxRQUFRLENBQUNxTSxNQUFNLEVBQUU7TUFDbkIsSUFBSXhTLE1BQU0sR0FBRyxJQUFJO01BQ2pCLElBQUltRyxRQUFRLENBQUM2UyxZQUFZLEVBQUU7UUFDekJoWixNQUFNLEdBQUdySyxTQUFTLENBQUM1QyxnQkFBZ0IsQ0FBQzJtQixLQUFLLEVBQUV2VCxRQUFRLENBQUM2UyxZQUFZLENBQUMsQ0FBQztNQUNwRTtNQUNBLElBQUk3UyxRQUFRLENBQUNxTSxNQUFNLEtBQUssS0FBSyxLQUFLa0gsS0FBSyxJQUFJMVosTUFBTSxDQUFDLEVBQUU7UUFDbERBLE1BQU0sR0FBR0EsTUFBTSxJQUFJMFosS0FBSztRQUN4QjFaLE1BQU0sQ0FBQzJaLFNBQVMsR0FBRyxDQUFDO01BQ3RCO01BQ0EsSUFBSXhULFFBQVEsQ0FBQ3FNLE1BQU0sS0FBSyxRQUFRLEtBQUszSSxJQUFJLElBQUk3SixNQUFNLENBQUMsRUFBRTtRQUNwREEsTUFBTSxHQUFHQSxNQUFNLElBQUk2SixJQUFJO1FBQ3ZCN0osTUFBTSxDQUFDMlosU0FBUyxHQUFHM1osTUFBTSxDQUFDNFosWUFBWTtNQUN4QztJQUNGO0lBQ0EsSUFBSXpULFFBQVEsQ0FBQ3NTLElBQUksRUFBRTtNQUNqQixJQUFJelksTUFBTSxHQUFHLElBQUk7TUFDakIsSUFBSW1HLFFBQVEsQ0FBQ2dULFVBQVUsRUFBRTtRQUN2QixJQUFJdFksU0FBUyxHQUFHc0YsUUFBUSxDQUFDZ1QsVUFBVTtRQUNuQyxJQUFJaFQsUUFBUSxDQUFDZ1QsVUFBVSxLQUFLLFFBQVEsRUFBRTtVQUNwQ3RZLFNBQVMsR0FBRyxNQUFNO1FBQ3BCO1FBQ0FiLE1BQU0sR0FBR3JLLFNBQVMsQ0FBQzVDLGdCQUFnQixDQUFDMm1CLEtBQUssRUFBRTdZLFNBQVMsQ0FBQyxDQUFDO01BQ3hEO01BQ0EsSUFBSXNGLFFBQVEsQ0FBQ3NTLElBQUksS0FBSyxLQUFLLEtBQUtpQixLQUFLLElBQUkxWixNQUFNLENBQUMsRUFBRTtRQUNoREEsTUFBTSxHQUFHQSxNQUFNLElBQUkwWixLQUFLO1FBQ3hCO1FBQ0ExWixNQUFNLENBQUM4SCxjQUFjLENBQUM7VUFBRUMsS0FBSyxFQUFFLE9BQU87VUFBRUMsUUFBUSxFQUFFbmIsSUFBSSxDQUFDMEIsTUFBTSxDQUFDdUI7UUFBZSxDQUFDLENBQUM7TUFDakY7TUFDQSxJQUFJcVcsUUFBUSxDQUFDc1MsSUFBSSxLQUFLLFFBQVEsS0FBSzVPLElBQUksSUFBSTdKLE1BQU0sQ0FBQyxFQUFFO1FBQ2xEQSxNQUFNLEdBQUdBLE1BQU0sSUFBSTZKLElBQUk7UUFDdkI7UUFDQTdKLE1BQU0sQ0FBQzhILGNBQWMsQ0FBQztVQUFFQyxLQUFLLEVBQUUsS0FBSztVQUFFQyxRQUFRLEVBQUVuYixJQUFJLENBQUMwQixNQUFNLENBQUN1QjtRQUFlLENBQUMsQ0FBQztNQUMvRTtJQUNGO0VBQ0Y7O0VBRUE7QUFDRjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7RUFDRSxTQUFTb29CLG1CQUFtQkEsQ0FBQzFxQixHQUFHLEVBQUVrSyxJQUFJLEVBQUVtaUIsYUFBYSxFQUFFdHNCLE1BQU0sRUFBRTtJQUM3RCxJQUFJQSxNQUFNLElBQUksSUFBSSxFQUFFO01BQ2xCQSxNQUFNLEdBQUcsQ0FBQyxDQUFDO0lBQ2I7SUFDQSxJQUFJQyxHQUFHLElBQUksSUFBSSxFQUFFO01BQ2YsT0FBT0QsTUFBTTtJQUNmO0lBQ0EsTUFBTTZILGNBQWMsR0FBR2xELGlCQUFpQixDQUFDMUUsR0FBRyxFQUFFa0ssSUFBSSxDQUFDO0lBQ25ELElBQUl0QyxjQUFjLEVBQUU7TUFDbEIsSUFBSTNCLEdBQUcsR0FBRzJCLGNBQWMsQ0FBQzBGLElBQUksQ0FBQyxDQUFDO01BQy9CLElBQUlnZixhQUFhLEdBQUdELGFBQWE7TUFDakMsSUFBSXBtQixHQUFHLEtBQUssT0FBTyxFQUFFO1FBQ25CLE9BQU8sSUFBSTtNQUNiO01BQ0EsSUFBSUEsR0FBRyxDQUFDK0IsT0FBTyxDQUFDLGFBQWEsQ0FBQyxLQUFLLENBQUMsRUFBRTtRQUNwQy9CLEdBQUcsR0FBR0EsR0FBRyxDQUFDSSxLQUFLLENBQUMsRUFBRSxDQUFDO1FBQ25CaW1CLGFBQWEsR0FBRyxJQUFJO01BQ3RCLENBQUMsTUFBTSxJQUFJcm1CLEdBQUcsQ0FBQytCLE9BQU8sQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLEVBQUU7UUFDbkMvQixHQUFHLEdBQUdBLEdBQUcsQ0FBQ0ksS0FBSyxDQUFDLENBQUMsQ0FBQztRQUNsQmltQixhQUFhLEdBQUcsSUFBSTtNQUN0QjtNQUNBLElBQUlybUIsR0FBRyxDQUFDK0IsT0FBTyxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsRUFBRTtRQUMxQi9CLEdBQUcsR0FBRyxHQUFHLEdBQUdBLEdBQUcsR0FBRyxHQUFHO01BQ3ZCO01BQ0EsSUFBSXNtQixVQUFVO01BQ2QsSUFBSUQsYUFBYSxFQUFFO1FBQ2pCQyxVQUFVLEdBQUc5ZCxTQUFTLENBQUN6TyxHQUFHLEVBQUUsWUFBVztVQUFFLE9BQU8yYyxRQUFRLENBQUMsVUFBVSxHQUFHMVcsR0FBRyxHQUFHLEdBQUcsQ0FBQyxDQUFDLENBQUM7UUFBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUM7TUFDM0YsQ0FBQyxNQUFNO1FBQ0xzbUIsVUFBVSxHQUFHNWUsU0FBUyxDQUFDMUgsR0FBRyxDQUFDO01BQzdCO01BQ0EsS0FBSyxNQUFNd0gsR0FBRyxJQUFJOGUsVUFBVSxFQUFFO1FBQzVCLElBQUlBLFVBQVUsQ0FBQzdlLGNBQWMsQ0FBQ0QsR0FBRyxDQUFDLEVBQUU7VUFDbEMsSUFBSTFOLE1BQU0sQ0FBQzBOLEdBQUcsQ0FBQyxJQUFJLElBQUksRUFBRTtZQUN2QjFOLE1BQU0sQ0FBQzBOLEdBQUcsQ0FBQyxHQUFHOGUsVUFBVSxDQUFDOWUsR0FBRyxDQUFDO1VBQy9CO1FBQ0Y7TUFDRjtJQUNGO0lBQ0EsT0FBT2lkLG1CQUFtQixDQUFDdmlCLFNBQVMsQ0FBQ3RCLFNBQVMsQ0FBQzdHLEdBQUcsQ0FBQyxDQUFDLEVBQUVrSyxJQUFJLEVBQUVtaUIsYUFBYSxFQUFFdHNCLE1BQU0sQ0FBQztFQUNwRjs7RUFFQTtBQUNGO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7RUFDRSxTQUFTME8sU0FBU0EsQ0FBQ3pPLEdBQUcsRUFBRXdzQixNQUFNLEVBQUVDLFVBQVUsRUFBRTtJQUMxQyxJQUFJcHRCLElBQUksQ0FBQzBCLE1BQU0sQ0FBQ2EsU0FBUyxFQUFFO01BQ3pCLE9BQU80cUIsTUFBTSxDQUFDLENBQUM7SUFDakIsQ0FBQyxNQUFNO01BQ0w5bUIsaUJBQWlCLENBQUMxRixHQUFHLEVBQUUsMEJBQTBCLENBQUM7TUFDbEQsT0FBT3lzQixVQUFVO0lBQ25CO0VBQ0Y7O0VBRUE7QUFDRjtBQUNBO0FBQ0E7QUFDQTtFQUNFLFNBQVNDLG1CQUFtQkEsQ0FBQzFzQixHQUFHLEVBQUUyc0IsY0FBYyxFQUFFO0lBQ2hELE9BQU9qQyxtQkFBbUIsQ0FBQzFxQixHQUFHLEVBQUUsU0FBUyxFQUFFLElBQUksRUFBRTJzQixjQUFjLENBQUM7RUFDbEU7O0VBRUE7QUFDRjtBQUNBO0FBQ0E7QUFDQTtFQUNFLFNBQVNDLG1CQUFtQkEsQ0FBQzVzQixHQUFHLEVBQUUyc0IsY0FBYyxFQUFFO0lBQ2hELE9BQU9qQyxtQkFBbUIsQ0FBQzFxQixHQUFHLEVBQUUsU0FBUyxFQUFFLEtBQUssRUFBRTJzQixjQUFjLENBQUM7RUFDbkU7O0VBRUE7QUFDRjtBQUNBO0FBQ0E7RUFDRSxTQUFTOW5CLGlCQUFpQkEsQ0FBQzdFLEdBQUcsRUFBRTtJQUM5QixPQUFPb0YsWUFBWSxDQUFDc25CLG1CQUFtQixDQUFDMXNCLEdBQUcsQ0FBQyxFQUFFNHNCLG1CQUFtQixDQUFDNXNCLEdBQUcsQ0FBQyxDQUFDO0VBQ3pFOztFQUVBO0FBQ0Y7QUFDQTtBQUNBO0FBQ0E7RUFDRSxTQUFTNnNCLG9CQUFvQkEsQ0FBQ2hTLEdBQUcsRUFBRUMsTUFBTSxFQUFFZ1MsV0FBVyxFQUFFO0lBQ3RELElBQUlBLFdBQVcsS0FBSyxJQUFJLEVBQUU7TUFDeEIsSUFBSTtRQUNGalMsR0FBRyxDQUFDMEwsZ0JBQWdCLENBQUN6TCxNQUFNLEVBQUVnUyxXQUFXLENBQUM7TUFDM0MsQ0FBQyxDQUFDLE9BQU81a0IsQ0FBQyxFQUFFO1FBQ1o7UUFDRTJTLEdBQUcsQ0FBQzBMLGdCQUFnQixDQUFDekwsTUFBTSxFQUFFdVAsa0JBQWtCLENBQUN5QyxXQUFXLENBQUMsQ0FBQztRQUM3RGpTLEdBQUcsQ0FBQzBMLGdCQUFnQixDQUFDekwsTUFBTSxHQUFHLGtCQUFrQixFQUFFLE1BQU0sQ0FBQztNQUMzRDtJQUNGO0VBQ0Y7O0VBRUE7QUFDRjtBQUNBO0FBQ0E7RUFDRSxTQUFTaVMsbUJBQW1CQSxDQUFDbFMsR0FBRyxFQUFFO0lBQ2xDO0lBQ0UsSUFBSUEsR0FBRyxDQUFDbVMsV0FBVyxJQUFJLE9BQVExZSxHQUFJLEtBQUssV0FBVyxFQUFFO01BQ25ELElBQUk7UUFDRixNQUFNRCxHQUFHLEdBQUcsSUFBSUMsR0FBRyxDQUFDdU0sR0FBRyxDQUFDbVMsV0FBVyxDQUFDO1FBQ3BDLE9BQU8zZSxHQUFHLENBQUNFLFFBQVEsR0FBR0YsR0FBRyxDQUFDRyxNQUFNO01BQ2xDLENBQUMsQ0FBQyxPQUFPdEcsQ0FBQyxFQUFFO1FBQ1Z4QyxpQkFBaUIsQ0FBQ3dCLFdBQVcsQ0FBQyxDQUFDLENBQUNtRSxJQUFJLEVBQUUscUJBQXFCLEVBQUU7VUFBRWdELEdBQUcsRUFBRXdNLEdBQUcsQ0FBQ21TO1FBQVksQ0FBQyxDQUFDO01BQ3hGO0lBQ0Y7RUFDRjs7RUFFQTtBQUNGO0FBQ0E7QUFDQTtBQUNBO0VBQ0UsU0FBU0MsU0FBU0EsQ0FBQ3BTLEdBQUcsRUFBRXFTLE1BQU0sRUFBRTtJQUM5QixPQUFPQSxNQUFNLENBQUNuZixJQUFJLENBQUM4TSxHQUFHLENBQUNzUyxxQkFBcUIsQ0FBQyxDQUFDLENBQUM7RUFDakQ7O0VBRUE7QUFDRjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7RUFDRSxTQUFTdnBCLFVBQVVBLENBQUNtQyxJQUFJLEVBQUVxSSxJQUFJLEVBQUU4RCxPQUFPLEVBQUU7SUFDdkNuTSxJQUFJLEdBQUkscUJBQXNCQSxJQUFJLENBQUNrRCxXQUFXLENBQUMsQ0FBRztJQUNsRCxJQUFJaUosT0FBTyxFQUFFO01BQ1gsSUFBSUEsT0FBTyxZQUFZeEwsT0FBTyxJQUFJLE9BQU93TCxPQUFPLEtBQUssUUFBUSxFQUFFO1FBQzdELE9BQU8rTSxnQkFBZ0IsQ0FBQ2xaLElBQUksRUFBRXFJLElBQUksRUFBRSxJQUFJLEVBQUUsSUFBSSxFQUFFO1VBQzlDZ2YsY0FBYyxFQUFFaGUsYUFBYSxDQUFDOEMsT0FBTyxDQUFDLElBQUljLFNBQVM7VUFDbkRxYSxhQUFhLEVBQUU7UUFDakIsQ0FBQyxDQUFDO01BQ0osQ0FBQyxNQUFNO1FBQ0wsSUFBSUMsY0FBYyxHQUFHbGUsYUFBYSxDQUFDOEMsT0FBTyxDQUFDTSxNQUFNLENBQUM7UUFDbEQ7UUFDQTtRQUNBLElBQUtOLE9BQU8sQ0FBQ00sTUFBTSxJQUFJLENBQUM4YSxjQUFjLElBQU1wYixPQUFPLENBQUMwSyxNQUFNLElBQUksQ0FBQzBRLGNBQWMsSUFBSSxDQUFDbGUsYUFBYSxDQUFDOEMsT0FBTyxDQUFDMEssTUFBTSxDQUFFLEVBQUU7VUFDaEgwUSxjQUFjLEdBQUd0YSxTQUFTO1FBQzVCO1FBQ0EsT0FBT2lNLGdCQUFnQixDQUFDbFosSUFBSSxFQUFFcUksSUFBSSxFQUFFZ0IsYUFBYSxDQUFDOEMsT0FBTyxDQUFDMEssTUFBTSxDQUFDLEVBQUUxSyxPQUFPLENBQUNwRCxLQUFLLEVBQzlFO1VBQ0VvUCxPQUFPLEVBQUVoTSxPQUFPLENBQUNnTSxPQUFPO1VBQ3hCdU0sT0FBTyxFQUFFdlksT0FBTyxDQUFDdVksT0FBTztVQUN4QjFxQixNQUFNLEVBQUVtUyxPQUFPLENBQUNuUyxNQUFNO1VBQ3RCcXRCLGNBQWMsRUFBRUUsY0FBYztVQUM5QkMsWUFBWSxFQUFFcmIsT0FBTyxDQUFDelIsSUFBSTtVQUMxQitZLE1BQU0sRUFBRXRILE9BQU8sQ0FBQ3NILE1BQU07VUFDdEI2VCxhQUFhLEVBQUU7UUFDakIsQ0FBQyxDQUFDO01BQ047SUFDRixDQUFDLE1BQU07TUFDTCxPQUFPcE8sZ0JBQWdCLENBQUNsWixJQUFJLEVBQUVxSSxJQUFJLEVBQUUsSUFBSSxFQUFFLElBQUksRUFBRTtRQUM5Q2lmLGFBQWEsRUFBRTtNQUNqQixDQUFDLENBQUM7SUFDSjtFQUNGOztFQUVBO0FBQ0Y7QUFDQTtBQUNBO0VBQ0UsU0FBU0csZUFBZUEsQ0FBQ3h0QixHQUFHLEVBQUU7SUFDNUIsTUFBTXVNLEdBQUcsR0FBRyxFQUFFO0lBQ2QsT0FBT3ZNLEdBQUcsRUFBRTtNQUNWdU0sR0FBRyxDQUFDRyxJQUFJLENBQUMxTSxHQUFHLENBQUM7TUFDYkEsR0FBRyxHQUFHQSxHQUFHLENBQUMrRyxhQUFhO0lBQ3pCO0lBQ0EsT0FBT3dGLEdBQUc7RUFDWjs7RUFFQTtBQUNGO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7RUFDRSxTQUFTa2hCLFVBQVVBLENBQUN6dEIsR0FBRyxFQUFFb08sSUFBSSxFQUFFc2YsYUFBYSxFQUFFO0lBQzVDLElBQUlDLFFBQVE7SUFDWixJQUFJdGYsR0FBRztJQUNQLElBQUksT0FBT0MsR0FBRyxLQUFLLFVBQVUsRUFBRTtNQUM3QkQsR0FBRyxHQUFHLElBQUlDLEdBQUcsQ0FBQ0YsSUFBSSxFQUFFakgsUUFBUSxDQUFDcVgsUUFBUSxDQUFDTyxJQUFJLENBQUM7TUFDM0MsTUFBTTZPLE1BQU0sR0FBR3ptQixRQUFRLENBQUNxWCxRQUFRLENBQUNvUCxNQUFNO01BQ3ZDRCxRQUFRLEdBQUdDLE1BQU0sS0FBS3ZmLEdBQUcsQ0FBQ3VmLE1BQU07SUFDbEMsQ0FBQyxNQUFNO01BQ1A7TUFDRXZmLEdBQUcsR0FBR0QsSUFBSTtNQUNWdWYsUUFBUSxHQUFHdmQsVUFBVSxDQUFDaEMsSUFBSSxFQUFFakgsUUFBUSxDQUFDcVgsUUFBUSxDQUFDb1AsTUFBTSxDQUFDO0lBQ3ZEO0lBRUEsSUFBSXZ1QixJQUFJLENBQUMwQixNQUFNLENBQUM0QixnQkFBZ0IsRUFBRTtNQUNoQyxJQUFJLENBQUNnckIsUUFBUSxFQUFFO1FBQ2IsT0FBTyxLQUFLO01BQ2Q7SUFDRjtJQUNBLE9BQU9ocUIsWUFBWSxDQUFDM0QsR0FBRyxFQUFFLGtCQUFrQixFQUFFb0YsWUFBWSxDQUFDO01BQUVpSixHQUFHO01BQUVzZjtJQUFTLENBQUMsRUFBRUQsYUFBYSxDQUFDLENBQUM7RUFDOUY7O0VBRUE7QUFDRjtBQUNBO0FBQ0E7RUFDRSxTQUFTbkQsa0JBQWtCQSxDQUFDc0QsR0FBRyxFQUFFO0lBQy9CLElBQUlBLEdBQUcsWUFBWTNFLFFBQVEsRUFBRSxPQUFPMkUsR0FBRztJQUN2QyxNQUFNMUYsUUFBUSxHQUFHLElBQUllLFFBQVEsQ0FBQyxDQUFDO0lBQy9CLEtBQUssTUFBTXpiLEdBQUcsSUFBSW9nQixHQUFHLEVBQUU7TUFDckIsSUFBSUEsR0FBRyxDQUFDbmdCLGNBQWMsQ0FBQ0QsR0FBRyxDQUFDLEVBQUU7UUFDM0IsSUFBSW9nQixHQUFHLENBQUNwZ0IsR0FBRyxDQUFDLElBQUksT0FBT29nQixHQUFHLENBQUNwZ0IsR0FBRyxDQUFDLENBQUN6RCxPQUFPLEtBQUssVUFBVSxFQUFFO1VBQ3RENmpCLEdBQUcsQ0FBQ3BnQixHQUFHLENBQUMsQ0FBQ3pELE9BQU8sQ0FBQyxVQUFTb2UsQ0FBQyxFQUFFO1lBQUVELFFBQVEsQ0FBQ3hlLE1BQU0sQ0FBQzhELEdBQUcsRUFBRTJhLENBQUMsQ0FBQztVQUFDLENBQUMsQ0FBQztRQUMzRCxDQUFDLE1BQU0sSUFBSSxPQUFPeUYsR0FBRyxDQUFDcGdCLEdBQUcsQ0FBQyxLQUFLLFFBQVEsSUFBSSxFQUFFb2dCLEdBQUcsQ0FBQ3BnQixHQUFHLENBQUMsWUFBWXFnQixJQUFJLENBQUMsRUFBRTtVQUN0RTNGLFFBQVEsQ0FBQ3hlLE1BQU0sQ0FBQzhELEdBQUcsRUFBRUksSUFBSSxDQUFDeVgsU0FBUyxDQUFDdUksR0FBRyxDQUFDcGdCLEdBQUcsQ0FBQyxDQUFDLENBQUM7UUFDaEQsQ0FBQyxNQUFNO1VBQ0wwYSxRQUFRLENBQUN4ZSxNQUFNLENBQUM4RCxHQUFHLEVBQUVvZ0IsR0FBRyxDQUFDcGdCLEdBQUcsQ0FBQyxDQUFDO1FBQ2hDO01BQ0Y7SUFDRjtJQUNBLE9BQU8wYSxRQUFRO0VBQ2pCOztFQUVBO0FBQ0Y7QUFDQTtBQUNBO0FBQ0E7QUFDQTtFQUNFLFNBQVM0RixrQkFBa0JBLENBQUM1RixRQUFRLEVBQUUxaEIsSUFBSSxFQUFFdW5CLEtBQUssRUFBRTtJQUNqRDtJQUNBLE9BQU8sSUFBSUMsS0FBSyxDQUFDRCxLQUFLLEVBQUU7TUFDdEJuTyxHQUFHLEVBQUUsU0FBQUEsQ0FBU3JOLE1BQU0sRUFBRS9FLEdBQUcsRUFBRTtRQUN6QixJQUFJLE9BQU9BLEdBQUcsS0FBSyxRQUFRLEVBQUUsT0FBTytFLE1BQU0sQ0FBQy9FLEdBQUcsQ0FBQztRQUMvQyxJQUFJQSxHQUFHLEtBQUssUUFBUSxFQUFFLE9BQU8rRSxNQUFNLENBQUM5SSxNQUFNO1FBQzFDLElBQUkrRCxHQUFHLEtBQUssTUFBTSxFQUFFO1VBQ2xCLE9BQU8sVUFBU3JELEtBQUssRUFBRTtZQUNyQm9JLE1BQU0sQ0FBQzlGLElBQUksQ0FBQ3RDLEtBQUssQ0FBQztZQUNsQitkLFFBQVEsQ0FBQ3hlLE1BQU0sQ0FBQ2xELElBQUksRUFBRTJELEtBQUssQ0FBQztVQUM5QixDQUFDO1FBQ0g7UUFDQSxJQUFJLE9BQU9vSSxNQUFNLENBQUMvRSxHQUFHLENBQUMsS0FBSyxVQUFVLEVBQUU7VUFDckMsT0FBTyxZQUFXO1lBQ2hCK0UsTUFBTSxDQUFDL0UsR0FBRyxDQUFDLENBQUN5Z0IsS0FBSyxDQUFDMWIsTUFBTSxFQUFFMmIsU0FBUyxDQUFDO1lBQ3BDaEcsUUFBUSxDQUFDSSxNQUFNLENBQUM5aEIsSUFBSSxDQUFDO1lBQ3JCK0wsTUFBTSxDQUFDeEksT0FBTyxDQUFDLFVBQVNvZSxDQUFDLEVBQUU7Y0FBRUQsUUFBUSxDQUFDeGUsTUFBTSxDQUFDbEQsSUFBSSxFQUFFMmhCLENBQUMsQ0FBQztZQUFDLENBQUMsQ0FBQztVQUMxRCxDQUFDO1FBQ0g7UUFFQSxJQUFJNVYsTUFBTSxDQUFDL0UsR0FBRyxDQUFDLElBQUkrRSxNQUFNLENBQUMvRSxHQUFHLENBQUMsQ0FBQy9ELE1BQU0sS0FBSyxDQUFDLEVBQUU7VUFDM0MsT0FBTzhJLE1BQU0sQ0FBQy9FLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQztRQUN2QixDQUFDLE1BQU07VUFDTCxPQUFPK0UsTUFBTSxDQUFDL0UsR0FBRyxDQUFDO1FBQ3BCO01BQ0YsQ0FBQztNQUNEbVMsR0FBRyxFQUFFLFNBQUFBLENBQVNwTixNQUFNLEVBQUU0YixLQUFLLEVBQUVoa0IsS0FBSyxFQUFFO1FBQ2xDb0ksTUFBTSxDQUFDNGIsS0FBSyxDQUFDLEdBQUdoa0IsS0FBSztRQUNyQitkLFFBQVEsQ0FBQ0ksTUFBTSxDQUFDOWhCLElBQUksQ0FBQztRQUNyQitMLE1BQU0sQ0FBQ3hJLE9BQU8sQ0FBQyxVQUFTb2UsQ0FBQyxFQUFFO1VBQUVELFFBQVEsQ0FBQ3hlLE1BQU0sQ0FBQ2xELElBQUksRUFBRTJoQixDQUFDLENBQUM7UUFBQyxDQUFDLENBQUM7UUFDeEQsT0FBTyxJQUFJO01BQ2I7SUFDRixDQUFDLENBQUM7RUFDSjs7RUFFQTtBQUNGO0FBQ0E7QUFDQTtFQUNFLFNBQVM2QixhQUFhQSxDQUFDOUIsUUFBUSxFQUFFO0lBQy9CLE9BQU8sSUFBSThGLEtBQUssQ0FBQzlGLFFBQVEsRUFBRTtNQUN6QnRJLEdBQUcsRUFBRSxTQUFBQSxDQUFTck4sTUFBTSxFQUFFL0wsSUFBSSxFQUFFO1FBQzFCLElBQUksT0FBT0EsSUFBSSxLQUFLLFFBQVEsRUFBRTtVQUM1QjtVQUNBLE1BQU11SyxNQUFNLEdBQUdxZCxPQUFPLENBQUN4TyxHQUFHLENBQUNyTixNQUFNLEVBQUUvTCxJQUFJLENBQUM7VUFDeEM7VUFDQSxJQUFJLE9BQU91SyxNQUFNLEtBQUssVUFBVSxFQUFFO1lBQ2hDLE9BQU8sWUFBVztjQUNoQixPQUFPQSxNQUFNLENBQUNrZCxLQUFLLENBQUMvRixRQUFRLEVBQUVnRyxTQUFTLENBQUM7WUFDMUMsQ0FBQztVQUNILENBQUMsTUFBTTtZQUNMLE9BQU9uZCxNQUFNO1VBQ2Y7UUFDRjtRQUNBLElBQUl2SyxJQUFJLEtBQUssUUFBUSxFQUFFO1VBQ3JCO1VBQ0EsT0FBTyxNQUFNc0YsTUFBTSxDQUFDdWlCLFdBQVcsQ0FBQ25HLFFBQVEsQ0FBQztRQUMzQztRQUNBLElBQUkxaEIsSUFBSSxJQUFJK0wsTUFBTSxFQUFFO1VBQ2xCO1VBQ0EsSUFBSSxPQUFPQSxNQUFNLENBQUMvTCxJQUFJLENBQUMsS0FBSyxVQUFVLEVBQUU7WUFDdEMsT0FBTyxZQUFXO2NBQ2hCLE9BQU8waEIsUUFBUSxDQUFDMWhCLElBQUksQ0FBQyxDQUFDeW5CLEtBQUssQ0FBQy9GLFFBQVEsRUFBRWdHLFNBQVMsQ0FBQztZQUNsRCxDQUFDO1VBQ0gsQ0FBQyxNQUFNO1lBQ0wsT0FBTzNiLE1BQU0sQ0FBQy9MLElBQUksQ0FBQztVQUNyQjtRQUNGO1FBQ0EsTUFBTXVuQixLQUFLLEdBQUc3RixRQUFRLENBQUNHLE1BQU0sQ0FBQzdoQixJQUFJLENBQUM7UUFDbkM7UUFDQSxJQUFJdW5CLEtBQUssQ0FBQ3RrQixNQUFNLEtBQUssQ0FBQyxFQUFFO1VBQ3RCLE9BQU94RCxTQUFTO1FBQ2xCLENBQUMsTUFBTSxJQUFJOG5CLEtBQUssQ0FBQ3RrQixNQUFNLEtBQUssQ0FBQyxFQUFFO1VBQzdCLE9BQU9za0IsS0FBSyxDQUFDLENBQUMsQ0FBQztRQUNqQixDQUFDLE1BQU07VUFDTCxPQUFPRCxrQkFBa0IsQ0FBQ3ZiLE1BQU0sRUFBRS9MLElBQUksRUFBRXVuQixLQUFLLENBQUM7UUFDaEQ7TUFDRixDQUFDO01BQ0RwTyxHQUFHLEVBQUUsU0FBQUEsQ0FBU3BOLE1BQU0sRUFBRS9MLElBQUksRUFBRTJELEtBQUssRUFBRTtRQUNqQyxJQUFJLE9BQU8zRCxJQUFJLEtBQUssUUFBUSxFQUFFO1VBQzVCLE9BQU8sS0FBSztRQUNkO1FBQ0ErTCxNQUFNLENBQUMrVixNQUFNLENBQUM5aEIsSUFBSSxDQUFDO1FBQ25CLElBQUkyRCxLQUFLLElBQUksT0FBT0EsS0FBSyxDQUFDSixPQUFPLEtBQUssVUFBVSxFQUFFO1VBQ2hESSxLQUFLLENBQUNKLE9BQU8sQ0FBQyxVQUFTb2UsQ0FBQyxFQUFFO1lBQUU1VixNQUFNLENBQUM3SSxNQUFNLENBQUNsRCxJQUFJLEVBQUUyaEIsQ0FBQyxDQUFDO1VBQUMsQ0FBQyxDQUFDO1FBQ3ZELENBQUMsTUFBTSxJQUFJLE9BQU9oZSxLQUFLLEtBQUssUUFBUSxJQUFJLEVBQUVBLEtBQUssWUFBWTBqQixJQUFJLENBQUMsRUFBRTtVQUNoRXRiLE1BQU0sQ0FBQzdJLE1BQU0sQ0FBQ2xELElBQUksRUFBRW9ILElBQUksQ0FBQ3lYLFNBQVMsQ0FBQ2xiLEtBQUssQ0FBQyxDQUFDO1FBQzVDLENBQUMsTUFBTTtVQUNMb0ksTUFBTSxDQUFDN0ksTUFBTSxDQUFDbEQsSUFBSSxFQUFFMkQsS0FBSyxDQUFDO1FBQzVCO1FBQ0EsT0FBTyxJQUFJO01BQ2IsQ0FBQztNQUNEbWtCLGNBQWMsRUFBRSxTQUFBQSxDQUFTL2IsTUFBTSxFQUFFL0wsSUFBSSxFQUFFO1FBQ3JDLElBQUksT0FBT0EsSUFBSSxLQUFLLFFBQVEsRUFBRTtVQUM1QitMLE1BQU0sQ0FBQytWLE1BQU0sQ0FBQzloQixJQUFJLENBQUM7UUFDckI7UUFDQSxPQUFPLElBQUk7TUFDYixDQUFDO01BQ0Q7TUFDQStuQixPQUFPLEVBQUUsU0FBQUEsQ0FBU2hjLE1BQU0sRUFBRTtRQUN4QixPQUFPNmIsT0FBTyxDQUFDRyxPQUFPLENBQUN6aUIsTUFBTSxDQUFDdWlCLFdBQVcsQ0FBQzliLE1BQU0sQ0FBQyxDQUFDO01BQ3BELENBQUM7TUFDRGljLHdCQUF3QixFQUFFLFNBQUFBLENBQVNqYyxNQUFNLEVBQUVrYyxJQUFJLEVBQUU7UUFDL0MsT0FBT0wsT0FBTyxDQUFDSSx3QkFBd0IsQ0FBQzFpQixNQUFNLENBQUN1aUIsV0FBVyxDQUFDOWIsTUFBTSxDQUFDLEVBQUVrYyxJQUFJLENBQUM7TUFDM0U7SUFDRixDQUFDLENBQUM7RUFDSjs7RUFFQTtBQUNGO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7RUFDRSxTQUFTelAsZ0JBQWdCQSxDQUFDbFosSUFBSSxFQUFFcUksSUFBSSxFQUFFcE8sR0FBRyxFQUFFOE8sS0FBSyxFQUFFNmYsR0FBRyxFQUFFQyxTQUFTLEVBQUU7SUFDaEUsSUFBSUMsT0FBTyxHQUFHLElBQUk7SUFDbEIsSUFBSUMsTUFBTSxHQUFHLElBQUk7SUFDakJILEdBQUcsR0FBR0EsR0FBRyxJQUFJLElBQUksR0FBR0EsR0FBRyxHQUFHLENBQUMsQ0FBQztJQUM1QixJQUFJQSxHQUFHLENBQUN0QixhQUFhLElBQUksT0FBTzBCLE9BQU8sS0FBSyxXQUFXLEVBQUU7TUFDdkQsSUFBSUMsT0FBTyxHQUFHLElBQUlELE9BQU8sQ0FBQyxVQUFTRSxRQUFRLEVBQUVDLE9BQU8sRUFBRTtRQUNwREwsT0FBTyxHQUFHSSxRQUFRO1FBQ2xCSCxNQUFNLEdBQUdJLE9BQU87TUFDbEIsQ0FBQyxDQUFDO0lBQ0o7SUFDQSxJQUFJbHZCLEdBQUcsSUFBSSxJQUFJLEVBQUU7TUFDZkEsR0FBRyxHQUFHa0gsV0FBVyxDQUFDLENBQUMsQ0FBQ21FLElBQUk7SUFDMUI7SUFDQSxNQUFNOGpCLGVBQWUsR0FBR1IsR0FBRyxDQUFDelEsT0FBTyxJQUFJa1Isa0JBQWtCO0lBQ3pELE1BQU01VixNQUFNLEdBQUdtVixHQUFHLENBQUNuVixNQUFNLElBQUksSUFBSTtJQUVqQyxJQUFJLENBQUNuVixZQUFZLENBQUNyRSxHQUFHLENBQUMsRUFBRTtNQUN4QjtNQUNFMkwsU0FBUyxDQUFDa2pCLE9BQU8sQ0FBQztNQUNsQixPQUFPRyxPQUFPO0lBQ2hCO0lBQ0EsTUFBTXhjLE1BQU0sR0FBR21jLEdBQUcsQ0FBQ3ZCLGNBQWMsSUFBSWpsQixTQUFTLENBQUNqRCxTQUFTLENBQUNsRixHQUFHLENBQUMsQ0FBQztJQUM5RCxJQUFJd1MsTUFBTSxJQUFJLElBQUksSUFBSUEsTUFBTSxJQUFJUSxTQUFTLEVBQUU7TUFDekN0TixpQkFBaUIsQ0FBQzFGLEdBQUcsRUFBRSxrQkFBa0IsRUFBRTtRQUFFd1MsTUFBTSxFQUFFOU4saUJBQWlCLENBQUMxRSxHQUFHLEVBQUUsV0FBVztNQUFFLENBQUMsQ0FBQztNQUMzRjJMLFNBQVMsQ0FBQ21qQixNQUFNLENBQUM7TUFDakIsT0FBT0UsT0FBTztJQUNoQjtJQUVBLElBQUlLLE9BQU8sR0FBR3RxQixlQUFlLENBQUMvRSxHQUFHLENBQUM7SUFDbEMsTUFBTXN2QixTQUFTLEdBQUdELE9BQU8sQ0FBQ3ZNLGlCQUFpQjtJQUUzQyxJQUFJd00sU0FBUyxFQUFFO01BQ2IsTUFBTUMsVUFBVSxHQUFHL29CLGVBQWUsQ0FBQzhvQixTQUFTLEVBQUUsWUFBWSxDQUFDO01BQzNELElBQUlDLFVBQVUsSUFBSSxJQUFJLEVBQUU7UUFDdEJuaEIsSUFBSSxHQUFHbWhCLFVBQVU7TUFDbkI7TUFFQSxNQUFNQyxVQUFVLEdBQUdocEIsZUFBZSxDQUFDOG9CLFNBQVMsRUFBRSxZQUFZLENBQUM7TUFDM0QsSUFBSUUsVUFBVSxJQUFJLElBQUksRUFBRTtRQUN4QjtRQUNFLElBQUlBLFVBQVUsQ0FBQ3ZtQixXQUFXLENBQUMsQ0FBQyxLQUFLLFFBQVEsRUFBRTtVQUN6Q2xELElBQUksR0FBSSxxQkFBc0J5cEIsVUFBWTtRQUM1QztNQUNGO0lBQ0Y7SUFFQSxNQUFNQyxlQUFlLEdBQUc5cUIsd0JBQXdCLENBQUMzRSxHQUFHLEVBQUUsWUFBWSxDQUFDO0lBQ25FO0lBQ0EsSUFBSTR1QixTQUFTLEtBQUsxb0IsU0FBUyxFQUFFO01BQzNCLE1BQU13cEIsWUFBWSxHQUFHLFNBQUFBLENBQVNDLGdCQUFnQixFQUFFO1FBQzlDLE9BQU8xUSxnQkFBZ0IsQ0FBQ2xaLElBQUksRUFBRXFJLElBQUksRUFBRXBPLEdBQUcsRUFBRThPLEtBQUssRUFBRTZmLEdBQUcsRUFBRSxDQUFDLENBQUNnQixnQkFBZ0IsQ0FBQztNQUMxRSxDQUFDO01BQ0QsTUFBTUMsY0FBYyxHQUFHO1FBQUVwZCxNQUFNO1FBQUV4UyxHQUFHO1FBQUVvTyxJQUFJO1FBQUVySSxJQUFJO1FBQUU4cEIsZUFBZSxFQUFFL2dCLEtBQUs7UUFBRTZmLEdBQUc7UUFBRWUsWUFBWTtRQUFFSSxRQUFRLEVBQUVMO01BQWdCLENBQUM7TUFDeEgsSUFBSTlyQixZQUFZLENBQUMzRCxHQUFHLEVBQUUsY0FBYyxFQUFFNHZCLGNBQWMsQ0FBQyxLQUFLLEtBQUssRUFBRTtRQUMvRGprQixTQUFTLENBQUNrakIsT0FBTyxDQUFDO1FBQ2xCLE9BQU9HLE9BQU87TUFDaEI7SUFDRjtJQUVBLElBQUllLE9BQU8sR0FBRy92QixHQUFHO0lBQ2pCLElBQUlnd0IsWUFBWSxHQUFHcnJCLHdCQUF3QixDQUFDM0UsR0FBRyxFQUFFLFNBQVMsQ0FBQztJQUMzRCxJQUFJaXdCLGFBQWEsR0FBRyxJQUFJO0lBQ3hCLElBQUlDLFNBQVMsR0FBRyxLQUFLO0lBQ3JCLElBQUlGLFlBQVksRUFBRTtNQUNoQixNQUFNRyxXQUFXLEdBQUdILFlBQVksQ0FBQ2pvQixLQUFLLENBQUMsR0FBRyxDQUFDO01BQzNDLE1BQU1NLFFBQVEsR0FBRzhuQixXQUFXLENBQUMsQ0FBQyxDQUFDLENBQUM3aUIsSUFBSSxDQUFDLENBQUM7TUFDdEMsSUFBSWpGLFFBQVEsS0FBSyxNQUFNLEVBQUU7UUFDdkIwbkIsT0FBTyxHQUFHeHJCLGVBQWUsQ0FBQ3ZFLEdBQUcsRUFBRSxTQUFTLENBQUM7TUFDM0MsQ0FBQyxNQUFNO1FBQ0wrdkIsT0FBTyxHQUFHNW5CLFNBQVMsQ0FBQzVDLGdCQUFnQixDQUFDdkYsR0FBRyxFQUFFcUksUUFBUSxDQUFDLENBQUM7TUFDdEQ7TUFDQTtNQUNBMm5CLFlBQVksR0FBRyxDQUFDRyxXQUFXLENBQUMsQ0FBQyxDQUFDLElBQUksTUFBTSxFQUFFN2lCLElBQUksQ0FBQyxDQUFDO01BQ2hEK2hCLE9BQU8sR0FBR3RxQixlQUFlLENBQUNnckIsT0FBTyxDQUFDO01BQ2xDLElBQUlDLFlBQVksS0FBSyxNQUFNLElBQUlYLE9BQU8sQ0FBQ3hVLEdBQUcsSUFBSXdVLE9BQU8sQ0FBQ2EsU0FBUyxLQUFLLElBQUksRUFBRTtRQUN4RXZrQixTQUFTLENBQUNrakIsT0FBTyxDQUFDO1FBQ2xCLE9BQU9HLE9BQU87TUFDaEIsQ0FBQyxNQUFNLElBQUlnQixZQUFZLEtBQUssT0FBTyxFQUFFO1FBQ25DLElBQUlYLE9BQU8sQ0FBQ3hVLEdBQUcsRUFBRTtVQUNmbFAsU0FBUyxDQUFDa2pCLE9BQU8sQ0FBQztVQUNsQixPQUFPRyxPQUFPO1FBQ2hCLENBQUMsTUFBTTtVQUNMa0IsU0FBUyxHQUFHLElBQUk7UUFDbEI7TUFDRixDQUFDLE1BQU0sSUFBSUYsWUFBWSxLQUFLLFNBQVMsRUFBRTtRQUNyQ3JzQixZQUFZLENBQUNvc0IsT0FBTyxFQUFFLFlBQVksQ0FBQyxFQUFDO01BQ3RDLENBQUMsTUFBTSxJQUFJQyxZQUFZLENBQUNob0IsT0FBTyxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsRUFBRTtRQUM5QyxNQUFNb29CLGFBQWEsR0FBR0osWUFBWSxDQUFDam9CLEtBQUssQ0FBQyxHQUFHLENBQUM7UUFDN0Nrb0IsYUFBYSxHQUFHLENBQUNHLGFBQWEsQ0FBQyxDQUFDLENBQUMsSUFBSSxNQUFNLEVBQUU5aUIsSUFBSSxDQUFDLENBQUM7TUFDckQ7SUFDRjtJQUVBLElBQUkraEIsT0FBTyxDQUFDeFUsR0FBRyxFQUFFO01BQ2YsSUFBSXdVLE9BQU8sQ0FBQ2EsU0FBUyxFQUFFO1FBQ3JCdnNCLFlBQVksQ0FBQ29zQixPQUFPLEVBQUUsWUFBWSxDQUFDLEVBQUM7TUFDdEMsQ0FBQyxNQUFNO1FBQ0wsSUFBSUUsYUFBYSxJQUFJLElBQUksRUFBRTtVQUN6QixJQUFJbmhCLEtBQUssRUFBRTtZQUNULE1BQU1rUixTQUFTLEdBQUdqYixlQUFlLENBQUMrSixLQUFLLENBQUM7WUFDeEMsSUFBSWtSLFNBQVMsSUFBSUEsU0FBUyxDQUFDeEMsV0FBVyxJQUFJd0MsU0FBUyxDQUFDeEMsV0FBVyxDQUFDTSxLQUFLLEVBQUU7Y0FDckVtUyxhQUFhLEdBQUdqUSxTQUFTLENBQUN4QyxXQUFXLENBQUNNLEtBQUs7WUFDN0M7VUFDRjtVQUNBLElBQUltUyxhQUFhLElBQUksSUFBSSxFQUFFO1lBQ3pCQSxhQUFhLEdBQUcsTUFBTTtVQUN4QjtRQUNGO1FBQ0EsSUFBSVosT0FBTyxDQUFDZ0IsY0FBYyxJQUFJLElBQUksRUFBRTtVQUNsQ2hCLE9BQU8sQ0FBQ2dCLGNBQWMsR0FBRyxFQUFFO1FBQzdCO1FBQ0EsSUFBSUosYUFBYSxLQUFLLE9BQU8sSUFBSVosT0FBTyxDQUFDZ0IsY0FBYyxDQUFDM21CLE1BQU0sS0FBSyxDQUFDLEVBQUU7VUFDcEUybEIsT0FBTyxDQUFDZ0IsY0FBYyxDQUFDM2pCLElBQUksQ0FBQyxZQUFXO1lBQ3JDdVMsZ0JBQWdCLENBQUNsWixJQUFJLEVBQUVxSSxJQUFJLEVBQUVwTyxHQUFHLEVBQUU4TyxLQUFLLEVBQUU2ZixHQUFHLENBQUM7VUFDL0MsQ0FBQyxDQUFDO1FBQ0osQ0FBQyxNQUFNLElBQUlzQixhQUFhLEtBQUssS0FBSyxFQUFFO1VBQ2xDWixPQUFPLENBQUNnQixjQUFjLENBQUMzakIsSUFBSSxDQUFDLFlBQVc7WUFDckN1UyxnQkFBZ0IsQ0FBQ2xaLElBQUksRUFBRXFJLElBQUksRUFBRXBPLEdBQUcsRUFBRThPLEtBQUssRUFBRTZmLEdBQUcsQ0FBQztVQUMvQyxDQUFDLENBQUM7UUFDSixDQUFDLE1BQU0sSUFBSXNCLGFBQWEsS0FBSyxNQUFNLEVBQUU7VUFDbkNaLE9BQU8sQ0FBQ2dCLGNBQWMsR0FBRyxFQUFFLEVBQUM7VUFDNUJoQixPQUFPLENBQUNnQixjQUFjLENBQUMzakIsSUFBSSxDQUFDLFlBQVc7WUFDckN1UyxnQkFBZ0IsQ0FBQ2xaLElBQUksRUFBRXFJLElBQUksRUFBRXBPLEdBQUcsRUFBRThPLEtBQUssRUFBRTZmLEdBQUcsQ0FBQztVQUMvQyxDQUFDLENBQUM7UUFDSjtRQUNBaGpCLFNBQVMsQ0FBQ2tqQixPQUFPLENBQUM7UUFDbEIsT0FBT0csT0FBTztNQUNoQjtJQUNGO0lBRUEsTUFBTW5VLEdBQUcsR0FBRyxJQUFJdUwsY0FBYyxDQUFDLENBQUM7SUFDaENpSixPQUFPLENBQUN4VSxHQUFHLEdBQUdBLEdBQUc7SUFDakJ3VSxPQUFPLENBQUNhLFNBQVMsR0FBR0EsU0FBUztJQUM3QixNQUFNSSxjQUFjLEdBQUcsU0FBQUEsQ0FBQSxFQUFXO01BQ2hDakIsT0FBTyxDQUFDeFUsR0FBRyxHQUFHLElBQUk7TUFDbEJ3VSxPQUFPLENBQUNhLFNBQVMsR0FBRyxLQUFLO01BQ3pCLElBQUliLE9BQU8sQ0FBQ2dCLGNBQWMsSUFBSSxJQUFJLElBQ2xDaEIsT0FBTyxDQUFDZ0IsY0FBYyxDQUFDM21CLE1BQU0sR0FBRyxDQUFDLEVBQUU7UUFDakMsTUFBTTZtQixhQUFhLEdBQUdsQixPQUFPLENBQUNnQixjQUFjLENBQUNuZixLQUFLLENBQUMsQ0FBQztRQUNwRHFmLGFBQWEsQ0FBQyxDQUFDO01BQ2pCO0lBQ0YsQ0FBQztJQUNELE1BQU1DLGNBQWMsR0FBRzdyQix3QkFBd0IsQ0FBQzNFLEdBQUcsRUFBRSxXQUFXLENBQUM7SUFDakUsSUFBSXd3QixjQUFjLEVBQUU7TUFDbEIsSUFBSUMsY0FBYyxHQUFHakcsTUFBTSxDQUFDZ0csY0FBYyxDQUFDO01BQzNDO01BQ0EsSUFBSUMsY0FBYyxLQUFLLElBQUksSUFDM0IsQ0FBQzlzQixZQUFZLENBQUMzRCxHQUFHLEVBQUUsYUFBYSxFQUFFO1FBQUV3cUIsTUFBTSxFQUFFaUcsY0FBYztRQUFFamU7TUFBTyxDQUFDLENBQUMsRUFBRTtRQUNyRTdHLFNBQVMsQ0FBQ2tqQixPQUFPLENBQUM7UUFDbEJ5QixjQUFjLENBQUMsQ0FBQztRQUNoQixPQUFPdEIsT0FBTztNQUNoQjtJQUNGO0lBRUEsSUFBSVMsZUFBZSxJQUFJLENBQUNiLFNBQVMsRUFBRTtNQUNqQyxJQUFJLENBQUM4QixPQUFPLENBQUNqQixlQUFlLENBQUMsRUFBRTtRQUM3QjlqQixTQUFTLENBQUNrakIsT0FBTyxDQUFDO1FBQ2xCeUIsY0FBYyxDQUFDLENBQUM7UUFDaEIsT0FBT3RCLE9BQU87TUFDaEI7SUFDRjtJQUVBLElBQUl2RSxPQUFPLEdBQUczbEIsVUFBVSxDQUFDOUUsR0FBRyxFQUFFd1MsTUFBTSxFQUFFaWUsY0FBYyxDQUFDO0lBRXJELElBQUkxcUIsSUFBSSxLQUFLLEtBQUssSUFBSSxDQUFDOGxCLFlBQVksQ0FBQzdyQixHQUFHLENBQUMsRUFBRTtNQUN4Q3lxQixPQUFPLENBQUMsY0FBYyxDQUFDLEdBQUcsbUNBQW1DO0lBQy9EO0lBRUEsSUFBSWtFLEdBQUcsQ0FBQ2xFLE9BQU8sRUFBRTtNQUNmQSxPQUFPLEdBQUdybEIsWUFBWSxDQUFDcWxCLE9BQU8sRUFBRWtFLEdBQUcsQ0FBQ2xFLE9BQU8sQ0FBQztJQUM5QztJQUNBLE1BQU01WSxPQUFPLEdBQUcxUixjQUFjLENBQUNILEdBQUcsRUFBRStGLElBQUksQ0FBQztJQUN6QyxJQUFJMGlCLE1BQU0sR0FBRzVXLE9BQU8sQ0FBQzRXLE1BQU07SUFDM0IsTUFBTWtJLFdBQVcsR0FBRzllLE9BQU8sQ0FBQ3NXLFFBQVE7SUFDcEMsSUFBSXdHLEdBQUcsQ0FBQzV1QixNQUFNLEVBQUU7TUFDZDBwQixnQkFBZ0IsQ0FBQ2tILFdBQVcsRUFBRXBHLGtCQUFrQixDQUFDb0UsR0FBRyxDQUFDNXVCLE1BQU0sQ0FBQyxDQUFDO0lBQy9EO0lBQ0EsTUFBTTRzQixjQUFjLEdBQUdwQyxrQkFBa0IsQ0FBQzFsQixpQkFBaUIsQ0FBQzdFLEdBQUcsQ0FBQyxDQUFDO0lBQ2pFLE1BQU00d0IsV0FBVyxHQUFHbkgsZ0JBQWdCLENBQUNrSCxXQUFXLEVBQUVoRSxjQUFjLENBQUM7SUFDakUsSUFBSWtFLGdCQUFnQixHQUFHcnNCLFlBQVksQ0FBQ29zQixXQUFXLEVBQUU1d0IsR0FBRyxDQUFDO0lBRXJELElBQUlYLElBQUksQ0FBQzBCLE1BQU0sQ0FBQ3lCLG1CQUFtQixJQUFJdUQsSUFBSSxLQUFLLEtBQUssRUFBRTtNQUNyRDhxQixnQkFBZ0IsQ0FBQ2pSLEdBQUcsQ0FBQyx1QkFBdUIsRUFBRXBaLGVBQWUsQ0FBQ2dNLE1BQU0sRUFBRSxJQUFJLENBQUMsSUFBSSxNQUFNLENBQUM7SUFDeEY7O0lBRUE7SUFDQSxJQUFJcEUsSUFBSSxJQUFJLElBQUksSUFBSUEsSUFBSSxLQUFLLEVBQUUsRUFBRTtNQUMvQkEsSUFBSSxHQUFHbEgsV0FBVyxDQUFDLENBQUMsQ0FBQ3NYLFFBQVEsQ0FBQ08sSUFBSTtJQUNwQzs7SUFFQTtBQUNKO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7SUFDSSxNQUFNK1IsaUJBQWlCLEdBQUdwRyxtQkFBbUIsQ0FBQzFxQixHQUFHLEVBQUUsWUFBWSxDQUFDO0lBRWhFLE1BQU0rd0IsWUFBWSxHQUFHaHNCLGVBQWUsQ0FBQy9FLEdBQUcsQ0FBQyxDQUFDc1QsT0FBTztJQUVqRCxJQUFJMGQsWUFBWSxHQUFHM3hCLElBQUksQ0FBQzBCLE1BQU0sQ0FBQzJCLHVCQUF1QixDQUFDc0YsT0FBTyxDQUFDakMsSUFBSSxDQUFDLElBQUksQ0FBQzs7SUFFekU7SUFDQSxNQUFNMm5CLGFBQWEsR0FBRztNQUNwQnBhLE9BQU8sRUFBRXlkLFlBQVk7TUFDckJDLFlBQVk7TUFDWjdJLFFBQVEsRUFBRTBJLGdCQUFnQjtNQUMxQkksVUFBVSxFQUFFaEgsYUFBYSxDQUFDNEcsZ0JBQWdCLENBQUM7TUFDM0NLLGtCQUFrQixFQUFFTixXQUFXO01BQy9CTyxvQkFBb0IsRUFBRWxILGFBQWEsQ0FBQzJHLFdBQVcsQ0FBQztNQUNoRG5HLE9BQU87TUFDUGpZLE1BQU07TUFDTnpNLElBQUk7TUFDSjBpQixNQUFNO01BQ054bUIsZUFBZSxFQUFFMHNCLEdBQUcsQ0FBQ3lDLFdBQVcsSUFBSU4saUJBQWlCLENBQUNNLFdBQVcsSUFBSS94QixJQUFJLENBQUMwQixNQUFNLENBQUNrQixlQUFlO01BQ2hHQyxPQUFPLEVBQUV5c0IsR0FBRyxDQUFDenNCLE9BQU8sSUFBSTR1QixpQkFBaUIsQ0FBQzV1QixPQUFPLElBQUk3QyxJQUFJLENBQUMwQixNQUFNLENBQUNtQixPQUFPO01BQ3hFa00sSUFBSTtNQUNKeWhCLGVBQWUsRUFBRS9nQjtJQUNuQixDQUFDO0lBRUQsSUFBSSxDQUFDbkwsWUFBWSxDQUFDM0QsR0FBRyxFQUFFLG9CQUFvQixFQUFFMHRCLGFBQWEsQ0FBQyxFQUFFO01BQzNEL2hCLFNBQVMsQ0FBQ2tqQixPQUFPLENBQUM7TUFDbEJ5QixjQUFjLENBQUMsQ0FBQztNQUNoQixPQUFPdEIsT0FBTztJQUNoQjs7SUFFQTtJQUNBNWdCLElBQUksR0FBR3NmLGFBQWEsQ0FBQ3RmLElBQUk7SUFDekJySSxJQUFJLEdBQUcybkIsYUFBYSxDQUFDM25CLElBQUk7SUFDekIwa0IsT0FBTyxHQUFHaUQsYUFBYSxDQUFDakQsT0FBTztJQUMvQm9HLGdCQUFnQixHQUFHdEcsa0JBQWtCLENBQUNtRCxhQUFhLENBQUN1RCxVQUFVLENBQUM7SUFDL0R4SSxNQUFNLEdBQUdpRixhQUFhLENBQUNqRixNQUFNO0lBQzdCdUksWUFBWSxHQUFHdEQsYUFBYSxDQUFDc0QsWUFBWTtJQUV6QyxJQUFJdkksTUFBTSxJQUFJQSxNQUFNLENBQUMvZSxNQUFNLEdBQUcsQ0FBQyxFQUFFO01BQy9CL0YsWUFBWSxDQUFDM0QsR0FBRyxFQUFFLHdCQUF3QixFQUFFMHRCLGFBQWEsQ0FBQztNQUMxRC9oQixTQUFTLENBQUNrakIsT0FBTyxDQUFDO01BQ2xCeUIsY0FBYyxDQUFDLENBQUM7TUFDaEIsT0FBT3RCLE9BQU87SUFDaEI7SUFFQSxNQUFNcUMsU0FBUyxHQUFHampCLElBQUksQ0FBQ3JHLEtBQUssQ0FBQyxHQUFHLENBQUM7SUFDakMsTUFBTXVwQixZQUFZLEdBQUdELFNBQVMsQ0FBQyxDQUFDLENBQUM7SUFDakMsTUFBTWpYLE1BQU0sR0FBR2lYLFNBQVMsQ0FBQyxDQUFDLENBQUM7SUFFM0IsSUFBSUUsU0FBUyxHQUFHbmpCLElBQUk7SUFDcEIsSUFBSTRpQixZQUFZLEVBQUU7TUFDaEJPLFNBQVMsR0FBR0QsWUFBWTtNQUN4QixNQUFNRSxTQUFTLEdBQUcsQ0FBQ1gsZ0JBQWdCLENBQUN2WixJQUFJLENBQUMsQ0FBQyxDQUFDbWEsSUFBSSxDQUFDLENBQUMsQ0FBQ0MsSUFBSTtNQUN0RCxJQUFJRixTQUFTLEVBQUU7UUFDYixJQUFJRCxTQUFTLENBQUN2cEIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsRUFBRTtVQUM5QnVwQixTQUFTLElBQUksR0FBRztRQUNsQixDQUFDLE1BQU07VUFDTEEsU0FBUyxJQUFJLEdBQUc7UUFDbEI7UUFDQUEsU0FBUyxJQUFJakgsU0FBUyxDQUFDdUcsZ0JBQWdCLENBQUM7UUFDeEMsSUFBSXpXLE1BQU0sRUFBRTtVQUNWbVgsU0FBUyxJQUFJLEdBQUcsR0FBR25YLE1BQU07UUFDM0I7TUFDRjtJQUNGO0lBRUEsSUFBSSxDQUFDcVQsVUFBVSxDQUFDenRCLEdBQUcsRUFBRXV4QixTQUFTLEVBQUU3RCxhQUFhLENBQUMsRUFBRTtNQUM5Q2hvQixpQkFBaUIsQ0FBQzFGLEdBQUcsRUFBRSxrQkFBa0IsRUFBRTB0QixhQUFhLENBQUM7TUFDekQvaEIsU0FBUyxDQUFDbWpCLE1BQU0sQ0FBQztNQUNqQixPQUFPRSxPQUFPO0lBQ2hCO0lBRUFuVSxHQUFHLENBQUN5TCxJQUFJLENBQUN2Z0IsSUFBSSxDQUFDNHJCLFdBQVcsQ0FBQyxDQUFDLEVBQUVKLFNBQVMsRUFBRSxJQUFJLENBQUM7SUFDN0MxVyxHQUFHLENBQUMrVyxnQkFBZ0IsQ0FBQyxXQUFXLENBQUM7SUFDakMvVyxHQUFHLENBQUM1WSxlQUFlLEdBQUd5ckIsYUFBYSxDQUFDenJCLGVBQWU7SUFDbkQ0WSxHQUFHLENBQUMzWSxPQUFPLEdBQUd3ckIsYUFBYSxDQUFDeHJCLE9BQU87O0lBRW5DO0lBQ0EsSUFBSTR1QixpQkFBaUIsQ0FBQ2UsU0FBUyxFQUFFO01BQ2pDO0lBQUEsQ0FDQyxNQUFNO01BQ0wsS0FBSyxNQUFNL1csTUFBTSxJQUFJMlAsT0FBTyxFQUFFO1FBQzVCLElBQUlBLE9BQU8sQ0FBQy9jLGNBQWMsQ0FBQ29OLE1BQU0sQ0FBQyxFQUFFO1VBQ2xDLE1BQU1nUyxXQUFXLEdBQUdyQyxPQUFPLENBQUMzUCxNQUFNLENBQUM7VUFDbkMrUixvQkFBb0IsQ0FBQ2hTLEdBQUcsRUFBRUMsTUFBTSxFQUFFZ1MsV0FBVyxDQUFDO1FBQ2hEO01BQ0Y7SUFDRjs7SUFFQTtJQUNBLE1BQU1nRixZQUFZLEdBQUc7TUFDbkJqWCxHQUFHO01BQ0hySSxNQUFNO01BQ05rYixhQUFhO01BQ2JpQixHQUFHO01BQ0hyYixPQUFPLEVBQUV5ZCxZQUFZO01BQ3JCdlgsTUFBTTtNQUNOdVksUUFBUSxFQUFFO1FBQ1JDLFdBQVcsRUFBRTVqQixJQUFJO1FBQ2pCNmpCLGdCQUFnQixFQUFFVixTQUFTO1FBQzNCVyxZQUFZLEVBQUUsSUFBSTtRQUNsQjlYO01BQ0Y7SUFDRixDQUFDO0lBRURTLEdBQUcsQ0FBQzJMLE1BQU0sR0FBRyxZQUFXO01BQ3RCLElBQUk7UUFDRixNQUFNMkwsU0FBUyxHQUFHM0UsZUFBZSxDQUFDeHRCLEdBQUcsQ0FBQztRQUN0Qzh4QixZQUFZLENBQUNDLFFBQVEsQ0FBQ0csWUFBWSxHQUFHbkYsbUJBQW1CLENBQUNsUyxHQUFHLENBQUM7UUFDN0RzVSxlQUFlLENBQUNudkIsR0FBRyxFQUFFOHhCLFlBQVksQ0FBQztRQUNsQyxJQUFJQSxZQUFZLENBQUNNLGNBQWMsS0FBSyxJQUFJLEVBQUU7VUFDeEMzSyx1QkFBdUIsQ0FBQ04sVUFBVSxFQUFFa0wsV0FBVyxDQUFDO1FBQ2xEO1FBQ0ExdUIsWUFBWSxDQUFDM0QsR0FBRyxFQUFFLG1CQUFtQixFQUFFOHhCLFlBQVksQ0FBQztRQUNwRG51QixZQUFZLENBQUMzRCxHQUFHLEVBQUUsa0JBQWtCLEVBQUU4eEIsWUFBWSxDQUFDO1FBQ25EO1FBQ0E7UUFDQSxJQUFJLENBQUN6dEIsWUFBWSxDQUFDckUsR0FBRyxDQUFDLEVBQUU7VUFDdEIsSUFBSXN5QixtQkFBbUIsR0FBRyxJQUFJO1VBQzlCLE9BQU9ILFNBQVMsQ0FBQ3pvQixNQUFNLEdBQUcsQ0FBQyxJQUFJNG9CLG1CQUFtQixJQUFJLElBQUksRUFBRTtZQUMxRCxNQUFNQyxvQkFBb0IsR0FBR0osU0FBUyxDQUFDamhCLEtBQUssQ0FBQyxDQUFDO1lBQzlDLElBQUk3TSxZQUFZLENBQUNrdUIsb0JBQW9CLENBQUMsRUFBRTtjQUN0Q0QsbUJBQW1CLEdBQUdDLG9CQUFvQjtZQUM1QztVQUNGO1VBQ0EsSUFBSUQsbUJBQW1CLEVBQUU7WUFDdkIzdUIsWUFBWSxDQUFDMnVCLG1CQUFtQixFQUFFLG1CQUFtQixFQUFFUixZQUFZLENBQUM7WUFDcEVudUIsWUFBWSxDQUFDMnVCLG1CQUFtQixFQUFFLGtCQUFrQixFQUFFUixZQUFZLENBQUM7VUFDckU7UUFDRjtRQUNBbm1CLFNBQVMsQ0FBQ2tqQixPQUFPLENBQUM7UUFDbEJ5QixjQUFjLENBQUMsQ0FBQztNQUNsQixDQUFDLENBQUMsT0FBT3BvQixDQUFDLEVBQUU7UUFDVnhDLGlCQUFpQixDQUFDMUYsR0FBRyxFQUFFLGtCQUFrQixFQUFFb0YsWUFBWSxDQUFDO1VBQUVsQyxLQUFLLEVBQUVnRjtRQUFFLENBQUMsRUFBRTRwQixZQUFZLENBQUMsQ0FBQztRQUNwRixNQUFNNXBCLENBQUM7TUFDVDtJQUNGLENBQUM7SUFDRDJTLEdBQUcsQ0FBQzJYLE9BQU8sR0FBRyxZQUFXO01BQ3ZCL0ssdUJBQXVCLENBQUNOLFVBQVUsRUFBRWtMLFdBQVcsQ0FBQztNQUNoRDNzQixpQkFBaUIsQ0FBQzFGLEdBQUcsRUFBRSxtQkFBbUIsRUFBRTh4QixZQUFZLENBQUM7TUFDekRwc0IsaUJBQWlCLENBQUMxRixHQUFHLEVBQUUsZ0JBQWdCLEVBQUU4eEIsWUFBWSxDQUFDO01BQ3REbm1CLFNBQVMsQ0FBQ21qQixNQUFNLENBQUM7TUFDakJ3QixjQUFjLENBQUMsQ0FBQztJQUNsQixDQUFDO0lBQ0R6VixHQUFHLENBQUM0WCxPQUFPLEdBQUcsWUFBVztNQUN2QmhMLHVCQUF1QixDQUFDTixVQUFVLEVBQUVrTCxXQUFXLENBQUM7TUFDaEQzc0IsaUJBQWlCLENBQUMxRixHQUFHLEVBQUUsbUJBQW1CLEVBQUU4eEIsWUFBWSxDQUFDO01BQ3pEcHNCLGlCQUFpQixDQUFDMUYsR0FBRyxFQUFFLGdCQUFnQixFQUFFOHhCLFlBQVksQ0FBQztNQUN0RG5tQixTQUFTLENBQUNtakIsTUFBTSxDQUFDO01BQ2pCd0IsY0FBYyxDQUFDLENBQUM7SUFDbEIsQ0FBQztJQUNEelYsR0FBRyxDQUFDNlgsU0FBUyxHQUFHLFlBQVc7TUFDekJqTCx1QkFBdUIsQ0FBQ04sVUFBVSxFQUFFa0wsV0FBVyxDQUFDO01BQ2hEM3NCLGlCQUFpQixDQUFDMUYsR0FBRyxFQUFFLG1CQUFtQixFQUFFOHhCLFlBQVksQ0FBQztNQUN6RHBzQixpQkFBaUIsQ0FBQzFGLEdBQUcsRUFBRSxjQUFjLEVBQUU4eEIsWUFBWSxDQUFDO01BQ3BEbm1CLFNBQVMsQ0FBQ21qQixNQUFNLENBQUM7TUFDakJ3QixjQUFjLENBQUMsQ0FBQztJQUNsQixDQUFDO0lBQ0QsSUFBSSxDQUFDM3NCLFlBQVksQ0FBQzNELEdBQUcsRUFBRSxvQkFBb0IsRUFBRTh4QixZQUFZLENBQUMsRUFBRTtNQUMxRG5tQixTQUFTLENBQUNrakIsT0FBTyxDQUFDO01BQ2xCeUIsY0FBYyxDQUFDLENBQUM7TUFDaEIsT0FBT3RCLE9BQU87SUFDaEI7SUFDQSxJQUFJN0gsVUFBVSxHQUFHRCwwQkFBMEIsQ0FBQ2xuQixHQUFHLENBQUM7SUFDaEQsSUFBSXF5QixXQUFXLEdBQUcvSyxlQUFlLENBQUN0bkIsR0FBRyxDQUFDO0lBRXRDZ0ssT0FBTyxDQUFDLENBQUMsV0FBVyxFQUFFLFNBQVMsRUFBRSxVQUFVLEVBQUUsT0FBTyxDQUFDLEVBQUUsVUFBU2tSLFNBQVMsRUFBRTtNQUN6RWxSLE9BQU8sQ0FBQyxDQUFDNlEsR0FBRyxFQUFFQSxHQUFHLENBQUM4WCxNQUFNLENBQUMsRUFBRSxVQUFTbmdCLE1BQU0sRUFBRTtRQUMxQ0EsTUFBTSxDQUFDSyxnQkFBZ0IsQ0FBQ3FJLFNBQVMsRUFBRSxVQUFTcE0sS0FBSyxFQUFFO1VBQ2pEbkwsWUFBWSxDQUFDM0QsR0FBRyxFQUFFLFdBQVcsR0FBR2tiLFNBQVMsRUFBRTtZQUN6QzBYLGdCQUFnQixFQUFFOWpCLEtBQUssQ0FBQzhqQixnQkFBZ0I7WUFDeEMvUixNQUFNLEVBQUUvUixLQUFLLENBQUMrUixNQUFNO1lBQ3BCZ1MsS0FBSyxFQUFFL2pCLEtBQUssQ0FBQytqQjtVQUNmLENBQUMsQ0FBQztRQUNKLENBQUMsQ0FBQztNQUNKLENBQUMsQ0FBQztJQUNKLENBQUMsQ0FBQztJQUNGbHZCLFlBQVksQ0FBQzNELEdBQUcsRUFBRSxpQkFBaUIsRUFBRTh4QixZQUFZLENBQUM7SUFDbEQsTUFBTWdCLE1BQU0sR0FBRzlCLFlBQVksR0FBRyxJQUFJLEdBQUdsRixtQkFBbUIsQ0FBQ2pSLEdBQUcsRUFBRTdhLEdBQUcsRUFBRTZ3QixnQkFBZ0IsQ0FBQztJQUNwRmhXLEdBQUcsQ0FBQ2dNLElBQUksQ0FBQ2lNLE1BQU0sQ0FBQztJQUNoQixPQUFPOUQsT0FBTztFQUNoQjs7RUFFQTtBQUNGO0FBQ0E7QUFDQTtBQUNBOztFQUVFO0FBQ0Y7QUFDQTtBQUNBO0FBQ0E7RUFDRSxTQUFTK0QsdUJBQXVCQSxDQUFDL3lCLEdBQUcsRUFBRTh4QixZQUFZLEVBQUU7SUFDbEQsTUFBTWpYLEdBQUcsR0FBR2lYLFlBQVksQ0FBQ2pYLEdBQUc7O0lBRTVCO0lBQ0E7SUFDQTtJQUNBLElBQUltWSxlQUFlLEdBQUcsSUFBSTtJQUMxQixJQUFJQyxlQUFlLEdBQUcsSUFBSTtJQUMxQixJQUFJaEcsU0FBUyxDQUFDcFMsR0FBRyxFQUFFLFdBQVcsQ0FBQyxFQUFFO01BQy9CbVksZUFBZSxHQUFHblksR0FBRyxDQUFDRyxpQkFBaUIsQ0FBQyxTQUFTLENBQUM7TUFDbERpWSxlQUFlLEdBQUcsTUFBTTtJQUMxQixDQUFDLE1BQU0sSUFBSWhHLFNBQVMsQ0FBQ3BTLEdBQUcsRUFBRSxlQUFlLENBQUMsRUFBRTtNQUMxQ21ZLGVBQWUsR0FBR25ZLEdBQUcsQ0FBQ0csaUJBQWlCLENBQUMsYUFBYSxDQUFDO01BQ3REaVksZUFBZSxHQUFHLE1BQU07SUFDMUIsQ0FBQyxNQUFNLElBQUloRyxTQUFTLENBQUNwUyxHQUFHLEVBQUUsa0JBQWtCLENBQUMsRUFBRTtNQUM3Q21ZLGVBQWUsR0FBR25ZLEdBQUcsQ0FBQ0csaUJBQWlCLENBQUMsZ0JBQWdCLENBQUM7TUFDekRpWSxlQUFlLEdBQUcsU0FBUztJQUM3Qjs7SUFFQTtJQUNBLElBQUlELGVBQWUsRUFBRTtNQUNuQixJQUFJQSxlQUFlLEtBQUssT0FBTyxFQUFFO1FBQy9CLE9BQU8sQ0FBQyxDQUFDO01BQ1gsQ0FBQyxNQUFNO1FBQ0wsT0FBTztVQUNML3lCLElBQUksRUFBRWd6QixlQUFlO1VBQ3JCN2tCLElBQUksRUFBRTRrQjtRQUNSLENBQUM7TUFDSDtJQUNGOztJQUVBO0lBQ0E7SUFDQTtJQUNBLE1BQU1oQixXQUFXLEdBQUdGLFlBQVksQ0FBQ0MsUUFBUSxDQUFDRSxnQkFBZ0I7SUFDMUQsTUFBTUMsWUFBWSxHQUFHSixZQUFZLENBQUNDLFFBQVEsQ0FBQ0csWUFBWTtJQUV2RCxNQUFNZ0IsT0FBTyxHQUFHdnVCLHdCQUF3QixDQUFDM0UsR0FBRyxFQUFFLGFBQWEsQ0FBQztJQUM1RCxNQUFNbXpCLFVBQVUsR0FBR3h1Qix3QkFBd0IsQ0FBQzNFLEdBQUcsRUFBRSxnQkFBZ0IsQ0FBQztJQUNsRSxNQUFNb3pCLGdCQUFnQixHQUFHcnVCLGVBQWUsQ0FBQy9FLEdBQUcsQ0FBQyxDQUFDc1QsT0FBTztJQUVyRCxJQUFJK2YsUUFBUSxHQUFHLElBQUk7SUFDbkIsSUFBSWpsQixJQUFJLEdBQUcsSUFBSTtJQUVmLElBQUk4a0IsT0FBTyxFQUFFO01BQ1hHLFFBQVEsR0FBRyxNQUFNO01BQ2pCamxCLElBQUksR0FBRzhrQixPQUFPO0lBQ2hCLENBQUMsTUFBTSxJQUFJQyxVQUFVLEVBQUU7TUFDckJFLFFBQVEsR0FBRyxTQUFTO01BQ3BCamxCLElBQUksR0FBRytrQixVQUFVO0lBQ25CLENBQUMsTUFBTSxJQUFJQyxnQkFBZ0IsRUFBRTtNQUMzQkMsUUFBUSxHQUFHLE1BQU07TUFDakJqbEIsSUFBSSxHQUFHOGpCLFlBQVksSUFBSUYsV0FBVyxFQUFDO0lBQ3JDO0lBRUEsSUFBSTVqQixJQUFJLEVBQUU7TUFDVjtNQUNFLElBQUlBLElBQUksS0FBSyxPQUFPLEVBQUU7UUFDcEIsT0FBTyxDQUFDLENBQUM7TUFDWDs7TUFFQTtNQUNBLElBQUlBLElBQUksS0FBSyxNQUFNLEVBQUU7UUFDbkJBLElBQUksR0FBRzhqQixZQUFZLElBQUlGLFdBQVcsRUFBQztNQUNyQzs7TUFFQTtNQUNBLElBQUlGLFlBQVksQ0FBQ0MsUUFBUSxDQUFDM1gsTUFBTSxJQUFJaE0sSUFBSSxDQUFDcEcsT0FBTyxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsQ0FBQyxFQUFFO1FBQzVEb0csSUFBSSxHQUFHQSxJQUFJLEdBQUcsR0FBRyxHQUFHMGpCLFlBQVksQ0FBQ0MsUUFBUSxDQUFDM1gsTUFBTTtNQUNsRDtNQUVBLE9BQU87UUFDTG5hLElBQUksRUFBRW96QixRQUFRO1FBQ2RqbEI7TUFDRixDQUFDO0lBQ0gsQ0FBQyxNQUFNO01BQ0wsT0FBTyxDQUFDLENBQUM7SUFDWDtFQUNGOztFQUVBO0FBQ0Y7QUFDQTtBQUNBO0FBQ0E7RUFDRSxTQUFTa2xCLFdBQVdBLENBQUNDLHNCQUFzQixFQUFFOU0sTUFBTSxFQUFFO0lBQ25ELElBQUkrTSxNQUFNLEdBQUcsSUFBSUMsTUFBTSxDQUFDRixzQkFBc0IsQ0FBQ3R3QixJQUFJLENBQUM7SUFDcEQsT0FBT3V3QixNQUFNLENBQUN6bEIsSUFBSSxDQUFDMFksTUFBTSxDQUFDeGEsUUFBUSxDQUFDLEVBQUUsQ0FBQyxDQUFDO0VBQ3pDOztFQUVBO0FBQ0Y7QUFDQTtBQUNBO0VBQ0UsU0FBU3luQix1QkFBdUJBLENBQUM3WSxHQUFHLEVBQUU7SUFDcEMsS0FBSyxJQUFJcE8sQ0FBQyxHQUFHLENBQUMsRUFBRUEsQ0FBQyxHQUFHcE4sSUFBSSxDQUFDMEIsTUFBTSxDQUFDaUMsZ0JBQWdCLENBQUMwRyxNQUFNLEVBQUUrQyxDQUFDLEVBQUUsRUFBRTtNQUM1RDtNQUNBLElBQUlrbkIsdUJBQXVCLEdBQUd0MEIsSUFBSSxDQUFDMEIsTUFBTSxDQUFDaUMsZ0JBQWdCLENBQUN5SixDQUFDLENBQUM7TUFDN0QsSUFBSTZtQixXQUFXLENBQUNLLHVCQUF1QixFQUFFOVksR0FBRyxDQUFDNEwsTUFBTSxDQUFDLEVBQUU7UUFDcEQsT0FBT2tOLHVCQUF1QjtNQUNoQztJQUNGO0lBQ0E7SUFDQSxPQUFPO01BQ0xsekIsSUFBSSxFQUFFO0lBQ1IsQ0FBQztFQUNIOztFQUVBO0FBQ0Y7QUFDQTtFQUNFLFNBQVN3WixXQUFXQSxDQUFDM08sS0FBSyxFQUFFO0lBQzFCLElBQUlBLEtBQUssRUFBRTtNQUNULE1BQU1zb0IsUUFBUSxHQUFHaDBCLElBQUksQ0FBQyxPQUFPLENBQUM7TUFDOUIsSUFBSWcwQixRQUFRLEVBQUU7UUFDWkEsUUFBUSxDQUFDOU8sU0FBUyxHQUFHeFosS0FBSztNQUM1QixDQUFDLE1BQU07UUFDTDZCLE1BQU0sQ0FBQ2hHLFFBQVEsQ0FBQ21FLEtBQUssR0FBR0EsS0FBSztNQUMvQjtJQUNGO0VBQ0Y7O0VBRUE7QUFDRjtBQUNBO0FBQ0E7RUFDRSxTQUFTOGpCLGtCQUFrQkEsQ0FBQ3B2QixHQUFHLEVBQUU4eEIsWUFBWSxFQUFFO0lBQzdDLE1BQU1qWCxHQUFHLEdBQUdpWCxZQUFZLENBQUNqWCxHQUFHO0lBQzVCLElBQUlySSxNQUFNLEdBQUdzZixZQUFZLENBQUN0ZixNQUFNO0lBQ2hDLE1BQU1tYyxHQUFHLEdBQUdtRCxZQUFZLENBQUNuRCxHQUFHO0lBQzVCLE1BQU1rRixrQkFBa0IsR0FBRy9CLFlBQVksQ0FBQ3RZLE1BQU07SUFFOUMsSUFBSSxDQUFDN1YsWUFBWSxDQUFDM0QsR0FBRyxFQUFFLG1CQUFtQixFQUFFOHhCLFlBQVksQ0FBQyxFQUFFO0lBRTNELElBQUk3RSxTQUFTLENBQUNwUyxHQUFHLEVBQUUsY0FBYyxDQUFDLEVBQUU7TUFDbENELG1CQUFtQixDQUFDQyxHQUFHLEVBQUUsWUFBWSxFQUFFN2EsR0FBRyxDQUFDO0lBQzdDO0lBRUEsSUFBSWl0QixTQUFTLENBQUNwUyxHQUFHLEVBQUUsZUFBZSxDQUFDLEVBQUU7TUFDbkM4Syx3QkFBd0IsQ0FBQyxDQUFDO01BQzFCLElBQUltTyxZQUFZLEdBQUdqWixHQUFHLENBQUNHLGlCQUFpQixDQUFDLGFBQWEsQ0FBQztNQUN2RDtNQUNBLElBQUkrWSxnQkFBZ0I7TUFDcEIsSUFBSUQsWUFBWSxDQUFDOXJCLE9BQU8sQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLEVBQUU7UUFDbkMrckIsZ0JBQWdCLEdBQUdwbUIsU0FBUyxDQUFDbW1CLFlBQVksQ0FBQztRQUMxQztRQUNBQSxZQUFZLEdBQUdDLGdCQUFnQixDQUFDM2xCLElBQUk7UUFDcEMsT0FBTzJsQixnQkFBZ0IsQ0FBQzNsQixJQUFJO01BQzlCO01BQ0F4SyxVQUFVLENBQUMsS0FBSyxFQUFFa3dCLFlBQVksRUFBRUMsZ0JBQWdCLENBQUMsQ0FBQ0MsSUFBSSxDQUFDLFlBQVc7UUFDaEVqTyxrQkFBa0IsQ0FBQytOLFlBQVksQ0FBQztNQUNsQyxDQUFDLENBQUM7TUFDRjtJQUNGO0lBRUEsTUFBTUcsYUFBYSxHQUFHaEgsU0FBUyxDQUFDcFMsR0FBRyxFQUFFLGNBQWMsQ0FBQyxJQUFJQSxHQUFHLENBQUNHLGlCQUFpQixDQUFDLFlBQVksQ0FBQyxLQUFLLE1BQU07SUFFdEcsSUFBSWlTLFNBQVMsQ0FBQ3BTLEdBQUcsRUFBRSxlQUFlLENBQUMsRUFBRTtNQUNuQ2lYLFlBQVksQ0FBQ00sY0FBYyxHQUFHLElBQUk7TUFDbEM1VCxRQUFRLENBQUNPLElBQUksR0FBR2xFLEdBQUcsQ0FBQ0csaUJBQWlCLENBQUMsYUFBYSxDQUFDO01BQ3BEaVosYUFBYSxJQUFJelYsUUFBUSxDQUFDeUksTUFBTSxDQUFDLENBQUM7TUFDbEM7SUFDRjtJQUVBLElBQUlnTixhQUFhLEVBQUU7TUFDakJuQyxZQUFZLENBQUNNLGNBQWMsR0FBRyxJQUFJO01BQ2xDNVQsUUFBUSxDQUFDeUksTUFBTSxDQUFDLENBQUM7TUFDakI7SUFDRjtJQUVBLElBQUlnRyxTQUFTLENBQUNwUyxHQUFHLEVBQUUsZUFBZSxDQUFDLEVBQUU7TUFDbkMsSUFBSUEsR0FBRyxDQUFDRyxpQkFBaUIsQ0FBQyxhQUFhLENBQUMsS0FBSyxNQUFNLEVBQUU7UUFDbkQ4VyxZQUFZLENBQUN0ZixNQUFNLEdBQUd4UyxHQUFHO01BQzNCLENBQUMsTUFBTTtRQUNMOHhCLFlBQVksQ0FBQ3RmLE1BQU0sR0FBR3JLLFNBQVMsQ0FBQzVDLGdCQUFnQixDQUFDdkYsR0FBRyxFQUFFNmEsR0FBRyxDQUFDRyxpQkFBaUIsQ0FBQyxhQUFhLENBQUMsQ0FBQyxDQUFDO01BQzlGO0lBQ0Y7SUFFQSxNQUFNa1osYUFBYSxHQUFHbkIsdUJBQXVCLENBQUMveUIsR0FBRyxFQUFFOHhCLFlBQVksQ0FBQztJQUVoRSxNQUFNOXVCLGdCQUFnQixHQUFHMHdCLHVCQUF1QixDQUFDN1ksR0FBRyxDQUFDO0lBQ3JELE1BQU1wRyxVQUFVLEdBQUd6UixnQkFBZ0IsQ0FBQ3ZDLElBQUk7SUFDeEMsSUFBSTB6QixPQUFPLEdBQUcsQ0FBQyxDQUFDbnhCLGdCQUFnQixDQUFDRSxLQUFLO0lBQ3RDLElBQUlOLFdBQVcsR0FBR3ZELElBQUksQ0FBQzBCLE1BQU0sQ0FBQzZCLFdBQVcsSUFBSUksZ0JBQWdCLENBQUNKLFdBQVc7SUFDekUsSUFBSXd4QixjQUFjLEdBQUdweEIsZ0JBQWdCLENBQUN3VyxNQUFNO0lBQzVDLElBQUl4VyxnQkFBZ0IsQ0FBQ3dQLE1BQU0sRUFBRTtNQUMzQnNmLFlBQVksQ0FBQ3RmLE1BQU0sR0FBR3JLLFNBQVMsQ0FBQzVDLGdCQUFnQixDQUFDdkYsR0FBRyxFQUFFZ0QsZ0JBQWdCLENBQUN3UCxNQUFNLENBQUMsQ0FBQztJQUNqRjtJQUNBLElBQUkrYSxZQUFZLEdBQUdvQixHQUFHLENBQUNwQixZQUFZO0lBQ25DLElBQUlBLFlBQVksSUFBSSxJQUFJLElBQUl2cUIsZ0JBQWdCLENBQUN1cUIsWUFBWSxFQUFFO01BQ3pEQSxZQUFZLEdBQUd2cUIsZ0JBQWdCLENBQUN1cUIsWUFBWTtJQUM5Qzs7SUFFQTtJQUNBLElBQUlOLFNBQVMsQ0FBQ3BTLEdBQUcsRUFBRSxlQUFlLENBQUMsRUFBRTtNQUNuQyxJQUFJQSxHQUFHLENBQUNHLGlCQUFpQixDQUFDLGFBQWEsQ0FBQyxLQUFLLE1BQU0sRUFBRTtRQUNuRDhXLFlBQVksQ0FBQ3RmLE1BQU0sR0FBR3hTLEdBQUc7TUFDM0IsQ0FBQyxNQUFNO1FBQ0w4eEIsWUFBWSxDQUFDdGYsTUFBTSxHQUFHckssU0FBUyxDQUFDNUMsZ0JBQWdCLENBQUN2RixHQUFHLEVBQUU2YSxHQUFHLENBQUNHLGlCQUFpQixDQUFDLGFBQWEsQ0FBQyxDQUFDLENBQUM7TUFDOUY7SUFDRjtJQUNBLElBQUlpUyxTQUFTLENBQUNwUyxHQUFHLEVBQUUsYUFBYSxDQUFDLEVBQUU7TUFDakMwUyxZQUFZLEdBQUcxUyxHQUFHLENBQUNHLGlCQUFpQixDQUFDLFdBQVcsQ0FBQztJQUNuRDtJQUVBLElBQUk0TCxjQUFjLEdBQUcvTCxHQUFHLENBQUM5UCxRQUFRO0lBQ2pDO0lBQ0EsSUFBSXlKLGlCQUFpQixHQUFHcFAsWUFBWSxDQUFDO01BQ25DcVAsVUFBVTtNQUNWbVMsY0FBYztNQUNkdU4sT0FBTztNQUNQdnhCLFdBQVc7TUFDWHd4QixjQUFjO01BQ2Q3RztJQUNGLENBQUMsRUFBRXVFLFlBQVksQ0FBQztJQUVoQixJQUFJOXVCLGdCQUFnQixDQUFDOEwsS0FBSyxJQUFJLENBQUNuTCxZQUFZLENBQUM2TyxNQUFNLEVBQUV4UCxnQkFBZ0IsQ0FBQzhMLEtBQUssRUFBRTBGLGlCQUFpQixDQUFDLEVBQUU7SUFFaEcsSUFBSSxDQUFDN1EsWUFBWSxDQUFDNk8sTUFBTSxFQUFFLGlCQUFpQixFQUFFZ0MsaUJBQWlCLENBQUMsRUFBRTtJQUVqRWhDLE1BQU0sR0FBR2dDLGlCQUFpQixDQUFDaEMsTUFBTSxFQUFDO0lBQ2xDb1UsY0FBYyxHQUFHcFMsaUJBQWlCLENBQUNvUyxjQUFjLEVBQUM7SUFDbER1TixPQUFPLEdBQUczZixpQkFBaUIsQ0FBQzJmLE9BQU8sRUFBQztJQUNwQ3Z4QixXQUFXLEdBQUc0UixpQkFBaUIsQ0FBQzVSLFdBQVcsRUFBQztJQUM1Q3d4QixjQUFjLEdBQUc1ZixpQkFBaUIsQ0FBQzRmLGNBQWMsRUFBQztJQUNsRDdHLFlBQVksR0FBRy9ZLGlCQUFpQixDQUFDK1ksWUFBWSxFQUFDOztJQUU5Q3VFLFlBQVksQ0FBQ3RmLE1BQU0sR0FBR0EsTUFBTSxFQUFDO0lBQzdCc2YsWUFBWSxDQUFDdUMsTUFBTSxHQUFHRixPQUFPLEVBQUM7SUFDOUJyQyxZQUFZLENBQUN3QyxVQUFVLEdBQUcsQ0FBQ0gsT0FBTyxFQUFDOztJQUVuQyxJQUFJM2YsaUJBQWlCLENBQUNDLFVBQVUsRUFBRTtNQUNoQyxJQUFJb0csR0FBRyxDQUFDNEwsTUFBTSxLQUFLLEdBQUcsRUFBRTtRQUN0QjFJLGFBQWEsQ0FBQy9kLEdBQUcsQ0FBQztNQUNwQjtNQUVBMkYsY0FBYyxDQUFDM0YsR0FBRyxFQUFFLFVBQVMrVCxTQUFTLEVBQUU7UUFDdEM2UyxjQUFjLEdBQUc3UyxTQUFTLENBQUN3Z0IsaUJBQWlCLENBQUMzTixjQUFjLEVBQUUvTCxHQUFHLEVBQUU3YSxHQUFHLENBQUM7TUFDeEUsQ0FBQyxDQUFDOztNQUVGO01BQ0EsSUFBSWswQixhQUFhLENBQUNqMEIsSUFBSSxFQUFFO1FBQ3RCMGxCLHdCQUF3QixDQUFDLENBQUM7TUFDNUI7TUFFQSxJQUFJaE4sUUFBUSxHQUFHM1Qsb0JBQW9CLENBQUNoRixHQUFHLEVBQUV1dEIsWUFBWSxDQUFDO01BRXRELElBQUksQ0FBQzVVLFFBQVEsQ0FBQ2pMLGNBQWMsQ0FBQyxhQUFhLENBQUMsRUFBRTtRQUMzQ2lMLFFBQVEsQ0FBQy9WLFdBQVcsR0FBR0EsV0FBVztNQUNwQztNQUVBNFAsTUFBTSxDQUFDM0MsU0FBUyxDQUFDQyxHQUFHLENBQUN6USxJQUFJLENBQUMwQixNQUFNLENBQUNZLGFBQWEsQ0FBQzs7TUFFL0M7TUFDQSxJQUFJNnlCLGFBQWEsR0FBRyxJQUFJO01BQ3hCLElBQUlDLFlBQVksR0FBRyxJQUFJO01BRXZCLElBQUlaLGtCQUFrQixFQUFFO1FBQ3RCTyxjQUFjLEdBQUdQLGtCQUFrQjtNQUNyQztNQUVBLElBQUk1RyxTQUFTLENBQUNwUyxHQUFHLEVBQUUsZUFBZSxDQUFDLEVBQUU7UUFDbkN1WixjQUFjLEdBQUd2WixHQUFHLENBQUNHLGlCQUFpQixDQUFDLGFBQWEsQ0FBQztNQUN2RDtNQUVBLE1BQU01QixTQUFTLEdBQUd6VSx3QkFBd0IsQ0FBQzNFLEdBQUcsRUFBRSxlQUFlLENBQUM7TUFDaEUsTUFBTXdaLE1BQU0sR0FBRzdVLHdCQUF3QixDQUFDM0UsR0FBRyxFQUFFLFdBQVcsQ0FBQztNQUV6RCxJQUFJMDBCLE1BQU0sR0FBRyxTQUFBQSxDQUFBLEVBQVc7UUFDdEIsSUFBSTtVQUNGO1VBQ0EsSUFBSVIsYUFBYSxDQUFDajBCLElBQUksRUFBRTtZQUN0QjBELFlBQVksQ0FBQ3VELFdBQVcsQ0FBQyxDQUFDLENBQUNtRSxJQUFJLEVBQUUsMEJBQTBCLEVBQUVqRyxZQUFZLENBQUM7Y0FBRXlnQixPQUFPLEVBQUVxTztZQUFjLENBQUMsRUFBRXBDLFlBQVksQ0FBQyxDQUFDO1lBQ3BILElBQUlvQyxhQUFhLENBQUNqMEIsSUFBSSxLQUFLLE1BQU0sRUFBRTtjQUNqQzhsQixrQkFBa0IsQ0FBQ21PLGFBQWEsQ0FBQzlsQixJQUFJLENBQUM7Y0FDdEN6SyxZQUFZLENBQUN1RCxXQUFXLENBQUMsQ0FBQyxDQUFDbUUsSUFBSSxFQUFFLHdCQUF3QixFQUFFO2dCQUFFK0MsSUFBSSxFQUFFOGxCLGFBQWEsQ0FBQzlsQjtjQUFLLENBQUMsQ0FBQztZQUMxRixDQUFDLE1BQU07Y0FDTDZYLG1CQUFtQixDQUFDaU8sYUFBYSxDQUFDOWxCLElBQUksQ0FBQztjQUN2Q3pLLFlBQVksQ0FBQ3VELFdBQVcsQ0FBQyxDQUFDLENBQUNtRSxJQUFJLEVBQUUsd0JBQXdCLEVBQUU7Z0JBQUUrQyxJQUFJLEVBQUU4bEIsYUFBYSxDQUFDOWxCO2NBQUssQ0FBQyxDQUFDO1lBQzFGO1VBQ0Y7VUFFQTNOLElBQUksQ0FBQytSLE1BQU0sRUFBRW9VLGNBQWMsRUFBRWpPLFFBQVEsRUFBRTtZQUNyQ2EsTUFBTSxFQUFFNGEsY0FBYyxJQUFJNWEsTUFBTTtZQUNoQ0osU0FBUztZQUNUVyxTQUFTLEVBQUUrWCxZQUFZO1lBQ3ZCMVgsTUFBTSxFQUFFMFgsWUFBWSxDQUFDQyxRQUFRLENBQUMzWCxNQUFNO1lBQ3BDdkIsY0FBYyxFQUFFN1ksR0FBRztZQUNuQmdhLGlCQUFpQixFQUFFLFNBQUFBLENBQUEsRUFBVztjQUM1QixJQUFJaVQsU0FBUyxDQUFDcFMsR0FBRyxFQUFFLHlCQUF5QixDQUFDLEVBQUU7Z0JBQzdDLElBQUk4WixRQUFRLEdBQUczMEIsR0FBRztnQkFDbEIsSUFBSSxDQUFDcUUsWUFBWSxDQUFDckUsR0FBRyxDQUFDLEVBQUU7a0JBQ3RCMjBCLFFBQVEsR0FBR3p0QixXQUFXLENBQUMsQ0FBQyxDQUFDbUUsSUFBSTtnQkFDL0I7Z0JBQ0F1UCxtQkFBbUIsQ0FBQ0MsR0FBRyxFQUFFLHVCQUF1QixFQUFFOFosUUFBUSxDQUFDO2NBQzdEO1lBQ0YsQ0FBQztZQUNEamEsbUJBQW1CLEVBQUUsU0FBQUEsQ0FBQSxFQUFXO2NBQzlCLElBQUl1UyxTQUFTLENBQUNwUyxHQUFHLEVBQUUsMkJBQTJCLENBQUMsRUFBRTtnQkFDL0MsSUFBSThaLFFBQVEsR0FBRzMwQixHQUFHO2dCQUNsQixJQUFJLENBQUNxRSxZQUFZLENBQUNyRSxHQUFHLENBQUMsRUFBRTtrQkFDdEIyMEIsUUFBUSxHQUFHenRCLFdBQVcsQ0FBQyxDQUFDLENBQUNtRSxJQUFJO2dCQUMvQjtnQkFDQXVQLG1CQUFtQixDQUFDQyxHQUFHLEVBQUUseUJBQXlCLEVBQUU4WixRQUFRLENBQUM7Y0FDL0Q7Y0FDQWhwQixTQUFTLENBQUM2b0IsYUFBYSxDQUFDO1lBQzFCO1VBQ0YsQ0FBQyxDQUFDO1FBQ0osQ0FBQyxDQUFDLE9BQU90c0IsQ0FBQyxFQUFFO1VBQ1Z4QyxpQkFBaUIsQ0FBQzFGLEdBQUcsRUFBRSxnQkFBZ0IsRUFBRTh4QixZQUFZLENBQUM7VUFDdERubUIsU0FBUyxDQUFDOG9CLFlBQVksQ0FBQztVQUN2QixNQUFNdnNCLENBQUM7UUFDVDtNQUNGLENBQUM7TUFFRCxJQUFJMHNCLGdCQUFnQixHQUFHdjFCLElBQUksQ0FBQzBCLE1BQU0sQ0FBQzBCLHFCQUFxQjtNQUN4RCxJQUFJa1csUUFBUSxDQUFDakwsY0FBYyxDQUFDLFlBQVksQ0FBQyxFQUFFO1FBQ3pDa25CLGdCQUFnQixHQUFHamMsUUFBUSxDQUFDdVMsVUFBVTtNQUN4QztNQUVBLElBQUkwSixnQkFBZ0IsSUFDWmp4QixZQUFZLENBQUMzRCxHQUFHLEVBQUUsdUJBQXVCLEVBQUU4eEIsWUFBWSxDQUFDLElBQ3hELE9BQU8vQyxPQUFPLEtBQUssV0FBVztNQUM5QjtNQUNBNW5CLFFBQVEsQ0FBQzB0QixtQkFBbUIsRUFBRTtRQUNwQyxNQUFNQyxhQUFhLEdBQUcsSUFBSS9GLE9BQU8sQ0FBQyxVQUFTRSxRQUFRLEVBQUVDLE9BQU8sRUFBRTtVQUM1RHNGLGFBQWEsR0FBR3ZGLFFBQVE7VUFDeEJ3RixZQUFZLEdBQUd2RixPQUFPO1FBQ3hCLENBQUMsQ0FBQztRQUNGO1FBQ0EsTUFBTTZGLFdBQVcsR0FBR0wsTUFBTTtRQUMxQkEsTUFBTSxHQUFHLFNBQUFBLENBQUEsRUFBVztVQUNsQjtVQUNBdnRCLFFBQVEsQ0FBQzB0QixtQkFBbUIsQ0FBQyxZQUFXO1lBQ3RDRSxXQUFXLENBQUMsQ0FBQztZQUNiLE9BQU9ELGFBQWE7VUFDdEIsQ0FBQyxDQUFDO1FBQ0osQ0FBQztNQUNIO01BRUEsSUFBSW5jLFFBQVEsQ0FBQ3FTLFNBQVMsR0FBRyxDQUFDLEVBQUU7UUFDMUI5YixTQUFTLENBQUMsQ0FBQyxDQUFDRyxVQUFVLENBQUNxbEIsTUFBTSxFQUFFL2IsUUFBUSxDQUFDcVMsU0FBUyxDQUFDO01BQ3BELENBQUMsTUFBTTtRQUNMMEosTUFBTSxDQUFDLENBQUM7TUFDVjtJQUNGO0lBQ0EsSUFBSVAsT0FBTyxFQUFFO01BQ1h6dUIsaUJBQWlCLENBQUMxRixHQUFHLEVBQUUsb0JBQW9CLEVBQUVvRixZQUFZLENBQUM7UUFBRWxDLEtBQUssRUFBRSw2QkFBNkIsR0FBRzJYLEdBQUcsQ0FBQzRMLE1BQU0sR0FBRyxRQUFRLEdBQUdxTCxZQUFZLENBQUNDLFFBQVEsQ0FBQ0M7TUFBWSxDQUFDLEVBQUVGLFlBQVksQ0FBQyxDQUFDO0lBQ2hMO0VBQ0Y7O0VBRUE7RUFDQTtFQUNBOztFQUVBO0VBQ0EsTUFBTWplLFVBQVUsR0FBRyxDQUFDLENBQUM7O0VBRXJCO0FBQ0Y7QUFDQTtBQUNBO0VBQ0UsU0FBU21oQixhQUFhQSxDQUFBLEVBQUc7SUFDdkIsT0FBTztNQUNMQyxJQUFJLEVBQUUsU0FBQUEsQ0FBU0MsR0FBRyxFQUFFO1FBQUUsT0FBTyxJQUFJO01BQUMsQ0FBQztNQUNuQzFTLFlBQVksRUFBRSxTQUFBQSxDQUFBLEVBQVc7UUFBRSxPQUFPLElBQUk7TUFBQyxDQUFDO01BQ3hDK0IsT0FBTyxFQUFFLFNBQUFBLENBQVM5ZCxJQUFJLEVBQUVtSSxHQUFHLEVBQUU7UUFBRSxPQUFPLElBQUk7TUFBQyxDQUFDO01BQzVDMmxCLGlCQUFpQixFQUFFLFNBQUFBLENBQVNZLElBQUksRUFBRXRhLEdBQUcsRUFBRTdhLEdBQUcsRUFBRTtRQUFFLE9BQU9tMUIsSUFBSTtNQUFDLENBQUM7TUFDM0R4aEIsWUFBWSxFQUFFLFNBQUFBLENBQVNDLFNBQVMsRUFBRTtRQUFFLE9BQU8sS0FBSztNQUFDLENBQUM7TUFDbEQwRSxVQUFVLEVBQUUsU0FBQUEsQ0FBUzFFLFNBQVMsRUFBRXBCLE1BQU0sRUFBRWhKLFFBQVEsRUFBRTBLLFVBQVUsRUFBRTtRQUFFLE9BQU8sS0FBSztNQUFDLENBQUM7TUFDOUUrWCxnQkFBZ0IsRUFBRSxTQUFBQSxDQUFTcFIsR0FBRyxFQUFFb1csVUFBVSxFQUFFanhCLEdBQUcsRUFBRTtRQUFFLE9BQU8sSUFBSTtNQUFDO0lBQ2pFLENBQUM7RUFDSDs7RUFFQTtBQUNGO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0VBQ0UsU0FBU1UsZUFBZUEsQ0FBQytGLElBQUksRUFBRXNOLFNBQVMsRUFBRTtJQUN4QyxJQUFJQSxTQUFTLENBQUNraEIsSUFBSSxFQUFFO01BQ2xCbGhCLFNBQVMsQ0FBQ2toQixJQUFJLENBQUM5d0IsV0FBVyxDQUFDO0lBQzdCO0lBQ0EwUCxVQUFVLENBQUNwTixJQUFJLENBQUMsR0FBR3JCLFlBQVksQ0FBQzR2QixhQUFhLENBQUMsQ0FBQyxFQUFFamhCLFNBQVMsQ0FBQztFQUM3RDs7RUFFQTtBQUNGO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtFQUNFLFNBQVNwVCxlQUFlQSxDQUFDOEYsSUFBSSxFQUFFO0lBQzdCLE9BQU9vTixVQUFVLENBQUNwTixJQUFJLENBQUM7RUFDekI7O0VBRUE7QUFDRjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtFQUNFLFNBQVNxTixhQUFhQSxDQUFDOVQsR0FBRyxFQUFFbzFCLGtCQUFrQixFQUFFQyxrQkFBa0IsRUFBRTtJQUNsRSxJQUFJRCxrQkFBa0IsSUFBSWx2QixTQUFTLEVBQUU7TUFDbkNrdkIsa0JBQWtCLEdBQUcsRUFBRTtJQUN6QjtJQUNBLElBQUlwMUIsR0FBRyxJQUFJa0csU0FBUyxFQUFFO01BQ3BCLE9BQU9rdkIsa0JBQWtCO0lBQzNCO0lBQ0EsSUFBSUMsa0JBQWtCLElBQUludkIsU0FBUyxFQUFFO01BQ25DbXZCLGtCQUFrQixHQUFHLEVBQUU7SUFDekI7SUFDQSxNQUFNQyxvQkFBb0IsR0FBRzV3QixpQkFBaUIsQ0FBQzFFLEdBQUcsRUFBRSxRQUFRLENBQUM7SUFDN0QsSUFBSXMxQixvQkFBb0IsRUFBRTtNQUN4QnRyQixPQUFPLENBQUNzckIsb0JBQW9CLENBQUN2dEIsS0FBSyxDQUFDLEdBQUcsQ0FBQyxFQUFFLFVBQVN3dEIsYUFBYSxFQUFFO1FBQy9EQSxhQUFhLEdBQUdBLGFBQWEsQ0FBQ3RxQixPQUFPLENBQUMsSUFBSSxFQUFFLEVBQUUsQ0FBQztRQUMvQyxJQUFJc3FCLGFBQWEsQ0FBQ2x2QixLQUFLLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxJQUFJLFNBQVMsRUFBRTtVQUMxQ2d2QixrQkFBa0IsQ0FBQzNvQixJQUFJLENBQUM2b0IsYUFBYSxDQUFDbHZCLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQztVQUMvQztRQUNGO1FBQ0EsSUFBSWd2QixrQkFBa0IsQ0FBQ3J0QixPQUFPLENBQUN1dEIsYUFBYSxDQUFDLEdBQUcsQ0FBQyxFQUFFO1VBQ2pELE1BQU14aEIsU0FBUyxHQUFHRixVQUFVLENBQUMwaEIsYUFBYSxDQUFDO1VBQzNDLElBQUl4aEIsU0FBUyxJQUFJcWhCLGtCQUFrQixDQUFDcHRCLE9BQU8sQ0FBQytMLFNBQVMsQ0FBQyxHQUFHLENBQUMsRUFBRTtZQUMxRHFoQixrQkFBa0IsQ0FBQzFvQixJQUFJLENBQUNxSCxTQUFTLENBQUM7VUFDcEM7UUFDRjtNQUNGLENBQUMsQ0FBQztJQUNKO0lBQ0EsT0FBT0QsYUFBYSxDQUFDM0wsU0FBUyxDQUFDdEIsU0FBUyxDQUFDN0csR0FBRyxDQUFDLENBQUMsRUFBRW8xQixrQkFBa0IsRUFBRUMsa0JBQWtCLENBQUM7RUFDekY7O0VBRUE7RUFDQTtFQUNBO0VBQ0EsSUFBSUcsT0FBTyxHQUFHLEtBQUs7RUFDbkJ0dUIsV0FBVyxDQUFDLENBQUMsQ0FBQzJMLGdCQUFnQixDQUFDLGtCQUFrQixFQUFFLFlBQVc7SUFDNUQyaUIsT0FBTyxHQUFHLElBQUk7RUFDaEIsQ0FBQyxDQUFDOztFQUVGO0FBQ0Y7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0VBQ0UsU0FBUzdpQixLQUFLQSxDQUFDOGlCLEVBQUUsRUFBRTtJQUNqQjtJQUNBO0lBQ0EsSUFBSUQsT0FBTyxJQUFJdHVCLFdBQVcsQ0FBQyxDQUFDLENBQUN3dUIsVUFBVSxLQUFLLFVBQVUsRUFBRTtNQUN0REQsRUFBRSxDQUFDLENBQUM7SUFDTixDQUFDLE1BQU07TUFDTHZ1QixXQUFXLENBQUMsQ0FBQyxDQUFDMkwsZ0JBQWdCLENBQUMsa0JBQWtCLEVBQUU0aUIsRUFBRSxDQUFDO0lBQ3hEO0VBQ0Y7RUFFQSxTQUFTRSxxQkFBcUJBLENBQUEsRUFBRztJQUMvQixJQUFJdDJCLElBQUksQ0FBQzBCLE1BQU0sQ0FBQ08sc0JBQXNCLEtBQUssS0FBSyxFQUFFO01BQ2hELE1BQU1zMEIsY0FBYyxHQUFHdjJCLElBQUksQ0FBQzBCLE1BQU0sQ0FBQ2dCLGdCQUFnQixHQUFHLFdBQVcxQyxJQUFJLENBQUMwQixNQUFNLENBQUNnQixnQkFBZ0IsR0FBRyxHQUFHLEVBQUU7TUFDckdtRixXQUFXLENBQUMsQ0FBQyxDQUFDMnVCLElBQUksQ0FBQ3pnQixrQkFBa0IsQ0FBQyxXQUFXLEVBQy9DLFFBQVEsR0FBR3dnQixjQUFjLEdBQUc7QUFDcEMsUUFBUSxHQUFHdjJCLElBQUksQ0FBQzBCLE1BQU0sQ0FBQ1EsY0FBYyxHQUFHO0FBQ3hDLFFBQVEsR0FBR2xDLElBQUksQ0FBQzBCLE1BQU0sQ0FBQ1MsWUFBWSxHQUFHLElBQUksR0FBR25DLElBQUksQ0FBQzBCLE1BQU0sQ0FBQ1EsY0FBYyxHQUFHO0FBQzFFLFFBQVEsR0FBR2xDLElBQUksQ0FBQzBCLE1BQU0sQ0FBQ1MsWUFBWSxHQUFHLEdBQUcsR0FBR25DLElBQUksQ0FBQzBCLE1BQU0sQ0FBQ1EsY0FBYyxHQUFHO0FBQ3pFLGVBQWUsQ0FBQztJQUNaO0VBQ0Y7RUFFQSxTQUFTdTBCLGFBQWFBLENBQUEsRUFBRztJQUN2QjtJQUNBLE1BQU01ZSxPQUFPLEdBQUdoUSxXQUFXLENBQUMsQ0FBQyxDQUFDcUUsYUFBYSxDQUFDLDBCQUEwQixDQUFDO0lBQ3ZFLElBQUkyTCxPQUFPLEVBQUU7TUFDWCxPQUFPdkosU0FBUyxDQUFDdUosT0FBTyxDQUFDMUwsT0FBTyxDQUFDO0lBQ25DLENBQUMsTUFBTTtNQUNMLE9BQU8sSUFBSTtJQUNiO0VBQ0Y7RUFFQSxTQUFTdXFCLGVBQWVBLENBQUEsRUFBRztJQUN6QixNQUFNQyxVQUFVLEdBQUdGLGFBQWEsQ0FBQyxDQUFDO0lBQ2xDLElBQUlFLFVBQVUsRUFBRTtNQUNkMzJCLElBQUksQ0FBQzBCLE1BQU0sR0FBR3FFLFlBQVksQ0FBQy9GLElBQUksQ0FBQzBCLE1BQU0sRUFBRWkxQixVQUFVLENBQUM7SUFDckQ7RUFDRjs7RUFFQTtFQUNBcmpCLEtBQUssQ0FBQyxZQUFXO0lBQ2ZvakIsZUFBZSxDQUFDLENBQUM7SUFDakJKLHFCQUFxQixDQUFDLENBQUM7SUFDdkIsSUFBSXRxQixJQUFJLEdBQUduRSxXQUFXLENBQUMsQ0FBQyxDQUFDbUUsSUFBSTtJQUM3QjdILFdBQVcsQ0FBQzZILElBQUksQ0FBQztJQUNqQixNQUFNNHFCLFlBQVksR0FBRy91QixXQUFXLENBQUMsQ0FBQyxDQUFDMEQsZ0JBQWdCLENBQ2pELHNEQUNGLENBQUM7SUFDRFMsSUFBSSxDQUFDd0gsZ0JBQWdCLENBQUMsWUFBWSxFQUFFLFVBQVNqRSxHQUFHLEVBQUU7TUFDaEQsTUFBTTRELE1BQU0sR0FBRzVELEdBQUcsQ0FBQzRELE1BQU07TUFDekIsTUFBTXNFLFlBQVksR0FBRy9SLGVBQWUsQ0FBQ3lOLE1BQU0sQ0FBQztNQUM1QyxJQUFJc0UsWUFBWSxJQUFJQSxZQUFZLENBQUMrRCxHQUFHLEVBQUU7UUFDcEMvRCxZQUFZLENBQUMrRCxHQUFHLENBQUNxYixLQUFLLENBQUMsQ0FBQztNQUMxQjtJQUNGLENBQUMsQ0FBQztJQUNGO0lBQ0EsTUFBTUMsZ0JBQWdCLEdBQUdocEIsTUFBTSxDQUFDaXBCLFVBQVUsR0FBR2pwQixNQUFNLENBQUNpcEIsVUFBVSxDQUFDQyxJQUFJLENBQUNscEIsTUFBTSxDQUFDLEdBQUcsSUFBSTtJQUNsRjtJQUNBQSxNQUFNLENBQUNpcEIsVUFBVSxHQUFHLFVBQVN0bkIsS0FBSyxFQUFFO01BQ2xDLElBQUlBLEtBQUssQ0FBQ3duQixLQUFLLElBQUl4bkIsS0FBSyxDQUFDd25CLEtBQUssQ0FBQ2ozQixJQUFJLEVBQUU7UUFDbkN5bkIsY0FBYyxDQUFDLENBQUM7UUFDaEI5YyxPQUFPLENBQUNpc0IsWUFBWSxFQUFFLFVBQVNqMkIsR0FBRyxFQUFFO1VBQ2xDMkQsWUFBWSxDQUFDM0QsR0FBRyxFQUFFLGVBQWUsRUFBRTtZQUNqQ21ILFFBQVEsRUFBRUQsV0FBVyxDQUFDLENBQUM7WUFDdkJ2RDtVQUNGLENBQUMsQ0FBQztRQUNKLENBQUMsQ0FBQztNQUNKLENBQUMsTUFBTTtRQUNMLElBQUl3eUIsZ0JBQWdCLEVBQUU7VUFDcEJBLGdCQUFnQixDQUFDcm5CLEtBQUssQ0FBQztRQUN6QjtNQUNGO0lBQ0YsQ0FBQztJQUNESSxTQUFTLENBQUMsQ0FBQyxDQUFDRyxVQUFVLENBQUMsWUFBVztNQUNoQzFMLFlBQVksQ0FBQzBILElBQUksRUFBRSxXQUFXLEVBQUUsQ0FBQyxDQUFDLENBQUMsRUFBQztNQUNwQ0EsSUFBSSxHQUFHLElBQUksRUFBQztJQUNkLENBQUMsRUFBRSxDQUFDLENBQUM7RUFDUCxDQUFDLENBQUM7RUFFRixPQUFPaE0sSUFBSTtBQUNiLENBQUMsQ0FBRSxDQUFDOztBQUVKOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGlFQUFlQSxJQUFJOzs7Ozs7Ozs7OztBQzdvS25COzs7Ozs7O1VDQUE7VUFDQTs7VUFFQTtVQUNBO1VBQ0E7VUFDQTtVQUNBO1VBQ0E7VUFDQTtVQUNBO1VBQ0E7VUFDQTtVQUNBO1VBQ0E7VUFDQTs7VUFFQTtVQUNBOztVQUVBO1VBQ0E7VUFDQTs7Ozs7V0N0QkE7V0FDQTtXQUNBO1dBQ0E7V0FDQSx5Q0FBeUMsd0NBQXdDO1dBQ2pGO1dBQ0E7V0FDQTs7Ozs7V0NQQTs7Ozs7V0NBQTtXQUNBO1dBQ0E7V0FDQSx1REFBdUQsaUJBQWlCO1dBQ3hFO1dBQ0EsZ0RBQWdELGFBQWE7V0FDN0Q7Ozs7Ozs7Ozs7OztBQ042QjtBQUM3QjhOLE1BQU0sQ0FBQzlOLElBQUksR0FBR2szQixtQkFBTyxDQUFDLDBEQUFVLENBQUM7QUFFakNwcEIsTUFBTSxDQUFDMEYsZ0JBQWdCLENBQUMsTUFBTSxFQUFFLE1BQU07RUFDbEM5RCxPQUFPLENBQUN5bkIsS0FBSyxDQUFDLHFCQUFxQixDQUFDO0FBQ3hDLENBQUMsQ0FBQyxDIiwic291cmNlcyI6WyJ3ZWJwYWNrOi8vbGFyYS1kamFuZ28vLi9ub2RlX21vZHVsZXMvaHRteC5vcmcvZGlzdC9odG14LmVzbS5qcyIsIndlYnBhY2s6Ly9sYXJhLWRqYW5nby8uL2xhcmFfZGphbmdvL2Fzc2V0cy9zdHlsZXMvaW5wdXQuY3NzPzY4OWMiLCJ3ZWJwYWNrOi8vbGFyYS1kamFuZ28vd2VicGFjay9ib290c3RyYXAiLCJ3ZWJwYWNrOi8vbGFyYS1kamFuZ28vd2VicGFjay9ydW50aW1lL2RlZmluZSBwcm9wZXJ0eSBnZXR0ZXJzIiwid2VicGFjazovL2xhcmEtZGphbmdvL3dlYnBhY2svcnVudGltZS9oYXNPd25Qcm9wZXJ0eSBzaG9ydGhhbmQiLCJ3ZWJwYWNrOi8vbGFyYS1kamFuZ28vd2VicGFjay9ydW50aW1lL21ha2UgbmFtZXNwYWNlIG9iamVjdCIsIndlYnBhY2s6Ly9sYXJhLWRqYW5nby8uL2xhcmFfZGphbmdvL2Fzc2V0cy9zY3JpcHRzL2luZGV4LmpzIl0sInNvdXJjZXNDb250ZW50IjpbInZhciBodG14ID0gKGZ1bmN0aW9uKCkge1xuICAndXNlIHN0cmljdCdcblxuICAvLyBQdWJsaWMgQVBJXG4gIGNvbnN0IGh0bXggPSB7XG4gICAgLy8gVHNjIG1hZG5lc3MgaGVyZSwgYXNzaWduaW5nIHRoZSBmdW5jdGlvbnMgZGlyZWN0bHkgcmVzdWx0cyBpbiBhbiBpbnZhbGlkIFR5cGVTY3JpcHQgb3V0cHV0LCBidXQgcmVhc3NpZ25pbmcgaXMgZmluZVxuICAgIC8qIEV2ZW50IHByb2Nlc3NpbmcgKi9cbiAgICAvKiogQHR5cGUge3R5cGVvZiBvbkxvYWRIZWxwZXJ9ICovXG4gICAgb25Mb2FkOiBudWxsLFxuICAgIC8qKiBAdHlwZSB7dHlwZW9mIHByb2Nlc3NOb2RlfSAqL1xuICAgIHByb2Nlc3M6IG51bGwsXG4gICAgLyoqIEB0eXBlIHt0eXBlb2YgYWRkRXZlbnRMaXN0ZW5lckltcGx9ICovXG4gICAgb246IG51bGwsXG4gICAgLyoqIEB0eXBlIHt0eXBlb2YgcmVtb3ZlRXZlbnRMaXN0ZW5lckltcGx9ICovXG4gICAgb2ZmOiBudWxsLFxuICAgIC8qKiBAdHlwZSB7dHlwZW9mIHRyaWdnZXJFdmVudH0gKi9cbiAgICB0cmlnZ2VyOiBudWxsLFxuICAgIC8qKiBAdHlwZSB7dHlwZW9mIGFqYXhIZWxwZXJ9ICovXG4gICAgYWpheDogbnVsbCxcbiAgICAvKiBET00gcXVlcnlpbmcgaGVscGVycyAqL1xuICAgIC8qKiBAdHlwZSB7dHlwZW9mIGZpbmR9ICovXG4gICAgZmluZDogbnVsbCxcbiAgICAvKiogQHR5cGUge3R5cGVvZiBmaW5kQWxsfSAqL1xuICAgIGZpbmRBbGw6IG51bGwsXG4gICAgLyoqIEB0eXBlIHt0eXBlb2YgY2xvc2VzdH0gKi9cbiAgICBjbG9zZXN0OiBudWxsLFxuICAgIC8qKlxuICAgICAqIFJldHVybnMgdGhlIGlucHV0IHZhbHVlcyB0aGF0IHdvdWxkIHJlc29sdmUgZm9yIGEgZ2l2ZW4gZWxlbWVudCB2aWEgdGhlIGh0bXggdmFsdWUgcmVzb2x1dGlvbiBtZWNoYW5pc21cbiAgICAgKlxuICAgICAqIEBzZWUgaHR0cHM6Ly9odG14Lm9yZy9hcGkvI3ZhbHVlc1xuICAgICAqXG4gICAgICogQHBhcmFtIHtFbGVtZW50fSBlbHQgdGhlIGVsZW1lbnQgdG8gcmVzb2x2ZSB2YWx1ZXMgb25cbiAgICAgKiBAcGFyYW0ge0h0dHBWZXJifSB0eXBlIHRoZSByZXF1ZXN0IHR5cGUgKGUuZy4gKipnZXQqKiBvciAqKnBvc3QqKikgbm9uLUdFVCdzIHdpbGwgaW5jbHVkZSB0aGUgZW5jbG9zaW5nIGZvcm0gb2YgdGhlIGVsZW1lbnQuIERlZmF1bHRzIHRvICoqcG9zdCoqXG4gICAgICogQHJldHVybnMge09iamVjdH1cbiAgICAgKi9cbiAgICB2YWx1ZXM6IGZ1bmN0aW9uKGVsdCwgdHlwZSkge1xuICAgICAgY29uc3QgaW5wdXRWYWx1ZXMgPSBnZXRJbnB1dFZhbHVlcyhlbHQsIHR5cGUgfHwgJ3Bvc3QnKVxuICAgICAgcmV0dXJuIGlucHV0VmFsdWVzLnZhbHVlc1xuICAgIH0sXG4gICAgLyogRE9NIG1hbmlwdWxhdGlvbiBoZWxwZXJzICovXG4gICAgLyoqIEB0eXBlIHt0eXBlb2YgcmVtb3ZlRWxlbWVudH0gKi9cbiAgICByZW1vdmU6IG51bGwsXG4gICAgLyoqIEB0eXBlIHt0eXBlb2YgYWRkQ2xhc3NUb0VsZW1lbnR9ICovXG4gICAgYWRkQ2xhc3M6IG51bGwsXG4gICAgLyoqIEB0eXBlIHt0eXBlb2YgcmVtb3ZlQ2xhc3NGcm9tRWxlbWVudH0gKi9cbiAgICByZW1vdmVDbGFzczogbnVsbCxcbiAgICAvKiogQHR5cGUge3R5cGVvZiB0b2dnbGVDbGFzc09uRWxlbWVudH0gKi9cbiAgICB0b2dnbGVDbGFzczogbnVsbCxcbiAgICAvKiogQHR5cGUge3R5cGVvZiB0YWtlQ2xhc3NGb3JFbGVtZW50fSAqL1xuICAgIHRha2VDbGFzczogbnVsbCxcbiAgICAvKiogQHR5cGUge3R5cGVvZiBzd2FwfSAqL1xuICAgIHN3YXA6IG51bGwsXG4gICAgLyogRXh0ZW5zaW9uIGVudHJ5cG9pbnRzICovXG4gICAgLyoqIEB0eXBlIHt0eXBlb2YgZGVmaW5lRXh0ZW5zaW9ufSAqL1xuICAgIGRlZmluZUV4dGVuc2lvbjogbnVsbCxcbiAgICAvKiogQHR5cGUge3R5cGVvZiByZW1vdmVFeHRlbnNpb259ICovXG4gICAgcmVtb3ZlRXh0ZW5zaW9uOiBudWxsLFxuICAgIC8qIERlYnVnZ2luZyAqL1xuICAgIC8qKiBAdHlwZSB7dHlwZW9mIGxvZ0FsbH0gKi9cbiAgICBsb2dBbGw6IG51bGwsXG4gICAgLyoqIEB0eXBlIHt0eXBlb2YgbG9nTm9uZX0gKi9cbiAgICBsb2dOb25lOiBudWxsLFxuICAgIC8qIERlYnVnZ2luZyAqL1xuICAgIC8qKlxuICAgICAqIFRoZSBsb2dnZXIgaHRteCB1c2VzIHRvIGxvZyB3aXRoXG4gICAgICpcbiAgICAgKiBAc2VlIGh0dHBzOi8vaHRteC5vcmcvYXBpLyNsb2dnZXJcbiAgICAgKi9cbiAgICBsb2dnZXI6IG51bGwsXG4gICAgLyoqXG4gICAgICogQSBwcm9wZXJ0eSBob2xkaW5nIHRoZSBjb25maWd1cmF0aW9uIGh0bXggdXNlcyBhdCBydW50aW1lLlxuICAgICAqXG4gICAgICogTm90ZSB0aGF0IHVzaW5nIGEgW21ldGEgdGFnXShodHRwczovL2h0bXgub3JnL2RvY3MvI2NvbmZpZykgaXMgdGhlIHByZWZlcnJlZCBtZWNoYW5pc20gZm9yIHNldHRpbmcgdGhlc2UgcHJvcGVydGllcy5cbiAgICAgKlxuICAgICAqIEBzZWUgaHR0cHM6Ly9odG14Lm9yZy9hcGkvI2NvbmZpZ1xuICAgICAqL1xuICAgIGNvbmZpZzoge1xuICAgICAgLyoqXG4gICAgICAgKiBXaGV0aGVyIHRvIHVzZSBoaXN0b3J5LlxuICAgICAgICogQHR5cGUgYm9vbGVhblxuICAgICAgICogQGRlZmF1bHQgdHJ1ZVxuICAgICAgICovXG4gICAgICBoaXN0b3J5RW5hYmxlZDogdHJ1ZSxcbiAgICAgIC8qKlxuICAgICAgICogVGhlIG51bWJlciBvZiBwYWdlcyB0byBrZWVwIGluICoqbG9jYWxTdG9yYWdlKiogZm9yIGhpc3Rvcnkgc3VwcG9ydC5cbiAgICAgICAqIEB0eXBlIG51bWJlclxuICAgICAgICogQGRlZmF1bHQgMTBcbiAgICAgICAqL1xuICAgICAgaGlzdG9yeUNhY2hlU2l6ZTogMTAsXG4gICAgICAvKipcbiAgICAgICAqIEB0eXBlIGJvb2xlYW5cbiAgICAgICAqIEBkZWZhdWx0IGZhbHNlXG4gICAgICAgKi9cbiAgICAgIHJlZnJlc2hPbkhpc3RvcnlNaXNzOiBmYWxzZSxcbiAgICAgIC8qKlxuICAgICAgICogVGhlIGRlZmF1bHQgc3dhcCBzdHlsZSB0byB1c2UgaWYgKipbaHgtc3dhcF0oaHR0cHM6Ly9odG14Lm9yZy9hdHRyaWJ1dGVzL2h4LXN3YXApKiogaXMgb21pdHRlZC5cbiAgICAgICAqIEB0eXBlIEh0bXhTd2FwU3R5bGVcbiAgICAgICAqIEBkZWZhdWx0ICdpbm5lckhUTUwnXG4gICAgICAgKi9cbiAgICAgIGRlZmF1bHRTd2FwU3R5bGU6ICdpbm5lckhUTUwnLFxuICAgICAgLyoqXG4gICAgICAgKiBUaGUgZGVmYXVsdCBkZWxheSBiZXR3ZWVuIHJlY2VpdmluZyBhIHJlc3BvbnNlIGZyb20gdGhlIHNlcnZlciBhbmQgZG9pbmcgdGhlIHN3YXAuXG4gICAgICAgKiBAdHlwZSBudW1iZXJcbiAgICAgICAqIEBkZWZhdWx0IDBcbiAgICAgICAqL1xuICAgICAgZGVmYXVsdFN3YXBEZWxheTogMCxcbiAgICAgIC8qKlxuICAgICAgICogVGhlIGRlZmF1bHQgZGVsYXkgYmV0d2VlbiBjb21wbGV0aW5nIHRoZSBjb250ZW50IHN3YXAgYW5kIHNldHRsaW5nIGF0dHJpYnV0ZXMuXG4gICAgICAgKiBAdHlwZSBudW1iZXJcbiAgICAgICAqIEBkZWZhdWx0IDIwXG4gICAgICAgKi9cbiAgICAgIGRlZmF1bHRTZXR0bGVEZWxheTogMjAsXG4gICAgICAvKipcbiAgICAgICAqIElmIHRydWUsIGh0bXggd2lsbCBpbmplY3QgYSBzbWFsbCBhbW91bnQgb2YgQ1NTIGludG8gdGhlIHBhZ2UgdG8gbWFrZSBpbmRpY2F0b3JzIGludmlzaWJsZSB1bmxlc3MgdGhlICoqaHRteC1pbmRpY2F0b3IqKiBjbGFzcyBpcyBwcmVzZW50LlxuICAgICAgICogQHR5cGUgYm9vbGVhblxuICAgICAgICogQGRlZmF1bHQgdHJ1ZVxuICAgICAgICovXG4gICAgICBpbmNsdWRlSW5kaWNhdG9yU3R5bGVzOiB0cnVlLFxuICAgICAgLyoqXG4gICAgICAgKiBUaGUgY2xhc3MgdG8gcGxhY2Ugb24gaW5kaWNhdG9ycyB3aGVuIGEgcmVxdWVzdCBpcyBpbiBmbGlnaHQuXG4gICAgICAgKiBAdHlwZSBzdHJpbmdcbiAgICAgICAqIEBkZWZhdWx0ICdodG14LWluZGljYXRvcidcbiAgICAgICAqL1xuICAgICAgaW5kaWNhdG9yQ2xhc3M6ICdodG14LWluZGljYXRvcicsXG4gICAgICAvKipcbiAgICAgICAqIFRoZSBjbGFzcyB0byBwbGFjZSBvbiB0cmlnZ2VyaW5nIGVsZW1lbnRzIHdoZW4gYSByZXF1ZXN0IGlzIGluIGZsaWdodC5cbiAgICAgICAqIEB0eXBlIHN0cmluZ1xuICAgICAgICogQGRlZmF1bHQgJ2h0bXgtcmVxdWVzdCdcbiAgICAgICAqL1xuICAgICAgcmVxdWVzdENsYXNzOiAnaHRteC1yZXF1ZXN0JyxcbiAgICAgIC8qKlxuICAgICAgICogVGhlIGNsYXNzIHRvIHRlbXBvcmFyaWx5IHBsYWNlIG9uIGVsZW1lbnRzIHRoYXQgaHRteCBoYXMgYWRkZWQgdG8gdGhlIERPTS5cbiAgICAgICAqIEB0eXBlIHN0cmluZ1xuICAgICAgICogQGRlZmF1bHQgJ2h0bXgtYWRkZWQnXG4gICAgICAgKi9cbiAgICAgIGFkZGVkQ2xhc3M6ICdodG14LWFkZGVkJyxcbiAgICAgIC8qKlxuICAgICAgICogVGhlIGNsYXNzIHRvIHBsYWNlIG9uIHRhcmdldCBlbGVtZW50cyB3aGVuIGh0bXggaXMgaW4gdGhlIHNldHRsaW5nIHBoYXNlLlxuICAgICAgICogQHR5cGUgc3RyaW5nXG4gICAgICAgKiBAZGVmYXVsdCAnaHRteC1zZXR0bGluZydcbiAgICAgICAqL1xuICAgICAgc2V0dGxpbmdDbGFzczogJ2h0bXgtc2V0dGxpbmcnLFxuICAgICAgLyoqXG4gICAgICAgKiBUaGUgY2xhc3MgdG8gcGxhY2Ugb24gdGFyZ2V0IGVsZW1lbnRzIHdoZW4gaHRteCBpcyBpbiB0aGUgc3dhcHBpbmcgcGhhc2UuXG4gICAgICAgKiBAdHlwZSBzdHJpbmdcbiAgICAgICAqIEBkZWZhdWx0ICdodG14LXN3YXBwaW5nJ1xuICAgICAgICovXG4gICAgICBzd2FwcGluZ0NsYXNzOiAnaHRteC1zd2FwcGluZycsXG4gICAgICAvKipcbiAgICAgICAqIEFsbG93cyB0aGUgdXNlIG9mIGV2YWwtbGlrZSBmdW5jdGlvbmFsaXR5IGluIGh0bXgsIHRvIGVuYWJsZSAqKmh4LXZhcnMqKiwgdHJpZ2dlciBjb25kaXRpb25zICYgc2NyaXB0IHRhZyBldmFsdWF0aW9uLiBDYW4gYmUgc2V0IHRvICoqZmFsc2UqKiBmb3IgQ1NQIGNvbXBhdGliaWxpdHkuXG4gICAgICAgKiBAdHlwZSBib29sZWFuXG4gICAgICAgKiBAZGVmYXVsdCB0cnVlXG4gICAgICAgKi9cbiAgICAgIGFsbG93RXZhbDogdHJ1ZSxcbiAgICAgIC8qKlxuICAgICAgICogSWYgc2V0IHRvIGZhbHNlLCBkaXNhYmxlcyB0aGUgaW50ZXJwcmV0YXRpb24gb2Ygc2NyaXB0IHRhZ3MuXG4gICAgICAgKiBAdHlwZSBib29sZWFuXG4gICAgICAgKiBAZGVmYXVsdCB0cnVlXG4gICAgICAgKi9cbiAgICAgIGFsbG93U2NyaXB0VGFnczogdHJ1ZSxcbiAgICAgIC8qKlxuICAgICAgICogSWYgc2V0LCB0aGUgbm9uY2Ugd2lsbCBiZSBhZGRlZCB0byBpbmxpbmUgc2NyaXB0cy5cbiAgICAgICAqIEB0eXBlIHN0cmluZ1xuICAgICAgICogQGRlZmF1bHQgJydcbiAgICAgICAqL1xuICAgICAgaW5saW5lU2NyaXB0Tm9uY2U6ICcnLFxuICAgICAgLyoqXG4gICAgICAgKiBJZiBzZXQsIHRoZSBub25jZSB3aWxsIGJlIGFkZGVkIHRvIGlubGluZSBzdHlsZXMuXG4gICAgICAgKiBAdHlwZSBzdHJpbmdcbiAgICAgICAqIEBkZWZhdWx0ICcnXG4gICAgICAgKi9cbiAgICAgIGlubGluZVN0eWxlTm9uY2U6ICcnLFxuICAgICAgLyoqXG4gICAgICAgKiBUaGUgYXR0cmlidXRlcyB0byBzZXR0bGUgZHVyaW5nIHRoZSBzZXR0bGluZyBwaGFzZS5cbiAgICAgICAqIEB0eXBlIHN0cmluZ1tdXG4gICAgICAgKiBAZGVmYXVsdCBbJ2NsYXNzJywgJ3N0eWxlJywgJ3dpZHRoJywgJ2hlaWdodCddXG4gICAgICAgKi9cbiAgICAgIGF0dHJpYnV0ZXNUb1NldHRsZTogWydjbGFzcycsICdzdHlsZScsICd3aWR0aCcsICdoZWlnaHQnXSxcbiAgICAgIC8qKlxuICAgICAgICogQWxsb3cgY3Jvc3Mtc2l0ZSBBY2Nlc3MtQ29udHJvbCByZXF1ZXN0cyB1c2luZyBjcmVkZW50aWFscyBzdWNoIGFzIGNvb2tpZXMsIGF1dGhvcml6YXRpb24gaGVhZGVycyBvciBUTFMgY2xpZW50IGNlcnRpZmljYXRlcy5cbiAgICAgICAqIEB0eXBlIGJvb2xlYW5cbiAgICAgICAqIEBkZWZhdWx0IGZhbHNlXG4gICAgICAgKi9cbiAgICAgIHdpdGhDcmVkZW50aWFsczogZmFsc2UsXG4gICAgICAvKipcbiAgICAgICAqIEB0eXBlIG51bWJlclxuICAgICAgICogQGRlZmF1bHQgMFxuICAgICAgICovXG4gICAgICB0aW1lb3V0OiAwLFxuICAgICAgLyoqXG4gICAgICAgKiBUaGUgZGVmYXVsdCBpbXBsZW1lbnRhdGlvbiBvZiAqKmdldFdlYlNvY2tldFJlY29ubmVjdERlbGF5KiogZm9yIHJlY29ubmVjdGluZyBhZnRlciB1bmV4cGVjdGVkIGNvbm5lY3Rpb24gbG9zcyBieSB0aGUgZXZlbnQgY29kZSAqKkFibm9ybWFsIENsb3N1cmUqKiwgKipTZXJ2aWNlIFJlc3RhcnQqKiBvciAqKlRyeSBBZ2FpbiBMYXRlcioqLlxuICAgICAgICogQHR5cGUgeydmdWxsLWppdHRlcicgfCAoKHJldHJ5Q291bnQ6bnVtYmVyKSA9PiBudW1iZXIpfVxuICAgICAgICogQGRlZmF1bHQgXCJmdWxsLWppdHRlclwiXG4gICAgICAgKi9cbiAgICAgIHdzUmVjb25uZWN0RGVsYXk6ICdmdWxsLWppdHRlcicsXG4gICAgICAvKipcbiAgICAgICAqIFRoZSB0eXBlIG9mIGJpbmFyeSBkYXRhIGJlaW5nIHJlY2VpdmVkIG92ZXIgdGhlIFdlYlNvY2tldCBjb25uZWN0aW9uXG4gICAgICAgKiBAdHlwZSBCaW5hcnlUeXBlXG4gICAgICAgKiBAZGVmYXVsdCAnYmxvYidcbiAgICAgICAqL1xuICAgICAgd3NCaW5hcnlUeXBlOiAnYmxvYicsXG4gICAgICAvKipcbiAgICAgICAqIEB0eXBlIHN0cmluZ1xuICAgICAgICogQGRlZmF1bHQgJ1toeC1kaXNhYmxlXSwgW2RhdGEtaHgtZGlzYWJsZV0nXG4gICAgICAgKi9cbiAgICAgIGRpc2FibGVTZWxlY3RvcjogJ1toeC1kaXNhYmxlXSwgW2RhdGEtaHgtZGlzYWJsZV0nLFxuICAgICAgLyoqXG4gICAgICAgKiBAdHlwZSB7J2F1dG8nIHwgJ2luc3RhbnQnIHwgJ3Ntb290aCd9XG4gICAgICAgKiBAZGVmYXVsdCAnaW5zdGFudCdcbiAgICAgICAqL1xuICAgICAgc2Nyb2xsQmVoYXZpb3I6ICdpbnN0YW50JyxcbiAgICAgIC8qKlxuICAgICAgICogSWYgdGhlIGZvY3VzZWQgZWxlbWVudCBzaG91bGQgYmUgc2Nyb2xsZWQgaW50byB2aWV3LlxuICAgICAgICogQHR5cGUgYm9vbGVhblxuICAgICAgICogQGRlZmF1bHQgZmFsc2VcbiAgICAgICAqL1xuICAgICAgZGVmYXVsdEZvY3VzU2Nyb2xsOiBmYWxzZSxcbiAgICAgIC8qKlxuICAgICAgICogSWYgc2V0IHRvIHRydWUgaHRteCB3aWxsIGluY2x1ZGUgYSBjYWNoZS1idXN0aW5nIHBhcmFtZXRlciBpbiBHRVQgcmVxdWVzdHMgdG8gYXZvaWQgY2FjaGluZyBwYXJ0aWFsIHJlc3BvbnNlcyBieSB0aGUgYnJvd3NlclxuICAgICAgICogQHR5cGUgYm9vbGVhblxuICAgICAgICogQGRlZmF1bHQgZmFsc2VcbiAgICAgICAqL1xuICAgICAgZ2V0Q2FjaGVCdXN0ZXJQYXJhbTogZmFsc2UsXG4gICAgICAvKipcbiAgICAgICAqIElmIHNldCB0byB0cnVlLCBodG14IHdpbGwgdXNlIHRoZSBWaWV3IFRyYW5zaXRpb24gQVBJIHdoZW4gc3dhcHBpbmcgaW4gbmV3IGNvbnRlbnQuXG4gICAgICAgKiBAdHlwZSBib29sZWFuXG4gICAgICAgKiBAZGVmYXVsdCBmYWxzZVxuICAgICAgICovXG4gICAgICBnbG9iYWxWaWV3VHJhbnNpdGlvbnM6IGZhbHNlLFxuICAgICAgLyoqXG4gICAgICAgKiBodG14IHdpbGwgZm9ybWF0IHJlcXVlc3RzIHdpdGggdGhlc2UgbWV0aG9kcyBieSBlbmNvZGluZyB0aGVpciBwYXJhbWV0ZXJzIGluIHRoZSBVUkwsIG5vdCB0aGUgcmVxdWVzdCBib2R5XG4gICAgICAgKiBAdHlwZSB7KEh0dHBWZXJiKVtdfVxuICAgICAgICogQGRlZmF1bHQgWydnZXQnLCAnZGVsZXRlJ11cbiAgICAgICAqL1xuICAgICAgbWV0aG9kc1RoYXRVc2VVcmxQYXJhbXM6IFsnZ2V0JywgJ2RlbGV0ZSddLFxuICAgICAgLyoqXG4gICAgICAgKiBJZiBzZXQgdG8gdHJ1ZSwgZGlzYWJsZXMgaHRteC1iYXNlZCByZXF1ZXN0cyB0byBub24tb3JpZ2luIGhvc3RzLlxuICAgICAgICogQHR5cGUgYm9vbGVhblxuICAgICAgICogQGRlZmF1bHQgZmFsc2VcbiAgICAgICAqL1xuICAgICAgc2VsZlJlcXVlc3RzT25seTogdHJ1ZSxcbiAgICAgIC8qKlxuICAgICAgICogSWYgc2V0IHRvIHRydWUgaHRteCB3aWxsIG5vdCB1cGRhdGUgdGhlIHRpdGxlIG9mIHRoZSBkb2N1bWVudCB3aGVuIGEgdGl0bGUgdGFnIGlzIGZvdW5kIGluIG5ldyBjb250ZW50XG4gICAgICAgKiBAdHlwZSBib29sZWFuXG4gICAgICAgKiBAZGVmYXVsdCBmYWxzZVxuICAgICAgICovXG4gICAgICBpZ25vcmVUaXRsZTogZmFsc2UsXG4gICAgICAvKipcbiAgICAgICAqIFdoZXRoZXIgdGhlIHRhcmdldCBvZiBhIGJvb3N0ZWQgZWxlbWVudCBpcyBzY3JvbGxlZCBpbnRvIHRoZSB2aWV3cG9ydC5cbiAgICAgICAqIEB0eXBlIGJvb2xlYW5cbiAgICAgICAqIEBkZWZhdWx0IHRydWVcbiAgICAgICAqL1xuICAgICAgc2Nyb2xsSW50b1ZpZXdPbkJvb3N0OiB0cnVlLFxuICAgICAgLyoqXG4gICAgICAgKiBUaGUgY2FjaGUgdG8gc3RvcmUgZXZhbHVhdGVkIHRyaWdnZXIgc3BlY2lmaWNhdGlvbnMgaW50by5cbiAgICAgICAqIFlvdSBtYXkgZGVmaW5lIGEgc2ltcGxlIG9iamVjdCB0byB1c2UgYSBuZXZlci1jbGVhcmluZyBjYWNoZSwgb3IgaW1wbGVtZW50IHlvdXIgb3duIHN5c3RlbSB1c2luZyBhIFtwcm94eSBvYmplY3RdKGh0dHBzOi8vZGV2ZWxvcGVyLm1vemlsbGEub3JnL2RvY3MvV2ViL0phdmFTY3JpcHQvUmVmZXJlbmNlL0dsb2JhbF9PYmplY3RzL1Byb3h5KVxuICAgICAgICogQHR5cGUge09iamVjdHxudWxsfVxuICAgICAgICogQGRlZmF1bHQgbnVsbFxuICAgICAgICovXG4gICAgICB0cmlnZ2VyU3BlY3NDYWNoZTogbnVsbCxcbiAgICAgIC8qKiBAdHlwZSBib29sZWFuICovXG4gICAgICBkaXNhYmxlSW5oZXJpdGFuY2U6IGZhbHNlLFxuICAgICAgLyoqIEB0eXBlIEh0bXhSZXNwb25zZUhhbmRsaW5nQ29uZmlnW10gKi9cbiAgICAgIHJlc3BvbnNlSGFuZGxpbmc6IFtcbiAgICAgICAgeyBjb2RlOiAnMjA0Jywgc3dhcDogZmFsc2UgfSxcbiAgICAgICAgeyBjb2RlOiAnWzIzXS4uJywgc3dhcDogdHJ1ZSB9LFxuICAgICAgICB7IGNvZGU6ICdbNDVdLi4nLCBzd2FwOiBmYWxzZSwgZXJyb3I6IHRydWUgfVxuICAgICAgXSxcbiAgICAgIC8qKlxuICAgICAgICogV2hldGhlciB0byBwcm9jZXNzIE9PQiBzd2FwcyBvbiBlbGVtZW50cyB0aGF0IGFyZSBuZXN0ZWQgd2l0aGluIHRoZSBtYWluIHJlc3BvbnNlIGVsZW1lbnQuXG4gICAgICAgKiBAdHlwZSBib29sZWFuXG4gICAgICAgKiBAZGVmYXVsdCB0cnVlXG4gICAgICAgKi9cbiAgICAgIGFsbG93TmVzdGVkT29iU3dhcHM6IHRydWVcbiAgICB9LFxuICAgIC8qKiBAdHlwZSB7dHlwZW9mIHBhcnNlSW50ZXJ2YWx9ICovXG4gICAgcGFyc2VJbnRlcnZhbDogbnVsbCxcbiAgICAvKiogQHR5cGUge3R5cGVvZiBpbnRlcm5hbEV2YWx9ICovXG4gICAgXzogbnVsbCxcbiAgICB2ZXJzaW9uOiAnMi4wLjQnXG4gIH1cbiAgLy8gVHNjIG1hZG5lc3MgcGFydCAyXG4gIGh0bXgub25Mb2FkID0gb25Mb2FkSGVscGVyXG4gIGh0bXgucHJvY2VzcyA9IHByb2Nlc3NOb2RlXG4gIGh0bXgub24gPSBhZGRFdmVudExpc3RlbmVySW1wbFxuICBodG14Lm9mZiA9IHJlbW92ZUV2ZW50TGlzdGVuZXJJbXBsXG4gIGh0bXgudHJpZ2dlciA9IHRyaWdnZXJFdmVudFxuICBodG14LmFqYXggPSBhamF4SGVscGVyXG4gIGh0bXguZmluZCA9IGZpbmRcbiAgaHRteC5maW5kQWxsID0gZmluZEFsbFxuICBodG14LmNsb3Nlc3QgPSBjbG9zZXN0XG4gIGh0bXgucmVtb3ZlID0gcmVtb3ZlRWxlbWVudFxuICBodG14LmFkZENsYXNzID0gYWRkQ2xhc3NUb0VsZW1lbnRcbiAgaHRteC5yZW1vdmVDbGFzcyA9IHJlbW92ZUNsYXNzRnJvbUVsZW1lbnRcbiAgaHRteC50b2dnbGVDbGFzcyA9IHRvZ2dsZUNsYXNzT25FbGVtZW50XG4gIGh0bXgudGFrZUNsYXNzID0gdGFrZUNsYXNzRm9yRWxlbWVudFxuICBodG14LnN3YXAgPSBzd2FwXG4gIGh0bXguZGVmaW5lRXh0ZW5zaW9uID0gZGVmaW5lRXh0ZW5zaW9uXG4gIGh0bXgucmVtb3ZlRXh0ZW5zaW9uID0gcmVtb3ZlRXh0ZW5zaW9uXG4gIGh0bXgubG9nQWxsID0gbG9nQWxsXG4gIGh0bXgubG9nTm9uZSA9IGxvZ05vbmVcbiAgaHRteC5wYXJzZUludGVydmFsID0gcGFyc2VJbnRlcnZhbFxuICBodG14Ll8gPSBpbnRlcm5hbEV2YWxcblxuICBjb25zdCBpbnRlcm5hbEFQSSA9IHtcbiAgICBhZGRUcmlnZ2VySGFuZGxlcixcbiAgICBib2R5Q29udGFpbnMsXG4gICAgY2FuQWNjZXNzTG9jYWxTdG9yYWdlLFxuICAgIGZpbmRUaGlzRWxlbWVudCxcbiAgICBmaWx0ZXJWYWx1ZXMsXG4gICAgc3dhcCxcbiAgICBoYXNBdHRyaWJ1dGUsXG4gICAgZ2V0QXR0cmlidXRlVmFsdWUsXG4gICAgZ2V0Q2xvc2VzdEF0dHJpYnV0ZVZhbHVlLFxuICAgIGdldENsb3Nlc3RNYXRjaCxcbiAgICBnZXRFeHByZXNzaW9uVmFycyxcbiAgICBnZXRIZWFkZXJzLFxuICAgIGdldElucHV0VmFsdWVzLFxuICAgIGdldEludGVybmFsRGF0YSxcbiAgICBnZXRTd2FwU3BlY2lmaWNhdGlvbixcbiAgICBnZXRUcmlnZ2VyU3BlY3MsXG4gICAgZ2V0VGFyZ2V0LFxuICAgIG1ha2VGcmFnbWVudCxcbiAgICBtZXJnZU9iamVjdHMsXG4gICAgbWFrZVNldHRsZUluZm8sXG4gICAgb29iU3dhcCxcbiAgICBxdWVyeVNlbGVjdG9yRXh0LFxuICAgIHNldHRsZUltbWVkaWF0ZWx5LFxuICAgIHNob3VsZENhbmNlbCxcbiAgICB0cmlnZ2VyRXZlbnQsXG4gICAgdHJpZ2dlckVycm9yRXZlbnQsXG4gICAgd2l0aEV4dGVuc2lvbnNcbiAgfVxuXG4gIGNvbnN0IFZFUkJTID0gWydnZXQnLCAncG9zdCcsICdwdXQnLCAnZGVsZXRlJywgJ3BhdGNoJ11cbiAgY29uc3QgVkVSQl9TRUxFQ1RPUiA9IFZFUkJTLm1hcChmdW5jdGlvbih2ZXJiKSB7XG4gICAgcmV0dXJuICdbaHgtJyArIHZlcmIgKyAnXSwgW2RhdGEtaHgtJyArIHZlcmIgKyAnXSdcbiAgfSkuam9pbignLCAnKVxuXG4gIC8vPSA9PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09XG4gIC8vIFV0aWxpdGllc1xuICAvLz0gPT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PVxuXG4gIC8qKlxuICAgKiBQYXJzZXMgYW4gaW50ZXJ2YWwgc3RyaW5nIGNvbnNpc3RlbnQgd2l0aCB0aGUgd2F5IGh0bXggZG9lcy4gVXNlZnVsIGZvciBwbHVnaW5zIHRoYXQgaGF2ZSB0aW1pbmctcmVsYXRlZCBhdHRyaWJ1dGVzLlxuICAgKlxuICAgKiBDYXV0aW9uOiBBY2NlcHRzIGFuIGludCBmb2xsb3dlZCBieSBlaXRoZXIgKipzKiogb3IgKiptcyoqLiBBbGwgb3RoZXIgdmFsdWVzIHVzZSAqKnBhcnNlRmxvYXQqKlxuICAgKlxuICAgKiBAc2VlIGh0dHBzOi8vaHRteC5vcmcvYXBpLyNwYXJzZUludGVydmFsXG4gICAqXG4gICAqIEBwYXJhbSB7c3RyaW5nfSBzdHIgdGltaW5nIHN0cmluZ1xuICAgKiBAcmV0dXJucyB7bnVtYmVyfHVuZGVmaW5lZH1cbiAgICovXG4gIGZ1bmN0aW9uIHBhcnNlSW50ZXJ2YWwoc3RyKSB7XG4gICAgaWYgKHN0ciA9PSB1bmRlZmluZWQpIHtcbiAgICAgIHJldHVybiB1bmRlZmluZWRcbiAgICB9XG5cbiAgICBsZXQgaW50ZXJ2YWwgPSBOYU5cbiAgICBpZiAoc3RyLnNsaWNlKC0yKSA9PSAnbXMnKSB7XG4gICAgICBpbnRlcnZhbCA9IHBhcnNlRmxvYXQoc3RyLnNsaWNlKDAsIC0yKSlcbiAgICB9IGVsc2UgaWYgKHN0ci5zbGljZSgtMSkgPT0gJ3MnKSB7XG4gICAgICBpbnRlcnZhbCA9IHBhcnNlRmxvYXQoc3RyLnNsaWNlKDAsIC0xKSkgKiAxMDAwXG4gICAgfSBlbHNlIGlmIChzdHIuc2xpY2UoLTEpID09ICdtJykge1xuICAgICAgaW50ZXJ2YWwgPSBwYXJzZUZsb2F0KHN0ci5zbGljZSgwLCAtMSkpICogMTAwMCAqIDYwXG4gICAgfSBlbHNlIHtcbiAgICAgIGludGVydmFsID0gcGFyc2VGbG9hdChzdHIpXG4gICAgfVxuICAgIHJldHVybiBpc05hTihpbnRlcnZhbCkgPyB1bmRlZmluZWQgOiBpbnRlcnZhbFxuICB9XG5cbiAgLyoqXG4gICAqIEBwYXJhbSB7Tm9kZX0gZWx0XG4gICAqIEBwYXJhbSB7c3RyaW5nfSBuYW1lXG4gICAqIEByZXR1cm5zIHsoc3RyaW5nIHwgbnVsbCl9XG4gICAqL1xuICBmdW5jdGlvbiBnZXRSYXdBdHRyaWJ1dGUoZWx0LCBuYW1lKSB7XG4gICAgcmV0dXJuIGVsdCBpbnN0YW5jZW9mIEVsZW1lbnQgJiYgZWx0LmdldEF0dHJpYnV0ZShuYW1lKVxuICB9XG5cbiAgLyoqXG4gICAqIEBwYXJhbSB7RWxlbWVudH0gZWx0XG4gICAqIEBwYXJhbSB7c3RyaW5nfSBxdWFsaWZpZWROYW1lXG4gICAqIEByZXR1cm5zIHtib29sZWFufVxuICAgKi9cbiAgLy8gcmVzb2x2ZSB3aXRoIGJvdGggaHggYW5kIGRhdGEtaHggcHJlZml4ZXNcbiAgZnVuY3Rpb24gaGFzQXR0cmlidXRlKGVsdCwgcXVhbGlmaWVkTmFtZSkge1xuICAgIHJldHVybiAhIWVsdC5oYXNBdHRyaWJ1dGUgJiYgKGVsdC5oYXNBdHRyaWJ1dGUocXVhbGlmaWVkTmFtZSkgfHxcbiAgICAgIGVsdC5oYXNBdHRyaWJ1dGUoJ2RhdGEtJyArIHF1YWxpZmllZE5hbWUpKVxuICB9XG5cbiAgLyoqXG4gICAqXG4gICAqIEBwYXJhbSB7Tm9kZX0gZWx0XG4gICAqIEBwYXJhbSB7c3RyaW5nfSBxdWFsaWZpZWROYW1lXG4gICAqIEByZXR1cm5zIHsoc3RyaW5nIHwgbnVsbCl9XG4gICAqL1xuICBmdW5jdGlvbiBnZXRBdHRyaWJ1dGVWYWx1ZShlbHQsIHF1YWxpZmllZE5hbWUpIHtcbiAgICByZXR1cm4gZ2V0UmF3QXR0cmlidXRlKGVsdCwgcXVhbGlmaWVkTmFtZSkgfHwgZ2V0UmF3QXR0cmlidXRlKGVsdCwgJ2RhdGEtJyArIHF1YWxpZmllZE5hbWUpXG4gIH1cblxuICAvKipcbiAgICogQHBhcmFtIHtOb2RlfSBlbHRcbiAgICogQHJldHVybnMge05vZGUgfCBudWxsfVxuICAgKi9cbiAgZnVuY3Rpb24gcGFyZW50RWx0KGVsdCkge1xuICAgIGNvbnN0IHBhcmVudCA9IGVsdC5wYXJlbnRFbGVtZW50XG4gICAgaWYgKCFwYXJlbnQgJiYgZWx0LnBhcmVudE5vZGUgaW5zdGFuY2VvZiBTaGFkb3dSb290KSByZXR1cm4gZWx0LnBhcmVudE5vZGVcbiAgICByZXR1cm4gcGFyZW50XG4gIH1cblxuICAvKipcbiAgICogQHJldHVybnMge0RvY3VtZW50fVxuICAgKi9cbiAgZnVuY3Rpb24gZ2V0RG9jdW1lbnQoKSB7XG4gICAgcmV0dXJuIGRvY3VtZW50XG4gIH1cblxuICAvKipcbiAgICogQHBhcmFtIHtOb2RlfSBlbHRcbiAgICogQHBhcmFtIHtib29sZWFufSBnbG9iYWxcbiAgICogQHJldHVybnMge05vZGV8RG9jdW1lbnR9XG4gICAqL1xuICBmdW5jdGlvbiBnZXRSb290Tm9kZShlbHQsIGdsb2JhbCkge1xuICAgIHJldHVybiBlbHQuZ2V0Um9vdE5vZGUgPyBlbHQuZ2V0Um9vdE5vZGUoeyBjb21wb3NlZDogZ2xvYmFsIH0pIDogZ2V0RG9jdW1lbnQoKVxuICB9XG5cbiAgLyoqXG4gICAqIEBwYXJhbSB7Tm9kZX0gZWx0XG4gICAqIEBwYXJhbSB7KGU6Tm9kZSkgPT4gYm9vbGVhbn0gY29uZGl0aW9uXG4gICAqIEByZXR1cm5zIHtOb2RlIHwgbnVsbH1cbiAgICovXG4gIGZ1bmN0aW9uIGdldENsb3Nlc3RNYXRjaChlbHQsIGNvbmRpdGlvbikge1xuICAgIHdoaWxlIChlbHQgJiYgIWNvbmRpdGlvbihlbHQpKSB7XG4gICAgICBlbHQgPSBwYXJlbnRFbHQoZWx0KVxuICAgIH1cblxuICAgIHJldHVybiBlbHQgfHwgbnVsbFxuICB9XG5cbiAgLyoqXG4gICAqIEBwYXJhbSB7RWxlbWVudH0gaW5pdGlhbEVsZW1lbnRcbiAgICogQHBhcmFtIHtFbGVtZW50fSBhbmNlc3RvclxuICAgKiBAcGFyYW0ge3N0cmluZ30gYXR0cmlidXRlTmFtZVxuICAgKiBAcmV0dXJucyB7c3RyaW5nfG51bGx9XG4gICAqL1xuICBmdW5jdGlvbiBnZXRBdHRyaWJ1dGVWYWx1ZVdpdGhEaXNpbmhlcml0YW5jZShpbml0aWFsRWxlbWVudCwgYW5jZXN0b3IsIGF0dHJpYnV0ZU5hbWUpIHtcbiAgICBjb25zdCBhdHRyaWJ1dGVWYWx1ZSA9IGdldEF0dHJpYnV0ZVZhbHVlKGFuY2VzdG9yLCBhdHRyaWJ1dGVOYW1lKVxuICAgIGNvbnN0IGRpc2luaGVyaXQgPSBnZXRBdHRyaWJ1dGVWYWx1ZShhbmNlc3RvciwgJ2h4LWRpc2luaGVyaXQnKVxuICAgIHZhciBpbmhlcml0ID0gZ2V0QXR0cmlidXRlVmFsdWUoYW5jZXN0b3IsICdoeC1pbmhlcml0JylcbiAgICBpZiAoaW5pdGlhbEVsZW1lbnQgIT09IGFuY2VzdG9yKSB7XG4gICAgICBpZiAoaHRteC5jb25maWcuZGlzYWJsZUluaGVyaXRhbmNlKSB7XG4gICAgICAgIGlmIChpbmhlcml0ICYmIChpbmhlcml0ID09PSAnKicgfHwgaW5oZXJpdC5zcGxpdCgnICcpLmluZGV4T2YoYXR0cmlidXRlTmFtZSkgPj0gMCkpIHtcbiAgICAgICAgICByZXR1cm4gYXR0cmlidXRlVmFsdWVcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICByZXR1cm4gbnVsbFxuICAgICAgICB9XG4gICAgICB9XG4gICAgICBpZiAoZGlzaW5oZXJpdCAmJiAoZGlzaW5oZXJpdCA9PT0gJyonIHx8IGRpc2luaGVyaXQuc3BsaXQoJyAnKS5pbmRleE9mKGF0dHJpYnV0ZU5hbWUpID49IDApKSB7XG4gICAgICAgIHJldHVybiAndW5zZXQnXG4gICAgICB9XG4gICAgfVxuICAgIHJldHVybiBhdHRyaWJ1dGVWYWx1ZVxuICB9XG5cbiAgLyoqXG4gICAqIEBwYXJhbSB7RWxlbWVudH0gZWx0XG4gICAqIEBwYXJhbSB7c3RyaW5nfSBhdHRyaWJ1dGVOYW1lXG4gICAqIEByZXR1cm5zIHtzdHJpbmcgfCBudWxsfVxuICAgKi9cbiAgZnVuY3Rpb24gZ2V0Q2xvc2VzdEF0dHJpYnV0ZVZhbHVlKGVsdCwgYXR0cmlidXRlTmFtZSkge1xuICAgIGxldCBjbG9zZXN0QXR0ciA9IG51bGxcbiAgICBnZXRDbG9zZXN0TWF0Y2goZWx0LCBmdW5jdGlvbihlKSB7XG4gICAgICByZXR1cm4gISEoY2xvc2VzdEF0dHIgPSBnZXRBdHRyaWJ1dGVWYWx1ZVdpdGhEaXNpbmhlcml0YW5jZShlbHQsIGFzRWxlbWVudChlKSwgYXR0cmlidXRlTmFtZSkpXG4gICAgfSlcbiAgICBpZiAoY2xvc2VzdEF0dHIgIT09ICd1bnNldCcpIHtcbiAgICAgIHJldHVybiBjbG9zZXN0QXR0clxuICAgIH1cbiAgfVxuXG4gIC8qKlxuICAgKiBAcGFyYW0ge05vZGV9IGVsdFxuICAgKiBAcGFyYW0ge3N0cmluZ30gc2VsZWN0b3JcbiAgICogQHJldHVybnMge2Jvb2xlYW59XG4gICAqL1xuICBmdW5jdGlvbiBtYXRjaGVzKGVsdCwgc2VsZWN0b3IpIHtcbiAgICAvLyBAdHMtaWdub3JlOiBub24tc3RhbmRhcmQgcHJvcGVydGllcyBmb3IgYnJvd3NlciBjb21wYXRpYmlsaXR5XG4gICAgLy8gbm9pbnNwZWN0aW9uIEpTVW5yZXNvbHZlZFZhcmlhYmxlXG4gICAgY29uc3QgbWF0Y2hlc0Z1bmN0aW9uID0gZWx0IGluc3RhbmNlb2YgRWxlbWVudCAmJiAoZWx0Lm1hdGNoZXMgfHwgZWx0Lm1hdGNoZXNTZWxlY3RvciB8fCBlbHQubXNNYXRjaGVzU2VsZWN0b3IgfHwgZWx0Lm1vek1hdGNoZXNTZWxlY3RvciB8fCBlbHQud2Via2l0TWF0Y2hlc1NlbGVjdG9yIHx8IGVsdC5vTWF0Y2hlc1NlbGVjdG9yKVxuICAgIHJldHVybiAhIW1hdGNoZXNGdW5jdGlvbiAmJiBtYXRjaGVzRnVuY3Rpb24uY2FsbChlbHQsIHNlbGVjdG9yKVxuICB9XG5cbiAgLyoqXG4gICAqIEBwYXJhbSB7c3RyaW5nfSBzdHJcbiAgICogQHJldHVybnMge3N0cmluZ31cbiAgICovXG4gIGZ1bmN0aW9uIGdldFN0YXJ0VGFnKHN0cikge1xuICAgIGNvbnN0IHRhZ01hdGNoZXIgPSAvPChbYS16XVteXFwvXFwwPlxceDIwXFx0XFxyXFxuXFxmXSopL2lcbiAgICBjb25zdCBtYXRjaCA9IHRhZ01hdGNoZXIuZXhlYyhzdHIpXG4gICAgaWYgKG1hdGNoKSB7XG4gICAgICByZXR1cm4gbWF0Y2hbMV0udG9Mb3dlckNhc2UoKVxuICAgIH0gZWxzZSB7XG4gICAgICByZXR1cm4gJydcbiAgICB9XG4gIH1cblxuICAvKipcbiAgICogQHBhcmFtIHtzdHJpbmd9IHJlc3BcbiAgICogQHJldHVybnMge0RvY3VtZW50fVxuICAgKi9cbiAgZnVuY3Rpb24gcGFyc2VIVE1MKHJlc3ApIHtcbiAgICBjb25zdCBwYXJzZXIgPSBuZXcgRE9NUGFyc2VyKClcbiAgICByZXR1cm4gcGFyc2VyLnBhcnNlRnJvbVN0cmluZyhyZXNwLCAndGV4dC9odG1sJylcbiAgfVxuXG4gIC8qKlxuICAgKiBAcGFyYW0ge0RvY3VtZW50RnJhZ21lbnR9IGZyYWdtZW50XG4gICAqIEBwYXJhbSB7Tm9kZX0gZWx0XG4gICAqL1xuICBmdW5jdGlvbiB0YWtlQ2hpbGRyZW5Gb3IoZnJhZ21lbnQsIGVsdCkge1xuICAgIHdoaWxlIChlbHQuY2hpbGROb2Rlcy5sZW5ndGggPiAwKSB7XG4gICAgICBmcmFnbWVudC5hcHBlbmQoZWx0LmNoaWxkTm9kZXNbMF0pXG4gICAgfVxuICB9XG5cbiAgLyoqXG4gICAqIEBwYXJhbSB7SFRNTFNjcmlwdEVsZW1lbnR9IHNjcmlwdFxuICAgKiBAcmV0dXJucyB7SFRNTFNjcmlwdEVsZW1lbnR9XG4gICAqL1xuICBmdW5jdGlvbiBkdXBsaWNhdGVTY3JpcHQoc2NyaXB0KSB7XG4gICAgY29uc3QgbmV3U2NyaXB0ID0gZ2V0RG9jdW1lbnQoKS5jcmVhdGVFbGVtZW50KCdzY3JpcHQnKVxuICAgIGZvckVhY2goc2NyaXB0LmF0dHJpYnV0ZXMsIGZ1bmN0aW9uKGF0dHIpIHtcbiAgICAgIG5ld1NjcmlwdC5zZXRBdHRyaWJ1dGUoYXR0ci5uYW1lLCBhdHRyLnZhbHVlKVxuICAgIH0pXG4gICAgbmV3U2NyaXB0LnRleHRDb250ZW50ID0gc2NyaXB0LnRleHRDb250ZW50XG4gICAgbmV3U2NyaXB0LmFzeW5jID0gZmFsc2VcbiAgICBpZiAoaHRteC5jb25maWcuaW5saW5lU2NyaXB0Tm9uY2UpIHtcbiAgICAgIG5ld1NjcmlwdC5ub25jZSA9IGh0bXguY29uZmlnLmlubGluZVNjcmlwdE5vbmNlXG4gICAgfVxuICAgIHJldHVybiBuZXdTY3JpcHRcbiAgfVxuXG4gIC8qKlxuICAgKiBAcGFyYW0ge0hUTUxTY3JpcHRFbGVtZW50fSBzY3JpcHRcbiAgICogQHJldHVybnMge2Jvb2xlYW59XG4gICAqL1xuICBmdW5jdGlvbiBpc0phdmFTY3JpcHRTY3JpcHROb2RlKHNjcmlwdCkge1xuICAgIHJldHVybiBzY3JpcHQubWF0Y2hlcygnc2NyaXB0JykgJiYgKHNjcmlwdC50eXBlID09PSAndGV4dC9qYXZhc2NyaXB0JyB8fCBzY3JpcHQudHlwZSA9PT0gJ21vZHVsZScgfHwgc2NyaXB0LnR5cGUgPT09ICcnKVxuICB9XG5cbiAgLyoqXG4gICAqIHdlIGhhdmUgdG8gbWFrZSBuZXcgY29waWVzIG9mIHNjcmlwdCB0YWdzIHRoYXQgd2UgYXJlIGdvaW5nIHRvIGluc2VydCBiZWNhdXNlXG4gICAqIFNPTUUgYnJvd3NlcnMgKG5vdCBzYXlpbmcgd2hvLCBidXQgaXQgaW52b2x2ZXMgYW4gZWxlbWVudCBhbmQgYW4gYW5pbWFsKSBkb24ndFxuICAgKiBleGVjdXRlIHNjcmlwdHMgY3JlYXRlZCBpbiA8dGVtcGxhdGU+IHRhZ3Mgd2hlbiB0aGV5IGFyZSBpbnNlcnRlZCBpbnRvIHRoZSBET01cbiAgICogYW5kIGFsbCB0aGUgb3RoZXJzIGRvIGxtYW9cbiAgICogQHBhcmFtIHtEb2N1bWVudEZyYWdtZW50fSBmcmFnbWVudFxuICAgKi9cbiAgZnVuY3Rpb24gbm9ybWFsaXplU2NyaXB0VGFncyhmcmFnbWVudCkge1xuICAgIEFycmF5LmZyb20oZnJhZ21lbnQucXVlcnlTZWxlY3RvckFsbCgnc2NyaXB0JykpLmZvckVhY2goLyoqIEBwYXJhbSB7SFRNTFNjcmlwdEVsZW1lbnR9IHNjcmlwdCAqLyAoc2NyaXB0KSA9PiB7XG4gICAgICBpZiAoaXNKYXZhU2NyaXB0U2NyaXB0Tm9kZShzY3JpcHQpKSB7XG4gICAgICAgIGNvbnN0IG5ld1NjcmlwdCA9IGR1cGxpY2F0ZVNjcmlwdChzY3JpcHQpXG4gICAgICAgIGNvbnN0IHBhcmVudCA9IHNjcmlwdC5wYXJlbnROb2RlXG4gICAgICAgIHRyeSB7XG4gICAgICAgICAgcGFyZW50Lmluc2VydEJlZm9yZShuZXdTY3JpcHQsIHNjcmlwdClcbiAgICAgICAgfSBjYXRjaCAoZSkge1xuICAgICAgICAgIGxvZ0Vycm9yKGUpXG4gICAgICAgIH0gZmluYWxseSB7XG4gICAgICAgICAgc2NyaXB0LnJlbW92ZSgpXG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9KVxuICB9XG5cbiAgLyoqXG4gICAqIEB0eXBlZGVmIHtEb2N1bWVudEZyYWdtZW50ICYge3RpdGxlPzogc3RyaW5nfX0gRG9jdW1lbnRGcmFnbWVudFdpdGhUaXRsZVxuICAgKiBAZGVzY3JpcHRpb24gIGEgZG9jdW1lbnQgZnJhZ21lbnQgcmVwcmVzZW50aW5nIHRoZSByZXNwb25zZSBIVE1MLCBpbmNsdWRpbmdcbiAgICogYSBgdGl0bGVgIHByb3BlcnR5IGZvciBhbnkgdGl0bGUgaW5mb3JtYXRpb24gZm91bmRcbiAgICovXG5cbiAgLyoqXG4gICAqIEBwYXJhbSB7c3RyaW5nfSByZXNwb25zZSBIVE1MXG4gICAqIEByZXR1cm5zIHtEb2N1bWVudEZyYWdtZW50V2l0aFRpdGxlfVxuICAgKi9cbiAgZnVuY3Rpb24gbWFrZUZyYWdtZW50KHJlc3BvbnNlKSB7XG4gICAgLy8gc3RyaXAgaGVhZCB0YWcgdG8gZGV0ZXJtaW5lIHNoYXBlIG9mIHJlc3BvbnNlIHdlIGFyZSBkZWFsaW5nIHdpdGhcbiAgICBjb25zdCByZXNwb25zZVdpdGhOb0hlYWQgPSByZXNwb25zZS5yZXBsYWNlKC88aGVhZChcXHNbXj5dKik/PltcXHNcXFNdKj88XFwvaGVhZD4vaSwgJycpXG4gICAgY29uc3Qgc3RhcnRUYWcgPSBnZXRTdGFydFRhZyhyZXNwb25zZVdpdGhOb0hlYWQpXG4gICAgLyoqIEB0eXBlIERvY3VtZW50RnJhZ21lbnRXaXRoVGl0bGUgKi9cbiAgICBsZXQgZnJhZ21lbnRcbiAgICBpZiAoc3RhcnRUYWcgPT09ICdodG1sJykge1xuICAgICAgLy8gaWYgaXQgaXMgYSBmdWxsIGRvY3VtZW50LCBwYXJzZSBpdCBhbmQgcmV0dXJuIHRoZSBib2R5XG4gICAgICBmcmFnbWVudCA9IC8qKiBAdHlwZSBEb2N1bWVudEZyYWdtZW50V2l0aFRpdGxlICovIChuZXcgRG9jdW1lbnRGcmFnbWVudCgpKVxuICAgICAgY29uc3QgZG9jID0gcGFyc2VIVE1MKHJlc3BvbnNlKVxuICAgICAgdGFrZUNoaWxkcmVuRm9yKGZyYWdtZW50LCBkb2MuYm9keSlcbiAgICAgIGZyYWdtZW50LnRpdGxlID0gZG9jLnRpdGxlXG4gICAgfSBlbHNlIGlmIChzdGFydFRhZyA9PT0gJ2JvZHknKSB7XG4gICAgICAvLyBwYXJzZSBib2R5IHcvbyB3cmFwcGluZyBpbiB0ZW1wbGF0ZVxuICAgICAgZnJhZ21lbnQgPSAvKiogQHR5cGUgRG9jdW1lbnRGcmFnbWVudFdpdGhUaXRsZSAqLyAobmV3IERvY3VtZW50RnJhZ21lbnQoKSlcbiAgICAgIGNvbnN0IGRvYyA9IHBhcnNlSFRNTChyZXNwb25zZVdpdGhOb0hlYWQpXG4gICAgICB0YWtlQ2hpbGRyZW5Gb3IoZnJhZ21lbnQsIGRvYy5ib2R5KVxuICAgICAgZnJhZ21lbnQudGl0bGUgPSBkb2MudGl0bGVcbiAgICB9IGVsc2Uge1xuICAgICAgLy8gb3RoZXJ3aXNlIHdlIGhhdmUgbm9uLWJvZHkgcGFydGlhbCBIVE1MIGNvbnRlbnQsIHNvIHdyYXAgaXQgaW4gYSB0ZW1wbGF0ZSB0byBtYXhpbWl6ZSBwYXJzaW5nIGZsZXhpYmlsaXR5XG4gICAgICBjb25zdCBkb2MgPSBwYXJzZUhUTUwoJzxib2R5Pjx0ZW1wbGF0ZSBjbGFzcz1cImludGVybmFsLWh0bXgtd3JhcHBlclwiPicgKyByZXNwb25zZVdpdGhOb0hlYWQgKyAnPC90ZW1wbGF0ZT48L2JvZHk+JylcbiAgICAgIGZyYWdtZW50ID0gLyoqIEB0eXBlIERvY3VtZW50RnJhZ21lbnRXaXRoVGl0bGUgKi8gKGRvYy5xdWVyeVNlbGVjdG9yKCd0ZW1wbGF0ZScpLmNvbnRlbnQpXG4gICAgICAvLyBleHRyYWN0IHRpdGxlIGludG8gZnJhZ21lbnQgZm9yIGxhdGVyIHByb2Nlc3NpbmdcbiAgICAgIGZyYWdtZW50LnRpdGxlID0gZG9jLnRpdGxlXG5cbiAgICAgIC8vIGZvciBsZWdhY3kgcmVhc29ucyB3ZSBzdXBwb3J0IGEgdGl0bGUgdGFnIGF0IHRoZSByb290IGxldmVsIG9mIG5vbi1ib2R5IHJlc3BvbnNlcywgc28gd2UgbmVlZCB0byBoYW5kbGUgaXRcbiAgICAgIHZhciB0aXRsZUVsZW1lbnQgPSBmcmFnbWVudC5xdWVyeVNlbGVjdG9yKCd0aXRsZScpXG4gICAgICBpZiAodGl0bGVFbGVtZW50ICYmIHRpdGxlRWxlbWVudC5wYXJlbnROb2RlID09PSBmcmFnbWVudCkge1xuICAgICAgICB0aXRsZUVsZW1lbnQucmVtb3ZlKClcbiAgICAgICAgZnJhZ21lbnQudGl0bGUgPSB0aXRsZUVsZW1lbnQuaW5uZXJUZXh0XG4gICAgICB9XG4gICAgfVxuICAgIGlmIChmcmFnbWVudCkge1xuICAgICAgaWYgKGh0bXguY29uZmlnLmFsbG93U2NyaXB0VGFncykge1xuICAgICAgICBub3JtYWxpemVTY3JpcHRUYWdzKGZyYWdtZW50KVxuICAgICAgfSBlbHNlIHtcbiAgICAgICAgLy8gcmVtb3ZlIGFsbCBzY3JpcHQgdGFncyBpZiBzY3JpcHRzIGFyZSBkaXNhYmxlZFxuICAgICAgICBmcmFnbWVudC5xdWVyeVNlbGVjdG9yQWxsKCdzY3JpcHQnKS5mb3JFYWNoKChzY3JpcHQpID0+IHNjcmlwdC5yZW1vdmUoKSlcbiAgICAgIH1cbiAgICB9XG4gICAgcmV0dXJuIGZyYWdtZW50XG4gIH1cblxuICAvKipcbiAgICogQHBhcmFtIHtGdW5jdGlvbn0gZnVuY1xuICAgKi9cbiAgZnVuY3Rpb24gbWF5YmVDYWxsKGZ1bmMpIHtcbiAgICBpZiAoZnVuYykge1xuICAgICAgZnVuYygpXG4gICAgfVxuICB9XG5cbiAgLyoqXG4gICAqIEBwYXJhbSB7YW55fSBvXG4gICAqIEBwYXJhbSB7c3RyaW5nfSB0eXBlXG4gICAqIEByZXR1cm5zXG4gICAqL1xuICBmdW5jdGlvbiBpc1R5cGUobywgdHlwZSkge1xuICAgIHJldHVybiBPYmplY3QucHJvdG90eXBlLnRvU3RyaW5nLmNhbGwobykgPT09ICdbb2JqZWN0ICcgKyB0eXBlICsgJ10nXG4gIH1cblxuICAvKipcbiAgICogQHBhcmFtIHsqfSBvXG4gICAqIEByZXR1cm5zIHtvIGlzIEZ1bmN0aW9ufVxuICAgKi9cbiAgZnVuY3Rpb24gaXNGdW5jdGlvbihvKSB7XG4gICAgcmV0dXJuIHR5cGVvZiBvID09PSAnZnVuY3Rpb24nXG4gIH1cblxuICAvKipcbiAgICogQHBhcmFtIHsqfSBvXG4gICAqIEByZXR1cm5zIHtvIGlzIE9iamVjdH1cbiAgICovXG4gIGZ1bmN0aW9uIGlzUmF3T2JqZWN0KG8pIHtcbiAgICByZXR1cm4gaXNUeXBlKG8sICdPYmplY3QnKVxuICB9XG5cbiAgLyoqXG4gICAqIEB0eXBlZGVmIHtPYmplY3R9IE9uSGFuZGxlclxuICAgKiBAcHJvcGVydHkgeyhrZXlvZiBIVE1MRWxlbWVudEV2ZW50TWFwKXxzdHJpbmd9IGV2ZW50XG4gICAqIEBwcm9wZXJ0eSB7RXZlbnRMaXN0ZW5lcn0gbGlzdGVuZXJcbiAgICovXG5cbiAgLyoqXG4gICAqIEB0eXBlZGVmIHtPYmplY3R9IExpc3RlbmVySW5mb1xuICAgKiBAcHJvcGVydHkge3N0cmluZ30gdHJpZ2dlclxuICAgKiBAcHJvcGVydHkge0V2ZW50TGlzdGVuZXJ9IGxpc3RlbmVyXG4gICAqIEBwcm9wZXJ0eSB7RXZlbnRUYXJnZXR9IG9uXG4gICAqL1xuXG4gIC8qKlxuICAgKiBAdHlwZWRlZiB7T2JqZWN0fSBIdG14Tm9kZUludGVybmFsRGF0YVxuICAgKiBFbGVtZW50IGRhdGFcbiAgICogQHByb3BlcnR5IHtudW1iZXJ9IFtpbml0SGFzaF1cbiAgICogQHByb3BlcnR5IHtib29sZWFufSBbYm9vc3RlZF1cbiAgICogQHByb3BlcnR5IHtPbkhhbmRsZXJbXX0gW29uSGFuZGxlcnNdXG4gICAqIEBwcm9wZXJ0eSB7bnVtYmVyfSBbdGltZW91dF1cbiAgICogQHByb3BlcnR5IHtMaXN0ZW5lckluZm9bXX0gW2xpc3RlbmVySW5mb3NdXG4gICAqIEBwcm9wZXJ0eSB7Ym9vbGVhbn0gW2NhbmNlbGxlZF1cbiAgICogQHByb3BlcnR5IHtib29sZWFufSBbdHJpZ2dlcmVkT25jZV1cbiAgICogQHByb3BlcnR5IHtudW1iZXJ9IFtkZWxheWVkXVxuICAgKiBAcHJvcGVydHkge251bWJlcnxudWxsfSBbdGhyb3R0bGVdXG4gICAqIEBwcm9wZXJ0eSB7V2Vha01hcDxIdG14VHJpZ2dlclNwZWNpZmljYXRpb24sV2Vha01hcDxFdmVudFRhcmdldCxzdHJpbmc+Pn0gW2xhc3RWYWx1ZV1cbiAgICogQHByb3BlcnR5IHtib29sZWFufSBbbG9hZGVkXVxuICAgKiBAcHJvcGVydHkge3N0cmluZ30gW3BhdGhdXG4gICAqIEBwcm9wZXJ0eSB7c3RyaW5nfSBbdmVyYl1cbiAgICogQHByb3BlcnR5IHtib29sZWFufSBbcG9sbGluZ11cbiAgICogQHByb3BlcnR5IHtIVE1MQnV0dG9uRWxlbWVudHxIVE1MSW5wdXRFbGVtZW50fG51bGx9IFtsYXN0QnV0dG9uQ2xpY2tlZF1cbiAgICogQHByb3BlcnR5IHtudW1iZXJ9IFtyZXF1ZXN0Q291bnRdXG4gICAqIEBwcm9wZXJ0eSB7WE1MSHR0cFJlcXVlc3R9IFt4aHJdXG4gICAqIEBwcm9wZXJ0eSB7KCgpID0+IHZvaWQpW119IFtxdWV1ZWRSZXF1ZXN0c11cbiAgICogQHByb3BlcnR5IHtib29sZWFufSBbYWJvcnRhYmxlXVxuICAgKiBAcHJvcGVydHkge2Jvb2xlYW59IFtmaXJzdEluaXRDb21wbGV0ZWRdXG4gICAqXG4gICAqIEV2ZW50IGRhdGFcbiAgICogQHByb3BlcnR5IHtIdG14VHJpZ2dlclNwZWNpZmljYXRpb259IFt0cmlnZ2VyU3BlY11cbiAgICogQHByb3BlcnR5IHtFdmVudFRhcmdldFtdfSBbaGFuZGxlZEZvcl1cbiAgICovXG5cbiAgLyoqXG4gICAqIGdldEludGVybmFsRGF0YSByZXRyaWV2ZXMgXCJwcml2YXRlXCIgZGF0YSBzdG9yZWQgYnkgaHRteCB3aXRoaW4gYW4gZWxlbWVudFxuICAgKiBAcGFyYW0ge0V2ZW50VGFyZ2V0fEV2ZW50fSBlbHRcbiAgICogQHJldHVybnMge0h0bXhOb2RlSW50ZXJuYWxEYXRhfVxuICAgKi9cbiAgZnVuY3Rpb24gZ2V0SW50ZXJuYWxEYXRhKGVsdCkge1xuICAgIGNvbnN0IGRhdGFQcm9wID0gJ2h0bXgtaW50ZXJuYWwtZGF0YSdcbiAgICBsZXQgZGF0YSA9IGVsdFtkYXRhUHJvcF1cbiAgICBpZiAoIWRhdGEpIHtcbiAgICAgIGRhdGEgPSBlbHRbZGF0YVByb3BdID0ge31cbiAgICB9XG4gICAgcmV0dXJuIGRhdGFcbiAgfVxuXG4gIC8qKlxuICAgKiB0b0FycmF5IGNvbnZlcnRzIGFuIEFycmF5TGlrZSBvYmplY3QgaW50byBhIHJlYWwgYXJyYXkuXG4gICAqIEB0ZW1wbGF0ZSBUXG4gICAqIEBwYXJhbSB7QXJyYXlMaWtlPFQ+fSBhcnJcbiAgICogQHJldHVybnMge1RbXX1cbiAgICovXG4gIGZ1bmN0aW9uIHRvQXJyYXkoYXJyKSB7XG4gICAgY29uc3QgcmV0dXJuQXJyID0gW11cbiAgICBpZiAoYXJyKSB7XG4gICAgICBmb3IgKGxldCBpID0gMDsgaSA8IGFyci5sZW5ndGg7IGkrKykge1xuICAgICAgICByZXR1cm5BcnIucHVzaChhcnJbaV0pXG4gICAgICB9XG4gICAgfVxuICAgIHJldHVybiByZXR1cm5BcnJcbiAgfVxuXG4gIC8qKlxuICAgKiBAdGVtcGxhdGUgVFxuICAgKiBAcGFyYW0ge1RbXXxOYW1lZE5vZGVNYXB8SFRNTENvbGxlY3Rpb258SFRNTEZvcm1Db250cm9sc0NvbGxlY3Rpb258QXJyYXlMaWtlPFQ+fSBhcnJcbiAgICogQHBhcmFtIHsoVCkgPT4gdm9pZH0gZnVuY1xuICAgKi9cbiAgZnVuY3Rpb24gZm9yRWFjaChhcnIsIGZ1bmMpIHtcbiAgICBpZiAoYXJyKSB7XG4gICAgICBmb3IgKGxldCBpID0gMDsgaSA8IGFyci5sZW5ndGg7IGkrKykge1xuICAgICAgICBmdW5jKGFycltpXSlcbiAgICAgIH1cbiAgICB9XG4gIH1cblxuICAvKipcbiAgICogQHBhcmFtIHtFbGVtZW50fSBlbFxuICAgKiBAcmV0dXJucyB7Ym9vbGVhbn1cbiAgICovXG4gIGZ1bmN0aW9uIGlzU2Nyb2xsZWRJbnRvVmlldyhlbCkge1xuICAgIGNvbnN0IHJlY3QgPSBlbC5nZXRCb3VuZGluZ0NsaWVudFJlY3QoKVxuICAgIGNvbnN0IGVsZW1Ub3AgPSByZWN0LnRvcFxuICAgIGNvbnN0IGVsZW1Cb3R0b20gPSByZWN0LmJvdHRvbVxuICAgIHJldHVybiBlbGVtVG9wIDwgd2luZG93LmlubmVySGVpZ2h0ICYmIGVsZW1Cb3R0b20gPj0gMFxuICB9XG5cbiAgLyoqXG4gICAqIENoZWNrcyB3aGV0aGVyIHRoZSBlbGVtZW50IGlzIGluIHRoZSBkb2N1bWVudCAoaW5jbHVkZXMgc2hhZG93IHJvb3RzKS5cbiAgICogVGhpcyBmdW5jdGlvbiB0aGlzIGlzIGEgc2xpZ2h0IG1pc25vbWVyOyBpdCB3aWxsIHJldHVybiB0cnVlIGV2ZW4gZm9yIGVsZW1lbnRzIGluIHRoZSBoZWFkLlxuICAgKlxuICAgKiBAcGFyYW0ge05vZGV9IGVsdFxuICAgKiBAcmV0dXJucyB7Ym9vbGVhbn1cbiAgICovXG4gIGZ1bmN0aW9uIGJvZHlDb250YWlucyhlbHQpIHtcbiAgICByZXR1cm4gZWx0LmdldFJvb3ROb2RlKHsgY29tcG9zZWQ6IHRydWUgfSkgPT09IGRvY3VtZW50XG4gIH1cblxuICAvKipcbiAgICogQHBhcmFtIHtzdHJpbmd9IHRyaWdnZXJcbiAgICogQHJldHVybnMge3N0cmluZ1tdfVxuICAgKi9cbiAgZnVuY3Rpb24gc3BsaXRPbldoaXRlc3BhY2UodHJpZ2dlcikge1xuICAgIHJldHVybiB0cmlnZ2VyLnRyaW0oKS5zcGxpdCgvXFxzKy8pXG4gIH1cblxuICAvKipcbiAgICogbWVyZ2VPYmplY3RzIHRha2VzIGFsbCB0aGUga2V5cyBmcm9tXG4gICAqIG9iajIgYW5kIGR1cGxpY2F0ZXMgdGhlbSBpbnRvIG9iajFcbiAgICogQHRlbXBsYXRlIFQxXG4gICAqIEB0ZW1wbGF0ZSBUMlxuICAgKiBAcGFyYW0ge1QxfSBvYmoxXG4gICAqIEBwYXJhbSB7VDJ9IG9iajJcbiAgICogQHJldHVybnMge1QxICYgVDJ9XG4gICAqL1xuICBmdW5jdGlvbiBtZXJnZU9iamVjdHMob2JqMSwgb2JqMikge1xuICAgIGZvciAoY29uc3Qga2V5IGluIG9iajIpIHtcbiAgICAgIGlmIChvYmoyLmhhc093blByb3BlcnR5KGtleSkpIHtcbiAgICAgICAgLy8gQHRzLWlnbm9yZSB0c2MgZG9lc24ndCBzZWVtIHRvIHByb3Blcmx5IGhhbmRsZSB0eXBlcyBtZXJnaW5nXG4gICAgICAgIG9iajFba2V5XSA9IG9iajJba2V5XVxuICAgICAgfVxuICAgIH1cbiAgICAvLyBAdHMtaWdub3JlIHRzYyBkb2Vzbid0IHNlZW0gdG8gcHJvcGVybHkgaGFuZGxlIHR5cGVzIG1lcmdpbmdcbiAgICByZXR1cm4gb2JqMVxuICB9XG5cbiAgLyoqXG4gICAqIEBwYXJhbSB7c3RyaW5nfSBqU3RyaW5nXG4gICAqIEByZXR1cm5zIHthbnl8bnVsbH1cbiAgICovXG4gIGZ1bmN0aW9uIHBhcnNlSlNPTihqU3RyaW5nKSB7XG4gICAgdHJ5IHtcbiAgICAgIHJldHVybiBKU09OLnBhcnNlKGpTdHJpbmcpXG4gICAgfSBjYXRjaCAoZXJyb3IpIHtcbiAgICAgIGxvZ0Vycm9yKGVycm9yKVxuICAgICAgcmV0dXJuIG51bGxcbiAgICB9XG4gIH1cblxuICAvKipcbiAgICogQHJldHVybnMge2Jvb2xlYW59XG4gICAqL1xuICBmdW5jdGlvbiBjYW5BY2Nlc3NMb2NhbFN0b3JhZ2UoKSB7XG4gICAgY29uc3QgdGVzdCA9ICdodG14OmxvY2FsU3RvcmFnZVRlc3QnXG4gICAgdHJ5IHtcbiAgICAgIGxvY2FsU3RvcmFnZS5zZXRJdGVtKHRlc3QsIHRlc3QpXG4gICAgICBsb2NhbFN0b3JhZ2UucmVtb3ZlSXRlbSh0ZXN0KVxuICAgICAgcmV0dXJuIHRydWVcbiAgICB9IGNhdGNoIChlKSB7XG4gICAgICByZXR1cm4gZmFsc2VcbiAgICB9XG4gIH1cblxuICAvKipcbiAgICogQHBhcmFtIHtzdHJpbmd9IHBhdGhcbiAgICogQHJldHVybnMge3N0cmluZ31cbiAgICovXG4gIGZ1bmN0aW9uIG5vcm1hbGl6ZVBhdGgocGF0aCkge1xuICAgIHRyeSB7XG4gICAgICBjb25zdCB1cmwgPSBuZXcgVVJMKHBhdGgpXG4gICAgICBpZiAodXJsKSB7XG4gICAgICAgIHBhdGggPSB1cmwucGF0aG5hbWUgKyB1cmwuc2VhcmNoXG4gICAgICB9XG4gICAgICAvLyByZW1vdmUgdHJhaWxpbmcgc2xhc2gsIHVubGVzcyBpbmRleCBwYWdlXG4gICAgICBpZiAoISgvXlxcLyQvLnRlc3QocGF0aCkpKSB7XG4gICAgICAgIHBhdGggPSBwYXRoLnJlcGxhY2UoL1xcLyskLywgJycpXG4gICAgICB9XG4gICAgICByZXR1cm4gcGF0aFxuICAgIH0gY2F0Y2ggKGUpIHtcbiAgICAgIC8vIGJlIGtpbmQgdG8gSUUxMSwgd2hpY2ggZG9lc24ndCBzdXBwb3J0IFVSTCgpXG4gICAgICByZXR1cm4gcGF0aFxuICAgIH1cbiAgfVxuXG4gIC8vPSA9PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PVxuICAvLyBwdWJsaWMgQVBJXG4gIC8vPSA9PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PVxuXG4gIC8qKlxuICAgKiBAcGFyYW0ge3N0cmluZ30gc3RyXG4gICAqIEByZXR1cm5zIHthbnl9XG4gICAqL1xuICBmdW5jdGlvbiBpbnRlcm5hbEV2YWwoc3RyKSB7XG4gICAgcmV0dXJuIG1heWJlRXZhbChnZXREb2N1bWVudCgpLmJvZHksIGZ1bmN0aW9uKCkge1xuICAgICAgcmV0dXJuIGV2YWwoc3RyKVxuICAgIH0pXG4gIH1cblxuICAvKipcbiAgICogQWRkcyBhIGNhbGxiYWNrIGZvciB0aGUgKipodG14OmxvYWQqKiBldmVudC4gVGhpcyBjYW4gYmUgdXNlZCB0byBwcm9jZXNzIG5ldyBjb250ZW50LCBmb3IgZXhhbXBsZSBpbml0aWFsaXppbmcgdGhlIGNvbnRlbnQgd2l0aCBhIGphdmFzY3JpcHQgbGlicmFyeVxuICAgKlxuICAgKiBAc2VlIGh0dHBzOi8vaHRteC5vcmcvYXBpLyNvbkxvYWRcbiAgICpcbiAgICogQHBhcmFtIHsoZWx0OiBOb2RlKSA9PiB2b2lkfSBjYWxsYmFjayB0aGUgY2FsbGJhY2sgdG8gY2FsbCBvbiBuZXdseSBsb2FkZWQgY29udGVudFxuICAgKiBAcmV0dXJucyB7RXZlbnRMaXN0ZW5lcn1cbiAgICovXG4gIGZ1bmN0aW9uIG9uTG9hZEhlbHBlcihjYWxsYmFjaykge1xuICAgIGNvbnN0IHZhbHVlID0gaHRteC5vbignaHRteDpsb2FkJywgLyoqIEBwYXJhbSB7Q3VzdG9tRXZlbnR9IGV2dCAqLyBmdW5jdGlvbihldnQpIHtcbiAgICAgIGNhbGxiYWNrKGV2dC5kZXRhaWwuZWx0KVxuICAgIH0pXG4gICAgcmV0dXJuIHZhbHVlXG4gIH1cblxuICAvKipcbiAgICogTG9nIGFsbCBodG14IGV2ZW50cywgdXNlZnVsIGZvciBkZWJ1Z2dpbmcuXG4gICAqXG4gICAqIEBzZWUgaHR0cHM6Ly9odG14Lm9yZy9hcGkvI2xvZ0FsbFxuICAgKi9cbiAgZnVuY3Rpb24gbG9nQWxsKCkge1xuICAgIGh0bXgubG9nZ2VyID0gZnVuY3Rpb24oZWx0LCBldmVudCwgZGF0YSkge1xuICAgICAgaWYgKGNvbnNvbGUpIHtcbiAgICAgICAgY29uc29sZS5sb2coZXZlbnQsIGVsdCwgZGF0YSlcbiAgICAgIH1cbiAgICB9XG4gIH1cblxuICBmdW5jdGlvbiBsb2dOb25lKCkge1xuICAgIGh0bXgubG9nZ2VyID0gbnVsbFxuICB9XG5cbiAgLyoqXG4gICAqIEZpbmRzIGFuIGVsZW1lbnQgbWF0Y2hpbmcgdGhlIHNlbGVjdG9yXG4gICAqXG4gICAqIEBzZWUgaHR0cHM6Ly9odG14Lm9yZy9hcGkvI2ZpbmRcbiAgICpcbiAgICogQHBhcmFtIHtQYXJlbnROb2RlfHN0cmluZ30gZWx0T3JTZWxlY3RvciAgdGhlIHJvb3QgZWxlbWVudCB0byBmaW5kIHRoZSBtYXRjaGluZyBlbGVtZW50IGluLCBpbmNsdXNpdmUgfCB0aGUgc2VsZWN0b3IgdG8gbWF0Y2hcbiAgICogQHBhcmFtIHtzdHJpbmd9IFtzZWxlY3Rvcl0gdGhlIHNlbGVjdG9yIHRvIG1hdGNoXG4gICAqIEByZXR1cm5zIHtFbGVtZW50fG51bGx9XG4gICAqL1xuICBmdW5jdGlvbiBmaW5kKGVsdE9yU2VsZWN0b3IsIHNlbGVjdG9yKSB7XG4gICAgaWYgKHR5cGVvZiBlbHRPclNlbGVjdG9yICE9PSAnc3RyaW5nJykge1xuICAgICAgcmV0dXJuIGVsdE9yU2VsZWN0b3IucXVlcnlTZWxlY3RvcihzZWxlY3RvcilcbiAgICB9IGVsc2Uge1xuICAgICAgcmV0dXJuIGZpbmQoZ2V0RG9jdW1lbnQoKSwgZWx0T3JTZWxlY3RvcilcbiAgICB9XG4gIH1cblxuICAvKipcbiAgICogRmluZHMgYWxsIGVsZW1lbnRzIG1hdGNoaW5nIHRoZSBzZWxlY3RvclxuICAgKlxuICAgKiBAc2VlIGh0dHBzOi8vaHRteC5vcmcvYXBpLyNmaW5kQWxsXG4gICAqXG4gICAqIEBwYXJhbSB7UGFyZW50Tm9kZXxzdHJpbmd9IGVsdE9yU2VsZWN0b3IgdGhlIHJvb3QgZWxlbWVudCB0byBmaW5kIHRoZSBtYXRjaGluZyBlbGVtZW50cyBpbiwgaW5jbHVzaXZlIHwgdGhlIHNlbGVjdG9yIHRvIG1hdGNoXG4gICAqIEBwYXJhbSB7c3RyaW5nfSBbc2VsZWN0b3JdIHRoZSBzZWxlY3RvciB0byBtYXRjaFxuICAgKiBAcmV0dXJucyB7Tm9kZUxpc3RPZjxFbGVtZW50Pn1cbiAgICovXG4gIGZ1bmN0aW9uIGZpbmRBbGwoZWx0T3JTZWxlY3Rvciwgc2VsZWN0b3IpIHtcbiAgICBpZiAodHlwZW9mIGVsdE9yU2VsZWN0b3IgIT09ICdzdHJpbmcnKSB7XG4gICAgICByZXR1cm4gZWx0T3JTZWxlY3Rvci5xdWVyeVNlbGVjdG9yQWxsKHNlbGVjdG9yKVxuICAgIH0gZWxzZSB7XG4gICAgICByZXR1cm4gZmluZEFsbChnZXREb2N1bWVudCgpLCBlbHRPclNlbGVjdG9yKVxuICAgIH1cbiAgfVxuXG4gIC8qKlxuICAgKiBAcmV0dXJucyBXaW5kb3dcbiAgICovXG4gIGZ1bmN0aW9uIGdldFdpbmRvdygpIHtcbiAgICByZXR1cm4gd2luZG93XG4gIH1cblxuICAvKipcbiAgICogUmVtb3ZlcyBhbiBlbGVtZW50IGZyb20gdGhlIERPTVxuICAgKlxuICAgKiBAc2VlIGh0dHBzOi8vaHRteC5vcmcvYXBpLyNyZW1vdmVcbiAgICpcbiAgICogQHBhcmFtIHtOb2RlfSBlbHRcbiAgICogQHBhcmFtIHtudW1iZXJ9IFtkZWxheV1cbiAgICovXG4gIGZ1bmN0aW9uIHJlbW92ZUVsZW1lbnQoZWx0LCBkZWxheSkge1xuICAgIGVsdCA9IHJlc29sdmVUYXJnZXQoZWx0KVxuICAgIGlmIChkZWxheSkge1xuICAgICAgZ2V0V2luZG93KCkuc2V0VGltZW91dChmdW5jdGlvbigpIHtcbiAgICAgICAgcmVtb3ZlRWxlbWVudChlbHQpXG4gICAgICAgIGVsdCA9IG51bGxcbiAgICAgIH0sIGRlbGF5KVxuICAgIH0gZWxzZSB7XG4gICAgICBwYXJlbnRFbHQoZWx0KS5yZW1vdmVDaGlsZChlbHQpXG4gICAgfVxuICB9XG5cbiAgLyoqXG4gICAqIEBwYXJhbSB7YW55fSBlbHRcbiAgICogQHJldHVybiB7RWxlbWVudHxudWxsfVxuICAgKi9cbiAgZnVuY3Rpb24gYXNFbGVtZW50KGVsdCkge1xuICAgIHJldHVybiBlbHQgaW5zdGFuY2VvZiBFbGVtZW50ID8gZWx0IDogbnVsbFxuICB9XG5cbiAgLyoqXG4gICAqIEBwYXJhbSB7YW55fSBlbHRcbiAgICogQHJldHVybiB7SFRNTEVsZW1lbnR8bnVsbH1cbiAgICovXG4gIGZ1bmN0aW9uIGFzSHRtbEVsZW1lbnQoZWx0KSB7XG4gICAgcmV0dXJuIGVsdCBpbnN0YW5jZW9mIEhUTUxFbGVtZW50ID8gZWx0IDogbnVsbFxuICB9XG5cbiAgLyoqXG4gICAqIEBwYXJhbSB7YW55fSB2YWx1ZVxuICAgKiBAcmV0dXJuIHtzdHJpbmd8bnVsbH1cbiAgICovXG4gIGZ1bmN0aW9uIGFzU3RyaW5nKHZhbHVlKSB7XG4gICAgcmV0dXJuIHR5cGVvZiB2YWx1ZSA9PT0gJ3N0cmluZycgPyB2YWx1ZSA6IG51bGxcbiAgfVxuXG4gIC8qKlxuICAgKiBAcGFyYW0ge0V2ZW50VGFyZ2V0fSBlbHRcbiAgICogQHJldHVybiB7UGFyZW50Tm9kZXxudWxsfVxuICAgKi9cbiAgZnVuY3Rpb24gYXNQYXJlbnROb2RlKGVsdCkge1xuICAgIHJldHVybiBlbHQgaW5zdGFuY2VvZiBFbGVtZW50IHx8IGVsdCBpbnN0YW5jZW9mIERvY3VtZW50IHx8IGVsdCBpbnN0YW5jZW9mIERvY3VtZW50RnJhZ21lbnQgPyBlbHQgOiBudWxsXG4gIH1cblxuICAvKipcbiAgICogVGhpcyBtZXRob2QgYWRkcyBhIGNsYXNzIHRvIHRoZSBnaXZlbiBlbGVtZW50LlxuICAgKlxuICAgKiBAc2VlIGh0dHBzOi8vaHRteC5vcmcvYXBpLyNhZGRDbGFzc1xuICAgKlxuICAgKiBAcGFyYW0ge0VsZW1lbnR8c3RyaW5nfSBlbHQgdGhlIGVsZW1lbnQgdG8gYWRkIHRoZSBjbGFzcyB0b1xuICAgKiBAcGFyYW0ge3N0cmluZ30gY2xhenogdGhlIGNsYXNzIHRvIGFkZFxuICAgKiBAcGFyYW0ge251bWJlcn0gW2RlbGF5XSB0aGUgZGVsYXkgKGluIG1pbGxpc2Vjb25kcykgYmVmb3JlIGNsYXNzIGlzIGFkZGVkXG4gICAqL1xuICBmdW5jdGlvbiBhZGRDbGFzc1RvRWxlbWVudChlbHQsIGNsYXp6LCBkZWxheSkge1xuICAgIGVsdCA9IGFzRWxlbWVudChyZXNvbHZlVGFyZ2V0KGVsdCkpXG4gICAgaWYgKCFlbHQpIHtcbiAgICAgIHJldHVyblxuICAgIH1cbiAgICBpZiAoZGVsYXkpIHtcbiAgICAgIGdldFdpbmRvdygpLnNldFRpbWVvdXQoZnVuY3Rpb24oKSB7XG4gICAgICAgIGFkZENsYXNzVG9FbGVtZW50KGVsdCwgY2xhenopXG4gICAgICAgIGVsdCA9IG51bGxcbiAgICAgIH0sIGRlbGF5KVxuICAgIH0gZWxzZSB7XG4gICAgICBlbHQuY2xhc3NMaXN0ICYmIGVsdC5jbGFzc0xpc3QuYWRkKGNsYXp6KVxuICAgIH1cbiAgfVxuXG4gIC8qKlxuICAgKiBSZW1vdmVzIGEgY2xhc3MgZnJvbSB0aGUgZ2l2ZW4gZWxlbWVudFxuICAgKlxuICAgKiBAc2VlIGh0dHBzOi8vaHRteC5vcmcvYXBpLyNyZW1vdmVDbGFzc1xuICAgKlxuICAgKiBAcGFyYW0ge05vZGV8c3RyaW5nfSBub2RlIGVsZW1lbnQgdG8gcmVtb3ZlIHRoZSBjbGFzcyBmcm9tXG4gICAqIEBwYXJhbSB7c3RyaW5nfSBjbGF6eiB0aGUgY2xhc3MgdG8gcmVtb3ZlXG4gICAqIEBwYXJhbSB7bnVtYmVyfSBbZGVsYXldIHRoZSBkZWxheSAoaW4gbWlsbGlzZWNvbmRzIGJlZm9yZSBjbGFzcyBpcyByZW1vdmVkKVxuICAgKi9cbiAgZnVuY3Rpb24gcmVtb3ZlQ2xhc3NGcm9tRWxlbWVudChub2RlLCBjbGF6eiwgZGVsYXkpIHtcbiAgICBsZXQgZWx0ID0gYXNFbGVtZW50KHJlc29sdmVUYXJnZXQobm9kZSkpXG4gICAgaWYgKCFlbHQpIHtcbiAgICAgIHJldHVyblxuICAgIH1cbiAgICBpZiAoZGVsYXkpIHtcbiAgICAgIGdldFdpbmRvdygpLnNldFRpbWVvdXQoZnVuY3Rpb24oKSB7XG4gICAgICAgIHJlbW92ZUNsYXNzRnJvbUVsZW1lbnQoZWx0LCBjbGF6eilcbiAgICAgICAgZWx0ID0gbnVsbFxuICAgICAgfSwgZGVsYXkpXG4gICAgfSBlbHNlIHtcbiAgICAgIGlmIChlbHQuY2xhc3NMaXN0KSB7XG4gICAgICAgIGVsdC5jbGFzc0xpc3QucmVtb3ZlKGNsYXp6KVxuICAgICAgICAvLyBpZiB0aGVyZSBhcmUgbm8gY2xhc3NlcyBsZWZ0LCByZW1vdmUgdGhlIGNsYXNzIGF0dHJpYnV0ZVxuICAgICAgICBpZiAoZWx0LmNsYXNzTGlzdC5sZW5ndGggPT09IDApIHtcbiAgICAgICAgICBlbHQucmVtb3ZlQXR0cmlidXRlKCdjbGFzcycpXG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9XG4gIH1cblxuICAvKipcbiAgICogVG9nZ2xlcyB0aGUgZ2l2ZW4gY2xhc3Mgb24gYW4gZWxlbWVudFxuICAgKlxuICAgKiBAc2VlIGh0dHBzOi8vaHRteC5vcmcvYXBpLyN0b2dnbGVDbGFzc1xuICAgKlxuICAgKiBAcGFyYW0ge0VsZW1lbnR8c3RyaW5nfSBlbHQgdGhlIGVsZW1lbnQgdG8gdG9nZ2xlIHRoZSBjbGFzcyBvblxuICAgKiBAcGFyYW0ge3N0cmluZ30gY2xhenogdGhlIGNsYXNzIHRvIHRvZ2dsZVxuICAgKi9cbiAgZnVuY3Rpb24gdG9nZ2xlQ2xhc3NPbkVsZW1lbnQoZWx0LCBjbGF6eikge1xuICAgIGVsdCA9IHJlc29sdmVUYXJnZXQoZWx0KVxuICAgIGVsdC5jbGFzc0xpc3QudG9nZ2xlKGNsYXp6KVxuICB9XG5cbiAgLyoqXG4gICAqIFRha2VzIHRoZSBnaXZlbiBjbGFzcyBmcm9tIGl0cyBzaWJsaW5ncywgc28gdGhhdCBhbW9uZyBpdHMgc2libGluZ3MsIG9ubHkgdGhlIGdpdmVuIGVsZW1lbnQgd2lsbCBoYXZlIHRoZSBjbGFzcy5cbiAgICpcbiAgICogQHNlZSBodHRwczovL2h0bXgub3JnL2FwaS8jdGFrZUNsYXNzXG4gICAqXG4gICAqIEBwYXJhbSB7Tm9kZXxzdHJpbmd9IGVsdCB0aGUgZWxlbWVudCB0aGF0IHdpbGwgdGFrZSB0aGUgY2xhc3NcbiAgICogQHBhcmFtIHtzdHJpbmd9IGNsYXp6IHRoZSBjbGFzcyB0byB0YWtlXG4gICAqL1xuICBmdW5jdGlvbiB0YWtlQ2xhc3NGb3JFbGVtZW50KGVsdCwgY2xhenopIHtcbiAgICBlbHQgPSByZXNvbHZlVGFyZ2V0KGVsdClcbiAgICBmb3JFYWNoKGVsdC5wYXJlbnRFbGVtZW50LmNoaWxkcmVuLCBmdW5jdGlvbihjaGlsZCkge1xuICAgICAgcmVtb3ZlQ2xhc3NGcm9tRWxlbWVudChjaGlsZCwgY2xhenopXG4gICAgfSlcbiAgICBhZGRDbGFzc1RvRWxlbWVudChhc0VsZW1lbnQoZWx0KSwgY2xhenopXG4gIH1cblxuICAvKipcbiAgICogRmluZHMgdGhlIGNsb3Nlc3QgbWF0Y2hpbmcgZWxlbWVudCBpbiB0aGUgZ2l2ZW4gZWxlbWVudHMgcGFyZW50YWdlLCBpbmNsdXNpdmUgb2YgdGhlIGVsZW1lbnRcbiAgICpcbiAgICogQHNlZSBodHRwczovL2h0bXgub3JnL2FwaS8jY2xvc2VzdFxuICAgKlxuICAgKiBAcGFyYW0ge0VsZW1lbnR8c3RyaW5nfSBlbHQgdGhlIGVsZW1lbnQgdG8gZmluZCB0aGUgc2VsZWN0b3IgZnJvbVxuICAgKiBAcGFyYW0ge3N0cmluZ30gc2VsZWN0b3IgdGhlIHNlbGVjdG9yIHRvIGZpbmRcbiAgICogQHJldHVybnMge0VsZW1lbnR8bnVsbH1cbiAgICovXG4gIGZ1bmN0aW9uIGNsb3Nlc3QoZWx0LCBzZWxlY3Rvcikge1xuICAgIGVsdCA9IGFzRWxlbWVudChyZXNvbHZlVGFyZ2V0KGVsdCkpXG4gICAgaWYgKGVsdCAmJiBlbHQuY2xvc2VzdCkge1xuICAgICAgcmV0dXJuIGVsdC5jbG9zZXN0KHNlbGVjdG9yKVxuICAgIH0gZWxzZSB7XG4gICAgICAvLyBUT0RPIHJlbW92ZSB3aGVuIElFIGdvZXMgYXdheVxuICAgICAgZG8ge1xuICAgICAgICBpZiAoZWx0ID09IG51bGwgfHwgbWF0Y2hlcyhlbHQsIHNlbGVjdG9yKSkge1xuICAgICAgICAgIHJldHVybiBlbHRcbiAgICAgICAgfVxuICAgICAgfVxuICAgICAgd2hpbGUgKGVsdCA9IGVsdCAmJiBhc0VsZW1lbnQocGFyZW50RWx0KGVsdCkpKVxuICAgICAgcmV0dXJuIG51bGxcbiAgICB9XG4gIH1cblxuICAvKipcbiAgICogQHBhcmFtIHtzdHJpbmd9IHN0clxuICAgKiBAcGFyYW0ge3N0cmluZ30gcHJlZml4XG4gICAqIEByZXR1cm5zIHtib29sZWFufVxuICAgKi9cbiAgZnVuY3Rpb24gc3RhcnRzV2l0aChzdHIsIHByZWZpeCkge1xuICAgIHJldHVybiBzdHIuc3Vic3RyaW5nKDAsIHByZWZpeC5sZW5ndGgpID09PSBwcmVmaXhcbiAgfVxuXG4gIC8qKlxuICAgKiBAcGFyYW0ge3N0cmluZ30gc3RyXG4gICAqIEBwYXJhbSB7c3RyaW5nfSBzdWZmaXhcbiAgICogQHJldHVybnMge2Jvb2xlYW59XG4gICAqL1xuICBmdW5jdGlvbiBlbmRzV2l0aChzdHIsIHN1ZmZpeCkge1xuICAgIHJldHVybiBzdHIuc3Vic3RyaW5nKHN0ci5sZW5ndGggLSBzdWZmaXgubGVuZ3RoKSA9PT0gc3VmZml4XG4gIH1cblxuICAvKipcbiAgICogQHBhcmFtIHtzdHJpbmd9IHNlbGVjdG9yXG4gICAqIEByZXR1cm5zIHtzdHJpbmd9XG4gICAqL1xuICBmdW5jdGlvbiBub3JtYWxpemVTZWxlY3RvcihzZWxlY3Rvcikge1xuICAgIGNvbnN0IHRyaW1tZWRTZWxlY3RvciA9IHNlbGVjdG9yLnRyaW0oKVxuICAgIGlmIChzdGFydHNXaXRoKHRyaW1tZWRTZWxlY3RvciwgJzwnKSAmJiBlbmRzV2l0aCh0cmltbWVkU2VsZWN0b3IsICcvPicpKSB7XG4gICAgICByZXR1cm4gdHJpbW1lZFNlbGVjdG9yLnN1YnN0cmluZygxLCB0cmltbWVkU2VsZWN0b3IubGVuZ3RoIC0gMilcbiAgICB9IGVsc2Uge1xuICAgICAgcmV0dXJuIHRyaW1tZWRTZWxlY3RvclxuICAgIH1cbiAgfVxuXG4gIC8qKlxuICAgKiBAcGFyYW0ge05vZGV8RWxlbWVudHxEb2N1bWVudHxzdHJpbmd9IGVsdFxuICAgKiBAcGFyYW0ge3N0cmluZ30gc2VsZWN0b3JcbiAgICogQHBhcmFtIHtib29sZWFuPX0gZ2xvYmFsXG4gICAqIEByZXR1cm5zIHsoTm9kZXxXaW5kb3cpW119XG4gICAqL1xuICBmdW5jdGlvbiBxdWVyeVNlbGVjdG9yQWxsRXh0KGVsdCwgc2VsZWN0b3IsIGdsb2JhbCkge1xuICAgIGlmIChzZWxlY3Rvci5pbmRleE9mKCdnbG9iYWwgJykgPT09IDApIHtcbiAgICAgIHJldHVybiBxdWVyeVNlbGVjdG9yQWxsRXh0KGVsdCwgc2VsZWN0b3Iuc2xpY2UoNyksIHRydWUpXG4gICAgfVxuXG4gICAgZWx0ID0gcmVzb2x2ZVRhcmdldChlbHQpXG5cbiAgICBjb25zdCBwYXJ0cyA9IFtdXG4gICAge1xuICAgICAgbGV0IGNoZXZyb25zQ291bnQgPSAwXG4gICAgICBsZXQgb2Zmc2V0ID0gMFxuICAgICAgZm9yIChsZXQgaSA9IDA7IGkgPCBzZWxlY3Rvci5sZW5ndGg7IGkrKykge1xuICAgICAgICBjb25zdCBjaGFyID0gc2VsZWN0b3JbaV1cbiAgICAgICAgaWYgKGNoYXIgPT09ICcsJyAmJiBjaGV2cm9uc0NvdW50ID09PSAwKSB7XG4gICAgICAgICAgcGFydHMucHVzaChzZWxlY3Rvci5zdWJzdHJpbmcob2Zmc2V0LCBpKSlcbiAgICAgICAgICBvZmZzZXQgPSBpICsgMVxuICAgICAgICAgIGNvbnRpbnVlXG4gICAgICAgIH1cbiAgICAgICAgaWYgKGNoYXIgPT09ICc8Jykge1xuICAgICAgICAgIGNoZXZyb25zQ291bnQrK1xuICAgICAgICB9IGVsc2UgaWYgKGNoYXIgPT09ICcvJyAmJiBpIDwgc2VsZWN0b3IubGVuZ3RoIC0gMSAmJiBzZWxlY3RvcltpICsgMV0gPT09ICc+Jykge1xuICAgICAgICAgIGNoZXZyb25zQ291bnQtLVxuICAgICAgICB9XG4gICAgICB9XG4gICAgICBpZiAob2Zmc2V0IDwgc2VsZWN0b3IubGVuZ3RoKSB7XG4gICAgICAgIHBhcnRzLnB1c2goc2VsZWN0b3Iuc3Vic3RyaW5nKG9mZnNldCkpXG4gICAgICB9XG4gICAgfVxuXG4gICAgY29uc3QgcmVzdWx0ID0gW11cbiAgICBjb25zdCB1bnByb2Nlc3NlZFBhcnRzID0gW11cbiAgICB3aGlsZSAocGFydHMubGVuZ3RoID4gMCkge1xuICAgICAgY29uc3Qgc2VsZWN0b3IgPSBub3JtYWxpemVTZWxlY3RvcihwYXJ0cy5zaGlmdCgpKVxuICAgICAgbGV0IGl0ZW1cbiAgICAgIGlmIChzZWxlY3Rvci5pbmRleE9mKCdjbG9zZXN0ICcpID09PSAwKSB7XG4gICAgICAgIGl0ZW0gPSBjbG9zZXN0KGFzRWxlbWVudChlbHQpLCBub3JtYWxpemVTZWxlY3RvcihzZWxlY3Rvci5zdWJzdHIoOCkpKVxuICAgICAgfSBlbHNlIGlmIChzZWxlY3Rvci5pbmRleE9mKCdmaW5kICcpID09PSAwKSB7XG4gICAgICAgIGl0ZW0gPSBmaW5kKGFzUGFyZW50Tm9kZShlbHQpLCBub3JtYWxpemVTZWxlY3RvcihzZWxlY3Rvci5zdWJzdHIoNSkpKVxuICAgICAgfSBlbHNlIGlmIChzZWxlY3RvciA9PT0gJ25leHQnIHx8IHNlbGVjdG9yID09PSAnbmV4dEVsZW1lbnRTaWJsaW5nJykge1xuICAgICAgICBpdGVtID0gYXNFbGVtZW50KGVsdCkubmV4dEVsZW1lbnRTaWJsaW5nXG4gICAgICB9IGVsc2UgaWYgKHNlbGVjdG9yLmluZGV4T2YoJ25leHQgJykgPT09IDApIHtcbiAgICAgICAgaXRlbSA9IHNjYW5Gb3J3YXJkUXVlcnkoZWx0LCBub3JtYWxpemVTZWxlY3RvcihzZWxlY3Rvci5zdWJzdHIoNSkpLCAhIWdsb2JhbClcbiAgICAgIH0gZWxzZSBpZiAoc2VsZWN0b3IgPT09ICdwcmV2aW91cycgfHwgc2VsZWN0b3IgPT09ICdwcmV2aW91c0VsZW1lbnRTaWJsaW5nJykge1xuICAgICAgICBpdGVtID0gYXNFbGVtZW50KGVsdCkucHJldmlvdXNFbGVtZW50U2libGluZ1xuICAgICAgfSBlbHNlIGlmIChzZWxlY3Rvci5pbmRleE9mKCdwcmV2aW91cyAnKSA9PT0gMCkge1xuICAgICAgICBpdGVtID0gc2NhbkJhY2t3YXJkc1F1ZXJ5KGVsdCwgbm9ybWFsaXplU2VsZWN0b3Ioc2VsZWN0b3Iuc3Vic3RyKDkpKSwgISFnbG9iYWwpXG4gICAgICB9IGVsc2UgaWYgKHNlbGVjdG9yID09PSAnZG9jdW1lbnQnKSB7XG4gICAgICAgIGl0ZW0gPSBkb2N1bWVudFxuICAgICAgfSBlbHNlIGlmIChzZWxlY3RvciA9PT0gJ3dpbmRvdycpIHtcbiAgICAgICAgaXRlbSA9IHdpbmRvd1xuICAgICAgfSBlbHNlIGlmIChzZWxlY3RvciA9PT0gJ2JvZHknKSB7XG4gICAgICAgIGl0ZW0gPSBkb2N1bWVudC5ib2R5XG4gICAgICB9IGVsc2UgaWYgKHNlbGVjdG9yID09PSAncm9vdCcpIHtcbiAgICAgICAgaXRlbSA9IGdldFJvb3ROb2RlKGVsdCwgISFnbG9iYWwpXG4gICAgICB9IGVsc2UgaWYgKHNlbGVjdG9yID09PSAnaG9zdCcpIHtcbiAgICAgICAgaXRlbSA9ICgvKiogQHR5cGUgU2hhZG93Um9vdCAqLyhlbHQuZ2V0Um9vdE5vZGUoKSkpLmhvc3RcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIHVucHJvY2Vzc2VkUGFydHMucHVzaChzZWxlY3RvcilcbiAgICAgIH1cblxuICAgICAgaWYgKGl0ZW0pIHtcbiAgICAgICAgcmVzdWx0LnB1c2goaXRlbSlcbiAgICAgIH1cbiAgICB9XG5cbiAgICBpZiAodW5wcm9jZXNzZWRQYXJ0cy5sZW5ndGggPiAwKSB7XG4gICAgICBjb25zdCBzdGFuZGFyZFNlbGVjdG9yID0gdW5wcm9jZXNzZWRQYXJ0cy5qb2luKCcsJylcbiAgICAgIGNvbnN0IHJvb3ROb2RlID0gYXNQYXJlbnROb2RlKGdldFJvb3ROb2RlKGVsdCwgISFnbG9iYWwpKVxuICAgICAgcmVzdWx0LnB1c2goLi4udG9BcnJheShyb290Tm9kZS5xdWVyeVNlbGVjdG9yQWxsKHN0YW5kYXJkU2VsZWN0b3IpKSlcbiAgICB9XG5cbiAgICByZXR1cm4gcmVzdWx0XG4gIH1cblxuICAvKipcbiAgICogQHBhcmFtIHtOb2RlfSBzdGFydFxuICAgKiBAcGFyYW0ge3N0cmluZ30gbWF0Y2hcbiAgICogQHBhcmFtIHtib29sZWFufSBnbG9iYWxcbiAgICogQHJldHVybnMge0VsZW1lbnR9XG4gICAqL1xuICB2YXIgc2NhbkZvcndhcmRRdWVyeSA9IGZ1bmN0aW9uKHN0YXJ0LCBtYXRjaCwgZ2xvYmFsKSB7XG4gICAgY29uc3QgcmVzdWx0cyA9IGFzUGFyZW50Tm9kZShnZXRSb290Tm9kZShzdGFydCwgZ2xvYmFsKSkucXVlcnlTZWxlY3RvckFsbChtYXRjaClcbiAgICBmb3IgKGxldCBpID0gMDsgaSA8IHJlc3VsdHMubGVuZ3RoOyBpKyspIHtcbiAgICAgIGNvbnN0IGVsdCA9IHJlc3VsdHNbaV1cbiAgICAgIGlmIChlbHQuY29tcGFyZURvY3VtZW50UG9zaXRpb24oc3RhcnQpID09PSBOb2RlLkRPQ1VNRU5UX1BPU0lUSU9OX1BSRUNFRElORykge1xuICAgICAgICByZXR1cm4gZWx0XG4gICAgICB9XG4gICAgfVxuICB9XG5cbiAgLyoqXG4gICAqIEBwYXJhbSB7Tm9kZX0gc3RhcnRcbiAgICogQHBhcmFtIHtzdHJpbmd9IG1hdGNoXG4gICAqIEBwYXJhbSB7Ym9vbGVhbn0gZ2xvYmFsXG4gICAqIEByZXR1cm5zIHtFbGVtZW50fVxuICAgKi9cbiAgdmFyIHNjYW5CYWNrd2FyZHNRdWVyeSA9IGZ1bmN0aW9uKHN0YXJ0LCBtYXRjaCwgZ2xvYmFsKSB7XG4gICAgY29uc3QgcmVzdWx0cyA9IGFzUGFyZW50Tm9kZShnZXRSb290Tm9kZShzdGFydCwgZ2xvYmFsKSkucXVlcnlTZWxlY3RvckFsbChtYXRjaClcbiAgICBmb3IgKGxldCBpID0gcmVzdWx0cy5sZW5ndGggLSAxOyBpID49IDA7IGktLSkge1xuICAgICAgY29uc3QgZWx0ID0gcmVzdWx0c1tpXVxuICAgICAgaWYgKGVsdC5jb21wYXJlRG9jdW1lbnRQb3NpdGlvbihzdGFydCkgPT09IE5vZGUuRE9DVU1FTlRfUE9TSVRJT05fRk9MTE9XSU5HKSB7XG4gICAgICAgIHJldHVybiBlbHRcbiAgICAgIH1cbiAgICB9XG4gIH1cblxuICAvKipcbiAgICogQHBhcmFtIHtOb2RlfHN0cmluZ30gZWx0T3JTZWxlY3RvclxuICAgKiBAcGFyYW0ge3N0cmluZz19IHNlbGVjdG9yXG4gICAqIEByZXR1cm5zIHtOb2RlfFdpbmRvd31cbiAgICovXG4gIGZ1bmN0aW9uIHF1ZXJ5U2VsZWN0b3JFeHQoZWx0T3JTZWxlY3Rvciwgc2VsZWN0b3IpIHtcbiAgICBpZiAodHlwZW9mIGVsdE9yU2VsZWN0b3IgIT09ICdzdHJpbmcnKSB7XG4gICAgICByZXR1cm4gcXVlcnlTZWxlY3RvckFsbEV4dChlbHRPclNlbGVjdG9yLCBzZWxlY3RvcilbMF1cbiAgICB9IGVsc2Uge1xuICAgICAgcmV0dXJuIHF1ZXJ5U2VsZWN0b3JBbGxFeHQoZ2V0RG9jdW1lbnQoKS5ib2R5LCBlbHRPclNlbGVjdG9yKVswXVxuICAgIH1cbiAgfVxuXG4gIC8qKlxuICAgKiBAdGVtcGxhdGUge0V2ZW50VGFyZ2V0fSBUXG4gICAqIEBwYXJhbSB7VHxzdHJpbmd9IGVsdE9yU2VsZWN0b3JcbiAgICogQHBhcmFtIHtUfSBbY29udGV4dF1cbiAgICogQHJldHVybnMge0VsZW1lbnR8VHxudWxsfVxuICAgKi9cbiAgZnVuY3Rpb24gcmVzb2x2ZVRhcmdldChlbHRPclNlbGVjdG9yLCBjb250ZXh0KSB7XG4gICAgaWYgKHR5cGVvZiBlbHRPclNlbGVjdG9yID09PSAnc3RyaW5nJykge1xuICAgICAgcmV0dXJuIGZpbmQoYXNQYXJlbnROb2RlKGNvbnRleHQpIHx8IGRvY3VtZW50LCBlbHRPclNlbGVjdG9yKVxuICAgIH0gZWxzZSB7XG4gICAgICByZXR1cm4gZWx0T3JTZWxlY3RvclxuICAgIH1cbiAgfVxuXG4gIC8qKlxuICAgKiBAdHlwZWRlZiB7a2V5b2YgSFRNTEVsZW1lbnRFdmVudE1hcHxzdHJpbmd9IEFueUV2ZW50TmFtZVxuICAgKi9cblxuICAvKipcbiAgICogQHR5cGVkZWYge09iamVjdH0gRXZlbnRBcmdzXG4gICAqIEBwcm9wZXJ0eSB7RXZlbnRUYXJnZXR9IHRhcmdldFxuICAgKiBAcHJvcGVydHkge0FueUV2ZW50TmFtZX0gZXZlbnRcbiAgICogQHByb3BlcnR5IHtFdmVudExpc3RlbmVyfSBsaXN0ZW5lclxuICAgKiBAcHJvcGVydHkge09iamVjdHxib29sZWFufSBvcHRpb25zXG4gICAqL1xuXG4gIC8qKlxuICAgKiBAcGFyYW0ge0V2ZW50VGFyZ2V0fEFueUV2ZW50TmFtZX0gYXJnMVxuICAgKiBAcGFyYW0ge0FueUV2ZW50TmFtZXxFdmVudExpc3RlbmVyfSBhcmcyXG4gICAqIEBwYXJhbSB7RXZlbnRMaXN0ZW5lcnxPYmplY3R8Ym9vbGVhbn0gW2FyZzNdXG4gICAqIEBwYXJhbSB7T2JqZWN0fGJvb2xlYW59IFthcmc0XVxuICAgKiBAcmV0dXJucyB7RXZlbnRBcmdzfVxuICAgKi9cbiAgZnVuY3Rpb24gcHJvY2Vzc0V2ZW50QXJncyhhcmcxLCBhcmcyLCBhcmczLCBhcmc0KSB7XG4gICAgaWYgKGlzRnVuY3Rpb24oYXJnMikpIHtcbiAgICAgIHJldHVybiB7XG4gICAgICAgIHRhcmdldDogZ2V0RG9jdW1lbnQoKS5ib2R5LFxuICAgICAgICBldmVudDogYXNTdHJpbmcoYXJnMSksXG4gICAgICAgIGxpc3RlbmVyOiBhcmcyLFxuICAgICAgICBvcHRpb25zOiBhcmczXG4gICAgICB9XG4gICAgfSBlbHNlIHtcbiAgICAgIHJldHVybiB7XG4gICAgICAgIHRhcmdldDogcmVzb2x2ZVRhcmdldChhcmcxKSxcbiAgICAgICAgZXZlbnQ6IGFzU3RyaW5nKGFyZzIpLFxuICAgICAgICBsaXN0ZW5lcjogYXJnMyxcbiAgICAgICAgb3B0aW9uczogYXJnNFxuICAgICAgfVxuICAgIH1cbiAgfVxuXG4gIC8qKlxuICAgKiBBZGRzIGFuIGV2ZW50IGxpc3RlbmVyIHRvIGFuIGVsZW1lbnRcbiAgICpcbiAgICogQHNlZSBodHRwczovL2h0bXgub3JnL2FwaS8jb25cbiAgICpcbiAgICogQHBhcmFtIHtFdmVudFRhcmdldHxzdHJpbmd9IGFyZzEgdGhlIGVsZW1lbnQgdG8gYWRkIHRoZSBsaXN0ZW5lciB0byB8IHRoZSBldmVudCBuYW1lIHRvIGFkZCB0aGUgbGlzdGVuZXIgZm9yXG4gICAqIEBwYXJhbSB7c3RyaW5nfEV2ZW50TGlzdGVuZXJ9IGFyZzIgdGhlIGV2ZW50IG5hbWUgdG8gYWRkIHRoZSBsaXN0ZW5lciBmb3IgfCB0aGUgbGlzdGVuZXIgdG8gYWRkXG4gICAqIEBwYXJhbSB7RXZlbnRMaXN0ZW5lcnxPYmplY3R8Ym9vbGVhbn0gW2FyZzNdIHRoZSBsaXN0ZW5lciB0byBhZGQgfCBvcHRpb25zIHRvIGFkZFxuICAgKiBAcGFyYW0ge09iamVjdHxib29sZWFufSBbYXJnNF0gb3B0aW9ucyB0byBhZGRcbiAgICogQHJldHVybnMge0V2ZW50TGlzdGVuZXJ9XG4gICAqL1xuICBmdW5jdGlvbiBhZGRFdmVudExpc3RlbmVySW1wbChhcmcxLCBhcmcyLCBhcmczLCBhcmc0KSB7XG4gICAgcmVhZHkoZnVuY3Rpb24oKSB7XG4gICAgICBjb25zdCBldmVudEFyZ3MgPSBwcm9jZXNzRXZlbnRBcmdzKGFyZzEsIGFyZzIsIGFyZzMsIGFyZzQpXG4gICAgICBldmVudEFyZ3MudGFyZ2V0LmFkZEV2ZW50TGlzdGVuZXIoZXZlbnRBcmdzLmV2ZW50LCBldmVudEFyZ3MubGlzdGVuZXIsIGV2ZW50QXJncy5vcHRpb25zKVxuICAgIH0pXG4gICAgY29uc3QgYiA9IGlzRnVuY3Rpb24oYXJnMilcbiAgICByZXR1cm4gYiA/IGFyZzIgOiBhcmczXG4gIH1cblxuICAvKipcbiAgICogUmVtb3ZlcyBhbiBldmVudCBsaXN0ZW5lciBmcm9tIGFuIGVsZW1lbnRcbiAgICpcbiAgICogQHNlZSBodHRwczovL2h0bXgub3JnL2FwaS8jb2ZmXG4gICAqXG4gICAqIEBwYXJhbSB7RXZlbnRUYXJnZXR8c3RyaW5nfSBhcmcxIHRoZSBlbGVtZW50IHRvIHJlbW92ZSB0aGUgbGlzdGVuZXIgZnJvbSB8IHRoZSBldmVudCBuYW1lIHRvIHJlbW92ZSB0aGUgbGlzdGVuZXIgZnJvbVxuICAgKiBAcGFyYW0ge3N0cmluZ3xFdmVudExpc3RlbmVyfSBhcmcyIHRoZSBldmVudCBuYW1lIHRvIHJlbW92ZSB0aGUgbGlzdGVuZXIgZnJvbSB8IHRoZSBsaXN0ZW5lciB0byByZW1vdmVcbiAgICogQHBhcmFtIHtFdmVudExpc3RlbmVyfSBbYXJnM10gdGhlIGxpc3RlbmVyIHRvIHJlbW92ZVxuICAgKiBAcmV0dXJucyB7RXZlbnRMaXN0ZW5lcn1cbiAgICovXG4gIGZ1bmN0aW9uIHJlbW92ZUV2ZW50TGlzdGVuZXJJbXBsKGFyZzEsIGFyZzIsIGFyZzMpIHtcbiAgICByZWFkeShmdW5jdGlvbigpIHtcbiAgICAgIGNvbnN0IGV2ZW50QXJncyA9IHByb2Nlc3NFdmVudEFyZ3MoYXJnMSwgYXJnMiwgYXJnMylcbiAgICAgIGV2ZW50QXJncy50YXJnZXQucmVtb3ZlRXZlbnRMaXN0ZW5lcihldmVudEFyZ3MuZXZlbnQsIGV2ZW50QXJncy5saXN0ZW5lcilcbiAgICB9KVxuICAgIHJldHVybiBpc0Z1bmN0aW9uKGFyZzIpID8gYXJnMiA6IGFyZzNcbiAgfVxuXG4gIC8vPSA9PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09XG4gIC8vIE5vZGUgcHJvY2Vzc2luZ1xuICAvLz0gPT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PVxuXG4gIGNvbnN0IERVTU1ZX0VMVCA9IGdldERvY3VtZW50KCkuY3JlYXRlRWxlbWVudCgnb3V0cHV0JykgLy8gZHVtbXkgZWxlbWVudCBmb3IgYmFkIHNlbGVjdG9yc1xuICAvKipcbiAgICogQHBhcmFtIHtFbGVtZW50fSBlbHRcbiAgICogQHBhcmFtIHtzdHJpbmd9IGF0dHJOYW1lXG4gICAqIEByZXR1cm5zIHsoTm9kZXxXaW5kb3cpW119XG4gICAqL1xuICBmdW5jdGlvbiBmaW5kQXR0cmlidXRlVGFyZ2V0cyhlbHQsIGF0dHJOYW1lKSB7XG4gICAgY29uc3QgYXR0clRhcmdldCA9IGdldENsb3Nlc3RBdHRyaWJ1dGVWYWx1ZShlbHQsIGF0dHJOYW1lKVxuICAgIGlmIChhdHRyVGFyZ2V0KSB7XG4gICAgICBpZiAoYXR0clRhcmdldCA9PT0gJ3RoaXMnKSB7XG4gICAgICAgIHJldHVybiBbZmluZFRoaXNFbGVtZW50KGVsdCwgYXR0ck5hbWUpXVxuICAgICAgfSBlbHNlIHtcbiAgICAgICAgY29uc3QgcmVzdWx0ID0gcXVlcnlTZWxlY3RvckFsbEV4dChlbHQsIGF0dHJUYXJnZXQpXG4gICAgICAgIGlmIChyZXN1bHQubGVuZ3RoID09PSAwKSB7XG4gICAgICAgICAgbG9nRXJyb3IoJ1RoZSBzZWxlY3RvciBcIicgKyBhdHRyVGFyZ2V0ICsgJ1wiIG9uICcgKyBhdHRyTmFtZSArICcgcmV0dXJuZWQgbm8gbWF0Y2hlcyEnKVxuICAgICAgICAgIHJldHVybiBbRFVNTVlfRUxUXVxuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgIHJldHVybiByZXN1bHRcbiAgICAgICAgfVxuICAgICAgfVxuICAgIH1cbiAgfVxuXG4gIC8qKlxuICAgKiBAcGFyYW0ge0VsZW1lbnR9IGVsdFxuICAgKiBAcGFyYW0ge3N0cmluZ30gYXR0cmlidXRlXG4gICAqIEByZXR1cm5zIHtFbGVtZW50fG51bGx9XG4gICAqL1xuICBmdW5jdGlvbiBmaW5kVGhpc0VsZW1lbnQoZWx0LCBhdHRyaWJ1dGUpIHtcbiAgICByZXR1cm4gYXNFbGVtZW50KGdldENsb3Nlc3RNYXRjaChlbHQsIGZ1bmN0aW9uKGVsdCkge1xuICAgICAgcmV0dXJuIGdldEF0dHJpYnV0ZVZhbHVlKGFzRWxlbWVudChlbHQpLCBhdHRyaWJ1dGUpICE9IG51bGxcbiAgICB9KSlcbiAgfVxuXG4gIC8qKlxuICAgKiBAcGFyYW0ge0VsZW1lbnR9IGVsdFxuICAgKiBAcmV0dXJucyB7Tm9kZXxXaW5kb3d8bnVsbH1cbiAgICovXG4gIGZ1bmN0aW9uIGdldFRhcmdldChlbHQpIHtcbiAgICBjb25zdCB0YXJnZXRTdHIgPSBnZXRDbG9zZXN0QXR0cmlidXRlVmFsdWUoZWx0LCAnaHgtdGFyZ2V0JylcbiAgICBpZiAodGFyZ2V0U3RyKSB7XG4gICAgICBpZiAodGFyZ2V0U3RyID09PSAndGhpcycpIHtcbiAgICAgICAgcmV0dXJuIGZpbmRUaGlzRWxlbWVudChlbHQsICdoeC10YXJnZXQnKVxuICAgICAgfSBlbHNlIHtcbiAgICAgICAgcmV0dXJuIHF1ZXJ5U2VsZWN0b3JFeHQoZWx0LCB0YXJnZXRTdHIpXG4gICAgICB9XG4gICAgfSBlbHNlIHtcbiAgICAgIGNvbnN0IGRhdGEgPSBnZXRJbnRlcm5hbERhdGEoZWx0KVxuICAgICAgaWYgKGRhdGEuYm9vc3RlZCkge1xuICAgICAgICByZXR1cm4gZ2V0RG9jdW1lbnQoKS5ib2R5XG4gICAgICB9IGVsc2Uge1xuICAgICAgICByZXR1cm4gZWx0XG4gICAgICB9XG4gICAgfVxuICB9XG5cbiAgLyoqXG4gICAqIEBwYXJhbSB7c3RyaW5nfSBuYW1lXG4gICAqIEByZXR1cm5zIHtib29sZWFufVxuICAgKi9cbiAgZnVuY3Rpb24gc2hvdWxkU2V0dGxlQXR0cmlidXRlKG5hbWUpIHtcbiAgICBjb25zdCBhdHRyaWJ1dGVzVG9TZXR0bGUgPSBodG14LmNvbmZpZy5hdHRyaWJ1dGVzVG9TZXR0bGVcbiAgICBmb3IgKGxldCBpID0gMDsgaSA8IGF0dHJpYnV0ZXNUb1NldHRsZS5sZW5ndGg7IGkrKykge1xuICAgICAgaWYgKG5hbWUgPT09IGF0dHJpYnV0ZXNUb1NldHRsZVtpXSkge1xuICAgICAgICByZXR1cm4gdHJ1ZVxuICAgICAgfVxuICAgIH1cbiAgICByZXR1cm4gZmFsc2VcbiAgfVxuXG4gIC8qKlxuICAgKiBAcGFyYW0ge0VsZW1lbnR9IG1lcmdlVG9cbiAgICogQHBhcmFtIHtFbGVtZW50fSBtZXJnZUZyb21cbiAgICovXG4gIGZ1bmN0aW9uIGNsb25lQXR0cmlidXRlcyhtZXJnZVRvLCBtZXJnZUZyb20pIHtcbiAgICBmb3JFYWNoKG1lcmdlVG8uYXR0cmlidXRlcywgZnVuY3Rpb24oYXR0cikge1xuICAgICAgaWYgKCFtZXJnZUZyb20uaGFzQXR0cmlidXRlKGF0dHIubmFtZSkgJiYgc2hvdWxkU2V0dGxlQXR0cmlidXRlKGF0dHIubmFtZSkpIHtcbiAgICAgICAgbWVyZ2VUby5yZW1vdmVBdHRyaWJ1dGUoYXR0ci5uYW1lKVxuICAgICAgfVxuICAgIH0pXG4gICAgZm9yRWFjaChtZXJnZUZyb20uYXR0cmlidXRlcywgZnVuY3Rpb24oYXR0cikge1xuICAgICAgaWYgKHNob3VsZFNldHRsZUF0dHJpYnV0ZShhdHRyLm5hbWUpKSB7XG4gICAgICAgIG1lcmdlVG8uc2V0QXR0cmlidXRlKGF0dHIubmFtZSwgYXR0ci52YWx1ZSlcbiAgICAgIH1cbiAgICB9KVxuICB9XG5cbiAgLyoqXG4gICAqIEBwYXJhbSB7SHRteFN3YXBTdHlsZX0gc3dhcFN0eWxlXG4gICAqIEBwYXJhbSB7RWxlbWVudH0gdGFyZ2V0XG4gICAqIEByZXR1cm5zIHtib29sZWFufVxuICAgKi9cbiAgZnVuY3Rpb24gaXNJbmxpbmVTd2FwKHN3YXBTdHlsZSwgdGFyZ2V0KSB7XG4gICAgY29uc3QgZXh0ZW5zaW9ucyA9IGdldEV4dGVuc2lvbnModGFyZ2V0KVxuICAgIGZvciAobGV0IGkgPSAwOyBpIDwgZXh0ZW5zaW9ucy5sZW5ndGg7IGkrKykge1xuICAgICAgY29uc3QgZXh0ZW5zaW9uID0gZXh0ZW5zaW9uc1tpXVxuICAgICAgdHJ5IHtcbiAgICAgICAgaWYgKGV4dGVuc2lvbi5pc0lubGluZVN3YXAoc3dhcFN0eWxlKSkge1xuICAgICAgICAgIHJldHVybiB0cnVlXG4gICAgICAgIH1cbiAgICAgIH0gY2F0Y2ggKGUpIHtcbiAgICAgICAgbG9nRXJyb3IoZSlcbiAgICAgIH1cbiAgICB9XG4gICAgcmV0dXJuIHN3YXBTdHlsZSA9PT0gJ291dGVySFRNTCdcbiAgfVxuXG4gIC8qKlxuICAgKiBAcGFyYW0ge3N0cmluZ30gb29iVmFsdWVcbiAgICogQHBhcmFtIHtFbGVtZW50fSBvb2JFbGVtZW50XG4gICAqIEBwYXJhbSB7SHRteFNldHRsZUluZm99IHNldHRsZUluZm9cbiAgICogQHBhcmFtIHtOb2RlfERvY3VtZW50fSBbcm9vdE5vZGVdXG4gICAqIEByZXR1cm5zXG4gICAqL1xuICBmdW5jdGlvbiBvb2JTd2FwKG9vYlZhbHVlLCBvb2JFbGVtZW50LCBzZXR0bGVJbmZvLCByb290Tm9kZSkge1xuICAgIHJvb3ROb2RlID0gcm9vdE5vZGUgfHwgZ2V0RG9jdW1lbnQoKVxuICAgIGxldCBzZWxlY3RvciA9ICcjJyArIGdldFJhd0F0dHJpYnV0ZShvb2JFbGVtZW50LCAnaWQnKVxuICAgIC8qKiBAdHlwZSBIdG14U3dhcFN0eWxlICovXG4gICAgbGV0IHN3YXBTdHlsZSA9ICdvdXRlckhUTUwnXG4gICAgaWYgKG9vYlZhbHVlID09PSAndHJ1ZScpIHtcbiAgICAgIC8vIGRvIG5vdGhpbmdcbiAgICB9IGVsc2UgaWYgKG9vYlZhbHVlLmluZGV4T2YoJzonKSA+IDApIHtcbiAgICAgIHN3YXBTdHlsZSA9IG9vYlZhbHVlLnN1YnN0cmluZygwLCBvb2JWYWx1ZS5pbmRleE9mKCc6JykpXG4gICAgICBzZWxlY3RvciA9IG9vYlZhbHVlLnN1YnN0cmluZyhvb2JWYWx1ZS5pbmRleE9mKCc6JykgKyAxKVxuICAgIH0gZWxzZSB7XG4gICAgICBzd2FwU3R5bGUgPSBvb2JWYWx1ZVxuICAgIH1cbiAgICBvb2JFbGVtZW50LnJlbW92ZUF0dHJpYnV0ZSgnaHgtc3dhcC1vb2InKVxuICAgIG9vYkVsZW1lbnQucmVtb3ZlQXR0cmlidXRlKCdkYXRhLWh4LXN3YXAtb29iJylcblxuICAgIGNvbnN0IHRhcmdldHMgPSBxdWVyeVNlbGVjdG9yQWxsRXh0KHJvb3ROb2RlLCBzZWxlY3RvciwgZmFsc2UpXG4gICAgaWYgKHRhcmdldHMpIHtcbiAgICAgIGZvckVhY2goXG4gICAgICAgIHRhcmdldHMsXG4gICAgICAgIGZ1bmN0aW9uKHRhcmdldCkge1xuICAgICAgICAgIGxldCBmcmFnbWVudFxuICAgICAgICAgIGNvbnN0IG9vYkVsZW1lbnRDbG9uZSA9IG9vYkVsZW1lbnQuY2xvbmVOb2RlKHRydWUpXG4gICAgICAgICAgZnJhZ21lbnQgPSBnZXREb2N1bWVudCgpLmNyZWF0ZURvY3VtZW50RnJhZ21lbnQoKVxuICAgICAgICAgIGZyYWdtZW50LmFwcGVuZENoaWxkKG9vYkVsZW1lbnRDbG9uZSlcbiAgICAgICAgICBpZiAoIWlzSW5saW5lU3dhcChzd2FwU3R5bGUsIHRhcmdldCkpIHtcbiAgICAgICAgICAgIGZyYWdtZW50ID0gYXNQYXJlbnROb2RlKG9vYkVsZW1lbnRDbG9uZSkgLy8gaWYgdGhpcyBpcyBub3QgYW4gaW5saW5lIHN3YXAsIHdlIHVzZSB0aGUgY29udGVudCBvZiB0aGUgbm9kZSwgbm90IHRoZSBub2RlIGl0c2VsZlxuICAgICAgICAgIH1cblxuICAgICAgICAgIGNvbnN0IGJlZm9yZVN3YXBEZXRhaWxzID0geyBzaG91bGRTd2FwOiB0cnVlLCB0YXJnZXQsIGZyYWdtZW50IH1cbiAgICAgICAgICBpZiAoIXRyaWdnZXJFdmVudCh0YXJnZXQsICdodG14Om9vYkJlZm9yZVN3YXAnLCBiZWZvcmVTd2FwRGV0YWlscykpIHJldHVyblxuXG4gICAgICAgICAgdGFyZ2V0ID0gYmVmb3JlU3dhcERldGFpbHMudGFyZ2V0IC8vIGFsbG93IHJlLXRhcmdldGluZ1xuICAgICAgICAgIGlmIChiZWZvcmVTd2FwRGV0YWlscy5zaG91bGRTd2FwKSB7XG4gICAgICAgICAgICBoYW5kbGVQcmVzZXJ2ZWRFbGVtZW50cyhmcmFnbWVudClcbiAgICAgICAgICAgIHN3YXBXaXRoU3R5bGUoc3dhcFN0eWxlLCB0YXJnZXQsIHRhcmdldCwgZnJhZ21lbnQsIHNldHRsZUluZm8pXG4gICAgICAgICAgICByZXN0b3JlUHJlc2VydmVkRWxlbWVudHMoKVxuICAgICAgICAgIH1cbiAgICAgICAgICBmb3JFYWNoKHNldHRsZUluZm8uZWx0cywgZnVuY3Rpb24oZWx0KSB7XG4gICAgICAgICAgICB0cmlnZ2VyRXZlbnQoZWx0LCAnaHRteDpvb2JBZnRlclN3YXAnLCBiZWZvcmVTd2FwRGV0YWlscylcbiAgICAgICAgICB9KVxuICAgICAgICB9XG4gICAgICApXG4gICAgICBvb2JFbGVtZW50LnBhcmVudE5vZGUucmVtb3ZlQ2hpbGQob29iRWxlbWVudClcbiAgICB9IGVsc2Uge1xuICAgICAgb29iRWxlbWVudC5wYXJlbnROb2RlLnJlbW92ZUNoaWxkKG9vYkVsZW1lbnQpXG4gICAgICB0cmlnZ2VyRXJyb3JFdmVudChnZXREb2N1bWVudCgpLmJvZHksICdodG14Om9vYkVycm9yTm9UYXJnZXQnLCB7IGNvbnRlbnQ6IG9vYkVsZW1lbnQgfSlcbiAgICB9XG4gICAgcmV0dXJuIG9vYlZhbHVlXG4gIH1cblxuICBmdW5jdGlvbiByZXN0b3JlUHJlc2VydmVkRWxlbWVudHMoKSB7XG4gICAgY29uc3QgcGFudHJ5ID0gZmluZCgnIy0taHRteC1wcmVzZXJ2ZS1wYW50cnktLScpXG4gICAgaWYgKHBhbnRyeSkge1xuICAgICAgZm9yIChjb25zdCBwcmVzZXJ2ZWRFbHQgb2YgWy4uLnBhbnRyeS5jaGlsZHJlbl0pIHtcbiAgICAgICAgY29uc3QgZXhpc3RpbmdFbGVtZW50ID0gZmluZCgnIycgKyBwcmVzZXJ2ZWRFbHQuaWQpXG4gICAgICAgIC8vIEB0cy1pZ25vcmUgLSB1c2UgcHJvcG9zZWQgbW92ZUJlZm9yZSBmZWF0dXJlXG4gICAgICAgIGV4aXN0aW5nRWxlbWVudC5wYXJlbnROb2RlLm1vdmVCZWZvcmUocHJlc2VydmVkRWx0LCBleGlzdGluZ0VsZW1lbnQpXG4gICAgICAgIGV4aXN0aW5nRWxlbWVudC5yZW1vdmUoKVxuICAgICAgfVxuICAgICAgcGFudHJ5LnJlbW92ZSgpXG4gICAgfVxuICB9XG5cbiAgLyoqXG4gICAqIEBwYXJhbSB7RG9jdW1lbnRGcmFnbWVudHxQYXJlbnROb2RlfSBmcmFnbWVudFxuICAgKi9cbiAgZnVuY3Rpb24gaGFuZGxlUHJlc2VydmVkRWxlbWVudHMoZnJhZ21lbnQpIHtcbiAgICBmb3JFYWNoKGZpbmRBbGwoZnJhZ21lbnQsICdbaHgtcHJlc2VydmVdLCBbZGF0YS1oeC1wcmVzZXJ2ZV0nKSwgZnVuY3Rpb24ocHJlc2VydmVkRWx0KSB7XG4gICAgICBjb25zdCBpZCA9IGdldEF0dHJpYnV0ZVZhbHVlKHByZXNlcnZlZEVsdCwgJ2lkJylcbiAgICAgIGNvbnN0IGV4aXN0aW5nRWxlbWVudCA9IGdldERvY3VtZW50KCkuZ2V0RWxlbWVudEJ5SWQoaWQpXG4gICAgICBpZiAoZXhpc3RpbmdFbGVtZW50ICE9IG51bGwpIHtcbiAgICAgICAgaWYgKHByZXNlcnZlZEVsdC5tb3ZlQmVmb3JlKSB7IC8vIGlmIHRoZSBtb3ZlQmVmb3JlIEFQSSBleGlzdHMsIHVzZSBpdFxuICAgICAgICAgIC8vIGdldCBvciBjcmVhdGUgYSBzdG9yYWdlIHNwb3QgZm9yIHN0dWZmXG4gICAgICAgICAgbGV0IHBhbnRyeSA9IGZpbmQoJyMtLWh0bXgtcHJlc2VydmUtcGFudHJ5LS0nKVxuICAgICAgICAgIGlmIChwYW50cnkgPT0gbnVsbCkge1xuICAgICAgICAgICAgZ2V0RG9jdW1lbnQoKS5ib2R5Lmluc2VydEFkamFjZW50SFRNTCgnYWZ0ZXJlbmQnLCBcIjxkaXYgaWQ9Jy0taHRteC1wcmVzZXJ2ZS1wYW50cnktLSc+PC9kaXY+XCIpXG4gICAgICAgICAgICBwYW50cnkgPSBmaW5kKCcjLS1odG14LXByZXNlcnZlLXBhbnRyeS0tJylcbiAgICAgICAgICB9XG4gICAgICAgICAgLy8gQHRzLWlnbm9yZSAtIHVzZSBwcm9wb3NlZCBtb3ZlQmVmb3JlIGZlYXR1cmVcbiAgICAgICAgICBwYW50cnkubW92ZUJlZm9yZShleGlzdGluZ0VsZW1lbnQsIG51bGwpXG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgcHJlc2VydmVkRWx0LnBhcmVudE5vZGUucmVwbGFjZUNoaWxkKGV4aXN0aW5nRWxlbWVudCwgcHJlc2VydmVkRWx0KVxuICAgICAgICB9XG4gICAgICB9XG4gICAgfSlcbiAgfVxuXG4gIC8qKlxuICAgKiBAcGFyYW0ge05vZGV9IHBhcmVudE5vZGVcbiAgICogQHBhcmFtIHtQYXJlbnROb2RlfSBmcmFnbWVudFxuICAgKiBAcGFyYW0ge0h0bXhTZXR0bGVJbmZvfSBzZXR0bGVJbmZvXG4gICAqL1xuICBmdW5jdGlvbiBoYW5kbGVBdHRyaWJ1dGVzKHBhcmVudE5vZGUsIGZyYWdtZW50LCBzZXR0bGVJbmZvKSB7XG4gICAgZm9yRWFjaChmcmFnbWVudC5xdWVyeVNlbGVjdG9yQWxsKCdbaWRdJyksIGZ1bmN0aW9uKG5ld05vZGUpIHtcbiAgICAgIGNvbnN0IGlkID0gZ2V0UmF3QXR0cmlidXRlKG5ld05vZGUsICdpZCcpXG4gICAgICBpZiAoaWQgJiYgaWQubGVuZ3RoID4gMCkge1xuICAgICAgICBjb25zdCBub3JtYWxpemVkSWQgPSBpZC5yZXBsYWNlKFwiJ1wiLCBcIlxcXFwnXCIpXG4gICAgICAgIGNvbnN0IG5vcm1hbGl6ZWRUYWcgPSBuZXdOb2RlLnRhZ05hbWUucmVwbGFjZSgnOicsICdcXFxcOicpXG4gICAgICAgIGNvbnN0IHBhcmVudEVsdCA9IGFzUGFyZW50Tm9kZShwYXJlbnROb2RlKVxuICAgICAgICBjb25zdCBvbGROb2RlID0gcGFyZW50RWx0ICYmIHBhcmVudEVsdC5xdWVyeVNlbGVjdG9yKG5vcm1hbGl6ZWRUYWcgKyBcIltpZD0nXCIgKyBub3JtYWxpemVkSWQgKyBcIiddXCIpXG4gICAgICAgIGlmIChvbGROb2RlICYmIG9sZE5vZGUgIT09IHBhcmVudEVsdCkge1xuICAgICAgICAgIGNvbnN0IG5ld0F0dHJpYnV0ZXMgPSBuZXdOb2RlLmNsb25lTm9kZSgpXG4gICAgICAgICAgY2xvbmVBdHRyaWJ1dGVzKG5ld05vZGUsIG9sZE5vZGUpXG4gICAgICAgICAgc2V0dGxlSW5mby50YXNrcy5wdXNoKGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgY2xvbmVBdHRyaWJ1dGVzKG5ld05vZGUsIG5ld0F0dHJpYnV0ZXMpXG4gICAgICAgICAgfSlcbiAgICAgICAgfVxuICAgICAgfVxuICAgIH0pXG4gIH1cblxuICAvKipcbiAgICogQHBhcmFtIHtOb2RlfSBjaGlsZFxuICAgKiBAcmV0dXJucyB7SHRteFNldHRsZVRhc2t9XG4gICAqL1xuICBmdW5jdGlvbiBtYWtlQWpheExvYWRUYXNrKGNoaWxkKSB7XG4gICAgcmV0dXJuIGZ1bmN0aW9uKCkge1xuICAgICAgcmVtb3ZlQ2xhc3NGcm9tRWxlbWVudChjaGlsZCwgaHRteC5jb25maWcuYWRkZWRDbGFzcylcbiAgICAgIHByb2Nlc3NOb2RlKGFzRWxlbWVudChjaGlsZCkpXG4gICAgICBwcm9jZXNzRm9jdXMoYXNQYXJlbnROb2RlKGNoaWxkKSlcbiAgICAgIHRyaWdnZXJFdmVudChjaGlsZCwgJ2h0bXg6bG9hZCcpXG4gICAgfVxuICB9XG5cbiAgLyoqXG4gICAqIEBwYXJhbSB7UGFyZW50Tm9kZX0gY2hpbGRcbiAgICovXG4gIGZ1bmN0aW9uIHByb2Nlc3NGb2N1cyhjaGlsZCkge1xuICAgIGNvbnN0IGF1dG9mb2N1cyA9ICdbYXV0b2ZvY3VzXSdcbiAgICBjb25zdCBhdXRvRm9jdXNlZEVsdCA9IGFzSHRtbEVsZW1lbnQobWF0Y2hlcyhjaGlsZCwgYXV0b2ZvY3VzKSA/IGNoaWxkIDogY2hpbGQucXVlcnlTZWxlY3RvcihhdXRvZm9jdXMpKVxuICAgIGlmIChhdXRvRm9jdXNlZEVsdCAhPSBudWxsKSB7XG4gICAgICBhdXRvRm9jdXNlZEVsdC5mb2N1cygpXG4gICAgfVxuICB9XG5cbiAgLyoqXG4gICAqIEBwYXJhbSB7Tm9kZX0gcGFyZW50Tm9kZVxuICAgKiBAcGFyYW0ge05vZGV9IGluc2VydEJlZm9yZVxuICAgKiBAcGFyYW0ge1BhcmVudE5vZGV9IGZyYWdtZW50XG4gICAqIEBwYXJhbSB7SHRteFNldHRsZUluZm99IHNldHRsZUluZm9cbiAgICovXG4gIGZ1bmN0aW9uIGluc2VydE5vZGVzQmVmb3JlKHBhcmVudE5vZGUsIGluc2VydEJlZm9yZSwgZnJhZ21lbnQsIHNldHRsZUluZm8pIHtcbiAgICBoYW5kbGVBdHRyaWJ1dGVzKHBhcmVudE5vZGUsIGZyYWdtZW50LCBzZXR0bGVJbmZvKVxuICAgIHdoaWxlIChmcmFnbWVudC5jaGlsZE5vZGVzLmxlbmd0aCA+IDApIHtcbiAgICAgIGNvbnN0IGNoaWxkID0gZnJhZ21lbnQuZmlyc3RDaGlsZFxuICAgICAgYWRkQ2xhc3NUb0VsZW1lbnQoYXNFbGVtZW50KGNoaWxkKSwgaHRteC5jb25maWcuYWRkZWRDbGFzcylcbiAgICAgIHBhcmVudE5vZGUuaW5zZXJ0QmVmb3JlKGNoaWxkLCBpbnNlcnRCZWZvcmUpXG4gICAgICBpZiAoY2hpbGQubm9kZVR5cGUgIT09IE5vZGUuVEVYVF9OT0RFICYmIGNoaWxkLm5vZGVUeXBlICE9PSBOb2RlLkNPTU1FTlRfTk9ERSkge1xuICAgICAgICBzZXR0bGVJbmZvLnRhc2tzLnB1c2gobWFrZUFqYXhMb2FkVGFzayhjaGlsZCkpXG4gICAgICB9XG4gICAgfVxuICB9XG5cbiAgLyoqXG4gICAqIGJhc2VkIG9uIGh0dHBzOi8vZ2lzdC5naXRodWIuY29tL2h5YW1hbW90by9mZDQzNTUwNWQyOWViZmEzZDk3MTZmZDJiZThkNDJmMCxcbiAgICogZGVyaXZlZCBmcm9tIEphdmEncyBzdHJpbmcgaGFzaGNvZGUgaW1wbGVtZW50YXRpb25cbiAgICogQHBhcmFtIHtzdHJpbmd9IHN0cmluZ1xuICAgKiBAcGFyYW0ge251bWJlcn0gaGFzaFxuICAgKiBAcmV0dXJucyB7bnVtYmVyfVxuICAgKi9cbiAgZnVuY3Rpb24gc3RyaW5nSGFzaChzdHJpbmcsIGhhc2gpIHtcbiAgICBsZXQgY2hhciA9IDBcbiAgICB3aGlsZSAoY2hhciA8IHN0cmluZy5sZW5ndGgpIHtcbiAgICAgIGhhc2ggPSAoaGFzaCA8PCA1KSAtIGhhc2ggKyBzdHJpbmcuY2hhckNvZGVBdChjaGFyKyspIHwgMCAvLyBiaXR3aXNlIG9yIGVuc3VyZXMgd2UgaGF2ZSBhIDMyLWJpdCBpbnRcbiAgICB9XG4gICAgcmV0dXJuIGhhc2hcbiAgfVxuXG4gIC8qKlxuICAgKiBAcGFyYW0ge0VsZW1lbnR9IGVsdFxuICAgKiBAcmV0dXJucyB7bnVtYmVyfVxuICAgKi9cbiAgZnVuY3Rpb24gYXR0cmlidXRlSGFzaChlbHQpIHtcbiAgICBsZXQgaGFzaCA9IDBcbiAgICAvLyBJRSBmaXhcbiAgICBpZiAoZWx0LmF0dHJpYnV0ZXMpIHtcbiAgICAgIGZvciAobGV0IGkgPSAwOyBpIDwgZWx0LmF0dHJpYnV0ZXMubGVuZ3RoOyBpKyspIHtcbiAgICAgICAgY29uc3QgYXR0cmlidXRlID0gZWx0LmF0dHJpYnV0ZXNbaV1cbiAgICAgICAgaWYgKGF0dHJpYnV0ZS52YWx1ZSkgeyAvLyBvbmx5IGluY2x1ZGUgYXR0cmlidXRlcyB3LyBhY3R1YWwgdmFsdWVzIChlbXB0eSBpcyBzYW1lIGFzIG5vbi1leGlzdGVudClcbiAgICAgICAgICBoYXNoID0gc3RyaW5nSGFzaChhdHRyaWJ1dGUubmFtZSwgaGFzaClcbiAgICAgICAgICBoYXNoID0gc3RyaW5nSGFzaChhdHRyaWJ1dGUudmFsdWUsIGhhc2gpXG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9XG4gICAgcmV0dXJuIGhhc2hcbiAgfVxuXG4gIC8qKlxuICAgKiBAcGFyYW0ge0V2ZW50VGFyZ2V0fSBlbHRcbiAgICovXG4gIGZ1bmN0aW9uIGRlSW5pdE9uSGFuZGxlcnMoZWx0KSB7XG4gICAgY29uc3QgaW50ZXJuYWxEYXRhID0gZ2V0SW50ZXJuYWxEYXRhKGVsdClcbiAgICBpZiAoaW50ZXJuYWxEYXRhLm9uSGFuZGxlcnMpIHtcbiAgICAgIGZvciAobGV0IGkgPSAwOyBpIDwgaW50ZXJuYWxEYXRhLm9uSGFuZGxlcnMubGVuZ3RoOyBpKyspIHtcbiAgICAgICAgY29uc3QgaGFuZGxlckluZm8gPSBpbnRlcm5hbERhdGEub25IYW5kbGVyc1tpXVxuICAgICAgICByZW1vdmVFdmVudExpc3RlbmVySW1wbChlbHQsIGhhbmRsZXJJbmZvLmV2ZW50LCBoYW5kbGVySW5mby5saXN0ZW5lcilcbiAgICAgIH1cbiAgICAgIGRlbGV0ZSBpbnRlcm5hbERhdGEub25IYW5kbGVyc1xuICAgIH1cbiAgfVxuXG4gIC8qKlxuICAgKiBAcGFyYW0ge05vZGV9IGVsZW1lbnRcbiAgICovXG4gIGZ1bmN0aW9uIGRlSW5pdE5vZGUoZWxlbWVudCkge1xuICAgIGNvbnN0IGludGVybmFsRGF0YSA9IGdldEludGVybmFsRGF0YShlbGVtZW50KVxuICAgIGlmIChpbnRlcm5hbERhdGEudGltZW91dCkge1xuICAgICAgY2xlYXJUaW1lb3V0KGludGVybmFsRGF0YS50aW1lb3V0KVxuICAgIH1cbiAgICBpZiAoaW50ZXJuYWxEYXRhLmxpc3RlbmVySW5mb3MpIHtcbiAgICAgIGZvckVhY2goaW50ZXJuYWxEYXRhLmxpc3RlbmVySW5mb3MsIGZ1bmN0aW9uKGluZm8pIHtcbiAgICAgICAgaWYgKGluZm8ub24pIHtcbiAgICAgICAgICByZW1vdmVFdmVudExpc3RlbmVySW1wbChpbmZvLm9uLCBpbmZvLnRyaWdnZXIsIGluZm8ubGlzdGVuZXIpXG4gICAgICAgIH1cbiAgICAgIH0pXG4gICAgfVxuICAgIGRlSW5pdE9uSGFuZGxlcnMoZWxlbWVudClcbiAgICBmb3JFYWNoKE9iamVjdC5rZXlzKGludGVybmFsRGF0YSksIGZ1bmN0aW9uKGtleSkgeyBpZiAoa2V5ICE9PSAnZmlyc3RJbml0Q29tcGxldGVkJykgZGVsZXRlIGludGVybmFsRGF0YVtrZXldIH0pXG4gIH1cblxuICAvKipcbiAgICogQHBhcmFtIHtOb2RlfSBlbGVtZW50XG4gICAqL1xuICBmdW5jdGlvbiBjbGVhblVwRWxlbWVudChlbGVtZW50KSB7XG4gICAgdHJpZ2dlckV2ZW50KGVsZW1lbnQsICdodG14OmJlZm9yZUNsZWFudXBFbGVtZW50JylcbiAgICBkZUluaXROb2RlKGVsZW1lbnQpXG4gICAgLy8gQHRzLWlnbm9yZSBJRTExIGNvZGVcbiAgICAvLyBub2luc3BlY3Rpb24gSlNVbnJlc29sdmVkUmVmZXJlbmNlXG4gICAgaWYgKGVsZW1lbnQuY2hpbGRyZW4pIHsgLy8gSUVcbiAgICAgIC8vIEB0cy1pZ25vcmVcbiAgICAgIGZvckVhY2goZWxlbWVudC5jaGlsZHJlbiwgZnVuY3Rpb24oY2hpbGQpIHsgY2xlYW5VcEVsZW1lbnQoY2hpbGQpIH0pXG4gICAgfVxuICB9XG5cbiAgLyoqXG4gICAqIEBwYXJhbSB7Tm9kZX0gdGFyZ2V0XG4gICAqIEBwYXJhbSB7UGFyZW50Tm9kZX0gZnJhZ21lbnRcbiAgICogQHBhcmFtIHtIdG14U2V0dGxlSW5mb30gc2V0dGxlSW5mb1xuICAgKi9cbiAgZnVuY3Rpb24gc3dhcE91dGVySFRNTCh0YXJnZXQsIGZyYWdtZW50LCBzZXR0bGVJbmZvKSB7XG4gICAgaWYgKHRhcmdldCBpbnN0YW5jZW9mIEVsZW1lbnQgJiYgdGFyZ2V0LnRhZ05hbWUgPT09ICdCT0RZJykgeyAvLyBzcGVjaWFsIGNhc2UgdGhlIGJvZHkgdG8gaW5uZXJIVE1MIGJlY2F1c2UgRG9jdW1lbnRGcmFnbWVudHMgY2FuJ3QgY29udGFpbiBhIGJvZHkgZWx0IHVuZm9ydHVuYXRlbHlcbiAgICAgIHJldHVybiBzd2FwSW5uZXJIVE1MKHRhcmdldCwgZnJhZ21lbnQsIHNldHRsZUluZm8pXG4gICAgfVxuICAgIC8qKiBAdHlwZSB7Tm9kZX0gKi9cbiAgICBsZXQgbmV3RWx0XG4gICAgY29uc3QgZWx0QmVmb3JlTmV3Q29udGVudCA9IHRhcmdldC5wcmV2aW91c1NpYmxpbmdcbiAgICBjb25zdCBwYXJlbnROb2RlID0gcGFyZW50RWx0KHRhcmdldClcbiAgICBpZiAoIXBhcmVudE5vZGUpIHsgLy8gd2hlbiBwYXJlbnQgbm9kZSBkaXNhcHBlYXJzLCB3ZSBjYW4ndCBkbyBhbnl0aGluZ1xuICAgICAgcmV0dXJuXG4gICAgfVxuICAgIGluc2VydE5vZGVzQmVmb3JlKHBhcmVudE5vZGUsIHRhcmdldCwgZnJhZ21lbnQsIHNldHRsZUluZm8pXG4gICAgaWYgKGVsdEJlZm9yZU5ld0NvbnRlbnQgPT0gbnVsbCkge1xuICAgICAgbmV3RWx0ID0gcGFyZW50Tm9kZS5maXJzdENoaWxkXG4gICAgfSBlbHNlIHtcbiAgICAgIG5ld0VsdCA9IGVsdEJlZm9yZU5ld0NvbnRlbnQubmV4dFNpYmxpbmdcbiAgICB9XG4gICAgc2V0dGxlSW5mby5lbHRzID0gc2V0dGxlSW5mby5lbHRzLmZpbHRlcihmdW5jdGlvbihlKSB7IHJldHVybiBlICE9PSB0YXJnZXQgfSlcbiAgICAvLyBzY2FuIHRocm91Z2ggYWxsIG5ld2x5IGFkZGVkIGNvbnRlbnQgYW5kIGFkZCBhbGwgZWxlbWVudHMgdG8gdGhlIHNldHRsZSBpbmZvIHNvIHdlIHRyaWdnZXJcbiAgICAvLyBldmVudHMgcHJvcGVybHkgb24gdGhlbVxuICAgIHdoaWxlIChuZXdFbHQgJiYgbmV3RWx0ICE9PSB0YXJnZXQpIHtcbiAgICAgIGlmIChuZXdFbHQgaW5zdGFuY2VvZiBFbGVtZW50KSB7XG4gICAgICAgIHNldHRsZUluZm8uZWx0cy5wdXNoKG5ld0VsdClcbiAgICAgIH1cbiAgICAgIG5ld0VsdCA9IG5ld0VsdC5uZXh0U2libGluZ1xuICAgIH1cbiAgICBjbGVhblVwRWxlbWVudCh0YXJnZXQpXG4gICAgaWYgKHRhcmdldCBpbnN0YW5jZW9mIEVsZW1lbnQpIHtcbiAgICAgIHRhcmdldC5yZW1vdmUoKVxuICAgIH0gZWxzZSB7XG4gICAgICB0YXJnZXQucGFyZW50Tm9kZS5yZW1vdmVDaGlsZCh0YXJnZXQpXG4gICAgfVxuICB9XG5cbiAgLyoqXG4gICAqIEBwYXJhbSB7Tm9kZX0gdGFyZ2V0XG4gICAqIEBwYXJhbSB7UGFyZW50Tm9kZX0gZnJhZ21lbnRcbiAgICogQHBhcmFtIHtIdG14U2V0dGxlSW5mb30gc2V0dGxlSW5mb1xuICAgKi9cbiAgZnVuY3Rpb24gc3dhcEFmdGVyQmVnaW4odGFyZ2V0LCBmcmFnbWVudCwgc2V0dGxlSW5mbykge1xuICAgIHJldHVybiBpbnNlcnROb2Rlc0JlZm9yZSh0YXJnZXQsIHRhcmdldC5maXJzdENoaWxkLCBmcmFnbWVudCwgc2V0dGxlSW5mbylcbiAgfVxuXG4gIC8qKlxuICAgKiBAcGFyYW0ge05vZGV9IHRhcmdldFxuICAgKiBAcGFyYW0ge1BhcmVudE5vZGV9IGZyYWdtZW50XG4gICAqIEBwYXJhbSB7SHRteFNldHRsZUluZm99IHNldHRsZUluZm9cbiAgICovXG4gIGZ1bmN0aW9uIHN3YXBCZWZvcmVCZWdpbih0YXJnZXQsIGZyYWdtZW50LCBzZXR0bGVJbmZvKSB7XG4gICAgcmV0dXJuIGluc2VydE5vZGVzQmVmb3JlKHBhcmVudEVsdCh0YXJnZXQpLCB0YXJnZXQsIGZyYWdtZW50LCBzZXR0bGVJbmZvKVxuICB9XG5cbiAgLyoqXG4gICAqIEBwYXJhbSB7Tm9kZX0gdGFyZ2V0XG4gICAqIEBwYXJhbSB7UGFyZW50Tm9kZX0gZnJhZ21lbnRcbiAgICogQHBhcmFtIHtIdG14U2V0dGxlSW5mb30gc2V0dGxlSW5mb1xuICAgKi9cbiAgZnVuY3Rpb24gc3dhcEJlZm9yZUVuZCh0YXJnZXQsIGZyYWdtZW50LCBzZXR0bGVJbmZvKSB7XG4gICAgcmV0dXJuIGluc2VydE5vZGVzQmVmb3JlKHRhcmdldCwgbnVsbCwgZnJhZ21lbnQsIHNldHRsZUluZm8pXG4gIH1cblxuICAvKipcbiAgICogQHBhcmFtIHtOb2RlfSB0YXJnZXRcbiAgICogQHBhcmFtIHtQYXJlbnROb2RlfSBmcmFnbWVudFxuICAgKiBAcGFyYW0ge0h0bXhTZXR0bGVJbmZvfSBzZXR0bGVJbmZvXG4gICAqL1xuICBmdW5jdGlvbiBzd2FwQWZ0ZXJFbmQodGFyZ2V0LCBmcmFnbWVudCwgc2V0dGxlSW5mbykge1xuICAgIHJldHVybiBpbnNlcnROb2Rlc0JlZm9yZShwYXJlbnRFbHQodGFyZ2V0KSwgdGFyZ2V0Lm5leHRTaWJsaW5nLCBmcmFnbWVudCwgc2V0dGxlSW5mbylcbiAgfVxuXG4gIC8qKlxuICAgKiBAcGFyYW0ge05vZGV9IHRhcmdldFxuICAgKi9cbiAgZnVuY3Rpb24gc3dhcERlbGV0ZSh0YXJnZXQpIHtcbiAgICBjbGVhblVwRWxlbWVudCh0YXJnZXQpXG4gICAgY29uc3QgcGFyZW50ID0gcGFyZW50RWx0KHRhcmdldClcbiAgICBpZiAocGFyZW50KSB7XG4gICAgICByZXR1cm4gcGFyZW50LnJlbW92ZUNoaWxkKHRhcmdldClcbiAgICB9XG4gIH1cblxuICAvKipcbiAgICogQHBhcmFtIHtOb2RlfSB0YXJnZXRcbiAgICogQHBhcmFtIHtQYXJlbnROb2RlfSBmcmFnbWVudFxuICAgKiBAcGFyYW0ge0h0bXhTZXR0bGVJbmZvfSBzZXR0bGVJbmZvXG4gICAqL1xuICBmdW5jdGlvbiBzd2FwSW5uZXJIVE1MKHRhcmdldCwgZnJhZ21lbnQsIHNldHRsZUluZm8pIHtcbiAgICBjb25zdCBmaXJzdENoaWxkID0gdGFyZ2V0LmZpcnN0Q2hpbGRcbiAgICBpbnNlcnROb2Rlc0JlZm9yZSh0YXJnZXQsIGZpcnN0Q2hpbGQsIGZyYWdtZW50LCBzZXR0bGVJbmZvKVxuICAgIGlmIChmaXJzdENoaWxkKSB7XG4gICAgICB3aGlsZSAoZmlyc3RDaGlsZC5uZXh0U2libGluZykge1xuICAgICAgICBjbGVhblVwRWxlbWVudChmaXJzdENoaWxkLm5leHRTaWJsaW5nKVxuICAgICAgICB0YXJnZXQucmVtb3ZlQ2hpbGQoZmlyc3RDaGlsZC5uZXh0U2libGluZylcbiAgICAgIH1cbiAgICAgIGNsZWFuVXBFbGVtZW50KGZpcnN0Q2hpbGQpXG4gICAgICB0YXJnZXQucmVtb3ZlQ2hpbGQoZmlyc3RDaGlsZClcbiAgICB9XG4gIH1cblxuICAvKipcbiAgICogQHBhcmFtIHtIdG14U3dhcFN0eWxlfSBzd2FwU3R5bGVcbiAgICogQHBhcmFtIHtFbGVtZW50fSBlbHRcbiAgICogQHBhcmFtIHtOb2RlfSB0YXJnZXRcbiAgICogQHBhcmFtIHtQYXJlbnROb2RlfSBmcmFnbWVudFxuICAgKiBAcGFyYW0ge0h0bXhTZXR0bGVJbmZvfSBzZXR0bGVJbmZvXG4gICAqL1xuICBmdW5jdGlvbiBzd2FwV2l0aFN0eWxlKHN3YXBTdHlsZSwgZWx0LCB0YXJnZXQsIGZyYWdtZW50LCBzZXR0bGVJbmZvKSB7XG4gICAgc3dpdGNoIChzd2FwU3R5bGUpIHtcbiAgICAgIGNhc2UgJ25vbmUnOlxuICAgICAgICByZXR1cm5cbiAgICAgIGNhc2UgJ291dGVySFRNTCc6XG4gICAgICAgIHN3YXBPdXRlckhUTUwodGFyZ2V0LCBmcmFnbWVudCwgc2V0dGxlSW5mbylcbiAgICAgICAgcmV0dXJuXG4gICAgICBjYXNlICdhZnRlcmJlZ2luJzpcbiAgICAgICAgc3dhcEFmdGVyQmVnaW4odGFyZ2V0LCBmcmFnbWVudCwgc2V0dGxlSW5mbylcbiAgICAgICAgcmV0dXJuXG4gICAgICBjYXNlICdiZWZvcmViZWdpbic6XG4gICAgICAgIHN3YXBCZWZvcmVCZWdpbih0YXJnZXQsIGZyYWdtZW50LCBzZXR0bGVJbmZvKVxuICAgICAgICByZXR1cm5cbiAgICAgIGNhc2UgJ2JlZm9yZWVuZCc6XG4gICAgICAgIHN3YXBCZWZvcmVFbmQodGFyZ2V0LCBmcmFnbWVudCwgc2V0dGxlSW5mbylcbiAgICAgICAgcmV0dXJuXG4gICAgICBjYXNlICdhZnRlcmVuZCc6XG4gICAgICAgIHN3YXBBZnRlckVuZCh0YXJnZXQsIGZyYWdtZW50LCBzZXR0bGVJbmZvKVxuICAgICAgICByZXR1cm5cbiAgICAgIGNhc2UgJ2RlbGV0ZSc6XG4gICAgICAgIHN3YXBEZWxldGUodGFyZ2V0KVxuICAgICAgICByZXR1cm5cbiAgICAgIGRlZmF1bHQ6XG4gICAgICAgIHZhciBleHRlbnNpb25zID0gZ2V0RXh0ZW5zaW9ucyhlbHQpXG4gICAgICAgIGZvciAobGV0IGkgPSAwOyBpIDwgZXh0ZW5zaW9ucy5sZW5ndGg7IGkrKykge1xuICAgICAgICAgIGNvbnN0IGV4dCA9IGV4dGVuc2lvbnNbaV1cbiAgICAgICAgICB0cnkge1xuICAgICAgICAgICAgY29uc3QgbmV3RWxlbWVudHMgPSBleHQuaGFuZGxlU3dhcChzd2FwU3R5bGUsIHRhcmdldCwgZnJhZ21lbnQsIHNldHRsZUluZm8pXG4gICAgICAgICAgICBpZiAobmV3RWxlbWVudHMpIHtcbiAgICAgICAgICAgICAgaWYgKEFycmF5LmlzQXJyYXkobmV3RWxlbWVudHMpKSB7XG4gICAgICAgICAgICAgICAgLy8gaWYgaGFuZGxlU3dhcCByZXR1cm5zIGFuIGFycmF5IChsaWtlKSBvZiBlbGVtZW50cywgd2UgaGFuZGxlIHRoZW1cbiAgICAgICAgICAgICAgICBmb3IgKGxldCBqID0gMDsgaiA8IG5ld0VsZW1lbnRzLmxlbmd0aDsgaisrKSB7XG4gICAgICAgICAgICAgICAgICBjb25zdCBjaGlsZCA9IG5ld0VsZW1lbnRzW2pdXG4gICAgICAgICAgICAgICAgICBpZiAoY2hpbGQubm9kZVR5cGUgIT09IE5vZGUuVEVYVF9OT0RFICYmIGNoaWxkLm5vZGVUeXBlICE9PSBOb2RlLkNPTU1FTlRfTk9ERSkge1xuICAgICAgICAgICAgICAgICAgICBzZXR0bGVJbmZvLnRhc2tzLnB1c2gobWFrZUFqYXhMb2FkVGFzayhjaGlsZCkpXG4gICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgIHJldHVyblxuICAgICAgICAgICAgfVxuICAgICAgICAgIH0gY2F0Y2ggKGUpIHtcbiAgICAgICAgICAgIGxvZ0Vycm9yKGUpXG4gICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICAgIGlmIChzd2FwU3R5bGUgPT09ICdpbm5lckhUTUwnKSB7XG4gICAgICAgICAgc3dhcElubmVySFRNTCh0YXJnZXQsIGZyYWdtZW50LCBzZXR0bGVJbmZvKVxuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgIHN3YXBXaXRoU3R5bGUoaHRteC5jb25maWcuZGVmYXVsdFN3YXBTdHlsZSwgZWx0LCB0YXJnZXQsIGZyYWdtZW50LCBzZXR0bGVJbmZvKVxuICAgICAgICB9XG4gICAgfVxuICB9XG5cbiAgLyoqXG4gICAqIEBwYXJhbSB7RG9jdW1lbnRGcmFnbWVudH0gZnJhZ21lbnRcbiAgICogQHBhcmFtIHtIdG14U2V0dGxlSW5mb30gc2V0dGxlSW5mb1xuICAgKiBAcGFyYW0ge05vZGV8RG9jdW1lbnR9IFtyb290Tm9kZV1cbiAgICovXG4gIGZ1bmN0aW9uIGZpbmRBbmRTd2FwT29iRWxlbWVudHMoZnJhZ21lbnQsIHNldHRsZUluZm8sIHJvb3ROb2RlKSB7XG4gICAgdmFyIG9vYkVsdHMgPSBmaW5kQWxsKGZyYWdtZW50LCAnW2h4LXN3YXAtb29iXSwgW2RhdGEtaHgtc3dhcC1vb2JdJylcbiAgICBmb3JFYWNoKG9vYkVsdHMsIGZ1bmN0aW9uKG9vYkVsZW1lbnQpIHtcbiAgICAgIGlmIChodG14LmNvbmZpZy5hbGxvd05lc3RlZE9vYlN3YXBzIHx8IG9vYkVsZW1lbnQucGFyZW50RWxlbWVudCA9PT0gbnVsbCkge1xuICAgICAgICBjb25zdCBvb2JWYWx1ZSA9IGdldEF0dHJpYnV0ZVZhbHVlKG9vYkVsZW1lbnQsICdoeC1zd2FwLW9vYicpXG4gICAgICAgIGlmIChvb2JWYWx1ZSAhPSBudWxsKSB7XG4gICAgICAgICAgb29iU3dhcChvb2JWYWx1ZSwgb29iRWxlbWVudCwgc2V0dGxlSW5mbywgcm9vdE5vZGUpXG4gICAgICAgIH1cbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIG9vYkVsZW1lbnQucmVtb3ZlQXR0cmlidXRlKCdoeC1zd2FwLW9vYicpXG4gICAgICAgIG9vYkVsZW1lbnQucmVtb3ZlQXR0cmlidXRlKCdkYXRhLWh4LXN3YXAtb29iJylcbiAgICAgIH1cbiAgICB9KVxuICAgIHJldHVybiBvb2JFbHRzLmxlbmd0aCA+IDBcbiAgfVxuXG4gIC8qKlxuICAgKiBJbXBsZW1lbnRzIGNvbXBsZXRlIHN3YXBwaW5nIHBpcGVsaW5lLCBpbmNsdWRpbmc6IGZvY3VzIGFuZCBzZWxlY3Rpb24gcHJlc2VydmF0aW9uLFxuICAgKiB0aXRsZSB1cGRhdGVzLCBzY3JvbGwsIE9PQiBzd2FwcGluZywgbm9ybWFsIHN3YXBwaW5nIGFuZCBzZXR0bGluZ1xuICAgKiBAcGFyYW0ge3N0cmluZ3xFbGVtZW50fSB0YXJnZXRcbiAgICogQHBhcmFtIHtzdHJpbmd9IGNvbnRlbnRcbiAgICogQHBhcmFtIHtIdG14U3dhcFNwZWNpZmljYXRpb259IHN3YXBTcGVjXG4gICAqIEBwYXJhbSB7U3dhcE9wdGlvbnN9IFtzd2FwT3B0aW9uc11cbiAgICovXG4gIGZ1bmN0aW9uIHN3YXAodGFyZ2V0LCBjb250ZW50LCBzd2FwU3BlYywgc3dhcE9wdGlvbnMpIHtcbiAgICBpZiAoIXN3YXBPcHRpb25zKSB7XG4gICAgICBzd2FwT3B0aW9ucyA9IHt9XG4gICAgfVxuXG4gICAgdGFyZ2V0ID0gcmVzb2x2ZVRhcmdldCh0YXJnZXQpXG4gICAgY29uc3Qgcm9vdE5vZGUgPSBzd2FwT3B0aW9ucy5jb250ZXh0RWxlbWVudCA/IGdldFJvb3ROb2RlKHN3YXBPcHRpb25zLmNvbnRleHRFbGVtZW50LCBmYWxzZSkgOiBnZXREb2N1bWVudCgpXG5cbiAgICAvLyBwcmVzZXJ2ZSBmb2N1cyBhbmQgc2VsZWN0aW9uXG4gICAgY29uc3QgYWN0aXZlRWx0ID0gZG9jdW1lbnQuYWN0aXZlRWxlbWVudFxuICAgIGxldCBzZWxlY3Rpb25JbmZvID0ge31cbiAgICB0cnkge1xuICAgICAgc2VsZWN0aW9uSW5mbyA9IHtcbiAgICAgICAgZWx0OiBhY3RpdmVFbHQsXG4gICAgICAgIC8vIEB0cy1pZ25vcmVcbiAgICAgICAgc3RhcnQ6IGFjdGl2ZUVsdCA/IGFjdGl2ZUVsdC5zZWxlY3Rpb25TdGFydCA6IG51bGwsXG4gICAgICAgIC8vIEB0cy1pZ25vcmVcbiAgICAgICAgZW5kOiBhY3RpdmVFbHQgPyBhY3RpdmVFbHQuc2VsZWN0aW9uRW5kIDogbnVsbFxuICAgICAgfVxuICAgIH0gY2F0Y2ggKGUpIHtcbiAgICAgIC8vIHNhZmFyaSBpc3N1ZSAtIHNlZSBodHRwczovL2dpdGh1Yi5jb20vbWljcm9zb2Z0L3BsYXl3cmlnaHQvaXNzdWVzLzU4OTRcbiAgICB9XG4gICAgY29uc3Qgc2V0dGxlSW5mbyA9IG1ha2VTZXR0bGVJbmZvKHRhcmdldClcblxuICAgIC8vIEZvciB0ZXh0IGNvbnRlbnQgc3dhcHMsIGRvbid0IHBhcnNlIHRoZSByZXNwb25zZSBhcyBIVE1MLCBqdXN0IGluc2VydCBpdFxuICAgIGlmIChzd2FwU3BlYy5zd2FwU3R5bGUgPT09ICd0ZXh0Q29udGVudCcpIHtcbiAgICAgIHRhcmdldC50ZXh0Q29udGVudCA9IGNvbnRlbnRcbiAgICAvLyBPdGhlcndpc2UsIG1ha2UgdGhlIGZyYWdtZW50IGFuZCBwcm9jZXNzIGl0XG4gICAgfSBlbHNlIHtcbiAgICAgIGxldCBmcmFnbWVudCA9IG1ha2VGcmFnbWVudChjb250ZW50KVxuXG4gICAgICBzZXR0bGVJbmZvLnRpdGxlID0gZnJhZ21lbnQudGl0bGVcblxuICAgICAgLy8gc2VsZWN0LW9vYiBzd2Fwc1xuICAgICAgaWYgKHN3YXBPcHRpb25zLnNlbGVjdE9PQikge1xuICAgICAgICBjb25zdCBvb2JTZWxlY3RWYWx1ZXMgPSBzd2FwT3B0aW9ucy5zZWxlY3RPT0Iuc3BsaXQoJywnKVxuICAgICAgICBmb3IgKGxldCBpID0gMDsgaSA8IG9vYlNlbGVjdFZhbHVlcy5sZW5ndGg7IGkrKykge1xuICAgICAgICAgIGNvbnN0IG9vYlNlbGVjdFZhbHVlID0gb29iU2VsZWN0VmFsdWVzW2ldLnNwbGl0KCc6JywgMilcbiAgICAgICAgICBsZXQgaWQgPSBvb2JTZWxlY3RWYWx1ZVswXS50cmltKClcbiAgICAgICAgICBpZiAoaWQuaW5kZXhPZignIycpID09PSAwKSB7XG4gICAgICAgICAgICBpZCA9IGlkLnN1YnN0cmluZygxKVxuICAgICAgICAgIH1cbiAgICAgICAgICBjb25zdCBvb2JWYWx1ZSA9IG9vYlNlbGVjdFZhbHVlWzFdIHx8ICd0cnVlJ1xuICAgICAgICAgIGNvbnN0IG9vYkVsZW1lbnQgPSBmcmFnbWVudC5xdWVyeVNlbGVjdG9yKCcjJyArIGlkKVxuICAgICAgICAgIGlmIChvb2JFbGVtZW50KSB7XG4gICAgICAgICAgICBvb2JTd2FwKG9vYlZhbHVlLCBvb2JFbGVtZW50LCBzZXR0bGVJbmZvLCByb290Tm9kZSlcbiAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICAgIC8vIG9vYiBzd2Fwc1xuICAgICAgZmluZEFuZFN3YXBPb2JFbGVtZW50cyhmcmFnbWVudCwgc2V0dGxlSW5mbywgcm9vdE5vZGUpXG4gICAgICBmb3JFYWNoKGZpbmRBbGwoZnJhZ21lbnQsICd0ZW1wbGF0ZScpLCAvKiogQHBhcmFtIHtIVE1MVGVtcGxhdGVFbGVtZW50fSB0ZW1wbGF0ZSAqL2Z1bmN0aW9uKHRlbXBsYXRlKSB7XG4gICAgICAgIGlmICh0ZW1wbGF0ZS5jb250ZW50ICYmIGZpbmRBbmRTd2FwT29iRWxlbWVudHModGVtcGxhdGUuY29udGVudCwgc2V0dGxlSW5mbywgcm9vdE5vZGUpKSB7XG4gICAgICAgICAgLy8gQXZvaWQgcG9sbHV0aW5nIHRoZSBET00gd2l0aCBlbXB0eSB0ZW1wbGF0ZXMgdGhhdCB3ZXJlIG9ubHkgdXNlZCB0byBlbmNhcHN1bGF0ZSBvb2Igc3dhcFxuICAgICAgICAgIHRlbXBsYXRlLnJlbW92ZSgpXG4gICAgICAgIH1cbiAgICAgIH0pXG5cbiAgICAgIC8vIG5vcm1hbCBzd2FwXG4gICAgICBpZiAoc3dhcE9wdGlvbnMuc2VsZWN0KSB7XG4gICAgICAgIGNvbnN0IG5ld0ZyYWdtZW50ID0gZ2V0RG9jdW1lbnQoKS5jcmVhdGVEb2N1bWVudEZyYWdtZW50KClcbiAgICAgICAgZm9yRWFjaChmcmFnbWVudC5xdWVyeVNlbGVjdG9yQWxsKHN3YXBPcHRpb25zLnNlbGVjdCksIGZ1bmN0aW9uKG5vZGUpIHtcbiAgICAgICAgICBuZXdGcmFnbWVudC5hcHBlbmRDaGlsZChub2RlKVxuICAgICAgICB9KVxuICAgICAgICBmcmFnbWVudCA9IG5ld0ZyYWdtZW50XG4gICAgICB9XG4gICAgICBoYW5kbGVQcmVzZXJ2ZWRFbGVtZW50cyhmcmFnbWVudClcbiAgICAgIHN3YXBXaXRoU3R5bGUoc3dhcFNwZWMuc3dhcFN0eWxlLCBzd2FwT3B0aW9ucy5jb250ZXh0RWxlbWVudCwgdGFyZ2V0LCBmcmFnbWVudCwgc2V0dGxlSW5mbylcbiAgICAgIHJlc3RvcmVQcmVzZXJ2ZWRFbGVtZW50cygpXG4gICAgfVxuXG4gICAgLy8gYXBwbHkgc2F2ZWQgZm9jdXMgYW5kIHNlbGVjdGlvbiBpbmZvcm1hdGlvbiB0byBzd2FwcGVkIGNvbnRlbnRcbiAgICBpZiAoc2VsZWN0aW9uSW5mby5lbHQgJiZcbiAgICAgICFib2R5Q29udGFpbnMoc2VsZWN0aW9uSW5mby5lbHQpICYmXG4gICAgICBnZXRSYXdBdHRyaWJ1dGUoc2VsZWN0aW9uSW5mby5lbHQsICdpZCcpKSB7XG4gICAgICBjb25zdCBuZXdBY3RpdmVFbHQgPSBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChnZXRSYXdBdHRyaWJ1dGUoc2VsZWN0aW9uSW5mby5lbHQsICdpZCcpKVxuICAgICAgY29uc3QgZm9jdXNPcHRpb25zID0geyBwcmV2ZW50U2Nyb2xsOiBzd2FwU3BlYy5mb2N1c1Njcm9sbCAhPT0gdW5kZWZpbmVkID8gIXN3YXBTcGVjLmZvY3VzU2Nyb2xsIDogIWh0bXguY29uZmlnLmRlZmF1bHRGb2N1c1Njcm9sbCB9XG4gICAgICBpZiAobmV3QWN0aXZlRWx0KSB7XG4gICAgICAgIC8vIEB0cy1pZ25vcmVcbiAgICAgICAgaWYgKHNlbGVjdGlvbkluZm8uc3RhcnQgJiYgbmV3QWN0aXZlRWx0LnNldFNlbGVjdGlvblJhbmdlKSB7XG4gICAgICAgICAgdHJ5IHtcbiAgICAgICAgICAgIC8vIEB0cy1pZ25vcmVcbiAgICAgICAgICAgIG5ld0FjdGl2ZUVsdC5zZXRTZWxlY3Rpb25SYW5nZShzZWxlY3Rpb25JbmZvLnN0YXJ0LCBzZWxlY3Rpb25JbmZvLmVuZClcbiAgICAgICAgICB9IGNhdGNoIChlKSB7XG4gICAgICAgICAgICAvLyB0aGUgc2V0U2VsZWN0aW9uUmFuZ2UgbWV0aG9kIGlzIHByZXNlbnQgb24gZmllbGRzIHRoYXQgZG9uJ3Qgc3VwcG9ydCBpdCwgc28ganVzdCBsZXQgdGhpcyBmYWlsXG4gICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICAgIG5ld0FjdGl2ZUVsdC5mb2N1cyhmb2N1c09wdGlvbnMpXG4gICAgICB9XG4gICAgfVxuXG4gICAgdGFyZ2V0LmNsYXNzTGlzdC5yZW1vdmUoaHRteC5jb25maWcuc3dhcHBpbmdDbGFzcylcbiAgICBmb3JFYWNoKHNldHRsZUluZm8uZWx0cywgZnVuY3Rpb24oZWx0KSB7XG4gICAgICBpZiAoZWx0LmNsYXNzTGlzdCkge1xuICAgICAgICBlbHQuY2xhc3NMaXN0LmFkZChodG14LmNvbmZpZy5zZXR0bGluZ0NsYXNzKVxuICAgICAgfVxuICAgICAgdHJpZ2dlckV2ZW50KGVsdCwgJ2h0bXg6YWZ0ZXJTd2FwJywgc3dhcE9wdGlvbnMuZXZlbnRJbmZvKVxuICAgIH0pXG4gICAgaWYgKHN3YXBPcHRpb25zLmFmdGVyU3dhcENhbGxiYWNrKSB7XG4gICAgICBzd2FwT3B0aW9ucy5hZnRlclN3YXBDYWxsYmFjaygpXG4gICAgfVxuXG4gICAgLy8gbWVyZ2UgaW4gbmV3IHRpdGxlIGFmdGVyIHN3YXAgYnV0IGJlZm9yZSBzZXR0bGVcbiAgICBpZiAoIXN3YXBTcGVjLmlnbm9yZVRpdGxlKSB7XG4gICAgICBoYW5kbGVUaXRsZShzZXR0bGVJbmZvLnRpdGxlKVxuICAgIH1cblxuICAgIC8vIHNldHRsZVxuICAgIGNvbnN0IGRvU2V0dGxlID0gZnVuY3Rpb24oKSB7XG4gICAgICBmb3JFYWNoKHNldHRsZUluZm8udGFza3MsIGZ1bmN0aW9uKHRhc2spIHtcbiAgICAgICAgdGFzay5jYWxsKClcbiAgICAgIH0pXG4gICAgICBmb3JFYWNoKHNldHRsZUluZm8uZWx0cywgZnVuY3Rpb24oZWx0KSB7XG4gICAgICAgIGlmIChlbHQuY2xhc3NMaXN0KSB7XG4gICAgICAgICAgZWx0LmNsYXNzTGlzdC5yZW1vdmUoaHRteC5jb25maWcuc2V0dGxpbmdDbGFzcylcbiAgICAgICAgfVxuICAgICAgICB0cmlnZ2VyRXZlbnQoZWx0LCAnaHRteDphZnRlclNldHRsZScsIHN3YXBPcHRpb25zLmV2ZW50SW5mbylcbiAgICAgIH0pXG5cbiAgICAgIGlmIChzd2FwT3B0aW9ucy5hbmNob3IpIHtcbiAgICAgICAgY29uc3QgYW5jaG9yVGFyZ2V0ID0gYXNFbGVtZW50KHJlc29sdmVUYXJnZXQoJyMnICsgc3dhcE9wdGlvbnMuYW5jaG9yKSlcbiAgICAgICAgaWYgKGFuY2hvclRhcmdldCkge1xuICAgICAgICAgIGFuY2hvclRhcmdldC5zY3JvbGxJbnRvVmlldyh7IGJsb2NrOiAnc3RhcnQnLCBiZWhhdmlvcjogJ2F1dG8nIH0pXG4gICAgICAgIH1cbiAgICAgIH1cblxuICAgICAgdXBkYXRlU2Nyb2xsU3RhdGUoc2V0dGxlSW5mby5lbHRzLCBzd2FwU3BlYylcbiAgICAgIGlmIChzd2FwT3B0aW9ucy5hZnRlclNldHRsZUNhbGxiYWNrKSB7XG4gICAgICAgIHN3YXBPcHRpb25zLmFmdGVyU2V0dGxlQ2FsbGJhY2soKVxuICAgICAgfVxuICAgIH1cblxuICAgIGlmIChzd2FwU3BlYy5zZXR0bGVEZWxheSA+IDApIHtcbiAgICAgIGdldFdpbmRvdygpLnNldFRpbWVvdXQoZG9TZXR0bGUsIHN3YXBTcGVjLnNldHRsZURlbGF5KVxuICAgIH0gZWxzZSB7XG4gICAgICBkb1NldHRsZSgpXG4gICAgfVxuICB9XG5cbiAgLyoqXG4gICAqIEBwYXJhbSB7WE1MSHR0cFJlcXVlc3R9IHhoclxuICAgKiBAcGFyYW0ge3N0cmluZ30gaGVhZGVyXG4gICAqIEBwYXJhbSB7RXZlbnRUYXJnZXR9IGVsdFxuICAgKi9cbiAgZnVuY3Rpb24gaGFuZGxlVHJpZ2dlckhlYWRlcih4aHIsIGhlYWRlciwgZWx0KSB7XG4gICAgY29uc3QgdHJpZ2dlckJvZHkgPSB4aHIuZ2V0UmVzcG9uc2VIZWFkZXIoaGVhZGVyKVxuICAgIGlmICh0cmlnZ2VyQm9keS5pbmRleE9mKCd7JykgPT09IDApIHtcbiAgICAgIGNvbnN0IHRyaWdnZXJzID0gcGFyc2VKU09OKHRyaWdnZXJCb2R5KVxuICAgICAgZm9yIChjb25zdCBldmVudE5hbWUgaW4gdHJpZ2dlcnMpIHtcbiAgICAgICAgaWYgKHRyaWdnZXJzLmhhc093blByb3BlcnR5KGV2ZW50TmFtZSkpIHtcbiAgICAgICAgICBsZXQgZGV0YWlsID0gdHJpZ2dlcnNbZXZlbnROYW1lXVxuICAgICAgICAgIGlmIChpc1Jhd09iamVjdChkZXRhaWwpKSB7XG4gICAgICAgICAgICAvLyBAdHMtaWdub3JlXG4gICAgICAgICAgICBlbHQgPSBkZXRhaWwudGFyZ2V0ICE9PSB1bmRlZmluZWQgPyBkZXRhaWwudGFyZ2V0IDogZWx0XG4gICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIGRldGFpbCA9IHsgdmFsdWU6IGRldGFpbCB9XG4gICAgICAgICAgfVxuICAgICAgICAgIHRyaWdnZXJFdmVudChlbHQsIGV2ZW50TmFtZSwgZGV0YWlsKVxuICAgICAgICB9XG4gICAgICB9XG4gICAgfSBlbHNlIHtcbiAgICAgIGNvbnN0IGV2ZW50TmFtZXMgPSB0cmlnZ2VyQm9keS5zcGxpdCgnLCcpXG4gICAgICBmb3IgKGxldCBpID0gMDsgaSA8IGV2ZW50TmFtZXMubGVuZ3RoOyBpKyspIHtcbiAgICAgICAgdHJpZ2dlckV2ZW50KGVsdCwgZXZlbnROYW1lc1tpXS50cmltKCksIFtdKVxuICAgICAgfVxuICAgIH1cbiAgfVxuXG4gIGNvbnN0IFdISVRFU1BBQ0UgPSAvXFxzL1xuICBjb25zdCBXSElURVNQQUNFX09SX0NPTU1BID0gL1tcXHMsXS9cbiAgY29uc3QgU1lNQk9MX1NUQVJUID0gL1tfJGEtekEtWl0vXG4gIGNvbnN0IFNZTUJPTF9DT05UID0gL1tfJGEtekEtWjAtOV0vXG4gIGNvbnN0IFNUUklOR0lTSF9TVEFSVCA9IFsnXCInLCBcIidcIiwgJy8nXVxuICBjb25zdCBOT1RfV0hJVEVTUEFDRSA9IC9bXlxcc10vXG4gIGNvbnN0IENPTUJJTkVEX1NFTEVDVE9SX1NUQVJUID0gL1t7KF0vXG4gIGNvbnN0IENPTUJJTkVEX1NFTEVDVE9SX0VORCA9IC9bfSldL1xuXG4gIC8qKlxuICAgKiBAcGFyYW0ge3N0cmluZ30gc3RyXG4gICAqIEByZXR1cm5zIHtzdHJpbmdbXX1cbiAgICovXG4gIGZ1bmN0aW9uIHRva2VuaXplU3RyaW5nKHN0cikge1xuICAgIC8qKiBAdHlwZSBzdHJpbmdbXSAqL1xuICAgIGNvbnN0IHRva2VucyA9IFtdXG4gICAgbGV0IHBvc2l0aW9uID0gMFxuICAgIHdoaWxlIChwb3NpdGlvbiA8IHN0ci5sZW5ndGgpIHtcbiAgICAgIGlmIChTWU1CT0xfU1RBUlQuZXhlYyhzdHIuY2hhckF0KHBvc2l0aW9uKSkpIHtcbiAgICAgICAgdmFyIHN0YXJ0UG9zaXRpb24gPSBwb3NpdGlvblxuICAgICAgICB3aGlsZSAoU1lNQk9MX0NPTlQuZXhlYyhzdHIuY2hhckF0KHBvc2l0aW9uICsgMSkpKSB7XG4gICAgICAgICAgcG9zaXRpb24rK1xuICAgICAgICB9XG4gICAgICAgIHRva2Vucy5wdXNoKHN0ci5zdWJzdHJpbmcoc3RhcnRQb3NpdGlvbiwgcG9zaXRpb24gKyAxKSlcbiAgICAgIH0gZWxzZSBpZiAoU1RSSU5HSVNIX1NUQVJULmluZGV4T2Yoc3RyLmNoYXJBdChwb3NpdGlvbikpICE9PSAtMSkge1xuICAgICAgICBjb25zdCBzdGFydENoYXIgPSBzdHIuY2hhckF0KHBvc2l0aW9uKVxuICAgICAgICB2YXIgc3RhcnRQb3NpdGlvbiA9IHBvc2l0aW9uXG4gICAgICAgIHBvc2l0aW9uKytcbiAgICAgICAgd2hpbGUgKHBvc2l0aW9uIDwgc3RyLmxlbmd0aCAmJiBzdHIuY2hhckF0KHBvc2l0aW9uKSAhPT0gc3RhcnRDaGFyKSB7XG4gICAgICAgICAgaWYgKHN0ci5jaGFyQXQocG9zaXRpb24pID09PSAnXFxcXCcpIHtcbiAgICAgICAgICAgIHBvc2l0aW9uKytcbiAgICAgICAgICB9XG4gICAgICAgICAgcG9zaXRpb24rK1xuICAgICAgICB9XG4gICAgICAgIHRva2Vucy5wdXNoKHN0ci5zdWJzdHJpbmcoc3RhcnRQb3NpdGlvbiwgcG9zaXRpb24gKyAxKSlcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIGNvbnN0IHN5bWJvbCA9IHN0ci5jaGFyQXQocG9zaXRpb24pXG4gICAgICAgIHRva2Vucy5wdXNoKHN5bWJvbClcbiAgICAgIH1cbiAgICAgIHBvc2l0aW9uKytcbiAgICB9XG4gICAgcmV0dXJuIHRva2Vuc1xuICB9XG5cbiAgLyoqXG4gICAqIEBwYXJhbSB7c3RyaW5nfSB0b2tlblxuICAgKiBAcGFyYW0ge3N0cmluZ3xudWxsfSBsYXN0XG4gICAqIEBwYXJhbSB7c3RyaW5nfSBwYXJhbU5hbWVcbiAgICogQHJldHVybnMge2Jvb2xlYW59XG4gICAqL1xuICBmdW5jdGlvbiBpc1Bvc3NpYmxlUmVsYXRpdmVSZWZlcmVuY2UodG9rZW4sIGxhc3QsIHBhcmFtTmFtZSkge1xuICAgIHJldHVybiBTWU1CT0xfU1RBUlQuZXhlYyh0b2tlbi5jaGFyQXQoMCkpICYmXG4gICAgICB0b2tlbiAhPT0gJ3RydWUnICYmXG4gICAgICB0b2tlbiAhPT0gJ2ZhbHNlJyAmJlxuICAgICAgdG9rZW4gIT09ICd0aGlzJyAmJlxuICAgICAgdG9rZW4gIT09IHBhcmFtTmFtZSAmJlxuICAgICAgbGFzdCAhPT0gJy4nXG4gIH1cblxuICAvKipcbiAgICogQHBhcmFtIHtFdmVudFRhcmdldHxzdHJpbmd9IGVsdFxuICAgKiBAcGFyYW0ge3N0cmluZ1tdfSB0b2tlbnNcbiAgICogQHBhcmFtIHtzdHJpbmd9IHBhcmFtTmFtZVxuICAgKiBAcmV0dXJucyB7Q29uZGl0aW9uYWxGdW5jdGlvbnxudWxsfVxuICAgKi9cbiAgZnVuY3Rpb24gbWF5YmVHZW5lcmF0ZUNvbmRpdGlvbmFsKGVsdCwgdG9rZW5zLCBwYXJhbU5hbWUpIHtcbiAgICBpZiAodG9rZW5zWzBdID09PSAnWycpIHtcbiAgICAgIHRva2Vucy5zaGlmdCgpXG4gICAgICBsZXQgYnJhY2tldENvdW50ID0gMVxuICAgICAgbGV0IGNvbmRpdGlvbmFsU291cmNlID0gJyByZXR1cm4gKGZ1bmN0aW9uKCcgKyBwYXJhbU5hbWUgKyAnKXsgcmV0dXJuICgnXG4gICAgICBsZXQgbGFzdCA9IG51bGxcbiAgICAgIHdoaWxlICh0b2tlbnMubGVuZ3RoID4gMCkge1xuICAgICAgICBjb25zdCB0b2tlbiA9IHRva2Vuc1swXVxuICAgICAgICAvLyBAdHMtaWdub3JlIEZvciBzb21lIHJlYXNvbiB0c2MgZG9lc24ndCB1bmRlcnN0YW5kIHRoZSBzaGlmdCBjYWxsLCBhbmQgdGhpbmtzIHdlJ3JlIGNvbXBhcmluZyB0aGUgc2FtZSB2YWx1ZSBoZXJlLCBpLmUuICdbJyB2cyAnXSdcbiAgICAgICAgaWYgKHRva2VuID09PSAnXScpIHtcbiAgICAgICAgICBicmFja2V0Q291bnQtLVxuICAgICAgICAgIGlmIChicmFja2V0Q291bnQgPT09IDApIHtcbiAgICAgICAgICAgIGlmIChsYXN0ID09PSBudWxsKSB7XG4gICAgICAgICAgICAgIGNvbmRpdGlvbmFsU291cmNlID0gY29uZGl0aW9uYWxTb3VyY2UgKyAndHJ1ZSdcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIHRva2Vucy5zaGlmdCgpXG4gICAgICAgICAgICBjb25kaXRpb25hbFNvdXJjZSArPSAnKX0pJ1xuICAgICAgICAgICAgdHJ5IHtcbiAgICAgICAgICAgICAgY29uc3QgY29uZGl0aW9uRnVuY3Rpb24gPSBtYXliZUV2YWwoZWx0LCBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgICAgICByZXR1cm4gRnVuY3Rpb24oY29uZGl0aW9uYWxTb3VyY2UpKClcbiAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgZnVuY3Rpb24oKSB7IHJldHVybiB0cnVlIH0pXG4gICAgICAgICAgICAgIGNvbmRpdGlvbkZ1bmN0aW9uLnNvdXJjZSA9IGNvbmRpdGlvbmFsU291cmNlXG4gICAgICAgICAgICAgIHJldHVybiBjb25kaXRpb25GdW5jdGlvblxuICAgICAgICAgICAgfSBjYXRjaCAoZSkge1xuICAgICAgICAgICAgICB0cmlnZ2VyRXJyb3JFdmVudChnZXREb2N1bWVudCgpLmJvZHksICdodG14OnN5bnRheDplcnJvcicsIHsgZXJyb3I6IGUsIHNvdXJjZTogY29uZGl0aW9uYWxTb3VyY2UgfSlcbiAgICAgICAgICAgICAgcmV0dXJuIG51bGxcbiAgICAgICAgICAgIH1cbiAgICAgICAgICB9XG4gICAgICAgIH0gZWxzZSBpZiAodG9rZW4gPT09ICdbJykge1xuICAgICAgICAgIGJyYWNrZXRDb3VudCsrXG4gICAgICAgIH1cbiAgICAgICAgaWYgKGlzUG9zc2libGVSZWxhdGl2ZVJlZmVyZW5jZSh0b2tlbiwgbGFzdCwgcGFyYW1OYW1lKSkge1xuICAgICAgICAgIGNvbmRpdGlvbmFsU291cmNlICs9ICcoKCcgKyBwYXJhbU5hbWUgKyAnLicgKyB0b2tlbiArICcpID8gKCcgKyBwYXJhbU5hbWUgKyAnLicgKyB0b2tlbiArICcpIDogKHdpbmRvdy4nICsgdG9rZW4gKyAnKSknXG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgY29uZGl0aW9uYWxTb3VyY2UgPSBjb25kaXRpb25hbFNvdXJjZSArIHRva2VuXG4gICAgICAgIH1cbiAgICAgICAgbGFzdCA9IHRva2Vucy5zaGlmdCgpXG4gICAgICB9XG4gICAgfVxuICB9XG5cbiAgLyoqXG4gICAqIEBwYXJhbSB7c3RyaW5nW119IHRva2Vuc1xuICAgKiBAcGFyYW0ge1JlZ0V4cH0gbWF0Y2hcbiAgICogQHJldHVybnMge3N0cmluZ31cbiAgICovXG4gIGZ1bmN0aW9uIGNvbnN1bWVVbnRpbCh0b2tlbnMsIG1hdGNoKSB7XG4gICAgbGV0IHJlc3VsdCA9ICcnXG4gICAgd2hpbGUgKHRva2Vucy5sZW5ndGggPiAwICYmICFtYXRjaC50ZXN0KHRva2Vuc1swXSkpIHtcbiAgICAgIHJlc3VsdCArPSB0b2tlbnMuc2hpZnQoKVxuICAgIH1cbiAgICByZXR1cm4gcmVzdWx0XG4gIH1cblxuICAvKipcbiAgICogQHBhcmFtIHtzdHJpbmdbXX0gdG9rZW5zXG4gICAqIEByZXR1cm5zIHtzdHJpbmd9XG4gICAqL1xuICBmdW5jdGlvbiBjb25zdW1lQ1NTU2VsZWN0b3IodG9rZW5zKSB7XG4gICAgbGV0IHJlc3VsdFxuICAgIGlmICh0b2tlbnMubGVuZ3RoID4gMCAmJiBDT01CSU5FRF9TRUxFQ1RPUl9TVEFSVC50ZXN0KHRva2Vuc1swXSkpIHtcbiAgICAgIHRva2Vucy5zaGlmdCgpXG4gICAgICByZXN1bHQgPSBjb25zdW1lVW50aWwodG9rZW5zLCBDT01CSU5FRF9TRUxFQ1RPUl9FTkQpLnRyaW0oKVxuICAgICAgdG9rZW5zLnNoaWZ0KClcbiAgICB9IGVsc2Uge1xuICAgICAgcmVzdWx0ID0gY29uc3VtZVVudGlsKHRva2VucywgV0hJVEVTUEFDRV9PUl9DT01NQSlcbiAgICB9XG4gICAgcmV0dXJuIHJlc3VsdFxuICB9XG5cbiAgY29uc3QgSU5QVVRfU0VMRUNUT1IgPSAnaW5wdXQsIHRleHRhcmVhLCBzZWxlY3QnXG5cbiAgLyoqXG4gICAqIEBwYXJhbSB7RWxlbWVudH0gZWx0XG4gICAqIEBwYXJhbSB7c3RyaW5nfSBleHBsaWNpdFRyaWdnZXJcbiAgICogQHBhcmFtIHtPYmplY3R9IGNhY2hlIGZvciB0cmlnZ2VyIHNwZWNzXG4gICAqIEByZXR1cm5zIHtIdG14VHJpZ2dlclNwZWNpZmljYXRpb25bXX1cbiAgICovXG4gIGZ1bmN0aW9uIHBhcnNlQW5kQ2FjaGVUcmlnZ2VyKGVsdCwgZXhwbGljaXRUcmlnZ2VyLCBjYWNoZSkge1xuICAgIC8qKiBAdHlwZSBIdG14VHJpZ2dlclNwZWNpZmljYXRpb25bXSAqL1xuICAgIGNvbnN0IHRyaWdnZXJTcGVjcyA9IFtdXG4gICAgY29uc3QgdG9rZW5zID0gdG9rZW5pemVTdHJpbmcoZXhwbGljaXRUcmlnZ2VyKVxuICAgIGRvIHtcbiAgICAgIGNvbnN1bWVVbnRpbCh0b2tlbnMsIE5PVF9XSElURVNQQUNFKVxuICAgICAgY29uc3QgaW5pdGlhbExlbmd0aCA9IHRva2Vucy5sZW5ndGhcbiAgICAgIGNvbnN0IHRyaWdnZXIgPSBjb25zdW1lVW50aWwodG9rZW5zLCAvWyxcXFtcXHNdLylcbiAgICAgIGlmICh0cmlnZ2VyICE9PSAnJykge1xuICAgICAgICBpZiAodHJpZ2dlciA9PT0gJ2V2ZXJ5Jykge1xuICAgICAgICAgIC8qKiBAdHlwZSBIdG14VHJpZ2dlclNwZWNpZmljYXRpb24gKi9cbiAgICAgICAgICBjb25zdCBldmVyeSA9IHsgdHJpZ2dlcjogJ2V2ZXJ5JyB9XG4gICAgICAgICAgY29uc3VtZVVudGlsKHRva2VucywgTk9UX1dISVRFU1BBQ0UpXG4gICAgICAgICAgZXZlcnkucG9sbEludGVydmFsID0gcGFyc2VJbnRlcnZhbChjb25zdW1lVW50aWwodG9rZW5zLCAvWyxcXFtcXHNdLykpXG4gICAgICAgICAgY29uc3VtZVVudGlsKHRva2VucywgTk9UX1dISVRFU1BBQ0UpXG4gICAgICAgICAgdmFyIGV2ZW50RmlsdGVyID0gbWF5YmVHZW5lcmF0ZUNvbmRpdGlvbmFsKGVsdCwgdG9rZW5zLCAnZXZlbnQnKVxuICAgICAgICAgIGlmIChldmVudEZpbHRlcikge1xuICAgICAgICAgICAgZXZlcnkuZXZlbnRGaWx0ZXIgPSBldmVudEZpbHRlclxuICAgICAgICAgIH1cbiAgICAgICAgICB0cmlnZ2VyU3BlY3MucHVzaChldmVyeSlcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAvKiogQHR5cGUgSHRteFRyaWdnZXJTcGVjaWZpY2F0aW9uICovXG4gICAgICAgICAgY29uc3QgdHJpZ2dlclNwZWMgPSB7IHRyaWdnZXIgfVxuICAgICAgICAgIHZhciBldmVudEZpbHRlciA9IG1heWJlR2VuZXJhdGVDb25kaXRpb25hbChlbHQsIHRva2VucywgJ2V2ZW50JylcbiAgICAgICAgICBpZiAoZXZlbnRGaWx0ZXIpIHtcbiAgICAgICAgICAgIHRyaWdnZXJTcGVjLmV2ZW50RmlsdGVyID0gZXZlbnRGaWx0ZXJcbiAgICAgICAgICB9XG4gICAgICAgICAgY29uc3VtZVVudGlsKHRva2VucywgTk9UX1dISVRFU1BBQ0UpXG4gICAgICAgICAgd2hpbGUgKHRva2Vucy5sZW5ndGggPiAwICYmIHRva2Vuc1swXSAhPT0gJywnKSB7XG4gICAgICAgICAgICBjb25zdCB0b2tlbiA9IHRva2Vucy5zaGlmdCgpXG4gICAgICAgICAgICBpZiAodG9rZW4gPT09ICdjaGFuZ2VkJykge1xuICAgICAgICAgICAgICB0cmlnZ2VyU3BlYy5jaGFuZ2VkID0gdHJ1ZVxuICAgICAgICAgICAgfSBlbHNlIGlmICh0b2tlbiA9PT0gJ29uY2UnKSB7XG4gICAgICAgICAgICAgIHRyaWdnZXJTcGVjLm9uY2UgPSB0cnVlXG4gICAgICAgICAgICB9IGVsc2UgaWYgKHRva2VuID09PSAnY29uc3VtZScpIHtcbiAgICAgICAgICAgICAgdHJpZ2dlclNwZWMuY29uc3VtZSA9IHRydWVcbiAgICAgICAgICAgIH0gZWxzZSBpZiAodG9rZW4gPT09ICdkZWxheScgJiYgdG9rZW5zWzBdID09PSAnOicpIHtcbiAgICAgICAgICAgICAgdG9rZW5zLnNoaWZ0KClcbiAgICAgICAgICAgICAgdHJpZ2dlclNwZWMuZGVsYXkgPSBwYXJzZUludGVydmFsKGNvbnN1bWVVbnRpbCh0b2tlbnMsIFdISVRFU1BBQ0VfT1JfQ09NTUEpKVxuICAgICAgICAgICAgfSBlbHNlIGlmICh0b2tlbiA9PT0gJ2Zyb20nICYmIHRva2Vuc1swXSA9PT0gJzonKSB7XG4gICAgICAgICAgICAgIHRva2Vucy5zaGlmdCgpXG4gICAgICAgICAgICAgIGlmIChDT01CSU5FRF9TRUxFQ1RPUl9TVEFSVC50ZXN0KHRva2Vuc1swXSkpIHtcbiAgICAgICAgICAgICAgICB2YXIgZnJvbV9hcmcgPSBjb25zdW1lQ1NTU2VsZWN0b3IodG9rZW5zKVxuICAgICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgIHZhciBmcm9tX2FyZyA9IGNvbnN1bWVVbnRpbCh0b2tlbnMsIFdISVRFU1BBQ0VfT1JfQ09NTUEpXG4gICAgICAgICAgICAgICAgaWYgKGZyb21fYXJnID09PSAnY2xvc2VzdCcgfHwgZnJvbV9hcmcgPT09ICdmaW5kJyB8fCBmcm9tX2FyZyA9PT0gJ25leHQnIHx8IGZyb21fYXJnID09PSAncHJldmlvdXMnKSB7XG4gICAgICAgICAgICAgICAgICB0b2tlbnMuc2hpZnQoKVxuICAgICAgICAgICAgICAgICAgY29uc3Qgc2VsZWN0b3IgPSBjb25zdW1lQ1NTU2VsZWN0b3IodG9rZW5zKVxuICAgICAgICAgICAgICAgICAgLy8gYG5leHRgIGFuZCBgcHJldmlvdXNgIGFsbG93IGEgc2VsZWN0b3ItbGVzcyBzeW50YXhcbiAgICAgICAgICAgICAgICAgIGlmIChzZWxlY3Rvci5sZW5ndGggPiAwKSB7XG4gICAgICAgICAgICAgICAgICAgIGZyb21fYXJnICs9ICcgJyArIHNlbGVjdG9yXG4gICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgIHRyaWdnZXJTcGVjLmZyb20gPSBmcm9tX2FyZ1xuICAgICAgICAgICAgfSBlbHNlIGlmICh0b2tlbiA9PT0gJ3RhcmdldCcgJiYgdG9rZW5zWzBdID09PSAnOicpIHtcbiAgICAgICAgICAgICAgdG9rZW5zLnNoaWZ0KClcbiAgICAgICAgICAgICAgdHJpZ2dlclNwZWMudGFyZ2V0ID0gY29uc3VtZUNTU1NlbGVjdG9yKHRva2VucylcbiAgICAgICAgICAgIH0gZWxzZSBpZiAodG9rZW4gPT09ICd0aHJvdHRsZScgJiYgdG9rZW5zWzBdID09PSAnOicpIHtcbiAgICAgICAgICAgICAgdG9rZW5zLnNoaWZ0KClcbiAgICAgICAgICAgICAgdHJpZ2dlclNwZWMudGhyb3R0bGUgPSBwYXJzZUludGVydmFsKGNvbnN1bWVVbnRpbCh0b2tlbnMsIFdISVRFU1BBQ0VfT1JfQ09NTUEpKVxuICAgICAgICAgICAgfSBlbHNlIGlmICh0b2tlbiA9PT0gJ3F1ZXVlJyAmJiB0b2tlbnNbMF0gPT09ICc6Jykge1xuICAgICAgICAgICAgICB0b2tlbnMuc2hpZnQoKVxuICAgICAgICAgICAgICB0cmlnZ2VyU3BlYy5xdWV1ZSA9IGNvbnN1bWVVbnRpbCh0b2tlbnMsIFdISVRFU1BBQ0VfT1JfQ09NTUEpXG4gICAgICAgICAgICB9IGVsc2UgaWYgKHRva2VuID09PSAncm9vdCcgJiYgdG9rZW5zWzBdID09PSAnOicpIHtcbiAgICAgICAgICAgICAgdG9rZW5zLnNoaWZ0KClcbiAgICAgICAgICAgICAgdHJpZ2dlclNwZWNbdG9rZW5dID0gY29uc3VtZUNTU1NlbGVjdG9yKHRva2VucylcbiAgICAgICAgICAgIH0gZWxzZSBpZiAodG9rZW4gPT09ICd0aHJlc2hvbGQnICYmIHRva2Vuc1swXSA9PT0gJzonKSB7XG4gICAgICAgICAgICAgIHRva2Vucy5zaGlmdCgpXG4gICAgICAgICAgICAgIHRyaWdnZXJTcGVjW3Rva2VuXSA9IGNvbnN1bWVVbnRpbCh0b2tlbnMsIFdISVRFU1BBQ0VfT1JfQ09NTUEpXG4gICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICB0cmlnZ2VyRXJyb3JFdmVudChlbHQsICdodG14OnN5bnRheDplcnJvcicsIHsgdG9rZW46IHRva2Vucy5zaGlmdCgpIH0pXG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBjb25zdW1lVW50aWwodG9rZW5zLCBOT1RfV0hJVEVTUEFDRSlcbiAgICAgICAgICB9XG4gICAgICAgICAgdHJpZ2dlclNwZWNzLnB1c2godHJpZ2dlclNwZWMpXG4gICAgICAgIH1cbiAgICAgIH1cbiAgICAgIGlmICh0b2tlbnMubGVuZ3RoID09PSBpbml0aWFsTGVuZ3RoKSB7XG4gICAgICAgIHRyaWdnZXJFcnJvckV2ZW50KGVsdCwgJ2h0bXg6c3ludGF4OmVycm9yJywgeyB0b2tlbjogdG9rZW5zLnNoaWZ0KCkgfSlcbiAgICAgIH1cbiAgICAgIGNvbnN1bWVVbnRpbCh0b2tlbnMsIE5PVF9XSElURVNQQUNFKVxuICAgIH0gd2hpbGUgKHRva2Vuc1swXSA9PT0gJywnICYmIHRva2Vucy5zaGlmdCgpKVxuICAgIGlmIChjYWNoZSkge1xuICAgICAgY2FjaGVbZXhwbGljaXRUcmlnZ2VyXSA9IHRyaWdnZXJTcGVjc1xuICAgIH1cbiAgICByZXR1cm4gdHJpZ2dlclNwZWNzXG4gIH1cblxuICAvKipcbiAgICogQHBhcmFtIHtFbGVtZW50fSBlbHRcbiAgICogQHJldHVybnMge0h0bXhUcmlnZ2VyU3BlY2lmaWNhdGlvbltdfVxuICAgKi9cbiAgZnVuY3Rpb24gZ2V0VHJpZ2dlclNwZWNzKGVsdCkge1xuICAgIGNvbnN0IGV4cGxpY2l0VHJpZ2dlciA9IGdldEF0dHJpYnV0ZVZhbHVlKGVsdCwgJ2h4LXRyaWdnZXInKVxuICAgIGxldCB0cmlnZ2VyU3BlY3MgPSBbXVxuICAgIGlmIChleHBsaWNpdFRyaWdnZXIpIHtcbiAgICAgIGNvbnN0IGNhY2hlID0gaHRteC5jb25maWcudHJpZ2dlclNwZWNzQ2FjaGVcbiAgICAgIHRyaWdnZXJTcGVjcyA9IChjYWNoZSAmJiBjYWNoZVtleHBsaWNpdFRyaWdnZXJdKSB8fCBwYXJzZUFuZENhY2hlVHJpZ2dlcihlbHQsIGV4cGxpY2l0VHJpZ2dlciwgY2FjaGUpXG4gICAgfVxuXG4gICAgaWYgKHRyaWdnZXJTcGVjcy5sZW5ndGggPiAwKSB7XG4gICAgICByZXR1cm4gdHJpZ2dlclNwZWNzXG4gICAgfSBlbHNlIGlmIChtYXRjaGVzKGVsdCwgJ2Zvcm0nKSkge1xuICAgICAgcmV0dXJuIFt7IHRyaWdnZXI6ICdzdWJtaXQnIH1dXG4gICAgfSBlbHNlIGlmIChtYXRjaGVzKGVsdCwgJ2lucHV0W3R5cGU9XCJidXR0b25cIl0sIGlucHV0W3R5cGU9XCJzdWJtaXRcIl0nKSkge1xuICAgICAgcmV0dXJuIFt7IHRyaWdnZXI6ICdjbGljaycgfV1cbiAgICB9IGVsc2UgaWYgKG1hdGNoZXMoZWx0LCBJTlBVVF9TRUxFQ1RPUikpIHtcbiAgICAgIHJldHVybiBbeyB0cmlnZ2VyOiAnY2hhbmdlJyB9XVxuICAgIH0gZWxzZSB7XG4gICAgICByZXR1cm4gW3sgdHJpZ2dlcjogJ2NsaWNrJyB9XVxuICAgIH1cbiAgfVxuXG4gIC8qKlxuICAgKiBAcGFyYW0ge0VsZW1lbnR9IGVsdFxuICAgKi9cbiAgZnVuY3Rpb24gY2FuY2VsUG9sbGluZyhlbHQpIHtcbiAgICBnZXRJbnRlcm5hbERhdGEoZWx0KS5jYW5jZWxsZWQgPSB0cnVlXG4gIH1cblxuICAvKipcbiAgICogQHBhcmFtIHtFbGVtZW50fSBlbHRcbiAgICogQHBhcmFtIHtUcmlnZ2VySGFuZGxlcn0gaGFuZGxlclxuICAgKiBAcGFyYW0ge0h0bXhUcmlnZ2VyU3BlY2lmaWNhdGlvbn0gc3BlY1xuICAgKi9cbiAgZnVuY3Rpb24gcHJvY2Vzc1BvbGxpbmcoZWx0LCBoYW5kbGVyLCBzcGVjKSB7XG4gICAgY29uc3Qgbm9kZURhdGEgPSBnZXRJbnRlcm5hbERhdGEoZWx0KVxuICAgIG5vZGVEYXRhLnRpbWVvdXQgPSBnZXRXaW5kb3coKS5zZXRUaW1lb3V0KGZ1bmN0aW9uKCkge1xuICAgICAgaWYgKGJvZHlDb250YWlucyhlbHQpICYmIG5vZGVEYXRhLmNhbmNlbGxlZCAhPT0gdHJ1ZSkge1xuICAgICAgICBpZiAoIW1heWJlRmlsdGVyRXZlbnQoc3BlYywgZWx0LCBtYWtlRXZlbnQoJ2h4OnBvbGw6dHJpZ2dlcicsIHtcbiAgICAgICAgICB0cmlnZ2VyU3BlYzogc3BlYyxcbiAgICAgICAgICB0YXJnZXQ6IGVsdFxuICAgICAgICB9KSkpIHtcbiAgICAgICAgICBoYW5kbGVyKGVsdClcbiAgICAgICAgfVxuICAgICAgICBwcm9jZXNzUG9sbGluZyhlbHQsIGhhbmRsZXIsIHNwZWMpXG4gICAgICB9XG4gICAgfSwgc3BlYy5wb2xsSW50ZXJ2YWwpXG4gIH1cblxuICAvKipcbiAgICogQHBhcmFtIHtIVE1MQW5jaG9yRWxlbWVudH0gZWx0XG4gICAqIEByZXR1cm5zIHtib29sZWFufVxuICAgKi9cbiAgZnVuY3Rpb24gaXNMb2NhbExpbmsoZWx0KSB7XG4gICAgcmV0dXJuIGxvY2F0aW9uLmhvc3RuYW1lID09PSBlbHQuaG9zdG5hbWUgJiZcbiAgICAgIGdldFJhd0F0dHJpYnV0ZShlbHQsICdocmVmJykgJiZcbiAgICAgIGdldFJhd0F0dHJpYnV0ZShlbHQsICdocmVmJykuaW5kZXhPZignIycpICE9PSAwXG4gIH1cblxuICAvKipcbiAgICogQHBhcmFtIHtFbGVtZW50fSBlbHRcbiAgICovXG4gIGZ1bmN0aW9uIGVsdElzRGlzYWJsZWQoZWx0KSB7XG4gICAgcmV0dXJuIGNsb3Nlc3QoZWx0LCBodG14LmNvbmZpZy5kaXNhYmxlU2VsZWN0b3IpXG4gIH1cblxuICAvKipcbiAgICogQHBhcmFtIHtFbGVtZW50fSBlbHRcbiAgICogQHBhcmFtIHtIdG14Tm9kZUludGVybmFsRGF0YX0gbm9kZURhdGFcbiAgICogQHBhcmFtIHtIdG14VHJpZ2dlclNwZWNpZmljYXRpb25bXX0gdHJpZ2dlclNwZWNzXG4gICAqL1xuICBmdW5jdGlvbiBib29zdEVsZW1lbnQoZWx0LCBub2RlRGF0YSwgdHJpZ2dlclNwZWNzKSB7XG4gICAgaWYgKChlbHQgaW5zdGFuY2VvZiBIVE1MQW5jaG9yRWxlbWVudCAmJiBpc0xvY2FsTGluayhlbHQpICYmIChlbHQudGFyZ2V0ID09PSAnJyB8fCBlbHQudGFyZ2V0ID09PSAnX3NlbGYnKSkgfHwgKGVsdC50YWdOYW1lID09PSAnRk9STScgJiYgU3RyaW5nKGdldFJhd0F0dHJpYnV0ZShlbHQsICdtZXRob2QnKSkudG9Mb3dlckNhc2UoKSAhPT0gJ2RpYWxvZycpKSB7XG4gICAgICBub2RlRGF0YS5ib29zdGVkID0gdHJ1ZVxuICAgICAgbGV0IHZlcmIsIHBhdGhcbiAgICAgIGlmIChlbHQudGFnTmFtZSA9PT0gJ0EnKSB7XG4gICAgICAgIHZlcmIgPSAoLyoqIEB0eXBlIEh0dHBWZXJiICovKCdnZXQnKSlcbiAgICAgICAgcGF0aCA9IGdldFJhd0F0dHJpYnV0ZShlbHQsICdocmVmJylcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIGNvbnN0IHJhd0F0dHJpYnV0ZSA9IGdldFJhd0F0dHJpYnV0ZShlbHQsICdtZXRob2QnKVxuICAgICAgICB2ZXJiID0gKC8qKiBAdHlwZSBIdHRwVmVyYiAqLyhyYXdBdHRyaWJ1dGUgPyByYXdBdHRyaWJ1dGUudG9Mb3dlckNhc2UoKSA6ICdnZXQnKSlcbiAgICAgICAgcGF0aCA9IGdldFJhd0F0dHJpYnV0ZShlbHQsICdhY3Rpb24nKVxuICAgICAgICBpZiAocGF0aCA9PSBudWxsIHx8IHBhdGggPT09ICcnKSB7XG4gICAgICAgICAgLy8gaWYgdGhlcmUgaXMgbm8gYWN0aW9uIGF0dHJpYnV0ZSBvbiB0aGUgZm9ybSBzZXQgcGF0aCB0byBjdXJyZW50IGhyZWYgYmVmb3JlIHRoZVxuICAgICAgICAgIC8vIGZvbGxvd2luZyBsb2dpYyB0byBwcm9wZXJseSBjbGVhciBwYXJhbWV0ZXJzIG9uIGEgR0VUIChub3Qgb24gYSBQT1NUISlcbiAgICAgICAgICBwYXRoID0gZ2V0RG9jdW1lbnQoKS5sb2NhdGlvbi5ocmVmXG4gICAgICAgIH1cbiAgICAgICAgaWYgKHZlcmIgPT09ICdnZXQnICYmIHBhdGguaW5jbHVkZXMoJz8nKSkge1xuICAgICAgICAgIHBhdGggPSBwYXRoLnJlcGxhY2UoL1xcP1teI10rLywgJycpXG4gICAgICAgIH1cbiAgICAgIH1cbiAgICAgIHRyaWdnZXJTcGVjcy5mb3JFYWNoKGZ1bmN0aW9uKHRyaWdnZXJTcGVjKSB7XG4gICAgICAgIGFkZEV2ZW50TGlzdGVuZXIoZWx0LCBmdW5jdGlvbihub2RlLCBldnQpIHtcbiAgICAgICAgICBjb25zdCBlbHQgPSBhc0VsZW1lbnQobm9kZSlcbiAgICAgICAgICBpZiAoZWx0SXNEaXNhYmxlZChlbHQpKSB7XG4gICAgICAgICAgICBjbGVhblVwRWxlbWVudChlbHQpXG4gICAgICAgICAgICByZXR1cm5cbiAgICAgICAgICB9XG4gICAgICAgICAgaXNzdWVBamF4UmVxdWVzdCh2ZXJiLCBwYXRoLCBlbHQsIGV2dClcbiAgICAgICAgfSwgbm9kZURhdGEsIHRyaWdnZXJTcGVjLCB0cnVlKVxuICAgICAgfSlcbiAgICB9XG4gIH1cblxuICAvKipcbiAgICogQHBhcmFtIHtFdmVudH0gZXZ0XG4gICAqIEBwYXJhbSB7Tm9kZX0gbm9kZVxuICAgKiBAcmV0dXJucyB7Ym9vbGVhbn1cbiAgICovXG4gIGZ1bmN0aW9uIHNob3VsZENhbmNlbChldnQsIG5vZGUpIHtcbiAgICBjb25zdCBlbHQgPSBhc0VsZW1lbnQobm9kZSlcbiAgICBpZiAoIWVsdCkge1xuICAgICAgcmV0dXJuIGZhbHNlXG4gICAgfVxuICAgIGlmIChldnQudHlwZSA9PT0gJ3N1Ym1pdCcgfHwgZXZ0LnR5cGUgPT09ICdjbGljaycpIHtcbiAgICAgIGlmIChlbHQudGFnTmFtZSA9PT0gJ0ZPUk0nKSB7XG4gICAgICAgIHJldHVybiB0cnVlXG4gICAgICB9XG4gICAgICBpZiAobWF0Y2hlcyhlbHQsICdpbnB1dFt0eXBlPVwic3VibWl0XCJdLCBidXR0b24nKSAmJlxuICAgICAgICAobWF0Y2hlcyhlbHQsICdbZm9ybV0nKSB8fCBjbG9zZXN0KGVsdCwgJ2Zvcm0nKSAhPT0gbnVsbCkpIHtcbiAgICAgICAgcmV0dXJuIHRydWVcbiAgICAgIH1cbiAgICAgIGlmIChlbHQgaW5zdGFuY2VvZiBIVE1MQW5jaG9yRWxlbWVudCAmJiBlbHQuaHJlZiAmJlxuICAgICAgICAoZWx0LmdldEF0dHJpYnV0ZSgnaHJlZicpID09PSAnIycgfHwgZWx0LmdldEF0dHJpYnV0ZSgnaHJlZicpLmluZGV4T2YoJyMnKSAhPT0gMCkpIHtcbiAgICAgICAgcmV0dXJuIHRydWVcbiAgICAgIH1cbiAgICB9XG4gICAgcmV0dXJuIGZhbHNlXG4gIH1cblxuICAvKipcbiAgICogQHBhcmFtIHtOb2RlfSBlbHRcbiAgICogQHBhcmFtIHtFdmVudHxNb3VzZUV2ZW50fEtleWJvYXJkRXZlbnR8VG91Y2hFdmVudH0gZXZ0XG4gICAqIEByZXR1cm5zIHtib29sZWFufVxuICAgKi9cbiAgZnVuY3Rpb24gaWdub3JlQm9vc3RlZEFuY2hvckN0cmxDbGljayhlbHQsIGV2dCkge1xuICAgIHJldHVybiBnZXRJbnRlcm5hbERhdGEoZWx0KS5ib29zdGVkICYmIGVsdCBpbnN0YW5jZW9mIEhUTUxBbmNob3JFbGVtZW50ICYmIGV2dC50eXBlID09PSAnY2xpY2snICYmXG4gICAgICAvLyBAdHMtaWdub3JlIHRoaXMgd2lsbCByZXNvbHZlIHRvIHVuZGVmaW5lZCBmb3IgZXZlbnRzIHRoYXQgZG9uJ3QgZGVmaW5lIHRob3NlIHByb3BlcnRpZXMsIHdoaWNoIGlzIGZpbmVcbiAgICAgIChldnQuY3RybEtleSB8fCBldnQubWV0YUtleSlcbiAgfVxuXG4gIC8qKlxuICAgKiBAcGFyYW0ge0h0bXhUcmlnZ2VyU3BlY2lmaWNhdGlvbn0gdHJpZ2dlclNwZWNcbiAgICogQHBhcmFtIHtOb2RlfSBlbHRcbiAgICogQHBhcmFtIHtFdmVudH0gZXZ0XG4gICAqIEByZXR1cm5zIHtib29sZWFufVxuICAgKi9cbiAgZnVuY3Rpb24gbWF5YmVGaWx0ZXJFdmVudCh0cmlnZ2VyU3BlYywgZWx0LCBldnQpIHtcbiAgICBjb25zdCBldmVudEZpbHRlciA9IHRyaWdnZXJTcGVjLmV2ZW50RmlsdGVyXG4gICAgaWYgKGV2ZW50RmlsdGVyKSB7XG4gICAgICB0cnkge1xuICAgICAgICByZXR1cm4gZXZlbnRGaWx0ZXIuY2FsbChlbHQsIGV2dCkgIT09IHRydWVcbiAgICAgIH0gY2F0Y2ggKGUpIHtcbiAgICAgICAgY29uc3Qgc291cmNlID0gZXZlbnRGaWx0ZXIuc291cmNlXG4gICAgICAgIHRyaWdnZXJFcnJvckV2ZW50KGdldERvY3VtZW50KCkuYm9keSwgJ2h0bXg6ZXZlbnRGaWx0ZXI6ZXJyb3InLCB7IGVycm9yOiBlLCBzb3VyY2UgfSlcbiAgICAgICAgcmV0dXJuIHRydWVcbiAgICAgIH1cbiAgICB9XG4gICAgcmV0dXJuIGZhbHNlXG4gIH1cblxuICAvKipcbiAgICogQHBhcmFtIHtOb2RlfSBlbHRcbiAgICogQHBhcmFtIHtUcmlnZ2VySGFuZGxlcn0gaGFuZGxlclxuICAgKiBAcGFyYW0ge0h0bXhOb2RlSW50ZXJuYWxEYXRhfSBub2RlRGF0YVxuICAgKiBAcGFyYW0ge0h0bXhUcmlnZ2VyU3BlY2lmaWNhdGlvbn0gdHJpZ2dlclNwZWNcbiAgICogQHBhcmFtIHtib29sZWFufSBbZXhwbGljaXRDYW5jZWxdXG4gICAqL1xuICBmdW5jdGlvbiBhZGRFdmVudExpc3RlbmVyKGVsdCwgaGFuZGxlciwgbm9kZURhdGEsIHRyaWdnZXJTcGVjLCBleHBsaWNpdENhbmNlbCkge1xuICAgIGNvbnN0IGVsZW1lbnREYXRhID0gZ2V0SW50ZXJuYWxEYXRhKGVsdClcbiAgICAvKiogQHR5cGUgeyhOb2RlfFdpbmRvdylbXX0gKi9cbiAgICBsZXQgZWx0c1RvTGlzdGVuT25cbiAgICBpZiAodHJpZ2dlclNwZWMuZnJvbSkge1xuICAgICAgZWx0c1RvTGlzdGVuT24gPSBxdWVyeVNlbGVjdG9yQWxsRXh0KGVsdCwgdHJpZ2dlclNwZWMuZnJvbSlcbiAgICB9IGVsc2Uge1xuICAgICAgZWx0c1RvTGlzdGVuT24gPSBbZWx0XVxuICAgIH1cbiAgICAvLyBzdG9yZSB0aGUgaW5pdGlhbCB2YWx1ZXMgb2YgdGhlIGVsZW1lbnRzLCBzbyB3ZSBjYW4gdGVsbCBpZiB0aGV5IGNoYW5nZVxuICAgIGlmICh0cmlnZ2VyU3BlYy5jaGFuZ2VkKSB7XG4gICAgICBpZiAoISgnbGFzdFZhbHVlJyBpbiBlbGVtZW50RGF0YSkpIHtcbiAgICAgICAgZWxlbWVudERhdGEubGFzdFZhbHVlID0gbmV3IFdlYWtNYXAoKVxuICAgICAgfVxuICAgICAgZWx0c1RvTGlzdGVuT24uZm9yRWFjaChmdW5jdGlvbihlbHRUb0xpc3Rlbk9uKSB7XG4gICAgICAgIGlmICghZWxlbWVudERhdGEubGFzdFZhbHVlLmhhcyh0cmlnZ2VyU3BlYykpIHtcbiAgICAgICAgICBlbGVtZW50RGF0YS5sYXN0VmFsdWUuc2V0KHRyaWdnZXJTcGVjLCBuZXcgV2Vha01hcCgpKVxuICAgICAgICB9XG4gICAgICAgIC8vIEB0cy1pZ25vcmUgdmFsdWUgd2lsbCBiZSB1bmRlZmluZWQgZm9yIG5vbi1pbnB1dCBlbGVtZW50cywgd2hpY2ggaXMgZmluZVxuICAgICAgICBlbGVtZW50RGF0YS5sYXN0VmFsdWUuZ2V0KHRyaWdnZXJTcGVjKS5zZXQoZWx0VG9MaXN0ZW5PbiwgZWx0VG9MaXN0ZW5Pbi52YWx1ZSlcbiAgICAgIH0pXG4gICAgfVxuICAgIGZvckVhY2goZWx0c1RvTGlzdGVuT24sIGZ1bmN0aW9uKGVsdFRvTGlzdGVuT24pIHtcbiAgICAgIC8qKiBAdHlwZSBFdmVudExpc3RlbmVyICovXG4gICAgICBjb25zdCBldmVudExpc3RlbmVyID0gZnVuY3Rpb24oZXZ0KSB7XG4gICAgICAgIGlmICghYm9keUNvbnRhaW5zKGVsdCkpIHtcbiAgICAgICAgICBlbHRUb0xpc3Rlbk9uLnJlbW92ZUV2ZW50TGlzdGVuZXIodHJpZ2dlclNwZWMudHJpZ2dlciwgZXZlbnRMaXN0ZW5lcilcbiAgICAgICAgICByZXR1cm5cbiAgICAgICAgfVxuICAgICAgICBpZiAoaWdub3JlQm9vc3RlZEFuY2hvckN0cmxDbGljayhlbHQsIGV2dCkpIHtcbiAgICAgICAgICByZXR1cm5cbiAgICAgICAgfVxuICAgICAgICBpZiAoZXhwbGljaXRDYW5jZWwgfHwgc2hvdWxkQ2FuY2VsKGV2dCwgZWx0KSkge1xuICAgICAgICAgIGV2dC5wcmV2ZW50RGVmYXVsdCgpXG4gICAgICAgIH1cbiAgICAgICAgaWYgKG1heWJlRmlsdGVyRXZlbnQodHJpZ2dlclNwZWMsIGVsdCwgZXZ0KSkge1xuICAgICAgICAgIHJldHVyblxuICAgICAgICB9XG4gICAgICAgIGNvbnN0IGV2ZW50RGF0YSA9IGdldEludGVybmFsRGF0YShldnQpXG4gICAgICAgIGV2ZW50RGF0YS50cmlnZ2VyU3BlYyA9IHRyaWdnZXJTcGVjXG4gICAgICAgIGlmIChldmVudERhdGEuaGFuZGxlZEZvciA9PSBudWxsKSB7XG4gICAgICAgICAgZXZlbnREYXRhLmhhbmRsZWRGb3IgPSBbXVxuICAgICAgICB9XG4gICAgICAgIGlmIChldmVudERhdGEuaGFuZGxlZEZvci5pbmRleE9mKGVsdCkgPCAwKSB7XG4gICAgICAgICAgZXZlbnREYXRhLmhhbmRsZWRGb3IucHVzaChlbHQpXG4gICAgICAgICAgaWYgKHRyaWdnZXJTcGVjLmNvbnN1bWUpIHtcbiAgICAgICAgICAgIGV2dC5zdG9wUHJvcGFnYXRpb24oKVxuICAgICAgICAgIH1cbiAgICAgICAgICBpZiAodHJpZ2dlclNwZWMudGFyZ2V0ICYmIGV2dC50YXJnZXQpIHtcbiAgICAgICAgICAgIGlmICghbWF0Y2hlcyhhc0VsZW1lbnQoZXZ0LnRhcmdldCksIHRyaWdnZXJTcGVjLnRhcmdldCkpIHtcbiAgICAgICAgICAgICAgcmV0dXJuXG4gICAgICAgICAgICB9XG4gICAgICAgICAgfVxuICAgICAgICAgIGlmICh0cmlnZ2VyU3BlYy5vbmNlKSB7XG4gICAgICAgICAgICBpZiAoZWxlbWVudERhdGEudHJpZ2dlcmVkT25jZSkge1xuICAgICAgICAgICAgICByZXR1cm5cbiAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgIGVsZW1lbnREYXRhLnRyaWdnZXJlZE9uY2UgPSB0cnVlXG4gICAgICAgICAgICB9XG4gICAgICAgICAgfVxuICAgICAgICAgIGlmICh0cmlnZ2VyU3BlYy5jaGFuZ2VkKSB7XG4gICAgICAgICAgICBjb25zdCBub2RlID0gZXZlbnQudGFyZ2V0XG4gICAgICAgICAgICAvLyBAdHMtaWdub3JlIHZhbHVlIHdpbGwgYmUgdW5kZWZpbmVkIGZvciBub24taW5wdXQgZWxlbWVudHMsIHdoaWNoIGlzIGZpbmVcbiAgICAgICAgICAgIGNvbnN0IHZhbHVlID0gbm9kZS52YWx1ZVxuICAgICAgICAgICAgY29uc3QgbGFzdFZhbHVlID0gZWxlbWVudERhdGEubGFzdFZhbHVlLmdldCh0cmlnZ2VyU3BlYylcbiAgICAgICAgICAgIGlmIChsYXN0VmFsdWUuaGFzKG5vZGUpICYmIGxhc3RWYWx1ZS5nZXQobm9kZSkgPT09IHZhbHVlKSB7XG4gICAgICAgICAgICAgIHJldHVyblxuICAgICAgICAgICAgfVxuICAgICAgICAgICAgbGFzdFZhbHVlLnNldChub2RlLCB2YWx1ZSlcbiAgICAgICAgICB9XG4gICAgICAgICAgaWYgKGVsZW1lbnREYXRhLmRlbGF5ZWQpIHtcbiAgICAgICAgICAgIGNsZWFyVGltZW91dChlbGVtZW50RGF0YS5kZWxheWVkKVxuICAgICAgICAgIH1cbiAgICAgICAgICBpZiAoZWxlbWVudERhdGEudGhyb3R0bGUpIHtcbiAgICAgICAgICAgIHJldHVyblxuICAgICAgICAgIH1cblxuICAgICAgICAgIGlmICh0cmlnZ2VyU3BlYy50aHJvdHRsZSA+IDApIHtcbiAgICAgICAgICAgIGlmICghZWxlbWVudERhdGEudGhyb3R0bGUpIHtcbiAgICAgICAgICAgICAgdHJpZ2dlckV2ZW50KGVsdCwgJ2h0bXg6dHJpZ2dlcicpXG4gICAgICAgICAgICAgIGhhbmRsZXIoZWx0LCBldnQpXG4gICAgICAgICAgICAgIGVsZW1lbnREYXRhLnRocm90dGxlID0gZ2V0V2luZG93KCkuc2V0VGltZW91dChmdW5jdGlvbigpIHtcbiAgICAgICAgICAgICAgICBlbGVtZW50RGF0YS50aHJvdHRsZSA9IG51bGxcbiAgICAgICAgICAgICAgfSwgdHJpZ2dlclNwZWMudGhyb3R0bGUpXG4gICAgICAgICAgICB9XG4gICAgICAgICAgfSBlbHNlIGlmICh0cmlnZ2VyU3BlYy5kZWxheSA+IDApIHtcbiAgICAgICAgICAgIGVsZW1lbnREYXRhLmRlbGF5ZWQgPSBnZXRXaW5kb3coKS5zZXRUaW1lb3V0KGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgICB0cmlnZ2VyRXZlbnQoZWx0LCAnaHRteDp0cmlnZ2VyJylcbiAgICAgICAgICAgICAgaGFuZGxlcihlbHQsIGV2dClcbiAgICAgICAgICAgIH0sIHRyaWdnZXJTcGVjLmRlbGF5KVxuICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICB0cmlnZ2VyRXZlbnQoZWx0LCAnaHRteDp0cmlnZ2VyJylcbiAgICAgICAgICAgIGhhbmRsZXIoZWx0LCBldnQpXG4gICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICB9XG4gICAgICBpZiAobm9kZURhdGEubGlzdGVuZXJJbmZvcyA9PSBudWxsKSB7XG4gICAgICAgIG5vZGVEYXRhLmxpc3RlbmVySW5mb3MgPSBbXVxuICAgICAgfVxuICAgICAgbm9kZURhdGEubGlzdGVuZXJJbmZvcy5wdXNoKHtcbiAgICAgICAgdHJpZ2dlcjogdHJpZ2dlclNwZWMudHJpZ2dlcixcbiAgICAgICAgbGlzdGVuZXI6IGV2ZW50TGlzdGVuZXIsXG4gICAgICAgIG9uOiBlbHRUb0xpc3Rlbk9uXG4gICAgICB9KVxuICAgICAgZWx0VG9MaXN0ZW5Pbi5hZGRFdmVudExpc3RlbmVyKHRyaWdnZXJTcGVjLnRyaWdnZXIsIGV2ZW50TGlzdGVuZXIpXG4gICAgfSlcbiAgfVxuXG4gIGxldCB3aW5kb3dJc1Njcm9sbGluZyA9IGZhbHNlIC8vIHVzZWQgYnkgaW5pdFNjcm9sbEhhbmRsZXJcbiAgbGV0IHNjcm9sbEhhbmRsZXIgPSBudWxsXG4gIGZ1bmN0aW9uIGluaXRTY3JvbGxIYW5kbGVyKCkge1xuICAgIGlmICghc2Nyb2xsSGFuZGxlcikge1xuICAgICAgc2Nyb2xsSGFuZGxlciA9IGZ1bmN0aW9uKCkge1xuICAgICAgICB3aW5kb3dJc1Njcm9sbGluZyA9IHRydWVcbiAgICAgIH1cbiAgICAgIHdpbmRvdy5hZGRFdmVudExpc3RlbmVyKCdzY3JvbGwnLCBzY3JvbGxIYW5kbGVyKVxuICAgICAgd2luZG93LmFkZEV2ZW50TGlzdGVuZXIoJ3Jlc2l6ZScsIHNjcm9sbEhhbmRsZXIpXG4gICAgICBzZXRJbnRlcnZhbChmdW5jdGlvbigpIHtcbiAgICAgICAgaWYgKHdpbmRvd0lzU2Nyb2xsaW5nKSB7XG4gICAgICAgICAgd2luZG93SXNTY3JvbGxpbmcgPSBmYWxzZVxuICAgICAgICAgIGZvckVhY2goZ2V0RG9jdW1lbnQoKS5xdWVyeVNlbGVjdG9yQWxsKFwiW2h4LXRyaWdnZXIqPSdyZXZlYWxlZCddLFtkYXRhLWh4LXRyaWdnZXIqPSdyZXZlYWxlZCddXCIpLCBmdW5jdGlvbihlbHQpIHtcbiAgICAgICAgICAgIG1heWJlUmV2ZWFsKGVsdClcbiAgICAgICAgICB9KVxuICAgICAgICB9XG4gICAgICB9LCAyMDApXG4gICAgfVxuICB9XG5cbiAgLyoqXG4gICAqIEBwYXJhbSB7RWxlbWVudH0gZWx0XG4gICAqL1xuICBmdW5jdGlvbiBtYXliZVJldmVhbChlbHQpIHtcbiAgICBpZiAoIWhhc0F0dHJpYnV0ZShlbHQsICdkYXRhLWh4LXJldmVhbGVkJykgJiYgaXNTY3JvbGxlZEludG9WaWV3KGVsdCkpIHtcbiAgICAgIGVsdC5zZXRBdHRyaWJ1dGUoJ2RhdGEtaHgtcmV2ZWFsZWQnLCAndHJ1ZScpXG4gICAgICBjb25zdCBub2RlRGF0YSA9IGdldEludGVybmFsRGF0YShlbHQpXG4gICAgICBpZiAobm9kZURhdGEuaW5pdEhhc2gpIHtcbiAgICAgICAgdHJpZ2dlckV2ZW50KGVsdCwgJ3JldmVhbGVkJylcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIC8vIGlmIHRoZSBub2RlIGlzbid0IGluaXRpYWxpemVkLCB3YWl0IGZvciBpdCBiZWZvcmUgdHJpZ2dlcmluZyB0aGUgcmVxdWVzdFxuICAgICAgICBlbHQuYWRkRXZlbnRMaXN0ZW5lcignaHRteDphZnRlclByb2Nlc3NOb2RlJywgZnVuY3Rpb24oKSB7IHRyaWdnZXJFdmVudChlbHQsICdyZXZlYWxlZCcpIH0sIHsgb25jZTogdHJ1ZSB9KVxuICAgICAgfVxuICAgIH1cbiAgfVxuXG4gIC8vPSA9PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09XG5cbiAgLyoqXG4gICAqIEBwYXJhbSB7RWxlbWVudH0gZWx0XG4gICAqIEBwYXJhbSB7VHJpZ2dlckhhbmRsZXJ9IGhhbmRsZXJcbiAgICogQHBhcmFtIHtIdG14Tm9kZUludGVybmFsRGF0YX0gbm9kZURhdGFcbiAgICogQHBhcmFtIHtudW1iZXJ9IGRlbGF5XG4gICAqL1xuICBmdW5jdGlvbiBsb2FkSW1tZWRpYXRlbHkoZWx0LCBoYW5kbGVyLCBub2RlRGF0YSwgZGVsYXkpIHtcbiAgICBjb25zdCBsb2FkID0gZnVuY3Rpb24oKSB7XG4gICAgICBpZiAoIW5vZGVEYXRhLmxvYWRlZCkge1xuICAgICAgICBub2RlRGF0YS5sb2FkZWQgPSB0cnVlXG4gICAgICAgIHRyaWdnZXJFdmVudChlbHQsICdodG14OnRyaWdnZXInKVxuICAgICAgICBoYW5kbGVyKGVsdClcbiAgICAgIH1cbiAgICB9XG4gICAgaWYgKGRlbGF5ID4gMCkge1xuICAgICAgZ2V0V2luZG93KCkuc2V0VGltZW91dChsb2FkLCBkZWxheSlcbiAgICB9IGVsc2Uge1xuICAgICAgbG9hZCgpXG4gICAgfVxuICB9XG5cbiAgLyoqXG4gICAqIEBwYXJhbSB7RWxlbWVudH0gZWx0XG4gICAqIEBwYXJhbSB7SHRteE5vZGVJbnRlcm5hbERhdGF9IG5vZGVEYXRhXG4gICAqIEBwYXJhbSB7SHRteFRyaWdnZXJTcGVjaWZpY2F0aW9uW119IHRyaWdnZXJTcGVjc1xuICAgKiBAcmV0dXJucyB7Ym9vbGVhbn1cbiAgICovXG4gIGZ1bmN0aW9uIHByb2Nlc3NWZXJicyhlbHQsIG5vZGVEYXRhLCB0cmlnZ2VyU3BlY3MpIHtcbiAgICBsZXQgZXhwbGljaXRBY3Rpb24gPSBmYWxzZVxuICAgIGZvckVhY2goVkVSQlMsIGZ1bmN0aW9uKHZlcmIpIHtcbiAgICAgIGlmIChoYXNBdHRyaWJ1dGUoZWx0LCAnaHgtJyArIHZlcmIpKSB7XG4gICAgICAgIGNvbnN0IHBhdGggPSBnZXRBdHRyaWJ1dGVWYWx1ZShlbHQsICdoeC0nICsgdmVyYilcbiAgICAgICAgZXhwbGljaXRBY3Rpb24gPSB0cnVlXG4gICAgICAgIG5vZGVEYXRhLnBhdGggPSBwYXRoXG4gICAgICAgIG5vZGVEYXRhLnZlcmIgPSB2ZXJiXG4gICAgICAgIHRyaWdnZXJTcGVjcy5mb3JFYWNoKGZ1bmN0aW9uKHRyaWdnZXJTcGVjKSB7XG4gICAgICAgICAgYWRkVHJpZ2dlckhhbmRsZXIoZWx0LCB0cmlnZ2VyU3BlYywgbm9kZURhdGEsIGZ1bmN0aW9uKG5vZGUsIGV2dCkge1xuICAgICAgICAgICAgY29uc3QgZWx0ID0gYXNFbGVtZW50KG5vZGUpXG4gICAgICAgICAgICBpZiAoY2xvc2VzdChlbHQsIGh0bXguY29uZmlnLmRpc2FibGVTZWxlY3RvcikpIHtcbiAgICAgICAgICAgICAgY2xlYW5VcEVsZW1lbnQoZWx0KVxuICAgICAgICAgICAgICByZXR1cm5cbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGlzc3VlQWpheFJlcXVlc3QodmVyYiwgcGF0aCwgZWx0LCBldnQpXG4gICAgICAgICAgfSlcbiAgICAgICAgfSlcbiAgICAgIH1cbiAgICB9KVxuICAgIHJldHVybiBleHBsaWNpdEFjdGlvblxuICB9XG5cbiAgLyoqXG4gICAqIEBjYWxsYmFjayBUcmlnZ2VySGFuZGxlclxuICAgKiBAcGFyYW0ge05vZGV9IGVsdFxuICAgKiBAcGFyYW0ge0V2ZW50fSBbZXZ0XVxuICAgKi9cblxuICAvKipcbiAgICogQHBhcmFtIHtOb2RlfSBlbHRcbiAgICogQHBhcmFtIHtIdG14VHJpZ2dlclNwZWNpZmljYXRpb259IHRyaWdnZXJTcGVjXG4gICAqIEBwYXJhbSB7SHRteE5vZGVJbnRlcm5hbERhdGF9IG5vZGVEYXRhXG4gICAqIEBwYXJhbSB7VHJpZ2dlckhhbmRsZXJ9IGhhbmRsZXJcbiAgICovXG4gIGZ1bmN0aW9uIGFkZFRyaWdnZXJIYW5kbGVyKGVsdCwgdHJpZ2dlclNwZWMsIG5vZGVEYXRhLCBoYW5kbGVyKSB7XG4gICAgaWYgKHRyaWdnZXJTcGVjLnRyaWdnZXIgPT09ICdyZXZlYWxlZCcpIHtcbiAgICAgIGluaXRTY3JvbGxIYW5kbGVyKClcbiAgICAgIGFkZEV2ZW50TGlzdGVuZXIoZWx0LCBoYW5kbGVyLCBub2RlRGF0YSwgdHJpZ2dlclNwZWMpXG4gICAgICBtYXliZVJldmVhbChhc0VsZW1lbnQoZWx0KSlcbiAgICB9IGVsc2UgaWYgKHRyaWdnZXJTcGVjLnRyaWdnZXIgPT09ICdpbnRlcnNlY3QnKSB7XG4gICAgICBjb25zdCBvYnNlcnZlck9wdGlvbnMgPSB7fVxuICAgICAgaWYgKHRyaWdnZXJTcGVjLnJvb3QpIHtcbiAgICAgICAgb2JzZXJ2ZXJPcHRpb25zLnJvb3QgPSBxdWVyeVNlbGVjdG9yRXh0KGVsdCwgdHJpZ2dlclNwZWMucm9vdClcbiAgICAgIH1cbiAgICAgIGlmICh0cmlnZ2VyU3BlYy50aHJlc2hvbGQpIHtcbiAgICAgICAgb2JzZXJ2ZXJPcHRpb25zLnRocmVzaG9sZCA9IHBhcnNlRmxvYXQodHJpZ2dlclNwZWMudGhyZXNob2xkKVxuICAgICAgfVxuICAgICAgY29uc3Qgb2JzZXJ2ZXIgPSBuZXcgSW50ZXJzZWN0aW9uT2JzZXJ2ZXIoZnVuY3Rpb24oZW50cmllcykge1xuICAgICAgICBmb3IgKGxldCBpID0gMDsgaSA8IGVudHJpZXMubGVuZ3RoOyBpKyspIHtcbiAgICAgICAgICBjb25zdCBlbnRyeSA9IGVudHJpZXNbaV1cbiAgICAgICAgICBpZiAoZW50cnkuaXNJbnRlcnNlY3RpbmcpIHtcbiAgICAgICAgICAgIHRyaWdnZXJFdmVudChlbHQsICdpbnRlcnNlY3QnKVxuICAgICAgICAgICAgYnJlYWtcbiAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgIH0sIG9ic2VydmVyT3B0aW9ucylcbiAgICAgIG9ic2VydmVyLm9ic2VydmUoYXNFbGVtZW50KGVsdCkpXG4gICAgICBhZGRFdmVudExpc3RlbmVyKGFzRWxlbWVudChlbHQpLCBoYW5kbGVyLCBub2RlRGF0YSwgdHJpZ2dlclNwZWMpXG4gICAgfSBlbHNlIGlmICghbm9kZURhdGEuZmlyc3RJbml0Q29tcGxldGVkICYmIHRyaWdnZXJTcGVjLnRyaWdnZXIgPT09ICdsb2FkJykge1xuICAgICAgaWYgKCFtYXliZUZpbHRlckV2ZW50KHRyaWdnZXJTcGVjLCBlbHQsIG1ha2VFdmVudCgnbG9hZCcsIHsgZWx0IH0pKSkge1xuICAgICAgICBsb2FkSW1tZWRpYXRlbHkoYXNFbGVtZW50KGVsdCksIGhhbmRsZXIsIG5vZGVEYXRhLCB0cmlnZ2VyU3BlYy5kZWxheSlcbiAgICAgIH1cbiAgICB9IGVsc2UgaWYgKHRyaWdnZXJTcGVjLnBvbGxJbnRlcnZhbCA+IDApIHtcbiAgICAgIG5vZGVEYXRhLnBvbGxpbmcgPSB0cnVlXG4gICAgICBwcm9jZXNzUG9sbGluZyhhc0VsZW1lbnQoZWx0KSwgaGFuZGxlciwgdHJpZ2dlclNwZWMpXG4gICAgfSBlbHNlIHtcbiAgICAgIGFkZEV2ZW50TGlzdGVuZXIoZWx0LCBoYW5kbGVyLCBub2RlRGF0YSwgdHJpZ2dlclNwZWMpXG4gICAgfVxuICB9XG5cbiAgLyoqXG4gICAqIEBwYXJhbSB7Tm9kZX0gbm9kZVxuICAgKiBAcmV0dXJucyB7Ym9vbGVhbn1cbiAgICovXG4gIGZ1bmN0aW9uIHNob3VsZFByb2Nlc3NIeE9uKG5vZGUpIHtcbiAgICBjb25zdCBlbHQgPSBhc0VsZW1lbnQobm9kZSlcbiAgICBpZiAoIWVsdCkge1xuICAgICAgcmV0dXJuIGZhbHNlXG4gICAgfVxuICAgIGNvbnN0IGF0dHJpYnV0ZXMgPSBlbHQuYXR0cmlidXRlc1xuICAgIGZvciAobGV0IGogPSAwOyBqIDwgYXR0cmlidXRlcy5sZW5ndGg7IGorKykge1xuICAgICAgY29uc3QgYXR0ck5hbWUgPSBhdHRyaWJ1dGVzW2pdLm5hbWVcbiAgICAgIGlmIChzdGFydHNXaXRoKGF0dHJOYW1lLCAnaHgtb246JykgfHwgc3RhcnRzV2l0aChhdHRyTmFtZSwgJ2RhdGEtaHgtb246JykgfHxcbiAgICAgICAgc3RhcnRzV2l0aChhdHRyTmFtZSwgJ2h4LW9uLScpIHx8IHN0YXJ0c1dpdGgoYXR0ck5hbWUsICdkYXRhLWh4LW9uLScpKSB7XG4gICAgICAgIHJldHVybiB0cnVlXG4gICAgICB9XG4gICAgfVxuICAgIHJldHVybiBmYWxzZVxuICB9XG5cbiAgLyoqXG4gICAqIEBwYXJhbSB7Tm9kZX0gZWx0XG4gICAqIEByZXR1cm5zIHtFbGVtZW50W119XG4gICAqL1xuICBjb25zdCBIWF9PTl9RVUVSWSA9IG5ldyBYUGF0aEV2YWx1YXRvcigpXG4gICAgLmNyZWF0ZUV4cHJlc3Npb24oJy4vLypbQCpbIHN0YXJ0cy13aXRoKG5hbWUoKSwgXCJoeC1vbjpcIikgb3Igc3RhcnRzLXdpdGgobmFtZSgpLCBcImRhdGEtaHgtb246XCIpIG9yJyArXG4gICAgICAnIHN0YXJ0cy13aXRoKG5hbWUoKSwgXCJoeC1vbi1cIikgb3Igc3RhcnRzLXdpdGgobmFtZSgpLCBcImRhdGEtaHgtb24tXCIpIF1dJylcblxuICBmdW5jdGlvbiBwcm9jZXNzSFhPblJvb3QoZWx0LCBlbGVtZW50cykge1xuICAgIGlmIChzaG91bGRQcm9jZXNzSHhPbihlbHQpKSB7XG4gICAgICBlbGVtZW50cy5wdXNoKGFzRWxlbWVudChlbHQpKVxuICAgIH1cbiAgICBjb25zdCBpdGVyID0gSFhfT05fUVVFUlkuZXZhbHVhdGUoZWx0KVxuICAgIGxldCBub2RlID0gbnVsbFxuICAgIHdoaWxlIChub2RlID0gaXRlci5pdGVyYXRlTmV4dCgpKSBlbGVtZW50cy5wdXNoKGFzRWxlbWVudChub2RlKSlcbiAgfVxuXG4gIGZ1bmN0aW9uIGZpbmRIeE9uV2lsZGNhcmRFbGVtZW50cyhlbHQpIHtcbiAgICAvKiogQHR5cGUge0VsZW1lbnRbXX0gKi9cbiAgICBjb25zdCBlbGVtZW50cyA9IFtdXG4gICAgaWYgKGVsdCBpbnN0YW5jZW9mIERvY3VtZW50RnJhZ21lbnQpIHtcbiAgICAgIGZvciAoY29uc3QgY2hpbGQgb2YgZWx0LmNoaWxkTm9kZXMpIHtcbiAgICAgICAgcHJvY2Vzc0hYT25Sb290KGNoaWxkLCBlbGVtZW50cylcbiAgICAgIH1cbiAgICB9IGVsc2Uge1xuICAgICAgcHJvY2Vzc0hYT25Sb290KGVsdCwgZWxlbWVudHMpXG4gICAgfVxuICAgIHJldHVybiBlbGVtZW50c1xuICB9XG5cbiAgLyoqXG4gICAqIEBwYXJhbSB7RWxlbWVudH0gZWx0XG4gICAqIEByZXR1cm5zIHtOb2RlTGlzdE9mPEVsZW1lbnQ+fFtdfVxuICAgKi9cbiAgZnVuY3Rpb24gZmluZEVsZW1lbnRzVG9Qcm9jZXNzKGVsdCkge1xuICAgIGlmIChlbHQucXVlcnlTZWxlY3RvckFsbCkge1xuICAgICAgY29uc3QgYm9vc3RlZFNlbGVjdG9yID0gJywgW2h4LWJvb3N0XSBhLCBbZGF0YS1oeC1ib29zdF0gYSwgYVtoeC1ib29zdF0sIGFbZGF0YS1oeC1ib29zdF0nXG5cbiAgICAgIGNvbnN0IGV4dGVuc2lvblNlbGVjdG9ycyA9IFtdXG4gICAgICBmb3IgKGNvbnN0IGUgaW4gZXh0ZW5zaW9ucykge1xuICAgICAgICBjb25zdCBleHRlbnNpb24gPSBleHRlbnNpb25zW2VdXG4gICAgICAgIGlmIChleHRlbnNpb24uZ2V0U2VsZWN0b3JzKSB7XG4gICAgICAgICAgdmFyIHNlbGVjdG9ycyA9IGV4dGVuc2lvbi5nZXRTZWxlY3RvcnMoKVxuICAgICAgICAgIGlmIChzZWxlY3RvcnMpIHtcbiAgICAgICAgICAgIGV4dGVuc2lvblNlbGVjdG9ycy5wdXNoKHNlbGVjdG9ycylcbiAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgIH1cblxuICAgICAgY29uc3QgcmVzdWx0cyA9IGVsdC5xdWVyeVNlbGVjdG9yQWxsKFZFUkJfU0VMRUNUT1IgKyBib29zdGVkU2VsZWN0b3IgKyBcIiwgZm9ybSwgW3R5cGU9J3N1Ym1pdCddLFwiICtcbiAgICAgICAgJyBbaHgtZXh0XSwgW2RhdGEtaHgtZXh0XSwgW2h4LXRyaWdnZXJdLCBbZGF0YS1oeC10cmlnZ2VyXScgKyBleHRlbnNpb25TZWxlY3RvcnMuZmxhdCgpLm1hcChzID0+ICcsICcgKyBzKS5qb2luKCcnKSlcblxuICAgICAgcmV0dXJuIHJlc3VsdHNcbiAgICB9IGVsc2Uge1xuICAgICAgcmV0dXJuIFtdXG4gICAgfVxuICB9XG5cbiAgLyoqXG4gICAqIEhhbmRsZSBzdWJtaXQgYnV0dG9ucy9pbnB1dHMgdGhhdCBoYXZlIHRoZSBmb3JtIGF0dHJpYnV0ZSBzZXRcbiAgICogc2VlIGh0dHBzOi8vZGV2ZWxvcGVyLm1vemlsbGEub3JnL2RvY3MvV2ViL0hUTUwvRWxlbWVudC9idXR0b25cbiAgICogQHBhcmFtIHtFdmVudH0gZXZ0XG4gICAqL1xuICBmdW5jdGlvbiBtYXliZVNldExhc3RCdXR0b25DbGlja2VkKGV2dCkge1xuICAgIGNvbnN0IGVsdCA9IC8qKiBAdHlwZSB7SFRNTEJ1dHRvbkVsZW1lbnR8SFRNTElucHV0RWxlbWVudH0gKi8gKGNsb3Nlc3QoYXNFbGVtZW50KGV2dC50YXJnZXQpLCBcImJ1dHRvbiwgaW5wdXRbdHlwZT0nc3VibWl0J11cIikpXG4gICAgY29uc3QgaW50ZXJuYWxEYXRhID0gZ2V0UmVsYXRlZEZvcm1EYXRhKGV2dClcbiAgICBpZiAoaW50ZXJuYWxEYXRhKSB7XG4gICAgICBpbnRlcm5hbERhdGEubGFzdEJ1dHRvbkNsaWNrZWQgPSBlbHRcbiAgICB9XG4gIH1cblxuICAvKipcbiAgICogQHBhcmFtIHtFdmVudH0gZXZ0XG4gICAqL1xuICBmdW5jdGlvbiBtYXliZVVuc2V0TGFzdEJ1dHRvbkNsaWNrZWQoZXZ0KSB7XG4gICAgY29uc3QgaW50ZXJuYWxEYXRhID0gZ2V0UmVsYXRlZEZvcm1EYXRhKGV2dClcbiAgICBpZiAoaW50ZXJuYWxEYXRhKSB7XG4gICAgICBpbnRlcm5hbERhdGEubGFzdEJ1dHRvbkNsaWNrZWQgPSBudWxsXG4gICAgfVxuICB9XG5cbiAgLyoqXG4gICAqIEBwYXJhbSB7RXZlbnR9IGV2dFxuICAgKiBAcmV0dXJucyB7SHRteE5vZGVJbnRlcm5hbERhdGF8dW5kZWZpbmVkfVxuICAgKi9cbiAgZnVuY3Rpb24gZ2V0UmVsYXRlZEZvcm1EYXRhKGV2dCkge1xuICAgIGNvbnN0IGVsdCA9IGNsb3Nlc3QoYXNFbGVtZW50KGV2dC50YXJnZXQpLCBcImJ1dHRvbiwgaW5wdXRbdHlwZT0nc3VibWl0J11cIilcbiAgICBpZiAoIWVsdCkge1xuICAgICAgcmV0dXJuXG4gICAgfVxuICAgIGNvbnN0IGZvcm0gPSByZXNvbHZlVGFyZ2V0KCcjJyArIGdldFJhd0F0dHJpYnV0ZShlbHQsICdmb3JtJyksIGVsdC5nZXRSb290Tm9kZSgpKSB8fCBjbG9zZXN0KGVsdCwgJ2Zvcm0nKVxuICAgIGlmICghZm9ybSkge1xuICAgICAgcmV0dXJuXG4gICAgfVxuICAgIHJldHVybiBnZXRJbnRlcm5hbERhdGEoZm9ybSlcbiAgfVxuXG4gIC8qKlxuICAgKiBAcGFyYW0ge0V2ZW50VGFyZ2V0fSBlbHRcbiAgICovXG4gIGZ1bmN0aW9uIGluaXRCdXR0b25UcmFja2luZyhlbHQpIHtcbiAgICAvLyBuZWVkIHRvIGhhbmRsZSBib3RoIGNsaWNrIGFuZCBmb2N1cyBpbjpcbiAgICAvLyAgIGZvY3VzaW4gLSBpbiBjYXNlIHNvbWVvbmUgdGFicyBpbiB0byBhIGJ1dHRvbiBhbmQgaGl0cyB0aGUgc3BhY2UgYmFyXG4gICAgLy8gICBjbGljayAtIG9uIE9TWCBidXR0b25zIGRvIG5vdCBmb2N1cyBvbiBjbGljayBzZWUgaHR0cHM6Ly9idWdzLndlYmtpdC5vcmcvc2hvd19idWcuY2dpP2lkPTEzNzI0XG4gICAgZWx0LmFkZEV2ZW50TGlzdGVuZXIoJ2NsaWNrJywgbWF5YmVTZXRMYXN0QnV0dG9uQ2xpY2tlZClcbiAgICBlbHQuYWRkRXZlbnRMaXN0ZW5lcignZm9jdXNpbicsIG1heWJlU2V0TGFzdEJ1dHRvbkNsaWNrZWQpXG4gICAgZWx0LmFkZEV2ZW50TGlzdGVuZXIoJ2ZvY3Vzb3V0JywgbWF5YmVVbnNldExhc3RCdXR0b25DbGlja2VkKVxuICB9XG5cbiAgLyoqXG4gICAqIEBwYXJhbSB7RWxlbWVudH0gZWx0XG4gICAqIEBwYXJhbSB7c3RyaW5nfSBldmVudE5hbWVcbiAgICogQHBhcmFtIHtzdHJpbmd9IGNvZGVcbiAgICovXG4gIGZ1bmN0aW9uIGFkZEh4T25FdmVudEhhbmRsZXIoZWx0LCBldmVudE5hbWUsIGNvZGUpIHtcbiAgICBjb25zdCBub2RlRGF0YSA9IGdldEludGVybmFsRGF0YShlbHQpXG4gICAgaWYgKCFBcnJheS5pc0FycmF5KG5vZGVEYXRhLm9uSGFuZGxlcnMpKSB7XG4gICAgICBub2RlRGF0YS5vbkhhbmRsZXJzID0gW11cbiAgICB9XG4gICAgbGV0IGZ1bmNcbiAgICAvKiogQHR5cGUgRXZlbnRMaXN0ZW5lciAqL1xuICAgIGNvbnN0IGxpc3RlbmVyID0gZnVuY3Rpb24oZSkge1xuICAgICAgbWF5YmVFdmFsKGVsdCwgZnVuY3Rpb24oKSB7XG4gICAgICAgIGlmIChlbHRJc0Rpc2FibGVkKGVsdCkpIHtcbiAgICAgICAgICByZXR1cm5cbiAgICAgICAgfVxuICAgICAgICBpZiAoIWZ1bmMpIHtcbiAgICAgICAgICBmdW5jID0gbmV3IEZ1bmN0aW9uKCdldmVudCcsIGNvZGUpXG4gICAgICAgIH1cbiAgICAgICAgZnVuYy5jYWxsKGVsdCwgZSlcbiAgICAgIH0pXG4gICAgfVxuICAgIGVsdC5hZGRFdmVudExpc3RlbmVyKGV2ZW50TmFtZSwgbGlzdGVuZXIpXG4gICAgbm9kZURhdGEub25IYW5kbGVycy5wdXNoKHsgZXZlbnQ6IGV2ZW50TmFtZSwgbGlzdGVuZXIgfSlcbiAgfVxuXG4gIC8qKlxuICAgKiBAcGFyYW0ge0VsZW1lbnR9IGVsdFxuICAgKi9cbiAgZnVuY3Rpb24gcHJvY2Vzc0h4T25XaWxkY2FyZChlbHQpIHtcbiAgICAvLyB3aXBlIGFueSBwcmV2aW91cyBvbiBoYW5kbGVycyBzbyB0aGF0IHRoaXMgZnVuY3Rpb24gdGFrZXMgcHJlY2VkZW5jZVxuICAgIGRlSW5pdE9uSGFuZGxlcnMoZWx0KVxuXG4gICAgZm9yIChsZXQgaSA9IDA7IGkgPCBlbHQuYXR0cmlidXRlcy5sZW5ndGg7IGkrKykge1xuICAgICAgY29uc3QgbmFtZSA9IGVsdC5hdHRyaWJ1dGVzW2ldLm5hbWVcbiAgICAgIGNvbnN0IHZhbHVlID0gZWx0LmF0dHJpYnV0ZXNbaV0udmFsdWVcbiAgICAgIGlmIChzdGFydHNXaXRoKG5hbWUsICdoeC1vbicpIHx8IHN0YXJ0c1dpdGgobmFtZSwgJ2RhdGEtaHgtb24nKSkge1xuICAgICAgICBjb25zdCBhZnRlck9uUG9zaXRpb24gPSBuYW1lLmluZGV4T2YoJy1vbicpICsgM1xuICAgICAgICBjb25zdCBuZXh0Q2hhciA9IG5hbWUuc2xpY2UoYWZ0ZXJPblBvc2l0aW9uLCBhZnRlck9uUG9zaXRpb24gKyAxKVxuICAgICAgICBpZiAobmV4dENoYXIgPT09ICctJyB8fCBuZXh0Q2hhciA9PT0gJzonKSB7XG4gICAgICAgICAgbGV0IGV2ZW50TmFtZSA9IG5hbWUuc2xpY2UoYWZ0ZXJPblBvc2l0aW9uICsgMSlcbiAgICAgICAgICAvLyBpZiB0aGUgZXZlbnROYW1lIHN0YXJ0cyB3aXRoIGEgY29sb24gb3IgZGFzaCwgcHJlcGVuZCBcImh0bXhcIiBmb3Igc2hvcnRoYW5kIHN1cHBvcnRcbiAgICAgICAgICBpZiAoc3RhcnRzV2l0aChldmVudE5hbWUsICc6JykpIHtcbiAgICAgICAgICAgIGV2ZW50TmFtZSA9ICdodG14JyArIGV2ZW50TmFtZVxuICAgICAgICAgIH0gZWxzZSBpZiAoc3RhcnRzV2l0aChldmVudE5hbWUsICctJykpIHtcbiAgICAgICAgICAgIGV2ZW50TmFtZSA9ICdodG14OicgKyBldmVudE5hbWUuc2xpY2UoMSlcbiAgICAgICAgICB9IGVsc2UgaWYgKHN0YXJ0c1dpdGgoZXZlbnROYW1lLCAnaHRteC0nKSkge1xuICAgICAgICAgICAgZXZlbnROYW1lID0gJ2h0bXg6JyArIGV2ZW50TmFtZS5zbGljZSg1KVxuICAgICAgICAgIH1cblxuICAgICAgICAgIGFkZEh4T25FdmVudEhhbmRsZXIoZWx0LCBldmVudE5hbWUsIHZhbHVlKVxuICAgICAgICB9XG4gICAgICB9XG4gICAgfVxuICB9XG5cbiAgLyoqXG4gICAqIEBwYXJhbSB7RWxlbWVudHxIVE1MSW5wdXRFbGVtZW50fSBlbHRcbiAgICovXG4gIGZ1bmN0aW9uIGluaXROb2RlKGVsdCkge1xuICAgIGlmIChjbG9zZXN0KGVsdCwgaHRteC5jb25maWcuZGlzYWJsZVNlbGVjdG9yKSkge1xuICAgICAgY2xlYW5VcEVsZW1lbnQoZWx0KVxuICAgICAgcmV0dXJuXG4gICAgfVxuICAgIGNvbnN0IG5vZGVEYXRhID0gZ2V0SW50ZXJuYWxEYXRhKGVsdClcbiAgICBjb25zdCBhdHRySGFzaCA9IGF0dHJpYnV0ZUhhc2goZWx0KVxuICAgIGlmIChub2RlRGF0YS5pbml0SGFzaCAhPT0gYXR0ckhhc2gpIHtcbiAgICAgIC8vIGNsZWFuIHVwIGFueSBwcmV2aW91c2x5IHByb2Nlc3NlZCBpbmZvXG4gICAgICBkZUluaXROb2RlKGVsdClcblxuICAgICAgbm9kZURhdGEuaW5pdEhhc2ggPSBhdHRySGFzaFxuXG4gICAgICB0cmlnZ2VyRXZlbnQoZWx0LCAnaHRteDpiZWZvcmVQcm9jZXNzTm9kZScpXG5cbiAgICAgIGNvbnN0IHRyaWdnZXJTcGVjcyA9IGdldFRyaWdnZXJTcGVjcyhlbHQpXG4gICAgICBjb25zdCBoYXNFeHBsaWNpdEh0dHBBY3Rpb24gPSBwcm9jZXNzVmVyYnMoZWx0LCBub2RlRGF0YSwgdHJpZ2dlclNwZWNzKVxuXG4gICAgICBpZiAoIWhhc0V4cGxpY2l0SHR0cEFjdGlvbikge1xuICAgICAgICBpZiAoZ2V0Q2xvc2VzdEF0dHJpYnV0ZVZhbHVlKGVsdCwgJ2h4LWJvb3N0JykgPT09ICd0cnVlJykge1xuICAgICAgICAgIGJvb3N0RWxlbWVudChlbHQsIG5vZGVEYXRhLCB0cmlnZ2VyU3BlY3MpXG4gICAgICAgIH0gZWxzZSBpZiAoaGFzQXR0cmlidXRlKGVsdCwgJ2h4LXRyaWdnZXInKSkge1xuICAgICAgICAgIHRyaWdnZXJTcGVjcy5mb3JFYWNoKGZ1bmN0aW9uKHRyaWdnZXJTcGVjKSB7XG4gICAgICAgICAgICAvLyBGb3IgXCJuYWtlZFwiIHRyaWdnZXJzLCBkb24ndCBkbyBhbnl0aGluZyBhdCBhbGxcbiAgICAgICAgICAgIGFkZFRyaWdnZXJIYW5kbGVyKGVsdCwgdHJpZ2dlclNwZWMsIG5vZGVEYXRhLCBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgIH0pXG4gICAgICAgICAgfSlcbiAgICAgICAgfVxuICAgICAgfVxuXG4gICAgICAvLyBIYW5kbGUgc3VibWl0IGJ1dHRvbnMvaW5wdXRzIHRoYXQgaGF2ZSB0aGUgZm9ybSBhdHRyaWJ1dGUgc2V0XG4gICAgICAvLyBzZWUgaHR0cHM6Ly9kZXZlbG9wZXIubW96aWxsYS5vcmcvZG9jcy9XZWIvSFRNTC9FbGVtZW50L2J1dHRvblxuICAgICAgaWYgKGVsdC50YWdOYW1lID09PSAnRk9STScgfHwgKGdldFJhd0F0dHJpYnV0ZShlbHQsICd0eXBlJykgPT09ICdzdWJtaXQnICYmIGhhc0F0dHJpYnV0ZShlbHQsICdmb3JtJykpKSB7XG4gICAgICAgIGluaXRCdXR0b25UcmFja2luZyhlbHQpXG4gICAgICB9XG5cbiAgICAgIG5vZGVEYXRhLmZpcnN0SW5pdENvbXBsZXRlZCA9IHRydWVcbiAgICAgIHRyaWdnZXJFdmVudChlbHQsICdodG14OmFmdGVyUHJvY2Vzc05vZGUnKVxuICAgIH1cbiAgfVxuXG4gIC8qKlxuICAgKiBQcm9jZXNzZXMgbmV3IGNvbnRlbnQsIGVuYWJsaW5nIGh0bXggYmVoYXZpb3IuIFRoaXMgY2FuIGJlIHVzZWZ1bCBpZiB5b3UgaGF2ZSBjb250ZW50IHRoYXQgaXMgYWRkZWQgdG8gdGhlIERPTSBvdXRzaWRlIG9mIHRoZSBub3JtYWwgaHRteCByZXF1ZXN0IGN5Y2xlIGJ1dCBzdGlsbCB3YW50IGh0bXggYXR0cmlidXRlcyB0byB3b3JrLlxuICAgKlxuICAgKiBAc2VlIGh0dHBzOi8vaHRteC5vcmcvYXBpLyNwcm9jZXNzXG4gICAqXG4gICAqIEBwYXJhbSB7RWxlbWVudHxzdHJpbmd9IGVsdCBlbGVtZW50IHRvIHByb2Nlc3NcbiAgICovXG4gIGZ1bmN0aW9uIHByb2Nlc3NOb2RlKGVsdCkge1xuICAgIGVsdCA9IHJlc29sdmVUYXJnZXQoZWx0KVxuICAgIGlmIChjbG9zZXN0KGVsdCwgaHRteC5jb25maWcuZGlzYWJsZVNlbGVjdG9yKSkge1xuICAgICAgY2xlYW5VcEVsZW1lbnQoZWx0KVxuICAgICAgcmV0dXJuXG4gICAgfVxuICAgIGluaXROb2RlKGVsdClcbiAgICBmb3JFYWNoKGZpbmRFbGVtZW50c1RvUHJvY2VzcyhlbHQpLCBmdW5jdGlvbihjaGlsZCkgeyBpbml0Tm9kZShjaGlsZCkgfSlcbiAgICBmb3JFYWNoKGZpbmRIeE9uV2lsZGNhcmRFbGVtZW50cyhlbHQpLCBwcm9jZXNzSHhPbldpbGRjYXJkKVxuICB9XG5cbiAgLy89ID09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT1cbiAgLy8gRXZlbnQvTG9nIFN1cHBvcnRcbiAgLy89ID09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT1cblxuICAvKipcbiAgICogQHBhcmFtIHtzdHJpbmd9IHN0clxuICAgKiBAcmV0dXJucyB7c3RyaW5nfVxuICAgKi9cbiAgZnVuY3Rpb24ga2ViYWJFdmVudE5hbWUoc3RyKSB7XG4gICAgcmV0dXJuIHN0ci5yZXBsYWNlKC8oW2EtejAtOV0pKFtBLVpdKS9nLCAnJDEtJDInKS50b0xvd2VyQ2FzZSgpXG4gIH1cblxuICAvKipcbiAgICogQHBhcmFtIHtzdHJpbmd9IGV2ZW50TmFtZVxuICAgKiBAcGFyYW0ge2FueX0gZGV0YWlsXG4gICAqIEByZXR1cm5zIHtDdXN0b21FdmVudH1cbiAgICovXG4gIGZ1bmN0aW9uIG1ha2VFdmVudChldmVudE5hbWUsIGRldGFpbCkge1xuICAgIGxldCBldnRcbiAgICBpZiAod2luZG93LkN1c3RvbUV2ZW50ICYmIHR5cGVvZiB3aW5kb3cuQ3VzdG9tRXZlbnQgPT09ICdmdW5jdGlvbicpIHtcbiAgICAgIC8vIFRPRE86IGBjb21wb3NlZDogdHJ1ZWAgaGVyZSBpcyBhIGhhY2sgdG8gbWFrZSBnbG9iYWwgZXZlbnQgaGFuZGxlcnMgd29yayB3aXRoIGV2ZW50cyBpbiBzaGFkb3cgRE9NXG4gICAgICAvLyBUaGlzIGJyZWFrcyBleHBlY3RlZCBlbmNhcHN1bGF0aW9uIGJ1dCBuZWVkcyB0byBiZSBoZXJlIHVudGlsIGRlY2lkZWQgb3RoZXJ3aXNlIGJ5IGNvcmUgZGV2c1xuICAgICAgZXZ0ID0gbmV3IEN1c3RvbUV2ZW50KGV2ZW50TmFtZSwgeyBidWJibGVzOiB0cnVlLCBjYW5jZWxhYmxlOiB0cnVlLCBjb21wb3NlZDogdHJ1ZSwgZGV0YWlsIH0pXG4gICAgfSBlbHNlIHtcbiAgICAgIGV2dCA9IGdldERvY3VtZW50KCkuY3JlYXRlRXZlbnQoJ0N1c3RvbUV2ZW50JylcbiAgICAgIGV2dC5pbml0Q3VzdG9tRXZlbnQoZXZlbnROYW1lLCB0cnVlLCB0cnVlLCBkZXRhaWwpXG4gICAgfVxuICAgIHJldHVybiBldnRcbiAgfVxuXG4gIC8qKlxuICAgKiBAcGFyYW0ge0V2ZW50VGFyZ2V0fHN0cmluZ30gZWx0XG4gICAqIEBwYXJhbSB7c3RyaW5nfSBldmVudE5hbWVcbiAgICogQHBhcmFtIHthbnk9fSBkZXRhaWxcbiAgICovXG4gIGZ1bmN0aW9uIHRyaWdnZXJFcnJvckV2ZW50KGVsdCwgZXZlbnROYW1lLCBkZXRhaWwpIHtcbiAgICB0cmlnZ2VyRXZlbnQoZWx0LCBldmVudE5hbWUsIG1lcmdlT2JqZWN0cyh7IGVycm9yOiBldmVudE5hbWUgfSwgZGV0YWlsKSlcbiAgfVxuXG4gIC8qKlxuICAgKiBAcGFyYW0ge3N0cmluZ30gZXZlbnROYW1lXG4gICAqIEByZXR1cm5zIHtib29sZWFufVxuICAgKi9cbiAgZnVuY3Rpb24gaWdub3JlRXZlbnRGb3JMb2dnaW5nKGV2ZW50TmFtZSkge1xuICAgIHJldHVybiBldmVudE5hbWUgPT09ICdodG14OmFmdGVyUHJvY2Vzc05vZGUnXG4gIH1cblxuICAvKipcbiAgICogYHdpdGhFeHRlbnNpb25zYCBsb2NhdGVzIGFsbCBhY3RpdmUgZXh0ZW5zaW9ucyBmb3IgYSBwcm92aWRlZCBlbGVtZW50LCB0aGVuXG4gICAqIGV4ZWN1dGVzIHRoZSBwcm92aWRlZCBmdW5jdGlvbiB1c2luZyBlYWNoIG9mIHRoZSBhY3RpdmUgZXh0ZW5zaW9ucy4gIEl0IHNob3VsZFxuICAgKiBiZSBjYWxsZWQgaW50ZXJuYWxseSBhdCBldmVyeSBleHRlbmRhYmxlIGV4ZWN1dGlvbiBwb2ludCBpbiBodG14LlxuICAgKlxuICAgKiBAcGFyYW0ge0VsZW1lbnR9IGVsdFxuICAgKiBAcGFyYW0geyhleHRlbnNpb246SHRteEV4dGVuc2lvbikgPT4gdm9pZH0gdG9Eb1xuICAgKiBAcmV0dXJucyB2b2lkXG4gICAqL1xuICBmdW5jdGlvbiB3aXRoRXh0ZW5zaW9ucyhlbHQsIHRvRG8pIHtcbiAgICBmb3JFYWNoKGdldEV4dGVuc2lvbnMoZWx0KSwgZnVuY3Rpb24oZXh0ZW5zaW9uKSB7XG4gICAgICB0cnkge1xuICAgICAgICB0b0RvKGV4dGVuc2lvbilcbiAgICAgIH0gY2F0Y2ggKGUpIHtcbiAgICAgICAgbG9nRXJyb3IoZSlcbiAgICAgIH1cbiAgICB9KVxuICB9XG5cbiAgZnVuY3Rpb24gbG9nRXJyb3IobXNnKSB7XG4gICAgaWYgKGNvbnNvbGUuZXJyb3IpIHtcbiAgICAgIGNvbnNvbGUuZXJyb3IobXNnKVxuICAgIH0gZWxzZSBpZiAoY29uc29sZS5sb2cpIHtcbiAgICAgIGNvbnNvbGUubG9nKCdFUlJPUjogJywgbXNnKVxuICAgIH1cbiAgfVxuXG4gIC8qKlxuICAgKiBUcmlnZ2VycyBhIGdpdmVuIGV2ZW50IG9uIGFuIGVsZW1lbnRcbiAgICpcbiAgICogQHNlZSBodHRwczovL2h0bXgub3JnL2FwaS8jdHJpZ2dlclxuICAgKlxuICAgKiBAcGFyYW0ge0V2ZW50VGFyZ2V0fHN0cmluZ30gZWx0IHRoZSBlbGVtZW50IHRvIHRyaWdnZXIgdGhlIGV2ZW50IG9uXG4gICAqIEBwYXJhbSB7c3RyaW5nfSBldmVudE5hbWUgdGhlIG5hbWUgb2YgdGhlIGV2ZW50IHRvIHRyaWdnZXJcbiAgICogQHBhcmFtIHthbnk9fSBkZXRhaWwgZGV0YWlscyBmb3IgdGhlIGV2ZW50XG4gICAqIEByZXR1cm5zIHtib29sZWFufVxuICAgKi9cbiAgZnVuY3Rpb24gdHJpZ2dlckV2ZW50KGVsdCwgZXZlbnROYW1lLCBkZXRhaWwpIHtcbiAgICBlbHQgPSByZXNvbHZlVGFyZ2V0KGVsdClcbiAgICBpZiAoZGV0YWlsID09IG51bGwpIHtcbiAgICAgIGRldGFpbCA9IHt9XG4gICAgfVxuICAgIGRldGFpbC5lbHQgPSBlbHRcbiAgICBjb25zdCBldmVudCA9IG1ha2VFdmVudChldmVudE5hbWUsIGRldGFpbClcbiAgICBpZiAoaHRteC5sb2dnZXIgJiYgIWlnbm9yZUV2ZW50Rm9yTG9nZ2luZyhldmVudE5hbWUpKSB7XG4gICAgICBodG14LmxvZ2dlcihlbHQsIGV2ZW50TmFtZSwgZGV0YWlsKVxuICAgIH1cbiAgICBpZiAoZGV0YWlsLmVycm9yKSB7XG4gICAgICBsb2dFcnJvcihkZXRhaWwuZXJyb3IpXG4gICAgICB0cmlnZ2VyRXZlbnQoZWx0LCAnaHRteDplcnJvcicsIHsgZXJyb3JJbmZvOiBkZXRhaWwgfSlcbiAgICB9XG4gICAgbGV0IGV2ZW50UmVzdWx0ID0gZWx0LmRpc3BhdGNoRXZlbnQoZXZlbnQpXG4gICAgY29uc3Qga2ViYWJOYW1lID0ga2ViYWJFdmVudE5hbWUoZXZlbnROYW1lKVxuICAgIGlmIChldmVudFJlc3VsdCAmJiBrZWJhYk5hbWUgIT09IGV2ZW50TmFtZSkge1xuICAgICAgY29uc3Qga2ViYWJlZEV2ZW50ID0gbWFrZUV2ZW50KGtlYmFiTmFtZSwgZXZlbnQuZGV0YWlsKVxuICAgICAgZXZlbnRSZXN1bHQgPSBldmVudFJlc3VsdCAmJiBlbHQuZGlzcGF0Y2hFdmVudChrZWJhYmVkRXZlbnQpXG4gICAgfVxuICAgIHdpdGhFeHRlbnNpb25zKGFzRWxlbWVudChlbHQpLCBmdW5jdGlvbihleHRlbnNpb24pIHtcbiAgICAgIGV2ZW50UmVzdWx0ID0gZXZlbnRSZXN1bHQgJiYgKGV4dGVuc2lvbi5vbkV2ZW50KGV2ZW50TmFtZSwgZXZlbnQpICE9PSBmYWxzZSAmJiAhZXZlbnQuZGVmYXVsdFByZXZlbnRlZClcbiAgICB9KVxuICAgIHJldHVybiBldmVudFJlc3VsdFxuICB9XG5cbiAgLy89ID09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT1cbiAgLy8gSGlzdG9yeSBTdXBwb3J0XG4gIC8vPSA9PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09XG4gIGxldCBjdXJyZW50UGF0aEZvckhpc3RvcnkgPSBsb2NhdGlvbi5wYXRobmFtZSArIGxvY2F0aW9uLnNlYXJjaFxuXG4gIC8qKlxuICAgKiBAcmV0dXJucyB7RWxlbWVudH1cbiAgICovXG4gIGZ1bmN0aW9uIGdldEhpc3RvcnlFbGVtZW50KCkge1xuICAgIGNvbnN0IGhpc3RvcnlFbHQgPSBnZXREb2N1bWVudCgpLnF1ZXJ5U2VsZWN0b3IoJ1toeC1oaXN0b3J5LWVsdF0sW2RhdGEtaHgtaGlzdG9yeS1lbHRdJylcbiAgICByZXR1cm4gaGlzdG9yeUVsdCB8fCBnZXREb2N1bWVudCgpLmJvZHlcbiAgfVxuXG4gIC8qKlxuICAgKiBAcGFyYW0ge3N0cmluZ30gdXJsXG4gICAqIEBwYXJhbSB7RWxlbWVudH0gcm9vdEVsdFxuICAgKi9cbiAgZnVuY3Rpb24gc2F2ZVRvSGlzdG9yeUNhY2hlKHVybCwgcm9vdEVsdCkge1xuICAgIGlmICghY2FuQWNjZXNzTG9jYWxTdG9yYWdlKCkpIHtcbiAgICAgIHJldHVyblxuICAgIH1cblxuICAgIC8vIGdldCBzdGF0ZSB0byBzYXZlXG4gICAgY29uc3QgaW5uZXJIVE1MID0gY2xlYW5Jbm5lckh0bWxGb3JIaXN0b3J5KHJvb3RFbHQpXG4gICAgY29uc3QgdGl0bGUgPSBnZXREb2N1bWVudCgpLnRpdGxlXG4gICAgY29uc3Qgc2Nyb2xsID0gd2luZG93LnNjcm9sbFlcblxuICAgIGlmIChodG14LmNvbmZpZy5oaXN0b3J5Q2FjaGVTaXplIDw9IDApIHtcbiAgICAgIC8vIG1ha2Ugc3VyZSB0aGF0IGFuIGV2ZW50dWFsbHkgYWxyZWFkeSBleGlzdGluZyBjYWNoZSBpcyBwdXJnZWRcbiAgICAgIGxvY2FsU3RvcmFnZS5yZW1vdmVJdGVtKCdodG14LWhpc3RvcnktY2FjaGUnKVxuICAgICAgcmV0dXJuXG4gICAgfVxuXG4gICAgdXJsID0gbm9ybWFsaXplUGF0aCh1cmwpXG5cbiAgICBjb25zdCBoaXN0b3J5Q2FjaGUgPSBwYXJzZUpTT04obG9jYWxTdG9yYWdlLmdldEl0ZW0oJ2h0bXgtaGlzdG9yeS1jYWNoZScpKSB8fCBbXVxuICAgIGZvciAobGV0IGkgPSAwOyBpIDwgaGlzdG9yeUNhY2hlLmxlbmd0aDsgaSsrKSB7XG4gICAgICBpZiAoaGlzdG9yeUNhY2hlW2ldLnVybCA9PT0gdXJsKSB7XG4gICAgICAgIGhpc3RvcnlDYWNoZS5zcGxpY2UoaSwgMSlcbiAgICAgICAgYnJlYWtcbiAgICAgIH1cbiAgICB9XG5cbiAgICAvKiogQHR5cGUgSHRteEhpc3RvcnlJdGVtICovXG4gICAgY29uc3QgbmV3SGlzdG9yeUl0ZW0gPSB7IHVybCwgY29udGVudDogaW5uZXJIVE1MLCB0aXRsZSwgc2Nyb2xsIH1cblxuICAgIHRyaWdnZXJFdmVudChnZXREb2N1bWVudCgpLmJvZHksICdodG14Omhpc3RvcnlJdGVtQ3JlYXRlZCcsIHsgaXRlbTogbmV3SGlzdG9yeUl0ZW0sIGNhY2hlOiBoaXN0b3J5Q2FjaGUgfSlcblxuICAgIGhpc3RvcnlDYWNoZS5wdXNoKG5ld0hpc3RvcnlJdGVtKVxuICAgIHdoaWxlIChoaXN0b3J5Q2FjaGUubGVuZ3RoID4gaHRteC5jb25maWcuaGlzdG9yeUNhY2hlU2l6ZSkge1xuICAgICAgaGlzdG9yeUNhY2hlLnNoaWZ0KClcbiAgICB9XG5cbiAgICAvLyBrZWVwIHRyeWluZyB0byBzYXZlIHRoZSBjYWNoZSB1bnRpbCBpdCBzdWNjZWVkcyBvciBpcyBlbXB0eVxuICAgIHdoaWxlIChoaXN0b3J5Q2FjaGUubGVuZ3RoID4gMCkge1xuICAgICAgdHJ5IHtcbiAgICAgICAgbG9jYWxTdG9yYWdlLnNldEl0ZW0oJ2h0bXgtaGlzdG9yeS1jYWNoZScsIEpTT04uc3RyaW5naWZ5KGhpc3RvcnlDYWNoZSkpXG4gICAgICAgIGJyZWFrXG4gICAgICB9IGNhdGNoIChlKSB7XG4gICAgICAgIHRyaWdnZXJFcnJvckV2ZW50KGdldERvY3VtZW50KCkuYm9keSwgJ2h0bXg6aGlzdG9yeUNhY2hlRXJyb3InLCB7IGNhdXNlOiBlLCBjYWNoZTogaGlzdG9yeUNhY2hlIH0pXG4gICAgICAgIGhpc3RvcnlDYWNoZS5zaGlmdCgpIC8vIHNocmluayB0aGUgY2FjaGUgYW5kIHJldHJ5XG4gICAgICB9XG4gICAgfVxuICB9XG5cbiAgLyoqXG4gICAqIEB0eXBlZGVmIHtPYmplY3R9IEh0bXhIaXN0b3J5SXRlbVxuICAgKiBAcHJvcGVydHkge3N0cmluZ30gdXJsXG4gICAqIEBwcm9wZXJ0eSB7c3RyaW5nfSBjb250ZW50XG4gICAqIEBwcm9wZXJ0eSB7c3RyaW5nfSB0aXRsZVxuICAgKiBAcHJvcGVydHkge251bWJlcn0gc2Nyb2xsXG4gICAqL1xuXG4gIC8qKlxuICAgKiBAcGFyYW0ge3N0cmluZ30gdXJsXG4gICAqIEByZXR1cm5zIHtIdG14SGlzdG9yeUl0ZW18bnVsbH1cbiAgICovXG4gIGZ1bmN0aW9uIGdldENhY2hlZEhpc3RvcnkodXJsKSB7XG4gICAgaWYgKCFjYW5BY2Nlc3NMb2NhbFN0b3JhZ2UoKSkge1xuICAgICAgcmV0dXJuIG51bGxcbiAgICB9XG5cbiAgICB1cmwgPSBub3JtYWxpemVQYXRoKHVybClcblxuICAgIGNvbnN0IGhpc3RvcnlDYWNoZSA9IHBhcnNlSlNPTihsb2NhbFN0b3JhZ2UuZ2V0SXRlbSgnaHRteC1oaXN0b3J5LWNhY2hlJykpIHx8IFtdXG4gICAgZm9yIChsZXQgaSA9IDA7IGkgPCBoaXN0b3J5Q2FjaGUubGVuZ3RoOyBpKyspIHtcbiAgICAgIGlmIChoaXN0b3J5Q2FjaGVbaV0udXJsID09PSB1cmwpIHtcbiAgICAgICAgcmV0dXJuIGhpc3RvcnlDYWNoZVtpXVxuICAgICAgfVxuICAgIH1cbiAgICByZXR1cm4gbnVsbFxuICB9XG5cbiAgLyoqXG4gICAqIEBwYXJhbSB7RWxlbWVudH0gZWx0XG4gICAqIEByZXR1cm5zIHtzdHJpbmd9XG4gICAqL1xuICBmdW5jdGlvbiBjbGVhbklubmVySHRtbEZvckhpc3RvcnkoZWx0KSB7XG4gICAgY29uc3QgY2xhc3NOYW1lID0gaHRteC5jb25maWcucmVxdWVzdENsYXNzXG4gICAgY29uc3QgY2xvbmUgPSAvKiogQHR5cGUgRWxlbWVudCAqLyAoZWx0LmNsb25lTm9kZSh0cnVlKSlcbiAgICBmb3JFYWNoKGZpbmRBbGwoY2xvbmUsICcuJyArIGNsYXNzTmFtZSksIGZ1bmN0aW9uKGNoaWxkKSB7XG4gICAgICByZW1vdmVDbGFzc0Zyb21FbGVtZW50KGNoaWxkLCBjbGFzc05hbWUpXG4gICAgfSlcbiAgICAvLyByZW1vdmUgdGhlIGRpc2FibGVkIGF0dHJpYnV0ZSBmb3IgYW55IGVsZW1lbnQgZGlzYWJsZWQgZHVlIHRvIGFuIGh0bXggcmVxdWVzdFxuICAgIGZvckVhY2goZmluZEFsbChjbG9uZSwgJ1tkYXRhLWRpc2FibGVkLWJ5LWh0bXhdJyksIGZ1bmN0aW9uKGNoaWxkKSB7XG4gICAgICBjaGlsZC5yZW1vdmVBdHRyaWJ1dGUoJ2Rpc2FibGVkJylcbiAgICB9KVxuICAgIHJldHVybiBjbG9uZS5pbm5lckhUTUxcbiAgfVxuXG4gIGZ1bmN0aW9uIHNhdmVDdXJyZW50UGFnZVRvSGlzdG9yeSgpIHtcbiAgICBjb25zdCBlbHQgPSBnZXRIaXN0b3J5RWxlbWVudCgpXG4gICAgY29uc3QgcGF0aCA9IGN1cnJlbnRQYXRoRm9ySGlzdG9yeSB8fCBsb2NhdGlvbi5wYXRobmFtZSArIGxvY2F0aW9uLnNlYXJjaFxuXG4gICAgLy8gQWxsb3cgaGlzdG9yeSBzbmFwc2hvdCBmZWF0dXJlIHRvIGJlIGRpc2FibGVkIHdoZXJlIGh4LWhpc3Rvcnk9XCJmYWxzZVwiXG4gICAgLy8gaXMgcHJlc2VudCAqYW55d2hlcmUqIGluIHRoZSBjdXJyZW50IGRvY3VtZW50IHdlJ3JlIGFib3V0IHRvIHNhdmUsXG4gICAgLy8gc28gd2UgY2FuIHByZXZlbnQgcHJpdmlsZWdlZCBkYXRhIGVudGVyaW5nIHRoZSBjYWNoZS5cbiAgICAvLyBUaGUgcGFnZSB3aWxsIHN0aWxsIGJlIHJlYWNoYWJsZSBhcyBhIGhpc3RvcnkgZW50cnksIGJ1dCBodG14IHdpbGwgZmV0Y2ggaXRcbiAgICAvLyBsaXZlIGZyb20gdGhlIHNlcnZlciBvbnBvcHN0YXRlIHJhdGhlciB0aGFuIGxvb2sgaW4gdGhlIGxvY2FsU3RvcmFnZSBjYWNoZVxuICAgIGxldCBkaXNhYmxlSGlzdG9yeUNhY2hlXG4gICAgdHJ5IHtcbiAgICAgIGRpc2FibGVIaXN0b3J5Q2FjaGUgPSBnZXREb2N1bWVudCgpLnF1ZXJ5U2VsZWN0b3IoJ1toeC1oaXN0b3J5PVwiZmFsc2VcIiBpXSxbZGF0YS1oeC1oaXN0b3J5PVwiZmFsc2VcIiBpXScpXG4gICAgfSBjYXRjaCAoZSkge1xuICAgIC8vIElFMTE6IGluc2Vuc2l0aXZlIG1vZGlmaWVyIG5vdCBzdXBwb3J0ZWQgc28gZmFsbGJhY2sgdG8gY2FzZSBzZW5zaXRpdmUgc2VsZWN0b3JcbiAgICAgIGRpc2FibGVIaXN0b3J5Q2FjaGUgPSBnZXREb2N1bWVudCgpLnF1ZXJ5U2VsZWN0b3IoJ1toeC1oaXN0b3J5PVwiZmFsc2VcIl0sW2RhdGEtaHgtaGlzdG9yeT1cImZhbHNlXCJdJylcbiAgICB9XG4gICAgaWYgKCFkaXNhYmxlSGlzdG9yeUNhY2hlKSB7XG4gICAgICB0cmlnZ2VyRXZlbnQoZ2V0RG9jdW1lbnQoKS5ib2R5LCAnaHRteDpiZWZvcmVIaXN0b3J5U2F2ZScsIHsgcGF0aCwgaGlzdG9yeUVsdDogZWx0IH0pXG4gICAgICBzYXZlVG9IaXN0b3J5Q2FjaGUocGF0aCwgZWx0KVxuICAgIH1cblxuICAgIGlmIChodG14LmNvbmZpZy5oaXN0b3J5RW5hYmxlZCkgaGlzdG9yeS5yZXBsYWNlU3RhdGUoeyBodG14OiB0cnVlIH0sIGdldERvY3VtZW50KCkudGl0bGUsIHdpbmRvdy5sb2NhdGlvbi5ocmVmKVxuICB9XG5cbiAgLyoqXG4gICAqIEBwYXJhbSB7c3RyaW5nfSBwYXRoXG4gICAqL1xuICBmdW5jdGlvbiBwdXNoVXJsSW50b0hpc3RvcnkocGF0aCkge1xuICAvLyByZW1vdmUgdGhlIGNhY2hlIGJ1c3RlciBwYXJhbWV0ZXIsIGlmIGFueVxuICAgIGlmIChodG14LmNvbmZpZy5nZXRDYWNoZUJ1c3RlclBhcmFtKSB7XG4gICAgICBwYXRoID0gcGF0aC5yZXBsYWNlKC9vcmdcXC5odG14XFwuY2FjaGUtYnVzdGVyPVteJl0qJj8vLCAnJylcbiAgICAgIGlmIChlbmRzV2l0aChwYXRoLCAnJicpIHx8IGVuZHNXaXRoKHBhdGgsICc/JykpIHtcbiAgICAgICAgcGF0aCA9IHBhdGguc2xpY2UoMCwgLTEpXG4gICAgICB9XG4gICAgfVxuICAgIGlmIChodG14LmNvbmZpZy5oaXN0b3J5RW5hYmxlZCkge1xuICAgICAgaGlzdG9yeS5wdXNoU3RhdGUoeyBodG14OiB0cnVlIH0sICcnLCBwYXRoKVxuICAgIH1cbiAgICBjdXJyZW50UGF0aEZvckhpc3RvcnkgPSBwYXRoXG4gIH1cblxuICAvKipcbiAgICogQHBhcmFtIHtzdHJpbmd9IHBhdGhcbiAgICovXG4gIGZ1bmN0aW9uIHJlcGxhY2VVcmxJbkhpc3RvcnkocGF0aCkge1xuICAgIGlmIChodG14LmNvbmZpZy5oaXN0b3J5RW5hYmxlZCkgaGlzdG9yeS5yZXBsYWNlU3RhdGUoeyBodG14OiB0cnVlIH0sICcnLCBwYXRoKVxuICAgIGN1cnJlbnRQYXRoRm9ySGlzdG9yeSA9IHBhdGhcbiAgfVxuXG4gIC8qKlxuICAgKiBAcGFyYW0ge0h0bXhTZXR0bGVUYXNrW119IHRhc2tzXG4gICAqL1xuICBmdW5jdGlvbiBzZXR0bGVJbW1lZGlhdGVseSh0YXNrcykge1xuICAgIGZvckVhY2godGFza3MsIGZ1bmN0aW9uKHRhc2spIHtcbiAgICAgIHRhc2suY2FsbCh1bmRlZmluZWQpXG4gICAgfSlcbiAgfVxuXG4gIC8qKlxuICAgKiBAcGFyYW0ge3N0cmluZ30gcGF0aFxuICAgKi9cbiAgZnVuY3Rpb24gbG9hZEhpc3RvcnlGcm9tU2VydmVyKHBhdGgpIHtcbiAgICBjb25zdCByZXF1ZXN0ID0gbmV3IFhNTEh0dHBSZXF1ZXN0KClcbiAgICBjb25zdCBkZXRhaWxzID0geyBwYXRoLCB4aHI6IHJlcXVlc3QgfVxuICAgIHRyaWdnZXJFdmVudChnZXREb2N1bWVudCgpLmJvZHksICdodG14Omhpc3RvcnlDYWNoZU1pc3MnLCBkZXRhaWxzKVxuICAgIHJlcXVlc3Qub3BlbignR0VUJywgcGF0aCwgdHJ1ZSlcbiAgICByZXF1ZXN0LnNldFJlcXVlc3RIZWFkZXIoJ0hYLVJlcXVlc3QnLCAndHJ1ZScpXG4gICAgcmVxdWVzdC5zZXRSZXF1ZXN0SGVhZGVyKCdIWC1IaXN0b3J5LVJlc3RvcmUtUmVxdWVzdCcsICd0cnVlJylcbiAgICByZXF1ZXN0LnNldFJlcXVlc3RIZWFkZXIoJ0hYLUN1cnJlbnQtVVJMJywgZ2V0RG9jdW1lbnQoKS5sb2NhdGlvbi5ocmVmKVxuICAgIHJlcXVlc3Qub25sb2FkID0gZnVuY3Rpb24oKSB7XG4gICAgICBpZiAodGhpcy5zdGF0dXMgPj0gMjAwICYmIHRoaXMuc3RhdHVzIDwgNDAwKSB7XG4gICAgICAgIHRyaWdnZXJFdmVudChnZXREb2N1bWVudCgpLmJvZHksICdodG14Omhpc3RvcnlDYWNoZU1pc3NMb2FkJywgZGV0YWlscylcbiAgICAgICAgY29uc3QgZnJhZ21lbnQgPSBtYWtlRnJhZ21lbnQodGhpcy5yZXNwb25zZSlcbiAgICAgICAgLyoqIEB0eXBlIFBhcmVudE5vZGUgKi9cbiAgICAgICAgY29uc3QgY29udGVudCA9IGZyYWdtZW50LnF1ZXJ5U2VsZWN0b3IoJ1toeC1oaXN0b3J5LWVsdF0sW2RhdGEtaHgtaGlzdG9yeS1lbHRdJykgfHwgZnJhZ21lbnRcbiAgICAgICAgY29uc3QgaGlzdG9yeUVsZW1lbnQgPSBnZXRIaXN0b3J5RWxlbWVudCgpXG4gICAgICAgIGNvbnN0IHNldHRsZUluZm8gPSBtYWtlU2V0dGxlSW5mbyhoaXN0b3J5RWxlbWVudClcbiAgICAgICAgaGFuZGxlVGl0bGUoZnJhZ21lbnQudGl0bGUpXG5cbiAgICAgICAgaGFuZGxlUHJlc2VydmVkRWxlbWVudHMoZnJhZ21lbnQpXG4gICAgICAgIHN3YXBJbm5lckhUTUwoaGlzdG9yeUVsZW1lbnQsIGNvbnRlbnQsIHNldHRsZUluZm8pXG4gICAgICAgIHJlc3RvcmVQcmVzZXJ2ZWRFbGVtZW50cygpXG4gICAgICAgIHNldHRsZUltbWVkaWF0ZWx5KHNldHRsZUluZm8udGFza3MpXG4gICAgICAgIGN1cnJlbnRQYXRoRm9ySGlzdG9yeSA9IHBhdGhcbiAgICAgICAgdHJpZ2dlckV2ZW50KGdldERvY3VtZW50KCkuYm9keSwgJ2h0bXg6aGlzdG9yeVJlc3RvcmUnLCB7IHBhdGgsIGNhY2hlTWlzczogdHJ1ZSwgc2VydmVyUmVzcG9uc2U6IHRoaXMucmVzcG9uc2UgfSlcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIHRyaWdnZXJFcnJvckV2ZW50KGdldERvY3VtZW50KCkuYm9keSwgJ2h0bXg6aGlzdG9yeUNhY2hlTWlzc0xvYWRFcnJvcicsIGRldGFpbHMpXG4gICAgICB9XG4gICAgfVxuICAgIHJlcXVlc3Quc2VuZCgpXG4gIH1cblxuICAvKipcbiAgICogQHBhcmFtIHtzdHJpbmd9IFtwYXRoXVxuICAgKi9cbiAgZnVuY3Rpb24gcmVzdG9yZUhpc3RvcnkocGF0aCkge1xuICAgIHNhdmVDdXJyZW50UGFnZVRvSGlzdG9yeSgpXG4gICAgcGF0aCA9IHBhdGggfHwgbG9jYXRpb24ucGF0aG5hbWUgKyBsb2NhdGlvbi5zZWFyY2hcbiAgICBjb25zdCBjYWNoZWQgPSBnZXRDYWNoZWRIaXN0b3J5KHBhdGgpXG4gICAgaWYgKGNhY2hlZCkge1xuICAgICAgY29uc3QgZnJhZ21lbnQgPSBtYWtlRnJhZ21lbnQoY2FjaGVkLmNvbnRlbnQpXG4gICAgICBjb25zdCBoaXN0b3J5RWxlbWVudCA9IGdldEhpc3RvcnlFbGVtZW50KClcbiAgICAgIGNvbnN0IHNldHRsZUluZm8gPSBtYWtlU2V0dGxlSW5mbyhoaXN0b3J5RWxlbWVudClcbiAgICAgIGhhbmRsZVRpdGxlKGNhY2hlZC50aXRsZSlcbiAgICAgIGhhbmRsZVByZXNlcnZlZEVsZW1lbnRzKGZyYWdtZW50KVxuICAgICAgc3dhcElubmVySFRNTChoaXN0b3J5RWxlbWVudCwgZnJhZ21lbnQsIHNldHRsZUluZm8pXG4gICAgICByZXN0b3JlUHJlc2VydmVkRWxlbWVudHMoKVxuICAgICAgc2V0dGxlSW1tZWRpYXRlbHkoc2V0dGxlSW5mby50YXNrcylcbiAgICAgIGdldFdpbmRvdygpLnNldFRpbWVvdXQoZnVuY3Rpb24oKSB7XG4gICAgICAgIHdpbmRvdy5zY3JvbGxUbygwLCBjYWNoZWQuc2Nyb2xsKVxuICAgICAgfSwgMCkgLy8gbmV4dCAndGljaycsIHNvIGJyb3dzZXIgaGFzIHRpbWUgdG8gcmVuZGVyIGxheW91dFxuICAgICAgY3VycmVudFBhdGhGb3JIaXN0b3J5ID0gcGF0aFxuICAgICAgdHJpZ2dlckV2ZW50KGdldERvY3VtZW50KCkuYm9keSwgJ2h0bXg6aGlzdG9yeVJlc3RvcmUnLCB7IHBhdGgsIGl0ZW06IGNhY2hlZCB9KVxuICAgIH0gZWxzZSB7XG4gICAgICBpZiAoaHRteC5jb25maWcucmVmcmVzaE9uSGlzdG9yeU1pc3MpIHtcbiAgICAgICAgLy8gQHRzLWlnbm9yZTogb3B0aW9uYWwgcGFyYW1ldGVyIGluIHJlbG9hZCgpIGZ1bmN0aW9uIHRocm93cyBlcnJvclxuICAgICAgICAvLyBub2luc3BlY3Rpb24gSlNVbnJlc29sdmVkUmVmZXJlbmNlXG4gICAgICAgIHdpbmRvdy5sb2NhdGlvbi5yZWxvYWQodHJ1ZSlcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIGxvYWRIaXN0b3J5RnJvbVNlcnZlcihwYXRoKVxuICAgICAgfVxuICAgIH1cbiAgfVxuXG4gIC8qKlxuICAgKiBAcGFyYW0ge0VsZW1lbnR9IGVsdFxuICAgKiBAcmV0dXJucyB7RWxlbWVudFtdfVxuICAgKi9cbiAgZnVuY3Rpb24gYWRkUmVxdWVzdEluZGljYXRvckNsYXNzZXMoZWx0KSB7XG4gICAgbGV0IGluZGljYXRvcnMgPSAvKiogQHR5cGUgRWxlbWVudFtdICovIChmaW5kQXR0cmlidXRlVGFyZ2V0cyhlbHQsICdoeC1pbmRpY2F0b3InKSlcbiAgICBpZiAoaW5kaWNhdG9ycyA9PSBudWxsKSB7XG4gICAgICBpbmRpY2F0b3JzID0gW2VsdF1cbiAgICB9XG4gICAgZm9yRWFjaChpbmRpY2F0b3JzLCBmdW5jdGlvbihpYykge1xuICAgICAgY29uc3QgaW50ZXJuYWxEYXRhID0gZ2V0SW50ZXJuYWxEYXRhKGljKVxuICAgICAgaW50ZXJuYWxEYXRhLnJlcXVlc3RDb3VudCA9IChpbnRlcm5hbERhdGEucmVxdWVzdENvdW50IHx8IDApICsgMVxuICAgICAgaWMuY2xhc3NMaXN0LmFkZC5jYWxsKGljLmNsYXNzTGlzdCwgaHRteC5jb25maWcucmVxdWVzdENsYXNzKVxuICAgIH0pXG4gICAgcmV0dXJuIGluZGljYXRvcnNcbiAgfVxuXG4gIC8qKlxuICAgKiBAcGFyYW0ge0VsZW1lbnR9IGVsdFxuICAgKiBAcmV0dXJucyB7RWxlbWVudFtdfVxuICAgKi9cbiAgZnVuY3Rpb24gZGlzYWJsZUVsZW1lbnRzKGVsdCkge1xuICAgIGxldCBkaXNhYmxlZEVsdHMgPSAvKiogQHR5cGUgRWxlbWVudFtdICovIChmaW5kQXR0cmlidXRlVGFyZ2V0cyhlbHQsICdoeC1kaXNhYmxlZC1lbHQnKSlcbiAgICBpZiAoZGlzYWJsZWRFbHRzID09IG51bGwpIHtcbiAgICAgIGRpc2FibGVkRWx0cyA9IFtdXG4gICAgfVxuICAgIGZvckVhY2goZGlzYWJsZWRFbHRzLCBmdW5jdGlvbihkaXNhYmxlZEVsZW1lbnQpIHtcbiAgICAgIGNvbnN0IGludGVybmFsRGF0YSA9IGdldEludGVybmFsRGF0YShkaXNhYmxlZEVsZW1lbnQpXG4gICAgICBpbnRlcm5hbERhdGEucmVxdWVzdENvdW50ID0gKGludGVybmFsRGF0YS5yZXF1ZXN0Q291bnQgfHwgMCkgKyAxXG4gICAgICBkaXNhYmxlZEVsZW1lbnQuc2V0QXR0cmlidXRlKCdkaXNhYmxlZCcsICcnKVxuICAgICAgZGlzYWJsZWRFbGVtZW50LnNldEF0dHJpYnV0ZSgnZGF0YS1kaXNhYmxlZC1ieS1odG14JywgJycpXG4gICAgfSlcbiAgICByZXR1cm4gZGlzYWJsZWRFbHRzXG4gIH1cblxuICAvKipcbiAgICogQHBhcmFtIHtFbGVtZW50W119IGluZGljYXRvcnNcbiAgICogQHBhcmFtIHtFbGVtZW50W119IGRpc2FibGVkXG4gICAqL1xuICBmdW5jdGlvbiByZW1vdmVSZXF1ZXN0SW5kaWNhdG9ycyhpbmRpY2F0b3JzLCBkaXNhYmxlZCkge1xuICAgIGZvckVhY2goaW5kaWNhdG9ycy5jb25jYXQoZGlzYWJsZWQpLCBmdW5jdGlvbihlbGUpIHtcbiAgICAgIGNvbnN0IGludGVybmFsRGF0YSA9IGdldEludGVybmFsRGF0YShlbGUpXG4gICAgICBpbnRlcm5hbERhdGEucmVxdWVzdENvdW50ID0gKGludGVybmFsRGF0YS5yZXF1ZXN0Q291bnQgfHwgMSkgLSAxXG4gICAgfSlcbiAgICBmb3JFYWNoKGluZGljYXRvcnMsIGZ1bmN0aW9uKGljKSB7XG4gICAgICBjb25zdCBpbnRlcm5hbERhdGEgPSBnZXRJbnRlcm5hbERhdGEoaWMpXG4gICAgICBpZiAoaW50ZXJuYWxEYXRhLnJlcXVlc3RDb3VudCA9PT0gMCkge1xuICAgICAgICBpYy5jbGFzc0xpc3QucmVtb3ZlLmNhbGwoaWMuY2xhc3NMaXN0LCBodG14LmNvbmZpZy5yZXF1ZXN0Q2xhc3MpXG4gICAgICB9XG4gICAgfSlcbiAgICBmb3JFYWNoKGRpc2FibGVkLCBmdW5jdGlvbihkaXNhYmxlZEVsZW1lbnQpIHtcbiAgICAgIGNvbnN0IGludGVybmFsRGF0YSA9IGdldEludGVybmFsRGF0YShkaXNhYmxlZEVsZW1lbnQpXG4gICAgICBpZiAoaW50ZXJuYWxEYXRhLnJlcXVlc3RDb3VudCA9PT0gMCkge1xuICAgICAgICBkaXNhYmxlZEVsZW1lbnQucmVtb3ZlQXR0cmlidXRlKCdkaXNhYmxlZCcpXG4gICAgICAgIGRpc2FibGVkRWxlbWVudC5yZW1vdmVBdHRyaWJ1dGUoJ2RhdGEtZGlzYWJsZWQtYnktaHRteCcpXG4gICAgICB9XG4gICAgfSlcbiAgfVxuXG4gIC8vPSA9PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09XG4gIC8vIElucHV0IFZhbHVlIFByb2Nlc3NpbmdcbiAgLy89ID09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT1cblxuICAvKipcbiAgICogQHBhcmFtIHtFbGVtZW50W119IHByb2Nlc3NlZFxuICAgKiBAcGFyYW0ge0VsZW1lbnR9IGVsdFxuICAgKiBAcmV0dXJucyB7Ym9vbGVhbn1cbiAgICovXG4gIGZ1bmN0aW9uIGhhdmVTZWVuTm9kZShwcm9jZXNzZWQsIGVsdCkge1xuICAgIGZvciAobGV0IGkgPSAwOyBpIDwgcHJvY2Vzc2VkLmxlbmd0aDsgaSsrKSB7XG4gICAgICBjb25zdCBub2RlID0gcHJvY2Vzc2VkW2ldXG4gICAgICBpZiAobm9kZS5pc1NhbWVOb2RlKGVsdCkpIHtcbiAgICAgICAgcmV0dXJuIHRydWVcbiAgICAgIH1cbiAgICB9XG4gICAgcmV0dXJuIGZhbHNlXG4gIH1cblxuICAvKipcbiAgICogQHBhcmFtIHtFbGVtZW50fSBlbGVtZW50XG4gICAqIEByZXR1cm4ge2Jvb2xlYW59XG4gICAqL1xuICBmdW5jdGlvbiBzaG91bGRJbmNsdWRlKGVsZW1lbnQpIHtcbiAgICAvLyBDYXN0IHRvIHRyaWNrIHRzYywgdW5kZWZpbmVkIHZhbHVlcyB3aWxsIHdvcmsgZmluZSBoZXJlXG4gICAgY29uc3QgZWx0ID0gLyoqIEB0eXBlIHtIVE1MSW5wdXRFbGVtZW50fSAqLyAoZWxlbWVudClcbiAgICBpZiAoZWx0Lm5hbWUgPT09ICcnIHx8IGVsdC5uYW1lID09IG51bGwgfHwgZWx0LmRpc2FibGVkIHx8IGNsb3Nlc3QoZWx0LCAnZmllbGRzZXRbZGlzYWJsZWRdJykpIHtcbiAgICAgIHJldHVybiBmYWxzZVxuICAgIH1cbiAgICAvLyBpZ25vcmUgXCJzdWJtaXR0ZXJcIiB0eXBlcyAoc2VlIGpRdWVyeSBzcmMvc2VyaWFsaXplLmpzKVxuICAgIGlmIChlbHQudHlwZSA9PT0gJ2J1dHRvbicgfHwgZWx0LnR5cGUgPT09ICdzdWJtaXQnIHx8IGVsdC50YWdOYW1lID09PSAnaW1hZ2UnIHx8IGVsdC50YWdOYW1lID09PSAncmVzZXQnIHx8IGVsdC50YWdOYW1lID09PSAnZmlsZScpIHtcbiAgICAgIHJldHVybiBmYWxzZVxuICAgIH1cbiAgICBpZiAoZWx0LnR5cGUgPT09ICdjaGVja2JveCcgfHwgZWx0LnR5cGUgPT09ICdyYWRpbycpIHtcbiAgICAgIHJldHVybiBlbHQuY2hlY2tlZFxuICAgIH1cbiAgICByZXR1cm4gdHJ1ZVxuICB9XG5cbiAgLyoqIEBwYXJhbSB7c3RyaW5nfSBuYW1lXG4gICAqIEBwYXJhbSB7c3RyaW5nfEFycmF5fEZvcm1EYXRhRW50cnlWYWx1ZX0gdmFsdWVcbiAgICogQHBhcmFtIHtGb3JtRGF0YX0gZm9ybURhdGEgKi9cbiAgZnVuY3Rpb24gYWRkVmFsdWVUb0Zvcm1EYXRhKG5hbWUsIHZhbHVlLCBmb3JtRGF0YSkge1xuICAgIGlmIChuYW1lICE9IG51bGwgJiYgdmFsdWUgIT0gbnVsbCkge1xuICAgICAgaWYgKEFycmF5LmlzQXJyYXkodmFsdWUpKSB7XG4gICAgICAgIHZhbHVlLmZvckVhY2goZnVuY3Rpb24odikgeyBmb3JtRGF0YS5hcHBlbmQobmFtZSwgdikgfSlcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIGZvcm1EYXRhLmFwcGVuZChuYW1lLCB2YWx1ZSlcbiAgICAgIH1cbiAgICB9XG4gIH1cblxuICAvKiogQHBhcmFtIHtzdHJpbmd9IG5hbWVcbiAgICogQHBhcmFtIHtzdHJpbmd8QXJyYXl9IHZhbHVlXG4gICAqIEBwYXJhbSB7Rm9ybURhdGF9IGZvcm1EYXRhICovXG4gIGZ1bmN0aW9uIHJlbW92ZVZhbHVlRnJvbUZvcm1EYXRhKG5hbWUsIHZhbHVlLCBmb3JtRGF0YSkge1xuICAgIGlmIChuYW1lICE9IG51bGwgJiYgdmFsdWUgIT0gbnVsbCkge1xuICAgICAgbGV0IHZhbHVlcyA9IGZvcm1EYXRhLmdldEFsbChuYW1lKVxuICAgICAgaWYgKEFycmF5LmlzQXJyYXkodmFsdWUpKSB7XG4gICAgICAgIHZhbHVlcyA9IHZhbHVlcy5maWx0ZXIodiA9PiB2YWx1ZS5pbmRleE9mKHYpIDwgMClcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIHZhbHVlcyA9IHZhbHVlcy5maWx0ZXIodiA9PiB2ICE9PSB2YWx1ZSlcbiAgICAgIH1cbiAgICAgIGZvcm1EYXRhLmRlbGV0ZShuYW1lKVxuICAgICAgZm9yRWFjaCh2YWx1ZXMsIHYgPT4gZm9ybURhdGEuYXBwZW5kKG5hbWUsIHYpKVxuICAgIH1cbiAgfVxuXG4gIC8qKlxuICAgKiBAcGFyYW0ge0VsZW1lbnRbXX0gcHJvY2Vzc2VkXG4gICAqIEBwYXJhbSB7Rm9ybURhdGF9IGZvcm1EYXRhXG4gICAqIEBwYXJhbSB7SHRteEVsZW1lbnRWYWxpZGF0aW9uRXJyb3JbXX0gZXJyb3JzXG4gICAqIEBwYXJhbSB7RWxlbWVudHxIVE1MSW5wdXRFbGVtZW50fEhUTUxTZWxlY3RFbGVtZW50fEhUTUxGb3JtRWxlbWVudH0gZWx0XG4gICAqIEBwYXJhbSB7Ym9vbGVhbn0gdmFsaWRhdGVcbiAgICovXG4gIGZ1bmN0aW9uIHByb2Nlc3NJbnB1dFZhbHVlKHByb2Nlc3NlZCwgZm9ybURhdGEsIGVycm9ycywgZWx0LCB2YWxpZGF0ZSkge1xuICAgIGlmIChlbHQgPT0gbnVsbCB8fCBoYXZlU2Vlbk5vZGUocHJvY2Vzc2VkLCBlbHQpKSB7XG4gICAgICByZXR1cm5cbiAgICB9IGVsc2Uge1xuICAgICAgcHJvY2Vzc2VkLnB1c2goZWx0KVxuICAgIH1cbiAgICBpZiAoc2hvdWxkSW5jbHVkZShlbHQpKSB7XG4gICAgICBjb25zdCBuYW1lID0gZ2V0UmF3QXR0cmlidXRlKGVsdCwgJ25hbWUnKVxuICAgICAgLy8gQHRzLWlnbm9yZSB2YWx1ZSB3aWxsIGJlIHVuZGVmaW5lZCBmb3Igbm9uLWlucHV0IGVsZW1lbnRzLCB3aGljaCBpcyBmaW5lXG4gICAgICBsZXQgdmFsdWUgPSBlbHQudmFsdWVcbiAgICAgIGlmIChlbHQgaW5zdGFuY2VvZiBIVE1MU2VsZWN0RWxlbWVudCAmJiBlbHQubXVsdGlwbGUpIHtcbiAgICAgICAgdmFsdWUgPSB0b0FycmF5KGVsdC5xdWVyeVNlbGVjdG9yQWxsKCdvcHRpb246Y2hlY2tlZCcpKS5tYXAoZnVuY3Rpb24oZSkgeyByZXR1cm4gKC8qKiBAdHlwZSBIVE1MT3B0aW9uRWxlbWVudCAqLyhlKSkudmFsdWUgfSlcbiAgICAgIH1cbiAgICAgIC8vIGluY2x1ZGUgZmlsZSBpbnB1dHNcbiAgICAgIGlmIChlbHQgaW5zdGFuY2VvZiBIVE1MSW5wdXRFbGVtZW50ICYmIGVsdC5maWxlcykge1xuICAgICAgICB2YWx1ZSA9IHRvQXJyYXkoZWx0LmZpbGVzKVxuICAgICAgfVxuICAgICAgYWRkVmFsdWVUb0Zvcm1EYXRhKG5hbWUsIHZhbHVlLCBmb3JtRGF0YSlcbiAgICAgIGlmICh2YWxpZGF0ZSkge1xuICAgICAgICB2YWxpZGF0ZUVsZW1lbnQoZWx0LCBlcnJvcnMpXG4gICAgICB9XG4gICAgfVxuICAgIGlmIChlbHQgaW5zdGFuY2VvZiBIVE1MRm9ybUVsZW1lbnQpIHtcbiAgICAgIGZvckVhY2goZWx0LmVsZW1lbnRzLCBmdW5jdGlvbihpbnB1dCkge1xuICAgICAgICBpZiAocHJvY2Vzc2VkLmluZGV4T2YoaW5wdXQpID49IDApIHtcbiAgICAgICAgICAvLyBUaGUgaW5wdXQgaGFzIGFscmVhZHkgYmVlbiBwcm9jZXNzZWQgYW5kIGFkZGVkIHRvIHRoZSB2YWx1ZXMsIGJ1dCB0aGUgRm9ybURhdGEgdGhhdCB3aWxsIGJlXG4gICAgICAgICAgLy8gIGNvbnN0cnVjdGVkIHJpZ2h0IGFmdGVyIG9uIHRoZSBmb3JtLCB3aWxsIGluY2x1ZGUgaXQgb25jZSBhZ2Fpbi4gU28gcmVtb3ZlIHRoYXQgaW5wdXQncyB2YWx1ZVxuICAgICAgICAgIC8vICBub3cgdG8gYXZvaWQgZHVwbGljYXRlc1xuICAgICAgICAgIHJlbW92ZVZhbHVlRnJvbUZvcm1EYXRhKGlucHV0Lm5hbWUsIGlucHV0LnZhbHVlLCBmb3JtRGF0YSlcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICBwcm9jZXNzZWQucHVzaChpbnB1dClcbiAgICAgICAgfVxuICAgICAgICBpZiAodmFsaWRhdGUpIHtcbiAgICAgICAgICB2YWxpZGF0ZUVsZW1lbnQoaW5wdXQsIGVycm9ycylcbiAgICAgICAgfVxuICAgICAgfSlcbiAgICAgIG5ldyBGb3JtRGF0YShlbHQpLmZvckVhY2goZnVuY3Rpb24odmFsdWUsIG5hbWUpIHtcbiAgICAgICAgaWYgKHZhbHVlIGluc3RhbmNlb2YgRmlsZSAmJiB2YWx1ZS5uYW1lID09PSAnJykge1xuICAgICAgICAgIHJldHVybiAvLyBpZ25vcmUgbm8tbmFtZSBmaWxlc1xuICAgICAgICB9XG4gICAgICAgIGFkZFZhbHVlVG9Gb3JtRGF0YShuYW1lLCB2YWx1ZSwgZm9ybURhdGEpXG4gICAgICB9KVxuICAgIH1cbiAgfVxuXG4gIC8qKlxuICAgKlxuICAgKiBAcGFyYW0ge0VsZW1lbnR9IGVsdFxuICAgKiBAcGFyYW0ge0h0bXhFbGVtZW50VmFsaWRhdGlvbkVycm9yW119IGVycm9yc1xuICAgKi9cbiAgZnVuY3Rpb24gdmFsaWRhdGVFbGVtZW50KGVsdCwgZXJyb3JzKSB7XG4gICAgY29uc3QgZWxlbWVudCA9IC8qKiBAdHlwZSB7SFRNTEVsZW1lbnQgJiBFbGVtZW50SW50ZXJuYWxzfSAqLyAoZWx0KVxuICAgIGlmIChlbGVtZW50LndpbGxWYWxpZGF0ZSkge1xuICAgICAgdHJpZ2dlckV2ZW50KGVsZW1lbnQsICdodG14OnZhbGlkYXRpb246dmFsaWRhdGUnKVxuICAgICAgaWYgKCFlbGVtZW50LmNoZWNrVmFsaWRpdHkoKSkge1xuICAgICAgICBlcnJvcnMucHVzaCh7IGVsdDogZWxlbWVudCwgbWVzc2FnZTogZWxlbWVudC52YWxpZGF0aW9uTWVzc2FnZSwgdmFsaWRpdHk6IGVsZW1lbnQudmFsaWRpdHkgfSlcbiAgICAgICAgdHJpZ2dlckV2ZW50KGVsZW1lbnQsICdodG14OnZhbGlkYXRpb246ZmFpbGVkJywgeyBtZXNzYWdlOiBlbGVtZW50LnZhbGlkYXRpb25NZXNzYWdlLCB2YWxpZGl0eTogZWxlbWVudC52YWxpZGl0eSB9KVxuICAgICAgfVxuICAgIH1cbiAgfVxuXG4gIC8qKlxuICAgKiBPdmVycmlkZSB2YWx1ZXMgaW4gdGhlIG9uZSBGb3JtRGF0YSB3aXRoIHRob3NlIGZyb20gYW5vdGhlci5cbiAgICogQHBhcmFtIHtGb3JtRGF0YX0gcmVjZWl2ZXIgdGhlIGZvcm1kYXRhIHRoYXQgd2lsbCBiZSBtdXRhdGVkXG4gICAqIEBwYXJhbSB7Rm9ybURhdGF9IGRvbm9yIHRoZSBmb3JtZGF0YSB0aGF0IHdpbGwgcHJvdmlkZSB0aGUgb3ZlcnJpZGluZyB2YWx1ZXNcbiAgICogQHJldHVybnMge0Zvcm1EYXRhfSB0aGUge0BsaW5rY29kZSByZWNlaXZlcn1cbiAgICovXG4gIGZ1bmN0aW9uIG92ZXJyaWRlRm9ybURhdGEocmVjZWl2ZXIsIGRvbm9yKSB7XG4gICAgZm9yIChjb25zdCBrZXkgb2YgZG9ub3Iua2V5cygpKSB7XG4gICAgICByZWNlaXZlci5kZWxldGUoa2V5KVxuICAgIH1cbiAgICBkb25vci5mb3JFYWNoKGZ1bmN0aW9uKHZhbHVlLCBrZXkpIHtcbiAgICAgIHJlY2VpdmVyLmFwcGVuZChrZXksIHZhbHVlKVxuICAgIH0pXG4gICAgcmV0dXJuIHJlY2VpdmVyXG4gIH1cblxuICAvKipcbiAqIEBwYXJhbSB7RWxlbWVudHxIVE1MRm9ybUVsZW1lbnR9IGVsdFxuICogQHBhcmFtIHtIdHRwVmVyYn0gdmVyYlxuICogQHJldHVybnMge3tlcnJvcnM6IEh0bXhFbGVtZW50VmFsaWRhdGlvbkVycm9yW10sIGZvcm1EYXRhOiBGb3JtRGF0YSwgdmFsdWVzOiBPYmplY3R9fVxuICovXG4gIGZ1bmN0aW9uIGdldElucHV0VmFsdWVzKGVsdCwgdmVyYikge1xuICAgIC8qKiBAdHlwZSBFbGVtZW50W10gKi9cbiAgICBjb25zdCBwcm9jZXNzZWQgPSBbXVxuICAgIGNvbnN0IGZvcm1EYXRhID0gbmV3IEZvcm1EYXRhKClcbiAgICBjb25zdCBwcmlvcml0eUZvcm1EYXRhID0gbmV3IEZvcm1EYXRhKClcbiAgICAvKiogQHR5cGUgSHRteEVsZW1lbnRWYWxpZGF0aW9uRXJyb3JbXSAqL1xuICAgIGNvbnN0IGVycm9ycyA9IFtdXG4gICAgY29uc3QgaW50ZXJuYWxEYXRhID0gZ2V0SW50ZXJuYWxEYXRhKGVsdClcbiAgICBpZiAoaW50ZXJuYWxEYXRhLmxhc3RCdXR0b25DbGlja2VkICYmICFib2R5Q29udGFpbnMoaW50ZXJuYWxEYXRhLmxhc3RCdXR0b25DbGlja2VkKSkge1xuICAgICAgaW50ZXJuYWxEYXRhLmxhc3RCdXR0b25DbGlja2VkID0gbnVsbFxuICAgIH1cblxuICAgIC8vIG9ubHkgdmFsaWRhdGUgd2hlbiBmb3JtIGlzIGRpcmVjdGx5IHN1Ym1pdHRlZCBhbmQgbm92YWxpZGF0ZSBvciBmb3Jtbm92YWxpZGF0ZSBhcmUgbm90IHNldFxuICAgIC8vIG9yIGlmIHRoZSBlbGVtZW50IGhhcyBhbiBleHBsaWNpdCBoeC12YWxpZGF0ZT1cInRydWVcIiBvbiBpdFxuICAgIGxldCB2YWxpZGF0ZSA9IChlbHQgaW5zdGFuY2VvZiBIVE1MRm9ybUVsZW1lbnQgJiYgZWx0Lm5vVmFsaWRhdGUgIT09IHRydWUpIHx8IGdldEF0dHJpYnV0ZVZhbHVlKGVsdCwgJ2h4LXZhbGlkYXRlJykgPT09ICd0cnVlJ1xuICAgIGlmIChpbnRlcm5hbERhdGEubGFzdEJ1dHRvbkNsaWNrZWQpIHtcbiAgICAgIHZhbGlkYXRlID0gdmFsaWRhdGUgJiYgaW50ZXJuYWxEYXRhLmxhc3RCdXR0b25DbGlja2VkLmZvcm1Ob1ZhbGlkYXRlICE9PSB0cnVlXG4gICAgfVxuXG4gICAgLy8gZm9yIGEgbm9uLUdFVCBpbmNsdWRlIHRoZSBjbG9zZXN0IGZvcm1cbiAgICBpZiAodmVyYiAhPT0gJ2dldCcpIHtcbiAgICAgIHByb2Nlc3NJbnB1dFZhbHVlKHByb2Nlc3NlZCwgcHJpb3JpdHlGb3JtRGF0YSwgZXJyb3JzLCBjbG9zZXN0KGVsdCwgJ2Zvcm0nKSwgdmFsaWRhdGUpXG4gICAgfVxuXG4gICAgLy8gaW5jbHVkZSB0aGUgZWxlbWVudCBpdHNlbGZcbiAgICBwcm9jZXNzSW5wdXRWYWx1ZShwcm9jZXNzZWQsIGZvcm1EYXRhLCBlcnJvcnMsIGVsdCwgdmFsaWRhdGUpXG5cbiAgICAvLyBpZiBhIGJ1dHRvbiBvciBzdWJtaXQgd2FzIGNsaWNrZWQgbGFzdCwgaW5jbHVkZSBpdHMgdmFsdWVcbiAgICBpZiAoaW50ZXJuYWxEYXRhLmxhc3RCdXR0b25DbGlja2VkIHx8IGVsdC50YWdOYW1lID09PSAnQlVUVE9OJyB8fFxuICAgIChlbHQudGFnTmFtZSA9PT0gJ0lOUFVUJyAmJiBnZXRSYXdBdHRyaWJ1dGUoZWx0LCAndHlwZScpID09PSAnc3VibWl0JykpIHtcbiAgICAgIGNvbnN0IGJ1dHRvbiA9IGludGVybmFsRGF0YS5sYXN0QnV0dG9uQ2xpY2tlZCB8fCAoLyoqIEB0eXBlIEhUTUxJbnB1dEVsZW1lbnR8SFRNTEJ1dHRvbkVsZW1lbnQgKi8oZWx0KSlcbiAgICAgIGNvbnN0IG5hbWUgPSBnZXRSYXdBdHRyaWJ1dGUoYnV0dG9uLCAnbmFtZScpXG4gICAgICBhZGRWYWx1ZVRvRm9ybURhdGEobmFtZSwgYnV0dG9uLnZhbHVlLCBwcmlvcml0eUZvcm1EYXRhKVxuICAgIH1cblxuICAgIC8vIGluY2x1ZGUgYW55IGV4cGxpY2l0IGluY2x1ZGVzXG4gICAgY29uc3QgaW5jbHVkZXMgPSBmaW5kQXR0cmlidXRlVGFyZ2V0cyhlbHQsICdoeC1pbmNsdWRlJylcbiAgICBmb3JFYWNoKGluY2x1ZGVzLCBmdW5jdGlvbihub2RlKSB7XG4gICAgICBwcm9jZXNzSW5wdXRWYWx1ZShwcm9jZXNzZWQsIGZvcm1EYXRhLCBlcnJvcnMsIGFzRWxlbWVudChub2RlKSwgdmFsaWRhdGUpXG4gICAgICAvLyBpZiBhIG5vbi1mb3JtIGlzIGluY2x1ZGVkLCBpbmNsdWRlIGFueSBpbnB1dCB2YWx1ZXMgd2l0aGluIGl0XG4gICAgICBpZiAoIW1hdGNoZXMobm9kZSwgJ2Zvcm0nKSkge1xuICAgICAgICBmb3JFYWNoKGFzUGFyZW50Tm9kZShub2RlKS5xdWVyeVNlbGVjdG9yQWxsKElOUFVUX1NFTEVDVE9SKSwgZnVuY3Rpb24oZGVzY2VuZGFudCkge1xuICAgICAgICAgIHByb2Nlc3NJbnB1dFZhbHVlKHByb2Nlc3NlZCwgZm9ybURhdGEsIGVycm9ycywgZGVzY2VuZGFudCwgdmFsaWRhdGUpXG4gICAgICAgIH0pXG4gICAgICB9XG4gICAgfSlcblxuICAgIC8vIHZhbHVlcyBmcm9tIGEgPGZvcm0+IHRha2UgcHJlY2VkZW5jZSwgb3ZlcnJpZGluZyB0aGUgcmVndWxhciB2YWx1ZXNcbiAgICBvdmVycmlkZUZvcm1EYXRhKGZvcm1EYXRhLCBwcmlvcml0eUZvcm1EYXRhKVxuXG4gICAgcmV0dXJuIHsgZXJyb3JzLCBmb3JtRGF0YSwgdmFsdWVzOiBmb3JtRGF0YVByb3h5KGZvcm1EYXRhKSB9XG4gIH1cblxuICAvKipcbiAgICogQHBhcmFtIHtzdHJpbmd9IHJldHVyblN0clxuICAgKiBAcGFyYW0ge3N0cmluZ30gbmFtZVxuICAgKiBAcGFyYW0ge2FueX0gcmVhbFZhbHVlXG4gICAqIEByZXR1cm5zIHtzdHJpbmd9XG4gICAqL1xuICBmdW5jdGlvbiBhcHBlbmRQYXJhbShyZXR1cm5TdHIsIG5hbWUsIHJlYWxWYWx1ZSkge1xuICAgIGlmIChyZXR1cm5TdHIgIT09ICcnKSB7XG4gICAgICByZXR1cm5TdHIgKz0gJyYnXG4gICAgfVxuICAgIGlmIChTdHJpbmcocmVhbFZhbHVlKSA9PT0gJ1tvYmplY3QgT2JqZWN0XScpIHtcbiAgICAgIHJlYWxWYWx1ZSA9IEpTT04uc3RyaW5naWZ5KHJlYWxWYWx1ZSlcbiAgICB9XG4gICAgY29uc3QgcyA9IGVuY29kZVVSSUNvbXBvbmVudChyZWFsVmFsdWUpXG4gICAgcmV0dXJuU3RyICs9IGVuY29kZVVSSUNvbXBvbmVudChuYW1lKSArICc9JyArIHNcbiAgICByZXR1cm4gcmV0dXJuU3RyXG4gIH1cblxuICAvKipcbiAgICogQHBhcmFtIHtGb3JtRGF0YXxPYmplY3R9IHZhbHVlc1xuICAgKiBAcmV0dXJucyBzdHJpbmdcbiAgICovXG4gIGZ1bmN0aW9uIHVybEVuY29kZSh2YWx1ZXMpIHtcbiAgICB2YWx1ZXMgPSBmb3JtRGF0YUZyb21PYmplY3QodmFsdWVzKVxuICAgIGxldCByZXR1cm5TdHIgPSAnJ1xuICAgIHZhbHVlcy5mb3JFYWNoKGZ1bmN0aW9uKHZhbHVlLCBrZXkpIHtcbiAgICAgIHJldHVyblN0ciA9IGFwcGVuZFBhcmFtKHJldHVyblN0ciwga2V5LCB2YWx1ZSlcbiAgICB9KVxuICAgIHJldHVybiByZXR1cm5TdHJcbiAgfVxuXG4gIC8vPSA9PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09XG4gIC8vIEFqYXhcbiAgLy89ID09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT1cblxuICAvKipcbiAqIEBwYXJhbSB7RWxlbWVudH0gZWx0XG4gKiBAcGFyYW0ge0VsZW1lbnR9IHRhcmdldFxuICogQHBhcmFtIHtzdHJpbmd9IHByb21wdFxuICogQHJldHVybnMge0h0bXhIZWFkZXJTcGVjaWZpY2F0aW9ufVxuICovXG4gIGZ1bmN0aW9uIGdldEhlYWRlcnMoZWx0LCB0YXJnZXQsIHByb21wdCkge1xuICAgIC8qKiBAdHlwZSBIdG14SGVhZGVyU3BlY2lmaWNhdGlvbiAqL1xuICAgIGNvbnN0IGhlYWRlcnMgPSB7XG4gICAgICAnSFgtUmVxdWVzdCc6ICd0cnVlJyxcbiAgICAgICdIWC1UcmlnZ2VyJzogZ2V0UmF3QXR0cmlidXRlKGVsdCwgJ2lkJyksXG4gICAgICAnSFgtVHJpZ2dlci1OYW1lJzogZ2V0UmF3QXR0cmlidXRlKGVsdCwgJ25hbWUnKSxcbiAgICAgICdIWC1UYXJnZXQnOiBnZXRBdHRyaWJ1dGVWYWx1ZSh0YXJnZXQsICdpZCcpLFxuICAgICAgJ0hYLUN1cnJlbnQtVVJMJzogZ2V0RG9jdW1lbnQoKS5sb2NhdGlvbi5ocmVmXG4gICAgfVxuICAgIGdldFZhbHVlc0ZvckVsZW1lbnQoZWx0LCAnaHgtaGVhZGVycycsIGZhbHNlLCBoZWFkZXJzKVxuICAgIGlmIChwcm9tcHQgIT09IHVuZGVmaW5lZCkge1xuICAgICAgaGVhZGVyc1snSFgtUHJvbXB0J10gPSBwcm9tcHRcbiAgICB9XG4gICAgaWYgKGdldEludGVybmFsRGF0YShlbHQpLmJvb3N0ZWQpIHtcbiAgICAgIGhlYWRlcnNbJ0hYLUJvb3N0ZWQnXSA9ICd0cnVlJ1xuICAgIH1cbiAgICByZXR1cm4gaGVhZGVyc1xuICB9XG5cbiAgLyoqXG4gKiBmaWx0ZXJWYWx1ZXMgdGFrZXMgYW4gb2JqZWN0IGNvbnRhaW5pbmcgZm9ybSBpbnB1dCB2YWx1ZXNcbiAqIGFuZCByZXR1cm5zIGEgbmV3IG9iamVjdCB0aGF0IG9ubHkgY29udGFpbnMga2V5cyB0aGF0IGFyZVxuICogc3BlY2lmaWVkIGJ5IHRoZSBjbG9zZXN0IFwiaHgtcGFyYW1zXCIgYXR0cmlidXRlXG4gKiBAcGFyYW0ge0Zvcm1EYXRhfSBpbnB1dFZhbHVlc1xuICogQHBhcmFtIHtFbGVtZW50fSBlbHRcbiAqIEByZXR1cm5zIHtGb3JtRGF0YX1cbiAqL1xuICBmdW5jdGlvbiBmaWx0ZXJWYWx1ZXMoaW5wdXRWYWx1ZXMsIGVsdCkge1xuICAgIGNvbnN0IHBhcmFtc1ZhbHVlID0gZ2V0Q2xvc2VzdEF0dHJpYnV0ZVZhbHVlKGVsdCwgJ2h4LXBhcmFtcycpXG4gICAgaWYgKHBhcmFtc1ZhbHVlKSB7XG4gICAgICBpZiAocGFyYW1zVmFsdWUgPT09ICdub25lJykge1xuICAgICAgICByZXR1cm4gbmV3IEZvcm1EYXRhKClcbiAgICAgIH0gZWxzZSBpZiAocGFyYW1zVmFsdWUgPT09ICcqJykge1xuICAgICAgICByZXR1cm4gaW5wdXRWYWx1ZXNcbiAgICAgIH0gZWxzZSBpZiAocGFyYW1zVmFsdWUuaW5kZXhPZignbm90ICcpID09PSAwKSB7XG4gICAgICAgIGZvckVhY2gocGFyYW1zVmFsdWUuc2xpY2UoNCkuc3BsaXQoJywnKSwgZnVuY3Rpb24obmFtZSkge1xuICAgICAgICAgIG5hbWUgPSBuYW1lLnRyaW0oKVxuICAgICAgICAgIGlucHV0VmFsdWVzLmRlbGV0ZShuYW1lKVxuICAgICAgICB9KVxuICAgICAgICByZXR1cm4gaW5wdXRWYWx1ZXNcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIGNvbnN0IG5ld1ZhbHVlcyA9IG5ldyBGb3JtRGF0YSgpXG4gICAgICAgIGZvckVhY2gocGFyYW1zVmFsdWUuc3BsaXQoJywnKSwgZnVuY3Rpb24obmFtZSkge1xuICAgICAgICAgIG5hbWUgPSBuYW1lLnRyaW0oKVxuICAgICAgICAgIGlmIChpbnB1dFZhbHVlcy5oYXMobmFtZSkpIHtcbiAgICAgICAgICAgIGlucHV0VmFsdWVzLmdldEFsbChuYW1lKS5mb3JFYWNoKGZ1bmN0aW9uKHZhbHVlKSB7IG5ld1ZhbHVlcy5hcHBlbmQobmFtZSwgdmFsdWUpIH0pXG4gICAgICAgICAgfVxuICAgICAgICB9KVxuICAgICAgICByZXR1cm4gbmV3VmFsdWVzXG4gICAgICB9XG4gICAgfSBlbHNlIHtcbiAgICAgIHJldHVybiBpbnB1dFZhbHVlc1xuICAgIH1cbiAgfVxuXG4gIC8qKlxuICAgKiBAcGFyYW0ge0VsZW1lbnR9IGVsdFxuICAgKiBAcmV0dXJuIHtib29sZWFufVxuICAgKi9cbiAgZnVuY3Rpb24gaXNBbmNob3JMaW5rKGVsdCkge1xuICAgIHJldHVybiAhIWdldFJhd0F0dHJpYnV0ZShlbHQsICdocmVmJykgJiYgZ2V0UmF3QXR0cmlidXRlKGVsdCwgJ2hyZWYnKS5pbmRleE9mKCcjJykgPj0gMFxuICB9XG5cbiAgLyoqXG4gKiBAcGFyYW0ge0VsZW1lbnR9IGVsdFxuICogQHBhcmFtIHtIdG14U3dhcFN0eWxlfSBbc3dhcEluZm9PdmVycmlkZV1cbiAqIEByZXR1cm5zIHtIdG14U3dhcFNwZWNpZmljYXRpb259XG4gKi9cbiAgZnVuY3Rpb24gZ2V0U3dhcFNwZWNpZmljYXRpb24oZWx0LCBzd2FwSW5mb092ZXJyaWRlKSB7XG4gICAgY29uc3Qgc3dhcEluZm8gPSBzd2FwSW5mb092ZXJyaWRlIHx8IGdldENsb3Nlc3RBdHRyaWJ1dGVWYWx1ZShlbHQsICdoeC1zd2FwJylcbiAgICAvKiogQHR5cGUgSHRteFN3YXBTcGVjaWZpY2F0aW9uICovXG4gICAgY29uc3Qgc3dhcFNwZWMgPSB7XG4gICAgICBzd2FwU3R5bGU6IGdldEludGVybmFsRGF0YShlbHQpLmJvb3N0ZWQgPyAnaW5uZXJIVE1MJyA6IGh0bXguY29uZmlnLmRlZmF1bHRTd2FwU3R5bGUsXG4gICAgICBzd2FwRGVsYXk6IGh0bXguY29uZmlnLmRlZmF1bHRTd2FwRGVsYXksXG4gICAgICBzZXR0bGVEZWxheTogaHRteC5jb25maWcuZGVmYXVsdFNldHRsZURlbGF5XG4gICAgfVxuICAgIGlmIChodG14LmNvbmZpZy5zY3JvbGxJbnRvVmlld09uQm9vc3QgJiYgZ2V0SW50ZXJuYWxEYXRhKGVsdCkuYm9vc3RlZCAmJiAhaXNBbmNob3JMaW5rKGVsdCkpIHtcbiAgICAgIHN3YXBTcGVjLnNob3cgPSAndG9wJ1xuICAgIH1cbiAgICBpZiAoc3dhcEluZm8pIHtcbiAgICAgIGNvbnN0IHNwbGl0ID0gc3BsaXRPbldoaXRlc3BhY2Uoc3dhcEluZm8pXG4gICAgICBpZiAoc3BsaXQubGVuZ3RoID4gMCkge1xuICAgICAgICBmb3IgKGxldCBpID0gMDsgaSA8IHNwbGl0Lmxlbmd0aDsgaSsrKSB7XG4gICAgICAgICAgY29uc3QgdmFsdWUgPSBzcGxpdFtpXVxuICAgICAgICAgIGlmICh2YWx1ZS5pbmRleE9mKCdzd2FwOicpID09PSAwKSB7XG4gICAgICAgICAgICBzd2FwU3BlYy5zd2FwRGVsYXkgPSBwYXJzZUludGVydmFsKHZhbHVlLnNsaWNlKDUpKVxuICAgICAgICAgIH0gZWxzZSBpZiAodmFsdWUuaW5kZXhPZignc2V0dGxlOicpID09PSAwKSB7XG4gICAgICAgICAgICBzd2FwU3BlYy5zZXR0bGVEZWxheSA9IHBhcnNlSW50ZXJ2YWwodmFsdWUuc2xpY2UoNykpXG4gICAgICAgICAgfSBlbHNlIGlmICh2YWx1ZS5pbmRleE9mKCd0cmFuc2l0aW9uOicpID09PSAwKSB7XG4gICAgICAgICAgICBzd2FwU3BlYy50cmFuc2l0aW9uID0gdmFsdWUuc2xpY2UoMTEpID09PSAndHJ1ZSdcbiAgICAgICAgICB9IGVsc2UgaWYgKHZhbHVlLmluZGV4T2YoJ2lnbm9yZVRpdGxlOicpID09PSAwKSB7XG4gICAgICAgICAgICBzd2FwU3BlYy5pZ25vcmVUaXRsZSA9IHZhbHVlLnNsaWNlKDEyKSA9PT0gJ3RydWUnXG4gICAgICAgICAgfSBlbHNlIGlmICh2YWx1ZS5pbmRleE9mKCdzY3JvbGw6JykgPT09IDApIHtcbiAgICAgICAgICAgIGNvbnN0IHNjcm9sbFNwZWMgPSB2YWx1ZS5zbGljZSg3KVxuICAgICAgICAgICAgdmFyIHNwbGl0U3BlYyA9IHNjcm9sbFNwZWMuc3BsaXQoJzonKVxuICAgICAgICAgICAgY29uc3Qgc2Nyb2xsVmFsID0gc3BsaXRTcGVjLnBvcCgpXG4gICAgICAgICAgICB2YXIgc2VsZWN0b3JWYWwgPSBzcGxpdFNwZWMubGVuZ3RoID4gMCA/IHNwbGl0U3BlYy5qb2luKCc6JykgOiBudWxsXG4gICAgICAgICAgICAvLyBAdHMtaWdub3JlXG4gICAgICAgICAgICBzd2FwU3BlYy5zY3JvbGwgPSBzY3JvbGxWYWxcbiAgICAgICAgICAgIHN3YXBTcGVjLnNjcm9sbFRhcmdldCA9IHNlbGVjdG9yVmFsXG4gICAgICAgICAgfSBlbHNlIGlmICh2YWx1ZS5pbmRleE9mKCdzaG93OicpID09PSAwKSB7XG4gICAgICAgICAgICBjb25zdCBzaG93U3BlYyA9IHZhbHVlLnNsaWNlKDUpXG4gICAgICAgICAgICB2YXIgc3BsaXRTcGVjID0gc2hvd1NwZWMuc3BsaXQoJzonKVxuICAgICAgICAgICAgY29uc3Qgc2hvd1ZhbCA9IHNwbGl0U3BlYy5wb3AoKVxuICAgICAgICAgICAgdmFyIHNlbGVjdG9yVmFsID0gc3BsaXRTcGVjLmxlbmd0aCA+IDAgPyBzcGxpdFNwZWMuam9pbignOicpIDogbnVsbFxuICAgICAgICAgICAgc3dhcFNwZWMuc2hvdyA9IHNob3dWYWxcbiAgICAgICAgICAgIHN3YXBTcGVjLnNob3dUYXJnZXQgPSBzZWxlY3RvclZhbFxuICAgICAgICAgIH0gZWxzZSBpZiAodmFsdWUuaW5kZXhPZignZm9jdXMtc2Nyb2xsOicpID09PSAwKSB7XG4gICAgICAgICAgICBjb25zdCBmb2N1c1Njcm9sbFZhbCA9IHZhbHVlLnNsaWNlKCdmb2N1cy1zY3JvbGw6Jy5sZW5ndGgpXG4gICAgICAgICAgICBzd2FwU3BlYy5mb2N1c1Njcm9sbCA9IGZvY3VzU2Nyb2xsVmFsID09ICd0cnVlJ1xuICAgICAgICAgIH0gZWxzZSBpZiAoaSA9PSAwKSB7XG4gICAgICAgICAgICBzd2FwU3BlYy5zd2FwU3R5bGUgPSB2YWx1ZVxuICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICBsb2dFcnJvcignVW5rbm93biBtb2RpZmllciBpbiBoeC1zd2FwOiAnICsgdmFsdWUpXG4gICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICB9XG4gICAgfVxuICAgIHJldHVybiBzd2FwU3BlY1xuICB9XG5cbiAgLyoqXG4gICAqIEBwYXJhbSB7RWxlbWVudH0gZWx0XG4gICAqIEByZXR1cm4ge2Jvb2xlYW59XG4gICAqL1xuICBmdW5jdGlvbiB1c2VzRm9ybURhdGEoZWx0KSB7XG4gICAgcmV0dXJuIGdldENsb3Nlc3RBdHRyaWJ1dGVWYWx1ZShlbHQsICdoeC1lbmNvZGluZycpID09PSAnbXVsdGlwYXJ0L2Zvcm0tZGF0YScgfHxcbiAgICAobWF0Y2hlcyhlbHQsICdmb3JtJykgJiYgZ2V0UmF3QXR0cmlidXRlKGVsdCwgJ2VuY3R5cGUnKSA9PT0gJ211bHRpcGFydC9mb3JtLWRhdGEnKVxuICB9XG5cbiAgLyoqXG4gICAqIEBwYXJhbSB7WE1MSHR0cFJlcXVlc3R9IHhoclxuICAgKiBAcGFyYW0ge0VsZW1lbnR9IGVsdFxuICAgKiBAcGFyYW0ge0Zvcm1EYXRhfSBmaWx0ZXJlZFBhcmFtZXRlcnNcbiAgICogQHJldHVybnMgeyp8c3RyaW5nfG51bGx9XG4gICAqL1xuICBmdW5jdGlvbiBlbmNvZGVQYXJhbXNGb3JCb2R5KHhociwgZWx0LCBmaWx0ZXJlZFBhcmFtZXRlcnMpIHtcbiAgICBsZXQgZW5jb2RlZFBhcmFtZXRlcnMgPSBudWxsXG4gICAgd2l0aEV4dGVuc2lvbnMoZWx0LCBmdW5jdGlvbihleHRlbnNpb24pIHtcbiAgICAgIGlmIChlbmNvZGVkUGFyYW1ldGVycyA9PSBudWxsKSB7XG4gICAgICAgIGVuY29kZWRQYXJhbWV0ZXJzID0gZXh0ZW5zaW9uLmVuY29kZVBhcmFtZXRlcnMoeGhyLCBmaWx0ZXJlZFBhcmFtZXRlcnMsIGVsdClcbiAgICAgIH1cbiAgICB9KVxuICAgIGlmIChlbmNvZGVkUGFyYW1ldGVycyAhPSBudWxsKSB7XG4gICAgICByZXR1cm4gZW5jb2RlZFBhcmFtZXRlcnNcbiAgICB9IGVsc2Uge1xuICAgICAgaWYgKHVzZXNGb3JtRGF0YShlbHQpKSB7XG4gICAgICAgIC8vIEZvcmNlIGNvbnZlcnNpb24gdG8gYW4gYWN0dWFsIEZvcm1EYXRhIG9iamVjdCBpbiBjYXNlIGZpbHRlcmVkUGFyYW1ldGVycyBpcyBhIGZvcm1EYXRhUHJveHlcbiAgICAgICAgLy8gU2VlIGh0dHBzOi8vZ2l0aHViLmNvbS9iaWdza3lzb2Z0d2FyZS9odG14L2lzc3Vlcy8yMzE3XG4gICAgICAgIHJldHVybiBvdmVycmlkZUZvcm1EYXRhKG5ldyBGb3JtRGF0YSgpLCBmb3JtRGF0YUZyb21PYmplY3QoZmlsdGVyZWRQYXJhbWV0ZXJzKSlcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIHJldHVybiB1cmxFbmNvZGUoZmlsdGVyZWRQYXJhbWV0ZXJzKVxuICAgICAgfVxuICAgIH1cbiAgfVxuXG4gIC8qKlxuICpcbiAqIEBwYXJhbSB7RWxlbWVudH0gdGFyZ2V0XG4gKiBAcmV0dXJucyB7SHRteFNldHRsZUluZm99XG4gKi9cbiAgZnVuY3Rpb24gbWFrZVNldHRsZUluZm8odGFyZ2V0KSB7XG4gICAgcmV0dXJuIHsgdGFza3M6IFtdLCBlbHRzOiBbdGFyZ2V0XSB9XG4gIH1cblxuICAvKipcbiAgICogQHBhcmFtIHtFbGVtZW50W119IGNvbnRlbnRcbiAgICogQHBhcmFtIHtIdG14U3dhcFNwZWNpZmljYXRpb259IHN3YXBTcGVjXG4gICAqL1xuICBmdW5jdGlvbiB1cGRhdGVTY3JvbGxTdGF0ZShjb250ZW50LCBzd2FwU3BlYykge1xuICAgIGNvbnN0IGZpcnN0ID0gY29udGVudFswXVxuICAgIGNvbnN0IGxhc3QgPSBjb250ZW50W2NvbnRlbnQubGVuZ3RoIC0gMV1cbiAgICBpZiAoc3dhcFNwZWMuc2Nyb2xsKSB7XG4gICAgICB2YXIgdGFyZ2V0ID0gbnVsbFxuICAgICAgaWYgKHN3YXBTcGVjLnNjcm9sbFRhcmdldCkge1xuICAgICAgICB0YXJnZXQgPSBhc0VsZW1lbnQocXVlcnlTZWxlY3RvckV4dChmaXJzdCwgc3dhcFNwZWMuc2Nyb2xsVGFyZ2V0KSlcbiAgICAgIH1cbiAgICAgIGlmIChzd2FwU3BlYy5zY3JvbGwgPT09ICd0b3AnICYmIChmaXJzdCB8fCB0YXJnZXQpKSB7XG4gICAgICAgIHRhcmdldCA9IHRhcmdldCB8fCBmaXJzdFxuICAgICAgICB0YXJnZXQuc2Nyb2xsVG9wID0gMFxuICAgICAgfVxuICAgICAgaWYgKHN3YXBTcGVjLnNjcm9sbCA9PT0gJ2JvdHRvbScgJiYgKGxhc3QgfHwgdGFyZ2V0KSkge1xuICAgICAgICB0YXJnZXQgPSB0YXJnZXQgfHwgbGFzdFxuICAgICAgICB0YXJnZXQuc2Nyb2xsVG9wID0gdGFyZ2V0LnNjcm9sbEhlaWdodFxuICAgICAgfVxuICAgIH1cbiAgICBpZiAoc3dhcFNwZWMuc2hvdykge1xuICAgICAgdmFyIHRhcmdldCA9IG51bGxcbiAgICAgIGlmIChzd2FwU3BlYy5zaG93VGFyZ2V0KSB7XG4gICAgICAgIGxldCB0YXJnZXRTdHIgPSBzd2FwU3BlYy5zaG93VGFyZ2V0XG4gICAgICAgIGlmIChzd2FwU3BlYy5zaG93VGFyZ2V0ID09PSAnd2luZG93Jykge1xuICAgICAgICAgIHRhcmdldFN0ciA9ICdib2R5J1xuICAgICAgICB9XG4gICAgICAgIHRhcmdldCA9IGFzRWxlbWVudChxdWVyeVNlbGVjdG9yRXh0KGZpcnN0LCB0YXJnZXRTdHIpKVxuICAgICAgfVxuICAgICAgaWYgKHN3YXBTcGVjLnNob3cgPT09ICd0b3AnICYmIChmaXJzdCB8fCB0YXJnZXQpKSB7XG4gICAgICAgIHRhcmdldCA9IHRhcmdldCB8fCBmaXJzdFxuICAgICAgICAvLyBAdHMtaWdub3JlIEZvciBzb21lIHJlYXNvbiB0c2MgZG9lc24ndCByZWNvZ25pemUgXCJpbnN0YW50XCIgYXMgYSB2YWxpZCBvcHRpb24gZm9yIG5vd1xuICAgICAgICB0YXJnZXQuc2Nyb2xsSW50b1ZpZXcoeyBibG9jazogJ3N0YXJ0JywgYmVoYXZpb3I6IGh0bXguY29uZmlnLnNjcm9sbEJlaGF2aW9yIH0pXG4gICAgICB9XG4gICAgICBpZiAoc3dhcFNwZWMuc2hvdyA9PT0gJ2JvdHRvbScgJiYgKGxhc3QgfHwgdGFyZ2V0KSkge1xuICAgICAgICB0YXJnZXQgPSB0YXJnZXQgfHwgbGFzdFxuICAgICAgICAvLyBAdHMtaWdub3JlIEZvciBzb21lIHJlYXNvbiB0c2MgZG9lc24ndCByZWNvZ25pemUgXCJpbnN0YW50XCIgYXMgYSB2YWxpZCBvcHRpb24gZm9yIG5vd1xuICAgICAgICB0YXJnZXQuc2Nyb2xsSW50b1ZpZXcoeyBibG9jazogJ2VuZCcsIGJlaGF2aW9yOiBodG14LmNvbmZpZy5zY3JvbGxCZWhhdmlvciB9KVxuICAgICAgfVxuICAgIH1cbiAgfVxuXG4gIC8qKlxuICogQHBhcmFtIHtFbGVtZW50fSBlbHRcbiAqIEBwYXJhbSB7c3RyaW5nfSBhdHRyXG4gKiBAcGFyYW0ge2Jvb2xlYW49fSBldmFsQXNEZWZhdWx0XG4gKiBAcGFyYW0ge09iamVjdD19IHZhbHVlc1xuICogQHJldHVybnMge09iamVjdH1cbiAqL1xuICBmdW5jdGlvbiBnZXRWYWx1ZXNGb3JFbGVtZW50KGVsdCwgYXR0ciwgZXZhbEFzRGVmYXVsdCwgdmFsdWVzKSB7XG4gICAgaWYgKHZhbHVlcyA9PSBudWxsKSB7XG4gICAgICB2YWx1ZXMgPSB7fVxuICAgIH1cbiAgICBpZiAoZWx0ID09IG51bGwpIHtcbiAgICAgIHJldHVybiB2YWx1ZXNcbiAgICB9XG4gICAgY29uc3QgYXR0cmlidXRlVmFsdWUgPSBnZXRBdHRyaWJ1dGVWYWx1ZShlbHQsIGF0dHIpXG4gICAgaWYgKGF0dHJpYnV0ZVZhbHVlKSB7XG4gICAgICBsZXQgc3RyID0gYXR0cmlidXRlVmFsdWUudHJpbSgpXG4gICAgICBsZXQgZXZhbHVhdGVWYWx1ZSA9IGV2YWxBc0RlZmF1bHRcbiAgICAgIGlmIChzdHIgPT09ICd1bnNldCcpIHtcbiAgICAgICAgcmV0dXJuIG51bGxcbiAgICAgIH1cbiAgICAgIGlmIChzdHIuaW5kZXhPZignamF2YXNjcmlwdDonKSA9PT0gMCkge1xuICAgICAgICBzdHIgPSBzdHIuc2xpY2UoMTEpXG4gICAgICAgIGV2YWx1YXRlVmFsdWUgPSB0cnVlXG4gICAgICB9IGVsc2UgaWYgKHN0ci5pbmRleE9mKCdqczonKSA9PT0gMCkge1xuICAgICAgICBzdHIgPSBzdHIuc2xpY2UoMylcbiAgICAgICAgZXZhbHVhdGVWYWx1ZSA9IHRydWVcbiAgICAgIH1cbiAgICAgIGlmIChzdHIuaW5kZXhPZigneycpICE9PSAwKSB7XG4gICAgICAgIHN0ciA9ICd7JyArIHN0ciArICd9J1xuICAgICAgfVxuICAgICAgbGV0IHZhcnNWYWx1ZXNcbiAgICAgIGlmIChldmFsdWF0ZVZhbHVlKSB7XG4gICAgICAgIHZhcnNWYWx1ZXMgPSBtYXliZUV2YWwoZWx0LCBmdW5jdGlvbigpIHsgcmV0dXJuIEZ1bmN0aW9uKCdyZXR1cm4gKCcgKyBzdHIgKyAnKScpKCkgfSwge30pXG4gICAgICB9IGVsc2Uge1xuICAgICAgICB2YXJzVmFsdWVzID0gcGFyc2VKU09OKHN0cilcbiAgICAgIH1cbiAgICAgIGZvciAoY29uc3Qga2V5IGluIHZhcnNWYWx1ZXMpIHtcbiAgICAgICAgaWYgKHZhcnNWYWx1ZXMuaGFzT3duUHJvcGVydHkoa2V5KSkge1xuICAgICAgICAgIGlmICh2YWx1ZXNba2V5XSA9PSBudWxsKSB7XG4gICAgICAgICAgICB2YWx1ZXNba2V5XSA9IHZhcnNWYWx1ZXNba2V5XVxuICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgfVxuICAgIH1cbiAgICByZXR1cm4gZ2V0VmFsdWVzRm9yRWxlbWVudChhc0VsZW1lbnQocGFyZW50RWx0KGVsdCkpLCBhdHRyLCBldmFsQXNEZWZhdWx0LCB2YWx1ZXMpXG4gIH1cblxuICAvKipcbiAgICogQHBhcmFtIHtFdmVudFRhcmdldHxzdHJpbmd9IGVsdFxuICAgKiBAcGFyYW0geygpID0+IGFueX0gdG9FdmFsXG4gICAqIEBwYXJhbSB7YW55PX0gZGVmYXVsdFZhbFxuICAgKiBAcmV0dXJucyB7YW55fVxuICAgKi9cbiAgZnVuY3Rpb24gbWF5YmVFdmFsKGVsdCwgdG9FdmFsLCBkZWZhdWx0VmFsKSB7XG4gICAgaWYgKGh0bXguY29uZmlnLmFsbG93RXZhbCkge1xuICAgICAgcmV0dXJuIHRvRXZhbCgpXG4gICAgfSBlbHNlIHtcbiAgICAgIHRyaWdnZXJFcnJvckV2ZW50KGVsdCwgJ2h0bXg6ZXZhbERpc2FsbG93ZWRFcnJvcicpXG4gICAgICByZXR1cm4gZGVmYXVsdFZhbFxuICAgIH1cbiAgfVxuXG4gIC8qKlxuICogQHBhcmFtIHtFbGVtZW50fSBlbHRcbiAqIEBwYXJhbSB7Kj99IGV4cHJlc3Npb25WYXJzXG4gKiBAcmV0dXJuc1xuICovXG4gIGZ1bmN0aW9uIGdldEhYVmFyc0ZvckVsZW1lbnQoZWx0LCBleHByZXNzaW9uVmFycykge1xuICAgIHJldHVybiBnZXRWYWx1ZXNGb3JFbGVtZW50KGVsdCwgJ2h4LXZhcnMnLCB0cnVlLCBleHByZXNzaW9uVmFycylcbiAgfVxuXG4gIC8qKlxuICogQHBhcmFtIHtFbGVtZW50fSBlbHRcbiAqIEBwYXJhbSB7Kj99IGV4cHJlc3Npb25WYXJzXG4gKiBAcmV0dXJuc1xuICovXG4gIGZ1bmN0aW9uIGdldEhYVmFsc0ZvckVsZW1lbnQoZWx0LCBleHByZXNzaW9uVmFycykge1xuICAgIHJldHVybiBnZXRWYWx1ZXNGb3JFbGVtZW50KGVsdCwgJ2h4LXZhbHMnLCBmYWxzZSwgZXhwcmVzc2lvblZhcnMpXG4gIH1cblxuICAvKipcbiAqIEBwYXJhbSB7RWxlbWVudH0gZWx0XG4gKiBAcmV0dXJucyB7Rm9ybURhdGF9XG4gKi9cbiAgZnVuY3Rpb24gZ2V0RXhwcmVzc2lvblZhcnMoZWx0KSB7XG4gICAgcmV0dXJuIG1lcmdlT2JqZWN0cyhnZXRIWFZhcnNGb3JFbGVtZW50KGVsdCksIGdldEhYVmFsc0ZvckVsZW1lbnQoZWx0KSlcbiAgfVxuXG4gIC8qKlxuICAgKiBAcGFyYW0ge1hNTEh0dHBSZXF1ZXN0fSB4aHJcbiAgICogQHBhcmFtIHtzdHJpbmd9IGhlYWRlclxuICAgKiBAcGFyYW0ge3N0cmluZ3xudWxsfSBoZWFkZXJWYWx1ZVxuICAgKi9cbiAgZnVuY3Rpb24gc2FmZWx5U2V0SGVhZGVyVmFsdWUoeGhyLCBoZWFkZXIsIGhlYWRlclZhbHVlKSB7XG4gICAgaWYgKGhlYWRlclZhbHVlICE9PSBudWxsKSB7XG4gICAgICB0cnkge1xuICAgICAgICB4aHIuc2V0UmVxdWVzdEhlYWRlcihoZWFkZXIsIGhlYWRlclZhbHVlKVxuICAgICAgfSBjYXRjaCAoZSkge1xuICAgICAgLy8gT24gYW4gZXhjZXB0aW9uLCB0cnkgdG8gc2V0IHRoZSBoZWFkZXIgVVJJIGVuY29kZWQgaW5zdGVhZFxuICAgICAgICB4aHIuc2V0UmVxdWVzdEhlYWRlcihoZWFkZXIsIGVuY29kZVVSSUNvbXBvbmVudChoZWFkZXJWYWx1ZSkpXG4gICAgICAgIHhoci5zZXRSZXF1ZXN0SGVhZGVyKGhlYWRlciArICctVVJJLUF1dG9FbmNvZGVkJywgJ3RydWUnKVxuICAgICAgfVxuICAgIH1cbiAgfVxuXG4gIC8qKlxuICAgKiBAcGFyYW0ge1hNTEh0dHBSZXF1ZXN0fSB4aHJcbiAgICogQHJldHVybiB7c3RyaW5nfVxuICAgKi9cbiAgZnVuY3Rpb24gZ2V0UGF0aEZyb21SZXNwb25zZSh4aHIpIHtcbiAgLy8gTkI6IElFMTEgZG9lcyBub3Qgc3VwcG9ydCB0aGlzIHN0dWZmXG4gICAgaWYgKHhoci5yZXNwb25zZVVSTCAmJiB0eXBlb2YgKFVSTCkgIT09ICd1bmRlZmluZWQnKSB7XG4gICAgICB0cnkge1xuICAgICAgICBjb25zdCB1cmwgPSBuZXcgVVJMKHhoci5yZXNwb25zZVVSTClcbiAgICAgICAgcmV0dXJuIHVybC5wYXRobmFtZSArIHVybC5zZWFyY2hcbiAgICAgIH0gY2F0Y2ggKGUpIHtcbiAgICAgICAgdHJpZ2dlckVycm9yRXZlbnQoZ2V0RG9jdW1lbnQoKS5ib2R5LCAnaHRteDpiYWRSZXNwb25zZVVybCcsIHsgdXJsOiB4aHIucmVzcG9uc2VVUkwgfSlcbiAgICAgIH1cbiAgICB9XG4gIH1cblxuICAvKipcbiAgICogQHBhcmFtIHtYTUxIdHRwUmVxdWVzdH0geGhyXG4gICAqIEBwYXJhbSB7UmVnRXhwfSByZWdleHBcbiAgICogQHJldHVybiB7Ym9vbGVhbn1cbiAgICovXG4gIGZ1bmN0aW9uIGhhc0hlYWRlcih4aHIsIHJlZ2V4cCkge1xuICAgIHJldHVybiByZWdleHAudGVzdCh4aHIuZ2V0QWxsUmVzcG9uc2VIZWFkZXJzKCkpXG4gIH1cblxuICAvKipcbiAgICogSXNzdWVzIGFuIGh0bXgtc3R5bGUgQUpBWCByZXF1ZXN0XG4gICAqXG4gICAqIEBzZWUgaHR0cHM6Ly9odG14Lm9yZy9hcGkvI2FqYXhcbiAgICpcbiAgICogQHBhcmFtIHtIdHRwVmVyYn0gdmVyYlxuICAgKiBAcGFyYW0ge3N0cmluZ30gcGF0aCB0aGUgVVJMIHBhdGggdG8gbWFrZSB0aGUgQUpBWFxuICAgKiBAcGFyYW0ge0VsZW1lbnR8c3RyaW5nfEh0bXhBamF4SGVscGVyQ29udGV4dH0gY29udGV4dCB0aGUgZWxlbWVudCB0byB0YXJnZXQgKGRlZmF1bHRzIHRvIHRoZSAqKmJvZHkqKikgfCBhIHNlbGVjdG9yIGZvciB0aGUgdGFyZ2V0IHwgYSBjb250ZXh0IG9iamVjdCB0aGF0IGNvbnRhaW5zIGFueSBvZiB0aGUgZm9sbG93aW5nXG4gICAqIEByZXR1cm4ge1Byb21pc2U8dm9pZD59IFByb21pc2UgdGhhdCByZXNvbHZlcyBpbW1lZGlhdGVseSBpZiBubyByZXF1ZXN0IGlzIHNlbnQsIG9yIHdoZW4gdGhlIHJlcXVlc3QgaXMgY29tcGxldGVcbiAgICovXG4gIGZ1bmN0aW9uIGFqYXhIZWxwZXIodmVyYiwgcGF0aCwgY29udGV4dCkge1xuICAgIHZlcmIgPSAoLyoqIEB0eXBlIEh0dHBWZXJiICovKHZlcmIudG9Mb3dlckNhc2UoKSkpXG4gICAgaWYgKGNvbnRleHQpIHtcbiAgICAgIGlmIChjb250ZXh0IGluc3RhbmNlb2YgRWxlbWVudCB8fCB0eXBlb2YgY29udGV4dCA9PT0gJ3N0cmluZycpIHtcbiAgICAgICAgcmV0dXJuIGlzc3VlQWpheFJlcXVlc3QodmVyYiwgcGF0aCwgbnVsbCwgbnVsbCwge1xuICAgICAgICAgIHRhcmdldE92ZXJyaWRlOiByZXNvbHZlVGFyZ2V0KGNvbnRleHQpIHx8IERVTU1ZX0VMVCxcbiAgICAgICAgICByZXR1cm5Qcm9taXNlOiB0cnVlXG4gICAgICAgIH0pXG4gICAgICB9IGVsc2Uge1xuICAgICAgICBsZXQgcmVzb2x2ZWRUYXJnZXQgPSByZXNvbHZlVGFyZ2V0KGNvbnRleHQudGFyZ2V0KVxuICAgICAgICAvLyBJZiB0YXJnZXQgaXMgc3VwcGxpZWQgYnV0IGNhbid0IHJlc29sdmUgT1Igc291cmNlIGlzIHN1cHBsaWVkIGJ1dCBib3RoIHRhcmdldCBhbmQgc291cmNlIGNhbid0IGJlIHJlc29sdmVkXG4gICAgICAgIC8vIHRoZW4gdXNlIERVTU1ZX0VMVCB0byBhYm9ydCB0aGUgcmVxdWVzdCB3aXRoIGh0bXg6dGFyZ2V0RXJyb3IgdG8gYXZvaWQgaXQgcmVwbGFjaW5nIGJvZHkgYnkgbWlzdGFrZVxuICAgICAgICBpZiAoKGNvbnRleHQudGFyZ2V0ICYmICFyZXNvbHZlZFRhcmdldCkgfHwgKGNvbnRleHQuc291cmNlICYmICFyZXNvbHZlZFRhcmdldCAmJiAhcmVzb2x2ZVRhcmdldChjb250ZXh0LnNvdXJjZSkpKSB7XG4gICAgICAgICAgcmVzb2x2ZWRUYXJnZXQgPSBEVU1NWV9FTFRcbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gaXNzdWVBamF4UmVxdWVzdCh2ZXJiLCBwYXRoLCByZXNvbHZlVGFyZ2V0KGNvbnRleHQuc291cmNlKSwgY29udGV4dC5ldmVudCxcbiAgICAgICAgICB7XG4gICAgICAgICAgICBoYW5kbGVyOiBjb250ZXh0LmhhbmRsZXIsXG4gICAgICAgICAgICBoZWFkZXJzOiBjb250ZXh0LmhlYWRlcnMsXG4gICAgICAgICAgICB2YWx1ZXM6IGNvbnRleHQudmFsdWVzLFxuICAgICAgICAgICAgdGFyZ2V0T3ZlcnJpZGU6IHJlc29sdmVkVGFyZ2V0LFxuICAgICAgICAgICAgc3dhcE92ZXJyaWRlOiBjb250ZXh0LnN3YXAsXG4gICAgICAgICAgICBzZWxlY3Q6IGNvbnRleHQuc2VsZWN0LFxuICAgICAgICAgICAgcmV0dXJuUHJvbWlzZTogdHJ1ZVxuICAgICAgICAgIH0pXG4gICAgICB9XG4gICAgfSBlbHNlIHtcbiAgICAgIHJldHVybiBpc3N1ZUFqYXhSZXF1ZXN0KHZlcmIsIHBhdGgsIG51bGwsIG51bGwsIHtcbiAgICAgICAgcmV0dXJuUHJvbWlzZTogdHJ1ZVxuICAgICAgfSlcbiAgICB9XG4gIH1cblxuICAvKipcbiAgICogQHBhcmFtIHtFbGVtZW50fSBlbHRcbiAgICogQHJldHVybiB7RWxlbWVudFtdfVxuICAgKi9cbiAgZnVuY3Rpb24gaGllcmFyY2h5Rm9yRWx0KGVsdCkge1xuICAgIGNvbnN0IGFyciA9IFtdXG4gICAgd2hpbGUgKGVsdCkge1xuICAgICAgYXJyLnB1c2goZWx0KVxuICAgICAgZWx0ID0gZWx0LnBhcmVudEVsZW1lbnRcbiAgICB9XG4gICAgcmV0dXJuIGFyclxuICB9XG5cbiAgLyoqXG4gICAqIEBwYXJhbSB7RWxlbWVudH0gZWx0XG4gICAqIEBwYXJhbSB7c3RyaW5nfSBwYXRoXG4gICAqIEBwYXJhbSB7SHRteFJlcXVlc3RDb25maWd9IHJlcXVlc3RDb25maWdcbiAgICogQHJldHVybiB7Ym9vbGVhbn1cbiAgICovXG4gIGZ1bmN0aW9uIHZlcmlmeVBhdGgoZWx0LCBwYXRoLCByZXF1ZXN0Q29uZmlnKSB7XG4gICAgbGV0IHNhbWVIb3N0XG4gICAgbGV0IHVybFxuICAgIGlmICh0eXBlb2YgVVJMID09PSAnZnVuY3Rpb24nKSB7XG4gICAgICB1cmwgPSBuZXcgVVJMKHBhdGgsIGRvY3VtZW50LmxvY2F0aW9uLmhyZWYpXG4gICAgICBjb25zdCBvcmlnaW4gPSBkb2N1bWVudC5sb2NhdGlvbi5vcmlnaW5cbiAgICAgIHNhbWVIb3N0ID0gb3JpZ2luID09PSB1cmwub3JpZ2luXG4gICAgfSBlbHNlIHtcbiAgICAvLyBJRTExIGRvZXNuJ3Qgc3VwcG9ydCBVUkxcbiAgICAgIHVybCA9IHBhdGhcbiAgICAgIHNhbWVIb3N0ID0gc3RhcnRzV2l0aChwYXRoLCBkb2N1bWVudC5sb2NhdGlvbi5vcmlnaW4pXG4gICAgfVxuXG4gICAgaWYgKGh0bXguY29uZmlnLnNlbGZSZXF1ZXN0c09ubHkpIHtcbiAgICAgIGlmICghc2FtZUhvc3QpIHtcbiAgICAgICAgcmV0dXJuIGZhbHNlXG4gICAgICB9XG4gICAgfVxuICAgIHJldHVybiB0cmlnZ2VyRXZlbnQoZWx0LCAnaHRteDp2YWxpZGF0ZVVybCcsIG1lcmdlT2JqZWN0cyh7IHVybCwgc2FtZUhvc3QgfSwgcmVxdWVzdENvbmZpZykpXG4gIH1cblxuICAvKipcbiAgICogQHBhcmFtIHtPYmplY3R8Rm9ybURhdGF9IG9ialxuICAgKiBAcmV0dXJuIHtGb3JtRGF0YX1cbiAgICovXG4gIGZ1bmN0aW9uIGZvcm1EYXRhRnJvbU9iamVjdChvYmopIHtcbiAgICBpZiAob2JqIGluc3RhbmNlb2YgRm9ybURhdGEpIHJldHVybiBvYmpcbiAgICBjb25zdCBmb3JtRGF0YSA9IG5ldyBGb3JtRGF0YSgpXG4gICAgZm9yIChjb25zdCBrZXkgaW4gb2JqKSB7XG4gICAgICBpZiAob2JqLmhhc093blByb3BlcnR5KGtleSkpIHtcbiAgICAgICAgaWYgKG9ialtrZXldICYmIHR5cGVvZiBvYmpba2V5XS5mb3JFYWNoID09PSAnZnVuY3Rpb24nKSB7XG4gICAgICAgICAgb2JqW2tleV0uZm9yRWFjaChmdW5jdGlvbih2KSB7IGZvcm1EYXRhLmFwcGVuZChrZXksIHYpIH0pXG4gICAgICAgIH0gZWxzZSBpZiAodHlwZW9mIG9ialtrZXldID09PSAnb2JqZWN0JyAmJiAhKG9ialtrZXldIGluc3RhbmNlb2YgQmxvYikpIHtcbiAgICAgICAgICBmb3JtRGF0YS5hcHBlbmQoa2V5LCBKU09OLnN0cmluZ2lmeShvYmpba2V5XSkpXG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgZm9ybURhdGEuYXBwZW5kKGtleSwgb2JqW2tleV0pXG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9XG4gICAgcmV0dXJuIGZvcm1EYXRhXG4gIH1cblxuICAvKipcbiAgICogQHBhcmFtIHtGb3JtRGF0YX0gZm9ybURhdGFcbiAgICogQHBhcmFtIHtzdHJpbmd9IG5hbWVcbiAgICogQHBhcmFtIHtBcnJheX0gYXJyYXlcbiAgICogQHJldHVybnMge0FycmF5fVxuICAgKi9cbiAgZnVuY3Rpb24gZm9ybURhdGFBcnJheVByb3h5KGZvcm1EYXRhLCBuYW1lLCBhcnJheSkge1xuICAgIC8vIG11dGF0aW5nIHRoZSBhcnJheSBzaG91bGQgbXV0YXRlIHRoZSB1bmRlcmx5aW5nIGZvcm0gZGF0YVxuICAgIHJldHVybiBuZXcgUHJveHkoYXJyYXksIHtcbiAgICAgIGdldDogZnVuY3Rpb24odGFyZ2V0LCBrZXkpIHtcbiAgICAgICAgaWYgKHR5cGVvZiBrZXkgPT09ICdudW1iZXInKSByZXR1cm4gdGFyZ2V0W2tleV1cbiAgICAgICAgaWYgKGtleSA9PT0gJ2xlbmd0aCcpIHJldHVybiB0YXJnZXQubGVuZ3RoXG4gICAgICAgIGlmIChrZXkgPT09ICdwdXNoJykge1xuICAgICAgICAgIHJldHVybiBmdW5jdGlvbih2YWx1ZSkge1xuICAgICAgICAgICAgdGFyZ2V0LnB1c2godmFsdWUpXG4gICAgICAgICAgICBmb3JtRGF0YS5hcHBlbmQobmFtZSwgdmFsdWUpXG4gICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICAgIGlmICh0eXBlb2YgdGFyZ2V0W2tleV0gPT09ICdmdW5jdGlvbicpIHtcbiAgICAgICAgICByZXR1cm4gZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICB0YXJnZXRba2V5XS5hcHBseSh0YXJnZXQsIGFyZ3VtZW50cylcbiAgICAgICAgICAgIGZvcm1EYXRhLmRlbGV0ZShuYW1lKVxuICAgICAgICAgICAgdGFyZ2V0LmZvckVhY2goZnVuY3Rpb24odikgeyBmb3JtRGF0YS5hcHBlbmQobmFtZSwgdikgfSlcbiAgICAgICAgICB9XG4gICAgICAgIH1cblxuICAgICAgICBpZiAodGFyZ2V0W2tleV0gJiYgdGFyZ2V0W2tleV0ubGVuZ3RoID09PSAxKSB7XG4gICAgICAgICAgcmV0dXJuIHRhcmdldFtrZXldWzBdXG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgcmV0dXJuIHRhcmdldFtrZXldXG4gICAgICAgIH1cbiAgICAgIH0sXG4gICAgICBzZXQ6IGZ1bmN0aW9uKHRhcmdldCwgaW5kZXgsIHZhbHVlKSB7XG4gICAgICAgIHRhcmdldFtpbmRleF0gPSB2YWx1ZVxuICAgICAgICBmb3JtRGF0YS5kZWxldGUobmFtZSlcbiAgICAgICAgdGFyZ2V0LmZvckVhY2goZnVuY3Rpb24odikgeyBmb3JtRGF0YS5hcHBlbmQobmFtZSwgdikgfSlcbiAgICAgICAgcmV0dXJuIHRydWVcbiAgICAgIH1cbiAgICB9KVxuICB9XG5cbiAgLyoqXG4gICAqIEBwYXJhbSB7Rm9ybURhdGF9IGZvcm1EYXRhXG4gICAqIEByZXR1cm5zIHtPYmplY3R9XG4gICAqL1xuICBmdW5jdGlvbiBmb3JtRGF0YVByb3h5KGZvcm1EYXRhKSB7XG4gICAgcmV0dXJuIG5ldyBQcm94eShmb3JtRGF0YSwge1xuICAgICAgZ2V0OiBmdW5jdGlvbih0YXJnZXQsIG5hbWUpIHtcbiAgICAgICAgaWYgKHR5cGVvZiBuYW1lID09PSAnc3ltYm9sJykge1xuICAgICAgICAgIC8vIEZvcndhcmQgc3ltYm9sIGNhbGxzIHRvIHRoZSBGb3JtRGF0YSBpdHNlbGYgZGlyZWN0bHlcbiAgICAgICAgICBjb25zdCByZXN1bHQgPSBSZWZsZWN0LmdldCh0YXJnZXQsIG5hbWUpXG4gICAgICAgICAgLy8gV3JhcCBpbiBmdW5jdGlvbiB3aXRoIGFwcGx5IHRvIGNvcnJlY3RseSBiaW5kIHRoZSBGb3JtRGF0YSBjb250ZXh0LCBhcyBhIGRpcmVjdCBjYWxsIHdvdWxkIHJlc3VsdCBpbiBhbiBpbGxlZ2FsIGludm9jYXRpb24gZXJyb3JcbiAgICAgICAgICBpZiAodHlwZW9mIHJlc3VsdCA9PT0gJ2Z1bmN0aW9uJykge1xuICAgICAgICAgICAgcmV0dXJuIGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgICByZXR1cm4gcmVzdWx0LmFwcGx5KGZvcm1EYXRhLCBhcmd1bWVudHMpXG4gICAgICAgICAgICB9XG4gICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIHJldHVybiByZXN1bHRcbiAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgICAgaWYgKG5hbWUgPT09ICd0b0pTT04nKSB7XG4gICAgICAgICAgLy8gU3VwcG9ydCBKU09OLnN0cmluZ2lmeSBjYWxsIG9uIHByb3h5XG4gICAgICAgICAgcmV0dXJuICgpID0+IE9iamVjdC5mcm9tRW50cmllcyhmb3JtRGF0YSlcbiAgICAgICAgfVxuICAgICAgICBpZiAobmFtZSBpbiB0YXJnZXQpIHtcbiAgICAgICAgICAvLyBXcmFwIGluIGZ1bmN0aW9uIHdpdGggYXBwbHkgdG8gY29ycmVjdGx5IGJpbmQgdGhlIEZvcm1EYXRhIGNvbnRleHQsIGFzIGEgZGlyZWN0IGNhbGwgd291bGQgcmVzdWx0IGluIGFuIGlsbGVnYWwgaW52b2NhdGlvbiBlcnJvclxuICAgICAgICAgIGlmICh0eXBlb2YgdGFyZ2V0W25hbWVdID09PSAnZnVuY3Rpb24nKSB7XG4gICAgICAgICAgICByZXR1cm4gZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICAgIHJldHVybiBmb3JtRGF0YVtuYW1lXS5hcHBseShmb3JtRGF0YSwgYXJndW1lbnRzKVxuICAgICAgICAgICAgfVxuICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICByZXR1cm4gdGFyZ2V0W25hbWVdXG4gICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICAgIGNvbnN0IGFycmF5ID0gZm9ybURhdGEuZ2V0QWxsKG5hbWUpXG4gICAgICAgIC8vIFRob3NlIDIgdW5kZWZpbmVkICYgc2luZ2xlIHZhbHVlIHJldHVybnMgYXJlIGZvciByZXRyby1jb21wYXRpYmlsaXR5IGFzIHdlIHdlcmVuJ3QgdXNpbmcgRm9ybURhdGEgYmVmb3JlXG4gICAgICAgIGlmIChhcnJheS5sZW5ndGggPT09IDApIHtcbiAgICAgICAgICByZXR1cm4gdW5kZWZpbmVkXG4gICAgICAgIH0gZWxzZSBpZiAoYXJyYXkubGVuZ3RoID09PSAxKSB7XG4gICAgICAgICAgcmV0dXJuIGFycmF5WzBdXG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgcmV0dXJuIGZvcm1EYXRhQXJyYXlQcm94eSh0YXJnZXQsIG5hbWUsIGFycmF5KVxuICAgICAgICB9XG4gICAgICB9LFxuICAgICAgc2V0OiBmdW5jdGlvbih0YXJnZXQsIG5hbWUsIHZhbHVlKSB7XG4gICAgICAgIGlmICh0eXBlb2YgbmFtZSAhPT0gJ3N0cmluZycpIHtcbiAgICAgICAgICByZXR1cm4gZmFsc2VcbiAgICAgICAgfVxuICAgICAgICB0YXJnZXQuZGVsZXRlKG5hbWUpXG4gICAgICAgIGlmICh2YWx1ZSAmJiB0eXBlb2YgdmFsdWUuZm9yRWFjaCA9PT0gJ2Z1bmN0aW9uJykge1xuICAgICAgICAgIHZhbHVlLmZvckVhY2goZnVuY3Rpb24odikgeyB0YXJnZXQuYXBwZW5kKG5hbWUsIHYpIH0pXG4gICAgICAgIH0gZWxzZSBpZiAodHlwZW9mIHZhbHVlID09PSAnb2JqZWN0JyAmJiAhKHZhbHVlIGluc3RhbmNlb2YgQmxvYikpIHtcbiAgICAgICAgICB0YXJnZXQuYXBwZW5kKG5hbWUsIEpTT04uc3RyaW5naWZ5KHZhbHVlKSlcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICB0YXJnZXQuYXBwZW5kKG5hbWUsIHZhbHVlKVxuICAgICAgICB9XG4gICAgICAgIHJldHVybiB0cnVlXG4gICAgICB9LFxuICAgICAgZGVsZXRlUHJvcGVydHk6IGZ1bmN0aW9uKHRhcmdldCwgbmFtZSkge1xuICAgICAgICBpZiAodHlwZW9mIG5hbWUgPT09ICdzdHJpbmcnKSB7XG4gICAgICAgICAgdGFyZ2V0LmRlbGV0ZShuYW1lKVxuICAgICAgICB9XG4gICAgICAgIHJldHVybiB0cnVlXG4gICAgICB9LFxuICAgICAgLy8gU3VwcG9ydCBPYmplY3QuYXNzaWduIGNhbGwgZnJvbSBwcm94eVxuICAgICAgb3duS2V5czogZnVuY3Rpb24odGFyZ2V0KSB7XG4gICAgICAgIHJldHVybiBSZWZsZWN0Lm93bktleXMoT2JqZWN0LmZyb21FbnRyaWVzKHRhcmdldCkpXG4gICAgICB9LFxuICAgICAgZ2V0T3duUHJvcGVydHlEZXNjcmlwdG9yOiBmdW5jdGlvbih0YXJnZXQsIHByb3ApIHtcbiAgICAgICAgcmV0dXJuIFJlZmxlY3QuZ2V0T3duUHJvcGVydHlEZXNjcmlwdG9yKE9iamVjdC5mcm9tRW50cmllcyh0YXJnZXQpLCBwcm9wKVxuICAgICAgfVxuICAgIH0pXG4gIH1cblxuICAvKipcbiAgICogQHBhcmFtIHtIdHRwVmVyYn0gdmVyYlxuICAgKiBAcGFyYW0ge3N0cmluZ30gcGF0aFxuICAgKiBAcGFyYW0ge0VsZW1lbnR9IGVsdFxuICAgKiBAcGFyYW0ge0V2ZW50fSBldmVudFxuICAgKiBAcGFyYW0ge0h0bXhBamF4RXRjfSBbZXRjXVxuICAgKiBAcGFyYW0ge2Jvb2xlYW59IFtjb25maXJtZWRdXG4gICAqIEByZXR1cm4ge1Byb21pc2U8dm9pZD59XG4gICAqL1xuICBmdW5jdGlvbiBpc3N1ZUFqYXhSZXF1ZXN0KHZlcmIsIHBhdGgsIGVsdCwgZXZlbnQsIGV0YywgY29uZmlybWVkKSB7XG4gICAgbGV0IHJlc29sdmUgPSBudWxsXG4gICAgbGV0IHJlamVjdCA9IG51bGxcbiAgICBldGMgPSBldGMgIT0gbnVsbCA/IGV0YyA6IHt9XG4gICAgaWYgKGV0Yy5yZXR1cm5Qcm9taXNlICYmIHR5cGVvZiBQcm9taXNlICE9PSAndW5kZWZpbmVkJykge1xuICAgICAgdmFyIHByb21pc2UgPSBuZXcgUHJvbWlzZShmdW5jdGlvbihfcmVzb2x2ZSwgX3JlamVjdCkge1xuICAgICAgICByZXNvbHZlID0gX3Jlc29sdmVcbiAgICAgICAgcmVqZWN0ID0gX3JlamVjdFxuICAgICAgfSlcbiAgICB9XG4gICAgaWYgKGVsdCA9PSBudWxsKSB7XG4gICAgICBlbHQgPSBnZXREb2N1bWVudCgpLmJvZHlcbiAgICB9XG4gICAgY29uc3QgcmVzcG9uc2VIYW5kbGVyID0gZXRjLmhhbmRsZXIgfHwgaGFuZGxlQWpheFJlc3BvbnNlXG4gICAgY29uc3Qgc2VsZWN0ID0gZXRjLnNlbGVjdCB8fCBudWxsXG5cbiAgICBpZiAoIWJvZHlDb250YWlucyhlbHQpKSB7XG4gICAgLy8gZG8gbm90IGlzc3VlIHJlcXVlc3RzIGZvciBlbGVtZW50cyByZW1vdmVkIGZyb20gdGhlIERPTVxuICAgICAgbWF5YmVDYWxsKHJlc29sdmUpXG4gICAgICByZXR1cm4gcHJvbWlzZVxuICAgIH1cbiAgICBjb25zdCB0YXJnZXQgPSBldGMudGFyZ2V0T3ZlcnJpZGUgfHwgYXNFbGVtZW50KGdldFRhcmdldChlbHQpKVxuICAgIGlmICh0YXJnZXQgPT0gbnVsbCB8fCB0YXJnZXQgPT0gRFVNTVlfRUxUKSB7XG4gICAgICB0cmlnZ2VyRXJyb3JFdmVudChlbHQsICdodG14OnRhcmdldEVycm9yJywgeyB0YXJnZXQ6IGdldEF0dHJpYnV0ZVZhbHVlKGVsdCwgJ2h4LXRhcmdldCcpIH0pXG4gICAgICBtYXliZUNhbGwocmVqZWN0KVxuICAgICAgcmV0dXJuIHByb21pc2VcbiAgICB9XG5cbiAgICBsZXQgZWx0RGF0YSA9IGdldEludGVybmFsRGF0YShlbHQpXG4gICAgY29uc3Qgc3VibWl0dGVyID0gZWx0RGF0YS5sYXN0QnV0dG9uQ2xpY2tlZFxuXG4gICAgaWYgKHN1Ym1pdHRlcikge1xuICAgICAgY29uc3QgYnV0dG9uUGF0aCA9IGdldFJhd0F0dHJpYnV0ZShzdWJtaXR0ZXIsICdmb3JtYWN0aW9uJylcbiAgICAgIGlmIChidXR0b25QYXRoICE9IG51bGwpIHtcbiAgICAgICAgcGF0aCA9IGJ1dHRvblBhdGhcbiAgICAgIH1cblxuICAgICAgY29uc3QgYnV0dG9uVmVyYiA9IGdldFJhd0F0dHJpYnV0ZShzdWJtaXR0ZXIsICdmb3JtbWV0aG9kJylcbiAgICAgIGlmIChidXR0b25WZXJiICE9IG51bGwpIHtcbiAgICAgIC8vIGlnbm9yZSBidXR0b25zIHdpdGggZm9ybW1ldGhvZD1cImRpYWxvZ1wiXG4gICAgICAgIGlmIChidXR0b25WZXJiLnRvTG93ZXJDYXNlKCkgIT09ICdkaWFsb2cnKSB7XG4gICAgICAgICAgdmVyYiA9ICgvKiogQHR5cGUgSHR0cFZlcmIgKi8oYnV0dG9uVmVyYikpXG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9XG5cbiAgICBjb25zdCBjb25maXJtUXVlc3Rpb24gPSBnZXRDbG9zZXN0QXR0cmlidXRlVmFsdWUoZWx0LCAnaHgtY29uZmlybScpXG4gICAgLy8gYWxsb3cgZXZlbnQtYmFzZWQgY29uZmlybWF0aW9uIHcvIGEgY2FsbGJhY2tcbiAgICBpZiAoY29uZmlybWVkID09PSB1bmRlZmluZWQpIHtcbiAgICAgIGNvbnN0IGlzc3VlUmVxdWVzdCA9IGZ1bmN0aW9uKHNraXBDb25maXJtYXRpb24pIHtcbiAgICAgICAgcmV0dXJuIGlzc3VlQWpheFJlcXVlc3QodmVyYiwgcGF0aCwgZWx0LCBldmVudCwgZXRjLCAhIXNraXBDb25maXJtYXRpb24pXG4gICAgICB9XG4gICAgICBjb25zdCBjb25maXJtRGV0YWlscyA9IHsgdGFyZ2V0LCBlbHQsIHBhdGgsIHZlcmIsIHRyaWdnZXJpbmdFdmVudDogZXZlbnQsIGV0YywgaXNzdWVSZXF1ZXN0LCBxdWVzdGlvbjogY29uZmlybVF1ZXN0aW9uIH1cbiAgICAgIGlmICh0cmlnZ2VyRXZlbnQoZWx0LCAnaHRteDpjb25maXJtJywgY29uZmlybURldGFpbHMpID09PSBmYWxzZSkge1xuICAgICAgICBtYXliZUNhbGwocmVzb2x2ZSlcbiAgICAgICAgcmV0dXJuIHByb21pc2VcbiAgICAgIH1cbiAgICB9XG5cbiAgICBsZXQgc3luY0VsdCA9IGVsdFxuICAgIGxldCBzeW5jU3RyYXRlZ3kgPSBnZXRDbG9zZXN0QXR0cmlidXRlVmFsdWUoZWx0LCAnaHgtc3luYycpXG4gICAgbGV0IHF1ZXVlU3RyYXRlZ3kgPSBudWxsXG4gICAgbGV0IGFib3J0YWJsZSA9IGZhbHNlXG4gICAgaWYgKHN5bmNTdHJhdGVneSkge1xuICAgICAgY29uc3Qgc3luY1N0cmluZ3MgPSBzeW5jU3RyYXRlZ3kuc3BsaXQoJzonKVxuICAgICAgY29uc3Qgc2VsZWN0b3IgPSBzeW5jU3RyaW5nc1swXS50cmltKClcbiAgICAgIGlmIChzZWxlY3RvciA9PT0gJ3RoaXMnKSB7XG4gICAgICAgIHN5bmNFbHQgPSBmaW5kVGhpc0VsZW1lbnQoZWx0LCAnaHgtc3luYycpXG4gICAgICB9IGVsc2Uge1xuICAgICAgICBzeW5jRWx0ID0gYXNFbGVtZW50KHF1ZXJ5U2VsZWN0b3JFeHQoZWx0LCBzZWxlY3RvcikpXG4gICAgICB9XG4gICAgICAvLyBkZWZhdWx0IHRvIHRoZSBkcm9wIHN0cmF0ZWd5XG4gICAgICBzeW5jU3RyYXRlZ3kgPSAoc3luY1N0cmluZ3NbMV0gfHwgJ2Ryb3AnKS50cmltKClcbiAgICAgIGVsdERhdGEgPSBnZXRJbnRlcm5hbERhdGEoc3luY0VsdClcbiAgICAgIGlmIChzeW5jU3RyYXRlZ3kgPT09ICdkcm9wJyAmJiBlbHREYXRhLnhociAmJiBlbHREYXRhLmFib3J0YWJsZSAhPT0gdHJ1ZSkge1xuICAgICAgICBtYXliZUNhbGwocmVzb2x2ZSlcbiAgICAgICAgcmV0dXJuIHByb21pc2VcbiAgICAgIH0gZWxzZSBpZiAoc3luY1N0cmF0ZWd5ID09PSAnYWJvcnQnKSB7XG4gICAgICAgIGlmIChlbHREYXRhLnhocikge1xuICAgICAgICAgIG1heWJlQ2FsbChyZXNvbHZlKVxuICAgICAgICAgIHJldHVybiBwcm9taXNlXG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgYWJvcnRhYmxlID0gdHJ1ZVxuICAgICAgICB9XG4gICAgICB9IGVsc2UgaWYgKHN5bmNTdHJhdGVneSA9PT0gJ3JlcGxhY2UnKSB7XG4gICAgICAgIHRyaWdnZXJFdmVudChzeW5jRWx0LCAnaHRteDphYm9ydCcpIC8vIGFib3J0IHRoZSBjdXJyZW50IHJlcXVlc3QgYW5kIGNvbnRpbnVlXG4gICAgICB9IGVsc2UgaWYgKHN5bmNTdHJhdGVneS5pbmRleE9mKCdxdWV1ZScpID09PSAwKSB7XG4gICAgICAgIGNvbnN0IHF1ZXVlU3RyQXJyYXkgPSBzeW5jU3RyYXRlZ3kuc3BsaXQoJyAnKVxuICAgICAgICBxdWV1ZVN0cmF0ZWd5ID0gKHF1ZXVlU3RyQXJyYXlbMV0gfHwgJ2xhc3QnKS50cmltKClcbiAgICAgIH1cbiAgICB9XG5cbiAgICBpZiAoZWx0RGF0YS54aHIpIHtcbiAgICAgIGlmIChlbHREYXRhLmFib3J0YWJsZSkge1xuICAgICAgICB0cmlnZ2VyRXZlbnQoc3luY0VsdCwgJ2h0bXg6YWJvcnQnKSAvLyBhYm9ydCB0aGUgY3VycmVudCByZXF1ZXN0IGFuZCBjb250aW51ZVxuICAgICAgfSBlbHNlIHtcbiAgICAgICAgaWYgKHF1ZXVlU3RyYXRlZ3kgPT0gbnVsbCkge1xuICAgICAgICAgIGlmIChldmVudCkge1xuICAgICAgICAgICAgY29uc3QgZXZlbnREYXRhID0gZ2V0SW50ZXJuYWxEYXRhKGV2ZW50KVxuICAgICAgICAgICAgaWYgKGV2ZW50RGF0YSAmJiBldmVudERhdGEudHJpZ2dlclNwZWMgJiYgZXZlbnREYXRhLnRyaWdnZXJTcGVjLnF1ZXVlKSB7XG4gICAgICAgICAgICAgIHF1ZXVlU3RyYXRlZ3kgPSBldmVudERhdGEudHJpZ2dlclNwZWMucXVldWVcbiAgICAgICAgICAgIH1cbiAgICAgICAgICB9XG4gICAgICAgICAgaWYgKHF1ZXVlU3RyYXRlZ3kgPT0gbnVsbCkge1xuICAgICAgICAgICAgcXVldWVTdHJhdGVneSA9ICdsYXN0J1xuICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgICBpZiAoZWx0RGF0YS5xdWV1ZWRSZXF1ZXN0cyA9PSBudWxsKSB7XG4gICAgICAgICAgZWx0RGF0YS5xdWV1ZWRSZXF1ZXN0cyA9IFtdXG4gICAgICAgIH1cbiAgICAgICAgaWYgKHF1ZXVlU3RyYXRlZ3kgPT09ICdmaXJzdCcgJiYgZWx0RGF0YS5xdWV1ZWRSZXF1ZXN0cy5sZW5ndGggPT09IDApIHtcbiAgICAgICAgICBlbHREYXRhLnF1ZXVlZFJlcXVlc3RzLnB1c2goZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICBpc3N1ZUFqYXhSZXF1ZXN0KHZlcmIsIHBhdGgsIGVsdCwgZXZlbnQsIGV0YylcbiAgICAgICAgICB9KVxuICAgICAgICB9IGVsc2UgaWYgKHF1ZXVlU3RyYXRlZ3kgPT09ICdhbGwnKSB7XG4gICAgICAgICAgZWx0RGF0YS5xdWV1ZWRSZXF1ZXN0cy5wdXNoKGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgaXNzdWVBamF4UmVxdWVzdCh2ZXJiLCBwYXRoLCBlbHQsIGV2ZW50LCBldGMpXG4gICAgICAgICAgfSlcbiAgICAgICAgfSBlbHNlIGlmIChxdWV1ZVN0cmF0ZWd5ID09PSAnbGFzdCcpIHtcbiAgICAgICAgICBlbHREYXRhLnF1ZXVlZFJlcXVlc3RzID0gW10gLy8gZHVtcCBleGlzdGluZyBxdWV1ZVxuICAgICAgICAgIGVsdERhdGEucXVldWVkUmVxdWVzdHMucHVzaChmdW5jdGlvbigpIHtcbiAgICAgICAgICAgIGlzc3VlQWpheFJlcXVlc3QodmVyYiwgcGF0aCwgZWx0LCBldmVudCwgZXRjKVxuICAgICAgICAgIH0pXG4gICAgICAgIH1cbiAgICAgICAgbWF5YmVDYWxsKHJlc29sdmUpXG4gICAgICAgIHJldHVybiBwcm9taXNlXG4gICAgICB9XG4gICAgfVxuXG4gICAgY29uc3QgeGhyID0gbmV3IFhNTEh0dHBSZXF1ZXN0KClcbiAgICBlbHREYXRhLnhociA9IHhoclxuICAgIGVsdERhdGEuYWJvcnRhYmxlID0gYWJvcnRhYmxlXG4gICAgY29uc3QgZW5kUmVxdWVzdExvY2sgPSBmdW5jdGlvbigpIHtcbiAgICAgIGVsdERhdGEueGhyID0gbnVsbFxuICAgICAgZWx0RGF0YS5hYm9ydGFibGUgPSBmYWxzZVxuICAgICAgaWYgKGVsdERhdGEucXVldWVkUmVxdWVzdHMgIT0gbnVsbCAmJlxuICAgICAgZWx0RGF0YS5xdWV1ZWRSZXF1ZXN0cy5sZW5ndGggPiAwKSB7XG4gICAgICAgIGNvbnN0IHF1ZXVlZFJlcXVlc3QgPSBlbHREYXRhLnF1ZXVlZFJlcXVlc3RzLnNoaWZ0KClcbiAgICAgICAgcXVldWVkUmVxdWVzdCgpXG4gICAgICB9XG4gICAgfVxuICAgIGNvbnN0IHByb21wdFF1ZXN0aW9uID0gZ2V0Q2xvc2VzdEF0dHJpYnV0ZVZhbHVlKGVsdCwgJ2h4LXByb21wdCcpXG4gICAgaWYgKHByb21wdFF1ZXN0aW9uKSB7XG4gICAgICB2YXIgcHJvbXB0UmVzcG9uc2UgPSBwcm9tcHQocHJvbXB0UXVlc3Rpb24pXG4gICAgICAvLyBwcm9tcHQgcmV0dXJucyBudWxsIGlmIGNhbmNlbGxlZCBhbmQgZW1wdHkgc3RyaW5nIGlmIGFjY2VwdGVkIHdpdGggbm8gZW50cnlcbiAgICAgIGlmIChwcm9tcHRSZXNwb25zZSA9PT0gbnVsbCB8fFxuICAgICAgIXRyaWdnZXJFdmVudChlbHQsICdodG14OnByb21wdCcsIHsgcHJvbXB0OiBwcm9tcHRSZXNwb25zZSwgdGFyZ2V0IH0pKSB7XG4gICAgICAgIG1heWJlQ2FsbChyZXNvbHZlKVxuICAgICAgICBlbmRSZXF1ZXN0TG9jaygpXG4gICAgICAgIHJldHVybiBwcm9taXNlXG4gICAgICB9XG4gICAgfVxuXG4gICAgaWYgKGNvbmZpcm1RdWVzdGlvbiAmJiAhY29uZmlybWVkKSB7XG4gICAgICBpZiAoIWNvbmZpcm0oY29uZmlybVF1ZXN0aW9uKSkge1xuICAgICAgICBtYXliZUNhbGwocmVzb2x2ZSlcbiAgICAgICAgZW5kUmVxdWVzdExvY2soKVxuICAgICAgICByZXR1cm4gcHJvbWlzZVxuICAgICAgfVxuICAgIH1cblxuICAgIGxldCBoZWFkZXJzID0gZ2V0SGVhZGVycyhlbHQsIHRhcmdldCwgcHJvbXB0UmVzcG9uc2UpXG5cbiAgICBpZiAodmVyYiAhPT0gJ2dldCcgJiYgIXVzZXNGb3JtRGF0YShlbHQpKSB7XG4gICAgICBoZWFkZXJzWydDb250ZW50LVR5cGUnXSA9ICdhcHBsaWNhdGlvbi94LXd3dy1mb3JtLXVybGVuY29kZWQnXG4gICAgfVxuXG4gICAgaWYgKGV0Yy5oZWFkZXJzKSB7XG4gICAgICBoZWFkZXJzID0gbWVyZ2VPYmplY3RzKGhlYWRlcnMsIGV0Yy5oZWFkZXJzKVxuICAgIH1cbiAgICBjb25zdCByZXN1bHRzID0gZ2V0SW5wdXRWYWx1ZXMoZWx0LCB2ZXJiKVxuICAgIGxldCBlcnJvcnMgPSByZXN1bHRzLmVycm9yc1xuICAgIGNvbnN0IHJhd0Zvcm1EYXRhID0gcmVzdWx0cy5mb3JtRGF0YVxuICAgIGlmIChldGMudmFsdWVzKSB7XG4gICAgICBvdmVycmlkZUZvcm1EYXRhKHJhd0Zvcm1EYXRhLCBmb3JtRGF0YUZyb21PYmplY3QoZXRjLnZhbHVlcykpXG4gICAgfVxuICAgIGNvbnN0IGV4cHJlc3Npb25WYXJzID0gZm9ybURhdGFGcm9tT2JqZWN0KGdldEV4cHJlc3Npb25WYXJzKGVsdCkpXG4gICAgY29uc3QgYWxsRm9ybURhdGEgPSBvdmVycmlkZUZvcm1EYXRhKHJhd0Zvcm1EYXRhLCBleHByZXNzaW9uVmFycylcbiAgICBsZXQgZmlsdGVyZWRGb3JtRGF0YSA9IGZpbHRlclZhbHVlcyhhbGxGb3JtRGF0YSwgZWx0KVxuXG4gICAgaWYgKGh0bXguY29uZmlnLmdldENhY2hlQnVzdGVyUGFyYW0gJiYgdmVyYiA9PT0gJ2dldCcpIHtcbiAgICAgIGZpbHRlcmVkRm9ybURhdGEuc2V0KCdvcmcuaHRteC5jYWNoZS1idXN0ZXInLCBnZXRSYXdBdHRyaWJ1dGUodGFyZ2V0LCAnaWQnKSB8fCAndHJ1ZScpXG4gICAgfVxuXG4gICAgLy8gYmVoYXZpb3Igb2YgYW5jaG9ycyB3LyBlbXB0eSBocmVmIGlzIHRvIHVzZSB0aGUgY3VycmVudCBVUkxcbiAgICBpZiAocGF0aCA9PSBudWxsIHx8IHBhdGggPT09ICcnKSB7XG4gICAgICBwYXRoID0gZ2V0RG9jdW1lbnQoKS5sb2NhdGlvbi5ocmVmXG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogQHR5cGUge09iamVjdH1cbiAgICAgKiBAcHJvcGVydHkge2Jvb2xlYW59IFtjcmVkZW50aWFsc11cbiAgICAgKiBAcHJvcGVydHkge251bWJlcn0gW3RpbWVvdXRdXG4gICAgICogQHByb3BlcnR5IHtib29sZWFufSBbbm9IZWFkZXJzXVxuICAgICAqL1xuICAgIGNvbnN0IHJlcXVlc3RBdHRyVmFsdWVzID0gZ2V0VmFsdWVzRm9yRWxlbWVudChlbHQsICdoeC1yZXF1ZXN0JylcblxuICAgIGNvbnN0IGVsdElzQm9vc3RlZCA9IGdldEludGVybmFsRGF0YShlbHQpLmJvb3N0ZWRcblxuICAgIGxldCB1c2VVcmxQYXJhbXMgPSBodG14LmNvbmZpZy5tZXRob2RzVGhhdFVzZVVybFBhcmFtcy5pbmRleE9mKHZlcmIpID49IDBcblxuICAgIC8qKiBAdHlwZSBIdG14UmVxdWVzdENvbmZpZyAqL1xuICAgIGNvbnN0IHJlcXVlc3RDb25maWcgPSB7XG4gICAgICBib29zdGVkOiBlbHRJc0Jvb3N0ZWQsXG4gICAgICB1c2VVcmxQYXJhbXMsXG4gICAgICBmb3JtRGF0YTogZmlsdGVyZWRGb3JtRGF0YSxcbiAgICAgIHBhcmFtZXRlcnM6IGZvcm1EYXRhUHJveHkoZmlsdGVyZWRGb3JtRGF0YSksXG4gICAgICB1bmZpbHRlcmVkRm9ybURhdGE6IGFsbEZvcm1EYXRhLFxuICAgICAgdW5maWx0ZXJlZFBhcmFtZXRlcnM6IGZvcm1EYXRhUHJveHkoYWxsRm9ybURhdGEpLFxuICAgICAgaGVhZGVycyxcbiAgICAgIHRhcmdldCxcbiAgICAgIHZlcmIsXG4gICAgICBlcnJvcnMsXG4gICAgICB3aXRoQ3JlZGVudGlhbHM6IGV0Yy5jcmVkZW50aWFscyB8fCByZXF1ZXN0QXR0clZhbHVlcy5jcmVkZW50aWFscyB8fCBodG14LmNvbmZpZy53aXRoQ3JlZGVudGlhbHMsXG4gICAgICB0aW1lb3V0OiBldGMudGltZW91dCB8fCByZXF1ZXN0QXR0clZhbHVlcy50aW1lb3V0IHx8IGh0bXguY29uZmlnLnRpbWVvdXQsXG4gICAgICBwYXRoLFxuICAgICAgdHJpZ2dlcmluZ0V2ZW50OiBldmVudFxuICAgIH1cblxuICAgIGlmICghdHJpZ2dlckV2ZW50KGVsdCwgJ2h0bXg6Y29uZmlnUmVxdWVzdCcsIHJlcXVlc3RDb25maWcpKSB7XG4gICAgICBtYXliZUNhbGwocmVzb2x2ZSlcbiAgICAgIGVuZFJlcXVlc3RMb2NrKClcbiAgICAgIHJldHVybiBwcm9taXNlXG4gICAgfVxuXG4gICAgLy8gY29weSBvdXQgaW4gY2FzZSB0aGUgb2JqZWN0IHdhcyBvdmVyd3JpdHRlblxuICAgIHBhdGggPSByZXF1ZXN0Q29uZmlnLnBhdGhcbiAgICB2ZXJiID0gcmVxdWVzdENvbmZpZy52ZXJiXG4gICAgaGVhZGVycyA9IHJlcXVlc3RDb25maWcuaGVhZGVyc1xuICAgIGZpbHRlcmVkRm9ybURhdGEgPSBmb3JtRGF0YUZyb21PYmplY3QocmVxdWVzdENvbmZpZy5wYXJhbWV0ZXJzKVxuICAgIGVycm9ycyA9IHJlcXVlc3RDb25maWcuZXJyb3JzXG4gICAgdXNlVXJsUGFyYW1zID0gcmVxdWVzdENvbmZpZy51c2VVcmxQYXJhbXNcblxuICAgIGlmIChlcnJvcnMgJiYgZXJyb3JzLmxlbmd0aCA+IDApIHtcbiAgICAgIHRyaWdnZXJFdmVudChlbHQsICdodG14OnZhbGlkYXRpb246aGFsdGVkJywgcmVxdWVzdENvbmZpZylcbiAgICAgIG1heWJlQ2FsbChyZXNvbHZlKVxuICAgICAgZW5kUmVxdWVzdExvY2soKVxuICAgICAgcmV0dXJuIHByb21pc2VcbiAgICB9XG5cbiAgICBjb25zdCBzcGxpdFBhdGggPSBwYXRoLnNwbGl0KCcjJylcbiAgICBjb25zdCBwYXRoTm9BbmNob3IgPSBzcGxpdFBhdGhbMF1cbiAgICBjb25zdCBhbmNob3IgPSBzcGxpdFBhdGhbMV1cblxuICAgIGxldCBmaW5hbFBhdGggPSBwYXRoXG4gICAgaWYgKHVzZVVybFBhcmFtcykge1xuICAgICAgZmluYWxQYXRoID0gcGF0aE5vQW5jaG9yXG4gICAgICBjb25zdCBoYXNWYWx1ZXMgPSAhZmlsdGVyZWRGb3JtRGF0YS5rZXlzKCkubmV4dCgpLmRvbmVcbiAgICAgIGlmIChoYXNWYWx1ZXMpIHtcbiAgICAgICAgaWYgKGZpbmFsUGF0aC5pbmRleE9mKCc/JykgPCAwKSB7XG4gICAgICAgICAgZmluYWxQYXRoICs9ICc/J1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgIGZpbmFsUGF0aCArPSAnJidcbiAgICAgICAgfVxuICAgICAgICBmaW5hbFBhdGggKz0gdXJsRW5jb2RlKGZpbHRlcmVkRm9ybURhdGEpXG4gICAgICAgIGlmIChhbmNob3IpIHtcbiAgICAgICAgICBmaW5hbFBhdGggKz0gJyMnICsgYW5jaG9yXG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9XG5cbiAgICBpZiAoIXZlcmlmeVBhdGgoZWx0LCBmaW5hbFBhdGgsIHJlcXVlc3RDb25maWcpKSB7XG4gICAgICB0cmlnZ2VyRXJyb3JFdmVudChlbHQsICdodG14OmludmFsaWRQYXRoJywgcmVxdWVzdENvbmZpZylcbiAgICAgIG1heWJlQ2FsbChyZWplY3QpXG4gICAgICByZXR1cm4gcHJvbWlzZVxuICAgIH1cblxuICAgIHhoci5vcGVuKHZlcmIudG9VcHBlckNhc2UoKSwgZmluYWxQYXRoLCB0cnVlKVxuICAgIHhoci5vdmVycmlkZU1pbWVUeXBlKCd0ZXh0L2h0bWwnKVxuICAgIHhoci53aXRoQ3JlZGVudGlhbHMgPSByZXF1ZXN0Q29uZmlnLndpdGhDcmVkZW50aWFsc1xuICAgIHhoci50aW1lb3V0ID0gcmVxdWVzdENvbmZpZy50aW1lb3V0XG5cbiAgICAvLyByZXF1ZXN0IGhlYWRlcnNcbiAgICBpZiAocmVxdWVzdEF0dHJWYWx1ZXMubm9IZWFkZXJzKSB7XG4gICAgLy8gaWdub3JlIGFsbCBoZWFkZXJzXG4gICAgfSBlbHNlIHtcbiAgICAgIGZvciAoY29uc3QgaGVhZGVyIGluIGhlYWRlcnMpIHtcbiAgICAgICAgaWYgKGhlYWRlcnMuaGFzT3duUHJvcGVydHkoaGVhZGVyKSkge1xuICAgICAgICAgIGNvbnN0IGhlYWRlclZhbHVlID0gaGVhZGVyc1toZWFkZXJdXG4gICAgICAgICAgc2FmZWx5U2V0SGVhZGVyVmFsdWUoeGhyLCBoZWFkZXIsIGhlYWRlclZhbHVlKVxuICAgICAgICB9XG4gICAgICB9XG4gICAgfVxuXG4gICAgLyoqIEB0eXBlIHtIdG14UmVzcG9uc2VJbmZvfSAqL1xuICAgIGNvbnN0IHJlc3BvbnNlSW5mbyA9IHtcbiAgICAgIHhocixcbiAgICAgIHRhcmdldCxcbiAgICAgIHJlcXVlc3RDb25maWcsXG4gICAgICBldGMsXG4gICAgICBib29zdGVkOiBlbHRJc0Jvb3N0ZWQsXG4gICAgICBzZWxlY3QsXG4gICAgICBwYXRoSW5mbzoge1xuICAgICAgICByZXF1ZXN0UGF0aDogcGF0aCxcbiAgICAgICAgZmluYWxSZXF1ZXN0UGF0aDogZmluYWxQYXRoLFxuICAgICAgICByZXNwb25zZVBhdGg6IG51bGwsXG4gICAgICAgIGFuY2hvclxuICAgICAgfVxuICAgIH1cblxuICAgIHhoci5vbmxvYWQgPSBmdW5jdGlvbigpIHtcbiAgICAgIHRyeSB7XG4gICAgICAgIGNvbnN0IGhpZXJhcmNoeSA9IGhpZXJhcmNoeUZvckVsdChlbHQpXG4gICAgICAgIHJlc3BvbnNlSW5mby5wYXRoSW5mby5yZXNwb25zZVBhdGggPSBnZXRQYXRoRnJvbVJlc3BvbnNlKHhocilcbiAgICAgICAgcmVzcG9uc2VIYW5kbGVyKGVsdCwgcmVzcG9uc2VJbmZvKVxuICAgICAgICBpZiAocmVzcG9uc2VJbmZvLmtlZXBJbmRpY2F0b3JzICE9PSB0cnVlKSB7XG4gICAgICAgICAgcmVtb3ZlUmVxdWVzdEluZGljYXRvcnMoaW5kaWNhdG9ycywgZGlzYWJsZUVsdHMpXG4gICAgICAgIH1cbiAgICAgICAgdHJpZ2dlckV2ZW50KGVsdCwgJ2h0bXg6YWZ0ZXJSZXF1ZXN0JywgcmVzcG9uc2VJbmZvKVxuICAgICAgICB0cmlnZ2VyRXZlbnQoZWx0LCAnaHRteDphZnRlck9uTG9hZCcsIHJlc3BvbnNlSW5mbylcbiAgICAgICAgLy8gaWYgdGhlIGJvZHkgbm8gbG9uZ2VyIGNvbnRhaW5zIHRoZSBlbGVtZW50LCB0cmlnZ2VyIHRoZSBldmVudCBvbiB0aGUgY2xvc2VzdCBwYXJlbnRcbiAgICAgICAgLy8gcmVtYWluaW5nIGluIHRoZSBET01cbiAgICAgICAgaWYgKCFib2R5Q29udGFpbnMoZWx0KSkge1xuICAgICAgICAgIGxldCBzZWNvbmRhcnlUcmlnZ2VyRWx0ID0gbnVsbFxuICAgICAgICAgIHdoaWxlIChoaWVyYXJjaHkubGVuZ3RoID4gMCAmJiBzZWNvbmRhcnlUcmlnZ2VyRWx0ID09IG51bGwpIHtcbiAgICAgICAgICAgIGNvbnN0IHBhcmVudEVsdEluSGllcmFyY2h5ID0gaGllcmFyY2h5LnNoaWZ0KClcbiAgICAgICAgICAgIGlmIChib2R5Q29udGFpbnMocGFyZW50RWx0SW5IaWVyYXJjaHkpKSB7XG4gICAgICAgICAgICAgIHNlY29uZGFyeVRyaWdnZXJFbHQgPSBwYXJlbnRFbHRJbkhpZXJhcmNoeVxuICAgICAgICAgICAgfVxuICAgICAgICAgIH1cbiAgICAgICAgICBpZiAoc2Vjb25kYXJ5VHJpZ2dlckVsdCkge1xuICAgICAgICAgICAgdHJpZ2dlckV2ZW50KHNlY29uZGFyeVRyaWdnZXJFbHQsICdodG14OmFmdGVyUmVxdWVzdCcsIHJlc3BvbnNlSW5mbylcbiAgICAgICAgICAgIHRyaWdnZXJFdmVudChzZWNvbmRhcnlUcmlnZ2VyRWx0LCAnaHRteDphZnRlck9uTG9hZCcsIHJlc3BvbnNlSW5mbylcbiAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgICAgbWF5YmVDYWxsKHJlc29sdmUpXG4gICAgICAgIGVuZFJlcXVlc3RMb2NrKClcbiAgICAgIH0gY2F0Y2ggKGUpIHtcbiAgICAgICAgdHJpZ2dlckVycm9yRXZlbnQoZWx0LCAnaHRteDpvbkxvYWRFcnJvcicsIG1lcmdlT2JqZWN0cyh7IGVycm9yOiBlIH0sIHJlc3BvbnNlSW5mbykpXG4gICAgICAgIHRocm93IGVcbiAgICAgIH1cbiAgICB9XG4gICAgeGhyLm9uZXJyb3IgPSBmdW5jdGlvbigpIHtcbiAgICAgIHJlbW92ZVJlcXVlc3RJbmRpY2F0b3JzKGluZGljYXRvcnMsIGRpc2FibGVFbHRzKVxuICAgICAgdHJpZ2dlckVycm9yRXZlbnQoZWx0LCAnaHRteDphZnRlclJlcXVlc3QnLCByZXNwb25zZUluZm8pXG4gICAgICB0cmlnZ2VyRXJyb3JFdmVudChlbHQsICdodG14OnNlbmRFcnJvcicsIHJlc3BvbnNlSW5mbylcbiAgICAgIG1heWJlQ2FsbChyZWplY3QpXG4gICAgICBlbmRSZXF1ZXN0TG9jaygpXG4gICAgfVxuICAgIHhoci5vbmFib3J0ID0gZnVuY3Rpb24oKSB7XG4gICAgICByZW1vdmVSZXF1ZXN0SW5kaWNhdG9ycyhpbmRpY2F0b3JzLCBkaXNhYmxlRWx0cylcbiAgICAgIHRyaWdnZXJFcnJvckV2ZW50KGVsdCwgJ2h0bXg6YWZ0ZXJSZXF1ZXN0JywgcmVzcG9uc2VJbmZvKVxuICAgICAgdHJpZ2dlckVycm9yRXZlbnQoZWx0LCAnaHRteDpzZW5kQWJvcnQnLCByZXNwb25zZUluZm8pXG4gICAgICBtYXliZUNhbGwocmVqZWN0KVxuICAgICAgZW5kUmVxdWVzdExvY2soKVxuICAgIH1cbiAgICB4aHIub250aW1lb3V0ID0gZnVuY3Rpb24oKSB7XG4gICAgICByZW1vdmVSZXF1ZXN0SW5kaWNhdG9ycyhpbmRpY2F0b3JzLCBkaXNhYmxlRWx0cylcbiAgICAgIHRyaWdnZXJFcnJvckV2ZW50KGVsdCwgJ2h0bXg6YWZ0ZXJSZXF1ZXN0JywgcmVzcG9uc2VJbmZvKVxuICAgICAgdHJpZ2dlckVycm9yRXZlbnQoZWx0LCAnaHRteDp0aW1lb3V0JywgcmVzcG9uc2VJbmZvKVxuICAgICAgbWF5YmVDYWxsKHJlamVjdClcbiAgICAgIGVuZFJlcXVlc3RMb2NrKClcbiAgICB9XG4gICAgaWYgKCF0cmlnZ2VyRXZlbnQoZWx0LCAnaHRteDpiZWZvcmVSZXF1ZXN0JywgcmVzcG9uc2VJbmZvKSkge1xuICAgICAgbWF5YmVDYWxsKHJlc29sdmUpXG4gICAgICBlbmRSZXF1ZXN0TG9jaygpXG4gICAgICByZXR1cm4gcHJvbWlzZVxuICAgIH1cbiAgICB2YXIgaW5kaWNhdG9ycyA9IGFkZFJlcXVlc3RJbmRpY2F0b3JDbGFzc2VzKGVsdClcbiAgICB2YXIgZGlzYWJsZUVsdHMgPSBkaXNhYmxlRWxlbWVudHMoZWx0KVxuXG4gICAgZm9yRWFjaChbJ2xvYWRzdGFydCcsICdsb2FkZW5kJywgJ3Byb2dyZXNzJywgJ2Fib3J0J10sIGZ1bmN0aW9uKGV2ZW50TmFtZSkge1xuICAgICAgZm9yRWFjaChbeGhyLCB4aHIudXBsb2FkXSwgZnVuY3Rpb24odGFyZ2V0KSB7XG4gICAgICAgIHRhcmdldC5hZGRFdmVudExpc3RlbmVyKGV2ZW50TmFtZSwgZnVuY3Rpb24oZXZlbnQpIHtcbiAgICAgICAgICB0cmlnZ2VyRXZlbnQoZWx0LCAnaHRteDp4aHI6JyArIGV2ZW50TmFtZSwge1xuICAgICAgICAgICAgbGVuZ3RoQ29tcHV0YWJsZTogZXZlbnQubGVuZ3RoQ29tcHV0YWJsZSxcbiAgICAgICAgICAgIGxvYWRlZDogZXZlbnQubG9hZGVkLFxuICAgICAgICAgICAgdG90YWw6IGV2ZW50LnRvdGFsXG4gICAgICAgICAgfSlcbiAgICAgICAgfSlcbiAgICAgIH0pXG4gICAgfSlcbiAgICB0cmlnZ2VyRXZlbnQoZWx0LCAnaHRteDpiZWZvcmVTZW5kJywgcmVzcG9uc2VJbmZvKVxuICAgIGNvbnN0IHBhcmFtcyA9IHVzZVVybFBhcmFtcyA/IG51bGwgOiBlbmNvZGVQYXJhbXNGb3JCb2R5KHhociwgZWx0LCBmaWx0ZXJlZEZvcm1EYXRhKVxuICAgIHhoci5zZW5kKHBhcmFtcylcbiAgICByZXR1cm4gcHJvbWlzZVxuICB9XG5cbiAgLyoqXG4gICAqIEB0eXBlZGVmIHtPYmplY3R9IEh0bXhIaXN0b3J5VXBkYXRlXG4gICAqIEBwcm9wZXJ0eSB7c3RyaW5nfG51bGx9IFt0eXBlXVxuICAgKiBAcHJvcGVydHkge3N0cmluZ3xudWxsfSBbcGF0aF1cbiAgICovXG5cbiAgLyoqXG4gICAqIEBwYXJhbSB7RWxlbWVudH0gZWx0XG4gICAqIEBwYXJhbSB7SHRteFJlc3BvbnNlSW5mb30gcmVzcG9uc2VJbmZvXG4gICAqIEByZXR1cm4ge0h0bXhIaXN0b3J5VXBkYXRlfVxuICAgKi9cbiAgZnVuY3Rpb24gZGV0ZXJtaW5lSGlzdG9yeVVwZGF0ZXMoZWx0LCByZXNwb25zZUluZm8pIHtcbiAgICBjb25zdCB4aHIgPSByZXNwb25zZUluZm8ueGhyXG5cbiAgICAvLz0gPT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09XG4gICAgLy8gRmlyc3QgY29uc3VsdCByZXNwb25zZSBoZWFkZXJzXG4gICAgLy89ID09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PVxuICAgIGxldCBwYXRoRnJvbUhlYWRlcnMgPSBudWxsXG4gICAgbGV0IHR5cGVGcm9tSGVhZGVycyA9IG51bGxcbiAgICBpZiAoaGFzSGVhZGVyKHhociwgL0hYLVB1c2g6L2kpKSB7XG4gICAgICBwYXRoRnJvbUhlYWRlcnMgPSB4aHIuZ2V0UmVzcG9uc2VIZWFkZXIoJ0hYLVB1c2gnKVxuICAgICAgdHlwZUZyb21IZWFkZXJzID0gJ3B1c2gnXG4gICAgfSBlbHNlIGlmIChoYXNIZWFkZXIoeGhyLCAvSFgtUHVzaC1Vcmw6L2kpKSB7XG4gICAgICBwYXRoRnJvbUhlYWRlcnMgPSB4aHIuZ2V0UmVzcG9uc2VIZWFkZXIoJ0hYLVB1c2gtVXJsJylcbiAgICAgIHR5cGVGcm9tSGVhZGVycyA9ICdwdXNoJ1xuICAgIH0gZWxzZSBpZiAoaGFzSGVhZGVyKHhociwgL0hYLVJlcGxhY2UtVXJsOi9pKSkge1xuICAgICAgcGF0aEZyb21IZWFkZXJzID0geGhyLmdldFJlc3BvbnNlSGVhZGVyKCdIWC1SZXBsYWNlLVVybCcpXG4gICAgICB0eXBlRnJvbUhlYWRlcnMgPSAncmVwbGFjZSdcbiAgICB9XG5cbiAgICAvLyBpZiB0aGVyZSB3YXMgYSByZXNwb25zZSBoZWFkZXIsIHRoYXQgaGFzIHByaW9yaXR5XG4gICAgaWYgKHBhdGhGcm9tSGVhZGVycykge1xuICAgICAgaWYgKHBhdGhGcm9tSGVhZGVycyA9PT0gJ2ZhbHNlJykge1xuICAgICAgICByZXR1cm4ge31cbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIHJldHVybiB7XG4gICAgICAgICAgdHlwZTogdHlwZUZyb21IZWFkZXJzLFxuICAgICAgICAgIHBhdGg6IHBhdGhGcm9tSGVhZGVyc1xuICAgICAgICB9XG4gICAgICB9XG4gICAgfVxuXG4gICAgLy89ID09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PVxuICAgIC8vIE5leHQgcmVzb2x2ZSB2aWEgRE9NIHZhbHVlc1xuICAgIC8vPSA9PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT1cbiAgICBjb25zdCByZXF1ZXN0UGF0aCA9IHJlc3BvbnNlSW5mby5wYXRoSW5mby5maW5hbFJlcXVlc3RQYXRoXG4gICAgY29uc3QgcmVzcG9uc2VQYXRoID0gcmVzcG9uc2VJbmZvLnBhdGhJbmZvLnJlc3BvbnNlUGF0aFxuXG4gICAgY29uc3QgcHVzaFVybCA9IGdldENsb3Nlc3RBdHRyaWJ1dGVWYWx1ZShlbHQsICdoeC1wdXNoLXVybCcpXG4gICAgY29uc3QgcmVwbGFjZVVybCA9IGdldENsb3Nlc3RBdHRyaWJ1dGVWYWx1ZShlbHQsICdoeC1yZXBsYWNlLXVybCcpXG4gICAgY29uc3QgZWxlbWVudElzQm9vc3RlZCA9IGdldEludGVybmFsRGF0YShlbHQpLmJvb3N0ZWRcblxuICAgIGxldCBzYXZlVHlwZSA9IG51bGxcbiAgICBsZXQgcGF0aCA9IG51bGxcblxuICAgIGlmIChwdXNoVXJsKSB7XG4gICAgICBzYXZlVHlwZSA9ICdwdXNoJ1xuICAgICAgcGF0aCA9IHB1c2hVcmxcbiAgICB9IGVsc2UgaWYgKHJlcGxhY2VVcmwpIHtcbiAgICAgIHNhdmVUeXBlID0gJ3JlcGxhY2UnXG4gICAgICBwYXRoID0gcmVwbGFjZVVybFxuICAgIH0gZWxzZSBpZiAoZWxlbWVudElzQm9vc3RlZCkge1xuICAgICAgc2F2ZVR5cGUgPSAncHVzaCdcbiAgICAgIHBhdGggPSByZXNwb25zZVBhdGggfHwgcmVxdWVzdFBhdGggLy8gaWYgdGhlcmUgaXMgbm8gcmVzcG9uc2UgcGF0aCwgZ28gd2l0aCB0aGUgb3JpZ2luYWwgcmVxdWVzdCBwYXRoXG4gICAgfVxuXG4gICAgaWYgKHBhdGgpIHtcbiAgICAvLyBmYWxzZSBpbmRpY2F0ZXMgbm8gcHVzaCwgcmV0dXJuIGVtcHR5IG9iamVjdFxuICAgICAgaWYgKHBhdGggPT09ICdmYWxzZScpIHtcbiAgICAgICAgcmV0dXJuIHt9XG4gICAgICB9XG5cbiAgICAgIC8vIHRydWUgaW5kaWNhdGVzIHdlIHdhbnQgdG8gZm9sbG93IHdoZXJldmVyIHRoZSBzZXJ2ZXIgZW5kZWQgdXAgc2VuZGluZyB1c1xuICAgICAgaWYgKHBhdGggPT09ICd0cnVlJykge1xuICAgICAgICBwYXRoID0gcmVzcG9uc2VQYXRoIHx8IHJlcXVlc3RQYXRoIC8vIGlmIHRoZXJlIGlzIG5vIHJlc3BvbnNlIHBhdGgsIGdvIHdpdGggdGhlIG9yaWdpbmFsIHJlcXVlc3QgcGF0aFxuICAgICAgfVxuXG4gICAgICAvLyByZXN0b3JlIGFueSBhbmNob3IgYXNzb2NpYXRlZCB3aXRoIHRoZSByZXF1ZXN0XG4gICAgICBpZiAocmVzcG9uc2VJbmZvLnBhdGhJbmZvLmFuY2hvciAmJiBwYXRoLmluZGV4T2YoJyMnKSA9PT0gLTEpIHtcbiAgICAgICAgcGF0aCA9IHBhdGggKyAnIycgKyByZXNwb25zZUluZm8ucGF0aEluZm8uYW5jaG9yXG4gICAgICB9XG5cbiAgICAgIHJldHVybiB7XG4gICAgICAgIHR5cGU6IHNhdmVUeXBlLFxuICAgICAgICBwYXRoXG4gICAgICB9XG4gICAgfSBlbHNlIHtcbiAgICAgIHJldHVybiB7fVxuICAgIH1cbiAgfVxuXG4gIC8qKlxuICAgKiBAcGFyYW0ge0h0bXhSZXNwb25zZUhhbmRsaW5nQ29uZmlnfSByZXNwb25zZUhhbmRsaW5nQ29uZmlnXG4gICAqIEBwYXJhbSB7bnVtYmVyfSBzdGF0dXNcbiAgICogQHJldHVybiB7Ym9vbGVhbn1cbiAgICovXG4gIGZ1bmN0aW9uIGNvZGVNYXRjaGVzKHJlc3BvbnNlSGFuZGxpbmdDb25maWcsIHN0YXR1cykge1xuICAgIHZhciByZWdFeHAgPSBuZXcgUmVnRXhwKHJlc3BvbnNlSGFuZGxpbmdDb25maWcuY29kZSlcbiAgICByZXR1cm4gcmVnRXhwLnRlc3Qoc3RhdHVzLnRvU3RyaW5nKDEwKSlcbiAgfVxuXG4gIC8qKlxuICAgKiBAcGFyYW0ge1hNTEh0dHBSZXF1ZXN0fSB4aHJcbiAgICogQHJldHVybiB7SHRteFJlc3BvbnNlSGFuZGxpbmdDb25maWd9XG4gICAqL1xuICBmdW5jdGlvbiByZXNvbHZlUmVzcG9uc2VIYW5kbGluZyh4aHIpIHtcbiAgICBmb3IgKHZhciBpID0gMDsgaSA8IGh0bXguY29uZmlnLnJlc3BvbnNlSGFuZGxpbmcubGVuZ3RoOyBpKyspIHtcbiAgICAgIC8qKiBAdHlwZSBIdG14UmVzcG9uc2VIYW5kbGluZ0NvbmZpZyAqL1xuICAgICAgdmFyIHJlc3BvbnNlSGFuZGxpbmdFbGVtZW50ID0gaHRteC5jb25maWcucmVzcG9uc2VIYW5kbGluZ1tpXVxuICAgICAgaWYgKGNvZGVNYXRjaGVzKHJlc3BvbnNlSGFuZGxpbmdFbGVtZW50LCB4aHIuc3RhdHVzKSkge1xuICAgICAgICByZXR1cm4gcmVzcG9uc2VIYW5kbGluZ0VsZW1lbnRcbiAgICAgIH1cbiAgICB9XG4gICAgLy8gbm8gbWF0Y2hlcywgcmV0dXJuIG5vIHN3YXBcbiAgICByZXR1cm4ge1xuICAgICAgc3dhcDogZmFsc2VcbiAgICB9XG4gIH1cblxuICAvKipcbiAgICogQHBhcmFtIHtzdHJpbmd9IHRpdGxlXG4gICAqL1xuICBmdW5jdGlvbiBoYW5kbGVUaXRsZSh0aXRsZSkge1xuICAgIGlmICh0aXRsZSkge1xuICAgICAgY29uc3QgdGl0bGVFbHQgPSBmaW5kKCd0aXRsZScpXG4gICAgICBpZiAodGl0bGVFbHQpIHtcbiAgICAgICAgdGl0bGVFbHQuaW5uZXJIVE1MID0gdGl0bGVcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIHdpbmRvdy5kb2N1bWVudC50aXRsZSA9IHRpdGxlXG4gICAgICB9XG4gICAgfVxuICB9XG5cbiAgLyoqXG4gICAqIEBwYXJhbSB7RWxlbWVudH0gZWx0XG4gICAqIEBwYXJhbSB7SHRteFJlc3BvbnNlSW5mb30gcmVzcG9uc2VJbmZvXG4gICAqL1xuICBmdW5jdGlvbiBoYW5kbGVBamF4UmVzcG9uc2UoZWx0LCByZXNwb25zZUluZm8pIHtcbiAgICBjb25zdCB4aHIgPSByZXNwb25zZUluZm8ueGhyXG4gICAgbGV0IHRhcmdldCA9IHJlc3BvbnNlSW5mby50YXJnZXRcbiAgICBjb25zdCBldGMgPSByZXNwb25zZUluZm8uZXRjXG4gICAgY29uc3QgcmVzcG9uc2VJbmZvU2VsZWN0ID0gcmVzcG9uc2VJbmZvLnNlbGVjdFxuXG4gICAgaWYgKCF0cmlnZ2VyRXZlbnQoZWx0LCAnaHRteDpiZWZvcmVPbkxvYWQnLCByZXNwb25zZUluZm8pKSByZXR1cm5cblxuICAgIGlmIChoYXNIZWFkZXIoeGhyLCAvSFgtVHJpZ2dlcjovaSkpIHtcbiAgICAgIGhhbmRsZVRyaWdnZXJIZWFkZXIoeGhyLCAnSFgtVHJpZ2dlcicsIGVsdClcbiAgICB9XG5cbiAgICBpZiAoaGFzSGVhZGVyKHhociwgL0hYLUxvY2F0aW9uOi9pKSkge1xuICAgICAgc2F2ZUN1cnJlbnRQYWdlVG9IaXN0b3J5KClcbiAgICAgIGxldCByZWRpcmVjdFBhdGggPSB4aHIuZ2V0UmVzcG9uc2VIZWFkZXIoJ0hYLUxvY2F0aW9uJylcbiAgICAgIC8qKiBAdHlwZSB7SHRteEFqYXhIZWxwZXJDb250ZXh0JntwYXRoOnN0cmluZ319ICovXG4gICAgICB2YXIgcmVkaXJlY3RTd2FwU3BlY1xuICAgICAgaWYgKHJlZGlyZWN0UGF0aC5pbmRleE9mKCd7JykgPT09IDApIHtcbiAgICAgICAgcmVkaXJlY3RTd2FwU3BlYyA9IHBhcnNlSlNPTihyZWRpcmVjdFBhdGgpXG4gICAgICAgIC8vIHdoYXQncyB0aGUgYmVzdCB3YXkgdG8gdGhyb3cgYW4gZXJyb3IgaWYgdGhlIHVzZXIgZGlkbid0IGluY2x1ZGUgdGhpc1xuICAgICAgICByZWRpcmVjdFBhdGggPSByZWRpcmVjdFN3YXBTcGVjLnBhdGhcbiAgICAgICAgZGVsZXRlIHJlZGlyZWN0U3dhcFNwZWMucGF0aFxuICAgICAgfVxuICAgICAgYWpheEhlbHBlcignZ2V0JywgcmVkaXJlY3RQYXRoLCByZWRpcmVjdFN3YXBTcGVjKS50aGVuKGZ1bmN0aW9uKCkge1xuICAgICAgICBwdXNoVXJsSW50b0hpc3RvcnkocmVkaXJlY3RQYXRoKVxuICAgICAgfSlcbiAgICAgIHJldHVyblxuICAgIH1cblxuICAgIGNvbnN0IHNob3VsZFJlZnJlc2ggPSBoYXNIZWFkZXIoeGhyLCAvSFgtUmVmcmVzaDovaSkgJiYgeGhyLmdldFJlc3BvbnNlSGVhZGVyKCdIWC1SZWZyZXNoJykgPT09ICd0cnVlJ1xuXG4gICAgaWYgKGhhc0hlYWRlcih4aHIsIC9IWC1SZWRpcmVjdDovaSkpIHtcbiAgICAgIHJlc3BvbnNlSW5mby5rZWVwSW5kaWNhdG9ycyA9IHRydWVcbiAgICAgIGxvY2F0aW9uLmhyZWYgPSB4aHIuZ2V0UmVzcG9uc2VIZWFkZXIoJ0hYLVJlZGlyZWN0JylcbiAgICAgIHNob3VsZFJlZnJlc2ggJiYgbG9jYXRpb24ucmVsb2FkKClcbiAgICAgIHJldHVyblxuICAgIH1cblxuICAgIGlmIChzaG91bGRSZWZyZXNoKSB7XG4gICAgICByZXNwb25zZUluZm8ua2VlcEluZGljYXRvcnMgPSB0cnVlXG4gICAgICBsb2NhdGlvbi5yZWxvYWQoKVxuICAgICAgcmV0dXJuXG4gICAgfVxuXG4gICAgaWYgKGhhc0hlYWRlcih4aHIsIC9IWC1SZXRhcmdldDovaSkpIHtcbiAgICAgIGlmICh4aHIuZ2V0UmVzcG9uc2VIZWFkZXIoJ0hYLVJldGFyZ2V0JykgPT09ICd0aGlzJykge1xuICAgICAgICByZXNwb25zZUluZm8udGFyZ2V0ID0gZWx0XG4gICAgICB9IGVsc2Uge1xuICAgICAgICByZXNwb25zZUluZm8udGFyZ2V0ID0gYXNFbGVtZW50KHF1ZXJ5U2VsZWN0b3JFeHQoZWx0LCB4aHIuZ2V0UmVzcG9uc2VIZWFkZXIoJ0hYLVJldGFyZ2V0JykpKVxuICAgICAgfVxuICAgIH1cblxuICAgIGNvbnN0IGhpc3RvcnlVcGRhdGUgPSBkZXRlcm1pbmVIaXN0b3J5VXBkYXRlcyhlbHQsIHJlc3BvbnNlSW5mbylcblxuICAgIGNvbnN0IHJlc3BvbnNlSGFuZGxpbmcgPSByZXNvbHZlUmVzcG9uc2VIYW5kbGluZyh4aHIpXG4gICAgY29uc3Qgc2hvdWxkU3dhcCA9IHJlc3BvbnNlSGFuZGxpbmcuc3dhcFxuICAgIGxldCBpc0Vycm9yID0gISFyZXNwb25zZUhhbmRsaW5nLmVycm9yXG4gICAgbGV0IGlnbm9yZVRpdGxlID0gaHRteC5jb25maWcuaWdub3JlVGl0bGUgfHwgcmVzcG9uc2VIYW5kbGluZy5pZ25vcmVUaXRsZVxuICAgIGxldCBzZWxlY3RPdmVycmlkZSA9IHJlc3BvbnNlSGFuZGxpbmcuc2VsZWN0XG4gICAgaWYgKHJlc3BvbnNlSGFuZGxpbmcudGFyZ2V0KSB7XG4gICAgICByZXNwb25zZUluZm8udGFyZ2V0ID0gYXNFbGVtZW50KHF1ZXJ5U2VsZWN0b3JFeHQoZWx0LCByZXNwb25zZUhhbmRsaW5nLnRhcmdldCkpXG4gICAgfVxuICAgIHZhciBzd2FwT3ZlcnJpZGUgPSBldGMuc3dhcE92ZXJyaWRlXG4gICAgaWYgKHN3YXBPdmVycmlkZSA9PSBudWxsICYmIHJlc3BvbnNlSGFuZGxpbmcuc3dhcE92ZXJyaWRlKSB7XG4gICAgICBzd2FwT3ZlcnJpZGUgPSByZXNwb25zZUhhbmRsaW5nLnN3YXBPdmVycmlkZVxuICAgIH1cblxuICAgIC8vIHJlc3BvbnNlIGhlYWRlcnMgb3ZlcnJpZGUgcmVzcG9uc2UgaGFuZGxpbmcgY29uZmlnXG4gICAgaWYgKGhhc0hlYWRlcih4aHIsIC9IWC1SZXRhcmdldDovaSkpIHtcbiAgICAgIGlmICh4aHIuZ2V0UmVzcG9uc2VIZWFkZXIoJ0hYLVJldGFyZ2V0JykgPT09ICd0aGlzJykge1xuICAgICAgICByZXNwb25zZUluZm8udGFyZ2V0ID0gZWx0XG4gICAgICB9IGVsc2Uge1xuICAgICAgICByZXNwb25zZUluZm8udGFyZ2V0ID0gYXNFbGVtZW50KHF1ZXJ5U2VsZWN0b3JFeHQoZWx0LCB4aHIuZ2V0UmVzcG9uc2VIZWFkZXIoJ0hYLVJldGFyZ2V0JykpKVxuICAgICAgfVxuICAgIH1cbiAgICBpZiAoaGFzSGVhZGVyKHhociwgL0hYLVJlc3dhcDovaSkpIHtcbiAgICAgIHN3YXBPdmVycmlkZSA9IHhoci5nZXRSZXNwb25zZUhlYWRlcignSFgtUmVzd2FwJylcbiAgICB9XG5cbiAgICB2YXIgc2VydmVyUmVzcG9uc2UgPSB4aHIucmVzcG9uc2VcbiAgICAvKiogQHR5cGUgSHRteEJlZm9yZVN3YXBEZXRhaWxzICovXG4gICAgdmFyIGJlZm9yZVN3YXBEZXRhaWxzID0gbWVyZ2VPYmplY3RzKHtcbiAgICAgIHNob3VsZFN3YXAsXG4gICAgICBzZXJ2ZXJSZXNwb25zZSxcbiAgICAgIGlzRXJyb3IsXG4gICAgICBpZ25vcmVUaXRsZSxcbiAgICAgIHNlbGVjdE92ZXJyaWRlLFxuICAgICAgc3dhcE92ZXJyaWRlXG4gICAgfSwgcmVzcG9uc2VJbmZvKVxuXG4gICAgaWYgKHJlc3BvbnNlSGFuZGxpbmcuZXZlbnQgJiYgIXRyaWdnZXJFdmVudCh0YXJnZXQsIHJlc3BvbnNlSGFuZGxpbmcuZXZlbnQsIGJlZm9yZVN3YXBEZXRhaWxzKSkgcmV0dXJuXG5cbiAgICBpZiAoIXRyaWdnZXJFdmVudCh0YXJnZXQsICdodG14OmJlZm9yZVN3YXAnLCBiZWZvcmVTd2FwRGV0YWlscykpIHJldHVyblxuXG4gICAgdGFyZ2V0ID0gYmVmb3JlU3dhcERldGFpbHMudGFyZ2V0IC8vIGFsbG93IHJlLXRhcmdldGluZ1xuICAgIHNlcnZlclJlc3BvbnNlID0gYmVmb3JlU3dhcERldGFpbHMuc2VydmVyUmVzcG9uc2UgLy8gYWxsb3cgdXBkYXRpbmcgY29udGVudFxuICAgIGlzRXJyb3IgPSBiZWZvcmVTd2FwRGV0YWlscy5pc0Vycm9yIC8vIGFsbG93IHVwZGF0aW5nIGVycm9yXG4gICAgaWdub3JlVGl0bGUgPSBiZWZvcmVTd2FwRGV0YWlscy5pZ25vcmVUaXRsZSAvLyBhbGxvdyB1cGRhdGluZyBpZ25vcmluZyB0aXRsZVxuICAgIHNlbGVjdE92ZXJyaWRlID0gYmVmb3JlU3dhcERldGFpbHMuc2VsZWN0T3ZlcnJpZGUgLy8gYWxsb3cgdXBkYXRpbmcgc2VsZWN0IG92ZXJyaWRlXG4gICAgc3dhcE92ZXJyaWRlID0gYmVmb3JlU3dhcERldGFpbHMuc3dhcE92ZXJyaWRlIC8vIGFsbG93IHVwZGF0aW5nIHN3YXAgb3ZlcnJpZGVcblxuICAgIHJlc3BvbnNlSW5mby50YXJnZXQgPSB0YXJnZXQgLy8gTWFrZSB1cGRhdGVkIHRhcmdldCBhdmFpbGFibGUgdG8gcmVzcG9uc2UgZXZlbnRzXG4gICAgcmVzcG9uc2VJbmZvLmZhaWxlZCA9IGlzRXJyb3IgLy8gTWFrZSBmYWlsZWQgcHJvcGVydHkgYXZhaWxhYmxlIHRvIHJlc3BvbnNlIGV2ZW50c1xuICAgIHJlc3BvbnNlSW5mby5zdWNjZXNzZnVsID0gIWlzRXJyb3IgLy8gTWFrZSBzdWNjZXNzZnVsIHByb3BlcnR5IGF2YWlsYWJsZSB0byByZXNwb25zZSBldmVudHNcblxuICAgIGlmIChiZWZvcmVTd2FwRGV0YWlscy5zaG91bGRTd2FwKSB7XG4gICAgICBpZiAoeGhyLnN0YXR1cyA9PT0gMjg2KSB7XG4gICAgICAgIGNhbmNlbFBvbGxpbmcoZWx0KVxuICAgICAgfVxuXG4gICAgICB3aXRoRXh0ZW5zaW9ucyhlbHQsIGZ1bmN0aW9uKGV4dGVuc2lvbikge1xuICAgICAgICBzZXJ2ZXJSZXNwb25zZSA9IGV4dGVuc2lvbi50cmFuc2Zvcm1SZXNwb25zZShzZXJ2ZXJSZXNwb25zZSwgeGhyLCBlbHQpXG4gICAgICB9KVxuXG4gICAgICAvLyBTYXZlIGN1cnJlbnQgcGFnZSBpZiB0aGVyZSB3aWxsIGJlIGEgaGlzdG9yeSB1cGRhdGVcbiAgICAgIGlmIChoaXN0b3J5VXBkYXRlLnR5cGUpIHtcbiAgICAgICAgc2F2ZUN1cnJlbnRQYWdlVG9IaXN0b3J5KClcbiAgICAgIH1cblxuICAgICAgdmFyIHN3YXBTcGVjID0gZ2V0U3dhcFNwZWNpZmljYXRpb24oZWx0LCBzd2FwT3ZlcnJpZGUpXG5cbiAgICAgIGlmICghc3dhcFNwZWMuaGFzT3duUHJvcGVydHkoJ2lnbm9yZVRpdGxlJykpIHtcbiAgICAgICAgc3dhcFNwZWMuaWdub3JlVGl0bGUgPSBpZ25vcmVUaXRsZVxuICAgICAgfVxuXG4gICAgICB0YXJnZXQuY2xhc3NMaXN0LmFkZChodG14LmNvbmZpZy5zd2FwcGluZ0NsYXNzKVxuXG4gICAgICAvLyBvcHRpb25hbCB0cmFuc2l0aW9uIEFQSSBwcm9taXNlIGNhbGxiYWNrc1xuICAgICAgbGV0IHNldHRsZVJlc29sdmUgPSBudWxsXG4gICAgICBsZXQgc2V0dGxlUmVqZWN0ID0gbnVsbFxuXG4gICAgICBpZiAocmVzcG9uc2VJbmZvU2VsZWN0KSB7XG4gICAgICAgIHNlbGVjdE92ZXJyaWRlID0gcmVzcG9uc2VJbmZvU2VsZWN0XG4gICAgICB9XG5cbiAgICAgIGlmIChoYXNIZWFkZXIoeGhyLCAvSFgtUmVzZWxlY3Q6L2kpKSB7XG4gICAgICAgIHNlbGVjdE92ZXJyaWRlID0geGhyLmdldFJlc3BvbnNlSGVhZGVyKCdIWC1SZXNlbGVjdCcpXG4gICAgICB9XG5cbiAgICAgIGNvbnN0IHNlbGVjdE9PQiA9IGdldENsb3Nlc3RBdHRyaWJ1dGVWYWx1ZShlbHQsICdoeC1zZWxlY3Qtb29iJylcbiAgICAgIGNvbnN0IHNlbGVjdCA9IGdldENsb3Nlc3RBdHRyaWJ1dGVWYWx1ZShlbHQsICdoeC1zZWxlY3QnKVxuXG4gICAgICBsZXQgZG9Td2FwID0gZnVuY3Rpb24oKSB7XG4gICAgICAgIHRyeSB7XG4gICAgICAgICAgLy8gaWYgd2UgbmVlZCB0byBzYXZlIGhpc3RvcnksIGRvIHNvLCBiZWZvcmUgc3dhcHBpbmcgc28gdGhhdCByZWxhdGl2ZSByZXNvdXJjZXMgaGF2ZSB0aGUgY29ycmVjdCBiYXNlIFVSTFxuICAgICAgICAgIGlmIChoaXN0b3J5VXBkYXRlLnR5cGUpIHtcbiAgICAgICAgICAgIHRyaWdnZXJFdmVudChnZXREb2N1bWVudCgpLmJvZHksICdodG14OmJlZm9yZUhpc3RvcnlVcGRhdGUnLCBtZXJnZU9iamVjdHMoeyBoaXN0b3J5OiBoaXN0b3J5VXBkYXRlIH0sIHJlc3BvbnNlSW5mbykpXG4gICAgICAgICAgICBpZiAoaGlzdG9yeVVwZGF0ZS50eXBlID09PSAncHVzaCcpIHtcbiAgICAgICAgICAgICAgcHVzaFVybEludG9IaXN0b3J5KGhpc3RvcnlVcGRhdGUucGF0aClcbiAgICAgICAgICAgICAgdHJpZ2dlckV2ZW50KGdldERvY3VtZW50KCkuYm9keSwgJ2h0bXg6cHVzaGVkSW50b0hpc3RvcnknLCB7IHBhdGg6IGhpc3RvcnlVcGRhdGUucGF0aCB9KVxuICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgcmVwbGFjZVVybEluSGlzdG9yeShoaXN0b3J5VXBkYXRlLnBhdGgpXG4gICAgICAgICAgICAgIHRyaWdnZXJFdmVudChnZXREb2N1bWVudCgpLmJvZHksICdodG14OnJlcGxhY2VkSW5IaXN0b3J5JywgeyBwYXRoOiBoaXN0b3J5VXBkYXRlLnBhdGggfSlcbiAgICAgICAgICAgIH1cbiAgICAgICAgICB9XG5cbiAgICAgICAgICBzd2FwKHRhcmdldCwgc2VydmVyUmVzcG9uc2UsIHN3YXBTcGVjLCB7XG4gICAgICAgICAgICBzZWxlY3Q6IHNlbGVjdE92ZXJyaWRlIHx8IHNlbGVjdCxcbiAgICAgICAgICAgIHNlbGVjdE9PQixcbiAgICAgICAgICAgIGV2ZW50SW5mbzogcmVzcG9uc2VJbmZvLFxuICAgICAgICAgICAgYW5jaG9yOiByZXNwb25zZUluZm8ucGF0aEluZm8uYW5jaG9yLFxuICAgICAgICAgICAgY29udGV4dEVsZW1lbnQ6IGVsdCxcbiAgICAgICAgICAgIGFmdGVyU3dhcENhbGxiYWNrOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgICAgaWYgKGhhc0hlYWRlcih4aHIsIC9IWC1UcmlnZ2VyLUFmdGVyLVN3YXA6L2kpKSB7XG4gICAgICAgICAgICAgICAgbGV0IGZpbmFsRWx0ID0gZWx0XG4gICAgICAgICAgICAgICAgaWYgKCFib2R5Q29udGFpbnMoZWx0KSkge1xuICAgICAgICAgICAgICAgICAgZmluYWxFbHQgPSBnZXREb2N1bWVudCgpLmJvZHlcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgaGFuZGxlVHJpZ2dlckhlYWRlcih4aHIsICdIWC1UcmlnZ2VyLUFmdGVyLVN3YXAnLCBmaW5hbEVsdClcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIGFmdGVyU2V0dGxlQ2FsbGJhY2s6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgICBpZiAoaGFzSGVhZGVyKHhociwgL0hYLVRyaWdnZXItQWZ0ZXItU2V0dGxlOi9pKSkge1xuICAgICAgICAgICAgICAgIGxldCBmaW5hbEVsdCA9IGVsdFxuICAgICAgICAgICAgICAgIGlmICghYm9keUNvbnRhaW5zKGVsdCkpIHtcbiAgICAgICAgICAgICAgICAgIGZpbmFsRWx0ID0gZ2V0RG9jdW1lbnQoKS5ib2R5XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIGhhbmRsZVRyaWdnZXJIZWFkZXIoeGhyLCAnSFgtVHJpZ2dlci1BZnRlci1TZXR0bGUnLCBmaW5hbEVsdClcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICBtYXliZUNhbGwoc2V0dGxlUmVzb2x2ZSlcbiAgICAgICAgICAgIH1cbiAgICAgICAgICB9KVxuICAgICAgICB9IGNhdGNoIChlKSB7XG4gICAgICAgICAgdHJpZ2dlckVycm9yRXZlbnQoZWx0LCAnaHRteDpzd2FwRXJyb3InLCByZXNwb25zZUluZm8pXG4gICAgICAgICAgbWF5YmVDYWxsKHNldHRsZVJlamVjdClcbiAgICAgICAgICB0aHJvdyBlXG4gICAgICAgIH1cbiAgICAgIH1cblxuICAgICAgbGV0IHNob3VsZFRyYW5zaXRpb24gPSBodG14LmNvbmZpZy5nbG9iYWxWaWV3VHJhbnNpdGlvbnNcbiAgICAgIGlmIChzd2FwU3BlYy5oYXNPd25Qcm9wZXJ0eSgndHJhbnNpdGlvbicpKSB7XG4gICAgICAgIHNob3VsZFRyYW5zaXRpb24gPSBzd2FwU3BlYy50cmFuc2l0aW9uXG4gICAgICB9XG5cbiAgICAgIGlmIChzaG91bGRUcmFuc2l0aW9uICYmXG4gICAgICAgICAgICAgIHRyaWdnZXJFdmVudChlbHQsICdodG14OmJlZm9yZVRyYW5zaXRpb24nLCByZXNwb25zZUluZm8pICYmXG4gICAgICAgICAgICAgIHR5cGVvZiBQcm9taXNlICE9PSAndW5kZWZpbmVkJyAmJlxuICAgICAgICAgICAgICAvLyBAdHMtaWdub3JlIGV4cGVyaW1lbnRhbCBmZWF0dXJlIGF0bVxuICAgICAgICAgICAgICBkb2N1bWVudC5zdGFydFZpZXdUcmFuc2l0aW9uKSB7XG4gICAgICAgIGNvbnN0IHNldHRsZVByb21pc2UgPSBuZXcgUHJvbWlzZShmdW5jdGlvbihfcmVzb2x2ZSwgX3JlamVjdCkge1xuICAgICAgICAgIHNldHRsZVJlc29sdmUgPSBfcmVzb2x2ZVxuICAgICAgICAgIHNldHRsZVJlamVjdCA9IF9yZWplY3RcbiAgICAgICAgfSlcbiAgICAgICAgLy8gd3JhcCB0aGUgb3JpZ2luYWwgZG9Td2FwKCkgaW4gYSBjYWxsIHRvIHN0YXJ0Vmlld1RyYW5zaXRpb24oKVxuICAgICAgICBjb25zdCBpbm5lckRvU3dhcCA9IGRvU3dhcFxuICAgICAgICBkb1N3YXAgPSBmdW5jdGlvbigpIHtcbiAgICAgICAgICAvLyBAdHMtaWdub3JlIGV4cGVyaW1lbnRhbCBmZWF0dXJlIGF0bVxuICAgICAgICAgIGRvY3VtZW50LnN0YXJ0Vmlld1RyYW5zaXRpb24oZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICBpbm5lckRvU3dhcCgpXG4gICAgICAgICAgICByZXR1cm4gc2V0dGxlUHJvbWlzZVxuICAgICAgICAgIH0pXG4gICAgICAgIH1cbiAgICAgIH1cblxuICAgICAgaWYgKHN3YXBTcGVjLnN3YXBEZWxheSA+IDApIHtcbiAgICAgICAgZ2V0V2luZG93KCkuc2V0VGltZW91dChkb1N3YXAsIHN3YXBTcGVjLnN3YXBEZWxheSlcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIGRvU3dhcCgpXG4gICAgICB9XG4gICAgfVxuICAgIGlmIChpc0Vycm9yKSB7XG4gICAgICB0cmlnZ2VyRXJyb3JFdmVudChlbHQsICdodG14OnJlc3BvbnNlRXJyb3InLCBtZXJnZU9iamVjdHMoeyBlcnJvcjogJ1Jlc3BvbnNlIFN0YXR1cyBFcnJvciBDb2RlICcgKyB4aHIuc3RhdHVzICsgJyBmcm9tICcgKyByZXNwb25zZUluZm8ucGF0aEluZm8ucmVxdWVzdFBhdGggfSwgcmVzcG9uc2VJbmZvKSlcbiAgICB9XG4gIH1cblxuICAvLz0gPT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PVxuICAvLyBFeHRlbnNpb25zIEFQSVxuICAvLz0gPT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PVxuXG4gIC8qKiBAdHlwZSB7T2JqZWN0PHN0cmluZywgSHRteEV4dGVuc2lvbj59ICovXG4gIGNvbnN0IGV4dGVuc2lvbnMgPSB7fVxuXG4gIC8qKlxuICAgKiBleHRlbnNpb25CYXNlIGRlZmluZXMgdGhlIGRlZmF1bHQgZnVuY3Rpb25zIGZvciBhbGwgZXh0ZW5zaW9ucy5cbiAgICogQHJldHVybnMge0h0bXhFeHRlbnNpb259XG4gICAqL1xuICBmdW5jdGlvbiBleHRlbnNpb25CYXNlKCkge1xuICAgIHJldHVybiB7XG4gICAgICBpbml0OiBmdW5jdGlvbihhcGkpIHsgcmV0dXJuIG51bGwgfSxcbiAgICAgIGdldFNlbGVjdG9yczogZnVuY3Rpb24oKSB7IHJldHVybiBudWxsIH0sXG4gICAgICBvbkV2ZW50OiBmdW5jdGlvbihuYW1lLCBldnQpIHsgcmV0dXJuIHRydWUgfSxcbiAgICAgIHRyYW5zZm9ybVJlc3BvbnNlOiBmdW5jdGlvbih0ZXh0LCB4aHIsIGVsdCkgeyByZXR1cm4gdGV4dCB9LFxuICAgICAgaXNJbmxpbmVTd2FwOiBmdW5jdGlvbihzd2FwU3R5bGUpIHsgcmV0dXJuIGZhbHNlIH0sXG4gICAgICBoYW5kbGVTd2FwOiBmdW5jdGlvbihzd2FwU3R5bGUsIHRhcmdldCwgZnJhZ21lbnQsIHNldHRsZUluZm8pIHsgcmV0dXJuIGZhbHNlIH0sXG4gICAgICBlbmNvZGVQYXJhbWV0ZXJzOiBmdW5jdGlvbih4aHIsIHBhcmFtZXRlcnMsIGVsdCkgeyByZXR1cm4gbnVsbCB9XG4gICAgfVxuICB9XG5cbiAgLyoqXG4gICAqIGRlZmluZUV4dGVuc2lvbiBpbml0aWFsaXplcyB0aGUgZXh0ZW5zaW9uIGFuZCBhZGRzIGl0IHRvIHRoZSBodG14IHJlZ2lzdHJ5XG4gICAqXG4gICAqIEBzZWUgaHR0cHM6Ly9odG14Lm9yZy9hcGkvI2RlZmluZUV4dGVuc2lvblxuICAgKlxuICAgKiBAcGFyYW0ge3N0cmluZ30gbmFtZSB0aGUgZXh0ZW5zaW9uIG5hbWVcbiAgICogQHBhcmFtIHtQYXJ0aWFsPEh0bXhFeHRlbnNpb24+fSBleHRlbnNpb24gdGhlIGV4dGVuc2lvbiBkZWZpbml0aW9uXG4gICAqL1xuICBmdW5jdGlvbiBkZWZpbmVFeHRlbnNpb24obmFtZSwgZXh0ZW5zaW9uKSB7XG4gICAgaWYgKGV4dGVuc2lvbi5pbml0KSB7XG4gICAgICBleHRlbnNpb24uaW5pdChpbnRlcm5hbEFQSSlcbiAgICB9XG4gICAgZXh0ZW5zaW9uc1tuYW1lXSA9IG1lcmdlT2JqZWN0cyhleHRlbnNpb25CYXNlKCksIGV4dGVuc2lvbilcbiAgfVxuXG4gIC8qKlxuICAgKiByZW1vdmVFeHRlbnNpb24gcmVtb3ZlcyBhbiBleHRlbnNpb24gZnJvbSB0aGUgaHRteCByZWdpc3RyeVxuICAgKlxuICAgKiBAc2VlIGh0dHBzOi8vaHRteC5vcmcvYXBpLyNyZW1vdmVFeHRlbnNpb25cbiAgICpcbiAgICogQHBhcmFtIHtzdHJpbmd9IG5hbWVcbiAgICovXG4gIGZ1bmN0aW9uIHJlbW92ZUV4dGVuc2lvbihuYW1lKSB7XG4gICAgZGVsZXRlIGV4dGVuc2lvbnNbbmFtZV1cbiAgfVxuXG4gIC8qKlxuICAgKiBnZXRFeHRlbnNpb25zIHNlYXJjaGVzIHVwIHRoZSBET00gdHJlZSB0byByZXR1cm4gYWxsIGV4dGVuc2lvbnMgdGhhdCBjYW4gYmUgYXBwbGllZCB0byBhIGdpdmVuIGVsZW1lbnRcbiAgICpcbiAgICogQHBhcmFtIHtFbGVtZW50fSBlbHRcbiAgICogQHBhcmFtIHtIdG14RXh0ZW5zaW9uW109fSBleHRlbnNpb25zVG9SZXR1cm5cbiAgICogQHBhcmFtIHtzdHJpbmdbXT19IGV4dGVuc2lvbnNUb0lnbm9yZVxuICAgKiBAcmV0dXJucyB7SHRteEV4dGVuc2lvbltdfVxuICAgKi9cbiAgZnVuY3Rpb24gZ2V0RXh0ZW5zaW9ucyhlbHQsIGV4dGVuc2lvbnNUb1JldHVybiwgZXh0ZW5zaW9uc1RvSWdub3JlKSB7XG4gICAgaWYgKGV4dGVuc2lvbnNUb1JldHVybiA9PSB1bmRlZmluZWQpIHtcbiAgICAgIGV4dGVuc2lvbnNUb1JldHVybiA9IFtdXG4gICAgfVxuICAgIGlmIChlbHQgPT0gdW5kZWZpbmVkKSB7XG4gICAgICByZXR1cm4gZXh0ZW5zaW9uc1RvUmV0dXJuXG4gICAgfVxuICAgIGlmIChleHRlbnNpb25zVG9JZ25vcmUgPT0gdW5kZWZpbmVkKSB7XG4gICAgICBleHRlbnNpb25zVG9JZ25vcmUgPSBbXVxuICAgIH1cbiAgICBjb25zdCBleHRlbnNpb25zRm9yRWxlbWVudCA9IGdldEF0dHJpYnV0ZVZhbHVlKGVsdCwgJ2h4LWV4dCcpXG4gICAgaWYgKGV4dGVuc2lvbnNGb3JFbGVtZW50KSB7XG4gICAgICBmb3JFYWNoKGV4dGVuc2lvbnNGb3JFbGVtZW50LnNwbGl0KCcsJyksIGZ1bmN0aW9uKGV4dGVuc2lvbk5hbWUpIHtcbiAgICAgICAgZXh0ZW5zaW9uTmFtZSA9IGV4dGVuc2lvbk5hbWUucmVwbGFjZSgvIC9nLCAnJylcbiAgICAgICAgaWYgKGV4dGVuc2lvbk5hbWUuc2xpY2UoMCwgNykgPT0gJ2lnbm9yZTonKSB7XG4gICAgICAgICAgZXh0ZW5zaW9uc1RvSWdub3JlLnB1c2goZXh0ZW5zaW9uTmFtZS5zbGljZSg3KSlcbiAgICAgICAgICByZXR1cm5cbiAgICAgICAgfVxuICAgICAgICBpZiAoZXh0ZW5zaW9uc1RvSWdub3JlLmluZGV4T2YoZXh0ZW5zaW9uTmFtZSkgPCAwKSB7XG4gICAgICAgICAgY29uc3QgZXh0ZW5zaW9uID0gZXh0ZW5zaW9uc1tleHRlbnNpb25OYW1lXVxuICAgICAgICAgIGlmIChleHRlbnNpb24gJiYgZXh0ZW5zaW9uc1RvUmV0dXJuLmluZGV4T2YoZXh0ZW5zaW9uKSA8IDApIHtcbiAgICAgICAgICAgIGV4dGVuc2lvbnNUb1JldHVybi5wdXNoKGV4dGVuc2lvbilcbiAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgIH0pXG4gICAgfVxuICAgIHJldHVybiBnZXRFeHRlbnNpb25zKGFzRWxlbWVudChwYXJlbnRFbHQoZWx0KSksIGV4dGVuc2lvbnNUb1JldHVybiwgZXh0ZW5zaW9uc1RvSWdub3JlKVxuICB9XG5cbiAgLy89ID09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT1cbiAgLy8gSW5pdGlhbGl6YXRpb25cbiAgLy89ID09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT1cbiAgdmFyIGlzUmVhZHkgPSBmYWxzZVxuICBnZXREb2N1bWVudCgpLmFkZEV2ZW50TGlzdGVuZXIoJ0RPTUNvbnRlbnRMb2FkZWQnLCBmdW5jdGlvbigpIHtcbiAgICBpc1JlYWR5ID0gdHJ1ZVxuICB9KVxuXG4gIC8qKlxuICAgKiBFeGVjdXRlIGEgZnVuY3Rpb24gbm93IGlmIERPTUNvbnRlbnRMb2FkZWQgaGFzIGZpcmVkLCBvdGhlcndpc2UgbGlzdGVuIGZvciBpdC5cbiAgICpcbiAgICogVGhpcyBmdW5jdGlvbiB1c2VzIGlzUmVhZHkgYmVjYXVzZSB0aGVyZSBpcyBubyByZWxpYWJsZSB3YXkgdG8gYXNrIHRoZSBicm93c2VyIHdoZXRoZXJcbiAgICogdGhlIERPTUNvbnRlbnRMb2FkZWQgZXZlbnQgaGFzIGFscmVhZHkgYmVlbiBmaXJlZDsgdGhlcmUncyBhIGdhcCBiZXR3ZWVuIERPTUNvbnRlbnRMb2FkZWRcbiAgICogZmlyaW5nIGFuZCByZWFkeXN0YXRlPWNvbXBsZXRlLlxuICAgKi9cbiAgZnVuY3Rpb24gcmVhZHkoZm4pIHtcbiAgICAvLyBDaGVja2luZyByZWFkeVN0YXRlIGhlcmUgaXMgYSBmYWlsc2FmZSBpbiBjYXNlIHRoZSBodG14IHNjcmlwdCB0YWcgZW50ZXJlZCB0aGUgRE9NIGJ5XG4gICAgLy8gc29tZSBtZWFucyBvdGhlciB0aGFuIHRoZSBpbml0aWFsIHBhZ2UgbG9hZC5cbiAgICBpZiAoaXNSZWFkeSB8fCBnZXREb2N1bWVudCgpLnJlYWR5U3RhdGUgPT09ICdjb21wbGV0ZScpIHtcbiAgICAgIGZuKClcbiAgICB9IGVsc2Uge1xuICAgICAgZ2V0RG9jdW1lbnQoKS5hZGRFdmVudExpc3RlbmVyKCdET01Db250ZW50TG9hZGVkJywgZm4pXG4gICAgfVxuICB9XG5cbiAgZnVuY3Rpb24gaW5zZXJ0SW5kaWNhdG9yU3R5bGVzKCkge1xuICAgIGlmIChodG14LmNvbmZpZy5pbmNsdWRlSW5kaWNhdG9yU3R5bGVzICE9PSBmYWxzZSkge1xuICAgICAgY29uc3Qgbm9uY2VBdHRyaWJ1dGUgPSBodG14LmNvbmZpZy5pbmxpbmVTdHlsZU5vbmNlID8gYCBub25jZT1cIiR7aHRteC5jb25maWcuaW5saW5lU3R5bGVOb25jZX1cImAgOiAnJ1xuICAgICAgZ2V0RG9jdW1lbnQoKS5oZWFkLmluc2VydEFkamFjZW50SFRNTCgnYmVmb3JlZW5kJyxcbiAgICAgICAgJzxzdHlsZScgKyBub25jZUF0dHJpYnV0ZSArICc+XFxcbiAgICAgIC4nICsgaHRteC5jb25maWcuaW5kaWNhdG9yQ2xhc3MgKyAne29wYWNpdHk6MH1cXFxuICAgICAgLicgKyBodG14LmNvbmZpZy5yZXF1ZXN0Q2xhc3MgKyAnIC4nICsgaHRteC5jb25maWcuaW5kaWNhdG9yQ2xhc3MgKyAne29wYWNpdHk6MTsgdHJhbnNpdGlvbjogb3BhY2l0eSAyMDBtcyBlYXNlLWluO31cXFxuICAgICAgLicgKyBodG14LmNvbmZpZy5yZXF1ZXN0Q2xhc3MgKyAnLicgKyBodG14LmNvbmZpZy5pbmRpY2F0b3JDbGFzcyArICd7b3BhY2l0eToxOyB0cmFuc2l0aW9uOiBvcGFjaXR5IDIwMG1zIGVhc2UtaW47fVxcXG4gICAgICA8L3N0eWxlPicpXG4gICAgfVxuICB9XG5cbiAgZnVuY3Rpb24gZ2V0TWV0YUNvbmZpZygpIHtcbiAgICAvKiogQHR5cGUgSFRNTE1ldGFFbGVtZW50ICovXG4gICAgY29uc3QgZWxlbWVudCA9IGdldERvY3VtZW50KCkucXVlcnlTZWxlY3RvcignbWV0YVtuYW1lPVwiaHRteC1jb25maWdcIl0nKVxuICAgIGlmIChlbGVtZW50KSB7XG4gICAgICByZXR1cm4gcGFyc2VKU09OKGVsZW1lbnQuY29udGVudClcbiAgICB9IGVsc2Uge1xuICAgICAgcmV0dXJuIG51bGxcbiAgICB9XG4gIH1cblxuICBmdW5jdGlvbiBtZXJnZU1ldGFDb25maWcoKSB7XG4gICAgY29uc3QgbWV0YUNvbmZpZyA9IGdldE1ldGFDb25maWcoKVxuICAgIGlmIChtZXRhQ29uZmlnKSB7XG4gICAgICBodG14LmNvbmZpZyA9IG1lcmdlT2JqZWN0cyhodG14LmNvbmZpZywgbWV0YUNvbmZpZylcbiAgICB9XG4gIH1cblxuICAvLyBpbml0aWFsaXplIHRoZSBkb2N1bWVudFxuICByZWFkeShmdW5jdGlvbigpIHtcbiAgICBtZXJnZU1ldGFDb25maWcoKVxuICAgIGluc2VydEluZGljYXRvclN0eWxlcygpXG4gICAgbGV0IGJvZHkgPSBnZXREb2N1bWVudCgpLmJvZHlcbiAgICBwcm9jZXNzTm9kZShib2R5KVxuICAgIGNvbnN0IHJlc3RvcmVkRWx0cyA9IGdldERvY3VtZW50KCkucXVlcnlTZWxlY3RvckFsbChcbiAgICAgIFwiW2h4LXRyaWdnZXI9J3Jlc3RvcmVkJ10sW2RhdGEtaHgtdHJpZ2dlcj0ncmVzdG9yZWQnXVwiXG4gICAgKVxuICAgIGJvZHkuYWRkRXZlbnRMaXN0ZW5lcignaHRteDphYm9ydCcsIGZ1bmN0aW9uKGV2dCkge1xuICAgICAgY29uc3QgdGFyZ2V0ID0gZXZ0LnRhcmdldFxuICAgICAgY29uc3QgaW50ZXJuYWxEYXRhID0gZ2V0SW50ZXJuYWxEYXRhKHRhcmdldClcbiAgICAgIGlmIChpbnRlcm5hbERhdGEgJiYgaW50ZXJuYWxEYXRhLnhocikge1xuICAgICAgICBpbnRlcm5hbERhdGEueGhyLmFib3J0KClcbiAgICAgIH1cbiAgICB9KVxuICAgIC8qKiBAdHlwZSB7KGV2OiBQb3BTdGF0ZUV2ZW50KSA9PiBhbnl9ICovXG4gICAgY29uc3Qgb3JpZ2luYWxQb3BzdGF0ZSA9IHdpbmRvdy5vbnBvcHN0YXRlID8gd2luZG93Lm9ucG9wc3RhdGUuYmluZCh3aW5kb3cpIDogbnVsbFxuICAgIC8qKiBAdHlwZSB7KGV2OiBQb3BTdGF0ZUV2ZW50KSA9PiBhbnl9ICovXG4gICAgd2luZG93Lm9ucG9wc3RhdGUgPSBmdW5jdGlvbihldmVudCkge1xuICAgICAgaWYgKGV2ZW50LnN0YXRlICYmIGV2ZW50LnN0YXRlLmh0bXgpIHtcbiAgICAgICAgcmVzdG9yZUhpc3RvcnkoKVxuICAgICAgICBmb3JFYWNoKHJlc3RvcmVkRWx0cywgZnVuY3Rpb24oZWx0KSB7XG4gICAgICAgICAgdHJpZ2dlckV2ZW50KGVsdCwgJ2h0bXg6cmVzdG9yZWQnLCB7XG4gICAgICAgICAgICBkb2N1bWVudDogZ2V0RG9jdW1lbnQoKSxcbiAgICAgICAgICAgIHRyaWdnZXJFdmVudFxuICAgICAgICAgIH0pXG4gICAgICAgIH0pXG4gICAgICB9IGVsc2Uge1xuICAgICAgICBpZiAob3JpZ2luYWxQb3BzdGF0ZSkge1xuICAgICAgICAgIG9yaWdpbmFsUG9wc3RhdGUoZXZlbnQpXG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9XG4gICAgZ2V0V2luZG93KCkuc2V0VGltZW91dChmdW5jdGlvbigpIHtcbiAgICAgIHRyaWdnZXJFdmVudChib2R5LCAnaHRteDpsb2FkJywge30pIC8vIGdpdmUgcmVhZHkgaGFuZGxlcnMgYSBjaGFuY2UgdG8gbG9hZCB1cCBiZWZvcmUgZmlyaW5nIHRoaXMgZXZlbnRcbiAgICAgIGJvZHkgPSBudWxsIC8vIGtpbGwgcmVmZXJlbmNlIGZvciBnY1xuICAgIH0sIDApXG4gIH0pXG5cbiAgcmV0dXJuIGh0bXhcbn0pKClcblxuLyoqIEB0eXBlZGVmIHsnZ2V0J3wnaGVhZCd8J3Bvc3QnfCdwdXQnfCdkZWxldGUnfCdjb25uZWN0J3wnb3B0aW9ucyd8J3RyYWNlJ3wncGF0Y2gnfSBIdHRwVmVyYiAqL1xuXG4vKipcbiAqIEB0eXBlZGVmIHtPYmplY3R9IFN3YXBPcHRpb25zXG4gKiBAcHJvcGVydHkge3N0cmluZ30gW3NlbGVjdF1cbiAqIEBwcm9wZXJ0eSB7c3RyaW5nfSBbc2VsZWN0T09CXVxuICogQHByb3BlcnR5IHsqfSBbZXZlbnRJbmZvXVxuICogQHByb3BlcnR5IHtzdHJpbmd9IFthbmNob3JdXG4gKiBAcHJvcGVydHkge0VsZW1lbnR9IFtjb250ZXh0RWxlbWVudF1cbiAqIEBwcm9wZXJ0eSB7c3dhcENhbGxiYWNrfSBbYWZ0ZXJTd2FwQ2FsbGJhY2tdXG4gKiBAcHJvcGVydHkge3N3YXBDYWxsYmFja30gW2FmdGVyU2V0dGxlQ2FsbGJhY2tdXG4gKi9cblxuLyoqXG4gKiBAY2FsbGJhY2sgc3dhcENhbGxiYWNrXG4gKi9cblxuLyoqXG4gKiBAdHlwZWRlZiB7J2lubmVySFRNTCcgfCAnb3V0ZXJIVE1MJyB8ICdiZWZvcmViZWdpbicgfCAnYWZ0ZXJiZWdpbicgfCAnYmVmb3JlZW5kJyB8ICdhZnRlcmVuZCcgfCAnZGVsZXRlJyB8ICdub25lJyB8IHN0cmluZ30gSHRteFN3YXBTdHlsZVxuICovXG5cbi8qKlxuICogQHR5cGVkZWYgSHRteFN3YXBTcGVjaWZpY2F0aW9uXG4gKiBAcHJvcGVydHkge0h0bXhTd2FwU3R5bGV9IHN3YXBTdHlsZVxuICogQHByb3BlcnR5IHtudW1iZXJ9IHN3YXBEZWxheVxuICogQHByb3BlcnR5IHtudW1iZXJ9IHNldHRsZURlbGF5XG4gKiBAcHJvcGVydHkge2Jvb2xlYW59IFt0cmFuc2l0aW9uXVxuICogQHByb3BlcnR5IHtib29sZWFufSBbaWdub3JlVGl0bGVdXG4gKiBAcHJvcGVydHkge3N0cmluZ30gW2hlYWRdXG4gKiBAcHJvcGVydHkgeyd0b3AnIHwgJ2JvdHRvbSd9IFtzY3JvbGxdXG4gKiBAcHJvcGVydHkge3N0cmluZ30gW3Njcm9sbFRhcmdldF1cbiAqIEBwcm9wZXJ0eSB7c3RyaW5nfSBbc2hvd11cbiAqIEBwcm9wZXJ0eSB7c3RyaW5nfSBbc2hvd1RhcmdldF1cbiAqIEBwcm9wZXJ0eSB7Ym9vbGVhbn0gW2ZvY3VzU2Nyb2xsXVxuICovXG5cbi8qKlxuICogQHR5cGVkZWYgeygodGhpczpOb2RlLCBldnQ6RXZlbnQpID0+IGJvb2xlYW4pICYge3NvdXJjZTogc3RyaW5nfX0gQ29uZGl0aW9uYWxGdW5jdGlvblxuICovXG5cbi8qKlxuICogQHR5cGVkZWYge09iamVjdH0gSHRteFRyaWdnZXJTcGVjaWZpY2F0aW9uXG4gKiBAcHJvcGVydHkge3N0cmluZ30gdHJpZ2dlclxuICogQHByb3BlcnR5IHtudW1iZXJ9IFtwb2xsSW50ZXJ2YWxdXG4gKiBAcHJvcGVydHkge0NvbmRpdGlvbmFsRnVuY3Rpb259IFtldmVudEZpbHRlcl1cbiAqIEBwcm9wZXJ0eSB7Ym9vbGVhbn0gW2NoYW5nZWRdXG4gKiBAcHJvcGVydHkge2Jvb2xlYW59IFtvbmNlXVxuICogQHByb3BlcnR5IHtib29sZWFufSBbY29uc3VtZV1cbiAqIEBwcm9wZXJ0eSB7bnVtYmVyfSBbZGVsYXldXG4gKiBAcHJvcGVydHkge3N0cmluZ30gW2Zyb21dXG4gKiBAcHJvcGVydHkge3N0cmluZ30gW3RhcmdldF1cbiAqIEBwcm9wZXJ0eSB7bnVtYmVyfSBbdGhyb3R0bGVdXG4gKiBAcHJvcGVydHkge3N0cmluZ30gW3F1ZXVlXVxuICogQHByb3BlcnR5IHtzdHJpbmd9IFtyb290XVxuICogQHByb3BlcnR5IHtzdHJpbmd9IFt0aHJlc2hvbGRdXG4gKi9cblxuLyoqXG4gKiBAdHlwZWRlZiB7e2VsdDogRWxlbWVudCwgbWVzc2FnZTogc3RyaW5nLCB2YWxpZGl0eTogVmFsaWRpdHlTdGF0ZX19IEh0bXhFbGVtZW50VmFsaWRhdGlvbkVycm9yXG4gKi9cblxuLyoqXG4gKiBAdHlwZWRlZiB7UmVjb3JkPHN0cmluZywgc3RyaW5nPn0gSHRteEhlYWRlclNwZWNpZmljYXRpb25cbiAqIEBwcm9wZXJ0eSB7J3RydWUnfSBIWC1SZXF1ZXN0XG4gKiBAcHJvcGVydHkge3N0cmluZ3xudWxsfSBIWC1UcmlnZ2VyXG4gKiBAcHJvcGVydHkge3N0cmluZ3xudWxsfSBIWC1UcmlnZ2VyLU5hbWVcbiAqIEBwcm9wZXJ0eSB7c3RyaW5nfG51bGx9IEhYLVRhcmdldFxuICogQHByb3BlcnR5IHtzdHJpbmd9IEhYLUN1cnJlbnQtVVJMXG4gKiBAcHJvcGVydHkge3N0cmluZ30gW0hYLVByb21wdF1cbiAqIEBwcm9wZXJ0eSB7J3RydWUnfSBbSFgtQm9vc3RlZF1cbiAqIEBwcm9wZXJ0eSB7c3RyaW5nfSBbQ29udGVudC1UeXBlXVxuICogQHByb3BlcnR5IHsndHJ1ZSd9IFtIWC1IaXN0b3J5LVJlc3RvcmUtUmVxdWVzdF1cbiAqL1xuXG4vKiogQHR5cGVkZWYgSHRteEFqYXhIZWxwZXJDb250ZXh0XG4gKiBAcHJvcGVydHkge0VsZW1lbnR8c3RyaW5nfSBbc291cmNlXVxuICogQHByb3BlcnR5IHtFdmVudH0gW2V2ZW50XVxuICogQHByb3BlcnR5IHtIdG14QWpheEhhbmRsZXJ9IFtoYW5kbGVyXVxuICogQHByb3BlcnR5IHtFbGVtZW50fHN0cmluZ30gW3RhcmdldF1cbiAqIEBwcm9wZXJ0eSB7SHRteFN3YXBTdHlsZX0gW3N3YXBdXG4gKiBAcHJvcGVydHkge09iamVjdHxGb3JtRGF0YX0gW3ZhbHVlc11cbiAqIEBwcm9wZXJ0eSB7UmVjb3JkPHN0cmluZyxzdHJpbmc+fSBbaGVhZGVyc11cbiAqIEBwcm9wZXJ0eSB7c3RyaW5nfSBbc2VsZWN0XVxuICovXG5cbi8qKlxuICogQHR5cGVkZWYge09iamVjdH0gSHRteFJlcXVlc3RDb25maWdcbiAqIEBwcm9wZXJ0eSB7Ym9vbGVhbn0gYm9vc3RlZFxuICogQHByb3BlcnR5IHtib29sZWFufSB1c2VVcmxQYXJhbXNcbiAqIEBwcm9wZXJ0eSB7Rm9ybURhdGF9IGZvcm1EYXRhXG4gKiBAcHJvcGVydHkge09iamVjdH0gcGFyYW1ldGVycyBmb3JtRGF0YSBwcm94eVxuICogQHByb3BlcnR5IHtGb3JtRGF0YX0gdW5maWx0ZXJlZEZvcm1EYXRhXG4gKiBAcHJvcGVydHkge09iamVjdH0gdW5maWx0ZXJlZFBhcmFtZXRlcnMgdW5maWx0ZXJlZEZvcm1EYXRhIHByb3h5XG4gKiBAcHJvcGVydHkge0h0bXhIZWFkZXJTcGVjaWZpY2F0aW9ufSBoZWFkZXJzXG4gKiBAcHJvcGVydHkge0VsZW1lbnR9IHRhcmdldFxuICogQHByb3BlcnR5IHtIdHRwVmVyYn0gdmVyYlxuICogQHByb3BlcnR5IHtIdG14RWxlbWVudFZhbGlkYXRpb25FcnJvcltdfSBlcnJvcnNcbiAqIEBwcm9wZXJ0eSB7Ym9vbGVhbn0gd2l0aENyZWRlbnRpYWxzXG4gKiBAcHJvcGVydHkge251bWJlcn0gdGltZW91dFxuICogQHByb3BlcnR5IHtzdHJpbmd9IHBhdGhcbiAqIEBwcm9wZXJ0eSB7RXZlbnR9IHRyaWdnZXJpbmdFdmVudFxuICovXG5cbi8qKlxuICogQHR5cGVkZWYge09iamVjdH0gSHRteFJlc3BvbnNlSW5mb1xuICogQHByb3BlcnR5IHtYTUxIdHRwUmVxdWVzdH0geGhyXG4gKiBAcHJvcGVydHkge0VsZW1lbnR9IHRhcmdldFxuICogQHByb3BlcnR5IHtIdG14UmVxdWVzdENvbmZpZ30gcmVxdWVzdENvbmZpZ1xuICogQHByb3BlcnR5IHtIdG14QWpheEV0Y30gZXRjXG4gKiBAcHJvcGVydHkge2Jvb2xlYW59IGJvb3N0ZWRcbiAqIEBwcm9wZXJ0eSB7c3RyaW5nfSBzZWxlY3RcbiAqIEBwcm9wZXJ0eSB7e3JlcXVlc3RQYXRoOiBzdHJpbmcsIGZpbmFsUmVxdWVzdFBhdGg6IHN0cmluZywgcmVzcG9uc2VQYXRoOiBzdHJpbmd8bnVsbCwgYW5jaG9yOiBzdHJpbmd9fSBwYXRoSW5mb1xuICogQHByb3BlcnR5IHtib29sZWFufSBbZmFpbGVkXVxuICogQHByb3BlcnR5IHtib29sZWFufSBbc3VjY2Vzc2Z1bF1cbiAqIEBwcm9wZXJ0eSB7Ym9vbGVhbn0gW2tlZXBJbmRpY2F0b3JzXVxuICovXG5cbi8qKlxuICogQHR5cGVkZWYge09iamVjdH0gSHRteEFqYXhFdGNcbiAqIEBwcm9wZXJ0eSB7Ym9vbGVhbn0gW3JldHVyblByb21pc2VdXG4gKiBAcHJvcGVydHkge0h0bXhBamF4SGFuZGxlcn0gW2hhbmRsZXJdXG4gKiBAcHJvcGVydHkge3N0cmluZ30gW3NlbGVjdF1cbiAqIEBwcm9wZXJ0eSB7RWxlbWVudH0gW3RhcmdldE92ZXJyaWRlXVxuICogQHByb3BlcnR5IHtIdG14U3dhcFN0eWxlfSBbc3dhcE92ZXJyaWRlXVxuICogQHByb3BlcnR5IHtSZWNvcmQ8c3RyaW5nLHN0cmluZz59IFtoZWFkZXJzXVxuICogQHByb3BlcnR5IHtPYmplY3R8Rm9ybURhdGF9IFt2YWx1ZXNdXG4gKiBAcHJvcGVydHkge2Jvb2xlYW59IFtjcmVkZW50aWFsc11cbiAqIEBwcm9wZXJ0eSB7bnVtYmVyfSBbdGltZW91dF1cbiAqL1xuXG4vKipcbiAqIEB0eXBlZGVmIHtPYmplY3R9IEh0bXhSZXNwb25zZUhhbmRsaW5nQ29uZmlnXG4gKiBAcHJvcGVydHkge3N0cmluZ30gW2NvZGVdXG4gKiBAcHJvcGVydHkge2Jvb2xlYW59IHN3YXBcbiAqIEBwcm9wZXJ0eSB7Ym9vbGVhbn0gW2Vycm9yXVxuICogQHByb3BlcnR5IHtib29sZWFufSBbaWdub3JlVGl0bGVdXG4gKiBAcHJvcGVydHkge3N0cmluZ30gW3NlbGVjdF1cbiAqIEBwcm9wZXJ0eSB7c3RyaW5nfSBbdGFyZ2V0XVxuICogQHByb3BlcnR5IHtzdHJpbmd9IFtzd2FwT3ZlcnJpZGVdXG4gKiBAcHJvcGVydHkge3N0cmluZ30gW2V2ZW50XVxuICovXG5cbi8qKlxuICogQHR5cGVkZWYge0h0bXhSZXNwb25zZUluZm8gJiB7c2hvdWxkU3dhcDogYm9vbGVhbiwgc2VydmVyUmVzcG9uc2U6IGFueSwgaXNFcnJvcjogYm9vbGVhbiwgaWdub3JlVGl0bGU6IGJvb2xlYW4sIHNlbGVjdE92ZXJyaWRlOnN0cmluZywgc3dhcE92ZXJyaWRlOnN0cmluZ319IEh0bXhCZWZvcmVTd2FwRGV0YWlsc1xuICovXG5cbi8qKlxuICogQGNhbGxiYWNrIEh0bXhBamF4SGFuZGxlclxuICogQHBhcmFtIHtFbGVtZW50fSBlbHRcbiAqIEBwYXJhbSB7SHRteFJlc3BvbnNlSW5mb30gcmVzcG9uc2VJbmZvXG4gKi9cblxuLyoqXG4gKiBAdHlwZWRlZiB7KCgpID0+IHZvaWQpfSBIdG14U2V0dGxlVGFza1xuICovXG5cbi8qKlxuICogQHR5cGVkZWYge09iamVjdH0gSHRteFNldHRsZUluZm9cbiAqIEBwcm9wZXJ0eSB7SHRteFNldHRsZVRhc2tbXX0gdGFza3NcbiAqIEBwcm9wZXJ0eSB7RWxlbWVudFtdfSBlbHRzXG4gKiBAcHJvcGVydHkge3N0cmluZ30gW3RpdGxlXVxuICovXG5cbi8qKlxuICogQHNlZSBodHRwczovL2dpdGh1Yi5jb20vYmlnc2t5c29mdHdhcmUvaHRteC1leHRlbnNpb25zL2Jsb2IvbWFpbi9SRUFETUUubWRcbiAqIEB0eXBlZGVmIHtPYmplY3R9IEh0bXhFeHRlbnNpb25cbiAqIEBwcm9wZXJ0eSB7KGFwaTogYW55KSA9PiB2b2lkfSBpbml0XG4gKiBAcHJvcGVydHkgeyhuYW1lOiBzdHJpbmcsIGV2ZW50OiBFdmVudHxDdXN0b21FdmVudCkgPT4gYm9vbGVhbn0gb25FdmVudFxuICogQHByb3BlcnR5IHsodGV4dDogc3RyaW5nLCB4aHI6IFhNTEh0dHBSZXF1ZXN0LCBlbHQ6IEVsZW1lbnQpID0+IHN0cmluZ30gdHJhbnNmb3JtUmVzcG9uc2VcbiAqIEBwcm9wZXJ0eSB7KHN3YXBTdHlsZTogSHRteFN3YXBTdHlsZSkgPT4gYm9vbGVhbn0gaXNJbmxpbmVTd2FwXG4gKiBAcHJvcGVydHkgeyhzd2FwU3R5bGU6IEh0bXhTd2FwU3R5bGUsIHRhcmdldDogTm9kZSwgZnJhZ21lbnQ6IE5vZGUsIHNldHRsZUluZm86IEh0bXhTZXR0bGVJbmZvKSA9PiBib29sZWFufE5vZGVbXX0gaGFuZGxlU3dhcFxuICogQHByb3BlcnR5IHsoeGhyOiBYTUxIdHRwUmVxdWVzdCwgcGFyYW1ldGVyczogRm9ybURhdGEsIGVsdDogTm9kZSkgPT4gKnxzdHJpbmd8bnVsbH0gZW5jb2RlUGFyYW1ldGVyc1xuICogQHByb3BlcnR5IHsoKSA9PiBzdHJpbmdbXXxudWxsfSBnZXRTZWxlY3RvcnNcbiAqL1xuZXhwb3J0IGRlZmF1bHQgaHRteFxuIiwiLy8gZXh0cmFjdGVkIGJ5IG1pbmktY3NzLWV4dHJhY3QtcGx1Z2luXG5leHBvcnQge307IiwiLy8gVGhlIG1vZHVsZSBjYWNoZVxudmFyIF9fd2VicGFja19tb2R1bGVfY2FjaGVfXyA9IHt9O1xuXG4vLyBUaGUgcmVxdWlyZSBmdW5jdGlvblxuZnVuY3Rpb24gX193ZWJwYWNrX3JlcXVpcmVfXyhtb2R1bGVJZCkge1xuXHQvLyBDaGVjayBpZiBtb2R1bGUgaXMgaW4gY2FjaGVcblx0dmFyIGNhY2hlZE1vZHVsZSA9IF9fd2VicGFja19tb2R1bGVfY2FjaGVfX1ttb2R1bGVJZF07XG5cdGlmIChjYWNoZWRNb2R1bGUgIT09IHVuZGVmaW5lZCkge1xuXHRcdHJldHVybiBjYWNoZWRNb2R1bGUuZXhwb3J0cztcblx0fVxuXHQvLyBDcmVhdGUgYSBuZXcgbW9kdWxlIChhbmQgcHV0IGl0IGludG8gdGhlIGNhY2hlKVxuXHR2YXIgbW9kdWxlID0gX193ZWJwYWNrX21vZHVsZV9jYWNoZV9fW21vZHVsZUlkXSA9IHtcblx0XHQvLyBubyBtb2R1bGUuaWQgbmVlZGVkXG5cdFx0Ly8gbm8gbW9kdWxlLmxvYWRlZCBuZWVkZWRcblx0XHRleHBvcnRzOiB7fVxuXHR9O1xuXG5cdC8vIEV4ZWN1dGUgdGhlIG1vZHVsZSBmdW5jdGlvblxuXHRfX3dlYnBhY2tfbW9kdWxlc19fW21vZHVsZUlkXShtb2R1bGUsIG1vZHVsZS5leHBvcnRzLCBfX3dlYnBhY2tfcmVxdWlyZV9fKTtcblxuXHQvLyBSZXR1cm4gdGhlIGV4cG9ydHMgb2YgdGhlIG1vZHVsZVxuXHRyZXR1cm4gbW9kdWxlLmV4cG9ydHM7XG59XG5cbiIsIi8vIGRlZmluZSBnZXR0ZXIgZnVuY3Rpb25zIGZvciBoYXJtb255IGV4cG9ydHNcbl9fd2VicGFja19yZXF1aXJlX18uZCA9IChleHBvcnRzLCBkZWZpbml0aW9uKSA9PiB7XG5cdGZvcih2YXIga2V5IGluIGRlZmluaXRpb24pIHtcblx0XHRpZihfX3dlYnBhY2tfcmVxdWlyZV9fLm8oZGVmaW5pdGlvbiwga2V5KSAmJiAhX193ZWJwYWNrX3JlcXVpcmVfXy5vKGV4cG9ydHMsIGtleSkpIHtcblx0XHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBrZXksIHsgZW51bWVyYWJsZTogdHJ1ZSwgZ2V0OiBkZWZpbml0aW9uW2tleV0gfSk7XG5cdFx0fVxuXHR9XG59OyIsIl9fd2VicGFja19yZXF1aXJlX18ubyA9IChvYmosIHByb3ApID0+IChPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwob2JqLCBwcm9wKSkiLCIvLyBkZWZpbmUgX19lc01vZHVsZSBvbiBleHBvcnRzXG5fX3dlYnBhY2tfcmVxdWlyZV9fLnIgPSAoZXhwb3J0cykgPT4ge1xuXHRpZih0eXBlb2YgU3ltYm9sICE9PSAndW5kZWZpbmVkJyAmJiBTeW1ib2wudG9TdHJpbmdUYWcpIHtcblx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgU3ltYm9sLnRvU3RyaW5nVGFnLCB7IHZhbHVlOiAnTW9kdWxlJyB9KTtcblx0fVxuXHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgJ19fZXNNb2R1bGUnLCB7IHZhbHVlOiB0cnVlIH0pO1xufTsiLCJpbXBvcnQgJy4uL3N0eWxlcy9pbnB1dC5jc3MnO1xud2luZG93Lmh0bXggPSByZXF1aXJlKCdodG14Lm9yZycpO1xuXG53aW5kb3cuYWRkRXZlbnRMaXN0ZW5lcignbG9hZCcsICgpID0+IHtcbiAgICBjb25zb2xlLnByaW50KCdIZWxsbywgTEFSQSBkamFuZ28hJyk7XG59KTtcbiJdLCJuYW1lcyI6WyJodG14Iiwib25Mb2FkIiwicHJvY2VzcyIsIm9uIiwib2ZmIiwidHJpZ2dlciIsImFqYXgiLCJmaW5kIiwiZmluZEFsbCIsImNsb3Nlc3QiLCJ2YWx1ZXMiLCJlbHQiLCJ0eXBlIiwiaW5wdXRWYWx1ZXMiLCJnZXRJbnB1dFZhbHVlcyIsInJlbW92ZSIsImFkZENsYXNzIiwicmVtb3ZlQ2xhc3MiLCJ0b2dnbGVDbGFzcyIsInRha2VDbGFzcyIsInN3YXAiLCJkZWZpbmVFeHRlbnNpb24iLCJyZW1vdmVFeHRlbnNpb24iLCJsb2dBbGwiLCJsb2dOb25lIiwibG9nZ2VyIiwiY29uZmlnIiwiaGlzdG9yeUVuYWJsZWQiLCJoaXN0b3J5Q2FjaGVTaXplIiwicmVmcmVzaE9uSGlzdG9yeU1pc3MiLCJkZWZhdWx0U3dhcFN0eWxlIiwiZGVmYXVsdFN3YXBEZWxheSIsImRlZmF1bHRTZXR0bGVEZWxheSIsImluY2x1ZGVJbmRpY2F0b3JTdHlsZXMiLCJpbmRpY2F0b3JDbGFzcyIsInJlcXVlc3RDbGFzcyIsImFkZGVkQ2xhc3MiLCJzZXR0bGluZ0NsYXNzIiwic3dhcHBpbmdDbGFzcyIsImFsbG93RXZhbCIsImFsbG93U2NyaXB0VGFncyIsImlubGluZVNjcmlwdE5vbmNlIiwiaW5saW5lU3R5bGVOb25jZSIsImF0dHJpYnV0ZXNUb1NldHRsZSIsIndpdGhDcmVkZW50aWFscyIsInRpbWVvdXQiLCJ3c1JlY29ubmVjdERlbGF5Iiwid3NCaW5hcnlUeXBlIiwiZGlzYWJsZVNlbGVjdG9yIiwic2Nyb2xsQmVoYXZpb3IiLCJkZWZhdWx0Rm9jdXNTY3JvbGwiLCJnZXRDYWNoZUJ1c3RlclBhcmFtIiwiZ2xvYmFsVmlld1RyYW5zaXRpb25zIiwibWV0aG9kc1RoYXRVc2VVcmxQYXJhbXMiLCJzZWxmUmVxdWVzdHNPbmx5IiwiaWdub3JlVGl0bGUiLCJzY3JvbGxJbnRvVmlld09uQm9vc3QiLCJ0cmlnZ2VyU3BlY3NDYWNoZSIsImRpc2FibGVJbmhlcml0YW5jZSIsInJlc3BvbnNlSGFuZGxpbmciLCJjb2RlIiwiZXJyb3IiLCJhbGxvd05lc3RlZE9vYlN3YXBzIiwicGFyc2VJbnRlcnZhbCIsIl8iLCJ2ZXJzaW9uIiwib25Mb2FkSGVscGVyIiwicHJvY2Vzc05vZGUiLCJhZGRFdmVudExpc3RlbmVySW1wbCIsInJlbW92ZUV2ZW50TGlzdGVuZXJJbXBsIiwidHJpZ2dlckV2ZW50IiwiYWpheEhlbHBlciIsInJlbW92ZUVsZW1lbnQiLCJhZGRDbGFzc1RvRWxlbWVudCIsInJlbW92ZUNsYXNzRnJvbUVsZW1lbnQiLCJ0b2dnbGVDbGFzc09uRWxlbWVudCIsInRha2VDbGFzc0ZvckVsZW1lbnQiLCJpbnRlcm5hbEV2YWwiLCJpbnRlcm5hbEFQSSIsImFkZFRyaWdnZXJIYW5kbGVyIiwiYm9keUNvbnRhaW5zIiwiY2FuQWNjZXNzTG9jYWxTdG9yYWdlIiwiZmluZFRoaXNFbGVtZW50IiwiZmlsdGVyVmFsdWVzIiwiaGFzQXR0cmlidXRlIiwiZ2V0QXR0cmlidXRlVmFsdWUiLCJnZXRDbG9zZXN0QXR0cmlidXRlVmFsdWUiLCJnZXRDbG9zZXN0TWF0Y2giLCJnZXRFeHByZXNzaW9uVmFycyIsImdldEhlYWRlcnMiLCJnZXRJbnRlcm5hbERhdGEiLCJnZXRTd2FwU3BlY2lmaWNhdGlvbiIsImdldFRyaWdnZXJTcGVjcyIsImdldFRhcmdldCIsIm1ha2VGcmFnbWVudCIsIm1lcmdlT2JqZWN0cyIsIm1ha2VTZXR0bGVJbmZvIiwib29iU3dhcCIsInF1ZXJ5U2VsZWN0b3JFeHQiLCJzZXR0bGVJbW1lZGlhdGVseSIsInNob3VsZENhbmNlbCIsInRyaWdnZXJFcnJvckV2ZW50Iiwid2l0aEV4dGVuc2lvbnMiLCJWRVJCUyIsIlZFUkJfU0VMRUNUT1IiLCJtYXAiLCJ2ZXJiIiwiam9pbiIsInN0ciIsInVuZGVmaW5lZCIsImludGVydmFsIiwiTmFOIiwic2xpY2UiLCJwYXJzZUZsb2F0IiwiaXNOYU4iLCJnZXRSYXdBdHRyaWJ1dGUiLCJuYW1lIiwiRWxlbWVudCIsImdldEF0dHJpYnV0ZSIsInF1YWxpZmllZE5hbWUiLCJwYXJlbnRFbHQiLCJwYXJlbnQiLCJwYXJlbnRFbGVtZW50IiwicGFyZW50Tm9kZSIsIlNoYWRvd1Jvb3QiLCJnZXREb2N1bWVudCIsImRvY3VtZW50IiwiZ2V0Um9vdE5vZGUiLCJnbG9iYWwiLCJjb21wb3NlZCIsImNvbmRpdGlvbiIsImdldEF0dHJpYnV0ZVZhbHVlV2l0aERpc2luaGVyaXRhbmNlIiwiaW5pdGlhbEVsZW1lbnQiLCJhbmNlc3RvciIsImF0dHJpYnV0ZU5hbWUiLCJhdHRyaWJ1dGVWYWx1ZSIsImRpc2luaGVyaXQiLCJpbmhlcml0Iiwic3BsaXQiLCJpbmRleE9mIiwiY2xvc2VzdEF0dHIiLCJlIiwiYXNFbGVtZW50IiwibWF0Y2hlcyIsInNlbGVjdG9yIiwibWF0Y2hlc0Z1bmN0aW9uIiwibWF0Y2hlc1NlbGVjdG9yIiwibXNNYXRjaGVzU2VsZWN0b3IiLCJtb3pNYXRjaGVzU2VsZWN0b3IiLCJ3ZWJraXRNYXRjaGVzU2VsZWN0b3IiLCJvTWF0Y2hlc1NlbGVjdG9yIiwiY2FsbCIsImdldFN0YXJ0VGFnIiwidGFnTWF0Y2hlciIsIm1hdGNoIiwiZXhlYyIsInRvTG93ZXJDYXNlIiwicGFyc2VIVE1MIiwicmVzcCIsInBhcnNlciIsIkRPTVBhcnNlciIsInBhcnNlRnJvbVN0cmluZyIsInRha2VDaGlsZHJlbkZvciIsImZyYWdtZW50IiwiY2hpbGROb2RlcyIsImxlbmd0aCIsImFwcGVuZCIsImR1cGxpY2F0ZVNjcmlwdCIsInNjcmlwdCIsIm5ld1NjcmlwdCIsImNyZWF0ZUVsZW1lbnQiLCJmb3JFYWNoIiwiYXR0cmlidXRlcyIsImF0dHIiLCJzZXRBdHRyaWJ1dGUiLCJ2YWx1ZSIsInRleHRDb250ZW50IiwiYXN5bmMiLCJub25jZSIsImlzSmF2YVNjcmlwdFNjcmlwdE5vZGUiLCJub3JtYWxpemVTY3JpcHRUYWdzIiwiQXJyYXkiLCJmcm9tIiwicXVlcnlTZWxlY3RvckFsbCIsImluc2VydEJlZm9yZSIsImxvZ0Vycm9yIiwicmVzcG9uc2UiLCJyZXNwb25zZVdpdGhOb0hlYWQiLCJyZXBsYWNlIiwic3RhcnRUYWciLCJEb2N1bWVudEZyYWdtZW50IiwiZG9jIiwiYm9keSIsInRpdGxlIiwicXVlcnlTZWxlY3RvciIsImNvbnRlbnQiLCJ0aXRsZUVsZW1lbnQiLCJpbm5lclRleHQiLCJtYXliZUNhbGwiLCJmdW5jIiwiaXNUeXBlIiwibyIsIk9iamVjdCIsInByb3RvdHlwZSIsInRvU3RyaW5nIiwiaXNGdW5jdGlvbiIsImlzUmF3T2JqZWN0IiwiZGF0YVByb3AiLCJkYXRhIiwidG9BcnJheSIsImFyciIsInJldHVybkFyciIsImkiLCJwdXNoIiwiaXNTY3JvbGxlZEludG9WaWV3IiwiZWwiLCJyZWN0IiwiZ2V0Qm91bmRpbmdDbGllbnRSZWN0IiwiZWxlbVRvcCIsInRvcCIsImVsZW1Cb3R0b20iLCJib3R0b20iLCJ3aW5kb3ciLCJpbm5lckhlaWdodCIsInNwbGl0T25XaGl0ZXNwYWNlIiwidHJpbSIsIm9iajEiLCJvYmoyIiwia2V5IiwiaGFzT3duUHJvcGVydHkiLCJwYXJzZUpTT04iLCJqU3RyaW5nIiwiSlNPTiIsInBhcnNlIiwidGVzdCIsImxvY2FsU3RvcmFnZSIsInNldEl0ZW0iLCJyZW1vdmVJdGVtIiwibm9ybWFsaXplUGF0aCIsInBhdGgiLCJ1cmwiLCJVUkwiLCJwYXRobmFtZSIsInNlYXJjaCIsIm1heWJlRXZhbCIsImV2YWwiLCJjYWxsYmFjayIsImV2dCIsImRldGFpbCIsImV2ZW50IiwiY29uc29sZSIsImxvZyIsImVsdE9yU2VsZWN0b3IiLCJnZXRXaW5kb3ciLCJkZWxheSIsInJlc29sdmVUYXJnZXQiLCJzZXRUaW1lb3V0IiwicmVtb3ZlQ2hpbGQiLCJhc0h0bWxFbGVtZW50IiwiSFRNTEVsZW1lbnQiLCJhc1N0cmluZyIsImFzUGFyZW50Tm9kZSIsIkRvY3VtZW50IiwiY2xhenoiLCJjbGFzc0xpc3QiLCJhZGQiLCJub2RlIiwicmVtb3ZlQXR0cmlidXRlIiwidG9nZ2xlIiwiY2hpbGRyZW4iLCJjaGlsZCIsInN0YXJ0c1dpdGgiLCJwcmVmaXgiLCJzdWJzdHJpbmciLCJlbmRzV2l0aCIsInN1ZmZpeCIsIm5vcm1hbGl6ZVNlbGVjdG9yIiwidHJpbW1lZFNlbGVjdG9yIiwicXVlcnlTZWxlY3RvckFsbEV4dCIsInBhcnRzIiwiY2hldnJvbnNDb3VudCIsIm9mZnNldCIsImNoYXIiLCJyZXN1bHQiLCJ1bnByb2Nlc3NlZFBhcnRzIiwic2hpZnQiLCJpdGVtIiwic3Vic3RyIiwibmV4dEVsZW1lbnRTaWJsaW5nIiwic2NhbkZvcndhcmRRdWVyeSIsInByZXZpb3VzRWxlbWVudFNpYmxpbmciLCJzY2FuQmFja3dhcmRzUXVlcnkiLCJob3N0Iiwic3RhbmRhcmRTZWxlY3RvciIsInJvb3ROb2RlIiwic3RhcnQiLCJyZXN1bHRzIiwiY29tcGFyZURvY3VtZW50UG9zaXRpb24iLCJOb2RlIiwiRE9DVU1FTlRfUE9TSVRJT05fUFJFQ0VESU5HIiwiRE9DVU1FTlRfUE9TSVRJT05fRk9MTE9XSU5HIiwiY29udGV4dCIsInByb2Nlc3NFdmVudEFyZ3MiLCJhcmcxIiwiYXJnMiIsImFyZzMiLCJhcmc0IiwidGFyZ2V0IiwibGlzdGVuZXIiLCJvcHRpb25zIiwicmVhZHkiLCJldmVudEFyZ3MiLCJhZGRFdmVudExpc3RlbmVyIiwiYiIsInJlbW92ZUV2ZW50TGlzdGVuZXIiLCJEVU1NWV9FTFQiLCJmaW5kQXR0cmlidXRlVGFyZ2V0cyIsImF0dHJOYW1lIiwiYXR0clRhcmdldCIsImF0dHJpYnV0ZSIsInRhcmdldFN0ciIsImJvb3N0ZWQiLCJzaG91bGRTZXR0bGVBdHRyaWJ1dGUiLCJjbG9uZUF0dHJpYnV0ZXMiLCJtZXJnZVRvIiwibWVyZ2VGcm9tIiwiaXNJbmxpbmVTd2FwIiwic3dhcFN0eWxlIiwiZXh0ZW5zaW9ucyIsImdldEV4dGVuc2lvbnMiLCJleHRlbnNpb24iLCJvb2JWYWx1ZSIsIm9vYkVsZW1lbnQiLCJzZXR0bGVJbmZvIiwidGFyZ2V0cyIsIm9vYkVsZW1lbnRDbG9uZSIsImNsb25lTm9kZSIsImNyZWF0ZURvY3VtZW50RnJhZ21lbnQiLCJhcHBlbmRDaGlsZCIsImJlZm9yZVN3YXBEZXRhaWxzIiwic2hvdWxkU3dhcCIsImhhbmRsZVByZXNlcnZlZEVsZW1lbnRzIiwic3dhcFdpdGhTdHlsZSIsInJlc3RvcmVQcmVzZXJ2ZWRFbGVtZW50cyIsImVsdHMiLCJwYW50cnkiLCJwcmVzZXJ2ZWRFbHQiLCJleGlzdGluZ0VsZW1lbnQiLCJpZCIsIm1vdmVCZWZvcmUiLCJnZXRFbGVtZW50QnlJZCIsImluc2VydEFkamFjZW50SFRNTCIsInJlcGxhY2VDaGlsZCIsImhhbmRsZUF0dHJpYnV0ZXMiLCJuZXdOb2RlIiwibm9ybWFsaXplZElkIiwibm9ybWFsaXplZFRhZyIsInRhZ05hbWUiLCJvbGROb2RlIiwibmV3QXR0cmlidXRlcyIsInRhc2tzIiwibWFrZUFqYXhMb2FkVGFzayIsInByb2Nlc3NGb2N1cyIsImF1dG9mb2N1cyIsImF1dG9Gb2N1c2VkRWx0IiwiZm9jdXMiLCJpbnNlcnROb2Rlc0JlZm9yZSIsImZpcnN0Q2hpbGQiLCJub2RlVHlwZSIsIlRFWFRfTk9ERSIsIkNPTU1FTlRfTk9ERSIsInN0cmluZ0hhc2giLCJzdHJpbmciLCJoYXNoIiwiY2hhckNvZGVBdCIsImF0dHJpYnV0ZUhhc2giLCJkZUluaXRPbkhhbmRsZXJzIiwiaW50ZXJuYWxEYXRhIiwib25IYW5kbGVycyIsImhhbmRsZXJJbmZvIiwiZGVJbml0Tm9kZSIsImVsZW1lbnQiLCJjbGVhclRpbWVvdXQiLCJsaXN0ZW5lckluZm9zIiwiaW5mbyIsImtleXMiLCJjbGVhblVwRWxlbWVudCIsInN3YXBPdXRlckhUTUwiLCJzd2FwSW5uZXJIVE1MIiwibmV3RWx0IiwiZWx0QmVmb3JlTmV3Q29udGVudCIsInByZXZpb3VzU2libGluZyIsIm5leHRTaWJsaW5nIiwiZmlsdGVyIiwic3dhcEFmdGVyQmVnaW4iLCJzd2FwQmVmb3JlQmVnaW4iLCJzd2FwQmVmb3JlRW5kIiwic3dhcEFmdGVyRW5kIiwic3dhcERlbGV0ZSIsImV4dCIsIm5ld0VsZW1lbnRzIiwiaGFuZGxlU3dhcCIsImlzQXJyYXkiLCJqIiwiZmluZEFuZFN3YXBPb2JFbGVtZW50cyIsIm9vYkVsdHMiLCJzd2FwU3BlYyIsInN3YXBPcHRpb25zIiwiY29udGV4dEVsZW1lbnQiLCJhY3RpdmVFbHQiLCJhY3RpdmVFbGVtZW50Iiwic2VsZWN0aW9uSW5mbyIsInNlbGVjdGlvblN0YXJ0IiwiZW5kIiwic2VsZWN0aW9uRW5kIiwic2VsZWN0T09CIiwib29iU2VsZWN0VmFsdWVzIiwib29iU2VsZWN0VmFsdWUiLCJ0ZW1wbGF0ZSIsInNlbGVjdCIsIm5ld0ZyYWdtZW50IiwibmV3QWN0aXZlRWx0IiwiZm9jdXNPcHRpb25zIiwicHJldmVudFNjcm9sbCIsImZvY3VzU2Nyb2xsIiwic2V0U2VsZWN0aW9uUmFuZ2UiLCJldmVudEluZm8iLCJhZnRlclN3YXBDYWxsYmFjayIsImhhbmRsZVRpdGxlIiwiZG9TZXR0bGUiLCJ0YXNrIiwiYW5jaG9yIiwiYW5jaG9yVGFyZ2V0Iiwic2Nyb2xsSW50b1ZpZXciLCJibG9jayIsImJlaGF2aW9yIiwidXBkYXRlU2Nyb2xsU3RhdGUiLCJhZnRlclNldHRsZUNhbGxiYWNrIiwic2V0dGxlRGVsYXkiLCJoYW5kbGVUcmlnZ2VySGVhZGVyIiwieGhyIiwiaGVhZGVyIiwidHJpZ2dlckJvZHkiLCJnZXRSZXNwb25zZUhlYWRlciIsInRyaWdnZXJzIiwiZXZlbnROYW1lIiwiZXZlbnROYW1lcyIsIldISVRFU1BBQ0UiLCJXSElURVNQQUNFX09SX0NPTU1BIiwiU1lNQk9MX1NUQVJUIiwiU1lNQk9MX0NPTlQiLCJTVFJJTkdJU0hfU1RBUlQiLCJOT1RfV0hJVEVTUEFDRSIsIkNPTUJJTkVEX1NFTEVDVE9SX1NUQVJUIiwiQ09NQklORURfU0VMRUNUT1JfRU5EIiwidG9rZW5pemVTdHJpbmciLCJ0b2tlbnMiLCJwb3NpdGlvbiIsImNoYXJBdCIsInN0YXJ0UG9zaXRpb24iLCJzdGFydENoYXIiLCJzeW1ib2wiLCJpc1Bvc3NpYmxlUmVsYXRpdmVSZWZlcmVuY2UiLCJ0b2tlbiIsImxhc3QiLCJwYXJhbU5hbWUiLCJtYXliZUdlbmVyYXRlQ29uZGl0aW9uYWwiLCJicmFja2V0Q291bnQiLCJjb25kaXRpb25hbFNvdXJjZSIsImNvbmRpdGlvbkZ1bmN0aW9uIiwiRnVuY3Rpb24iLCJzb3VyY2UiLCJjb25zdW1lVW50aWwiLCJjb25zdW1lQ1NTU2VsZWN0b3IiLCJJTlBVVF9TRUxFQ1RPUiIsInBhcnNlQW5kQ2FjaGVUcmlnZ2VyIiwiZXhwbGljaXRUcmlnZ2VyIiwiY2FjaGUiLCJ0cmlnZ2VyU3BlY3MiLCJpbml0aWFsTGVuZ3RoIiwiZXZlcnkiLCJwb2xsSW50ZXJ2YWwiLCJldmVudEZpbHRlciIsInRyaWdnZXJTcGVjIiwiY2hhbmdlZCIsIm9uY2UiLCJjb25zdW1lIiwiZnJvbV9hcmciLCJ0aHJvdHRsZSIsInF1ZXVlIiwiY2FuY2VsUG9sbGluZyIsImNhbmNlbGxlZCIsInByb2Nlc3NQb2xsaW5nIiwiaGFuZGxlciIsInNwZWMiLCJub2RlRGF0YSIsIm1heWJlRmlsdGVyRXZlbnQiLCJtYWtlRXZlbnQiLCJpc0xvY2FsTGluayIsImxvY2F0aW9uIiwiaG9zdG5hbWUiLCJlbHRJc0Rpc2FibGVkIiwiYm9vc3RFbGVtZW50IiwiSFRNTEFuY2hvckVsZW1lbnQiLCJTdHJpbmciLCJyYXdBdHRyaWJ1dGUiLCJocmVmIiwiaW5jbHVkZXMiLCJpc3N1ZUFqYXhSZXF1ZXN0IiwiaWdub3JlQm9vc3RlZEFuY2hvckN0cmxDbGljayIsImN0cmxLZXkiLCJtZXRhS2V5IiwiZXhwbGljaXRDYW5jZWwiLCJlbGVtZW50RGF0YSIsImVsdHNUb0xpc3Rlbk9uIiwibGFzdFZhbHVlIiwiV2Vha01hcCIsImVsdFRvTGlzdGVuT24iLCJoYXMiLCJzZXQiLCJnZXQiLCJldmVudExpc3RlbmVyIiwicHJldmVudERlZmF1bHQiLCJldmVudERhdGEiLCJoYW5kbGVkRm9yIiwic3RvcFByb3BhZ2F0aW9uIiwidHJpZ2dlcmVkT25jZSIsImRlbGF5ZWQiLCJ3aW5kb3dJc1Njcm9sbGluZyIsInNjcm9sbEhhbmRsZXIiLCJpbml0U2Nyb2xsSGFuZGxlciIsInNldEludGVydmFsIiwibWF5YmVSZXZlYWwiLCJpbml0SGFzaCIsImxvYWRJbW1lZGlhdGVseSIsImxvYWQiLCJsb2FkZWQiLCJwcm9jZXNzVmVyYnMiLCJleHBsaWNpdEFjdGlvbiIsIm9ic2VydmVyT3B0aW9ucyIsInJvb3QiLCJ0aHJlc2hvbGQiLCJvYnNlcnZlciIsIkludGVyc2VjdGlvbk9ic2VydmVyIiwiZW50cmllcyIsImVudHJ5IiwiaXNJbnRlcnNlY3RpbmciLCJvYnNlcnZlIiwiZmlyc3RJbml0Q29tcGxldGVkIiwicG9sbGluZyIsInNob3VsZFByb2Nlc3NIeE9uIiwiSFhfT05fUVVFUlkiLCJYUGF0aEV2YWx1YXRvciIsImNyZWF0ZUV4cHJlc3Npb24iLCJwcm9jZXNzSFhPblJvb3QiLCJlbGVtZW50cyIsIml0ZXIiLCJldmFsdWF0ZSIsIml0ZXJhdGVOZXh0IiwiZmluZEh4T25XaWxkY2FyZEVsZW1lbnRzIiwiZmluZEVsZW1lbnRzVG9Qcm9jZXNzIiwiYm9vc3RlZFNlbGVjdG9yIiwiZXh0ZW5zaW9uU2VsZWN0b3JzIiwiZ2V0U2VsZWN0b3JzIiwic2VsZWN0b3JzIiwiZmxhdCIsInMiLCJtYXliZVNldExhc3RCdXR0b25DbGlja2VkIiwiZ2V0UmVsYXRlZEZvcm1EYXRhIiwibGFzdEJ1dHRvbkNsaWNrZWQiLCJtYXliZVVuc2V0TGFzdEJ1dHRvbkNsaWNrZWQiLCJmb3JtIiwiaW5pdEJ1dHRvblRyYWNraW5nIiwiYWRkSHhPbkV2ZW50SGFuZGxlciIsInByb2Nlc3NIeE9uV2lsZGNhcmQiLCJhZnRlck9uUG9zaXRpb24iLCJuZXh0Q2hhciIsImluaXROb2RlIiwiYXR0ckhhc2giLCJoYXNFeHBsaWNpdEh0dHBBY3Rpb24iLCJrZWJhYkV2ZW50TmFtZSIsIkN1c3RvbUV2ZW50IiwiYnViYmxlcyIsImNhbmNlbGFibGUiLCJjcmVhdGVFdmVudCIsImluaXRDdXN0b21FdmVudCIsImlnbm9yZUV2ZW50Rm9yTG9nZ2luZyIsInRvRG8iLCJtc2ciLCJlcnJvckluZm8iLCJldmVudFJlc3VsdCIsImRpc3BhdGNoRXZlbnQiLCJrZWJhYk5hbWUiLCJrZWJhYmVkRXZlbnQiLCJvbkV2ZW50IiwiZGVmYXVsdFByZXZlbnRlZCIsImN1cnJlbnRQYXRoRm9ySGlzdG9yeSIsImdldEhpc3RvcnlFbGVtZW50IiwiaGlzdG9yeUVsdCIsInNhdmVUb0hpc3RvcnlDYWNoZSIsInJvb3RFbHQiLCJpbm5lckhUTUwiLCJjbGVhbklubmVySHRtbEZvckhpc3RvcnkiLCJzY3JvbGwiLCJzY3JvbGxZIiwiaGlzdG9yeUNhY2hlIiwiZ2V0SXRlbSIsInNwbGljZSIsIm5ld0hpc3RvcnlJdGVtIiwic3RyaW5naWZ5IiwiY2F1c2UiLCJnZXRDYWNoZWRIaXN0b3J5IiwiY2xhc3NOYW1lIiwiY2xvbmUiLCJzYXZlQ3VycmVudFBhZ2VUb0hpc3RvcnkiLCJkaXNhYmxlSGlzdG9yeUNhY2hlIiwiaGlzdG9yeSIsInJlcGxhY2VTdGF0ZSIsInB1c2hVcmxJbnRvSGlzdG9yeSIsInB1c2hTdGF0ZSIsInJlcGxhY2VVcmxJbkhpc3RvcnkiLCJsb2FkSGlzdG9yeUZyb21TZXJ2ZXIiLCJyZXF1ZXN0IiwiWE1MSHR0cFJlcXVlc3QiLCJkZXRhaWxzIiwib3BlbiIsInNldFJlcXVlc3RIZWFkZXIiLCJvbmxvYWQiLCJzdGF0dXMiLCJoaXN0b3J5RWxlbWVudCIsImNhY2hlTWlzcyIsInNlcnZlclJlc3BvbnNlIiwic2VuZCIsInJlc3RvcmVIaXN0b3J5IiwiY2FjaGVkIiwic2Nyb2xsVG8iLCJyZWxvYWQiLCJhZGRSZXF1ZXN0SW5kaWNhdG9yQ2xhc3NlcyIsImluZGljYXRvcnMiLCJpYyIsInJlcXVlc3RDb3VudCIsImRpc2FibGVFbGVtZW50cyIsImRpc2FibGVkRWx0cyIsImRpc2FibGVkRWxlbWVudCIsInJlbW92ZVJlcXVlc3RJbmRpY2F0b3JzIiwiZGlzYWJsZWQiLCJjb25jYXQiLCJlbGUiLCJoYXZlU2Vlbk5vZGUiLCJwcm9jZXNzZWQiLCJpc1NhbWVOb2RlIiwic2hvdWxkSW5jbHVkZSIsImNoZWNrZWQiLCJhZGRWYWx1ZVRvRm9ybURhdGEiLCJmb3JtRGF0YSIsInYiLCJyZW1vdmVWYWx1ZUZyb21Gb3JtRGF0YSIsImdldEFsbCIsImRlbGV0ZSIsInByb2Nlc3NJbnB1dFZhbHVlIiwiZXJyb3JzIiwidmFsaWRhdGUiLCJIVE1MU2VsZWN0RWxlbWVudCIsIm11bHRpcGxlIiwiSFRNTElucHV0RWxlbWVudCIsImZpbGVzIiwidmFsaWRhdGVFbGVtZW50IiwiSFRNTEZvcm1FbGVtZW50IiwiaW5wdXQiLCJGb3JtRGF0YSIsIkZpbGUiLCJ3aWxsVmFsaWRhdGUiLCJjaGVja1ZhbGlkaXR5IiwibWVzc2FnZSIsInZhbGlkYXRpb25NZXNzYWdlIiwidmFsaWRpdHkiLCJvdmVycmlkZUZvcm1EYXRhIiwicmVjZWl2ZXIiLCJkb25vciIsInByaW9yaXR5Rm9ybURhdGEiLCJub1ZhbGlkYXRlIiwiZm9ybU5vVmFsaWRhdGUiLCJidXR0b24iLCJkZXNjZW5kYW50IiwiZm9ybURhdGFQcm94eSIsImFwcGVuZFBhcmFtIiwicmV0dXJuU3RyIiwicmVhbFZhbHVlIiwiZW5jb2RlVVJJQ29tcG9uZW50IiwidXJsRW5jb2RlIiwiZm9ybURhdGFGcm9tT2JqZWN0IiwicHJvbXB0IiwiaGVhZGVycyIsImdldFZhbHVlc0ZvckVsZW1lbnQiLCJwYXJhbXNWYWx1ZSIsIm5ld1ZhbHVlcyIsImlzQW5jaG9yTGluayIsInN3YXBJbmZvT3ZlcnJpZGUiLCJzd2FwSW5mbyIsInN3YXBEZWxheSIsInNob3ciLCJ0cmFuc2l0aW9uIiwic2Nyb2xsU3BlYyIsInNwbGl0U3BlYyIsInNjcm9sbFZhbCIsInBvcCIsInNlbGVjdG9yVmFsIiwic2Nyb2xsVGFyZ2V0Iiwic2hvd1NwZWMiLCJzaG93VmFsIiwic2hvd1RhcmdldCIsImZvY3VzU2Nyb2xsVmFsIiwidXNlc0Zvcm1EYXRhIiwiZW5jb2RlUGFyYW1zRm9yQm9keSIsImZpbHRlcmVkUGFyYW1ldGVycyIsImVuY29kZWRQYXJhbWV0ZXJzIiwiZW5jb2RlUGFyYW1ldGVycyIsImZpcnN0Iiwic2Nyb2xsVG9wIiwic2Nyb2xsSGVpZ2h0IiwiZXZhbEFzRGVmYXVsdCIsImV2YWx1YXRlVmFsdWUiLCJ2YXJzVmFsdWVzIiwidG9FdmFsIiwiZGVmYXVsdFZhbCIsImdldEhYVmFyc0ZvckVsZW1lbnQiLCJleHByZXNzaW9uVmFycyIsImdldEhYVmFsc0ZvckVsZW1lbnQiLCJzYWZlbHlTZXRIZWFkZXJWYWx1ZSIsImhlYWRlclZhbHVlIiwiZ2V0UGF0aEZyb21SZXNwb25zZSIsInJlc3BvbnNlVVJMIiwiaGFzSGVhZGVyIiwicmVnZXhwIiwiZ2V0QWxsUmVzcG9uc2VIZWFkZXJzIiwidGFyZ2V0T3ZlcnJpZGUiLCJyZXR1cm5Qcm9taXNlIiwicmVzb2x2ZWRUYXJnZXQiLCJzd2FwT3ZlcnJpZGUiLCJoaWVyYXJjaHlGb3JFbHQiLCJ2ZXJpZnlQYXRoIiwicmVxdWVzdENvbmZpZyIsInNhbWVIb3N0Iiwib3JpZ2luIiwib2JqIiwiQmxvYiIsImZvcm1EYXRhQXJyYXlQcm94eSIsImFycmF5IiwiUHJveHkiLCJhcHBseSIsImFyZ3VtZW50cyIsImluZGV4IiwiUmVmbGVjdCIsImZyb21FbnRyaWVzIiwiZGVsZXRlUHJvcGVydHkiLCJvd25LZXlzIiwiZ2V0T3duUHJvcGVydHlEZXNjcmlwdG9yIiwicHJvcCIsImV0YyIsImNvbmZpcm1lZCIsInJlc29sdmUiLCJyZWplY3QiLCJQcm9taXNlIiwicHJvbWlzZSIsIl9yZXNvbHZlIiwiX3JlamVjdCIsInJlc3BvbnNlSGFuZGxlciIsImhhbmRsZUFqYXhSZXNwb25zZSIsImVsdERhdGEiLCJzdWJtaXR0ZXIiLCJidXR0b25QYXRoIiwiYnV0dG9uVmVyYiIsImNvbmZpcm1RdWVzdGlvbiIsImlzc3VlUmVxdWVzdCIsInNraXBDb25maXJtYXRpb24iLCJjb25maXJtRGV0YWlscyIsInRyaWdnZXJpbmdFdmVudCIsInF1ZXN0aW9uIiwic3luY0VsdCIsInN5bmNTdHJhdGVneSIsInF1ZXVlU3RyYXRlZ3kiLCJhYm9ydGFibGUiLCJzeW5jU3RyaW5ncyIsInF1ZXVlU3RyQXJyYXkiLCJxdWV1ZWRSZXF1ZXN0cyIsImVuZFJlcXVlc3RMb2NrIiwicXVldWVkUmVxdWVzdCIsInByb21wdFF1ZXN0aW9uIiwicHJvbXB0UmVzcG9uc2UiLCJjb25maXJtIiwicmF3Rm9ybURhdGEiLCJhbGxGb3JtRGF0YSIsImZpbHRlcmVkRm9ybURhdGEiLCJyZXF1ZXN0QXR0clZhbHVlcyIsImVsdElzQm9vc3RlZCIsInVzZVVybFBhcmFtcyIsInBhcmFtZXRlcnMiLCJ1bmZpbHRlcmVkRm9ybURhdGEiLCJ1bmZpbHRlcmVkUGFyYW1ldGVycyIsImNyZWRlbnRpYWxzIiwic3BsaXRQYXRoIiwicGF0aE5vQW5jaG9yIiwiZmluYWxQYXRoIiwiaGFzVmFsdWVzIiwibmV4dCIsImRvbmUiLCJ0b1VwcGVyQ2FzZSIsIm92ZXJyaWRlTWltZVR5cGUiLCJub0hlYWRlcnMiLCJyZXNwb25zZUluZm8iLCJwYXRoSW5mbyIsInJlcXVlc3RQYXRoIiwiZmluYWxSZXF1ZXN0UGF0aCIsInJlc3BvbnNlUGF0aCIsImhpZXJhcmNoeSIsImtlZXBJbmRpY2F0b3JzIiwiZGlzYWJsZUVsdHMiLCJzZWNvbmRhcnlUcmlnZ2VyRWx0IiwicGFyZW50RWx0SW5IaWVyYXJjaHkiLCJvbmVycm9yIiwib25hYm9ydCIsIm9udGltZW91dCIsInVwbG9hZCIsImxlbmd0aENvbXB1dGFibGUiLCJ0b3RhbCIsInBhcmFtcyIsImRldGVybWluZUhpc3RvcnlVcGRhdGVzIiwicGF0aEZyb21IZWFkZXJzIiwidHlwZUZyb21IZWFkZXJzIiwicHVzaFVybCIsInJlcGxhY2VVcmwiLCJlbGVtZW50SXNCb29zdGVkIiwic2F2ZVR5cGUiLCJjb2RlTWF0Y2hlcyIsInJlc3BvbnNlSGFuZGxpbmdDb25maWciLCJyZWdFeHAiLCJSZWdFeHAiLCJyZXNvbHZlUmVzcG9uc2VIYW5kbGluZyIsInJlc3BvbnNlSGFuZGxpbmdFbGVtZW50IiwidGl0bGVFbHQiLCJyZXNwb25zZUluZm9TZWxlY3QiLCJyZWRpcmVjdFBhdGgiLCJyZWRpcmVjdFN3YXBTcGVjIiwidGhlbiIsInNob3VsZFJlZnJlc2giLCJoaXN0b3J5VXBkYXRlIiwiaXNFcnJvciIsInNlbGVjdE92ZXJyaWRlIiwiZmFpbGVkIiwic3VjY2Vzc2Z1bCIsInRyYW5zZm9ybVJlc3BvbnNlIiwic2V0dGxlUmVzb2x2ZSIsInNldHRsZVJlamVjdCIsImRvU3dhcCIsImZpbmFsRWx0Iiwic2hvdWxkVHJhbnNpdGlvbiIsInN0YXJ0Vmlld1RyYW5zaXRpb24iLCJzZXR0bGVQcm9taXNlIiwiaW5uZXJEb1N3YXAiLCJleHRlbnNpb25CYXNlIiwiaW5pdCIsImFwaSIsInRleHQiLCJleHRlbnNpb25zVG9SZXR1cm4iLCJleHRlbnNpb25zVG9JZ25vcmUiLCJleHRlbnNpb25zRm9yRWxlbWVudCIsImV4dGVuc2lvbk5hbWUiLCJpc1JlYWR5IiwiZm4iLCJyZWFkeVN0YXRlIiwiaW5zZXJ0SW5kaWNhdG9yU3R5bGVzIiwibm9uY2VBdHRyaWJ1dGUiLCJoZWFkIiwiZ2V0TWV0YUNvbmZpZyIsIm1lcmdlTWV0YUNvbmZpZyIsIm1ldGFDb25maWciLCJyZXN0b3JlZEVsdHMiLCJhYm9ydCIsIm9yaWdpbmFsUG9wc3RhdGUiLCJvbnBvcHN0YXRlIiwiYmluZCIsInN0YXRlIiwicmVxdWlyZSIsInByaW50Il0sInNvdXJjZVJvb3QiOiIifQ==