"""_____________________________________________________________________

:PROJECT: LARAsuite

*lara_django barcode_generator*

:details: lara_django barcode_generator.
         - 
:authors: mark doerr <mark.doerr@uni-greifswald.de>

.. note:: -
.. todo:: - 
________________________________________________________________________
"""


def generate_barcode1D(self, barcode_format="code128", barcode_height=50, barcode_width=1, barcode_quiet_zone=10, 
                        barcode_human_readable=True, barcode_background_color="white", barcode_foreground_color="black",
                        barcode_font_size=10, barcode_font_path=None, barcode_text=False, barcode_text_distance=5, 
                        barcode_text_position="bottom",
                        output_format="png"):
    """generates a 1D barcode for the entity,
    e.g. for printing on a badge or for scanning
    """
    pass

def generate_barcode2D(self, barcode_format="qrcode", barcode_error_correction="L", barcode_box_size=10,
                        barcode_border=4, barcode_version=None, barcode_mask_pattern=None, barcode_image_factory=None,
                        barcode_background_color="white", barcode_foreground_color="black", barcode_text=False,
                        barcode_text_distance=5, output_format="png"):
    """generates a 2D barcode for the entity,
    e.g. for printing on a badge or for scanning
    """
    pass

