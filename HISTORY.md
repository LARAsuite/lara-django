
# History  of lara_django 


* 0.2.20 (2023-04-24)


* First release ....

v0.2.43-1-g110aff9
------------------

Features:

 - 110aff9 Tools documentation added

v0.2.46
-------

Bug fixes:

 - de20b50 App versions
 - f210ce9 Wheel added to .gitlab-ci.yml

Other changes:

 - cb5f9ee Bump version: 0.2.45 → 0.2.46

v0.2.99
-------

Features:

 - dbe621a W3id.org/lara

Bug fixes:

 - a7bb7a1 Lara app versions updated;
 - a18940d LARA uri prefixes; LARAServer -> LARASyncServer
 - b53a1f1 Requirements_base.txt -> base.txt
 - b47ae46 W3c.org -> w3id.org; apps.py path_url

Other changes:

 - 2c5e808 Bump version: 0.2.98 → 0.2.99

