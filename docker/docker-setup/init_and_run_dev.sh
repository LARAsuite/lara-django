#!/usr/bin/env bash
#cd /opt/lara-django
#python3 manage.py wait_for_db
#python3 manage.py migrate || { echo 'migrate failed' ; exit 1; }
#python3 manage.py collectstatic
#python3 manage.py init_admin
#/usr/local/bin/uwsgi --ini /etc/uwsgi/apps-enabled/uwsgi-app.ini
#gunicorn hellodjango3.wsgi:application --bind 0.0.0.0:8000

./lara-django-dev init_django
# ./lara-django-dev collectstatic --noinput
./lara-django-dev runserver 0.0.0.0:8008 & lara-django-dev grpcrunaioserver --dev & celery -A lara_django worker -l info 
