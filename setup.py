

"""_____________________________________________________________________

:PROJECT: LARA

*lara_django setup *

:details: LARA-django is a python django project of the Lab Automation Suite LARA - (lara.uni-greifswald.de/larasuite)
         - For installation, run:
           run pip3 install .
           or  python3 setup.py install

:authors: mark doerr mark.doerr@uni-greifswald.de

:date: (creation)         2019-10-16

.. note:: -
.. todo:: -
________________________________________________________________________
"""

import os
import re

from setuptools import setup, find_packages

# allow setup.py to be run from any path
os.chdir(os.path.normpath(os.path.join(os.path.abspath(__file__), os.pardir)))

dir_path = os.path.dirname(os.path.realpath(__file__))

with open(os.path.join(dir_path, "VERSION"), "r") as version_file:
    version = str(version_file.readline()).strip()

package_name = 'lara_django'

package_data = {package_name: [
    'fixtures/*.json', f'templates/{package_name}/*.html', 'static/icons/*.svg',
    'static/css/*.css', 'static/sass/*.scss', 'static/js/*.js', '*.json',
]}

setup(
    name=package_name,
    version=version,
    packages=find_packages(),
    include_package_data=True,
    # package_data=package_data,
    scripts=['lara_django/bin/lara-django',
               'lara_django/bin/lara-django-dev'],

)
