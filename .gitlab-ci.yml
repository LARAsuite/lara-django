# Config file for GitLab CI pipeline

image: python:latest

# Change pip's cache directory to be inside the project directory since we can
# only cache local items.
variables:
    PIP_CACHE_DIR: "$CI_PROJECT_DIR/.cache/pip"
    SOURCE_DIR: "$CI_PROJECT_DIR/lara_django"
    DOCS_DIR: "docs"
    DOCS_SOURCE_DIR: "docs/source"
    DOCS_BUILD_DIR: "docs/_build"

stages:
    - build
    - compliance
    - test
    - build_and_publish_docs
    - publish_pypi

# Configuration --------------------------------------------------------------------

# Pip's cache doesn't store the python packages
# https://pip.pypa.io/en/stable/reference/pip_install/#caching
#
# If you want to also cache the installed packages, you have to install
# them in a virtualenv and cache it as well.
cache:
    paths:
        - .cache/pip
        - venv/

before_script:
    - python -V # Print out python version for debugging
    - pip install virtualenv
    - virtualenv venv
    - source venv/bin/activate
    - pip install setuptools wheel>=0.40.0

.parallel_python_jobs:
    parallel:
        matrix:
            - python_version: ["3.9", "3.10", "3.11"]

.run_on_all_branches:
    only:
        refs:
            - develop
            - merge_requests
            - triggers
            - master
            - main
            - tags
            - /^v[0-9]+.[0-9]+.*/

.run_on_master:
    only:
        refs:
            - triggers
            - master
            - main

.run_on_release_tag:
    only:
        refs:
            - tags
            - /^v[0-9]+.[0-9]+.*/

.run_on_manual:
    only:
        refs:
            - manual

# Jobs: Build -----------------------------------------------------------------

build-package:
    stage: build
    image: python:latest
    script:
        - pip install twine build hatchling
        #- python -m build --wheel 
        - python setup.py bdist_wheel
        - TWINE_PASSWORD=${CI_JOB_TOKEN} TWINE_USERNAME=gitlab-ci-token python -m twine upload --verbose --repository-url ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/pypi dist/*
    only:
        - master
        - tags

# --- docker ----
docker-build-buildimage:
  # Official docker image.
  image: docker:latest
  stage: build
  services:
    - docker:dind
  variables:
    # Use TLS https://docs.gitlab.com/ee/ci/docker/using_docker_build.html#tls-enabled
    #DOCKER_HOST: tcp://docker:2376
    #DOCKER_TLS_CERTDIR: "/certs"
    #CONTAINER_TEST_IMAGE: $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG
    CONTAINER_BUILD_IMAGE_DEV: $CI_REGISTRY_IMAGE/lara-django-dev:${CI_COMMIT_SHORT_SHA}
    CONTAINER_BUILD_IMAGE_DEV_LATEST: $CI_REGISTRY_IMAGE/lara-django-build-dev:latest

  before_script:
    - docker login -u $CI_REGISTRY_USER -p $CI_JOB_TOKEN $CI_REGISTRY
  script:
    - VERSION=`cat VERSION`
    - CONTAINER_BUILD="$CI_REGISTRY_IMAGE/labdata-aggregator-build-dev:"
    - CONTAINER_BUILD_IMAGE_DEV_VER=$CONTAINER_BUILD$VERSION
    - cd docker
    # development image
    #- docker pull "$DOCKER_CACHE" || true  # Pull the cache image if it exists
    - docker build --pull -f Dockerfile.build.dev --build-arg BUILDKIT_INLINE_CACHE=1 --cache-from $CI_REGISTRY_IMAGE:latest -t "$CONTAINER_BUILD_IMAGE_DEV" -t "$CONTAINER_BUILD_IMAGE_DEV_VER" --tag $CONTAINER_BUILD_IMAGE_DEV_LATEST . 
    - docker push "$CONTAINER_BUILD_IMAGE_DEV_LATEST"
  only:
    refs:
      - develop
    #   - feature/docker-build

docker-build-lara-django-master:
  # Official docker image.
  image: docker:latest
  stage: build
  services:
    - docker:dind
  variables:
    # Use TLS https://docs.gitlab.com/ee/ci/docker/using_docker_build.html#tls-enabled
    #DOCKER_HOST: tcp://docker:2376
    #DOCKER_TLS_CERTDIR: "/certs"
    #CONTAINER_TEST_IMAGE: $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG
    DOCKER_BUILD: $CI_REGISTRY_IMAGE/lara-django-build-dev:latest
    CONTAINER_RELEASE_IMAGE: $CI_REGISTRY_IMAGE/lara-django:${CI_COMMIT_SHORT_SHA}
    CONTAINER_RELEASE_IMAGE_DEV: $CI_REGISTRY_IMAGE/lara-django-dev:${CI_COMMIT_SHORT_SHA}
    CONTAINER_RELEASE_IMAGE_DEV_LATEST: $CI_REGISTRY_IMAGE/lara-django-dev:latest
    

  before_script:
    #- docker login -u $CI_JOB_USER -p $CI_JOB_TOKEN $CI_REGISTRY
    - docker login -u $CI_REGISTRY_USER -p $CI_JOB_TOKEN $CI_REGISTRY
    #- echo $CI_BUILD_TOKEN | docker login -u "$CI_REGISTRY_USER" --password-stdin $CI_REGISTRY
  script:
    - VERSION=`cat VERSION`
    - CONTAINER_RELEASE="$CI_REGISTRY_IMAGE/lara-django-dev:"
    - CONTAINER_RELEASE_IMAGE_DEV_VER=$CONTAINER_RELEASE$VERSION
    - cd docker
    # development image
    #- docker build --pull -f Dockerfile.dev -t "$CONTAINER_RELEASE_IMAGE_DEV" -t "$CONTAINER_RELEASE_IMAGE_DEV_VER" .
    - docker build --pull -f Dockerfile.dev --build-arg BUILDKIT_INLINE_CACHE=1 --cache-from $CI_REGISTRY_IMAGE:latest -t "$CONTAINER_RELEASE_IMAGE_DEV" -t "$CONTAINER_RELEASE_IMAGE_DEV_VER" --tag $CONTAINER_RELEASE_IMAGE_DEV_LATEST .
    - docker push "$CONTAINER_RELEASE_IMAGE_DEV"
    - docker push "$CONTAINER_RELEASE_IMAGE_DEV_LATEST"
    # production image
    #- docker build --pull -f Dockerfile -t "$CONTAINER_RELEASE_IMAGE" .
    #- docker push "$CONTAINER_RELEASE_IMAGE"
  only:
    - master 
    - tags
    #- manual

docker-build-nginx-master:
  # Official docker image.
  image: docker:latest
  stage: build
  services:
    - docker:dind
  variables:
    # Use TLS https://docs.gitlab.com/ee/ci/docker/using_docker_build.html#tls-enabled
    #DOCKER_HOST: tcp://docker:2376
    #DOCKER_TLS_CERTDIR: "/certs"
    #CONTAINER_TEST_IMAGE: $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG
    CONTAINER_NGINX_RELEASE_IMAGE: $CI_REGISTRY_IMAGE/nginx-django:latest

  before_script:
    #- docker login -u $CI_JOB_USER -p $CI_JOB_TOKEN $CI_REGISTRY
    #- echo $CI_BUILD_TOKEN | docker login -u "$CI_REGISTRY_USER" --password-stdin $CI_REGISTRY
    - docker login -u $CI_REGISTRY_USER -p $CI_JOB_TOKEN $CI_REGISTRY
  script:
    - cd docker/nginx
    - docker build --pull -t "$CONTAINER_NGINX_RELEASE_IMAGE" .
    - docker push "$CONTAINER_NGINX_RELEASE_IMAGE"
  only:
    #- master
    - manual


# build:
#   stage: build
#   script:
#     - PYTHON_VERSION=${python_version} PYPI_URL=${pip_repository_index} make pull
#     - PYTHON_VERSION=${python_version} PYPI_URL=${pip_repository_index} make build
#     - PYTHON_VERSION=${python_version} PYPI_URL=${pip_repository_index} make push
#   extends:
#     - .docker_login
#     - .run_on_all_branches
#     - .parallel_python_jobs

# Jobs: Compliance ------------------------------------------------------------

#compliance:
#    stage: compliance
#    script:
#        - poetry run invoke format --check
#        - poetry run invoke lint
#        - poetry run invoke security-bandit
#    extends:
#        - .run_on_all_branches
#        - .parallel_python_jobs
#        - .use_generated_docker_image

# Jobs: Test ------------------------------------------------------------------

# ============================= TEST ========================
test:
    script:
        #- pip install tox flake8
        #- tox -e py39,flake8
        - pip install pytest
        - pytest
    extends:
        #- .run_on_all_branches
        #- .parallel_python_jobs
        - .run_on_manual

# Jobs: Docs -----------------------------------------------------------------------

pages:
    stage: build_and_publish_docs
    script:
        - pip install sphinx myst_parser python_docs_theme
        - pip install .
        #- cd docs ; make html
        - sphinx-apidoc -e -P -o $DOCS_SOURCE_DIR $SOURCE_DIR
        - sphinx-build -b html $DOCS_DIR $DOCS_BUILD_DIR
        - mv $DOCS_BUILD_DIR public
    artifacts:
        paths:
            - public
    extends:
        - .run_on_master

# Jobs: Publish ---------------------------------------------------------------

release_pypi:
    variables:
        python_version: "3.9"
    stage: publish_pypi
    script:
        - |
            poetry run invoke release-twine \
              --tag_name="${tag_name}" \
              --pypi_user="${pypi_user}" \
              --pypi_pass="${pypi_pass}" \
              --pypi_publish_repository="${pypi_publish_repository}" \
              --pip_repository_index="${pip_repository_index}"
    extends:
        #- .run_on_release_tag
        - .run_on_manual
        #- .use_generated_docker_image
