#!/usr/bin/env python3
# vim:fileencoding=utf-8
"""
________________________________________________________________________

:PROJECT: 

*brief summary*

:details: :

          - python manage.py dumpdata lara.Currency --indent 2 --natural-primary > /tmp/1_curr_fix.json

:file:    currency_JSON_generator.py

:author:  mark doerr <mark@MicT660p> : contrib.

:version: 0.0.1

:date: (creation)          20190704
:date: (last modification) 20190704
.. note:: some remarks
.. todo:: -

________________________________________________________________________

**Copyright**:
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
  This file is provided "AS IS" with NO WARRANTY OF ANY KIND,
  INCLUDING THE WARRANTIES OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
  For further Information see COPYING file that comes with this distribution.
________________________________________________________________________

"""

import os
import json

import pycountry as pc


def genCurrencyJSON(lara_model = "lara.currency", 
                    fix_filename_full = ""):
    """ :param lara_model: this refers to the database table 
        :param fix_filename_full: fixture output file in JSON format
    """
    
    # loading country JSON db from pycountry
    
    country_file = pc.countries.filename
    
    with open(country_file, 'r') as f:
        country_dic = json.load(f)
        
    country_list = country_dic['3166-1']
    
    # loading currency JSON db from pycountry
        
    currency_JSON_filename = pc.currencies.filename
    
    with open(currency_JSON_filename, 'r') as f:
        curr_dic = json.load(f)
        
    curr_list = curr_dic['4217']
    
    # currency symbol dict
    curr_symb = {'EUR': '\u20ac',  } #, 'CEN':'\u00a2',  'CHF':'sF',
     #~ 'BP':'\u00a3', 'USD':'\u0024',
     #~ 'JPY':'\u00a5', '':'\u', 'AUS':'\u20b3',
     #~ 'RUP':'\u20B9', '':'\u', '':'\u', '':'\u',
     #~ 'RUB':'\u20BD', '':'\u', '':'\u', '':'\u',
     #~ '':'\u', '':'\u', '':'\u', '':'\u',
    
    # exchange rate dictionary (could be retrieved from internet)
    curr_EUR = {'USD':0.87,  'YEN':0.0082,  } # 'YUA':0.13, 'CHF':0.89  
    
    i = 1
    curr_lst = []
    curr_idx_dic = {}
    for currency in curr_list:            
        currency['currency'] = currency.pop('name')
        currency['alpha3code'] = currency.pop('alpha_3')
        currency['numeric_code'] = currency.pop('numeric')
        try: 
            currency['symbol'] = curr_symb[ currency['alpha3code'] ]
        except KeyError:
            currency['symbol'] = "u\00a4"
            
        try:
            currency['exchange_rate_EUR'] = curr_EUR[ currency['alpha3code'] ]
        except KeyError as err:
            currency['exchange_rate_EUR'] = 1.0
        
        cur_fix = dict( model=lara_model, pk=i, fields=currency )
        curr_lst.append(cur_fix)
        
        i += 1
        
    #~ print(json.dumps(curr_lst, indent=2) )
    
    # write currency fixture file
    with open( fix_filename_full, 'w') as f: json.dump(curr_lst, f, indent=2)
    
if __name__ == '__main__':
    """Main: """
            
    fixture_filename = '1_currency_fix.json'
    
    root_path = "/" # OS dependent
    fixture_target_path = os.path.join( root_path, 'tmp','fixtures')
    
    try:  
        os.makedirs(fixture_target_path)
    except OSError:  
        print("Creation of the directory %s failed - does it already exist ?" % fixture_target_path)
    else:  
        print("Successfully created the directory %s" % fixture_target_path )
            
    fix_filename_full = os.path.join( fixture_target_path, fixture_filename )
    
    genCurrencyJSON(fix_filename_full=fix_filename_full)
    
