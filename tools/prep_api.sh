#! /bin/bash
file_list=( \
#"lara-django-base" \
#"lara-django-people" \
#"lara-django-library" \
#"lara-django-material" \
"lara-django-material-store"  \
"lara-django-samples" \
"lara-django-sequences" \
"lara-django-substances" \
"lara-django-substances-store" \
"lara-django-organisms" \
"lara-django-organisms-store" \
#"lara-django-data" \
"lara-django-processes" \
"lara-django-projects" )


cd $(pwd)

for i in ${file_list[*]}
do
   echo --- $i ----:
   cp -R lara-django-data/api $i
   cd $i
   ls api
   cd ..
   echo 
done
