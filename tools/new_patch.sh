#! /bin/bash

# This script is used to create a new patch for the LARA suite.
# The patch is created in the current directory.

# show help message --help or -h

file=$2

if [ "$1" == "-h" ] || [ "$1" == "--help" ]
  then
    echo "Usage: make_new_patch.sh [-a]"
    echo "  -a: generate gRPC API"
    exit 0
fi

# check, if  argument -a for generating gRPC API is given
if [ "$1" == "-a" ]
  then
    echo cp  $LARA_SRC/lara-django/protos/${file//[-]/_}.proto $LARA_SRC/$file/api/proto/${file//[-]/_}_grpc/v1
    cp  $LARA_SRC/lara-django/protos/${file//[-]/_}.proto $LARA_SRC/$file/api/proto/${file//[-]/_}_grpc/v1    
    cd api && \
    echo "generating gRPC API" in $(pwd) && \
    /home/linuxbrew/.linuxbrew/bin/buf generate proto && \ 
    cd ..
    git commit -am "fix: gRPC API updated"
fi

bumpversion  --verbose patch && \
changelog.sh >> HISTORY.md && \
git commit -am "fix: HISTORY.md" && \
git flow feature publish && git push --tags  && \
#git push && git push --tags  && \
cat HISTORY.md && \
cat VERSION                                                                                                                         
                                                                                                                                       
                                                                                                                                            
  