# cloning all lara repositories

git clone git@gitlab.com:LARAsuite/lara-django.git
git clone git@gitlab.com:LARAsuite/lara-django-base.git
git clone git@gitlab.com:LARAsuite/lara_library.git
git clone git@gitlab.com:LARAsuite/lara-django-grpc.git
git clone git@gitlab.com:LARAsuite/lara-django-people.git

git clone git@gitlab.com:LARAsuite/lara-django-material.git
git clone git@gitlab.com:LARAsuite/lara-django-library.git
git clone git@gitlab.com:LARAsuite/lara-django-parts.git
git clone git@gitlab.com:LARAsuite/lara-django-devices.git
git clone git@gitlab.com:LARAsuite/lara-django-sequences.git
git clone git@gitlab.com:LARAsuite/lara-django-substances.git
git clone git@gitlab.com:LARAsuite/lara-django-substances-store.git
git clone git@gitlab.com:LARAsuite/lara-django-containers.git

git clone git@gitlab.com:LARAsuite/lara-django-data.git
git clone git@gitlab.com:LARAsuite/lara-django-processes.git
git clone git@gitlab.com:LARAsuite/lara-django-projects.git

#~ git clone git@gitlab.com:LARAsuite/lara_python_qt5 
#~ git clone git@gitlab.com:LARAsuite/lara-simpleprocman 
