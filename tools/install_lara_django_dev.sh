#!/bin/bash

# This script is install a local development version of the LARA suite.

LARASUITE_GIT_REPO="https://gitlab.com/larasuite"

APP_list=( \
"lara-django" \
 "lara-django-base" \
"lara-django-people" \
"lara-django-library" \
"lara-django-material" \
"lara-django-store" \
"lara-django-material-store"  \
"lara-django-samples" \
"lara-django-sequences" \
"lara-django-substances" \
"lara-django-substances-store" \
"lara-django-organisms" \
"lara-django-organisms-store" \
"lara-django-data" \
"lara-django-processes" \
"lara-django-projects" \
)

POSITIONAL_ARGS=()

LARA_LOGO="Welcome to the
             __      __    ____    __                  
            (  )    /__\  (  _ \  /__\                 
             )(__  /(__)\  )   / /(__)\                
            (____)(__)(__)(_)\_)(__)(__)               
                                                        
                development installation
          _______________________________________
          
      "

HELP="Usage: install_lara_django_dev.sh[-a] [-i] [-h] 
        -a | --app-list : <file list of apps> 
        -i | --install-dev
        -h | --help : show this help message 
        This script will generate a python virtual development environment for LARA and
        install a local development version of the LARA suite in the current directory. "


# if no arguments are given, show help message
if [ $# -eq 0 ]
  then
    printf "$LARA_LOGO"
    printf "$HELP"
    exit 0
fi

# parse arguments
while [[ $# -gt 0 ]]; do
  case $1 in
    -a | --app-list)
      APP_LIST=true
      shift # past argument
      shift # past value
      ;;
    -i | --install-dev)
      shift # past argument
       printf "$LARA_LOGO"
      INSTALL_DEV=true
      ;;
    -h|--help)
      # show help message --help or -h
       printf "$LARA_LOGO"
        printf "$HELP"
        exit 0
      shift # past argument
      #shift # past value
      ;;
    # --default)
    # DEFAULT=YES
    # shift # past argument
    # ;;
    *)    # unknown option
      POSITIONAL_ARGS+=("$1") # save it in an array for later
      shift # past argument
      ;;
  esac
done

#if APP_LIST is true, show the list of apps
if [ "$APP_LIST" == "true" ]; then
  printf "The following apps will be installed: \n"
  for file in "${APP_list[@]}"; do
    echo $file
  done
  exit 0
fi

# test, if git-lfs is installed
if ! command -v git-lfs &> /dev/null
then
    echo "git-lfs could not be found, please install it first"
    exit
fi

# ask, if user, which python venv to be used for installation

read -p "Do you have a python virtual environment for the installation? (y/n) " yn
# if yes, ask for the path

if [ "$yn" == "y" ]; then
  read -p "Please enter the path to the LARA development python venv: " venv_path
  source $venv_path/bin/activate
fi
# if no, create a new venv, but ask for the path

if [ "$yn" == "n" ]; then
  read -p "Please enter a new path/venv-name to the LARA development python virtual environment 
           - if empty, a venv "lara-dev-venv" is generated in the current directory: " venv_path
  if [ -z "$venv_path" ]; then
    venv_path="lara-dev-venv"
  fi
  python3 -m venv $venv_path
  source $venv_path/bin/activate
fi


# clone all LARA git repos
for file in "${APP_list[@]}"; do

  # first check, if the repo is already cloned
  if [ -d "$file" ]; then
    echo "$file already cloned"
    continue
  fi
  git clone $LARASUITE_GIT_REPO/$file.git
done

# install all LARA apps in development mode
pip install --upgrade pip
# install all LARA development dependencies
pip install -r lara-django/requirements/develop.txt

# install all LARA apps and the gRPC interface in development "edit" mode (pip install -e .)

echo "Installing the following apps in development mode: "
pwd

# remove first element from array, since lara-django has no api/grpc/python dir
APP_list=("${APP_list[@]:1}")

for file in "${APP_list[@]}"; do
  pwd
  cd $file
  git pull
  git checkout feature/unittests
  git pull
  pip install -e ".[dev]"
  cd api/grpc/python
  pwd
  pip install -e .
  cd ../../../..
  pwd
done

# hint on how to set environment variables for the LARA suite development
# init the django LARA database and create a superuser: 
source lara-django/tools/envs-dev
lara-django-dev init_django 
lara-django-dev collectstatic

# ask, if to write current dir as $LARA_SRC to ~/.bashrc file

read -p "Do you wish to write the current directory as \$LARA_SRC to ~/.bashrc? (y/n) " yn
# if yes, write to ~/.bashrc
if [ "$yn" == "y" ]; then
  echo "export LARA_SRC=$(pwd)" >> ~/.bashrc
fi

echo -e "For the LARA suite development, modify the env-dev file according to your local setup and then source it:
      source lara-dev-env\n"

echo The LARA development environment is installed in $(pwd), venv: $venv_path

echo -e "\nTo start the LARA suite, run the following commands:
      source lara-dev-env
      lara-django-dev runserver\n"

echo -e "Mind to run the additional services, like virtuoso, redis, celery, etc. 
      with the docker-compose.dev.local.yml file as described in the LARA documentation:
      https://gitlab.com/LARAsuite/lara-django\n" 

echo "Happy experimenting with the LARA suite ;)"
