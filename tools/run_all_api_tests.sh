#!/bin/bash

# This script is used to run all low level API tests for all django apps of the LARA suite.

file_list=( \
#  "lara-django" \
 "lara-django-base" \
 "lara-django-people" \
 "lara-django-material" \
 "lara-django-sequences" \
 "lara-django-material-store"  \
 "lara-django-samples" \
 "lara-django-substances" \
 "lara-django-substances-store" \
 "lara-django-organisms" \
 "lara-django-organisms-store" \
 "lara-django-data" \
"lara-django-processes" \
 "lara-django-projects" \

#  "lara-django-store" \
#  "lara-django-library" \
)

run_api_unittests () {
  echo __________________________ $file _________________________
  app_name=${file//[-]/_}
  #app_dir=${file//[-]/_}
   cd $file
     echo ---- pytest --durations=0 -s --host localhost --port 50051 --num-instances 3 api/tests ---
     pytest --durations=0 -s --num-instances 3 api/tests
   cd ..
 
   printf "\n\n"
}

# concatenate all the files in the list and pass them to pytest as arguments
#pytest --durations=0 -s --num-instances $1 $(for file in ${file_list[*]}; do echo $file/api/tests; done)

for file in ${file_list[*]}
do 
   run_api_unittests $file  
done

