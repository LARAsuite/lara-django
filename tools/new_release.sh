#!/bin/bash

# This script is used to start a new release for all django apps of the LARA suite.

APP_list=( 
"lara-django-base" \
"lara-django-people" \
"lara-django-store" \
"lara-django-material" \
"lara-django-material-store"  \
"lara-django-samples" \
"lara-django-sequences" \
"lara-django-substances" \
"lara-django-substances-store" \
"lara-django-organisms" \
"lara-django-organisms-store" \
"lara-django-data" \
"lara-django-processes" \
"lara-django-projects" \
# "lara-django-library" \
)

POSITIONAL_ARGS=()

HELP="Usage: new_release.sh[-r] [-f] [-h] 
  -a | --app-list : <file list of apps> 
  -r | --release : <release number, e.g. 0.1.0> 
  -f | --finish-release
  -h | --help : show this help message "

# if no arguments are given, show help message
if [ $# -eq 0 ]
  then
    printf "$HELP"
    exit 0
fi


while [[ $# -gt 0 ]]; do
  case $1 in
    -r|--release)
      RELEASE="$2"
      shift # past argument
      shift # past value
      ;;
    -a | --app-list)
      APP_LIST="$2"
      shift # past argument
      shift # past value
      ;;
    -f | --finish-release)
      shift # past argument
      FINISH_RELEASE=true
      ;;
    -h|--help)
      # show help message --help or -h
        printf "$HELP"
        exit 0
      shift # past argument
      #shift # past value
      ;;
    # --default)
    #   DEFAULT=YES
    #   shift # past argument
    #   ;;
    -*|--*)
      echo "Unknown option $1"
        printf "$HELP"
      exit 1
      ;;
    *)
      POSITIONAL_ARGS+=("$1") # save positional arg
      shift # past argument
      ;;
  esac
done

set -- "${POSITIONAL_ARGS[@]}" # restore positional parameters

if [ -z "$RELEASE" ]
  then
    echo "No release number given. Use -r or --release option."
    printf "$HELP"
    exit 1
fi

if [ -z "$APP_LIST" ]
  then
    echo "No file list given, using the internal list. Use -f or --app-list option."    
# else generate file list from file list file
  else
    echo "Using file list from file $APP_LIST"
    APP_list=($(cat $APP_LIST))
fi

echo "RELEASE = ${RELEASE}"
echo "FILE LIST = ${APP_LIST}"

# if finish release is given, iterate over content of file list and finish release
#$(cat $APP_LIST)

if [ "$FINISH_RELEASE" = true ] 
  then
    for app in ${APP_list[*]} 
    do
      echo __________________________ $app _________________________
      cd $app
      # bumpversion -n --verbose --current-version 0.2.1 --new-version 0.2.33 patch
      # !! mind data
      git commit -am "fix: preparing release ${RELEASE}"
      bumpversion --verbose --no-tag --new-version ${RELEASE} patch
      changelog.sh >> HISTORY.md && \
      git commit -am "fix: HISTORY.md" && \
      git flow release finish ${RELEASE}

      printf "git flow release finish \"${RELEASE}\" \n"
     cd ..
    done
    exit 0
fi

# iterate over content of file list and start release

for app in ${APP_list[*]}
do
  cd $app
  echo "git flow feature finish"
  git flow release start ${RELEASE}
  git flow release publish
  
  cd ..
done