#!/bin/bash

# This script is used to start a new release for all django apps of the LARA suite.

file_list=( \
#  "lara-django" \
 "lara-django-base" \
"lara-django-people" \
#  "lara-django-material" \
#  "lara-django-material-store"  \
#  "lara-django-samples" \
#  "lara-django-substances" \
#  "lara-django-substances-store" \
#  "lara-django-organisms" \
#  "lara-django-organisms-store" \
#  "lara-django-data" \
#  "lara-django-processes" \
 "lara-django-projects" \
#  "lara-django-sequences" \
#  "lara-django-store" \
#  "lara-django-library" \
)

new_forms () {
  echo __________________________ $file _________________________
  app_dir=${file//[-]/_}
   cd $file/$app_dir
     pwd
     lara-django-dev forms_generator -c $app_dir > forms_temp.py && \
     meld forms_temp.py forms.py
     rm forms_temp.py
      # ask to proceed
     read -p "Do you wish to continue? (y/n) " yn
     # if no exit
     if [ "$yn" != "y" ]; then
       exit 0
     fi
     git commit -am "fix: forms.py updated"
   cd ../..
   printf "\n\n"
}

edit_file () {
  echo __________________________ $file _________________________
  app_dir=${file//[-]/_}
   cd $file/$app_dir
     pwd
      code .
      # ask to proceed
     read -p "Do you wish to continue? (y/n) " yn
     # if no exit
     if [ "$yn" != "y" ]; then
       exit 0
     fi
     cd ..
     git commit -am "fix: filters.py; forms.py;"
     $LARA_SRC/lara-django/tools/new_patch.sh -a
   cd ..
   printf "\n\n"
}

commit_all () {
  echo __________________________ $file _________________________
  lara_app=${file//[-]/_}
   cd $file
     pwd
      #code .
      # ask to proceed
    #  read -p "Do you wish to continue? (y/n) " yn
    #  # if no exit
    #  if [ "$yn" != "y" ]; then
    #    exit 0
    #  fi
    #  cd ..
    # git commit -am "fix: literature->resources_external; handle->PID; create & update view;"

    # git commit -am "fix: intermediate commit - models.py; forms.py; admin.py; filters.py;"

    # bumpversion  --verbose patch && \
    # changelog.sh >> HISTORY.md && \
    # git commit -am "fix: HISTORY.md" && \
    # git flow feature publish && git push --tags  && \
    #git commit -am "fix: prepare patch 0.2.68"
    #bumpversion --verbose --new-version '0.2.68' patch
    #git commit -am "fix: more consistent, PEP8 compliant naming of fields and variables;"
    #git flow release publish && git push --tags  && \
    #git push && git push --tags  && \ 
    #cat HISTORY.md && \
    #cat VERSION                                                                                                                         
    git checkout develop
    git merge feature/$2
    git flow feature start rc-0.2.112                
   cd ..
   printf "\n\n"
}

create_new_patch () {
  echo __________________________ $file _________________________
  app_dir=${file//[-]/_}
  cp  $LARA_SRC/lara-django/protos/${file//[-]/_}.proto $file/api/proto/${file//[-]/_}_grpc/v1
   cd $file
    #  git commit -am "feat: decentralised url settings"
     $LARA_SRC/lara-django/tools/new_patch.sh -a $file
   cd ..
   printf "\n\n"
}

create_new_api () {
  echo __________________________ $file _________________________
  #app_dir=${file//[-]/_}
  cp  $LARA_SRC/lara-django/protos/${file//[-]/_}.proto $file/api/proto/${file//[-]/_}_grpc/v1
   cd $file/api 
    echo "generating gRPC API" in $(pwd) 
    buf generate proto
    cd ..
    git commit -am "fix: gRPC API updated"
   cd ..
   printf "\n\n"
}

create_new_unittests () {
  echo __________________________ $file _________________________
  app_name=${file//[-]/_}
  #app_dir=${file//[-]/_}
   cd $file/api 
    mkdir tests
    cd tests

    lara-django-dev generate_unittests_grpc --author-name "mark doerr" --author-email "mark.doerr@uni-greifswald.de"  $app_name

    cd ../..
    git add api/tests
    git commit -am "feat: new gRPC unittests added"
    git flow feature publish
   cd ..
   printf "\n\n"
}

create_new_schema () {
  echo __________________________ $file _________________________
  app_name=${file//[-]/_}
   cd $file
   #mkdir schemata
   cd schemata
    echo "generating LinkML Schema" in $(pwd)
    #rm *.yaml
    lara-django-dev generate_linkml_schema --author-name "mark doerr" --author-email "mark.doerr@uni-greifswald.de" --force $app_name
    lara-django-dev generate_yaml_import_template --author-name "mark doerr" --author-email "mark.doerr@uni-greifswald.de" --force $app_name
    #rm ../api/grpc/python/"$app_name"_data_model.py 
    gen-pydantic "$app_name"_schema.yaml > ../api/grpc/python/"$app_name"_grpc/"$app_name"_data_model.py 
    # gen-owl --format ttl "$app_name"_schema.yaml > "$app_name"_rdf_owl.ttl
    #git add *.yaml
    #git add *.ttl
    cd ../api/grpc/python/"$app_name"_grpc/
    # generate high level grpc interface
    #mv "$app_name"_interface.py "$app_name"_interface_241127.py
    lara-django-dev generate_high_level_interface --author-name "mark doerr" --author-email "mark.doerr@uni-greifswald.de"  $app_name
    #!git add *interface.py
    cd ../../../..
    #git commit -am "feat: new  high-level interface added"
    git commit -am "feat: new LinkML schema, template, data model generated and high-level interface added"
    #git commit -am "feat: new high-level interface generated"
    #git flow release publish && git push --tags 
    # git push
    #git checkout develop 
    #git merge feature/$2
    #git push 
    # git checkout feature/unittests
    # git merge develop
    # git push
    # git checkout release/0.2.66
    #echo git checkout feature/$2
   cd ..
   printf "\n\n"
   pwd
}

create_new_feature () {
  #create_new_feature $file grpc-server unittests
  echo __________________________ $file _________________________
  app_dir=${file//[-]/_}
   cd $file
     pwd
     # git checkout develop
     #echo git merge feature/$2
     # git push && git push --tags
     git flow feature start $2
     git flow feature publish
     #git checkout feature/$2
   cd ..
   printf "\n\n"
}

update_unittest_branch () {
  #create_new_feature $file grpc-server unittests
  echo __________________________ $file _________________________
  app_dir=${file//[-]/_}
   cd $file
     pwd
     git checkout develop
     git merge feature/$2
     git push
     git checkout feature/unittests
     git pull
     git merge develop
     git flow feature publish
     git checkout feature/$2
   cd ..
   printf "\n\n"
}

switch_2_branch () {
  #create_new_feature $file grpc-server unittests
  echo __________________________ $file _________________________
  app=${file//[-]/_}
   cd $file
     pwd
     git flow feature finish 
     git push && git push --tags
     # take branch name from input parameter
     #echo "$1" "$2" "$3"
     #echo git checkout feature/grpc-server
   cd ..
   printf "\n\n"
}


for file in ${file_list[*]}
do 
   #create_new_feature $file semantics-v1
   #update_unittest_branch $file
   #commit_all $file
  # commit_all $file semantics-v1

  # create_new_api $file

  #create_new_unittests $file
  # create_new_schema $file rc-0.2.112
  create_new_patch $file
   #-a
  # update_unittest_branch $file htmx-gui unittests
   #switch_2_branch $file
done

