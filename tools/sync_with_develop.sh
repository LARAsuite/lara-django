#!/bin/bash

echo "This script is used to update the unittest branch of all django apps of the LARAsuite...."

file_list=( \
"lara-django" \
"lara-django-base" \
"lara-django-people" \
"lara-django-material" \
"lara-django-material-store"  \
"lara-django-samples" \
"lara-django-substances" \
"lara-django-substances-store" \
"lara-django-organisms" \
"lara-django-organisms-store" \
"lara-django-data" \
"lara-django-processes" \
"lara-django-projects" \
"lara-django-sequences" \
"lara-django-store" \
"lara-django-library" \
)

update_unittest_branch () {
  #create_new_feature $file grpc-server unittests
  echo __________________________ $file _________________________
  lara_app=${file//[-]/_}
   cd $file
     pwd
   # check if there are uncommitted changes
     if [[ $(git status --porcelain=v1 --untracked-files no 2>/dev/null) ]]; then
         echo "There are uncommitted changes in $file"
         exit 1
     fi
     git checkout develop
        git pull --rebase
     git checkout feature/unittests
     git pull --rebase
        git merge develop
   cd ..
   printf "\n\n"
}

# check, if the lara-django directory exists

if [ ! -d "lara-django" ]; then
  echo "The directory lara-django cannot be found, please run this script in the LARA apps root directory."
  exit 1
fi

for file in ${file_list[*]}
do
   update_unittest_branch $file
done

export DJANGO_ALLOWED_HOSTS=localhost

# ask if the database should be reset
echo "Do you want to reset the database? (y/n)"
read answer
if [ "$answer" != "${answer#[Yy]}" ] ;then
    echo "Yes"
    lara-django/tools/reset_dev_db.sh
else
    echo "No"
fi

lara-django-dev init_django --make-all-migrations

