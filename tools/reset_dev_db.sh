#!/bin/bash

file_list=( \
"lara-django-base" \
"lara-django-people" \
"lara-django-store" \
"lara-django-material" \
"lara-django-material-store"  \
"lara-django-samples" \
"lara-django-substances" \
"lara-django-substances-store" \
"lara-django-organisms" \
"lara-django-organisms-store" \
"lara-django-data" \
"lara-django-processes" \
"lara-django-projects" \
"lara-django-sequences" \
"lara-django-library" \
)


reset_LARA_db () {
  echo __________________________ $file _________________________
  lara_app=${file//[-]/_}
   cd $file/$lara_app
     pwd
     # check if there are uncommitted changes
        # if [[ $(git status --porcelain) ]]; then
        # echo "There are uncommitted changes in $file"
        # exit 1
        # fi
      #echo rm -R $lara_app/migrations
      rm -R migrations
      ls
   cd ../..
   printf "\n\n"
}

# check, if the lara-django directory exists

if [ ! -d "lara-django" ]; then
  echo "The directory lara-django cannot be found, please run this script in the LARA apps root directory."
  exit 1
fi

# ask, if the database should be really reset

echo "This script will reset the LARA database and delete all migrations. Do you want to continue? (y/n)"
read answer
if [ "$answer" != "${answer#[Yy]}" ] ;then
    echo "Yes"
else
    echo "No"
    exit 1
fi

for file in ${file_list[*]}
do
   reset_LARA_db $file
done

rm lara-django/lara_django/db_lara_django_dev.sqlite3

lara-django-dev init_django 

#--make-all-migrations

