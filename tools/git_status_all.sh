#!/bin/bash

# This script is used to start a new release for all django apps of the LARA suite.

file_list=( \
"lara-django" \
 "lara-django-base" \
 "lara-django-people" \
  "lara-django-store" \
 "lara-django-material" \
 "lara-django-material-store"  \
 "lara-django-samples" \
 "lara-django-substances" \
 "lara-django-substances-store" \
 "lara-django-organisms" \
 "lara-django-organisms-store" \
 "lara-django-data" \
 "lara-django-processes" \
 "lara-django-projects" \
 "lara-django-sequences" \
 "lara-django-library" \
)

POSITIONAL_ARGS=()

HELP="Usage: git_status_all.sh[-r] [-f] [-h] \n \
 -f | --file-list : <file list file> \n \
 -h | --help : show this help message"

# if no arguments are given, show help message
# if [ $# -eq 0 ]
#   then
#     printf "$HELP"
#     exit 0
# fi

while [[ $# -gt 0 ]]; do
  case $1 in
   -f | --file-list)
      FILE_LIST="$2"
      shift # past argument
      shift # past value
      ;;
   -h |--help)
      # show help message --help or -h
        printf "$HELP"
        exit 0
      shift # past argument
      #shift # past value
      ;;
    # --default)
    #   DEFAULT=YES
    #   shift # past argument
    #   ;;
    -*|--*)
      echo "Unknown option $1"
        printf "$HELP"
      exit 1
      ;;
    *)
      POSITIONAL_ARGS+=("$1") # save positional arg
      shift # past argument
      ;;
  esac
done

set -- "${POSITIONAL_ARGS[@]}" # restore positional parameters

if [ -z "$FILE_LIST" ]
  then
    echo "No file list given, using the internal list. Use -f or --file-list option."    
# else generate file list from file list file
  else
    echo "Using file list from file $FILE_LIST"
    file_list=($(cat $FILE_LIST))
fi


# iterate over content of file list

for file in ${file_list[*]} #$(cat $FILE_LIST)
do
  echo __________________________ $file _________________________
   cd $file
   cat VERSION
   git branch -a
   git status
   
   cd ..
   printf "\n\n"
done

