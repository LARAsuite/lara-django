# LARA-django

The core LARA project database and webservice, hosting LARA-django applications for planning, storing and presenting experimental data and its meta data, realised as a python-django project


### Installation

The installation process of the database core of LARA is a very simple two-step process:
Download the *docker-compose* file and the *.env* file, and then run `docker compose pull and then up`, like described below:


To Run development version with docker compose:

```bash
 wget https://gitlab.com/LARAsuite/lara-django/-/raw/master/docker/docker-compose.dev-full.yml
 wget https://gitlab.com/LARAsuite/lara-django/-/raw/master/docker/.env.dev
 
 # or if you like curl:
 curl -O https://gitlab.com/LARAsuite/lara-django/-/blob/master/docker/docker-compose.dev-full.yml
 curl -O https://gitlab.com/LARAsuite/lara-django/-/blob/master/docker/.env.dev

```


**Important**:  rename .env.dev -> .env : 

```bash
 mv .env.dev .env
```

**Important**: in .env replace 'latest' in DOCKER_IMAGE_TAG=latest by current short [git commit hash (first 8 characters of hash)](https://gitlab.com/LARAsuite/lara-django/container_registry/4201028).

```bash
 # example: 
 DOCKER_IMAGE_TAG=b6e72259
```

**mind**: the **LARA admin / login credentials** are also set in the *.env file*, change them to your liking

To run docker compose as a non-root user, add your user to the docker group:
and mind to log out and in again to activate the group membership

```bash
sudo addgroup $USER docker
```

To pull the docker containers from the github container registry, run in the directory where the docker-compose file is located:
 
```bash
 docker compose -f docker-compose.dev-full.yml  pull
```


This above procedure needs to be done only once, to update the docker containers, run the above *pull command* again.

## Starting LARA-django with docker compose (development version)

 To start the docker containers, run in the directory where the docker-compose file is located (add -d to run in the background):

```bash
 docker compose -f docker-compose.dev-full.yml up
```


## Monitoring and Creating new entries to the LARA database:

Login to LARA-django interface with the credentials set in the .env file:

http://localhost:8008

or

http://127.0.0.1:8008 


If you see the LARA home screen, then  *Welcome to the new LARA world!*


## LARA-django admin interface:

http://localhost:8008/admin
or
http://127.0.0.1:8008/admin



## Environment variables

for development, please set (in the .env file)

    export DJANGO_ALLOWED_HOSTS=localhost
    export DJANGO_SETTINGS_MODULE=lara_django.settings.devel

for production, please set 

    export DJANGO_SETTINGS_MODULE=lara_django.settings.production

if your media does not reside in the default media folder, please set
environment variable to 

    export DJANGO_MEDIA_PATH='path/to/my/media'

to use user defined fixtures, please set: :: export
    
    DJANGO_FIXTURE_PATH='path/to/user/fixtures'


Testing all applications

## Basic Commands

### Type checks

Running type checks with mypy:

    $ mypy lara_django

### Test coverage

To run the tests, check your test coverage, and generate an HTML coverage report:

    $ coverage run -m pytest
    $ coverage html
    $ open htmlcov/index.html

#### Running tests with pytest

    $ pytest


[![Built with Cookiecutter Django](https://img.shields.io/badge/built%20with-Cookiecutter%20Django-ff69b4.svg?logo=cookiecutter)](https://github.com/cookiecutter/cookiecutter-django/)
[![Black code style](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/ambv/black)

License: GPLv3

