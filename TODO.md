# TODOs  - lara-django todo list


0.2.66
 - git repo URL
 - git commit hash

  - improved PID system (sample names, data, material, ...)
    https://www.youtube.com/watch?v=kWaJepwvbR0
    s. menoci.io

  - images: add BLOBS to all models to store image in database  (test how to reconstruct image from BLOB)
  - all admins: remove ids, useful lists, nice inlines, ...
  - add groups to instances & data
  - add groups to data / devices etc.


  - storage class (subst, organism) 
 
  - preparation next to experiments ? (like solution preparation)
  - mixture instances - procedure instance
  - itemstate (substances, mixtures, organmisms, material)
 
  - substance subset for simulation
  
 - setup rules for namespaces and paths (enforced by validators)

 - status of part/device/labware/sample ...
 
 - further cleaning of models (remove commented fields)
  - auth mixin for all ? / auth middleware
    Orcid OAuth / allauth

  - models Literature: ManyToManyField 

 - views:
  - substances view (s. Stuart Chalk, django tutorial)

 - fixtures
    - datatypes
    - tags
    - status
    - room category
    - sequence classes
    - residues
    - physical units
    - elements
    - std. substances

 - 404 handling s. django-cookiecutter
 - check, if saving with default values works for all models (also dates -> 0001-01-01)
 - correct hash calculation for all data
 - IRIs for semantics
 - gRPC interface / server client testing
  - data import
  
 - grpc interface generator -> publish
  lara-django-dev generateproto --model lara_base.models.GeoLocation  --file protos/lara_base.proto


 
 - setting up test environment
 - all simple unittests: CRUD
 
 - remove hardcoded links in files-fields
 - forms: autocomplete  https://django-select2.readthedocs.io/en/latest/


 - versioning of data, evaluation, visualisation (s. https://github.com/LARAsuite/django-reversion )

 - documentation
 - basic documentation

   - description of the architecture, data model, data flow
 - documentation generation in CI/CD pipline (s. cookiecutter)
 - demo data -> fake library https://faker.readthedocs.io/
 - autocomplete https://www.youtube.com/watch?v=Ch85i8yNT6E
   https://stackoverflow.com/questions/50380769/how-to-create-a-autocomplete-input-field-in-a-form-using-django
   https://stackoverflow.com/questions/66764548/django-assigning-or-saving-value-to-foreign-key-field
   tables2 pagination
   basic search
   -ics auto generation (see also: https://pypi.org/project/ics/
         and  https://pypi.org/project/icalendar/)


  tests (s. django-cookiecutter)

   - license -> MIT ?

  add concepts from django-cookiecutter

0.3
 - full semantic integration
 - 
 - processes: blockly (see there and old process)
 - lara-django data machine learning example (s. old repo)
- blockchain integration
 - vue.js frontend POC
 - add budget

## todo

lara scheduler

- lara-django-simplescheduler : a scheduler that directly interacts with the LARA database
- 


lara-django-report : report generator LaTeX / PDF

- semantic text editor



lara people:
 -auto slugify
 -urls
 -authetification

meta info categories to individual models

lara devices:
 - admin: inline extras ...

- correct  structure
 - replace slope by average
 - loop in ABAO
  ABAO : use resluts from L1

- ABAO En eval (using all L2  (which are already selected) : output selected, output stat, output boxplot

 - no of wells used in statistics

- combine everything and make a total run (time)

- fix filename of L1 growth

next version:

   python based

 - layout loader
  - exp loader
 - eval part of lara_data module Evaluation
 - moving management to lara
 - python based project definition
 - using eva for evalutions
 - generic views
 - new model (methods) -> view_class in models
 - Text barcode system !!!
 - eval scheduler: reacts on changing events: change triggers, next change triggers after time interval
    - eval: state tag (evaluated, not clean)
 - 3D view
 - spektrum / chromatogram view
 - Django-Wiki
 - user login system

 -removal of projects, experiments (with all related stuff)

  SciDraw vs ChemStruct: multiple views: 2D / 3D struct
  based on pymol, inkskape and chimera

!! always consider removing/correcting of entries
(model: on cascade ...)

generic views

lara_projects
 incorporation of published data
 add new project
 rst2html converter

lara_substances
 retrieving data from dbs
 sequences, electroporetogram
 plasmid maps

lara_containers
  import of container files

devices and device groups in analogy to people
 does and donts
 SOPS
 network settings

robot scripts / blockly

protocols/text documents versioning

literature database

documentation
  links to documentation

adopt to new, alphanumeric barcode system

possiblity of later adding of evaluations

adding data:
 create entry first in django, then just pass id to R

log term:
- get rid of colour package dependency (make own)
- getting rid of most R evaluation (some might move to rpy2)
- each evaluation/project has its own jupyter notebook
- mindmaps (e.g. javascript)

templates
_________

    * app level templates
    * project templates


settings
________

    * development / release-deployment


setup scripts
_____________

    * virtualenv
    * setup scripts
    * resolve dependencies

# setup

# probing os
https://pymotw.com/2/platform/

if OSX or Windows
manual



if linux

distribution

kernel

platform

cores
python


installation modes:

    interactive:
        completly user

    demo
        demo data
        local test server

    production
        virtual env
        postgreSQL
        engix

    test
        just performing all tests




installation in virtual env



validators, properties and setters
___________________________________


https://www.stavros.io/posts/how-replace-django-model-field-property/

A way to make the code more readable ?

  @property
    def name(self):
        return self._name

    @name.setter
    def name(self, value):
        if not value :
            self._name = u''.join((self.first_name, self.middle_names, self.last_name))
            logger.warning("no name given, generating a name")
        else :
            self._name = value

from django.core.exceptions import ValidationError


class Cart(models.Model):
    def __repr__(self):
        return self.product

    class Meta:
        db_table = 'db_cart'
        verbose_name = 'cart'
        verbose_name_plural = 'cart'
        unique_together = ("user", "product")
        ordering = ['-id']

    #validation in django administration
    def validate_quantity(value):
        if value == 0 or value > 999:
            raise ValidationError('%s - is incorrect number, 1-999 only!' % value)

    user = models.ForeignKey(User)
    quantity = models.PositiveIntegerField(validators=[validate_quantity], default=1)
    product = models.ForeignKey(Products)

    @property
    def total_item_cost(self):
        return self.quantity * self.product.price

Device life view
Menu: acknowledements documentatin

Interactive evaluation
image maps
documentation through the system
blockly and json based process editor

devive view: webcam output of current dev
gant chart of process
order and selection per user e.g. for reporting or comparision of exp. Results
container info
Excjange protocol with github
Device bookig
Collection of sust containers exper results
Add collection to project

Rich text editor f. Django
round user ivon on each experiment
Store location
Orders
Notify
Interaction
process planning in lara
larastore
Laraorders
Login on demand

literature
Login
sequencing
Network settings

Fix title descr: date,bc
Project status
rt transformation

# ver 0.1
local vs. Remote settings
Test vs. Prod. Settings
all current features working
raw csv layout in db
Raw JASON in db
CSV table structure Act. Experiments
unittests
Docu.
Models on del cascade

# ver 0.2
preparation for postgresql
Prep. F. Python 3
interface 2 lara
Webservices

# major changes for 0.3
Python 3
Auth. For certain projects

Barcode alphanumeric
New database engine PostgreSQL ?
NgiX as data webserver
mM to uM

# Structuring data
 django
- project view of subtree
- project view-> plate table
- user app: affil. Recent Experiments,
- dev. View
- dev maint.  Reminder
- upload new subst.
- unittest subst.
-
- ext. Data images
- add navigation treeview
- remove/deleting  subtree
-

# R evaluation
- auto def clsss
- enzyme units
- varioskan matrix
- meta info in list
- plots indep. of geometry
- index excl.
- list df
- evaluation parent
T test f. Outlier
Modifikation Mutation derivatisation
Unit chooser U catal au/s
Exp state: test replication final
Average blank
Outlier test
Baseline correction
AV over replicates in hitplate

# plots
- Col in col plot
JavaScript f. Spectral plots s. Fl page
auto range plot for all plots

# database structure
Schemes and figs also f. Introduction.
Proj Item class to Item class
Unifying resultiert classes
Product
Gene as a compound
Substance table
- updating data in DB
Sequencing data
Jason plante layout
MD5 sha sum hash also f. File integrety
All robot devices
Better method descr. Parameters
Cell strain
Medium
Lysis cond.

Chemicals Storage location multiple places
MongoDb f. Results and sequences
Sqlite f. Process relevant data


# Assays
Selection of results

# MM Kin
-MM Kin add 0

# growth
- use only last for statistics
- histogram

# Kemp assay
- number of replicates
- average and error on replicates
- T-test
- average without outliers

# FL assay
Fl panel

#other documentation
PCR and PAGE

# Sequencing
- read seq. data
- find reading frame
- translate
- align with ref/wt
- find mutations
- plot electroporetogram
- integrate electroporetogram

# Webpage
- breadcrumb navigation
Plate layout 2 HTML
Primer sequence
Syntetic gene seq
Arrangements
Linker spacer
Fig. Caption css
Static html export
Javaskript spectrum and chromatogr.
Plate layout view
device view user view
Image map
Source button
Link to full table

# Android APP
- Barcode reader: display of plate info
- adding new plates to db

# Init projects
 -  check option
-  multi barcode auto generate:  improve generation
- faster DB import: profiling

# Report generator
- table conv
- rem undersc in titles
- Description without math env
- vars for methods and devs
- generic table translation
- correct description and  Titles and xlim
- output as LaTeX or PDF

Overview Scheme reaction
Js button - post button
Report view
report css
Import html in oo
generic table (LaTeX, html)
generic special characters
Reporting string bitfield final trail
 - selector: important, test, to publish
- svg in LaTeX

# old files handling
 - script to adjust to new filename scheme
 - omega ID: wrong

# Later development
- Gene explorer
 - logging of errors -> webpage
