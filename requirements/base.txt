wheel
PyYAML==6.0

# minio
s3fs==2023.12.1
boto3==1.33.1

django==4.2.9
django-environ==0.11.2  # https://github.com/joke2k/django-environ
django-model-utils==4.3.1  # https://github.com/jazzband/django-model-utils
django-mptt==0.14.0
django-crispy-forms==2.0  # https://github.com/django-crispy-forms/django-crispy-forms
django-bootstrap5==23.3
crispy-bootstrap5==0.7  # https://github.com/django-crispy-forms/crispy-bootstrap5
django-cors-headers==4.3.1  # https://github.com/adamchainz/django-cors-headers
django-storages==1.14.2  #
# DRF-spectacular for api documentation
drf-spectacular==0.26.2  # https://github.com/tfranzel/drf-spectacular
django-tables2==2.5.3
tablib==3.5.0
django-filter==23.2
# colour might be included as source in LARA - it is just one file
colour==0.1.5
Pillow==10.2.0

#grpc
django-socio-grpc==0.22.6
grpcio==1.64.1
grpcio-tools==1.64.1

#semantics
EMMOntoPy==0.7.2
rdflib==6.3.2
SPARQLWrapper==2.0.0

# for prefect
prefect==2.16.3

# celery 
celery==5.3.6

# for LARA:
# PyPDF2>=1.26.0
#-r lara_django_apps.txt

--extra-index-url https://gitlab.com/api/v4/projects/24034636/packages/pypi/simple
chemdatareader==0.0.4