# using webpack with LARA django

## setup

1. Initialise the new project with the following command:

```bash
npm init 
```
2. Install webpack and webpack-cli:

```bash
npm install webpack webpack-cli --save-dev
```
3. Install the following additional packages / plugins:

```bash
npm install --save-dev mini-css-extract-plugin webpack-merge webpack-dev-server webpack-bundle-tracker
```

4. Install the following loaders:

```bash
npm install --save-dev css-loader style-loader sass-loader node-sass postcss-loader postcss-preset-env autoprefixer babel-loader @babel/core @babel/preset-env 
```

4. Create a new file named `common.config.js` in the webpack directory and add the following code:

```javascript
const path = require('path');



# example of common.config.js

{
  "name": "lara_django_vue3",
  "version": "0.0.1",
  "devDependencies": {
    "@babel/core": "^7.16.5",
    "@babel/preset-env": "^7.16.5",
    "@bufbuild/buf": "^1.42.0",
    "@bufbuild/protoc-gen-es": "^1.10.0",
    "@connectrpc/protoc-gen-connect-es": "^1.5.0",
    "@popperjs/core": "^2.10.2",
    "autoprefixer": "^10.4.0",
    "babel-loader": "^9.1.2",
    "bootstrap": "^5.2.3",
    "css-loader": "^7.1.2",
    "mini-css-extract-plugin": "^2.4.5",
    "node-sass-tilde-importer": "^1.0.2",
    "pixrem": "^5.0.0",
    "postcss": "^8.3.11",
    "postcss-loader": "^8.0.0",
    "postcss-preset-env": "^10.0.3",
    "sass": "^1.43.4",
    "sass-loader": "^16.0.1",
    "vue-loader": "^17.4.2",
    "vue-template-compiler": "^0.1.0",
    "webpack": "^5.95.0",
    "webpack-bundle-tracker": "^3.0.1",
    "webpack-cli": "^5.1.4",
    "webpack-dev-server": "^5.0.2",
    "webpack-merge": "^6.0.1"
  },
  "engines": {
    "node": "22"
  },
  "browserslist": [
    "last 2 versions"
  ],
  "babel": {
    "presets": [
      "@babel/preset-env"
    ]
  },
  "scripts": {
    "dev": "webpack serve --config webpack/dev.config.js",
    "build": "webpack --config webpack/prod.config.js",
    "build-dev": "webpack --config webpack/dev.config.js"
  },
  "dependencies": {
    "@bufbuild/protobuf": "^1.10.0",
    "@connectrpc/connect": "^1.6.1",
    "@connectrpc/connect-web": "^1.6.1",
    "@fullcalendar/core": "^6.1.15",
    "@fullcalendar/daygrid": "^6.1.15",
    "@fullcalendar/interaction": "^6.1.15",
    "@fullcalendar/timegrid": "^6.1.15",
    "@fullcalendar/vue3": "^6.1.15",
    "@mdi/font": "^7.4.47",
    "@vuepic/vue-datepicker": "^9.0.3",
    "google-protobuf": "^3.21.4",
    "grpc-web": "^1.5.0",
    "npm": "^10.8.3",
    "roboto-fontface": "^0.10.0",
    "vue": "^3.5.9",
    "vuetify": "^3.7.2"
  }
}

## tailwind.css

1. Install the following packages:

```bash
npm install tailwindcss postcss autoprefixer --save-dev
```

2. Create a new file named `tailwind.config.js` in the root directory and add the following code:

```javascript
module.exports = {
  purge: [
    './**/*.html',
    './**/*.js',
    './**/*.jsx',
    './**/*.ts',
    './**/*.tsx',
    './**/*.vue',
  ],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {},
  },
  variants: {
    extend: {},
  },
  plugins: [],
}
```

npx tailwindcss -i ./lara_django/assets/styles/input.css -o ./lara_django/static/css/output.css