# Development tools

## git flow

git flow is a set of git extensions to provide high-level repository operations for Vincent Driessen's branching model.

[git flow]()

## Changelog generation

changelog.sh for the ohmyzsh project is a good example of how to generate a changelog from git commits.

(tools/changelog.sh script)[https://github.com/ohmyzsh/ohmyzsh/blob/beeda72826f7288d3edf6cec4114bbda9bbae347/tools/changelog.sh]


## git commit message

https://www.conventionalcommits.org/en/v1.0.0/


## git semantic versioning

https://semver.org/


## LARA tools

### clone all repositories

```bash
  tools/clone_all_lara_repos.sh
```

### remove migrations

```bash
  tools/remove_migrations.sh
```

### display the status of all git repositories

```bash
  tools/git_status_all.sh
```